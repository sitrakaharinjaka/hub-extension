<?php

namespace App\Validator\Constraints;

use App\Utils\Managers\MyBoxesProductManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MyBoxesItemValidator extends ConstraintValidator
{
    protected $myBoxesProductManager;

    public function __construct(MyBoxesProductManager $myBoxesProductManager)
    {
        $this->myBoxesProductManager = $myBoxesProductManager;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $products = $this->myBoxesProductManager->findAll();

        /** @var MyBoxesCommand $command */
        $command = $value;

        $total = 0;
        foreach ($command->getItems() as $item) {
            $total = $item->getQuantity() + $total;

            if ($item->getQuantity() > $item->getProduct()->getStock()) {
                $this->context->buildViolation("La quantité demandée est supérieure au stock possible pour un des type de caisse (".$item->getProduct()->getName().")")->addViolation();
            }
        }

        if ($total <= 0) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
