<?php

namespace App\Entity\Traits;

trait GpsTrait
{
    /**
     * Longitude
     *
     * @ORM\Column(type="float", nullable=true)
     */
    protected $longitude;

    /**
     * Latitude
     *
     * @ORM\Column(type="float", nullable=true)
     */
    protected $latitude;

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }
}
