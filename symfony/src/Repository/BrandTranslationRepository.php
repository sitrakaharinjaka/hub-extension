<?php

/**
 * Repository
 *
 * @author Pierre Baumes <pierre.b@disko.fr>
 */

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class BrandTranslationRepository
 *
 * @package App\Repository
 */
class BrandTranslationRepository extends BaseRepository
{
}
