<?php

// src/Twig/StaticClassExtension.php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class StaticNamespaceExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('staticNamespace', [$this, 'staticNamespace']),
        ];
    }

    public function staticNamespace($object)
    {
        if (! is_object($object)) {
            return $object;
        }

        return get_class($object);
    }
}
