<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Paweł Jędrzejewski
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace App\Form\Type\MyBoxes;

use App\Entity\MyBoxesCommand;
use App\Entity\MyBoxesItem;
use App\Form\Type\MyBoxes\ItemType;
use App\Utils\Managers\MyBoxesItemManager;
use App\Utils\Managers\MyBoxesProductManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BringHomeType extends AbstractType
{
    protected $myBoxesProductManager;
    protected $myBoxesItemManager;

    public function __construct(MyBoxesItemManager $myBoxesItemManager, MyBoxesProductManager $myBoxesProductManager)
    {
        $this->myBoxesItemManager = $myBoxesItemManager;
        $this->myBoxesProductManager = $myBoxesProductManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder ->add('firstname', TextType::class, [
            'label' => "Prénom",
            'required' => true,
            'attr' => [
                'class' => 'field selectric-disabled'
            ]
        ]);
        $builder ->add('lastname', TextType::class, [
            'label' => "Nom",
            'required' => true,
            'attr' => [
                'class' => 'field selectric-disabled'
            ]
        ]);
        $builder ->add('officeNumber', TextType::class, [
            'label' => "Numéro de bureau",
            'required' => true,
            'attr' => [
                'class' => 'field selectric-disabled'
            ]
        ]);
        $builder ->add('stage', TextType::class, [
            'label' => "Etage",
            'required' => true,
            'attr' => [
                'class' => 'field selectric-disabled'
            ]
        ]);

        $builder->add('adressType', ChoiceType::class, [
            'label' => "Adresse de destination",
            'choices'  => [
                MyBoxesCommand::AD_BAC => MyBoxesCommand::AD_BAC,
                MyBoxesCommand::AD_ARMEE => MyBoxesCommand::AD_ARMEE,
            ],
            'attr' => [
                'class' => 'box-type',
            ],
            'mapped' => false,
            'required' => true,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MyBoxesCommand::class,
            'validation_groups' => ['Create']
        ]);
    }
}
