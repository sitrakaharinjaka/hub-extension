<?php

namespace App\Controller\Admin;

use App\Entity\Project;
use App\Entity\RunnerTracing;
use App\Entity\StockEntry115;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\ProjectItemManager;
use App\Utils\Managers\RunnerTracingManager;
use App\Utils\Managers\StockEntry115ItemManager;
use App\Utils\Managers\StockEntry115Manager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\Services\MailerService;

/**
 * Class RunnerController
 * @Route("/admin/runner")
 * @package App\Controller
 */
class RunnerController extends BaseController
{
    /**
     * Homepage Runner
     * @Route("/", name="runner_homepage")
     * @return Response
     */
    public function index(ProjectManager $projectManager, StockEntry115Manager $entry115Manager)
    {
        $waitingOrders = $projectManager->count([
            'cave'  => Project::CAVE_115,
            'state' => [Project::STATE_HUB_ACCEPTED_FIXED, Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE],
        ]);

        $runner           = $this->getUser();
        $inProgressOrders = $projectManager->count([
            'cave'   => Project::CAVE_115,
            'state'  => Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE,
            'runner' => $runner,
        ]);

        $shippedOrders = $projectManager->count([
            'cave'  => Project::CAVE_115,
            'state' => [Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP],
        ]);

        $deliveredOrders = $projectManager->count([
            'cave'  => Project::CAVE_115,
            'state' => [Project::STATE_HUB_DELIVERED],
        ]);

        $waitingStockEntries = $entry115Manager->count([
            'cave'   => StockEntry115::CAVE_115,
            'status' => [StockEntry115::STATE_REQUEST_TAKEN],
        ]);

        $updatedStockEntries = $entry115Manager->count([
            'cave'   => StockEntry115::CAVE_115,
            'status' => [StockEntry115::STATE_ITEM_UPDATE],
        ]);

        $validatedStockEntriesQuery = $entry115Manager->queryForSearch([
            "cave"   => StockEntry115::CAVE_115,
            "status" => [StockEntry115::STATE_ITEM_VALIDATE],
        ]);

        $deliveredOrdersQuery = $projectManager->queryForSearch([
            "cave"  => Project::CAVE_115,
            "state" => [Project::STATE_HUB_DELIVERED],
        ]);

        $paginator  = $this->get('knp_paginator');
        $validatedStockEntriesLatest = $paginator->paginate(
            $validatedStockEntriesQuery,
            1,
            10,
            ['wrap-queries' => true]
        );

        $deliveredOrdersLatest = $paginator->paginate(
            $deliveredOrdersQuery,
            1,
            10,
            ['wrap-queries' => true]
        );

        return $this->render('@Admin/Runner/index.html.twig', [
            'active'                      => 'general',
            'waitingOrders'               => $waitingOrders,
            'inProgressOrders'            => $inProgressOrders,
            'shippedOrders'               => $shippedOrders,
            'deliveredOrders'             => $deliveredOrders,
            'waitingStockEntries'         => $waitingStockEntries,
            'updatedStockEntries'         => $updatedStockEntries,
            'validatedStockEntriesLatest' => $validatedStockEntriesLatest,
            'deliveredOrdersLatest'       => $deliveredOrdersLatest,
        ]);
    }

    /**
     * Liste des commandes en attentes
     * @Route("/commandes-en-attentes", name="runner_waiting")
     * @return Response
     */
    public function waitingList(Request $request, ProjectManager $projectManager)
    {
        $query = $projectManager->queryForSearch([
            "cave"  => Project::CAVE_115,
            "state" => [Project::STATE_HUB_ACCEPTED_FIXED, Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE],
        ]);

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            ['wrap-queries' => true]
        );

        return $this->render('@Admin/Runner/waiting.html.twig', [
            'active'     => 'waiting',
            'pagination' => $pagination,
        ]);
    }

    /**
     * Liste des entrées de stock en attentes
     * @Route("/entrees_de_stock-en-attentes", name="runner_stock_entry_115_waiting")
     * @return Response
     */
    public function waitingStockEntryList(Request $request, StockEntry115Manager $stockEntry115Manager)
    {
        $query = $stockEntry115Manager->queryForSearch([
            "cave"   => StockEntry115::CAVE_115,
            "status" => [StockEntry115::STATE_REQUEST_TAKEN],
        ]);

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            ['wrap-queries' => true]
        );

        return $this->render('@Admin/Runner/waitingStockEntry115.html.twig', [
            'active'     => 'stock_entry_115_waiting',
            'pagination' => $pagination,
        ]);
    }

    /**
     * Liste des entrées de stock en validees
     * @Route("/entrees_de_stock-validees", name="runner_stock_entry_115_validated")
     * @return Response
     */
    public function validatedStockEntryList(Request $request, StockEntry115Manager $stockEntry115Manager)
    {
      $query = $stockEntry115Manager->queryForSearch([
        "cave"   => StockEntry115::CAVE_115,
        "status" => [StockEntry115::STATE_ITEM_VALIDATE],
      ]);

      // Paginate transform
      $paginator  = $this->get('knp_paginator');
      $pagination = $paginator->paginate(
        $query,
        $request->query->get('page', 1),
        20,
        ['wrap-queries' => true]
      );

      return $this->render('@Admin/Runner/validatedStockEntry115.html.twig', [
        'active'     => 'stock_entry_115_validated',
        'pagination' => $pagination,
      ]);
    }

    /**
     * Ramassage d'une entrée de stock 115
     * @Route("/ramasser/{id}", name="runner_ramasser")
     * @return Response
     */
    public function pickUp(Request $request, StockEntry115Manager $stockEntry115Manager, MailerService $mailer, $id)
    {
        $stockEntry = $stockEntry115Manager->findOneBy([
            "cave" => StockEntry115::CAVE_115,
            'id'   => $id,
        ]);

        //$stockEntry->setStatus(StockEntry115::STATE_IN_STOCK);
        //$stockEntry115Manager->save($stockEntry);

        return $this->redirect($this->generateUrl('runner_preparation_entry_stock', ['id' => $id]));
    }

    /**
     * Lancement d'une préparation de commande
     * @Route("/lancer-preparation/{id}", name="runner_preparation_start")
     * @return Response
     */
    public function preparationStart(Request $request, ProjectManager $projectManager, MailerService $mailer, RunnerTracingManager $runnerTracingManager, $id)
    {
        $project = $projectManager->findOneBy([
            "cave" => Project::CAVE_115,
            'id'   => $id,
        ]);

        if (!$project) {
            throw $this->createNotFoundException('Project not found');
        }

        $runner = $this->getUser();
        if (sizeof($runner->getPreparationsFiltered(Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE)) > 0) {
            $this->get('session')->getFlashBag()->set('error', 'Vous avez déjà une commande en cours de préparation.');

            return $this->redirect($this->generateUrl('runner_waiting', []));
        }

        $runner->addPreparation($project);
        $project->setState(Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE);
        $projectManager->save($project);
        $mailer->sendMailCave115PrepareProject($project);

        $runnerTracing = new RunnerTracing();
        $runnerTracing->setProject($project);
        $runnerTracing->setState(Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE);
        $runnerTracing->setRunner($project->getRunner()->getUsername());
        $runnerTracingManager->save($runnerTracing);

        return $this->redirect($this->generateUrl('runner_preparation', []));
    }

    /**
     * Lancement d'une préparation de commande
     * @Route("/annuler-preparation/{id}", name="runner_preparation_cancel")
     * @return Response
     */
    public function preparationCancel(Request $request, ProjectManager $projectManager, MailerService $mailer, UrlGeneratorInterface $router, RunnerTracingManager $runnerTracingManager, $id)
    {
        $referer = $this->getReferer($request, $router) || 'runner_preparation';
        $project = $projectManager->findOneBy([
            "cave" => Project::CAVE_115,
            'id'   => $id,
        ]);

        if (!$project) {
            throw $this->createNotFoundException('Project not found');
        }

        if ($project->getState() !== Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE) {
            $this->get('session')->getFlashBag()->set('error', 'Impossible d\'annuler cette préparation de commande.');

            return $this->redirectToRoute('runner_waiting');
        }

        $runner = $project->getRunner();
        $runner->removePreparation($project);
        $project->setState(Project::STATE_HUB_ACCEPTED_FIXED);

        foreach ($project->getItems() as $item) {
            $item->setRunnerStatus(null);
            $item->setRunnerComment(null);
        }

        $projectManager->save($project);
        $mailer->sendMailCave115UnprepareProject($project);
        $this->get('session')->getFlashBag()->set('notice', 'Préparation de commande réinitialisée.');

        $runnerTracing = new RunnerTracing();
        $runnerTracing->setProject($project);
        $runnerTracing->setState(Project::STATE_HUB_ACCEPTED_FIXED);
        $runnerTracing->setRunner($runner->getUsername());
        $runnerTracingManager->save($runnerTracing);

        return $this->redirect($this->generateUrl('runner_preparation', []));
    }

    /**
     * Validation de la préparation de commande
     * @Route("/valider-preparation/{id}", name="runner_preparation_valid")
     * @return Response
     */
    public function preparationValid(Request $request, ProjectManager $projectManager, MailerService $mailer, RunnerTracingManager $runnerTracingManager, $id)
    {
        $project = $projectManager->findOneBy([
            "cave" => Project::CAVE_115,
            'id'   => $id,
        ]);

        if (!$project) {
            throw $this->createNotFoundException('Project not found');
        }

        if ($project->getState() !== Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE) {
            $this->get('session')->getFlashBag()->set('error', 'Impossible de valider cette préparation de commande.');

            return $this->redirect($this->generateUrl('runner_preparation', []));
        }

        foreach ($project->getItems() as $item) {
            $status = $item->getRunnerStatus();
            if ($status !== 'ok' && $status !== 'partial' && $status !== 'empty') {
                $this->get('session')->getFlashBag()
                     ->set('error', 'Impossible de valider cette préparation de commande.')
                ;

                return $this->redirect($this->generateUrl('runner_preparation', []));
            }
        }

        $responsable = $project->getRunner();
        $runner      = $this->getUser();
        if ($runner->getId() != $responsable->getId()) {
            $this->get('session')->getFlashBag()->set('error', 'Impossible de valider cette préparation de commande.');

            return $this->redirect($this->generateUrl('runner_preparation', []));
        }

        $project->setState(Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP);
        $projectManager->save($project);
        $mailer->sendMailCave115ExpeditionProject($project);

        $runnerTracing = new RunnerTracing();
        $runnerTracing->setProject($project);
        $runnerTracing->setState(Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP);
        $runnerTracing->setRunner($project->getRunner()->getUsername());
        $runnerTracingManager->save($runnerTracing);

        $this->get('session')->getFlashBag()->set('notice', 'Commande préparée.');

        return $this->redirect($this->generateUrl('runner_waiting', []));
    }

    /**
     * Rejet de la préparation de commande
     * @Route("/reject-preparation/{id}", name="runner_preparation_reject")
     * @return Response
     */
    public function preparationReject(Request $request, ProjectManager $projectManager, MailerService $mailer, RunnerTracingManager $runnerTracingManager, $id)
    {
        $project = $projectManager->findOneBy([
            "cave" => Project::CAVE_115,
            'id'   => $id,
        ]);

        if (!$project) {
            throw $this->createNotFoundException('Project not found');
        }

        if ($project->getState() !== Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE) {
            $this->get('session')->getFlashBag()->set('error', 'Impossible de rejeter cette préparation de commande.');

            return $this->redirect($this->generateUrl('runner_preparation', []));
        }

        foreach ($project->getItems() as $item) {
            $status = $item->getRunnerStatus();
            if ($status !== 'ok' && $status !== 'partial' && $status !== 'empty') {
                $this->get('session')->getFlashBag()
                     ->set('error', 'Impossible de rejeter cette préparation de commande.')
                ;

                return $this->redirect($this->generateUrl('runner_preparation', []));
            }
        }

        $responsable = $project->getRunner();
        $runner      = $this->getUser();
        if ($runner->getId() != $responsable->getId()) {
            $this->get('session')->getFlashBag()->set('error', 'Impossible de rejeter cette préparation de commande.');

            return $this->redirect($this->generateUrl('runner_preparation', []));
        }

        $project->setState(Project::STATE_HUB_CANCEL);
        $projectManager->save($project);

        #todo mail commande reject
        //$mailer->sendMailCave115ExpeditionProject($project);

        $runnerTracing = new RunnerTracing();
        $runnerTracing->setProject($project);
        $runnerTracing->setState(Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP);
        $runnerTracing->setRunner($project->getRunner()->getUsername());
        $runnerTracingManager->save($runnerTracing);

        $this->get('session')->getFlashBag()->set('error', 'Commande rejetée.');

        return $this->redirect($this->generateUrl('runner_waiting', []));
    }

    /**
     * Validation de la préparation entrée de stock
     * @Route("/valider-preparation-stock-entry/{id}", name="runner_preparation_stock_entry_valid")
     * @return Response
     */
    public function preparationStockEntryValid(Request $request, StockEntry115Manager $stockEntry115Manager, MailerService $mailer, RunnerTracingManager $runnerTracingManager, $id)
    {
        $stockEntry115 = $stockEntry115Manager->findOneBy([
            "cave" => Project::CAVE_115,
            'id'   => $id,
        ]);

        if (!$stockEntry115) {
            throw $this->createNotFoundException('Project not found');
        }

        /*if ($project->getState() !== Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE) {
          $this->get('session')->getFlashBag()->set('error', 'Impossible de valider cette préparation de commande.');
          return $this->redirect($this->generateUrl('runner_preparation', array()));
        }*/

        foreach ($stockEntry115->getStockEntry115Items() as $item) {
            $status = $item->getStatus();
            if ($status !== 'ok' && $status !== 'partial' && $status !== 'empty') {
                $this->get('session')->getFlashBag()->set('error', 'Impossible de valider cette entrée de commande.');

                return $this->redirect($this->generateUrl('runner_preparation_entry_stock', ['id' => $stockEntry115->getId()]));
            }
        }

        /*$responsable = $project->getRunner();
        $runner = $this->getUser();
        if ($runner->getId() != $responsable->getId()) {
          $this->get('session')->getFlashBag()->set('error', 'Impossible de valider cette préparation de commande.');
          return $this->redirect($this->generateUrl('runner_preparation', array()));
        }*/

        $stockEntry115->setStatus(StockEntry115::STATE_ITEM_UPDATE);
        $stockEntry115Manager->save($stockEntry115);

        $mailer->sendStocketEntryUpdate($stockEntry115);

        return $this->redirect($this->generateUrl('runner_stock_entry_115_waiting', []));
    }

    /**
     * Validation d'un item de commande
     * @Route("/valider-item/{id}/{status}", name="runner_preparation_validation_item")
     * @return Response
     */
    public function preparationValidationItem(Request $request, ProjectItemManager $projectItemManager, $id, $status)
    {
        $item = $projectItemManager->findOneBy([
            'id' => $id,
        ]);

        if (!$item) {
            throw $this->createNotFoundException('Item not found');
        }

        $project = $item->getProject();
        if (!$project) {
            throw $this->createNotFoundException('Item error');
        }

        if ($project->getState() != Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE) {
            throw $this->createNotFoundException('Bad state');
        }

        $responsable = $project->getRunner();
        if (!$responsable) {
            throw $this->createNotFoundException('Bad runner');
        }

        $runner = $this->getUser();
        if ($runner->getId() != $responsable->getId()) {
            throw $this->createNotFoundException('Bad runner');
        }

        $comment = $request->get('comment', null);

        $item->setRunnerStatus($status);
        $item->setRunnerComment($comment);
        $projectItemManager->save($item);

        return $this->redirect($this->generateUrl('runner_preparation', [])/* . "#item-" . $id*/);
    }

    /**
     * Validation d'un item de commande
     * @Route("/valider-item-entry-stock/{id}/{status}", name="runner_preparation_entry_stock_validation_item")
     * @return Response
     */
    public function preparationValidationEntryStockItem(Request $request, StockEntry115ItemManager $stockEntry115ItemManager, $id, $status)
    {
        $item = $stockEntry115ItemManager->findOneBy([
            'id' => $id,
        ]);

        if (!$item) {
            throw $this->createNotFoundException('Item not found');
        }

        /*$project = $item->getProject();
        if (!$project) {
          throw $this->createNotFoundException('Item error');
        }*/

        /*if ($project->getState() != Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE) {
          throw $this->createNotFoundException('Bad state');
        }*/

        /*$responsable = $project->getRunner();
        if (!$responsable) {
          throw $this->createNotFoundException('Bad runner');
        }*/

        /*$runner = $this->getUser();
        if ($runner->getId() != $responsable->getId()) {
          throw $this->createNotFoundException('Bad runner');
        }*/

        $comment  = $request->get('comment', null);

        $item->setStatus($status);
        $item->setComment($comment);

        if ('ok' === $status) {
            $item->setQuantityReceived($item->getQuantity());
        } else {
            $item->setQuantityReceived(intval($request->get('quantity', null)));
        }

        $stockEntry115ItemManager->save($item);

        return $this->redirect($this->generateUrl('runner_preparation_entry_stock', [
            'id' => $item->getStockEntry115()->getId(),
        ])/* . "#item-" . $id*/);
    }

    /**
     * Commande en cours de préparation
     * @Route("/commande-en-preparation", name="runner_preparation")
     * @return Response
     */
    public function preparationEdit(Request $request, ProjectManager $projectManager)
    {
        $runner  = $this->getUser();
        $project = $projectManager->findOneBy([
            "cave"   => Project::CAVE_115,
            "state"  => Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE,
            "runner" => $runner,
        ]);

        return $this->render('@Admin/Runner/preparation.html.twig', [
            'active'  => 'preparation',
            'project' => $project,
        ]);
    }

    /**
     * Commande en cours de préparation
     * @Route("/entree-de-stock-en-preparation/{id}", name="runner_preparation_entry_stock")
     * @return Response
     */
    public function preparationEntryStockEdit(Request $request, StockEntry115Manager $stockEntry115Manager, $id)
    {
        $runner = $this->getUser();
        /*$project = $projectManager->findOneBy([
          "cave" => Project::CAVE_115,
          "state" => Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE,
          "runner" => $runner,
        ]);*/

        $stockEntry = $stockEntry115Manager->findOneBy([
            "cave" => StockEntry115::CAVE_115,
            'id'   => $id,
        ]);

        //$stockEntry->setStatus(StockEntry115::STATE_IN_STOCK);
        //$stockEntry115Manager->save($stockEntry);

        return $this->render('@Admin/Runner/preparation_stock_entry.html.twig', [
            'active'     => 'preparation',
            'stockEntry' => $stockEntry,
        ]);
    }

    /**
     * Liste des commandes en cours d'acheminement
     * @Route("/commandes-en-cours-d-acheminement", name="runner_inway")
     * @return Response
     */
    public function inWayList(Request $request, ProjectManager $projectManager)
    {
        $query = $projectManager->queryForSearch([
            "cave"  => Project::CAVE_115,
            "state" => [Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP],
        ]);

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            ['wrap-queries' => true]
        );

        return $this->render('@Admin/Runner/inway.html.twig', [
            'active'     => 'inway',
            'pagination' => $pagination,
        ]);
    }

    /**
     * Livraison d'une commande
     * @Route("/livrer/{id}", name="runner_deliver")
     * @return Response
     */
    public function deliver(Request $request, ProjectManager $projectManager, MailerService $mailer, $id)
    {
        $project = $projectManager->findOneBy([
            "cave" => Project::CAVE_115,
            'id'   => $id,
        ]);

        if (!$project) {
            throw $this->createNotFoundException('Project not found');
        }

        if ($project->getState() !== Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP) {
            $this->get('session')->getFlashBag()->set('error', 'Impossible de livrer cette commande.');

            return $this->redirect($this->generateUrl('runner_inway', []));
        }

        $responsable = $project->getRunner();
        $runner      = $this->getUser();
        if ($runner->getId() != $responsable->getId()) {
            $this->get('session')->getFlashBag()->set('error', 'Impossible de livrer cette commande.');

            return $this->redirect($this->generateUrl('runner_inway', []));
        }

        $project->setLandingPerson($request->get('receiver', null));
        $project->setLandingComment($request->get('comment', null));
        $project->setLandingHour($request->get('hour', null));

        $project->setState(Project::STATE_HUB_DELIVERED);
        $projectManager->save($project);
        $mailer->sendMailCave115DeliverProject($project);

        $this->get('session')->getFlashBag()->set('notice', 'Commande livrée.');

        return $this->redirect($this->generateUrl('runner_inway', []));
    }

    /**
     * Liste des commandes terminées
     * @Route("/commandes-terminees", name="runner_delivered")
     * @return Response
     */
    public function deliveredList(Request $request, ProjectManager $projectManager)
    {
        $query = $projectManager->queryForSearch([
            "cave"  => Project::CAVE_115,
            "state" => [Project::STATE_HUB_DELIVERED],
        ]);

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            ['wrap-queries' => true]
        );

        return $this->render('@Admin/Runner/delivered.html.twig', [
            'active'     => 'delivered',
            'pagination' => $pagination,
        ]);
    }

    /**
     * Liste des centrées de stock terminées
     * @Route("/entrees-stock-terminees", name="runner_delivered_stock_entry_115")
     * @return Response
     */
    public function deliveredStockEntryList(Request $request, StockEntry115Manager $stockEntry115Manager)
    {
        $query = $stockEntry115Manager->queryForSearch([
            "cave"  => StockEntry115::CAVE_115,
            "state" => [StockEntry115::STATE_IN_STOCK],
        ]);

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            ['wrap-queries' => true]
        );

        return $this->render('@Admin/Runner/deliveredStockEntry115.html.twig', [
            'active'     => 'delivered',
            'pagination' => $pagination,
        ]);
    }

    private function getReferer(Request $request, UrlGeneratorInterface $router)
    {
        $referer = $request->headers->get('referer'); // get the referer, it can be empty!
        if (!\is_string($referer) || !$referer) {
            return null;
        }
        $refererPathInfo = Request::create($referer)->getPathInfo();
        // Remove the scriptname if using a dev controller like app_dev.php (Symfony 3.x only)
        $refererPathInfo = str_replace($request->getScriptName(), '', $refererPathInfo);
        // try to match the path with the application routing
        $routeInfos = $router->match($refererPathInfo);
        // get the Symfony route name
        $refererRoute = $routeInfos['_route'] ?? '';
        // No route found (external URL for example)
        if (!$refererRoute) {
            return null;
        }

        return $refererRoute;
    }
}
