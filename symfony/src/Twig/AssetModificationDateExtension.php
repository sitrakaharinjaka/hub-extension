<?php
namespace App\Twig;

class AssetModificationDateExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('assetModificationDate', array($this, 'assetModificationDate')),
        );
    }

    public function assetModificationDate($asset)
    {
        $path = __DIR__;
        $filename = $path . '/../../public' . $asset;
        if (file_exists($filename)) {
            $asset = $asset . '?' . filemtime($filename);
        }
        return $asset;
    }

    public function getName()
    {
        return 'display_asset_file_date_extension';
    }
}
