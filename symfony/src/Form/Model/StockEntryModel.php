<?php

namespace App\Form\Model;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

class StockEntryModel
{
    /**
     * Username
     */
    protected $username;

    /**
     * Stock
     */
    protected $stock;

    /**
     * Email
     */
    protected $email;

    /**
     * Phone
     */
    protected $phone;

    /**
     * Carrier
     */
    protected $carrier;

    /**
     * Billings Center
     */
    protected $billingCenter;

    /**
     * Delivery Date
     */
    protected $deliveryDate;

    /**
     * Time Slots
     */
    protected $timeSlots;

    /**
     * @var Collection|StockEntryItemModel[]
     */
    protected $stockEntryItems;

    public function __construct()
    {
        $this->stockEntryItems = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUserName($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock($stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * @param mixed $carrier
     */
    public function setCarrier($carrier): void
    {
        $this->carrier = $carrier;
    }

    /**
     * @return mixed
     */
    public function getBillingCenter()
    {
        return $this->billingCenter;
    }

    /**
     * @param mixed $billingCenter
     */
    public function setBillingCenter($billingCenter): void
    {
        $this->billingCenter = $billingCenter;
    }

    /**
     * @return \DateTime
     */
    public function getDeliveryDate(): ?\DateTime
    {
        return $this->deliveryDate;
    }

    /**
     * @param \DateTime $deliveryDate
     */
    public function setDeliveryDate(\DateTime $deliveryDate): void
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return mixed
     */
    public function getTimeSlots()
    {
        return $this->timeSlots;
    }

    /**
     * @param mixed $timeSlots
     */
    public function setTimeSlots($timeSlots): void
    {
        $this->timeSlots = $timeSlots;
    }

    /**
     * @param mixed $stockEntryItemModel
     */
    public function addStockEntryItem(StockEntryItemModel $stockEntryItem)
    {
        $this->stockEntryItems->add($stockEntryItem);
    }

    /**
     * @param mixed $stockEntryItemModel
     */
    public function removeStockEntryItem(StockEntryItemModel $stockEntryItem)
    {
        $this->stockEntryItems->removeElement($stockEntryItem);
    }

    /**
     * @return mixed
     */
    public function getStockEntryItems()
    {
        return $this->stockEntryItems;
    }
}
