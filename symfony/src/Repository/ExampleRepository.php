<?php

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class ExampleRepository
 */
class ExampleRepository extends BaseRepository
{



    /**
     * ================= FRONT ===================
     */


    /**
     * ================= BACK ===================
     */


    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et')
            ->leftJoin('e.translations', 'et')
            ->orderBy('et.name', 'asc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'et';
                if ($key == 'id') {
                    $alias = 'e';
                }
                $qb->andWhere($alias.'.'.$key.' LIKE :'.$key);
                $qb->setParameter($key, '%'.$filter.'%');
            }
        }

        $query = $qb->getQuery();

        return $query;
    }

    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et', 'c', 'oc', 'cr', 'pdf')
            ->leftJoin('e.translations', 'et')
            ->leftJoin('et.cover', 'c')
            ->leftJoin('et.crop', 'cr')
            ->leftJoin('et.pdf', 'pdf')
            ->leftJoin('et.ogCover', 'oc')
            ->where('e.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }


    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findAllTranslations($example)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->from('App\Entity\ExampleTranslation', 't')
            ->select('t')
            ->andWhere('t.example = :example')
            ->setParameter('example', $example);

        $query = $qb->getQuery();
        return $query->getResult();
    }
}
