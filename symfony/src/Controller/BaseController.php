<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Extend abstract controller of Symfony to add service inside minimal container
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */
abstract class BaseController extends AbstractController
{
    public static function getSubscribedServices()
    {
        return array_merge(parent::getSubscribedServices(), [
            'knp_paginator' => '?knp_paginator',
            'white_october_breadcrumbs' => '?white_october_breadcrumbs',
            'mailer' => '?mailer',
        ]);
    }
}
