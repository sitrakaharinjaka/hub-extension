<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockEntry115ItemRepository")
 * @ORM\Table(name="dk_item_stock_entry_115")
 * @ORM\HasLifecycleCallbacks
 */
class StockEntry115Item
{
    use GedmoTrait;
    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $product;

    /**
     * Status
     *
     * @ORM\Column(length=255, nullable=false)
     */
    protected $status;

    /**
     * Product State
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $productState;

    /**
     * Status
     *
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 0})
     */
    protected $challenged = false;

    /**
     * Quantity
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $quantity;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * Quantity Received
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $quantityReceived = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StockEntry115", inversedBy="stockEntry115Items")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $stockEntry115;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product): void
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getStockEntry115()
    {
        return $this->stockEntry115;
    }

    /**
     * @param mixed $stockEntry115
     */
    public function setStockEntry115($stockEntry115): void
    {
        $this->stockEntry115 = $stockEntry115;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getQuantityReceived()
    {
        return $this->quantityReceived;
    }

    /**
     * @param mixed $quantityReceived
     */
    public function setQuantityReceived($quantityReceived): void
    {
        $this->quantityReceived = $quantityReceived;
    }

    /**
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function isChallenged()
    {
        return $this->challenged;
    }

    /**
     * @param mixed $challenged
     */
    public function setChallenged($challenged): void
    {
        $this->challenged = $challenged;
    }

    /**
     * Get product State
     */
    public function getProductState()
    {
        return $this->productState;
    }

    /**
     * Set product State
     *
     * @return  self
     */
    public function setProductState($productState)
    {
        $this->productState = $productState;

        return $this;
    }
}
