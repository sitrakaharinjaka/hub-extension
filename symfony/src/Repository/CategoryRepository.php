<?php

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class CategoryRepository
 */
class CategoryRepository extends BaseRepository
{



    /**
     * ================= FRONT ===================
     */


    /**
     * ================= BACK ===================
     */


    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et')
            ->leftJoin('e.translations', 'et')
            ->orderBy('et.name', 'asc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'et';
                if ($key == 'id') {
                    $alias = 'e';
                }
                $qb->andWhere($alias.'.'.$key.' LIKE :'.$key);
                $qb->setParameter($key, '%'.$filter.'%');
            }
        }

        $query = $qb->getQuery();

        return $query;
    }

    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et')
            ->leftJoin('e.translations', 'et')


            ->where('e.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }


    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findAllTranslations($category)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->from('App\Entity\CategoryTranslation', 't')
            ->select('t')
            ->andWhere('t.category = :category')
            ->setParameter('category', $category);

        $query = $qb->getQuery();
        return $query->getResult();
    }


    /**
     * Find all active
     *
     * @param $id
     * @return mixed
     */
    public function findAllActive()
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et')
            ->leftJoin('e.translations', 'et', 'WITH', 'et.locale = :locale')
            ->setParameter('locale', 'fr')
            ->orderBy('et.name', 'ASC');

        $query = $qb->getQuery();

        return $query->getResult();
    }
}
