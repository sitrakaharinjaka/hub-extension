<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Contact;
use App\Form\Model\SearchContact;
use App\Form\Type\Contact\ContactType;
use App\Form\Type\Contact\SearchContactType;
use App\Utils\Managers\ContactManager;
use App\Utils\Managers\LocaleManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContactController
 * @Route("/admin/contacts")
 * @package Disko\AdminBundle\Controller
 */
class ContactController extends BaseController
{
    /**
     * Contact's list
     * @Route("/list/{askLocale}", name="admin_contact_list", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listAction(Request $request, ContactManager $contactManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchContactType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des contacts de centres de coûts');

        // Generate query
        $query = $contactManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/Contact/index.html.twig', array(
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchContact();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setName((isset($filters['name']))   ? $filters['name'] : '');

        return $data;
    }

    /**
     * Create a contact
     * @Route("/create", name="admin_contact_create")
     * @param Request $request Request
     *
     * @return Response
     */
    public function createAction(Request $request, ContactManager $contactManager, LocaleManager $localeManager)
    {
        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");

        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des contacts des centres de facturationss", $this->get("router")->generate("admin_contact_list"));
        $breadcrumbs->addItem("Création");


        // Init form
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact, [
            'validation_groups' => ['Default']
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($contact->getTranslations()) > 0) {
                    $contactManager->save($contact);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Création terminée"
                    );

                    return $this->redirect($this->generateUrl('admin_contact_edit', array(
                        'id' => $contact->getId()
                    )));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            } else {

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'error',
                    "Le formulaire contient des erreurs ou est vide"
                );
            }
        }

        // View
        return $this->render(
            '@Admin/Contact/create.html.twig',
            array(
                'form'           => $form->createView(),
                'contact'           => $contact,
            )
        );
    }

    /**
     * Update a contact
     * @Route("/edit/{id}", name="admin_contact_edit")
     * @param Request $request Request
     * @param integer $id      Id of contact
     *
     * @return Response
     */
    public function editAction(Request $request, ContactManager $contactManager, LocaleManager $localeManager, $id)
    {
        // Find it
        /** @var Contact $contact */
        $contact = $contactManager->findOneToEdit($id);
        if (!$contact) {
            throw $this->createNotFoundException('Contact not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des contacts des centres de facturationss", $this->get("router")->generate("admin_contact_list"));
        $breadcrumbs->addItem("Modification");

        $form = $this->createForm(ContactType::class, $contact, [
            'validation_groups' => ['Default']
        ]);


        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($contact->getTranslations()) > 0) {
                    $contactManager->save($contact);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Mise à jour effectuée"
                    );

                    return $this->redirect($request->headers->get('referer'));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/Contact/edit.html.twig',
            array(
                'form'           => $form->createView(),
                'contact'           => $contact,
            )
        );
    }

    /**
     * Delete one contact
     * @Route("/delete/{id}", name="admin_contact_delete")
     * @param Request $request Request
     * @param integer $id      Id of contact
     */
    public function deleteAction(Request $request, ContactManager $contactManager, $id)
    {
        // Find it
        /** @var Contact $contact */
        $contact = $contactManager->findOneToEdit($id);
        if (!$contact) {
            throw $this->createNotFoundException('Contact not found');
        }

        $contactManager->remove($contact);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été supprimé"
        );

        return $this->redirect($request->headers->get('referer'));
    }



    /**
     * Duplicate one contact
     * @Route("/duplicate/{id}", name="admin_contact_duplicate")
     * @param Request $request Request
     * @param integer $id      Id of contact
     */
    public function duplicateAction(Request $request, ContactManager $contactManager, $id)
    {
        // Find it
        /** @var Contact $contact */
        $contact = $contactManager->findOneToEdit($id);
        if (!$contact) {
            throw $this->createNotFoundException('Contact not found');
        }

        $this->getDoctrine()->getManager()->detach($contact);
        foreach ($contact->getTranslations() as $translation) {
            $translation->setId(null);
            $translation->setSlug(null);
            $this->getDoctrine()->getManager()->detach($translation);
        }
        $contactManager->save($contact);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été dupliqué"
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Export csv
     * @Route("/export/{locale}", name="admin_contact_export")
     * @param Request $request
     */
    public function exportAction(Request $request, ContactManager $contactManager, $locale)
    {
        $data = $this->initSearch($request);
        $query = $contactManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Nom',
            'Date de création',

        ], ';');

        $locale = $request->get('locale', $request->getDefaultLocale());
        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->translate($locale)->getName(),
                ($result->translate($locale)->getCreated()) ? $result->translate($locale)->getCreated()->format('d/m/Y') : $result->getCreated()->format('d/m/Y'),

            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_'.$locale.'_contacts_export.csv"'
        ));
    }
}
