<?php

namespace App\Twig;

use App\Entity\Media;
use Doctrine\ORM\EntityManagerInterface;

class TranslationMediaExtension extends \Twig_Extension
{
    private $mediaRepository;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->mediaRepository = $em->getRepository(Media::class);
    }

    /**
     * Expose filters
     *
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('media_informations', array($this, 'getMediaInformations')),
        );
    }

    /**
     * Inject File system in object Media
     *
     * @param $obj
     * @return mixed
     */
    public function getMediaInformations($id)
    {
        return $this->mediaRepository->find($id);
    }
}
