<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Form\Model\SearchLogFile;
use App\Form\Type\LogFile\SearchLogFileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RedirectGeneratorController
 * @Route("/admin/redirect-generator")
 * @package App\Controller
 */
class RedirectGeneratorController extends BaseController
{
    /**
     * Info about
     * @Route("/", name="admin_redirect_generator")
     * @param Request $request Request
     */
    public function index(Request $request)
    {
        $gen = $request->get('gen', ['start' => null, 'end' => null]);
        $res = [];

        if (!empty($gen) && !empty($gen['start']) && !empty($gen['end'])) {
            $start = str_replace('\l', "", $gen['start']);
            $end = str_replace('\l', "", $gen['end']);
            $start = explode("\r\n", $start);
            $end = explode("\r\n", $end);

            for ($i = 0; $i < count($start); $i++) {
                $exp = explode("/", $start[$i]);
                if ($exp[0] == "http:" or $exp[0] == "https:") {
                    unset($exp[0]);
                    unset($exp[1]);
                    unset($exp[2]);
                    $start[$i] = '/'.implode("/", $exp).' '.$end[$i];
                }
            }


            asort($start);
            $start = array_values($start);


            for ($i = 0; $i < count($start); $i++) {
                if (!isset($start[$i]) or !isset($end[$i])) {
                    continue;
                }
                $res[] = "Redirect 301 ".$start[$i];
            }
        }



        // View
        return $this->render(
            '@Admin/RedirectGenerator/tool.html.twig',
            array(
                'gen' => $gen,
                'res' => implode("\r\n", $res)
            )
        );
    }
}
