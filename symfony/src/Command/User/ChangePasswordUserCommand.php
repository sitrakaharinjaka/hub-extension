<?php

namespace App\Command\User;

use App\Utils\Managers\IPManager;
use App\Utils\Managers\UserManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class ChangePasswordUserCommand extends Command
{
    /** @var  EntityManager */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;

        parent::__construct();
    }

    protected static $defaultName = 'disko:users:change-password';

    protected function configure()
    {
        $this->setDescription('Change password of one user');
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'User email')
            ->addArgument('password', InputArgument::REQUIRED, 'User password')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $user = $this->userManager->findOneBy(['email' => $input->getArgument("email")]);
        if ($user) {
            $user->setPlainPassword($input->getArgument("password"));
            $this->userManager->save($user);
            $output->writeln("<info>Le mot de passe a bien été modifié</info>");
        } else {
            $output->writeln("<error>Utilisateur introuvable</error>");
        }
    }
}
