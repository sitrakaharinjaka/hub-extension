<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Delivery;
use App\Form\Model\SearchDelivery;
use App\Form\Type\Delivery\DeliveryType;
use App\Form\Type\Delivery\SearchDeliveryType;
use App\Utils\Managers\DeliveryManager;
use App\Utils\Managers\LocaleManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DeliveryController
 * @Route("/admin/deliverys")
 * @package Disko\AdminBundle\Controller
 */
class DeliveryController extends BaseController
{
    /**
     * Delivery's list
     * @Route("/list", name="admin_delivery_list")
     * @param Request $request Request
     */
    public function listAction(Request $request, DeliveryManager $deliveryManager)
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchDeliveryType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste');

        // Generate query
        $query = $deliveryManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/Delivery/index.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView()
        ));
    }


    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchDelivery();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setDeliver((isset($filters['deliver']))   ? $filters['deliver'] : '');
        $data->setZip((isset($filters['zip']))   ? $filters['zip'] : '');
        $data->setCountry((isset($filters['country']))   ? $filters['country'] : '');

        return $data;
    }
}
