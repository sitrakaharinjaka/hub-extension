<?php
namespace App\Form\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class NotBlankOnDefaultOnly extends Constraint
{
    const IS_BLANK_ERROR = 'c1051bb4-d103-4f74-8988-acbcafc7fdc4';

    protected static $errorNames = [
        self::IS_BLANK_ERROR => 'IS_BLANK_ERROR',
    ];

    public $message = 'This value should not be blank.';

    public function validatedBy()
    {
        return \get_class($this).'Validator';
    }
}
