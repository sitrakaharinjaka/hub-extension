<?php

namespace App\Command\Veolog;

use App\Command\Veolog\AbstractVeologCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\Product;
use App\Entity\Project;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use App\Utils\Managers\ProductManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\ProgressBar;

class SynchProductCommand extends AbstractVeologCommand
{
    public const MODE_FULL = 'full';
    public const MODE_DELTA = 'delta';

    /** @var  EntityManager */
    private $productManager;
    private $rootDir;

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, ProductManager $productManager, KernelInterface $kernel)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->productManager = $productManager;
        $this->rootDir = $kernel->getProjectDir();
    }

    protected static $defaultName = 'veolog:synch-product';

    protected function configure()
    {
        $this
            ->setDescription('Synch veolog product')
            ->addOption(
                'vlgenv',
                null,
                InputOption::VALUE_OPTIONAL,
                'Env veolog prod?',
                false
            )
            ->addOption(
                'mode',
                null,
                InputOption::VALUE_REQUIRED,
                'Type de Synchonisation : ' . SynchProductCommand::MODE_FULL . ' | ' . SynchProductCommand::MODE_DELTA . '',
                null
            )
            ->setHelp('This command allows you to synch veolog product base');
    }

    protected function executeCommand()
    {
        $mode = $this->input->getOption('mode');
        if ($mode !== SynchProductCommand::MODE_FULL && $mode !== SynchProductCommand::MODE_DELTA) {
            $this->log('info', '<error>Option MODE must be "'.SynchProductCommand::MODE_FULL.'" or "'.SynchProductCommand::MODE_DELTA.'" </error>');
            return;
        }

        $this->log('info', 'Generate file to synchonize product with VEOLOG');

        $products = $this->productManager->getProductNoSynch($mode, Project::CAVE_PANTIN);
        if (count($products) > 0) {
            $this->log('info', ' Products ready for synch : <info>' . count($products) . '</info>');
            $filename = $this->getFilename($mode);
            $localname = $this->getLocalname($filename);
            $this->generateFile($localname, $products);
            $ftpname = $this->getFTPname($filename);
            $this->sendFTP($localname, $ftpname);
            $this->log('info', 'Process ending with <info>success</info>');
        } else {
            $this->log('info', '<comment>No product</comment> find to synch');
        }
    }

    private function getFilename($mode)
    {
        return 'article_' . date("ymdhis") . '_' . $mode . '.csv';
    }

    private function getLocalname($filename)
    {
        return $this->rootDir . "/veolog/" . $this->getDirVlgIN() . "/" . $filename;
    }

    private function getFTPname($filename)
    {
        return "/" . $this->getDirVlgIN() . "/" . $filename;
    }

    private function createLineL($fp, $product)
    {
        $this->log('info', ' #' . $product->getId() . ' - ' . $product->translate('fr')->getName());

        $array_product = [];
        $array_product['type_ligne'] = 'L';
        $array_product['code_article'] = strtoupper($product->translate('fr')->getCode());
        $array_product['libelle_long'] = str_replace(';', '', strtoupper($product->translate('fr')->getDescription()));

        if (empty($product->translate('fr')->getName())) {
            $array_product['libelle_court'] = 'NIL';
        } else {
            $name = \Transliterator::create('NFD; [:Nonspacing Mark:] Remove; NFC')->transliterate($product->translate('fr')->getName());
            $name = substr($name, 0, 27);
            $array_product['libelle_court'] = strtoupper($name);
        }

        $ean = strtoupper($product->translate('fr')->getEanPart());
        $array_product['ean'] = (empty($ean)) ? '' : $ean;
        $array_product['conditionement_unite'] = strtoupper($product->translate('fr')->getUnitConditioning());

        if (!empty($product->translate('fr')->getCategories()[0])) {
            $array_product['famille_produit'] = strtoupper($product->translate('fr')->getCategories()[0]->getName());
        } else {
            $array_product['famille_produit'] = '';
        }

        $array_product['longueur'] = strtoupper($product->translate('fr')->getWidth());
        $array_product['largeur'] = strtoupper($product->translate('fr')->getDepth());
        $array_product['hauteur'] = strtoupper($product->translate('fr')->getHeight());
        $array_product['poids'] = strtoupper($product->translate('fr')->getWeight());
        $array_product['unite_par_carton'] = strtoupper($product->translate('fr')->getTotalUnitsByBox());

        $sku = strtoupper($product->translate('fr')->getCode());
        if (strpos($sku, "HUB") !== false) {
            if (empty($product->translate('fr')->getEanBox())) {
                $ean = strtoupper($product->translate('fr')->getEanPart());
                $array_product['ean_carton'] = (empty($ean)) ? '' : $ean;
            } else {
                $ean = strtoupper($product->translate('fr')->getEanBox());
                $array_product['ean_carton'] = (empty($ean)) ? '' : $ean;
            }
        } else {
            if (empty($product->translate('fr')->getEanBox())) {
                $array_product['ean_carton'] = '';
            } else {
                $ean = strtoupper($product->translate('fr')->getEanBox());
                $array_product['ean_carton'] = (empty($ean)) ? '' : $ean;
            }
        }

        $array_product['longueur_carton'] = strtoupper($product->translate('fr')->getWidthBox());
        $array_product['largeur_carton'] = strtoupper($product->translate('fr')->getDepthBox());
        $array_product['hauteur_carton'] = strtoupper($product->translate('fr')->getHeightBox());
        $array_product['poids_carton'] = strtoupper($product->translate('fr')->getWeightBox());
        //$array_product['informations_suppl'] = strtoupper($product->translate('fr')->getOtherInformation());
        $array_product['informations_suppl'] = '';
        $array_product['produit_consommable'] = strtoupper($product->translate('fr')->isExpendable());
        /**
         * Limit to 10 characters to meet Veolog specs
         * @see https://bugkiller.disko.fr/issues/69973
         */
        //$array_product['nomenclature_douaniere'] = strtoupper($product->translate('fr')->getCustomsNomenclature());
        $array_product['nomenclature_douaniere'] = substr(strtoupper($product->translate('fr')->getCustomsNomenclature()), 0, 10);

        $array_product['crd'] = strtoupper($product->translate('fr')->isCrd());
        $array_product['iso2'] = strtoupper($product->translate('fr')->getCountry());
        $array_product['litrage'] = strtoupper($product->translate('fr')->getLitrageBottle());
        $array_product['effervescent'] = strtoupper($product->translate('fr')->isEffervescent());
        $array_product['region'] = strtoupper($product->translate('fr')->getDesignationBottle());

        $vintage = $product->translate('fr')->getVintage();
        if (empty($vintage) || !is_integer($vintage)) {
            $array_product['millesime'] = 0;
        } else {
            $array_product['millesime'] = strtoupper($product->translate('fr')->getVintage());
        }

        $array_product['couleur_du_vin'] = strtoupper($product->translate('fr')->getWineColor());
        $array_product['categorie'] = strtoupper($product->translate('fr')->getWineCategory());

        if (!empty($product->translate('fr')->getDegreeAlcohol())) {
            $array_product['pourcentage_alcool'] = floatval($product->translate('fr')->getDegreeAlcohol()) * 1000;
        } else {
            $array_product['pourcentage_alcool'] = '';
        }

        fputcsv($fp, $array_product, ';');
        $this->log('info', ' - Writing into file');

        $today = new \DateTime();
        $today->modify("+1 minutes");
        $product->setDateSynch($today);
        $this->productManager->save($product);
        $this->log('info', ' - Updating product');
    }

    private function generateFile($localname, $products)
    {
        $this->log('info', ' --> Calculating file : ' . $localname);
        $fp = fopen($localname, "w");
        foreach ($products as $key => $product) {
            $this->createLineL($fp, $product);
        }
        fputcsv($fp, array('F', count($products)), ';');
        $this->log('info', ' ==> Calcultaling file ' . $localname .' <info>success</info>');
    }

    private function sendFTP($localname, $ftpname)
    {
        $this->log('info', ' --> Uploading file : ' . $localname . ' to VEOLOG');
        try {
            $ftp_handle = ftp_connect('ftp.veolog.fr');
            if (false === $ftp_handle) {
                throw new \Exception('Unable to connect');
            }

            $loggedIn = ftp_login($ftp_handle, 'vlgcli18', '1L8vJv2U75');
            ftp_pasv($ftp_handle, true);
            if (true === $loggedIn) {
                $this->log('info', '     --> Connexion <info>success</info>');
                if (ftp_put($ftp_handle, $ftpname, $localname, FTP_ASCII)) {
                    $this->log('info', '     --> Upload <info>success</info>');
                } else {
                    throw new \Exception('Unable to upload');
                }
            } else {
                throw new \Exception('Unable to log in');
            }
            ftp_close($ftp_handle);
        } catch (Exception $e) {
            echo "<error>Failure</error>: " . $e->getMessage();
        }
    }
}
