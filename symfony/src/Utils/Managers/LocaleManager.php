<?php

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;

use App\Entity\Locale;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class LocaleManager
 * `
 * Object manager of locale
 *
 * @package App\Utils\Managers
 */
class LocaleManager extends BaseManager
{
    /**
     * @var LocaleRepository
     */
    protected $localeRepository;


    /**
     * ExampleManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem)
    {
        parent::__construct($em, $fileSystem, null);
        $this->addRepository('localeRepository', Locale::class);
    }

    /**
     * Save a locale
     *
     * @param Locale $locale
     */
    public function save(Locale $locale)
    {
        if ($locale->getDefault()) {
            $lastLocaleDefault = $this->findOneBy(['default' => true]);
            if ($lastLocaleDefault) {
                $lastLocaleDefault->setDefault(false);
                $this->save($lastLocaleDefault);
            }
        }

        $locale->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($locale);
        $this->getEntityManager()->flush();

        return $locale;
    }

    /**
     * Remove one locale
     *
     * @param Locale $locale
     */
    public function remove(Locale $locale)
    {
        $this->getEntityManager()->remove($locale);
        $this->getEntityManager()->flush();
    }

    /**
     * Get all locale
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function getQueryForSearch($filters = array())
    {
        return $this->localeRepository->queryForSearch($filters);
    }

    /**
     * Find locale by slug for edit profil
     *
     * @param string $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        return $this->localeRepository->findOneToEdit($id);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAllList()
    {
        return $this->localeRepository->findAllList();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->localeRepository->findOneBy($filters);
    }

    /**
     * Find all form
     *
     * @return mixed
     */
    public function findAllForm()
    {
        $objs = $this->localeRepository->findAllList();

        $results = array();
        foreach ($objs as $obj) {
            $locale = $obj->getCode();
            if (strtolower($locale) == 'jp') {
                $locale = 'jpn';
            }
            if (strtolower($locale) == 'cn') {
                $locale = 'zh';
            }

            $results[locale_get_display_name(strtolower($locale), strtolower('en'))] = $obj->getCode();
        }

        return $results;
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->localeRepository->findAll();
    }


    /**
     * Find all
     *
     * @return mixed
     */
    public function findAllCodes()
    {
        $locales = $this->findAll();
        $i = 0;
        foreach ($locales as $locale) {
            $langs[$i] = strtolower($locale->getCode());
            $i++;
        }

        return $langs;
    }


    /**
     * Find all
     *
     * @return mixed
     */
    public function findAllCodesActive()
    {
        $locales = $this->localeRepository->findBy(['active' => true]);
        $i = 0;
        foreach ($locales as $locale) {
            $langs[$i] = strtolower($locale->getCode());
            $i++;
        }

        return $langs;
    }

    /**
     * findAllSlug
     *
     * @return mixed
     */
    public function findAllSlug()
    {
        return $this->localeRepository->findAllSlug();
    }

    /**
     * findDefaultSlug
     *
     * @return mixed
     */
    public function findDefaultSlug()
    {
        return $this->localeRepository->findDefaultSlug();
    }
}
