<?php
/**
 * Created by PhpStorm.
 * User: Luk
 * Date: 20/05/2017
 * Time: 20:35
 */

namespace App\Form\Type\MyBoxes;

use App\Entity\MyBoxesBox;
use App\Entity\MyBoxesProduct;
use App\Entity\MyBoxesSelectBox;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SelectBoxType extends AbstractType
{
    protected $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('box', EntityType::class, [
            'class' => MyBoxesBox::class,
            'query_builder' => function (EntityRepository $er) use ($options) {
                if ($options['type'] == 'bring-home') {
                    return $er->queryBuilderBringable($options['user']);
                } elseif ($options['type'] == 'back-hub') {
                    return $er->queryBuilderBackable($options['user']);
                } else {
                    return null;
                }
            },
            'choice_label' => function ($box) {
                return $box->getName() . ' (#' . $box->getId() . ')';
            },
            'by_reference' => false,
            'required'  => true
        ]);
//        $builder->add('type', HiddenType::class, array('required' => true));
    }

    /**
     * Configure Options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'user' => null,
            'type' => null,
            'data_class' => MyBoxesSelectBox::class,
            'validation_groups' => 'Default',
        ));
    }
}
