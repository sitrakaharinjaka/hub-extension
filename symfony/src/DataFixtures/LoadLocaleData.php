<?php

/**
 * Fixture
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\DataFixtures;

use App\Utils\Managers\LocaleManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\Locale;

/**
 * Class LoadLocaleData
 *
 * @package Disko\UserBundle\DataFixtures\ORM
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class LoadLocaleData extends Fixture
{
    private $localeManager;

    public function __construct(LocaleManager $localeManager)
    {
        $this->localeManager = $localeManager;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $locales = array('fr');
        foreach ($locales as $locale) {
            $obj = new Locale();
            $obj->setCode($locale);
            $obj->setActive(true);

            if ($locale == "fr") {
                $obj->setDefault(true);
            }

            $this->localeManager->save($obj);

            $this->addReference('admin-locale-'.$obj->getCode(), $obj);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
