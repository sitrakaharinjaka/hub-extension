<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Utils\Managers\LocaleManager;
use App\Utils\Managers\MediaManager;
use App\Utils\Managers\NewsletterManager;
use App\Utils\Managers\UserManager;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 *
 * @Route("/admin")
 * @package App\Controller
 */
class HomeController extends BaseController
{

    /**
     * Homepage Admin
     * @Route("/", name="admin_homepage")
     * @return Response
     */
    public function index(LocaleManager $localeManager, UserManager $userManager, MediaManager $mediaManager)
    {
        if ($this->isGranted('ROLE_RUNNER') && !$this->isGranted('ROLE_SUPER_ADMIN') && !$this->isGranted('ROLE_GENERAL_ADMIN')) {
            return $this->redirect($this->generateUrl('runner_homepage'));
        }

        // Breadcrumb
        $breadcrumbs = $this->container->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));

        $countUsersAdmin = $userManager->countAll('admin');
        $countUsersMember = $userManager->countAll('member');

        

        $countMedias = $mediaManager->countAll();

        return $this->render('@Admin/Home/index.html.twig', [
            'locales' => $localeManager->findAll(),
            'countUsersAdmin' => $countUsersAdmin,
            'countUsersMember' => $countUsersMember,
            
            'countMedias' => $countMedias,
        ]);
    }
}
