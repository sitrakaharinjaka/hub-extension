<?php

namespace App\Command\Import;

use App\Entity\MyBoxesProduct;
use App\Utils\Managers\MyBoxesProductManager;
use App\Utils\Managers\ProductImportManager;
use App\Utils\Services\CsvToArray;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ImportMyBoxesProductCommand extends Command
{


    /**
     * @var string
     */
    protected static $defaultName = 'disko:import:myBoxesProduct';


    /**
     * @var MyBoxesProductManager
     */
    protected $myBoxesProductManager;

    /**
     * ImportProductCommand constructor.
     *
     * @param CsvToArray $csvToArray
     * @param ProductImportManager $productImportManager
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(MyBoxesProductManager $myBoxesProductManager)
    {
        $this->myBoxesProductManager = $myBoxesProductManager;

        parent::__construct();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mybp = $this->myBoxesProductManager->findOneBy(["sku" => "BOX_40L_001"]);
        if (!$mybp) {
            $mybp = new MyBoxesProduct();
        }
        $mybp->setName("Box Jaune 40L");
        $mybp->setSku("BOX_40L_001");
        $mybp->setDescription(null);
        $mybp->setStock(0);
        $this->myBoxesProductManager->save($mybp);

        $mybp = $this->myBoxesProductManager->findOneBy(["sku" => "BOX_55L_001"]);
        if (!$mybp) {
            $mybp = new MyBoxesProduct();
        }
        $mybp->setName("Box Rouge 55L");
        $mybp->setSku("BOX_55L_001");
        $mybp->setDescription(null);
        $mybp->setStock(0);
        $this->myBoxesProductManager->save($mybp);


        $mybp = $this->myBoxesProductManager->findOneBy(["sku" => "BOX_72L_001"]);
        if (!$mybp) {
            $mybp = new MyBoxesProduct();
        }
        $mybp->setName("Box Bleue 72L");
        $mybp->setSku("BOX_72L_001");
        $mybp->setDescription(null);
        $mybp->setStock(0);
        $this->myBoxesProductManager->save($mybp);
    }
}
