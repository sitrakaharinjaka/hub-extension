<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Front;

use App\Controller\BaseController;
use App\Entity\Project;
use App\Utils\Managers\ProjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RightController
 *
 * @Route("/droits")
 * @package App\Controller
 */
class RightController extends BaseController
{

    /**
     * Homepage
     * @Route("/demande", name="right")
     * @return Response
     */
    public function index(Request $request)
    {
        if ($this->get('session')->get('current_stock', false)) {
            return $this->redirectToRoute("homepage");
        }

        return $this->render('hub/right/index.html.twig', [
        ]);
    }
}
