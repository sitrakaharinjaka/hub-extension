<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockEntry115Repository")
 * @ORM\Table(name="dk_stock_entry_115")
 * @ORM\HasLifecycleCallbacks
 */
class StockEntry115
{
    use GedmoTrait;
    use AuthorableTrait;

    public const STATE_REQUEST_TAKEN = 'request_taken';
    public const STATE_IN_STOCK      = 'in_stock';

    public const STATE_ITEM_CREATE   = 'created';   // L'utilisateur à créé la demande pour le produit.
    public const STATE_ITEM_UPDATE   = 'updated';   // VEOLOG à reçu des produits.
    public const STATE_ITEM_VALIDATE = 'validated'; // L'utilisateur a validé la quantité reçue.
    public const STATE_ITEM_REJECT   = 'rejected';  // L'utilisateur a rejeté la quantité reçue.

    public const CAVE_115 = '115';
    public const CAVE_PANTIN = 'pantin';

    public const URL_STATE_ITEM_VALIDATE = 'validation'; // Status passé dans l'URL de confirmation "VALIDE"
    public const URL_STATE_ITEM_REJECT   = 'rejet'; // Status passé dans l'URL de confirmation "REJET"

  /**
   * Id
   *
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
    protected $id;

    /**
     * Username
     *
     * @ORM\Column(length=255, nullable=false)
     */
    protected $username;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", cascade={"persist"})
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $stock;

    /**
     * Email
     *
     * @ORM\Column(length=255, nullable=false)
     */
    protected $email;

    /**
     * Phone
     *
     * @ORM\Column(length=255, nullable=false)
     */
    protected $phone;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BillingCenter", cascade={"persist"})
     * @ORM\JoinColumn(name="billing_center_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $billingCenter;

    /**
     * PickupDate
     *
     * @var DateTime $pickupDate
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $pickUpDate;

    /**
     * Time Slots
     *
     * @ORM\Column(length=255, nullable=false)
     */
    protected $timeSlots;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $user;

    /**
     * @var Collection|StockEntry115Item[]
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\StockEntry115Item",
     *     mappedBy="stockEntry115",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     */
    protected $stockEntry115Items;

    /**
     * @var \DateTimeInterface|null $dateSendToVeolog
     * @ORM\Column(name="date_send_to_veolog", type="date", nullable=true)
     */
    public $dateSendToVeolog;

    /**
     * Status
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $officeNumber;

    /**
     * Status
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $floor;

    /**
     * Status
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $pickUpLocation;

    /**
     * @var User|null
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="preparations")
     * @ORM\JoinColumn(name="runner_id", referencedColumnName="id")
     */
    protected $runner;


    /**
     * Status
     *
     * @ORM\Column(length=255, nullable=false)
     */
    protected $status;


    /**
     * Cave
     *
     * @var string
     * @ORM\Column(type="string", options={"default" : "pantin"})
     */
    protected $cave;

    public function __construct()
    {
        $this->stockEntry115Items = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUserName($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock($stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getBillingCenter()
    {
        return $this->billingCenter;
    }

    /**
     * @param mixed $billingCenter
     */
    public function setBillingCenter($billingCenter): void
    {
        $this->billingCenter = $billingCenter;
    }

    /**
     * @return DateTime
     */
    public function getPickUpDate(): ?DateTime
    {
        return $this->pickUpDate;
    }

    /**
     * @param DateTime $pickUpDate
     */
    public function setPickUpDate(DateTime $pickUpDate): void
    {
        $this->pickUpDate = $pickUpDate;
    }

    /**
     * @return mixed
     */
    public function getTimeSlots()
    {
        return $this->timeSlots;
    }

    /**
     * @param mixed $timeSlots
     */
    public function setTimeSlots($timeSlots): void
    {
        $this->timeSlots = $timeSlots;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getDateSendToVeolog()
    {
        return $this->dateSendToVeolog;
    }

    /**
     * @param mixed $dateSendToVeolog
     */
    public function setDateSendToVeolog($dateSendToVeolog): void
    {
        $this->dateSendToVeolog = $dateSendToVeolog;
    }

    public function addStockEntry115Item(StockEntry115Item $stockEntry115Item)
    {
        $stockEntry115Item->setStockEntry115($this);

        $this->stockEntry115Items->add($stockEntry115Item);
    }

    public function removeStockEntry115Item(StockEntry115Item $stockEntry115Item)
    {
        if ($this->stockEntry115Items->removeElement($stockEntry115Item)) {
            // set the owning side to null (unless already changed)
            if ($stockEntry115Item->getStockEntry115() === $this) {
                $stockEntry115Item->setStockEntry115(null);
            }
        }
    }

    public function getStockEntry115Items()
    {
        return $this->stockEntry115Items;
    }

    public function getStockEntry115ItemByCode($code)
    {
        foreach ($this->stockEntry115Items as $item) {
            if ($item->getProduct() && $item->getProduct()->translate('fr')->getCode() == $code) {
                return $item;
            }
        }
        return null;
    }

    public function awaitingValidation()
    {
        $awaitingValidation = true;
        if ($this->getStatus() != self::STATE_ITEM_UPDATE) {
                $awaitingValidation = false;
        }
        return $awaitingValidation;
    }

    /**
     * @return User|null
     */
    public function getRunner()
    {
        return $this->runner;
    }

    /**
     * @param  User|null  $runner
     */
    public function setRunner(?User $runner)
    {
        $this->runner = $runner;
    }

    public function getStockEntry115ItemById($id)
    {
        foreach ($this->stockEntry115Items as $item) {
            if ($item->getId() == $id) {
                return $item;
            }
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getOfficeNumber()
    {
        return $this->officeNumber;
    }

    /**
     * @param mixed $officeNumber
     */
    public function setOfficeNumber($officeNumber): void
    {
        $this->officeNumber = $officeNumber;
    }

    /**
     * @return mixed
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * @param mixed $floor
     */
    public function setFloor($floor): void
    {
        $this->floor = $floor;
    }

    /**
     * @return string
     */
    public function getCave(): string
    {
        return $this->cave;
    }

    /**
     * @param string $cave
     */
    public function setCave(string $cave): void
    {
        $this->cave = $cave;
    }

    /**
     * @return mixed
     */
    public function getPickUpLocation()
    {
        return $this->pickUpLocation;
    }

    /**
     * @param mixed $pickUpLocation
     */
    public function setPickUpLocation($pickUpLocation): void
    {
        $this->pickUpLocation = $pickUpLocation;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }
}
