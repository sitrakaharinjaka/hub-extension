<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IPRepository")
 * @ORM\Table(name="dk_ip")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class IP
{
    use GedmoTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * name
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * Description
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * v4
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $v4;

    /**
     * v6
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $v6;

    /**
     * Edit
     *
     * @var datetime $publishDate
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $publishDate;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Example
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set description
     *
     * @param string $description
     * @return Example
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Example
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return datetime
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * @param datetime $publishDate
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;
    }

    /**
     * Get v4
     */
    public function getV4()
    {
        return $this->v4;
    }

    /**
     * Set v4
     *
     * @return  self
     */
    public function setV4($v4)
    {
        $this->v4 = $v4;

        return $this;
    }

    /**
     * Get v6
     */
    public function getV6()
    {
        return $this->v6;
    }

    /**
     * Set v6
     *
     * @return  self
     */
    public function setV6($v6)
    {
        $this->v6 = $v6;

        return $this;
    }

    /**
     * Get rangeFrom
     */
    public function getRangeFrom()
    {
        return $this->rangeFrom;
    }

    /**
     * Set rangeFrom
     *
     * @return  self
     */
    public function setRangeFrom($rangeFrom)
    {
        $this->rangeFrom = $rangeFrom;

        return $this;
    }

    /**
     * Get rangeTo
     */
    public function getRangeTo()
    {
        return $this->rangeTo;
    }

    /**
     * Set rangeTo
     *
     * @return  self
     */
    public function setRangeTo($rangeTo)
    {
        $this->rangeTo = $rangeTo;

        return $this;
    }
}
