<?php

namespace App\Form\Model;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

class StockEntry115Model
{
    /**
     * Username
     */
    protected $username;

    /**
     * Stock
     */
    protected $stock;

    /**
     * Email
     */
    protected $email;

    /**
     * Phone
     */
    protected $phone;

    /**
     * Billings Center
     */
    protected $billingCenter;

    /**
     * Delivery Date
     */
    protected $pickUpDate;

    /**
     * Time Slots
     */
    protected $timeSlots;

    /**
     * Floor
     * @Assert\NotBlank(groups={"pickup_142"})
     */
    protected $floor;

    /**
     * Office Number
     * @Assert\NotBlank(groups={"pickup_142"})
     */
    protected $officeNumber;

    /**
     * Office Number
     */
    protected $pickUpLocation;

    /**
     * @var Collection|StockEntry115ItemModel[]
     */
    protected $stockEntry115Items;

    public function __construct()
    {
        $this->stockEntry115Items = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUserName($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock($stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getBillingCenter()
    {
        return $this->billingCenter;
    }

    /**
     * @param mixed $billingCenter
     */
    public function setBillingCenter($billingCenter): void
    {
        $this->billingCenter = $billingCenter;
    }

    /**
     * @return \DateTime
     */
    public function getPickUpDate(): ?\DateTime
    {
        return $this->pickUpDate;
    }

    /**
     * @param \DateTime $pickUpDate
     */
    public function setPickUpDate(\DateTime $pickUpDate): void
    {
        $this->pickUpDate = $pickUpDate;
    }

    /**
     * @return mixed
     */
    public function getTimeSlots()
    {
        return $this->timeSlots;
    }

    /**
     * @param mixed $timeSlots
     */
    public function setTimeSlots($timeSlots): void
    {
        $this->timeSlots = $timeSlots;
    }

    /**
     * @param StockEntry115ItemModel $stockEntry115Item
     */
    public function addStockEntry115Item(StockEntry115ItemModel $stockEntry115Item)
    {
        $this->stockEntry115Items->add($stockEntry115Item);
    }

    /**
     * @param StockEntry115ItemModel $stockEntry115Item
     */
    public function removeStockEntry115Item(StockEntry115ItemModel $stockEntry115Item)
    {
        $this->stockEntry115Items->removeElement($stockEntry115Item);
    }

    /**
     * @return mixed
     */
    public function getStockEntry115Items()
    {
        return $this->stockEntry115Items;
    }

    /**
     * @return mixed
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * @param mixed $floor
     */
    public function setFloor($floor): void
    {
        $this->floor = $floor;
    }

    /**
     * @return mixed
     */
    public function getOfficeNumber()
    {
        return $this->officeNumber;
    }

    /**
     * @param mixed $officeNumber
     */
    public function setOfficeNumber($officeNumber): void
    {
        $this->officeNumber = $officeNumber;
    }

    /**
     * @return mixed
     */
    public function getPickUpLocation()
    {
        return $this->pickUpLocation;
    }

    /**
     * @param mixed $pickUpLocation
     */
    public function setPickUpLocation($pickUpLocation): void
    {
        $this->pickUpLocation = $pickUpLocation;
    }
}
