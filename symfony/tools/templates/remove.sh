#!/bin/bash

ORANGE='\033[0;33m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

DIRECTORY=$(cd `dirname $0` && pwd)

echo " "
echo ${GREEN}Remove twig${NC}
for filename in ${DIRECTORY}/../../front/src/*.twig; do
    if [ -f "$filename" ]; then
        echo "    ${RED}REMOVE${NC} $filename"
        rm $filename
    fi
done
for filename in ${DIRECTORY}/../../front/src/**/*.twig; do
    if [ -f "$filename" ]; then
        echo "    ${RED}REMOVE${NC} $filename"
        rm $filename
    fi
done


echo " "