<?php

/**
 * Model for search
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Model;

/**
 * Class SearchUser
 *
 * @package App\Model
 */
class SearchLogFile
{
    /**
     * Email
     *
     * @var string
     */
    private $name;

    private $nbLine;

    /**
     * SearchLogFile constructor.
     */
    public function __construct()
    {
        $this->nbLine = 20;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getNbLine()
    {
        return $this->nbLine;
    }

    /**
     * @param int $nbLine
     */
    public function setNbLine($nbLine)
    {
        $this->nbLine = $nbLine;
    }
}
