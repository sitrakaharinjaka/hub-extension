<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin\MyBoxes;

use App\Entity\MyBoxesCommand;
use App\Form\Model\SearchMyBoxesCommand;
use App\Form\Type\MyBoxesCommand\SearchMyBoxesCommandType;
use App\Utils\Managers\MyBoxesCommandManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MyBoxesCommandController
 * @Route("/admin/myBoxesCommands")
 * @package Disko\AdminBundle\Controller
 */
class MyBoxesCommandController extends BaseController
{
    /**
     * MyBoxesCommand's list
     * @Route("/list/{askLocale}", name="admin_myBoxesCommand_list", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listAction(Request $request, MyBoxesCommandManager $myBoxesCommandManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchMyBoxesCommandType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des commandes');

        // Generate query
        $query = $myBoxesCommandManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/MyBoxesCommand/index.html.twig', array(
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchMyBoxesCommand();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setFirstname((isset($filters['firstname']))   ? $filters['firstname'] : '');
        $data->setLastname((isset($filters['lastname']))   ? $filters['lastname'] : '');
        $data->setOfficeNumber((isset($filters['officeNumber']))   ? $filters['officeNumber'] : '');
        $data->setStage((isset($filters['stage']))   ? $filters['stage'] : '');
        $data->setState((isset($filters['state']))   ? $filters['state'] : '');

        return $data;
    }


    /**
     * Delete one myBoxesCommand
     * @Route("/delete/{id}", name="admin_myBoxesCommand_delete")
     * @param Request $request Request
     * @param integer $id      Id of myBoxesCommand
     */
    public function deleteAction(Request $request, MyBoxesCommandManager $myBoxesCommandManager, $id)
    {
        // Find it
        /** @var MyBoxesCommand $myBoxesCommand */
        $myBoxesCommand = $myBoxesCommandManager->findOneToEdit($id);
        if (!$myBoxesCommand) {
            throw $this->createNotFoundException('MyBoxesCommand not found');
        }

        $myBoxesCommandManager->remove($myBoxesCommand);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été supprimé"
        );

        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * Export csv
     * @Route("/export/{locale}", name="admin_myBoxesCommand_export")
     * @param Request $request
     */
    public function exportAction(Request $request, MyBoxesCommandManager $myBoxesCommandManager, $locale)
    {
        $data = $this->initSearch($request);
        $query = $myBoxesCommandManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Prénom',
            'Nom',
            'Etage',
            'Num de bureau',
            'Etat',
            'Date de création',
            
        ], ';');

        $locale = $request->get('locale', $request->getDefaultLocale());
        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->getFirstname(),
                $result->getLastname(),
                $result->getStage(),
                $result->getOfficeNumber(),
                $result->getState(),
                ($result->getCreated()) ? $result->getCreated()->format('d/m/Y') : '',
                
            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_'.$locale.'_commands_export.csv"'
        ));
    }
}
