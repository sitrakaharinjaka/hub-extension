<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\Model\SearchUser;
use App\Form\Type\User\SearchUserType;
use App\Form\Type\User\UserRestrictCreateType;
use App\Form\Type\User\UserType;
use App\Utils\Managers\StockManager;
use App\Utils\Managers\UserManager;
use App\Controller\BaseController;
use App\Repository\StockRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class ExecCommandController
 *
 * @Route("/admin/command")
 * @package App\Controller
 */
class ExecCommandController extends BaseController
{
    /**
     * @Route("/exec/{id}", name="admin_exec_command")
     * @param Request $request Request
     */
    public function execAction($id, KernelInterface $kernel)
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => $id,
        ]);

        $output = new BufferedOutput();
        $application->run($input, $output);

        if ($id === "disko:project:return") {
            $this->get('session')->getFlashBag()->set(
                'notice',
                "Email(s) \"Reminder Retour\" envoyé(s)."
            );
        } else {
            $this->get('session')->getFlashBag()->set(
                'notice',
                "Commande $id executée."
            );
        }

        return $this->redirectToRoute('admin_homepage');
    }
}
