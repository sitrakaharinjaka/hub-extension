<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Front;

use App\Entity\Stock;
use App\Entity\StockTranslation;
use App\Form\Model\SearchStock;
use App\Form\Type\Stock\StockTranslationType;
use App\Form\Type\Stock\SearchStockType;
use App\Form\Type\Stock\StockType;
use App\Utils\Managers\StockManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StockController
 * @Route("/stocks")
 * @package Disko\AdminBundle\Controller
 */
class StockController extends BaseController
{

    /**
     * Choice stock
     *
     * @param Request $request
     */
    public function choice(Request $request, StockManager $stockManager)
    {
        // Get Stocks
        $stocks = $stockManager->findByUser($this->getUser());

        $template = "choice";
        $type = $request->get('type', '');
        if (!empty($type)) {
            $template = 'choice-'.$type;
        }

        // View
        return $this->render(
            'hub/stock/partials/'.$template.'.html.twig',
            array(
                'stocks'           => $stocks,
            )
        );
    }


    /**
     * Change stock
     *
     * @param Request $request
     * @Route("/changer", name="change_stock")
     */
    public function change(Request $request, StockManager $stockManager)
    {
        $id = $request->get('id', null);
        if (!$id) {
            throw $this->createNotFoundException("Id missing");
        }

        // Get Stocks
        $stocks = $stockManager->findByUser($this->getUser());

        $find = false;
        $stockFind = null;
        foreach ($stocks as $stock) {
            if ($id == $stock->getId()) {
                $find = true;
                $stockFind = $stock;
            }
        }

        if ($find) {
            $this->get('session')->set('current_stock', $stockFind);
            $this->get('session')->set('current_project', null);
        } else {
            throw $this->createAccessDeniedException("This stock is not accessbile for you");
        }

        // View
        return $this->redirect($request->headers->get('referer', $this->generateUrl('homepage')));
    }
}
