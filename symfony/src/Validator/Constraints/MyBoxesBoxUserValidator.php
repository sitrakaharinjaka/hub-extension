<?php

namespace App\Validator\Constraints;

use App\Utils\Managers\MyBoxesBoxUserManager;
use App\Utils\Managers\UserManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MyBoxesBoxUserValidator extends ConstraintValidator
{
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($boxUser, Constraint $constraint)
    {
        if ($boxUser->getUser() == null) {
            return;
        }
        $email = $boxUser->getUser()->getEmail();
        $user = $this->userManager->findOneBy(['email' => $email]);

        if (!$user) {
            $this
                ->context
                ->buildViolation($constraint->message)
                ->atPath('user.email')
                ->addViolation()
                ;
        }
    }
}
