<?php

namespace App\Entity\Traits;

use App\Interfaces\WorkflowInterface;

trait WorkflowTrait
{
    /**
     * State
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $state = WorkflowInterface::WORKFLOW_DRAFT;

    protected $stateAction = WorkflowInterface::WORKFLOW_DRAFT;

    /**
     * Ref
     *
     * Reference using for leftjoin to know good active version
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $ref = null;

    /**
     * Ref base
     *
     * Reference base
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $refBase = null;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $master = false;

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param mixed $ref
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    /**
     * @return mixed
     */
    public function getRefBase()
    {
        return $this->refBase;
    }

    /**
     * @param mixed $refBase
     */
    public function setRefBase($refBase)
    {
        $this->refBase = $refBase;
    }

    /**
     * @return string
     */
    public function getStateAction()
    {
        return $this->stateAction;
    }

    /**
     * @param string $stateAction
     */
    public function setStateAction($stateAction)
    {
        $this->stateAction = $stateAction;
    }

    /**
     * @return mixed
     */
    public function getMaster()
    {
        return $this->master;
    }

    /**
     * @param mixed $master
     */
    public function setMaster($master)
    {
        $this->master = $master;
    }

    /**
     * @return string
     */
    public function getVersionNumber()
    {
        return "#".substr($this->getRefBase(), 0, 2)."-".$this->getId();
    }
}
