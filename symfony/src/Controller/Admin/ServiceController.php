<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\Service;
use App\Entity\ServiceTranslation;
use App\Form\Model\SearchService;
use App\Form\Type\Service\ServiceTranslationType;
use App\Form\Type\Service\SearchServiceType;
use App\Form\Type\Service\ServiceType;
use App\Utils\Managers\ServiceManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ServiceController
 * @Route("/admin/services")
 * @package Disko\AdminBundle\Controller
 */
class ServiceController extends BaseController
{
    /**
     * Service's list
     * @Route("/list/{askLocale}", name="admin_service_list", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listAction(Request $request, ServiceManager $serviceManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchServiceType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des services');

        // Generate query
        $query = $serviceManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/Service/index.html.twig', array(
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchService();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setName((isset($filters['name']))   ? $filters['name'] : '');

        return $data;
    }

    /**
     * Create a service
     * @Route("/create", name="admin_service_create")
     * @param Request $request Request
     *
     * @return Response
     */
    public function createAction(Request $request, ServiceManager $serviceManager, LocaleManager $localeManager)
    {
        if ($this->isGranted('ROLE_VEOLOG_ADMIN')) {
            throw $this->createAccessDeniedException("Veolog not access");
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");

        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des services", $this->get("router")->generate("admin_service_list"));
        $breadcrumbs->addItem("Création");


        // Init form
        $service = new Service();
        $form = $this->createForm(ServiceType::class, $service, [
            'validation_groups' => ['Default']
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($service->getTranslations()) > 0) {
                    $serviceManager->save($service);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Création terminée"
                    );

                    return $this->redirect($this->generateUrl('admin_service_edit', array(
                        'id' => $service->getId()
                    )));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            } else {

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'error',
                    "Le formulaire contient des erreurs ou est vide"
                );
            }
        }

        // View
        return $this->render(
            '@Admin/Service/create.html.twig',
            array(
                'form'           => $form->createView(),
                'service'           => $service,
            )
        );
    }

    /**
     * Update a service
     * @Route("/edit/{id}", name="admin_service_edit")
     * @param Request $request Request
     * @param integer $id      Id of service
     *
     * @return Response
     */
    public function editAction(Request $request, ServiceManager $serviceManager, LocaleManager $localeManager, $id)
    {
        // Find it
        /** @var Service $service */
        $service = $serviceManager->findOneToEdit($id);
        if (!$service) {
            throw $this->createNotFoundException('Service not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des services", $this->get("router")->generate("admin_service_list"));
        $breadcrumbs->addItem("Modification");

        $form = $this->createForm(ServiceType::class, $service, [
            'validation_groups' => ['Default']
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($service->getTranslations()) > 0) {
                    $serviceManager->save($service);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Mise à jour effectuée"
                    );

                    return $this->redirect($request->headers->get('referer'));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/Service/edit.html.twig',
            array(
                'form'           => $form->createView(),
                'service'           => $service,
            )
        );
    }

    /**
     * Delete one service
     * @Route("/delete/{id}", name="admin_service_delete")
     * @param Request $request Request
     * @param integer $id      Id of service
     */
    public function deleteAction(Request $request, ServiceManager $serviceManager, $id)
    {
        if ($this->isGranted('ROLE_VEOLOG_ADMIN')) {
            throw $this->createAccessDeniedException("Veolog not access");
        }

        // Find it
        /** @var Service $service */
        $service = $serviceManager->findOneToEdit($id);
        if (!$service) {
            throw $this->createNotFoundException('Service not found');
        }

        $serviceManager->remove($service);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été supprimé"
        );

        return $this->redirect($request->headers->get('referer'));
    }



    /**
     * Duplicate one service
     * @Route("/duplicate/{id}", name="admin_service_duplicate")
     * @param Request $request Request
     * @param integer $id      Id of service
     */
    public function duplicateAction(Request $request, ServiceManager $serviceManager, $id)
    {
        // Find it
        /** @var Service $service */
        $service = $serviceManager->findOneToEdit($id);
        if (!$service) {
            throw $this->createNotFoundException('Service not found');
        }

        $this->getDoctrine()->getManager()->detach($service);
        foreach ($service->getTranslations() as $translation) {
            $translation->setId(null);
            $translation->setSlug(null);
            $this->getDoctrine()->getManager()->detach($translation);
        }
        $serviceManager->save($service);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été dupliqué"
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Export csv
     * @Route("/export/{locale}", name="admin_service_export")
     * @param Request $request
     */
    public function exportAction(Request $request, ServiceManager $serviceManager, $locale)
    {
        $data = $this->initSearch($request);
        $query = $serviceManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Nom',
            'Date de création',
            'En ligne'
        ], ';');

        $locale = $request->get('locale', $request->getDefaultLocale());
        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->translate($locale)->getName(),
                ($result->translate($locale)->getCreated()) ? $result->translate($locale)->getCreated()->format('d/m/Y') : $result->getCreated()->format('d/m/Y'),
                $result->translate($locale)->getActive()
            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_'.$locale.'_services_export.csv"'
        ));
    }
}
