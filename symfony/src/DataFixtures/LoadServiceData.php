<?php

namespace App\DataFixtures;

use App\Entity\Project;
use App\Entity\Service;
use App\Entity\ServiceTranslation;
use App\Utils\Managers\LocaleManager;
use App\Utils\Managers\ServiceManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadServiceData
 *
 * @package Disko\UserBundle\DataFixtures\ORM
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class LoadServiceData extends Fixture implements DependentFixtureInterface
{
    private $serviceManager;
    private $localeManager;

    public function __construct(ServiceManager $serviceManager, LocaleManager $localeManager)
    {
        $this->serviceManager = $serviceManager;
        $this->localeManager = $localeManager;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $services = array(
//            [
//              'name' => 'Nettoyage',
//              'code' => 'NET',
//              'isServiceStandard' => true,
//              'choiceMomentService' => ServiceTranslation::CHOICE_SERVICE_BOTH,
//              'stock' => Project::CAVE_PANTIN
//            ],
//            [
//              'name' => 'Peinture',
//              'code' => 'PEI',
//              'isServiceStandard' => true,
//              'choiceMomentService' => ServiceTranslation::CHOICE_SERVICE_AFTER,
//              'stock' => Project::CAVE_PANTIN
//            ],
//            [
//              'name' => 'Réparation',
//              'code' => 'REP',
//              'isServiceStandard' => true,
//              'choiceMomentService' => ServiceTranslation::CHOICE_SERVICE_BEFORE,
//              'stock' => Project::CAVE_PANTIN
//            ]
        );

        $locales = $this->localeManager->findAll();

        foreach ($services as $service) {
            $obj = new Service();
            foreach ($locales as $locale) {
                $trans = new ServiceTranslation();
                $trans->setLocale($locale->getSlug());
                $trans->setName($service['name']);
                $trans->setCode($service['code']);
                $trans->setIsServiceStandard($service['isServiceStandard']);
                $trans->setChoiceMomentService($service['choiceMomentService']);
                $trans->setStock($service['stock']);
                $obj->addTranslation($trans);
            }
            $this->serviceManager->save($obj);
            $this->addReference('admin-service-'.$obj->getId(), $obj);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

    public function getDependencies()
    {
        return [
          LoadLocaleData::class
      ];
    }
}
