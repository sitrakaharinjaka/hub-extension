<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MyBoxesBarcodeConstraint extends Constraint
{
    public $message_exist = 'Ce code barre est incorrect';
    public $message_free = 'Ce code barre est déjà attribué';

    public function validatedBy()
    {
        return 'myboxes_barcode_constraint';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
