<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 * @ORM\Table(name="dk_project")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Project
{
    public const RETURN_DAY = '+1 weekday';

    public const CAVE_115 = '115';
    public const CAVE_PANTIN = 'pantin';

    public const AD_BAC = "142 rue du bac, 75007 Paris";
    public const AD_ARMEE = "4 rue Pergolèse, 75016 Paris";

    public const AD_BAC_CODE = 1;
    public const AD_ARMEE_CODE = 2;

    public const LIST_CHOICES_SERVICE_CAVES = [
        'app.cave.service.cave_115' => self::CAVE_115,
        'app.cave.service.cave_pantin' => self::CAVE_PANTIN,
    ];

    public const STATE_HUB_DRAFT = 'draft'; // Draft (no stock stun)
    public const STATE_HUB_ACCEPTED = 'accepted'; // Acctepted (Stock stun)
    public const STATE_HUB_REFUSED = 'refused'; // Veolog refuse state
    public const STATE_HUB_ACCEPTED_FIXED = 'final_acceptation'; // Project non editable in time
    public const STATE_HUB_ACCEPTED_FIXED_VEOLOG_REJECTED = 'final_acceptation_rejection_veolog'; // Project rejected by veolog
    public const STATE_HUB_ACCEPTED_FIXED_VEOLOG_INT = 'final_acceptation_integration_veolog'; // Project non editable in time sending to veolog
    public const STATE_HUB_ACCEPTED_FIXED_VEOLOG_CANCEL = 'final_acceptation_cancel_veolog'; // Project non editable in time sending to veolog
    public const STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE = 'final_acceptation_preparation_veolog'; // Project non editable in time sending to veolog
    public const STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP = 'final_acceptation_expedition_veolog'; // Project non editable in time sending to veolog
    public const STATE_HUB_CANCEL_ASK = 'cancel_ask'; // Delete by user
    public const STATE_HUB_CANCEL = 'cancel'; // Delete by user
    public const STATE_HUB_DRAFT_CANCEL_ASK = 'draft_cancel_ask'; // Delete by user from a draft state
    public const STATE_HUB_DELIVERED = 'delivered'; // Project délivré par le runner (Cave 115 only)

    public const DELAY_VEOLOG_STUN = "-7 days"; // J-7 project stun by Veolog

    public const CUTOFF_CLASSIC = "17"; // before 17h D Day, delivery between 10h/17h in D Day + 1
    public const CUTOFF_SHUTTLE = "10"; // before 10h D Day, delivery 14h in D Day
    public const CUTOFF_EMERGENCY = "16"; // before 16h D Day, delivery D Day in H+4

    public const STATE_EDITABLE = [
        self::STATE_HUB_DRAFT,
        self::STATE_HUB_ACCEPTED,
        self::STATE_HUB_REFUSED,
        //self::STATE_HUB_ACCEPTED_FIXED,
        //self::STATE_HUB_ACCEPTED_FIXED_VEOLOG_REJECTED,
        //self::STATE_HUB_ACCEPTED_FIXED_VEOLOG_INT
    ];

    public const TYPE_BASIC   = "basic";
    public const TYPE_URGENCY = "urgency";
    public const TYPE_SHUTTLE = "shuttle";
    public const TYPE_BULK    = "bulk";

    use GedmoTrait;
    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Project|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="subProjects", cascade={"persist"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    /**
     * @var Collection<mixed, mixed>|Project[]
     * @ORM\OneToMany(targetEntity=Project::class, mappedBy="parent")
     */
    private $subProjects;

    /**
     * Name
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $name;

    /**
     * Slug
     *
     * @Gedmo\Slug(fields={"name"}, unique=false)
     * @ORM\Column(length=255, unique=false)
     */
    protected $slug;

    /**
     * @var \DateTimeInterface|null $dateAddVlg
     * @ORM\Column(name="date_add_vlg", type="datetime", nullable=true)
     */
    public $dateAddVlg;

    /**
     * @var \DateTimeInterface|null $dateStart
     * @ORM\Column(name="date_start", type="date")
     */
    public $dateStart;

    /**
     * @var \DateTimeInterface|null $hourStart
     * @ORM\Column(name="hour_start", type="time")
     */
    private $hourStart;

    /**
     * @var \DateTimeInterface|null $dateEnd
     * @ORM\Column(name="date_end", type="date")
     */
    private $dateEnd;

    /**
     * @var \DateTimeInterface|null $dateEnd
     * @ORM\Column(name="date_return", type="date")
     */
    private $dateReturn;

    /**
     * @var \DateTimeInterface|null $hourEnd
     * @ORM\Column(name="hour_end", type="time")
     */
    public $hourEnd;

    /**
     * Information de contact retour
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $infoReturn;

    /**
     * Email de contact retour
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $emailReturn;

    /**
     * Telephone de contact retour
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phoneReturn;

    /**
     * @var User|null
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="projects")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var User|null
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="preparations")
     * @ORM\JoinColumn(name="runner_id", referencedColumnName="id")
     */
    protected $runner;

    /**
     * @var Stock|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", inversedBy="projects")
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
     */
    protected $stock;

    /**
     * @var string
     * @ORM\Column(type="string", options={"default" : "draft"})
     */
    protected $state = self::STATE_HUB_DRAFT;

    /**
     * @ORM\Column(type="array")
     */
    protected $trackers;

    /**
     * @var string
     * @ORM\Column(type="string", options={"default" : "basic"})
     */
    protected $type = self::TYPE_BASIC;

    /**
     * @var string|null
     * @ORM\Column(name="reason_of_delete", type="string", nullable=true)
     */
    protected $reasonOfDelete;

    /**
     * @var Collection|Project[]
     * @ORM\OneToMany(targetEntity="App\Entity\StockLine", mappedBy="project", cascade={"all"})
     */
    protected $stockLines;

    /**
     * @var Collection|ProjectItem[]
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectItem", mappedBy="project", cascade={"all"})
     */
    protected $items;

    /**
     * @var string|null
     */
    protected $button;


    /**
     * @var string
     * @ORM\Column(type="string", options={"default" : "pantin"})
     */
    protected $cave = Project::CAVE_PANTIN;


    /**
     * @var Stock|null
     * @ORM\ManyToOne(targetEntity="App\Entity\BillingCenter", inversedBy="projects")
     * @ORM\JoinColumn(name="billing_center_id", referencedColumnName="id")
     */
    protected $billingCenter;

    /**
     * @var string
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    protected $needReturn = false;

    /**
     * @var string
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    protected $returnMail = false;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $recipientSociety;


    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $recipientFirstName;


    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $recipientLastName;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $recipientPhone;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $recipientMobilePhone;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $recipientEmail;


    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $recipientCountry;


    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $recipientAddress;


    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $recipientAddressComplement;


    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $recipientZip;


    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $recipientCity;


    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $recipientInformation;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Service")
     * @ORM\JoinTable(name="dk_project_services_before")
     *
     * @var ArrayCollection
     */
    protected $beforeServices;

    /**
     * @var bool
     */
    protected $needQuote = false;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $beforeQuote;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $beforeOther;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Service")
     * @ORM\JoinTable(name="dk_project_services_after")
     *
     * @var ArrayCollection
     */
    protected $afterServices;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $afterQuote;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $afterOther;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $landingPerson;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $landingHour;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $landingComment;


    /**
     * Date of expedition
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $expeditionDate;


    /**
     * Date of wait to prepare
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $waitDate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectRejection", mappedBy="project", orphanRemoval=true, cascade={"all"})
     */
    private $rejectionMessages;

    public function __construct()
    {
        $this->stockLines     = new ArrayCollection();
        $this->items          = new ArrayCollection();
        $this->afterServices  = new ArrayCollection();
        $this->beforeServices = new ArrayCollection();
        $this->subProjects    = new ArrayCollection();

        $this->trackers = [];

        $date = new \Datetime();
        $date->setTime(10, 0, 0);
        $this->hourStart = $date;

        $date = new \Datetime();
        $date->setTime(7, 0, 0);
        $this->hourEnd = $date;
        $this->rejectionMessages = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param  mixed  $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return ?Project
     */
    public function getParent(): ?Project
    {
        return $this->parent;
    }

    /**
     * @param ?Project $parent
     */
    public function setParent(?Project $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return Collection<mixed, mixed>|self[]
     */
    public function getSubProjects(): ?Collection
    {
        return $this->subProjects;
    }

    public function addSubProject(self $subProject): self
    {
        if (!$this->subProjects->contains($subProject)) {
            $this->subProjects[] = $subProject;
            $subProject->setParent($this);
        }

        return $this;
    }

    public function removeSubProject(self $subProject): self
    {
        if ($this->subProjects->removeElement($subProject)) {
            // set the owning side to null (unless already changed)
            if ($subProject->getParent() === $this) {
                $subProject->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param  mixed  $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param  mixed  $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getPlace()
    {
        $place = $this->getRecipientSociety() . ', ' . $this->getRecipientAddress()
             . "\n";
        $place = $place . $this->getRecipientZip() . ' ' . $this->getRecipientCity()
             . ", " . $this->getRecipientCountry();

        $place = trim(trim($place, ','));

        return $place;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateAddVlg(): ?\DateTimeInterface
    {
        return $this->dateAddVlg;
    }

    /**
     * @param  \DateTimeInterface|null  $dateAddVlg
     */
    public function setDateAddVlg(?\DateTimeInterface $dateAddVlg): void
    {
        $this->dateAddVlg = $dateAddVlg;
    }


    /**
     * @return \DateTimeInterface|null
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * @param  \DateTimeInterface|null  $dateStart
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getHourStart()
    {
        return $this->hourStart;
    }

    /**
     * @param  \DateTimeInterface|null  $hourStart
     */
    public function setHourStart($hourStart)
    {
        $this->hourStart = $hourStart;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @param  \DateTimeInterface|null  $dateEnd
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;
        $this->updateDateReturn();
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateReturn()
    {
        return $this->dateReturn;
    }

    /**
     * @param  \DateTimeInterface|null  $dateReturn
     */
    public function setDateReturn($dateReturn)
    {
        $this->dateReturn = $dateReturn;
    }


    public function updateDateReturn()
    {
        if ($this->dateEnd!==null) {
            // $date = new \DateTime($this->dateEnd->format('Y-m-d'));
            // $date->modify(self::RETURN_DAY);
            // $this->setDateReturn($date);
            $this->setDateReturn($this->dateEnd);
        } else {
            $this->setDateReturn(null);
        }
    }


    /**
     * @return \DateTimeInterface|null
     */
    public function getHourEnd()
    {
        return $this->hourEnd;
    }

    /**
     * @param  \DateTimeInterface|null  $hourEnd
     */
    public function setHourEnd($hourEnd)
    {
        $this->hourEnd = $hourEnd;
    }

    /**
     * @return mixed
     */
    public function getInfoReturn()
    {
        return $this->infoReturn;
    }

    /**
     * @param  mixed  $text
     */
    public function setInfoReturn($text)
    {
        $this->infoReturn = $text;
    }

    /**
     * @return mixed
     */
    public function getEmailReturn()
    {
        return $this->emailReturn;
    }

    /**
     * @param  mixed  $text
     */
    public function setEmailReturn($text)
    {
        $this->emailReturn = $text;
    }

    /**
     * @return mixed
     */
    public function getPhoneReturn()
    {
        return $this->phoneReturn;
    }

    /**
     * @param  mixed  $text
     */
    public function setPhoneReturn($text)
    {
        $this->phoneReturn = $text;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param  User|null  $user
     */
    public function setUser(?User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User|null
     */
    public function getRunner()
    {
        return $this->runner;
    }

    /**
     * @param  User|null  $runner
     */
    public function setRunner(?User $runner)
    {
        $this->runner = $runner;
    }

    /**
     * @return User|null
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param  User|null  $stock
     */
    public function setStock(?Stock $stock)
    {
        $this->stock = $stock;
    }


    /**
     * {@inheritdoc}
     */
    public function addStockLine(StockLine $stockLine)
    {
        if ($this->hasStockLine($stockLine)) {
            return;
        }

        $this->stockLines->add($stockLine);
    }

    /**
     * {@inheritdoc}
     */
    public function removeStockLine(StockLine $stockLine)
    {
        if ($this->hasStockLine($stockLine)) {
            $this->stockLines->removeElement($stockLine);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getStockLines()
    {
        return $this->stockLines;
    }

    /**
     * {@inheritdoc}
     */
    public function clearStockLines()
    {
        $this->stockLines->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function hasStockLine(StockLine $stockLine)
    {
        return $this->stockLines->contains($stockLine);
    }


    /**
     * @return bool
     */
    public function isClean(): bool
    {
        return $this->clean;
    }

    /**
     * {@inheritdoc}
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * {@inheritdoc}
     */
    public function setState($state)
    {
        if ($state == self::STATE_HUB_ACCEPTED_FIXED) {
            $this->setWaitDate(new \DateTime());
        }

        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getTrackers()
    {
        return !$this->trackers ? [] : $this->trackers;
    }

    /**
     * @param mixed $trackers
     */
    public function setTrackers($trackers)
    {
        $this->trackers = $trackers;
    }

    /**
     * @param mixed $tracker
     */
    public function addTracker($tracker)
    {
        if (!$this->trackers) {
            $this->trackers = [];
        }
        if (false === array_search($tracker, $this->trackers)) {
            $this->trackers[] = $tracker;
        }
    }

    /**
     * @return bool
     */
    public function isFinalAcceptation()
    {
        return $this->state === self::STATE_HUB_ACCEPTED_FIXED;
    }

    public function isEditable(): bool
    {
        return in_array($this->state, self::STATE_EDITABLE);
    }

    public function isDraft(): bool
    {
        return $this->state === self::STATE_HUB_DRAFT;
    }

    public function isAccepted(): bool
    {
        return $this->state === self::STATE_HUB_ACCEPTED;
    }

    /**
     * @return null|string
     */
    public function getReasonOfDelete()
    {
        return $this->reasonOfDelete;
    }

    /**
     * @param  null|$reasonOfDelete
     */
    public function setReasonOfDelete($reasonOfDelete)
    {
        $this->reasonOfDelete = $reasonOfDelete;
    }


    /**
     * {@inheritdoc}
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * {@inheritdoc}
     */
    public function clearItems()
    {
        $this->items->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function countItems()
    {
        return $this->items->count();
    }

    /**
     * {@inheritdoc}
     */
    public function addItem(ProjectItem $item)
    {
        if ($this->hasItem($item)) {
            return;
        }

        $this->items->add($item);
        $item->setProject($this);
    }

    /**
     * {@inheritdoc}
     */
    public function removeItem(ProjectItem $item)
    {
        if ($this->hasItem($item)) {
            $this->items->removeElement($item);
            $item->setProject(null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasItem(ProjectItem $item)
    {
        return $this->items->contains($item);
    }

    public function getTotalQuantity()
    {
        $total = 0;
        $items = $this->getItems();

        foreach ($items as $item) {
            $total += $item->getQuantity();
        }

        return $total;
    }

    public function isEmpty()
    {
        return $this->getTotalQuantity() <= 0;
    }

    /**
     * @return null|string
     */
    public function getButton()
    {
        return $this->button;
    }

    /**
     * @param  null|$button
     */
    public function setButton($button)
    {
        $this->button = $button;
    }

    public function getBaseItem($productId)
    {
        $base = 0;
        foreach ($this->getItems() as $it) {
            if ($it->getProduct()->getId() == $productId) {
                $base = $base + $it->getQuantity();
            }
        }

        return $base;
    }

    public function getBookItem($productId)
    {
        $base = 0;
        foreach ($this->getStockLines() as $it) {
            if ($it->getProduct()->getId() == $productId && $it->getNumber() < 0) {
                $base = $base + $it->getNumber();
            }
        }

        return $base;
    }


    /**
     * @return string
     */
    public function getCave()
    {
        return $this->cave;
    }

    /**
     * @param $cave
     */
    public function setCave($cave)
    {
        $this->cave = $cave;
    }

    /**
     * @return BillingCenter|null
     */
    public function getBillingCenter()
    {
        return $this->billingCenter;
    }

    /**
     * @param  BillingCenter|null  $billingCenter
     */
    public function setBillingCenter($billingCenter)
    {
        $this->billingCenter = $billingCenter;
    }

    /**
     * @return string
     */
    public function getNeedReturn()
    {
        return $this->needReturn;
    }

    /**
     * @param $needReturn
     */
    public function setNeedReturn($needReturn)
    {
        $this->needReturn = $needReturn;
    }

    /**
     * @return string
     */
    public function getRecipientSociety()
    {
        return $this->recipientSociety;
    }

    /**
     * @param $recipientSociety
     */
    public function setRecipientSociety($recipientSociety)
    {
        $this->recipientSociety = $recipientSociety;
    }

    /**
     * @return string
     */
    public function getRecipientFirstName()
    {
        return $this->recipientFirstName;
    }

    /**
     * @param $recipientFirstName
     */
    public function setRecipientFirstName($recipientFirstName)
    {
        $this->recipientFirstName = $recipientFirstName;
    }

    /**
     * @return string
     */
    public function getRecipientLastName()
    {
        return $this->recipientLastName;
    }

    /**
     * @param $recipientLastName
     */
    public function setRecipientLastName($recipientLastName)
    {
        $this->recipientLastName = $recipientLastName;
    }

    /**
     * @return string
     */
    public function getRecipientPhone()
    {
        return $this->recipientPhone;
    }

    /**
     * @param $recipientPhone
     */
    public function setRecipientPhone($recipientPhone)
    {
        $this->recipientPhone = $recipientPhone;
    }

    /**
     * @return string
     */
    public function getRecipientMobilePhone()
    {
        return $this->recipientMobilePhone;
    }

    /**
     * @param $recipientMobilePhone
     */
    public function setRecipientMobilePhone($recipientMobilePhone)
    {
        $this->recipientMobilePhone = $recipientMobilePhone;
    }

    /**
     * @return string
     */
    public function getRecipientEmail()
    {
        return $this->recipientEmail;
    }

    /**
     * @param $recipientEmail
     */
    public function setRecipientEmail($recipientEmail)
    {
        $this->recipientEmail = $recipientEmail;
    }

    /**
     * @return string
     */
    public function getRecipientCountry()
    {
        return $this->recipientCountry;
    }

    /**
     * @param $recipientCountry
     */
    public function setRecipientCountry($recipientCountry)
    {
        if (strlen($recipientCountry) != 2) {
            $isoCountry = self::getIsoCountry($recipientCountry);
            $this->recipientCountry = $isoCountry;
        } else {
            $this->recipientCountry = $recipientCountry;
        }
    }

    /**
     * @return string
     */
    public function getRecipientAddress()
    {
        return $this->recipientAddress;
    }

    /**
     * @param $recipientAddress
     */
    public function setRecipientAddress($recipientAddress)
    {
        $this->recipientAddress = $recipientAddress;
    }

    /**
     * @return string
     */
    public function getRecipientAddressComplement()
    {
        return $this->recipientAddressComplement;
    }

    /**
     * @param $recipientAddressComplement
     */
    public function setRecipientAddressComplement($recipientAddressComplement)
    {
        $this->recipientAddressComplement = $recipientAddressComplement;
    }

    /**
     * @return string
     */
    public function getRecipientZip()
    {
        return $this->recipientZip;
    }

    /**
     * @param $recipientZip
     */
    public function setRecipientZip($recipientZip)
    {
        $this->recipientZip = $recipientZip;
    }

    /**
     * @return string
     */
    public function getRecipientCity()
    {
        return $this->recipientCity;
    }

    /**
     * @param $recipientCity
     */
    public function setRecipientCity($recipientCity)
    {
        $this->recipientCity = $recipientCity;
    }

    /**
     * @return string
     */
    public function getRecipientInformation()
    {
        return $this->recipientInformation;
    }

    /**
     * @param $recipientInformation
     */
    public function setRecipientInformation($recipientInformation)
    {
        $this->recipientInformation = $recipientInformation;
    }

    /**
     * @return ArrayCollection
     */
    public function getBeforeServices()
    {
        return $this->beforeServices;
    }

    /**
     * {@inheritdoc}
     */
    public function addBeforeService($service)
    {
        if ($this->hasBeforeService($service)) {
            return;
        }

        $this->beforeServices->add($service);
    }

    /**
     * {@inheritdoc}
     */
    public function removeBeforeService($service)
    {
        if ($this->hasBeforeService($service)) {
            $this->beforeServices->removeElement($service);
        }
    }


    /**
     * {@inheritdoc}
     */
    public function clearBeforeServices()
    {
        $this->beforeServices->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function hasBeforeService($service)
    {
        return $this->beforeServices->contains($service);
    }


    /**
     * @return ArrayCollection
     */
    public function getAfterServices()
    {
        return $this->afterServices;
    }

    /**
     * {@inheritdoc}
     */
    public function addAfterService($service)
    {
        if ($this->hasAfterService($service)) {
            return;
        }

        $this->afterServices->add($service);
    }

    /**
     * {@inheritdoc}
     */
    public function removeAfterService($service)
    {
        if ($this->hasAfterService($service)) {
            $this->afterServices->removeElement($service);
        }
    }


    /**
     * {@inheritdoc}
     */
    public function clearAfterServices()
    {
        $this->afterServices->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function hasAfterService($service)
    {
        return $this->afterServices->contains($service);
    }

    /**
     * @return bool
     */
    public function isNeedQuote(): bool
    {
        return $this->needQuote;
    }

    /**
     * @param bool $needQuote
     */
    public function setNeedQuote(bool $needQuote): void
    {
        $this->needQuote = $needQuote;
    }

    /**
     * @return string
     */
    public function getBeforeQuote()
    {
        return $this->beforeQuote;
    }

    /**
     * @param $beforeQuote
     */
    public function setBeforeQuote($beforeQuote)
    {
        $this->beforeQuote = $beforeQuote;
    }

    /**
     * @return string
     */
    public function getBeforeOther()
    {
        return $this->beforeOther;
    }

    /**
     * @param $beforeOther
     */
    public function setBeforeOther($beforeOther)
    {
        $this->beforeOther = $beforeOther;
    }

    /**
     * @return string
     */
    public function getAfterQuote()
    {
        return $this->afterQuote;
    }

    /**
     * @param $afterQuote
     */
    public function setAfterQuote($afterQuote)
    {
        $this->afterQuote = $afterQuote;
    }

    /**
     * @return string
     */
    public function getAfterOther()
    {
        return $this->afterOther;
    }

    /**
     * @param $afterOther
     */
    public function setAfterOther($afterOther)
    {
        $this->afterOther = $afterOther;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function isOwnedBy(User $user): bool
    {
        $owned = $this->user === $user;
        $subOwned = true;
        if ($this->type === self::TYPE_BULK && $this->subProjects->count()) {
            $subOwned = $this->countSubProjectsOwnedBy($user) === $this->subProjects->count();
        }

        return $owned && $subOwned;
    }

    public function countSubProjectsOwnedBy(User $user)
    {
        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->eq('user', $user))
        ;

        return $this->subProjects->matching($criteria)->count();
    }

    public function countSubProjectDeletable()
    {
        if($this->subProjects){
            $projectCount = 0;

            foreach($this->subProjects as $project){
                if(($project->isDeletable() || $project->getState() == self::STATE_HUB_CANCEL_ASK || $project->getState() == self::STATE_HUB_DRAFT_CANCEL_ASK) && $project->getType() !== self::TYPE_BULK){
                    $projectCount++;
                }
            }

            return $projectCount;
        }

        return 0;
    }
    
    public function isDeletable(): bool
    {
        if ($this->type === self::TYPE_BULK && $this->subProjects && $this->countSubProjectDeletable() !== $this->subProjects->count()) {
            return false;
        }

        if ($this->state == self::STATE_HUB_DRAFT) {
            return true;
        }

        if ($this->state == self::STATE_HUB_ACCEPTED_FIXED && is_null($this->getDateAddVlg())) {
            return true;
        }

        if ($this->isEditable()) {
            return true;
        }

        $dateStart = $this->getDateStart();
        $today     = new \DateTime();
        $diff      = $today->diff($dateStart)->format("%a");
        if ($this->state == self::STATE_HUB_ACCEPTED_FIXED && $diff > 3) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getLandingPerson()
    {
        return $this->landingPerson;
    }

    /**
     * @param  string  $person
     */
    public function setLandingPerson($person)
    {
        $this->landingPerson = $person;
    }

    /**
     * @return string
     */
    public function getLandingHour()
    {
        return $this->landingHour;
    }

    /**
     * @param  string  $hour
     */
    public function setLandingHour($hour)
    {
        $this->landingHour = $hour;
    }

    /**
     * @return string
     */
    public function getLandingComment()
    {
        return $this->landingComment;
    }

    /**
     * @param  string  $comment
     */
    public function setLandingComment($comment)
    {
        $this->landingComment = $comment;
    }

    /**
     * @return mixed
     */
    public function getExpeditionDate()
    {
        return $this->expeditionDate;
    }

    /**
     * @param mixed $expeditionDate
     */
    public function setExpeditionDate($expeditionDate)
    {
        $this->expeditionDate = $expeditionDate;
    }

    /**
     * @return mixed
     */
    public function getWaitDate()
    {
        return $this->waitDate;
    }

    /**
     * @param mixed $waitDate
     */
    public function setWaitDate($waitDate)
    {
        $this->waitDate = $waitDate;
    }

    /**
     * @return string
     */
    public function getReturnMail()
    {
        return $this->returnMail;
    }

    /**
     * @param string $returnMail
     */
    public function setReturnMail($returnMail)
    {
        $this->returnMail = $returnMail;
    }

    public function getIsoCountry($country)
    {
        $countries = array(
            'Afghanistan' => 'AF',
            'Aland Islands' => 'AX',
            'Albania' => 'AL',
            'Algeria' => 'DZ',
            'American Samoa' => 'AS',
            'Andorra' => 'AD',
            'Angola' => 'AO',
            'Anguilla' => 'AI',
            'Antarctica' => 'AQ',
            'Antigua And Barbuda' => 'AG',
            'Argentina' => 'AR',
            'Armenia' => 'AM',
            'Aruba' => 'AW',
            'Australia' => 'AU',
            'Austria' => 'AT',
            'Azerbaijan' => 'AZ',
            'Bahamas' => 'BS',
            'Bahrain' => 'BH',
            'Bangladesh' => 'BD',
            'Barbados' => 'BB',
            'Belarus' => 'BY',
            'Belgium' => 'BE',
            'Belize' => 'BZ',
            'Benin' => 'BJ',
            'Bermuda' => 'BM',
            'Bhutan' => 'BT',
            'Bolivia' => 'BO',
            'Bosnia And Herzegovina' => 'BA',
            'Botswana' => 'BW',
            'Bouvet Island' => 'BV',
            'Brazil' => 'BR',
            'British Indian Ocean Territory' => 'IO',
            'Brunei Darussalam' => 'BN',
            'Bulgaria' => 'BG',
            'Burkina Faso' => 'BF',
            'Burundi' => 'BI',
            'Cambodia' => 'KH',
            'Cameroon' => 'CM',
            'Canada' => 'CA',
            'Cape Verde' => 'CV',
            'Cayman Islands' => 'KY',
            'Central African Republic' => 'CF',
            'Chad' => 'TD',
            'Chile' => 'CL',
            'China' => 'CN',
            'Christmas Island' => 'CX',
            'Cocos (Keeling) Islands' => 'CC',
            'Colombia' => 'CO',
            'Comoros' => 'KM',
            'Congo' => 'CG',
            'Congo, Democratic Republic' => 'CD',
            'Cook Islands' => 'CK',
            'Costa Rica' => 'CR',
            'Cote D\'Ivoire' => 'CI',
            'Croatia' => 'HR',
            'Cuba' => 'CU',
            'Cyprus' => 'CY',
            'Czech Republic' => 'CZ',
            'Denmark' => 'DK',
            'Djibouti' => 'DJ',
            'Dominica' => 'DM',
            'Dominican Republic' => 'DO',
            'Ecuador' => 'EC',
            'Egypt' => 'EG',
            'El Salvador' => 'SV',
            'Equatorial Guinea' => 'GQ',
            'Eritrea' => 'ER',
            'Estonia' => 'EE',
            'Ethiopia' => 'ET',
            'Falkland Islands (Malvinas)' => 'FK',
            'Faroe Islands' => 'FO',
            'Fiji' => 'FJ',
            'Finland' => 'FI',
            'France' => 'FR',
            'French Guiana' => 'GF',
            'French Polynesia' => 'PF',
            'French Southern Territories' => 'TF',
            'Gabon' => 'GA',
            'Gambia' => 'GM',
            'Georgia' => 'GE',
            'Germany' => 'DE',
            'Ghana' => 'GH',
            'Gibraltar' => 'GI',
            'Greece' => 'GR',
            'Greenland' => 'GL',
            'Grenada' => 'GD',
            'Guadeloupe' => 'GP',
            'Guam' => 'GU',
            'Guatemala' => 'GT',
            'Guernsey' => 'GG',
            'Guinea' => 'GN',
            'Guinea-Bissau' => 'GW',
            'Guyana' => 'GY',
            'Haiti' => 'HT',
            'Heard Island & Mcdonald Islands' => 'HM',
            'Holy See (Vatican City State)' => 'VA',
            'Honduras' => 'HN',
            'Hong Kong' => 'HK',
            'Hungary' => 'HU',
            'Iceland' => 'IS',
            'India' => 'IN',
            'Indonesia' => 'ID',
            'Iran, Islamic Republic Of' => 'IR',
            'Iraq' => 'IQ',
            'Ireland' => 'IE',
            'Isle Of Man' => 'IM',
            'Israel' => 'IL',
            'Italy' => 'IT',
            'Jamaica' => 'JM',
            'Japan' => 'JP',
            'Jersey' => 'JE',
            'Jordan' => 'JO',
            'Kazakhstan' => 'KZ',
            'Kenya' => 'KE',
            'Kiribati' => 'KI',
            'Korea' => 'KR',
            'Kuwait' => 'KW',
            'Kyrgyzstan' => 'KG',
            'Lao People\'s Democratic Republic' => 'LA',
            'Latvia' => 'LV',
            'Lebanon' => 'LB',
            'Lesotho' => 'LS',
            'Liberia' => 'LR',
            'Libyan Arab Jamahiriya' => 'LY',
            'Liechtenstein' => 'LI',
            'Lithuania' => 'LT',
            'Luxembourg' => 'LU',
            'Macao' => 'MO',
            'Macedonia' => 'MK',
            'Madagascar' => 'MG',
            'Malawi' => 'MW',
            'Malaysia' => 'MY',
            'Maldives' => 'MV',
            'Mali' => 'ML',
            'Malta' => 'MT',
            'Marshall Islands' => 'MH',
            'Martinique' => 'MQ',
            'Mauritania' => 'MR',
            'Mauritius' => 'MU',
            'Mayotte' => 'YT',
            'Mexico' => 'MX',
            'Micronesia, Federated States Of' => 'FM',
            'Moldova' => 'MD',
            'Monaco' => 'MC',
            'Mongolia' => 'MN',
            'Montenegro' => 'ME',
            'Montserrat' => 'MS',
            'Morocco' => 'MA',
            'Mozambique' => 'MZ',
            'Myanmar' => 'MM',
            'Namibia' => 'NA',
            'Nauru' => 'NR',
            'Nepal' => 'NP',
            'Netherlands' => 'NL',
            'Netherlands Antilles' => 'AN',
            'New Caledonia' => 'NC',
            'New Zealand' => 'NZ',
            'Nicaragua' => 'NI',
            'Niger' => 'NE',
            'Nigeria' => 'NG',
            'Niue' => 'NU',
            'Norfolk Island' => 'NF',
            'Northern Mariana Islands' => 'MP',
            'Norway' => 'NO',
            'Oman' => 'OM',
            'Pakistan' => 'PK',
            'Palau' => 'PW',
            'Palestinian Territory, Occupied' => 'PS',
            'Panama' => 'PA',
            'Papua New Guinea' => 'PG',
            'Paraguay' => 'PY',
            'Peru' => 'PE',
            'Philippines' => 'PH',
            'Pitcairn' => 'PN',
            'Poland' => 'PL',
            'Portugal' => 'PT',
            'Puerto Rico' => 'PR',
            'Qatar' => 'QA',
            'Reunion' => 'RE',
            'Romania' => 'RO',
            'Russian Federation' => 'RU',
            'Rwanda' => 'RW',
            'Saint Barthelemy' => 'BL',
            'Saint Helena' => 'SH',
            'Saint Kitts And Nevis' => 'KN',
            'Saint Lucia' => 'LC',
            'Saint Martin' => 'MF',
            'Saint Pierre And Miquelon' => 'PM',
            'Saint Vincent And Grenadines' => 'VC',
            'Samoa' => 'WS',
            'San Marino' => 'SM',
            'Sao Tome And Principe' => 'ST',
            'Saudi Arabia' => 'SA',
            'Senegal' => 'SN',
            'Serbia' => 'RS',
            'Seychelles' => 'SC',
            'Sierra Leone' => 'SL',
            'Singapore' => 'SG',
            'Slovakia' => 'SK',
            'Slovenia' => 'SI',
            'Solomon Islands' => 'SB',
            'Somalia' => 'SO',
            'South Africa' => 'ZA',
            'South Georgia And Sandwich Isl.' => 'GS',
            'Spain' => 'ES',
            'Sri Lanka' => 'LK',
            'Sudan' => 'SD',
            'Suriname' => 'SR',
            'Svalbard And Jan Mayen' => 'SJ',
            'Swaziland' => 'SZ',
            'Sweden' => 'SE',
            'Switzerland' => 'CH',
            'Syrian Arab Republic' => 'SY',
            'Taiwan' => 'TW',
            'Tajikistan' => 'TJ',
            'Tanzania' => 'TZ',
            'Thailand' => 'TH',
            'Timor-Leste' => 'TL',
            'Togo' => 'TG',
            'Tokelau' => 'TK',
            'Tonga' => 'TO',
            'Trinidad And Tobago' => 'TT',
            'Tunisia' => 'TN',
            'Turkey' => 'TR',
            'Turkmenistan' => 'TM',
            'Turks And Caicos Islands' => 'TC',
            'Tuvalu' => 'TV',
            'Uganda' => 'UG',
            'Ukraine' => 'UA',
            'United Arab Emirates' => 'AE',
            'United Kingdom' => 'GB',
            'United States' => 'US',
            'United States Outlying Islands' => 'UM',
            'Uruguay' => 'UY',
            'Uzbekistan' => 'UZ',
            'Vanuatu' => 'VU',
            'Venezuela' => 'VE',
            'Viet Nam' => 'VN',
            'Virgin Islands, British' => 'VG',
            'Virgin Islands, U.S.' => 'VI',
            'Wallis And Futuna' => 'WF',
            'Western Sahara' => 'EH',
            'Yemen' => 'YE',
            'Zambia' => 'ZM',
            'Zimbabwe' => 'ZW',
            'Allemagne' => 'DE',
            'Autriche' => 'AT',
            'Belgique' => 'BE',
            'Danemark' => 'DK',
            'Espagne' => 'ES',
            'Estonie' => 'EE',
            'Finlande' => 'FI',
            'Grèce' => 'GR',
            'Hongrie' => 'HU',
            'Islande' => 'IS',
            'Italie' => 'IT',
            'Lettonie' => 'LV',
            'Lituanie' => 'LT',
            'Malte' => 'MT',
            'Norvège' => 'NO',
            'Pays-Bas' => 'NL',
            'Pologne' => 'PL',
            'Portugal' => 'PT',
            'République Tchèque' => 'CZ',
            'Slovaquie' => 'SK',
            'Slovénie' => 'SI',
            'Suède' => 'SE'
        );

        if (array_key_exists(ucfirst(strtolower($country)), $countries)) {
            $isoCountry = $countries[ucfirst(strtolower($country))];
        } else {
            $isoCountry = null;
        }

        return $isoCountry;
    }

    /**
     * @return Collection|ProjectRejection[]
     */
    public function getRejectionMessages(): Collection
    {
        return $this->rejectionMessages;
    }

    public function addRejectionMessage(ProjectRejection $rejectionMessage): self
    {
        if (!$this->rejectionMessages->contains($rejectionMessage)) {
            $this->rejectionMessages[] = $rejectionMessage;
            $rejectionMessage->setProject($this);
        }

        return $this;
    }

    public function removeRejectionMessage(ProjectRejection $rejectionMessage): self
    {
        if ($this->rejectionMessages->contains($rejectionMessage)) {
            $this->rejectionMessages->removeElement($rejectionMessage);
            // set the owning side to null (unless already changed)
            if ($rejectionMessage->getProject() === $this) {
                $rejectionMessage->setProject(null);
            }
        }

        return $this;
    }
}
