<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class StockEntryModelConstraint extends Constraint
{
    public function validatedBy()
    {
        return 'stock_entry_model_constraint';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
