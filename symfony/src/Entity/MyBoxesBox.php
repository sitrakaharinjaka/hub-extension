<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MyBoxesBoxRepository")
 * @ORM\Table(name="dk_myboxes_box")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class MyBoxesBox
{
    use GedmoTrait;

    /* ================= */
    /* == COMMANDE ===== */
    /* ================= */
    public const STATE_CMD_USER_PREPARED = 'waiting'; // Demandé (mais demande pas encore envoyé à VEOLOG)
    public const STATE_CMD_USER_ASKED = 'ask'; // Demandé et envoyé à VEOLOG
    public const STATE_CMD_VEOLOG_ACCEPTED = 'accepted'; // Integré
    public const STATE_CMD_VEOLOG_REFUSED = 'refused'; // Abandonné
    public const STATE_CMD_VEOLOG_PREPARED = 'prepared'; // Préparé
    public const STATE_CMD_VEOLOG_SENT = 'sent'; // Expédié

    /* ================= */
    /* == BOX ========== */
    /* ================= */
    public const STATE_BOX_VEOLOG_ACCEPTED = 'accepted'; // Integré
    public const STATE_BOX_VEOLOG_REFUSED = 'refused'; // Abandonné
    public const STATE_BOX_VEOLOG_PREPARED = 'prepared'; // Préparé
    public const STATE_BOX_VEOLOG_SENT = 'sent'; // Expédié

    public const STATE_BOX_VEOLOG_STOCK = 'stock_in_veolog'; // En stock chez veolog
    public const STATE_BOX_USER_STOCK = 'member'; // Chez le user

    public const STATE_BOX_USER_ASK = 'user_ask'; // Rapatriment demandé par le user
    public const STATE_BOX_USER_SENT = 'user_sent'; // Envoyé par le user

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User|null
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id",  referencedColumnName="id")
     */
    protected $user;

    /**
     * name
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * Content
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content;

    /**
     * barcode
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $barcode;

    /**
     * @ORM\Column(type="array")
     */
    protected $trackers = [];

    /**
     * seal
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $seal;

    /**
     * @var BoxMedia
     * @ORM\OneToMany(targetEntity="App\Entity\MyBoxesBoxMedia", mappedBy="box", cascade={"all"})
     */
    protected $boxMedias;

    /**
     * @var BoxUser
     * @ORM\OneToMany(targetEntity="App\Entity\MyBoxesBoxUser", mappedBy="box", cascade={"all"})
     */
    protected $boxUsers;


    /**
     * @var MyBoxesBoxLog
     * @ORM\OneToMany(targetEntity="App\Entity\MyBoxesBoxLog", mappedBy="box")
     */
    protected $logs;

    /**
     * Cover
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="picture_id", referencedColumnName="id", onDelete="SET NULL")
     * @var Media
     */
    protected $picture;

    /**
     * Product (ie type of box)
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\MyBoxesProduct")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * @var MyBoxesProduct
     */
    protected $product;

    /**
     * State
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $state = "";

    /**
     * Date of current state
     *
     * @var datetime $stateDate
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $stateDate;

    protected $type = "";

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="is_empty", options={"default":0})
     */
    protected $isEmpty;

    public function __construct()
    {
        $this->boxMedias = new ArrayCollection();
        $this->boxUsers = new ArrayCollection();
        $this->trackers = [];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param mixed $barcode
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    }

    /**
     * @return mixed
     */
    public function getSeal()
    {
        return $this->seal;
    }

    /**
     * @param mixed $seal
     */
    public function setSeal($seal)
    {
        $this->seal = $seal;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return MyBoxesProduct
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param MyBoxesProduct $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * {@inheritdoc}
     */
    public function addBoxMedia(MyBoxesBoxMedia $boxMedia)
    {
        if ($this->hasBoxMedia($boxMedia)) {
            return;
        }

        $this->boxMedias->add($boxMedia);
        $boxMedia->setBox($this);
    }

    /**
     * {@inheritdoc}
     */
    public function removeBoxMedia(MyBoxesBoxMedia $boxMedia)
    {
        if ($this->hasBoxMedia($boxMedia)) {
            $this->boxMedias->removeElement($boxMedia);
            $boxMedia->setBox(null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBoxMedias()
    {
        return $this->boxMedias;
    }

    /**
     * {@inheritdoc}
     */
    public function clearBoxMedias()
    {
        foreach ($this->boxMedias as $bm) {
            $this->removeBoxMedia($bm);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasBoxMedia(MyBoxesBoxMedia $boxMedia)
    {
        return $this->boxMedias->contains($boxMedia);
    }

    /**
     * {@inheritdoc}
     */
    public function addBoxUser(MyBoxesBoxUser $boxUser)
    {
        if ($this->hasBoxUser($boxUser)) {
            return;
        }
        if (!$boxUser) {
            return;
        }
        $this->boxUsers->add($boxUser);
        $boxUser->setBox($this);
    }

    /**
     * {@inheritdoc}
     */
    public function addUser(?User $user)
    {
        if (is_null($user)) {
            return;
        }
        if ($this->hasUser($user)) {
            return;
        }
        if (!$user) {
            return;
        }
        $boxUser = new MyBoxesBoxUser();
        $this->boxUsers->add($boxUser);
        $boxUser->setBox($this);
        $boxUser->setUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function removeBoxUser(MyBoxesBoxUser $boxUser)
    {
        if ($this->hasBoxUser($boxUser)) {
            $this->boxUsers->removeElement($boxUser);
            $boxUser->setBox(null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBoxUsers()
    {
        return $this->boxUsers;
    }

    /**
     * {@inheritdoc}
     */
    public function clearBoxUsers()
    {
        foreach ($this->boxUsers as $bu) {
            $this->removeBoxUser($bu);
        }
    }

    /**
     * Remove all user empty.
     * {@inheritdoc}
     */
    public function cleanBoxUsers()
    {
        foreach ($this->boxUsers as $bu) {
            if ($bu == null) {
                $this->removeBoxUser($bu);
            } elseif ($bu->getUser() == null) {
                $this->boxUsers->removeElement($bu);
                $bu->setBox(null);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasBoxUser(MyBoxesBoxUser $boxUser)
    {
        return $this->hasUser($boxUser->getUser());
    }

    /**
     * {@inheritdoc}
     */
    public function hasUser(?User $user)
    {
        if (is_null($user)) {
            return false;
        }
        foreach ($this->boxUsers as $boxUser) {
            if ($boxUser->getUser() == $user) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getTrackers()
    {
        return !$this->trackers ? [] : $this->trackers;
    }

    /**
     * @param mixed $trackers
     */
    public function setTrackers($trackers)
    {
        $this->trackers = $trackers;
    }

    /**
     * @param mixed $tracker
     */
    public function addTracker($tracker)
    {
        if (!$this->trackers) {
            $this->trackers = [];
        }
        if (false === array_search($tracker, $this->trackers)) {
            $this->trackers[] = $tracker;
        }
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
        $this->stateDate = new \DateTime();
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return boolean
     */
    public function getIsEmpty()
    {
        return $this->isEmpty ? true : false;
    }

    /**
     * @param boolean $isEmpty
     */
    public function setIsEmpty($isEmpty)
    {
        $this->isEmpty = $isEmpty ? true : false;
    }

    /**
     * @return datetime
     */
    public function getStateDate()
    {
        return $this->stateDate;
    }
}
