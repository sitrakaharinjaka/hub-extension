<?php

/**
 * Repository
 *
 * @author Pierre Baumes <pierre.b@disko.fr>
 */

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class ToolboxTranslationRepository
 *
 * @package App\Repository
 */
class ToolboxTranslationRepository extends BaseRepository
{
}
