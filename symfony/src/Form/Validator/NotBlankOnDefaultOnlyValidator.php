<?php
namespace App\Form\Validator;

use App\Utils\Managers\LocaleManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class NotBlankOnDefaultOnlyValidator extends ConstraintValidator
{
    protected $localeManager;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(LocaleManager $localeManager)
    {
        $this->localeManager = $localeManager;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof NotBlankOnDefaultOnly) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\NotBlankOnDefaultOnly');
        }

        $defaultLocales = $this->localeManager->findDefaultSlug();
        $defaultLocale = array_shift($defaultLocales);

        if ($this->context->getObject()->getLocale() != $defaultLocale) {
            return;
        }

        if (false === $value || (empty($value) && '0' != $value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(NotBlankOnDefaultOnly::IS_BLANK_ERROR)
                ->addViolation();
        }
    }
}
