<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin\MyBoxes;

use App\Entity\Toolbox;
use App\Entity\ToolboxTranslation;
use App\Form\Model\SearchToolbox;
use App\Form\Type\Toolbox\ToolboxTranslationType;
use App\Form\Type\Toolbox\SearchToolboxType;
use App\Form\Type\Toolbox\ToolboxType;
use App\Utils\Managers\ToolboxManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class ToolboxController
 * @Route("/admin/toolboxs")
 * @package Disko\AdminBundle\Controller
 */
class ToolboxController extends BaseController
{
    /**
     * Toolbox's list
     * @Route("/list/{askLocale}", name="admin_toolbox_list", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listAction(Request $request, ToolboxManager $toolboxManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchToolboxType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des toolboxs');

        // Generate query
        $query = $toolboxManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/Toolbox/index.html.twig', array(
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchToolbox();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setTitle((isset($filters['title']))   ? $filters['title'] : '');

        return $data;
    }

    /**
     * Create a toolbox
     * @Route("/create", name="admin_toolbox_create")
     */
    public function createAction(Breadcrumbs $breadcrumbs, Request $request, ToolboxManager $toolboxManager): Response
    {
        // Breadcrumb
        $breadcrumbs->addItem("Tableau de bord", $this->generateUrl('admin_homepage'));
        $breadcrumbs->addItem("Liste des toolboxs", $this->generateUrl('admin_toolbox_list'));
        $breadcrumbs->addItem("Création");

        // Init form
        $toolbox = new Toolbox();
        $form = $this->createForm(ToolboxType::class, $toolbox, [
            'validation_groups' => ['Default']
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {
            // Bind value with form
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                if (count($toolbox->getTranslations()) > 0) {
                    $toolboxManager->save($toolbox);

                    // Launch the message flash
                    $this->addFlash(
                        'notice',
                        "Création terminée"
                    );

                    return $this->redirectToRoute('admin_toolbox_edit', [
                        'id' => $toolbox->getId()
                    ]);
                } else {
                    // Launch the message flash
                    $this->addFlash(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            } else {
                // Launch the message flash
                $this->addFlash(
                    'error',
                    "Le formulaire contient des erreurs ou est vide"
                );
            }
        }

        // View
        return $this->render('admin/Toolbox/create.html.twig', [
            'form'           => $form->createView(),
            'toolbox'           => $toolbox,
        ]);
    }

    /**
     * Update a toolbox
     * @Route("/edit/{id}", name="admin_toolbox_edit")
     * @param Request $request Request
     * @param integer $id      Id of toolbox
     *
     * @return Response
     */
    public function editAction(
        Breadcrumbs $breadcrumbs,
        Request $request,
        ToolboxManager $toolboxManager,
        LocaleManager $localeManager,
        $id
    ) {
        // Find it
        /** @var Toolbox $toolbox */
        $toolbox = $toolboxManager->findOneToEdit($id);
        if (!$toolbox) {
            throw $this->createNotFoundException('Toolbox not found');
        }

        // Breadcrumb
        $breadcrumbs->addItem("Tableau de bord", $this->generateUrl('admin_homepage'));
        $breadcrumbs->addItem("Liste des toolboxs", $this->generateUrl('admin_toolbox_list'));
        $breadcrumbs->addItem("Modification");

        $form = $this->createForm(ToolboxType::class, $toolbox, [
            'validation_groups' => ['Default']
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($toolbox->getTranslations()) > 0) {
                    $toolboxManager->save($toolbox);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Mise à jour effectuée"
                    );

                    return $this->redirect($request->headers->get('referer'));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/Toolbox/edit.html.twig',
            array(
                'form'           => $form->createView(),
                'toolbox'           => $toolbox,
            )
        );
    }

    /**
     * Delete one toolbox
     * @Route("/delete/{id}", name="admin_toolbox_delete")
     * @param Request $request Request
     * @param integer $id      Id of toolbox
     */
    public function deleteAction(Request $request, ToolboxManager $toolboxManager, $id)
    {
        // Find it
        /** @var Toolbox $toolbox */
        $toolbox = $toolboxManager->findOneToEdit($id);
        if (!$toolbox) {
            throw $this->createNotFoundException('Toolbox not found');
        }

        $toolboxManager->remove($toolbox);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été supprimé"
        );

        return $this->redirect($request->headers->get('referer'));
    }



    /**
     * Duplicate one toolbox
     * @Route("/duplicate/{id}", name="admin_toolbox_duplicate")
     * @param Request $request Request
     * @param integer $id      Id of toolbox
     */
    public function duplicateAction(Request $request, ToolboxManager $toolboxManager, $id)
    {
        // Find it
        /** @var Toolbox $toolbox */
        $toolbox = $toolboxManager->findOneToEdit($id);
        if (!$toolbox) {
            throw $this->createNotFoundException('Toolbox not found');
        }

        $this->getDoctrine()->getManager()->detach($toolbox);
        foreach ($toolbox->getTranslations() as $translation) {
            $translation->setId(null);
            $translation->setSlug(null);
            $this->getDoctrine()->getManager()->detach($translation);
        }
        $toolboxManager->save($toolbox);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été dupliqué"
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Export csv
     * @Route("/export/{locale}", name="admin_toolbox_export")
     * @param Request $request
     */
    public function exportAction(Request $request, ToolboxManager $toolboxManager, $locale)
    {
        $data = $this->initSearch($request);
        $query = $toolboxManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Titre',
            'Date de création',

        ], ';');

        $locale = $request->get('locale', $request->getDefaultLocale());
        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->translate($locale)->getTitle(),
                ($result->translate($locale)->getCreated()) ? $result->translate($locale)->getCreated()->format('d/m/Y') : $result->getCreated()->format('d/m/Y'),

            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_'.$locale.'_toolboxs_export.csv"'
        ));
    }
}
