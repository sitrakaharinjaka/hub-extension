<?php

/**
 * Repository
 *
 * @author Pierre Baumes <pierre.b@disko.fr>
 */

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class MenuItemTranslationRepository
 *
 * @package App\Repository
 */
class MenuItemTranslationRepository extends BaseRepository
{
}
