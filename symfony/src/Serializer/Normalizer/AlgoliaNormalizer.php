<?php


namespace App\Serializer\Normalizer;

use App\Entity\Example;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;

class AlgoliaNormalizer implements NormalizerInterface
{
    protected $fileSystem;
    /**
     * Constructor inject fileSystem
     *
     */
    public function setFileSystem($fileSystem)
    {
        $this->fileSystem = $fileSystem->get('current');
    }

    /**
     * Normalize a user into a set of arrays/scalars.
     */
    public function normalize($object, $format = null, array $context = array())
    {
        $context = array_merge($context, ['fs' => $this->fileSystem]);
        return $object->normalize($this, $format, $context);
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof NormalizableInterface;
    }
}
