<?php

namespace App\Listener;

use App\Entity\IP;
use App\Utils\Managers\IPManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Subscriber on kernelRequest
 * Checks if user IP is authorized
 */
class ClientIpListener implements EventSubscriberInterface
{

    /**
     * @var IPManager
     */
    private $ipManager;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    private $clientIp;

    /**
     * Constructor
     */
    public function __construct(
        IPManager $ipManager,
        RouterInterface $router,
        SessionInterface $session,
        TokenStorageInterface $token,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->ipManager = $ipManager;
        $this->router = $router;
        $this->session = $session;
        $this->token = $token;
        $this->authorizationChecker = $authorizationChecker;
    }

    private function isAuthenticatedByIP($request)
    {
        if ($request->server->get('REMOTE_ADDR')) {
            $clientIp = $request->server->get('REMOTE_ADDR');
        } elseif ($request->server->get('X_FORWARDED_FOR')) {
            $clientIp = $request->server->get('X_FORWARDED_FOR');
        } else {
            throw new AccessDeniedException('Your IP address could not be defined; access denied.');
        }

        $haveAccess = $this->ipManager->haveAccess($clientIp);

        $this->clientIp = $clientIp;

        return $haveAccess;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if (
            (preg_match('/\/admin/', $event->getRequest()->getRequestUri()) == 1) ||
            ($this->token->getToken() !== null && ($this->authorizationChecker->isGranted('ROLE_ADMIN') || $this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN')))
           ) {
            if (!$this->isAuthenticatedByIP($request)) {
                if ($this->token->getToken()->isAuthenticated() == true) {
                    $this->token->setToken(null);
                    $event->getRequest()->getSession()->invalidate();
                }

                $this->session->getFlashBag()->add(
                    'restrictip',
                    "Votre adresse IP <b>".$this->clientIp."</b> est pas autorisée"
                );
                $response = new RedirectResponse($this->router->generate('fos_user_security_login'));
                $event->setResponse($response);
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            //'kernel.request' => 'onKernelRequest',
            KernelEvents::REQUEST => array(
                array('onKernelRequest', 4),
            ),
        );
    }
}
