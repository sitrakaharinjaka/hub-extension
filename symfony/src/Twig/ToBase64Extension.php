<?php


namespace App\Twig;

use Twig_Extension;

class ToBase64Extension extends Twig_Extension
{
    public function setContainer($container)
    {
        $this->container = $container;
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('toBase64', [$this, 'toBase64']),
            new \Twig_SimpleFilter('md5', [$this, 'md5']),
            new \Twig_SimpleFilter('preImagine', [$this, 'preImagine']),
        ];
    }

    public static function preImagine($url)
    {
        $exp = explode("http", $url);
        if (count($exp) == 1) {
            return $url;
        }

        $exp = explode("uploads/", $url);
        if (count($exp) > 1) {
            return $exp[1];
        }

        return $url;
    }

    public function toBase64($url)
    {
        $imageData = base64_encode(file_get_contents($url));
        $imageData = 'data:image/jpeg;base64,'.$imageData;
        return $imageData;
    }

    public function md5($string)
    {
        return md5($string);
    }

    public function getName()
    {
        return 'tobase64_extension';
    }
}
