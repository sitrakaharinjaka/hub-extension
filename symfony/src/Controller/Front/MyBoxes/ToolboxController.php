<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Front\MyBoxes;

use App\Controller\BaseController;
use App\Entity\MyBoxesBox;
use App\Entity\MyBoxesCommand;
use App\Entity\Project;
use App\Form\Type\MyBoxes\CommandType;
use App\Utils\Managers\MyBoxesBoxManager;
use App\Utils\Managers\MyBoxesCommandManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockManager;
use App\Utils\Managers\ToolboxManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndexController
 *
 * @Route("/myboxes")
 *
 * @package App\Controller
 */
class ToolboxController extends BaseController
{
    /**
     * Homepage
     *
     * @Route("/toolbox", name="myboxes_toolbox")
     * @return Response
     */
    public function index(Request $request, ToolboxManager $toolboxManager)
    {
        $toolboxes = $toolboxManager->findAll();

        return $this->render('hub/myboxes/toolbox/index.html.twig', [
            'toolboxes' => $toolboxes,
        ]);
    }
}
