<?php

namespace App\Command\Import;

use App\Utils\Managers\ProductImportManager;
use App\Utils\Services\CsvToArray;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ImportProductCommand extends Command
{
    const FILENAME = 'importProduct.csv';

    /**
     * @var CsvToArray
     */
    private $csvToArray;

    /**
     * @var ProductImportManager
     */
    private $productImportManager;

    /**
     * @var string
     */
    protected static $defaultName = 'disko:import:product';

    /**
     * @var ParameterBagInterface
     */
    protected $parameterBag;

    /**
     * @var EntityManagerInterface $em
     */
    protected $em;

    /**
     * @var string
     */
    protected $importSourceFolder;

    /**
     * ImportProductCommand constructor.
     *
     * @param CsvToArray $csvToArray
     * @param ProductImportManager $productImportManager
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        CsvToArray $csvToArray,
        ProductImportManager $productImportManager,
        ParameterBagInterface $parameterBag,
        EntityManagerInterface $em
    ) {
        $this->csvToArray = $csvToArray;
        $this->productImportManager = $productImportManager;
        $this->parameterBag = $parameterBag;
        $this->importSourceFolder = $this->parameterBag->get('kernel.project_dir').'/imports/';
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('filename', InputArgument::OPTIONAL, 'The filename on the source folder');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $filePath = $input->getArgument('filename') ?? $this->importSourceFolder.self::FILENAME;

        $output->writeln("File : " . $filePath);
        if (file_exists($filePath)) {
            $data = $this->csvToArray->convert($filePath);
            $size = count($data);
            if ($size > 0) {
                $progress = new ProgressBar($output, $size);
                $progress->start();
                $errors = $this->productImportManager->importProducts($data, $progress, $output);
                $progress->finish();
                $output->writeln(" ===> INFO. Import finished.");
                if (sizeof($errors) > 0) {
                    $output->writeln('<error>ERREURS RENCONTREES</error>');
                    foreach ($errors as $error) {
                        $output->writeln(' ==> <error>ERREUR</error> : ' . $error);
                    }
                }
            } else {
                $output->writeln(" ===> <info>WARNING.</info> File is empty.");
            }
        } else {
            $output->writeln(" ===> <error>ERROR.</error> No file found for product import.");
        }
    }
}
