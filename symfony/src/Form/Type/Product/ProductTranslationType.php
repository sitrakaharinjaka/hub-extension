<?php

/**
 * Form type
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Type\Product;

use App\Entity\Brand;
use App\Entity\Category;
use App\Entity\Media;
use App\Entity\ProductTranslation;
use App\Form\Transformer\MediaTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\Fo;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

/**
 *  Form Type
 */
class ProductTranslationType extends AbstractType
{
    protected $em;
    private $security;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;
        $this->security = $security;
    }


    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            $builder->create('cover', FormType::class, [
                'required'     => false,
                'data_class'   => Media::class,
                'by_reference' => false,
            ])
                 ->add('id', TextType::class, ['required' => false])
                 ->addModelTransformer(new MediaTransformer($this->em))
        );

        $builder->add('imageGallery', CollectionType::class, [
            'entry_type'   => ImageGalleryType::class,
            'allow_add'    => true,
            'by_reference' => false,
            'allow_delete' => true,
            'required'     => false
        ]);

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $productTranslation = $event->getData();
            $form = $event->getForm();

            if ($productTranslation) {
                $origin = $productTranslation->getOrigin();
                $isEditMode = $productTranslation->getId()? true: false;

                $this->generateForm($form, $isEditMode, $isEditMode && $origin == 0? true: false);

                foreach($form->all() as $formChildName => $formChild){
                    $modelGetter = 'get' . ucfirst($formChildName);
                    if (method_exists($productTranslation, $modelGetter) && !$formChild->isSubmitted()) {
                        $formChild->setData($productTranslation->$modelGetter());
                    }
                }
            } else {
                $this->generateForm($form, false, false);
            }

        });


        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            $productTranslation = $event->getData();

            $form = $event->getForm();

            if ($productTranslation) {
                $origin = $productTranslation->getOrigin();
                $isEditMode = $productTranslation->getId()? true: false;

                $this->generateForm($form, $isEditMode, $isEditMode && $origin == 0? true: false);

                foreach($form->all() as $formChildName => $formChild){
                    $modelGetter = 'get' . ucfirst($formChildName);
                    if (method_exists($productTranslation, $modelGetter) && !$formChild->isSubmitted()) {
                        $formChild->setData($productTranslation->$modelGetter());
                    }
                }
            } else {
                $this->generateForm($form, false, false);
            }

        });
    }

    private function generateForm(FormInterface $form, bool $editMode, bool $isSap)
    {
        $options = [];

        $user = $this->security->getUser();

        if ($editMode) {
            if ($isSap) {
                $form->add('brand', EntityType::class, [
                    'class'         => Brand::class,
                    'query_builder' => function (EntityRepository $erb) {
                        return $erb->createQueryBuilder('u')
                                   ->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale')
                                   ->setParameter('locale', 'fr')
                                   ->orderBy('t.name', 'ASC');
                    },
                    "placeholder"   => "Sélectionner une marque",
                    'choice_label'  => 'name',
                    'multiple'      => false,
                    'required'      => false,
                    'disabled' => 'disabled'
                ]);

                $form->add('categories', EntityType::class, [
                    'class'         => Category::class,
                    'query_builder' => function (EntityRepository $erc) {
                        $user = $this->security->getUser();

                        $qb = $erc->createQueryBuilder('u')
                            ->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale')
                            ->setParameter('locale', 'fr')
                            ->orderBy('t.name', 'ASC');
    
                        if(!$user->hasRole('ROLE_SUPER_ADMIN') && !$user->isGeneralAdmin()){
                            $qb->andWhere("u.id NOT IN (:ids)")
                                ->setParameter('ids', [5]);
                        }
    
                        return $qb;
                    },
                    'choice_label'  => 'name',
                    'by_reference'  => false,
                    'multiple'      => true,
                    'empty_data'    => false,
                    'required'      => false,
                    'disabled' => 'disabled'
                ]);

                $options['required'] = false;
                $options['disabled'] = 'disabled';
                //$options['attr'] = ['readonly' => true];
                $options['attr'] = ['readonly' => false];

                $form->add('isFragile', CheckboxType::class, $options);
                $form->add('isExpendable', CheckboxType::class, $options);
                $form->add('isEffervescent', CheckboxType::class, $options);
                $form->add('crd', CheckboxType::class, $options);

                unset($options['disabled']);

                $form->add('brandOther', TextType::class, $options);

                $form->add('state', ChoiceType::class, [
                    'choices'  => ProductTranslation::LIST_STATES,
                    'disabled' => 'disabled',
                    'required' => false,
                ]);
            } else {
                $options['required'] = false;

                $form->add('state', ChoiceType::class, [
                    'choices'  => ProductTranslation::LIST_STATES,
                    'required' => false,
                    'empty_data' => ProductTranslation::STATE_NEW
                ]);
            }

            // $form->add('state', ChoiceType::class, [
            //     'choices'  => ProductTranslation::LIST_STATES,
            //     'disabled' => 'disabled',
            //     'required' => false,
            // ]);
        } else {
            $form->add('state', ChoiceType::class, [
                'choices' => ProductTranslation::LIST_STATES,
                'data' => ProductTranslation::STATE_NEW,
                'empty_data' => ProductTranslation::STATE_NEW
            ]);
            $options['required'] = false;
        }

        $roleOptions = [];

          if ($user->hasRole('ROLE_SUPER_ADMIN') || $user->isGeneralAdmin()) {
            $roleOptions = [
                'required' => false,
            ];
          }else{
            $roleOptions = [
                'required' => false, 
                'attr' => ['readonly' => true]
            ];
          }

        if (!$isSap) {
            $form->add('brand', EntityType::class, [
                'class'         => Brand::class,
                'query_builder' => function (EntityRepository $erb) {
                    return $erb->createQueryBuilder('u')
                               ->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale')
                               ->setParameter('locale', 'fr')
                               ->orderBy('t.name', 'ASC');
                },
                "placeholder"   => "Sélectionner une marque",
                'choice_label'  => 'name',
                'multiple'      => false,
                'required'      => true,
            ]);
            //$form->add('brandOther', TextType::class, $options);

            $form->add('categories', EntityType::class, [
                'class'         => Category::class,
                'query_builder' => function (EntityRepository $erc) {
                    $user = $this->security->getUser();

                    $qb = $erc->createQueryBuilder('u')
                        ->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale')
                        ->setParameter('locale', 'fr')
                        ->orderBy('t.name', 'ASC');

                    if(!$user->hasRole('ROLE_SUPER_ADMIN') && !$user->isGeneralAdmin()){
                        $qb->andWhere("u.id NOT IN (:ids)")
                            ->setParameter('ids', [5]);
                    }

                    return $qb;
                },
                'choice_label'  => 'name',
                'by_reference'  => false,
                'multiple'      => true,
                'empty_data'    => false,
                'required'      => true
            ]);

            $form->add('isFragile', CheckboxType::class, $options);
            $form->add('isExpendable', CheckboxType::class, $options);

            if ($user->hasRole('ROLE_SUPER_ADMIN') || $user->isGeneralAdmin()) {
                $form->add('isEffervescent', CheckboxType::class, $options);
                $form->add('crd', CheckboxType::class, $options);
            }else{
                $form->add('isEffervescent', CheckboxType::class, ['required' => false, 'disabled' => 'disabled']);
                $form->add('crd', CheckboxType::class, ['required' => false, 'disabled' => 'disabled']);
            }

        }

      

      if($user->getId() == 4 || $user->getId() == 1){
        $sapOptions = [
          'required' => false,
        ];
      }else{
        $sapOptions = [
          'required' => false,
          'disabled' => 'disabled',
          'attr' => ['readonly' => false]
        ];
      }
    

        $form->add('name', TextType::class, ($isSap) ? $sapOptions : []);
        $form->add('code', TextType::class, ($isSap) ? $sapOptions : ['required' => false, 'attr' => ['readonly' => true]]);
        $form->add('description', TextType::class, ($isSap) ? $sapOptions : []);
        $form->add('eanPart', TextType::class, ($isSap) ? $sapOptions : $options);
        $form->add('unitConditioning', TextType::class, ($isSap) ? $sapOptions : $options);
        $form->add('totalUnitsByBox', NumberType::class, ($isSap) ? $sapOptions : $options);
        $form->add('eanBox', NumberType::class, ($isSap) ? $sapOptions : $options);
        $form->add('referenceSAP', NumberType::class, ($isSap) ? $sapOptions : $options);
        $form->add('referenceProvider', TextType::class, ($isSap) ? $sapOptions : $options);
        $form->add('color', TextType::class, ($isSap) ? $sapOptions : $options);
        $form->add('material', TextType::class, ($isSap) ? $sapOptions : $options);
        $form->add('place', TextType::class, ($isSap) ? $sapOptions : $options);
        $form->add('valorization', TextType::class, ($isSap) ? $sapOptions : $options);
        $form->add('weight', NumberType::class, ($isSap) ? $sapOptions : $options);
        $form->add('width', NumberType::class, ($isSap) ? $sapOptions : $options);
        $form->add('height', NumberType::class, ($isSap) ? $sapOptions : $options);
        $form->add('depth', NumberType::class, ($isSap) ? $sapOptions : $options);
        $form->add('weightBox', NumberType::class, ($isSap) ? $sapOptions : $options);
        $form->add('widthBox', NumberType::class, ($isSap) ? $sapOptions : $options);
        $form->add('heightBox', NumberType::class, ($isSap) ? $sapOptions : $options);
        $form->add('depthBox', NumberType::class, ($isSap) ? $sapOptions : $options);
        $form->add('otherInformation', TextType::class, ($isSap) ? $sapOptions : ['required' => false, 'attr' => ['readonly' => true]]);
        $form->add('country', TextType::class, ($isSap) ? $sapOptions : $roleOptions);
        $form->add('litrageBottle', TextType::class, ($isSap) ? $sapOptions : $options);
        $form->add('designationBottle', TextType::class, ($isSap) ? $sapOptions : $roleOptions);
        $form->add('vintage', NumberType::class, ($isSap) ? $sapOptions : $options);

        $wineColorOptions = [
            'placeholder' => 'Sélectionner une couleur',
            'choices' => ProductTranslation::LIST_WINE_COLOR,
            'required' => false,
        ];

        if (!$user->hasRole('ROLE_SUPER_ADMIN') && !$user->isGeneralAdmin()) {
            $wineColorOptions['attr'] = ['disabled' => true];
        }

        $form->add('wineColor', ChoiceType::class, ($isSap) ? $wineColorOptions + $sapOptions : $wineColorOptions);

        $wineCategoryOptions = [
            'placeholder' => 'Sélectionner une catégorie',
            'choices' => ProductTranslation::LIST_WINE_CAT
        ];

        if (!$user->hasRole('ROLE_SUPER_ADMIN') && !$user->isGeneralAdmin()) {
            $wineCategoryOptions['attr'] = ['disabled' => true];
            $wineCategoryOptions['required'] = false;
        }

        $form->add('wineCategory', ChoiceType::class, ($isSap) ? $wineCategoryOptions + $sapOptions : $wineCategoryOptions);

        $form->add('degreeAlcohol', TextType::class, ($isSap) ? $sapOptions : $roleOptions);
        $form->add('customsNomenclature', NumberType::class, ($isSap) ? $sapOptions : $options);

        $originOptions = [
            'choices' => ProductTranslation::LIST_ORIGIN
        ];

        if (!$editMode) {
            $originOptions['data'] = 1;
        }

        //if (!$user->hasRole('ROLE_SUPER_ADMIN') && !$user->isGeneralAdmin()) {
            $originOptions['attr'] = ['disabled' => true];
        //}

        $form->add('origin', ChoiceType::class, ($isSap) ? $originOptions + $sapOptions : $originOptions);
        // $form->add('origin', ChoiceType::class, [
        //     'choices' => ProductTranslation::LIST_ORIGIN
        // ], $options);
    }

    /**
     * Configure Options
     *
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
            'data_class' => ProductTranslation::class
        ));
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'product';
    }
}
