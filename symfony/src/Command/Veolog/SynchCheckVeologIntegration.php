<?php

namespace App\Command\Veolog;

use App\Command\Veolog\AbstractVeologCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\Project;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use App\Utils\Managers\ProjectManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class SynchCheckVeologIntegration extends AbstractVeologCommand
{

  /** @var  EntityManager */
    private $projectManager;
    private $rootDir;

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, ProjectManager $projectManager, KernelInterface $kernel)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->projectManager = $projectManager;
        $this->rootDir = $kernel->getProjectDir();
    }

    protected static $defaultName = 'veolog:check-integration';

    protected function configure()
    {
        $this
            ->setDescription('Check if project are treating by veolog')
            ->setHelp('This command allows you to check if veolog are treating your project');
    }

    protected function executeCommand()
    {
        $this->log('info', 'Find projects to synch into veolog');
        
        // Die => Désactivation de la commande suite à une demande Veolog :
        // Veolog avait demandé en rapide de désactivé cette commande
        // Du coup pour être sûr que lbn ne fasse pas de bêtise on a mis un die comme ça même si ils avaient oublié de retirer la task cron le script ce jouait pas
        $this->log('info', '<comment>DESACTIVATION DU TRAITEMENT</comment>');
        return;
        
        /*Hack to reset state project for dev !!!!*/
        //for ($i = 1; $i <= 20; $i++) {
        //    /** @var Project $project */
        //    $project = $this->projectManager->findOneToEdit($i);
        //    if (! empty($project)) {
        //        //$project->setState('draft');
        //        //$this->projectManager->save($project);
        //    }
        //}
        $projects = $this->projectManager->findProjectbyState(Project::STATE_HUB_ACCEPTED_FIXED, Project::CAVE_PANTIN);
        $total_projects = count($projects);

        if ($total_projects > 0) {
            $this->log('info', $total_projects . ' projects ready for synch');

            foreach ($projects as $key => $project) {
                $datevlg = new DateTime($project->getDateAddVlg()->format('Y-m-d H:i:s'));
                $datevlg->modify("+10 minutes");
                $now = new DateTime();
                if ($datevlg < $now) {
                    if ($project->getState() == Project::STATE_HUB_ACCEPTED_FIXED) {
                        $contacts = $project->getStock()->translate('fr');
                        $mails    = [];
                        foreach ($contacts->getTranslatable()->getAdminUsers()->getValues() as $contact) {
                            $mails[] = $contact->getEmail();
                        }
                        $mails[] = $contacts->getTranslatable()->getReferentUser()->getEmail();
                        $mails[] = $project->getUser()->getEmail();
                        $emails = implode(",", $mails);
                        $project->setState(Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_CANCEL);
                        $this->projectManager->save($project);
                        $this->mailer->sendMailnoIntegrateByVeolog($project, $emails);
                    }
                    $this->log('info', $project->getName() . ' projects not integrate by veolog');
                }
            }
        } else {
            $this->log('info', 'no project found to cancel');
        }
    }
}
