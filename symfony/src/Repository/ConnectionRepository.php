<?php

/**
 * Repository
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Repository;

use App\Repository\BaseRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Class ConnectionRepository
 *
 */
class ConnectionRepository extends BaseRepository
{
    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function stats($start, $end)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('IFNULL(count(c.id), 0) as cpt, CONCAT(YEAR(c.date), \'/\' ,MONTH(c.date), \'/\' , DAY(c.date)) AS date, DAYOFYEAR(c.date) as dayofyear');
        $qb->andWhere("c.date IS NOT NULL");
        $qb->groupBy("dayofyear");
        $qb->addOrderBy('dayofyear', 'asc');

        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("c.date >= :start");
        $qb->andWhere("c.date <= :end");

        $results = $qb->getQuery()->getScalarResult();
        return $results;
    }
}
