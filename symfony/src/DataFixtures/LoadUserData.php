<?php

/**
 * Fixture
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Localisation;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class LoadUserData
 *
 * Load user's fixtures
 *
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class LoadUserData extends Fixture
{
    /** @var ObjectManager */
    private $manager;

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $user = $this->addUser(array(
            'username' => 'dev',
            'firstname'=> 'dev',
            'lastname' => 'dev',
            'email'    => 'dev@gmail.com',
            'password' => 'devdisko123',
            'role'     => 'ROLE_SUPER_ADMIN',
            'enabled'  => true
        ));
        $this->addReference('dev-user', $user);

//        $user = $this->addUser(array(
//            'username' => 'admin',
//            'firstname'=> 'admin',
//            'lastname' => 'admin'.'ing',
//            'email'    => 'admin@gmail.com',
//            'password' => 'admindisko123',
//            'role'     => 'ROLE_GENERAL_ADMIN',
//            'enabled'  => true
//        ));
//        $this->addReference('admin-user', $user);


//        $users = array('aba', 'lilian', 'stemhane', 'eliot', 'francine');
//
//        for ($i = 0; $i <= 0; $i++) {
//            foreach ($users as $user) {
//                $tmp = $user.$i;
//                $this->addUser(array(
//                    'username' => $tmp,
//                    'firstname'=> $tmp,
//                    'lastname' => $tmp.'ing',
//                    'email'    => $tmp.'@user.com',
//                    'password' => $tmp,
//                    'enabled'  => true,
//                    'role'     => 'ROLE_USER'
//                ));
//            }
//        }
    }

    public function addUser($settings)
    {
        $user = new User();
        $user->setActive(true);
        $user->setLocked(false);
        $user->setUsername($settings['username']);
        $user->setEmail($settings['email']);
        $user->setFirstName($settings['firstname']);
        $user->setLastName($settings['lastname']);
        $password = $this->encoder->encodePassword($user, $settings['password']);
        $user->setPassword($password);

        if (isset($settings['role'])) {
            $user->addRole($settings['role']);
        }

        $this->manager->persist($user);
        $this->manager->flush();

        $this->addReference('user-'.strtolower($settings['username']), $user);

        return $user;
    }
}
