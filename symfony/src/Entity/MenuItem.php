<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MenuItemRepository")
 * @ORM\Table(name="dk_menuitem")
 * @ORM\HasLifecycleCallbacks
 */
class MenuItem
{
    use GedmoTrait;
    use AuthorableTrait;
    use ORMBehaviors\Translatable\Translatable;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * Position
     *
     * @Gedmo\SortablePosition
     * @ORM\Column(type="integer")
     */
    protected $position = 0;

    /**
     * deepness
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $deepness = 1;

    /**
     * base
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $base;

    /**
     * deepness
     *
     * @Gedmo\SortableGroup
     * @ORM\Column(type="text", nullable=true)
     */
    protected $parent = 0;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getDeepness()
    {
        return $this->deepness;
    }

    /**
     * @param mixed $deepness
     */
    public function setDeepness($deepness)
    {
        $this->deepness = $deepness;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @param mixed $base
     */
    public function setBase($base): void
    {
        $this->base = $base;
    }
}
