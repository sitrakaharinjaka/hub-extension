<?php

/**
 * Repository
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Repository;

use Doctrine\ORM\Query;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

/**
 * Class UserRepository
 *
 */
class UserRepository extends BaseRepository implements UserLoaderInterface
{

    /**
     * Get all user query, using for pagination
     *
     * @param array<string, mixed> $filters
     * @return Query
     */
    public function queryForSearch(array $filters = []): Query
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u')
            ->orderBy('u.lastName', 'asc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                if ($key === 'stock') {
                    $qb->andWhere(':stock MEMBER OF u.stocks OR :stock MEMBER OF u.adminStocks')
                        ->setParameter('stock', $filter);
                } else {
                    $qb->andWhere('u.'.$key.' LIKE :'.$key);
                    $qb->setParameter($key, '%'.$filter.'%');
                }
            }
        }

        $qb->andWhere("u.email != :emaildev");
        $qb->setParameter("emaildev", "dev@gmail.com");

        return $qb->getQuery();
    }

    public function findLikeSearch($search, $noemail)
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u')
            ->andWhere('u.email LIKE :emailseach')
            ->setParameter('emailseach', '%'.$search.'%')
            ->orderBy('u.email', 'asc');


        $qb->andWhere("u.email != :emailnot");
        $qb->setParameter("emailnot", $noemail);

        $qb->andWhere("u.email != :emaildev");
        $qb->setParameter("emaildev", "dev@gmail.com");

        $qb->setMaxResults(15);

        return $qb->getQuery()->getResult();
    }

    /**
     * Count all
     */
    public function countAll($mode = "member")
    {
        $qb = $this->createQueryBuilder('e')
            ->select('COUNT(e.id) as cpt');

        if ($mode == "admin") {
            $qb->andWhere("e.roles LIKE '%ROLE_ADMIN%' OR e.roles LIKE '%ROLE_SUPER_ADMIN%'");
        } else {
            $qb->andWhere("e.roles NOT LIKE '%ROLE_ADMIN%' AND e.roles NOT LIKE '%ROLE_SUPER_ADMIN%' ");
        }

        $query = $qb->getQuery();

        return $query->getScalarResult()[0]['cpt'];
    }

    /**
     * @param string $username
     * @return mixed|null|\Symfony\Component\Security\Core\User\UserInterface
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }



    /**
     * Get one User by one email
     *
     * @param $email
     *
     * @return mixed
     */
    public function findOneByEmail($email)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('c');
        $qb->andWhere("c.email = :email");
        $qb->setParameter('email', $email);

        $query = $qb->getQuery();
        $result = $query->getOneOrNullResult();
        return $result;
    }


    /**
     * Get one User by one token
     *
     * @param $token
     *
     * @return mixed
     */
    public function findOneByToken($token)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('c');
        $qb->andWhere("c.confirmationToken = :token");
        $qb->setParameter('token', $token);
        $qb->andWhere("c.active = FALSE");
        $qb->andWhere("c.confirmationToken IS NOT NULL");

        $query = $qb->getQuery();
        $result = $query->getOneOrNullResult();
        return $result;
    }


    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsCreated($start, $end)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('IFNULL(count(c.id), 0) as cpt, CONCAT(YEAR(c.created), \'/\' ,MONTH(c.created), \'/\' , DAY(c.created)) AS date, DAYOFYEAR(c.created) as dayofyear');
        $qb->andWhere("c.created IS NOT NULL");
        $qb->groupBy("dayofyear");
        $qb->addOrderBy('dayofyear', 'asc');

        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("c.created >= :start");
        $qb->andWhere("c.created <= :end");


        $results = $qb->getQuery()->getScalarResult();
        return $results;
    }

    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsFirstConnect($start, $end)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('IFNULL(count(c.id), 0) as cpt, CONCAT(YEAR(c.lastConnectionFirst), \'/\' ,MONTH(c.lastConnectionFirst), \'/\' , DAY(c.lastConnectionFirst)) AS date, DAYOFYEAR(c.lastConnectionFirst) as dayofyear');
        $qb->andWhere("c.lastConnectionFirst IS NOT NULL");
        $qb->groupBy("dayofyear");
        $qb->addOrderBy('dayofyear', 'asc');

        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("c.lastConnectionFirst >= :start");
        $qb->andWhere("c.lastConnectionFirst <= :end");



        $results = $qb->getQuery()->getScalarResult();
        return $results;
    }


    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function topBox($limit = 10, $emailpart = "")
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('DISTINCT(c.email) as email, c.id, count(i.id) as cpt');
        $qb->leftJoin('c.boxebis', 'i');
        $qb->groupBy("c");
        $qb->setMaxResults($limit);
        $qb->andWhere('i.box IS NOT NULL');
        $qb->addOrderBy('cpt', 'desc');

        if (!empty($emailpart)) {
            $qb->andWhere('c.email LIKE :email');
            $qb->setParameter('email', '%'.$emailpart.'%');
        }


        $results = $qb->getQuery()->getScalarResult();

        foreach($results as &$result){
            $user = $this->findOneBy(['id' => $result['id']]);

            $sharedBoxes = 0;
            $singleBoxes = 0;

            foreach($user->getBoxebis() as $boxUser){
                if(is_object($boxUser->getBox())){
                    if(count($boxUser->getBox()->getBoxUsers()) > 1){
                        ++$sharedBoxes;
                    }else{
                        ++$singleBoxes;
                    }

                }
            }

            $result['sharedboxes'] = $sharedBoxes;
            $result['singleboxes'] = $singleBoxes;

        }

        return $results;
    }

    public function findAllExceptAdmins($term = null){
        $qb = $this->createQueryBuilder('u');

        $qb->select('u');

        $qb
        ->andWhere('u.roles NOT LIKE :role');

        if(!is_null($term)){
            $qb
            ->andWhere('CONCAT(u.firstName, \' \', u.lastName) LIKE :term')
            ->setParameter('term', '%' . $term . '%');
        }

        $qb
        ->setParameter('role', '%ROLE_SUPER_ADMIN%')
        ->orderBy('u.lastName', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function findByIds($ids = [])
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('c');

        if (empty($ids)) {
            $ids = [0];
        }
        $qb->add('where', $qb->expr()->in('c.id', $ids));

        $query = $qb->getQuery();

        return $query->getResult();
    }
}
