<?php

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;

use App\Utils\Managers\LocaleManager;
use App\Entity\Service;
use App\Entity\ServiceTranslation;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class ServiceManager
 * `
 * Object manager of service
 *
 * @package Disko\PageBundle\Managers
 */
class ServiceManager extends BaseManager
{

    /**
     * Repository
     *
     * @var ServiceRepository
     */
    protected $serviceRepository;

    /**
     * ServiceManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('serviceRepository', Service::class);
    }

    /**
     * Save a service
     *
     * @param Service $service
     */
    public function save(Service $service)
    {
        $service->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($service);
        $this->getEntityManager()->flush();

        return $service;
    }

    /**
     * Remove one service
     *
     * @param Service $service
     */
    public function remove(Service $service)
    {
        $this->getEntityManager()->remove($service);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one Active
     *
     * @param $slugWl
     * @param $locale
     * @return mixed
     */
    public function findOneActive($slugWl, $locale)
    {
        $service = $this->serviceRepository->findOneActive($slugWl, $locale);

        return $service;
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $service = $this->serviceRepository->findOneToEdit($id);

        return $service;
    }

    /**
     * Get all service
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->serviceRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->serviceRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->serviceRepository->findOneBy($filters);
    }
}
