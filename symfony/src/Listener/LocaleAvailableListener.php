<?php

namespace App\Listener;

use App\Utils\Managers\LocaleManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Subscriber on kernelRequest
 */
class LocaleAvailableListener implements EventSubscriberInterface
{

    /**
     * @var LocaleManager
     */
    private $localeManager;

    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * Constructor
     */
    public function __construct(
        LocaleManager $localeManager,
        RouterInterface $router
    ) {
        $this->localeManager = $localeManager;
        $this->router = $router;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        $locales = $this->localeManager->findAllSlug();


        if (!in_array($request->getLocale(), $locales)) {
            throw new NotFoundHttpException("Locale ".$request->getLocale()." not found");
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST => array(
                array('onKernelRequest', 5),
            ),
        );
    }
}
