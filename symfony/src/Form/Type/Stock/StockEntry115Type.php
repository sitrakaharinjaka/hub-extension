<?php

/**
 * Form type
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Type\Stock;

use App\Entity\BillingCenter;
use App\Form\Model\StockEntry115Model;
use App\Form\Model\StockEntryModel;
use App\Utils\Managers\LocaleManager;
use App\Repository\StockRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\Fo;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 *  Form Type
 */
class StockEntry115Type extends AbstractType
{
    protected $localeManager;

    /**
     * @param LocaleManager $localeManager
     */
    public function __construct(LocaleManager $localeManager, SessionInterface $session)
    {
        $this->localeManager = $localeManager;
        $this->session       = $session;
    }

    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class, array(
            'label'    => 'Demandeur*',
            'required' => true,
            'attr'     => array(
                'class' => 'field'
            )
        ));
        $builder ->add('pickUpLocation', ChoiceType::class, [
            'label' => 'Lieu de ramassage',
            'choices' => [
                'Quai de déchargement du 115 (Service courrier)' => '115',
                'Etage du 142'                                   => '142',
            ],
            'placeholder' => false,
            'attr' => [
                'class' => 'custom-select',
                'id'    => 'stock_entry115_pickUpLocation'
            ],
        ]);
        $builder->add('floor', TextType::class, array(
            'label'    => 'Etage*',
            'required' => false,
            'attr'     => array(
                'class' => 'field'
            )
        ));
        $builder->add('officeNumber', TextType::class, array(
            'label'    => 'N° du bureau*',
            'required' => false,
            'attr'     => array(
                'class' => 'field'
            )
        ));
        $builder->add('email', TextType::class, array(
            'required' => true,
            'attr'     => array(
                'class' => 'field',
                'id'    => 'field-mail'
            )
        ));
        $builder->add('phone', TextType::class, array(
            'required' => true,
            'attr'     => array(
                'class' => 'field',
                'id'    => 'field-phone'
            )
        ));
        $builder->add('billingCenter', EntityType::class, array(
            'required'     => true,
            'class'        => BillingCenter::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('b')
                    ->leftJoin('b.translations', 't', 'WITH', "t.locale = 'fr'")
                    ->leftJoin('b.stocks', 's')
                    ->andWhere('s.id = :stockId')
                    ->setParameter('stockId', $this->session->get('current_stock', null)->getId());
            },
            'choice_label' => 'name',
            'attr'         => array(
                'class' => 'custom-select',
                'id'    => 'select-centre'
            )
        ));
        $builder->add('pickUpDate', DateType::class, array(
            'widget' => 'single_text',
            'label' => 'Date de ramassage demandée',
            'html5' => false,
            'format' => 'dd/MM/yyyy',
            'required'  => true,
            'attr' => [
                'class' => 'field datepicker jsDateStart no-weekend',
                'id'    => 'field-date',
                'autocomplete' => 'off'
            ],
            'form_date' => true
        ));
        $builder->add('timeSlots', ChoiceType::class, array(
            'required' => true,
            'choices'  => array(
                'Matin : 9H - 12H' => 'matin',
                'Après-midi (vendredi avant 16H) : 14H- 17H' => 'après-midi',
            ),
            'attr'     => array(
                'class' => 'custom-select',
                'id'    => 'select-livraison'
            )
        ));
        $builder->add('stockEntry115Items', CollectionType::class, array(
            'entry_type'    => StockEntry115ItemType::class,
            'entry_options' => ['label' => false],
            'allow_delete'  => true,
            'allow_add'     => true,
            'required'      => true,
            'by_reference'  => false,
            'label'         => false,
            'error_bubbling' => false,
        ));
    }

    /**
     * Configure Options
     *
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => StockEntry115Model::class,
            'stocks_query_builder' => function (StockRepository $sr) {
                return $sr->createQueryBuilder('s')
                    ->leftJoin('s.translations', 't', 'WITH', 't.locale = :locale')
                    ->setParameter('locale', 'fr')
                    ->orderBy('t.name', 'ASC');
            },
        ));
    }
}
