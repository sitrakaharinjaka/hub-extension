<?php
/**
 * Created by PhpStorm.
 * User: Luk
 * Date: 20/05/2017
 * Time: 20:35
 */

namespace App\Form\Type\Project;

use App\Entity\Project;
use App\Form\Model\ProjectSwitch;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectSwitchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];
        $stock = $options['stock'];
        $builder->add('project', EntityType::class, [
            'required' => true,
            'class' => Project::class,
            'placeholder' => 'Voir le catalogue sans commande',
            'query_builder' => function (EntityRepository $repository) use ($user, $stock) {
                return $repository->findProjectAvailableSwitch($user, $stock)->select('p');
            },
            'choice_label' => 'name',
            'label' => false,
            'attr' => [
                'class' => 'custom-select-simple'
            ]
        ]);
    }

    /**
     * Configure Options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ProjectSwitch::class,
            'stock' => null,
            'user' => null
        ));
    }
}
