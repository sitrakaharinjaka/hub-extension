<?php

namespace App\Command;

use App\Command\AbstractCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\Delivery;
use App\Entity\Project;
use App\Event\ProjectEvent;
use App\Utils\Managers\DeliveryManager;
use App\Utils\Managers\ProjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

class DeliveryLoadCommand extends AbstractCommand
{
    protected static $defaultName = 'disko:delivery:load';


    private $deliveryManager;

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, DeliveryManager $deliveryManager)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->deliveryManager = $deliveryManager;
    }


    protected function configure()
    {
        $this
            ->setDescription('Load all config for delivery')
        ;
    }

    /**
     * @return int|void|null
     * @throws \Exception
     */
    protected function executeCommand()
    {
        $this->deliveryManager->removeAll();

        $strangerProject = [
            ['AD','48', 'DHL'],
            ['AE','48', 'DHL'],
            ['AF','96', 'DHL'],
            ['AG','96', 'DHL'],
            ['AI','96', 'DHL'],
            ['AL','48', 'DHL'],
            ['AM','72', 'DHL'],
            ['AO','96', 'DHL'],
            ['AR','72', 'DHL'],
            ['AS','120', 'DHL'],
            ['AT','24', 'DHL'],
            ['AU','96', 'DHL'],
            ['AW','96', 'DHL'],
            ['AZ','72', 'DHL'],
            ['BA','48', 'DHL'],
            ['BB','72', 'DHL'],
            ['BD','72', 'DHL'],
            ['BE','24', 'DHL'],
            ['BF','96', 'DHL'],
            ['BG','24', 'DHL'],
            ['BH','72', 'DHL'],
            ['BI','96', 'DHL'],
            ['BJ','72', 'DHL'],
            ['BL','96', 'DHL'],
            ['BM','72', 'DHL'],
            ['BN','96', 'DHL'],
            ['BO','96', 'DHL'],
            ['BQ','96', 'DHL'],
            ['BR','48', 'DHL'],
            ['BS','72', 'DHL'],
            ['BT','120', 'DHL'],
            ['BW','72', 'DHL'],
            ['BY','72', 'DHL'],
            ['BZ','72', 'DHL'],
            ['CA','48', 'DHL'],
            ['CD','72', 'DHL'],
            ['CF','96', 'DHL'],
            ['CG','72', 'DHL'],
            ['CH','24', 'DHL'],
            ['CI','72', 'DHL'],
            ['CK','120', 'DHL'],
            ['CL','72', 'DHL'],
            ['CM','72', 'DHL'],
            ['CN','48', 'DHL'],
            ['CO','72', 'DHL'],
            ['CR','72', 'DHL'],
            ['CU','96', 'DHL'],
            ['CV','96', 'DHL'],
            ['CW','72', 'DHL'],
            ['CY','48', 'DHL'],
            ['CZ','24', 'DHL'],
            ['DE','24', 'DHL'],
            ['DJ','96', 'DHL'],
            ['DK','24', 'DHL'],
            ['DM','96', 'DHL'],
            ['DO','96', 'DHL'],
            ['DZ','48', 'DHL'],
            ['EC','72', 'DHL'],
            ['EE','24', 'DHL'],
            ['EG','48', 'DHL'],
            ['ER','72', 'DHL'],
            ['ES','24', 'DHL'],
            ['ES-CN','72', 'DHL'],
            ['ET','72', 'DHL'],
            ['FI','24', 'DHL'],
            ['FJ','120', 'DHL'],
            ['FK','120', 'DHL'],
            ['FM','120', 'DHL'],
            ['FO','96', 'DHL'],
            ['FR','24', 'DHL'],
            ['GA','72', 'DHL'],
            ['GB','24', 'DHL'],
            ['GD','96', 'DHL'],
            ['GE','72', 'DHL'],
            ['GF','72', 'DHL'],
            ['GG','48', 'DHL'],
            ['GH','72', 'DHL'],
            ['GI','72', 'DHL'],
            ['GL','120', 'DHL'],
            ['GM','72', 'DHL'],
            ['GN','72', 'DHL'],
            ['GP','72', 'DHL'],
            ['GQ','96', 'DHL'],
            ['GR','24', 'DHL'],
            ['GRM','48', 'DHL'],
            ['GT','72', 'DHL'],
            ['GU','96', 'DHL'],
            ['GW','96', 'DHL'],
            ['GY','96', 'DHL'],
            ['HK','48', 'DHL'],
            ['HN','72', 'DHL'],
            ['HR','24', 'DHL'],
            ['HT','72', 'DHL'],
            ['HU','24', 'DHL'],
            ['ID','72', 'DHL'],
            ['IE','24', 'DHL'],
            ['IL','48', 'DHL'],
            ['IM','48', 'DHL'],
            ['IN','72', 'DHL'],
            ['IQ','96', 'DHL'],
            ['IR','96', 'DHL'],
            ['IS','48', 'DHL'],
            ['IT','24', 'DHL'],
            ['JE','48', 'DHL'],
            ['JM','72', 'DHL'],
            ['JO','72', 'DHL'],
            ['JP','72', 'DHL'],
            ['KE','72', 'DHL'],
            ['KG','72', 'DHL'],
            ['KH','72', 'DHL'],
            ['KI','120', 'DHL'],
            ['KM','144', 'DHL'],
            ['KN','96', 'DHL'],
            ['KP','72', 'DHL'],
            ['KR','96', 'DHL'],
            ['KW','72', 'DHL'],
            ['KY','72', 'DHL'],
            ['KZ','72', 'DHL'],
            ['LA','96', 'DHL'],
            ['LB','48', 'DHL'],
            ['LC','96', 'DHL'],
            ['LI','24', 'DHL'],
            ['LK','72', 'DHL'],
            ['LR','72', 'DHL'],
            ['LS','72', 'DHL'],
            ['LT','24', 'DHL'],
            ['LU','24', 'DHL'],
            ['LV','24', 'DHL'],
            ['LY','72', 'DHL'],
            ['MA','48', 'DHL'],
            ['MD','72', 'DHL'],
            ['ME','72', 'DHL'],
            ['MG','96', 'DHL'],
            ['MH','96', 'DHL'],
            ['MK','48', 'DHL'],
            ['ML','48', 'DHL'],
            ['MM','72', 'DHL'],
            ['MN','96', 'DHL'],
            ['MO','72', 'DHL'],
            ['MP','96', 'DHL'],
            ['MQ','72', 'DHL'],
            ['MR','72', 'DHL'],
            ['MS','96', 'DHL'],
            ['MT','48', 'DHL'],
            ['MU','72', 'DHL'],
            ['MV','96', 'DHL'],
            ['MW','72', 'DHL'],
            ['MX','48', 'DHL'],
            ['MY','72', 'DHL'],
            ['MZ','72', 'DHL'],
            ['NA','72', 'DHL'],
            ['NC','120', 'DHL'],
            ['NE','96', 'DHL'],
            ['NG','72', 'DHL'],
            ['NI','72', 'DHL'],
            ['NL','24', 'DHL'],
            ['NO','48', 'DHL'],
            ['NP','96', 'DHL'],
            ['NR','120', 'DHL'],
            ['NU','120', 'DHL'],
            ['NZ','96', 'DHL'],
            ['OM','72', 'DHL'],
            ['PA','48', 'DHL'],
            ['PE','72', 'DHL'],
            ['PF','96', 'DHL'],
            ['PG','120', 'DHL'],
            ['PH','72', 'DHL'],
            ['PK','72', 'DHL'],
            ['PL','24', 'DHL'],
            ['PM','120', 'DHL'],
            ['PR','72', 'DHL'],
            ['PT','24', 'DHL'],
            ['PT-20','48', 'DHL'],
            ['PT-30','48', 'DHL'],
            ['PW','120', 'DHL'],
            ['PY','72', 'DHL'],
            ['QA','72', 'DHL'],
            ['RE','72', 'DHL'],
            ['RO','24', 'DHL'],
            ['RS','48', 'DHL'],
            ['RU','72', 'DHL'],
            ['RW','72', 'DHL'],
            ['SA','72', 'DHL'],
            ['SB','120', 'DHL'],
            ['SC','72', 'DHL'],
            ['SD','72', 'DHL'],
            ['SE','24', 'DHL'],
            ['SG','48', 'DHL'],
            ['SH','120', 'DHL'],
            ['SI','24', 'DHL'],
            ['SK','24', 'DHL'],
            ['SL','72', 'DHL'],
            ['SM','48', 'DHL'],
            ['SM/SX','96', 'DHL'],
            ['SN','72', 'DHL'],
            ['SO','96', 'DHL'],
            ['SR','96', 'DHL'],
            ['SS','72', 'DHL'],
            ['ST','96', 'DHL'],
            ['SV','72', 'DHL'],
            ['SY','72', 'DHL'],
            ['SZ','72', 'DHL'],
            ['TC','72', 'DHL'],
            ['TD','72', 'DHL'],
            ['TG','48', 'DHL'],
            ['TH','72', 'DHL'],
            ['TJ','96', 'DHL'],
            ['TL','120', 'DHL'],
            ['TN','72', 'DHL'],
            ['TO','120', 'DHL'],
            ['TR','48', 'DHL'],
            ['TT','72', 'DHL'],
            ['TV','120', 'DHL'],
            ['TW','72', 'DHL'],
            ['TZ','72', 'DHL'],
            ['UA','72', 'DHL'],
            ['UG','72', 'DHL'],
            ['US','48', 'DHL'],
            ['UY','72', 'DHL'],
            ['UZ','72', 'DHL'],
            ['VA','24', 'DHL'],
            ['VC','96', 'DHL'],
            ['VE','96', 'DHL'],
            ['VG','96', 'DHL'],
            ['VN','72', 'DHL'],
            ['VU','144', 'DHL'],
            ['WF','144', 'DHL'],
            ['XK','48', 'DHL'],
            ['YE','96', 'DHL'],
            ['YT','96', 'DHL'],
            ['ZA','72', 'DHL'],
            ['ZM','96', 'DHL'],
            ['ZM','96', 'DHL'],
            ['MC','24', 'DHL'],
        ];


        $urgencyProject = [
            ['75', '4', 'Top Chrono'],
            ['92', '4', 'Top Chrono'],
            ['93', '4', 'Top Chrono'],
            ['94', '4', 'Top Chrono'],
            ['75', '4', 'Top Chrono']
        ];

        $basicProject = [
            ['20','48','Chronopost'],
            //['2B','48','Chronopost'],
            ['95','24','Top Chrono'],
            ['94','24','Top Chrono'],
            ['93','24','Top Chrono'],
            ['92','24','Top Chrono'],
            ['91','24','Top Chrono'],
            ['90','24','Chronopost'],
            ['89','24','Chronopost'],
            ['88','24','Chronopost'],
            ['87','24','Chronopost'],
            ['86','24','Chronopost'],
            ['85','24','Chronopost'],
            ['84','24','Chronopost'],
            ['83','24','Chronopost'],
            ['82','24','Chronopost'],
            ['81','24','Chronopost'],
            ['80','24','Chronopost'],
            ['79','24','Chronopost'],
            ['78','24','Top Chrono'],
            ['77','24','Top Chrono'],
            ['76','24','Chronopost'],
            ['75','24','Top Chrono'],
            ['74','24','Chronopost'],
            ['73','24','Chronopost'],
            ['72','24','Chronopost'],
            ['71','24','Chronopost'],
            ['70','24','Chronopost'],
            ['69','24','Chronopost'],
            ['68','24','Chronopost'],
            ['67','24','Chronopost'],
            ['66','24','Chronopost'],
            ['65','24','Chronopost'],
            ['64','24','Chronopost'],
            ['63','24','Chronopost'],
            ['62','24','Chronopost'],
            ['61','24','Chronopost'],
            ['60','24','Chronopost'],
            ['59','24','Chronopost'],
            ['58','24','Chronopost'],
            ['57','24','Chronopost'],
            ['56','24','Chronopost'],
            ['55','24','Chronopost'],
            ['54','24','Chronopost'],
            ['53','24','Chronopost'],
            ['52','24','Chronopost'],
            ['51','24','Chronopost'],
            ['50','24','Chronopost'],
            ['49','24','Chronopost'],
            ['48','24','Chronopost'],
            ['47','24','Chronopost'],
            ['46','24','Chronopost'],
            ['45','24','Chronopost'],
            ['44','24','Chronopost'],
            ['43','24','Chronopost'],
            ['42','24','Chronopost'],
            ['41','24','Chronopost'],
            ['40','24','Chronopost'],
            ['39','24','Chronopost'],
            ['38','24','Chronopost'],
            ['37','24','Chronopost'],
            ['36','24','Chronopost'],
            ['35','24','Chronopost'],
            ['34','24','Chronopost'],
            ['33','24','Chronopost'],
            ['32','24','Chronopost'],
            ['31','24','Chronopost'],
            ['30','24','Chronopost'],
            ['29','24','Chronopost'],
            ['28','24','Chronopost'],
            ['27','24','Chronopost'],
            ['26','24','Chronopost'],
            ['25','24','Chronopost'],
            ['24','24','Chronopost'],
            ['23','24','Chronopost'],
            ['22','24','Chronopost'],
            ['21','24','Chronopost'],
            ['19','24','Chronopost'],
            ['18','24','Chronopost'],
            ['17','24','Chronopost'],
            ['16','24','Chronopost'],
            ['15','24','Chronopost'],
            ['14','24','Chronopost'],
            ['13','24','Chronopost'],
            ['12','24','Chronopost'],
            ['11','24','Chronopost'],
            ['10','24','Chronopost'],
            ['09','24','Chronopost'],
            ['08','24','Chronopost'],
            ['07','24','Chronopost'],
            ['06','24','Chronopost'],
            ['05','24','Chronopost'],
            ['04','24','Chronopost'],
            ['03','24','Chronopost'],
            ['02','24','Chronopost'],
            ['01','24','Chronopost'],
            ['98','24','Chronopost'],
        ];



        foreach ($strangerProject as $project) {
            $delivery = new Delivery();
            $delivery->setCountry($project[0]);
            $delivery->setHour($project[1]);
            $delivery->setDeliver($project[2]);
            $delivery->setType(Project::TYPE_BASIC);
            $this->deliveryManager->save($delivery);
        }


        foreach ($basicProject as $project) {
            $delivery = new Delivery();
            $delivery->setZip($project[0]);
            $delivery->setHour($project[1]);
            $delivery->setDeliver($project[2]);
            $delivery->setType(Project::TYPE_BASIC);
            $this->deliveryManager->save($delivery);
        }

        foreach ($urgencyProject as $project) {
            $delivery = new Delivery();
            $delivery->setZip($project[0]);
            $delivery->setHour($project[1]);
            $delivery->setDeliver($project[2]);
            $delivery->setType(Project::TYPE_URGENCY);
            $this->deliveryManager->save($delivery);
        }
    }
}
