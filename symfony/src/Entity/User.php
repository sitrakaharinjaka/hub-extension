<?php

namespace App\Entity;

use App\Entity\Traits\AuthorableTrait;
use App\Entity\Traits\GedmoTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="dk_user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    use GedmoTrait;
    use AuthorableTrait;

    const PATTERN_EMAIL = '/^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{8,}$/';
    const ROLE_DEFAULT = 'ROLE_USER';
    const MODE_ADMIN = 'admin';
    const MODE_MEMBER = 'member';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Firstname
     * @var string
     * @ORM\Column(name="first_name", length=255, nullable=true)
     */
    private $firstName;

    /**
     * Lastname
     * @var string
     * @ORM\Column(name="last_name", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $username;

    /**
     * Slug
     *
     * @Gedmo\Slug(fields={"username"})
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var array
     * @ORM\Column(name="roles", type="array")
     */
    private $roles;

    /**
     * @var boolean
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = false;

    /**
     * @var boolean
     * @ORM\Column(name="locked", type="boolean")
     */
    private $locked = false;

    /**
     * @var string
     * @ORM\Column(name="confirmation_token", type="string", nullable=true)
     */
    private $confirmationToken;

    /**
     * @var string
     *
     */
    private $plainPassword;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Stock", inversedBy="users")
     * @ORM\JoinTable(name="dk_user_stocks")
     *
     * @var ArrayCollection
     */
    protected $stocks;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Stock", inversedBy="adminUsers")
     * @ORM\JoinTable(name="dk_user_stocks_admin")
     *
     * @var ArrayCollection
     */
    protected $adminStocks;

    /**
     * @var Collection|Stock[]
     * @ORM\OneToMany(targetEntity="App\Entity\Stock", mappedBy="referentUser", cascade={"all"})
     */
    protected $referentStocks;

    /**
     * @var Collection|Project[]
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="user", cascade={"all"})
     */
    protected $projects;

    /**
     * @var Collection|Project[]
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="runner", cascade={"all"})
     */
    protected $preparations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project")
     * @ORM\JoinColumn(name="current_project_id", referencedColumnName="id", nullable=true)
     * @var Project|null
     */
    protected $currentProject;

    /**
     * Display Name
     * @var string
     * @ORM\Column(name="display_name", length=255, nullable=true)
     */
    protected $displayName;

    /**
     * @var string
     * @ORM\Column(length=255, nullable=true)
     */
    protected $department;

    /**
     * @var string
     * @ORM\Column(length=255, nullable=true)
     */
    protected $division;

    /**
     * @var string
     * @ORM\Column(length=255, nullable=true)
     */
    protected $company;

    /**
     * @var string
     * @ORM\Column(length=255, nullable=true)
     */
    protected $phone;

    /**
     * @var string
     * @ORM\Column(length=255, nullable=true)
     */
    protected $country;
    
    /**
     * @var string
     * @ORM\Column(length=255, nullable=true)
     */
    protected $organizationUnit;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $attributes;

    /**
     * Last Connection
     *
     * @var \DateTime $lastConnection
     *
     * @ORM\Column(name="last_connection", type="datetime", nullable=true)
     */
    protected $lastConnection;

    /**
     * Last Connection
     *
     * @var \DateTime $lastConnectionFirst
     *
     * @ORM\Column(name="last_connection_first", type="datetime", nullable=true)
     */
    protected $lastConnectionFirst;

    /**
     * @ORM\ManyToMany(targetEntity="BillingCenterTranslation", mappedBy="contact")
     * @var BillingCenterTranslation
     */
    protected $billingCenterTranslation;

    /**
     * @var Collection|Project[]
     * @ORM\OneToMany(targetEntity="App\Entity\MyBoxesBox", mappedBy="user")
     */
    protected $boxes;


    /**
     * @var Collection|Project[]
     * @ORM\OneToMany(targetEntity="App\Entity\MyBoxesBoxUser", mappedBy="user")
     */
    protected $boxebis;

    public function __construct()
    {
        $this->active = false;
        $this->locked = false;
        $this->roles = [];
        $this->projects = new ArrayCollection();
        $this->preparations = new ArrayCollection();
        $this->stocks = new ArrayCollection();
        $this->adminStocks = new ArrayCollection();
        $this->referentStocks = new ArrayCollection();
        $this->boxes = new ArrayCollection();
        $this->boxebis = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getUsername();
    }

    /**
     * @param $role
     *
     * @return $this
     */
    public function addRole($role)
    {
        $role = strtoupper($role);
        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * @param $role
     *
     * @return $this
     */
    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $email
     */
    public function setEmail($email)
    {
        $username = $this->getUsername();
        if (empty($username)) {
            $this->setUsername($email);
        }
        $this->email = $email;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * {@inheritdoc}
     */
    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }


    /**
     * @return boolean
     */
    public function isGeneralAdmin()
    {
        return $this->hasRole('ROLE_GENERAL_ADMIN');
    }

    /**
     * @param boolean $isAdmin
     */
    public function setIsGeneralAdmin($isGeneralAdmin)
    {
        if ($isGeneralAdmin) {
            $this->addRole('ROLE_GENERAL_ADMIN');
        } else {
            $this->removeRole('ROLE_GENERAL_ADMIN');
        }
    }

    /**
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->hasRole('ROLE_ADMIN');
    }

    /**
     * @param boolean $isAdmin
     */
    public function setIsAdmin($isAdmin)
    {
        if ($isAdmin) {
            $this->addRole('ROLE_ADMIN');
        } else {
            $this->removeRole('ROLE_ADMIN');
        }
    }

    /**
     * @return boolean
     */
    public function isVeologAdmin()
    {
        return $this->hasRole('ROLE_VEOLOG_ADMIN');
    }

    /**
     * @param boolean $isVeologAdmin
     */
    public function setIsVeologAdmin($isVeologAdmin)
    {
        if ($isVeologAdmin) {
            $this->addRole('ROLE_VEOLOG_ADMIN');
        } else {
            $this->removeRole('ROLE_VEOLOG_ADMIN');
        }
    }

    /**
     * @return boolean
     */
    public function isRunner()
    {
        return $this->hasRole('ROLE_RUNNER');
    }

    /**
     * @param boolean $isRunner
     */
    public function setIsRunner($isRunner)
    {
        if ($isRunner) {
            $this->addRole('ROLE_RUNNER');
        } else {
            $this->removeRole('ROLE_RUNNER');
        }
    }

    /**
     * @return bool
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * @param bool $locked
     */
    public function setLocked(bool $locked)
    {
        $this->locked = $locked;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @param $confirmationToken
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
    }

    public function hasStock(Stock $stock)
    {
        foreach ($this->getStocks() as $s) {
            if ($s->getId() == $stock->getId()) {
                return true;
            }
        }

        return false;
    }

    public function addStock(Stock $stock)
    {
        if (!$this->hasStock($stock)) {
            $this->stocks->add($stock);
        }
    }

    public function removeStock(Stock $stock)
    {
        if ($this->hasStock($stock)) {
            foreach ($this->getStocks() as $key => $s) {
                if ($s->getId() == $stock->getId()) {
                    unset($this->stocks[$key]);
                }
            }
        }
    }

    public function getStocks()
    {
        return $this->stocks;
    }

    public function getDefaultStock()
    {
        $stocks = $this->getStocks();

        if (empty($stocks)) {
            return null;
        }

        return $stocks->first();
    }

    /**
     * {@inheritdoc}
     */
    public function addProject(Project $project)
    {
        if ($this->hasProject($project)) {
            return;
        }

        $this->projects->add($project);
        $project->setUser($this);
        $this->setCurrentProject($project);
    }

    /**
     * {@inheritdoc}
     */
    public function removeProject(Project $project)
    {
        if ($this->hasProject($project)) {
            $this->projects->removeElement($project);
            $project->setUser(null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * {@inheritdoc}
     */
    public function clearProjects()
    {
        $this->projects->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function hasProject(Project $project)
    {
        return $this->projects->contains($project);
    }

    /**
     * {@inheritdoc}
     */
    public function addPreparation(Project $project)
    {
        if ($this->hasPreparation($project)) {
            return;
        }
        $this->preparations->add($project);
        $project->setRunner($this);
    }

    /**
     * {@inheritdoc}
     */
    public function removePreparation(Project $project)
    {
        if ($this->hasPreparation($project)) {
            $this->preparations->removeElement($project);
            $project->setRunner(null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPreparations()
    {
        return $this->preparations;
    }

    /**
     * {@inheritdoc}
     */
    public function getPreparationsFiltered(string $state)
    {
        $filtered = new ArrayCollection();
        foreach ($this->preparations as $project) {
            if ($project->getState() == $state) {
                $filtered->add($project);
            }
        }
        return $filtered;
    }

    /**
     * {@inheritdoc}
     */
    public function clearPreparations()
    {
        $this->preparations->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function hasPreparation(Project $project)
    {
        return $this->preparations->contains($project);
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @param $displayName
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
    }

    /**
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return string
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * @param $division
     */
    public function setDivision($division)
    {
        $this->division = $division;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getOrganizationUnit()
    {
        return $this->organizationUnit;
    }

    /**
     * @param $organizationUnit
     */
    public function setOrganizationUnit($organizationUnit)
    {
        $this->organizationUnit = $organizationUnit;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->getFirstName().' '.$this->getLastName();
    }

    /**
     * @return mixed
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param mixed $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @return BillingCenterTranslation
     */
    public function getBillingCenterTranslation(): BillingCenterTranslation
    {
        return $this->billingCenterTranslation;
    }

    /**
     * @param BillingCenterTranslation $billingCenterTranslation
     */
    public function setBillingCenterTranslation(BillingCenterTranslation $billingCenterTranslation): void
    {
        $this->billingCenterTranslation = $billingCenterTranslation;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->email
        ));
    }

    /**
     * @param $serialized
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->password,
            $this->email
            ) = unserialize($serialized);
    }


    public function hasAdminStock(Stock $stock)
    {
        foreach ($this->getAdminStocks() as $s) {
            if ($s->getId() == $stock->getId()) {
                return true;
            }
        }

        return false;
    }


    public function addAdminStock(Stock $stock)
    {
        if (!$this->hasAdminStock($stock)) {
            if (!$this->hasStock($stock)) {
                $this->stocks->add($stock);
            }
            
            $this->adminStocks->add($stock);
            $this->setIsAdmin(true);
        }
    }

    public function removeAdminStock(Stock $stock)
    {
        if ($this->hasAdminStock($stock)) {
            foreach ($this->getAdminStocks() as $key => $s) {
                if ($s->getId() == $stock->getId()) {
                    unset($this->adminStocks[$key]);
                }
            }

            $this->adminStocks->removeElement($stock);
        }

        if (count($this->adminStocks) <= 0) {
            $this->setIsAdmin(false);
        }
    }

    public function getAdminStocks()
    {
        return $this->adminStocks;
    }

    public function getDefaultAdminStock()
    {
        $adminStocks = $this->getAdminStocks();

        if (empty($adminStocks)) {
            return null;
        }

        return $adminStocks->first();
    }

    /**
     * @return Collection|Stock[]
     */
    public function getReferentStocks()
    {
        return $this->referentStocks;
    }

    /**
     * @param Collection|Stock[] $referentStocks
     */
    public function setReferentStocks($referentStocks): void
    {
        $this->referentStocks = $referentStocks;
    }

    /**
     * @return \DateTime
     */
    public function getLastConnection()
    {
        return $this->lastConnection;
    }

    /**
     * @param \DateTime $lastConnection
     */
    public function setLastConnection($lastConnection)
    {
        if (empty($this->lastConnectionFirst)) {
            $this->lastConnectionFirst = $lastConnection;
        }

        $this->lastConnection = $lastConnection;
    }

    /**
     * Get the value of boxes
     *
     * @return  Collection|Project[]
     */ 
    public function getBoxes()
    {
        return $this->boxes;
    }

    /**
     * Set the value of boxes
     *
     * @param  Collection|Project[]  $boxes
     *
     * @return  self
     */ 
    public function setBoxes($boxes)
    {
        $this->boxes = $boxes;

        return $this;
    }

    /**
     * Get the value of boxebis
     *
     * @return  Collection|Project[]
     */ 
    public function getBoxebis()
    {
        return $this->boxebis;
    }

    /**
     * Set the value of boxebis
     *
     * @param  Collection|Project[]  $boxebis
     *
     * @return  self
     */ 
    public function setBoxebis($boxebis)
    {
        $this->boxebis = $boxebis;

        return $this;
    }
}
