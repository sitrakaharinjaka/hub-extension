<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockEntryItemRepository")
 * @ORM\Table(name="dk_item_stock_entry")
 * @ORM\HasLifecycleCallbacks
 */
class StockEntryItem
{
    use GedmoTrait;
    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $product;

    /**
     * Quantity
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $quantity;

    /**
     * Quantity Received
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $quantityReceived = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StockEntry", inversedBy="stockEntryItems")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    protected $stockEntry;

    /**
     * Status
     *
     * @ORM\Column(length=255, nullable=false)
     */
    protected $status;

    /**
     * Product State
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $productState;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="crd_boolean", options={"default":0})
     */
    protected $crd = false;

    /**
     * Wash Basins
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $washBasins;

    /**
     * Dry Cleaning
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $dryCleaning;

    /**
     * Wash Glasses
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $washGlasses;

    /**
     * Invoice file
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $invoiceFile;

    /**
     * @var Collection|StockEntryRejection[]
     * @ORM\OneToMany(targetEntity="App\Entity\StockEntryRejection", mappedBy="stockEntryItem", cascade={"persist"})
     */
    protected $itemRejections;

    public function __construct()
    {
        $this->itemRejections = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product): void
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getStockEntry()
    {
        return $this->stockEntry;
    }

    /**
     * @param mixed $stockEntry
     */
    public function setStockEntry($stockEntry): void
    {
        $this->stockEntry = $stockEntry;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getQuantityReceived()
    {
        return $this->quantityReceived;
    }

    /**
     * @param mixed $quantityReceived
     */
    public function setQuantityReceived($quantityReceived): void
    {
        $this->quantityReceived = $quantityReceived;
    }

    /**
     * @return mixed
     */
    public function getWashBasins()
    {
        return $this->washBasins;
    }

    /**
     * @param mixed $washBasins
     */
    public function setWashBasins($washBasins): void
    {
        $this->washBasins = $washBasins;
    }

    /**
     * @return mixed
     */
    public function getDryCleaning()
    {
        return $this->dryCleaning;
    }

    /**
     * @param mixed $dryCleaning
     */
    public function setDryCleaning($dryCleaning): void
    {
        $this->dryCleaning = $dryCleaning;
    }

    /**
     * @return mixed
     */
    public function getWashGlasses()
    {
        return $this->washGlasses;
    }

    /**
     * @param mixed $washGlasses
     */
    public function setWashGlasses($washGlasses): void
    {
        $this->washGlasses = $washGlasses;
    }

    /**
     * Get product State
     */
    public function getProductState()
    {
        return $this->productState;
    }

    /**
     * Set product State
     *
     * @return  self
     */
    public function setProductState($productState)
    {
        $this->productState = $productState;

        return $this;
    }

    /**
     * Get the value of crd
     *
     * @return  boolean
     */
    public function getCrd()
    {
        return $this->crd;
    }

    /**
     * Set the value of crd
     *
     * @param  boolean  $crd
     *
     * @return  self
     */
    public function setCrd($crd)
    {
        $this->crd = $crd;

        return $this;
    }

    /**
     * Get invoice file
     */
    public function getInvoiceFile()
    {
        return $this->invoiceFile;
    }

    /**
     * Set invoice file
     *
     * @return  self
     */
    public function setInvoiceFile($invoiceFile)
    {
        $this->invoiceFile = $invoiceFile;

        return $this;
    }

    public function setItemRejections($itemRejections)
    {
        $this->itemRejections = $itemRejections;

        return $this;
    }

    /**
     * Check if an item has been rejected
     *
     * @return mixed
     */
    public function getItemRejections()
    {
        return $this->itemRejections;
    }

    public function addItemRejection(StockEntryRejection $itemRejection): self
    {
        if (!$this->itemRejections->contains($itemRejection)) {
            $itemRejection->setStockEntry($this->getStockEntry());
            $itemRejection->setStockEntryItem($this);
            $this->itemRejections[] = $itemRejection;
        }

        return $this;
    }

    public function removeItemRejection(StockEntryRejection $itemRejection): self
    {
        if ($this->itemRejections->contains($itemRejection)) {
            $this->itemRejections->removeElement($itemRejection);
            // set the owning side to null (unless already changed)
            if ($itemRejection->getStockEntryItem() === $this) {
                $itemRejection->setStockEntryItem(null);
            }
        }

        return $this;
    }

}
