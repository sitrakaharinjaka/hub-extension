<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\Model\SearchUser;
use App\Form\Type\User\SearchUserType;
use App\Form\Type\User\UserRestrictCreateType;
use App\Utils\Managers\StockManager;
use App\Utils\Managers\UserManager;
use App\Controller\BaseController;
use App\Repository\StockRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class UserRestrictController
 *
 * @Route("/admin/restrict/users")
 * @package App\Controller
 */
class UserRestrictController extends BaseController
{
    /**
     * User's list
     * @Route("/list", name="admin_restrict_user_list")
     */
    public function listAction(Breadcrumbs $breadcrumbs, Request $request, UserManager $userManager): Response
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchUserType::class, $data, [
            'method' => 'get'
        ]);

        // Breadcrumb
        $breadcrumbs->addItem('Tableau de bord', $this->generateUrl('admin_homepage'));
        $breadcrumbs->addItem('Liste des utilisateurs du stock');

        // Generate query
        $filters = $data->getSearchData();

        $filters['stock'] = $request->getSession()->get('current_admin_stock');
        $query = $userManager->queryForSearch($filters);

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20
        );

        // Render view
        return $this->render('admin/User/restrict/index.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView()
        ]);
    }

    /**
     * Update a user
     * @Route("/create", name="admin_restrict_user_create")
     * @param Request $request Request
     *
     * @return Response
     */
    public function createAction(Request $request, UserManager $userManager, StockManager $stockManager)
    {
        $sessionStock = $this->get('session')->get('current_admin_stock');
        $currentStock = $stockManager->findOneBy(['id' => $sessionStock->getId()]);

        // Find it
        $user =  new User();
        $user->setPlainPassword("diskodisko");
        $user->addStock($currentStock);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des utilisateurs du stock", $this->get("router")->generate("admin_restrict_user_list"));
        $breadcrumbs->addItem("Création");

        // Init form
        $form = $this->createForm(UserRestrictCreateType::class, $user, [
            'em' => $this->getDoctrine()->getManager(),
            'stocks_query_builder' => function (StockRepository $er) {
                $connect = $this->getUser();
                $qb = $er->createQueryBuilder('u');
                if (!$connect->hasRole('ROLE_GENERAL_ADMIN') and !$connect->hasRole('ROLE_SUPER_ADMIN')) {
                    $qb->leftJoin('u.adminUsers', 'us');
                    $qb->andWhere("us.id = :user");
                    $qb->setParameter('user', $connect->getId());
                }
                $qb->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale');
                $qb->setParameter('locale', 'fr');
                $qb->orderBy('t.name', 'ASC');
                return $qb;
            },
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                $userManager->save($user);
                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'notice',
                    "Utilisateur " . $user->getEmail() . " créé"
                );
                return $this->redirectToRoute('admin_restrict_user_list');
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/User/restrict/create.html.twig',
            array(
                'form'           => $form->createView(),
                'user'           => $user,
            )
        );
    }

    /**
     * Init Search object
     *
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchUser();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setEmail((isset($filters['email']))   ? $filters['email'] : '');
        $data->setFirstName((isset($filters['firstName'])) ? $filters['firstName'] : '');
        $data->setLastName((isset($filters['lastName'])) ? $filters['lastName'] : '');

        return $data;
    }


    /**
     * Move to admin
     * @Route("/admin/{id}", name="admin_restrict_user_admin")
     *
     * @param Request $request Request
     * @param integer $id      Id of user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function adminAction(Request $request, UserManager $userManager, StockManager $stockManager, $id)
    {
        // Find it
        /** @var User $user */
        $user =  $userManager->findOneBy(['id' => $id]);
        if (!$user) {
            throw $this->createNotFoundException('User not found');
        }

        $currentStock = $this->get('session')->get('current_admin_stock');

        if (!$user->hasAdminStock($currentStock)) {
            $currentStock = $stockManager->findOneBy(['id' => $currentStock->getId()]);
            $user->addAdminStock($currentStock);
            $userManager->save($user);

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'notice',
                'L\'utilisateur "'.$user->getFirstName().' '.$user->getLastName().'" a bien été ajouté en tant que gestionnaire du stock'
            );
        }

        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * Move to membre
     * @Route("/member/{id}", name="admin_restrict_user_member")
     *
     * @param Request $request Request
     * @param integer $id      Id of user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function memberAction(Request $request, UserManager $userManager, StockManager $stockManager, $id)
    {
        // Find it
        /** @var User $user */
        $user =  $userManager->findOneBy(['id' => $id]);
        if (!$user) {
            throw $this->createNotFoundException('User not found');
        }

        $currentStock = $this->get('session')->get('current_admin_stock');

        if (!$user->hasStock($currentStock)) {
            $currentStock = $stockManager->findOneBy(['id' => $currentStock->getId()]);
            $user->addStock($currentStock);
            $userManager->save($user);

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'notice',
                'L\'utilisateur "'.$user->getFirstName().' '.$user->getLastName().'" a bien été ajouté en tant que membre du stock'
            );
        }

        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * Delete on user
     * @Route("/remove/{id}", name="admin_restrict_user_remove")
     * @param Request $request Request
     * @param integer $id      Id of user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Request $request, UserManager $userManager, $id)
    {
        // Find it
        /** @var User $user */
        $user =  $userManager->findOneBy(['id' => $id]);
        if (!$user) {
            throw $this->createNotFoundException('User not found');
        }

        $currentStock = $this->get('session')->get('current_admin_stock');

        if ($user->hasStock($currentStock)) {
            $user->removeStock($currentStock);
            $user->removeAdminStock($currentStock);
            $userManager->save($user);

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'notice',
                'L\'utilisateur "'.$user->getFirstName().' '.$user->getLastName().'" a bien été supprimé des utilisateurs du stock'
            );
        }

        return $this->redirect($request->headers->get('referer'));
    }
}
