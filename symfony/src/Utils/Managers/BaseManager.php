<?php

/**
 * Manager
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Utils\Managers;

use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Abstract class for the service which manage the Entity Manager
 */
abstract class BaseManager
{

    /**
     * @var FileSystem
     */
    protected $fileSystem;

    /**
     * @var EntityManagerInterface The Entity Manager
     */
    protected $em;


    /**
     * @var LocaleManager locale service
     */
    protected $localeManager;

    /**
     * BaseManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager = null)
    {
        $this->em = $em;
        $this->fileSystem = $fileSystem;
        $this->localeManager = $localeManager;
    }


    /**
     * Getter of the Entity Manager
     *
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * Add a repository to this service
     *
     * @param integer $key   Key
     * @param string  $class Class
     *
     * @return void
     */
    public function addRepository($key, $class)
    {
        $this->$key = $this->em->getRepository($class);
    }

    /**
     * Add a service to this service
     *
     * @param integer $key     Key
     * @param string  $service Class
     *
     * @return void
     */
    public function addManager($key, $service)
    {
        $this->$key = $service;
    }

    /**
     * Clear result cache
     *
     * @param string $prefix
     */
    public function clearResultCache($prefix = '')
    {
        $config = $this->em->getConfiguration();
        $cacheDriver = $config->getResultCacheImpl();
        $cacheDriver->flushAll();

        if (get_class($cacheDriver) == 'MemcachedCache') {
            $cacheDriver->doFlush();
        }
    }


    /**
     * Inject file system gaufrette
     *
     * @param $obj
     */
    public function injectFileSystem($obj, $adapter = "current")
    {
        if (count($obj) > 1 or is_array($obj)) {
            foreach ($obj as $ob) {
                $this->injectFileSystem($ob, $adapter);
            }
            return;
        }

        $fileSystem = $this->fileSystem->get($adapter);
        if (method_exists($obj, "setFileSystem")) {
            $obj->setFileSystem($fileSystem);
            if (method_exists($obj, "getTranslations") && count($obj->getTranslations()) > 0) {
                foreach ($obj->getTranslations() as $translation) {
                    $this->injectFileSystem($translation, $adapter);


                    if (method_exists($translation, "getManyToMany") && count($translation->getManyToMany()) > 0) {
                        foreach ($translation->getManyToMany() as $field) {
                            $getter = 'get'.ucfirst($field);
                            if (count($translation->$getter()) > 0) {
                                foreach ($translation->$getter() as $o) {
                                    $this->injectFileSystem($o, $adapter);
                                }
                            }
                        }
                    }

                    if (method_exists($translation, "getToManyManage") && count($translation->getToManyManage()) > 0) {
                        foreach ($translation->getToManyManage() as $field) {
                            $getter = 'get'.ucfirst($field);
                            if (count($translation->$getter()) > 0) {
                                foreach ($translation->$getter() as $o) {
                                    $this->injectFileSystem($o, $adapter);
                                }
                            }
                        }
                    }
                }
            }



            if (method_exists($obj, "getToManyManage") && count($obj->getToManyManage()) > 0) {
                foreach ($obj->getToManyManage() as $field) {
                    $getter = 'get'.ucfirst($field);
                    if (count($obj->$getter()) > 0) {
                        foreach ($obj->$getter() as $o) {
                            $this->injectFileSystem($o, $adapter);
                        }
                    }
                }
            }
        }

        return $obj;
    }
}
