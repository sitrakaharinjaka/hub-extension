<?php
/**
 * Created by PhpStorm.
 * User: lsimonin
 * Date: 23/07/2018
 * Time: 09:59
 */
namespace App\Hydrators;

use Doctrine\ORM\Internal\Hydration\AbstractHydrator;
use PDO;

class ListHydrator extends AbstractHydrator
{
    /**
     * @return array
     */
    protected function hydrateAllData()
    {
        $result = array();
        foreach ($this->_stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
            $this->hydrateRowData($row, $result);
        }

        return $result;
    }

    /**
     * @param array $data
     * @param array $result
     */
    protected function hydrateRowData(array $data, array &$result)
    {
        $keys = array_keys($data);
        $keyCount = count($keys);
        $key = $data[$keys[0]];
        if ($keyCount === 1) {
            $value = $data[$keys[0]];
        } elseif ($keyCount === 2) {
            $value = $data[$keys[1]];
        } else {
            array_shift($data);
            $value = array_values($data);
        }

        $result[$key] = $value;
    }
}
