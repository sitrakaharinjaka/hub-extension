<?php

namespace App\Command\User;

use App\Utils\Managers\IPManager;
use App\Utils\Managers\UserManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class ChangeRolesUserCommand extends Command
{
    /** @var  EntityManager */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;

        parent::__construct();
    }

    protected static $defaultName = 'disko:users:change-roles';

    protected function configure()
    {
        $this->setDescription('Change roles of one user (roles need to be seperate by "," ');
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'User email')
            ->addArgument('roles', InputArgument::REQUIRED, 'User roles')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $roles = $input->getArgument("roles");
        $roles = explode(',', $roles);

        $user = $this->userManager->findOneBy(['email' => $input->getArgument("email")]);
        if ($user) {
            foreach ($roles as $k => $role) {
                $roles[$k] = trim($roles[$k]);
            }

            $user->setRoles($roles);
            $this->userManager->save($user);
            $output->writeln("<info>Les rôles ont biens été modifiés</info>");
        } else {
            $output->writeln("<error>Utilisateur introuvable</error>");
        }
    }
}
