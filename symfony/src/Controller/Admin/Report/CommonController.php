<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin\Report;

use App\Entity\Connection;
use App\Entity\User;
use App\Form\Model\SearchDateToDate;
use App\Form\Model\SearchUser;
use App\Form\Type\SearchDateToDateType;
use App\Form\Type\User\SearchUserType;
use App\Form\Type\User\UserCreateType;
use App\Form\Type\User\UserType;
use App\Utils\Managers\ConnectionManager;
use App\Utils\Managers\StockLineManager;
use App\Utils\Managers\UserManager;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 *
 * @Route("/admin/report")
 * @package App\Controller
 */
class CommonController extends BaseController
{
    /**
     * @Route("/common", name="admin_common_report")
     */
    public function indexAction(Request $request, UserManager $userManager, ConnectionManager $connectionManager, StockLineManager $stockLineManager)
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchDateToDateType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Rapports Communs');

        $stockLineManager->checkAndUpdateFullGroupOnlyOption();

        $statsCreated = $userManager->statsCreated($data->getStart(), $data->getEnd());
        $statsFirstConnections = $userManager->statsFirstConnect($data->getStart(), $data->getEnd());
        $statsConnections = $connectionManager->stats($data->getStart(), $data->getEnd());

        // Render view
        return $this->render('@Admin/Report/Common/index.html.twig', array(
            'statsCreated' => $statsCreated,
            'statsFirstConnections' => $statsFirstConnections,
            'statsConnections' => $statsConnections,
            'data' => $data,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     *
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        $dateTimeDefaultStart = new \DateTime();
        $dateTimeDefaultStart->modify('-14 day');

        $dateTimeDefaultEnd = new \DateTime();

        // Init form
        $data = new SearchDateToDate();
        $data->setStart((isset($filters['start'])) ? \DateTime::createFromFormat("d/m/Y", $filters['start']) : $dateTimeDefaultStart);
        $data->setEnd((isset($filters['end']))   ? \DateTime::createFromFormat("d/m/Y", $filters['end']) : $dateTimeDefaultEnd);

        return $data;
    }
}
