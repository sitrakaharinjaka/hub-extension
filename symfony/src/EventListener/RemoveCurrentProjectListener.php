<?php
/**
 * Created by PhpStorm.
 * User: lsimonin
 * Date: 14/11/2018
 * Time: 10:44
 */

namespace App\EventListener;

use App\Event\ProjectEvent;
use App\ProjectEvents;
use App\Service\ProjectInterface;
use App\Service\UserInterface;
use App\Utils\MailerInterface;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\UserManager;
use App\Utils\Services\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class RemoveCurrentProjectListener implements EventSubscriberInterface
{
    /**
     * @var MailerService
     */
    private $mailer;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var ProjectManager
     */
    private $projectManager;

    private $session;


    /**
     * RemoveCurrentProjectListener constructor.
     *
     * @param UserManager    $userManager
     * @param ProjectManager $projectManager
     * @param MailerService  $mailer
     */
    public function __construct(SessionInterface $session, UserManager $userManager, ProjectManager $projectManager, MailerService $mailer)
    {
        $this->mailer = $mailer;
        $this->userManager = $userManager;
        $this->projectManager = $projectManager;
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return [
            ProjectEvent::PROJECT_STATE_UPDATED => 'stateUpdated',
            ProjectEvent::ADMIN_USER_EXIT_IMPERSONATE => 'adminExitImpersonate',
            ProjectEvent::PROJECT_SAVED_DRAFT => 'saved',
            ProjectEvent::PROJECT_DELETE => 'projectRemoved'
        ];
    }

    /**
     * @param ProjectEvent $event
     */
    public function projectRemoved(ProjectEvent $event)
    {
        $project = $event->getProject();
        $this->removeCurrentProject($event);
        if (!$project->isDraft() && null !== $event->getFrom()) {
            $this->mailer->sendMailRemoveProject($project, $event->getFrom());
        }
        $project->setUser(null);
        $this->projectManager->save($project);
    }

    public function stateUpdated(ProjectEvent $event)
    {
        $this->removeCurrentProject($event);
    }

    public function saved(ProjectEvent $event)
    {
        $this->removeCurrentProject($event);
    }

    public function adminExitImpersonate(ProjectEvent $event)
    {
        $this->removeCurrentProject($event);
    }

    private function removeCurrentProject(ProjectEvent $event)
    {
        $project = $event->getProject();
        $currentProject = $this->session->get('current_project', null);

        if ($currentProject && $currentProject->getId() == $project->getId()) {
            $this->session->set('current_project', null);
        }
    }
}
