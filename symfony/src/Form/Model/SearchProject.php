<?php

/**
 * Model for search
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Model;

/**
 * Class SearchProject
 *
 * @package App\Form\Model
 */
class SearchProject
{
    /**
     * Id
     *
     * @var string
     */
    protected $id;

    /**
     * Name
     *
     * @var string
     */
    protected $name;

    /**
     * State
     *
     * @var string
     */
    protected $state = 'all';

    /**
     * State
     *
     * @var string
     */
    protected $trackers;

    /**
     * Type
     *
     * @var string
     */
    protected $type = 'all';

    /**
     * Stock
     *
     * @var string
     */
    protected $stock;

    /**
     * Cave
     *
     * @var string
     */
    protected $cave = 'all';

    /**
     * Type
     *
     * @var string
     */
    protected $dateAddVlg;

    /**
     * Type
     *
     * @var string
     */
    protected $dateStart;

    /**
     * Type
     *
     * @var string
     */
    protected $dateEnd;

    /**
     * Type
     *
     * @var string
     */
    protected $userFullName;

    /**
     * Type
     *
     * @var string
     */
    protected $created;


    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getTrackers()
    {
        return $this->trackers;
    }

    /**
     * @return string
     */
    public function setTrackers($trackers)
    {
        return $this->trackers = $trackers;;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function setType($type)
    {
        return $this->type = $type;;
    }

    /**
     * @return string
     */
    public function isTrackingAvailable()
    {
        if ($this->trackers) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string
     */
    public function getDateAddVlg()
    {
        return $this->dateAddVlg;
    }


    public function setDateAddVlg(?\DateTimeInterface $dateAddVlg): self
    {
        $this->dateAddVlg = $dateAddVlg;

        return $this;
    }

    /**
     * @param string
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    public function setDateStart(?\DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * @param string
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    public function setDateEnd(?\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserFullName()
    {
        return $this->userFullName;
    }

    /**
     * @return string
     */
    public function setUserFullName($userFullName)
    {
        return $this->userFullName = $userFullName;
    }

    /**
     * @param string
     */
    public function getCreated()
    {
        return $this->created;
    }

    public function setCreated(?\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get search data
     *
     * @return array
     */
    public function getSearchData()
    {
        $tab = array();

        if (!empty($this->id)) {
            $tab['id'] = $this->id;
        }
        if (!empty($this->name)) {
            $tab['name'] = $this->name;
        }
        if (!empty($this->state) && $this->state != 'all') {
            $tab['state'] = $this->state;
        }
        if (!empty($this->trackers)) {
            $tab['trackers'] = $this->trackers;
        }
        if (!empty($this->type) && $this->type != 'all') {
            $tab['type'] = $this->type;
        }
        if (!empty($this->stock)) {
            $tab['stock'] = $this->stock;
        }
        if (!empty($this->cave) && $this->cave != 'all') {
            $tab['cave'] = $this->cave;
        }
        if (!empty($this->dateAddVlg)) {
            $tab['dateAddVlg'] = $this->dateAddVlg;
        }
        if (!empty($this->dateStart)) {
            $tab['dateStart'] = $this->dateStart;
        }
        if (!empty($this->dateEnd)) {
            $tab['dateEnd'] = $this->dateEnd;
        }
        if (!empty($this->userFullName)) {
            $tab['userFullName'] = $this->userFullName;
        }
        if (!empty($this->created)) {
            $tab['created'] = $this->created;
        }
        
        return $tab;
    }

    /**
     * Get cave
     *
     * @return  string
     */ 
    public function getCave()
    {
        return $this->cave;
    }

    /**
     * Set cave
     *
     * @param  $cave  Cave
     *
     * @return  self
     */ 
    public function setCave($cave)
    {
        $this->cave = $cave;

        return $this;
    }

    /**
     * Get stock
     *
     * @return  Stock
     */ 
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set stock
     *
     * @param  $stock  Stock
     *
     * @return  self
     */ 
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }
}
