<?php

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class Fail2BanRepository
 *
 */
class Fail2BanRepository extends BaseRepository
{
    public function countBadLogBefore($ip, $time = '-1 hour')
    {
        $date = new \DateTime();
        $date->modify($time);

        return $this->createQueryBuilder('u')
            ->select('COUNT(u)')
            ->andWhere('u.ip = :ip')
            ->andWhere('u.created >= :date')
            ->andWhere('u.success = FALSE')
            ->setParameters([
                'ip' => $ip,
                'date' => $date
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u')
            ->orderBy('u.id', 'DESC');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $qb->andWhere('u.'.$key.' LIKE :'.$key);
                $qb->setParameter($key, '%'.$filter.'%');
            }
        }

        return $qb->getQuery();
    }
}
