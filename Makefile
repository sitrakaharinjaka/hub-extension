DOCKER_COMPOSE  = docker-compose

EXEC_PHP        = $(DOCKER_COMPOSE) exec php
YARN         = $(EXEC_PHP) yarn

SYMFONY         = $(EXEC_PHP) bin/console
COMPOSER        = $(EXEC_PHP) composer

ifneq ("$(wildcard .env)","")
    include .env
    export $(shell sed 's/=.*//' .env)
endif



##
## Project
## -------
##
clean-aws:
	sudo rm /var/lib/dpkg/lock
	sudo dpkg --configure -a
	sudo apt update
	sudo apt install make

build:
	@$(DOCKER_COMPOSE) pull --parallel --ignore-pull-failures 2> /dev/null
	$(DOCKER_COMPOSE) build --pull

build-nocache:
	@$(DOCKER_COMPOSE) pull --parallel --ignore-pull-failures 2> /dev/null
	$(DOCKER_COMPOSE) build --pull --no-cache

start: ## Start the project
start: sync-letsencrypt
	$(DOCKER_COMPOSE) up -d --remove-orphans --no-recreate

stop: ## Stop the project
	$(DOCKER_COMPOSE) stop

kill:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

clean: ## Stop the project and remove generated files
clean: kill
	rm -rf .env vendor node_modules var/cache/* var/log/* public/build/*

reset: ## Stop and start a fresh install of the project
reset: kill install

install: ## Install and start the project
install: .env .env-symfony sync-letsencrypt build start db assets hook-git


gitflow: ## Create all gitflow
gitflow:
	sh gitflow.sh
	exit 1;\

install-nocache: ## Install without cache docker and start the project
install-nocache: .env .env-symfony sync-letsencrypt build-nocache start db assets hook-git

deploy: sync-letsencrypt .env vendor assets-prod db-update
	rm -rf var/cache/*

no-docker:
	$(eval DOCKER_COMPOSE := \#)
	$(eval EXEC_PHP := )

.PHONY: build build-nocache install-nocache kill install reset start stop clean no-docker deploy install-nocache
##
## Utils
## -----
##

db: ## Reset the database and load fixtures
db: flush .env vendor
	$(SYMFONY) doctrine:database:drop --if-exists --force
	$(SYMFONY) doctrine:database:create --if-not-exists
	$(SYMFONY) doctrine:schema:update --force --no-interaction
	$(SYMFONY) doctrine:fixtures:load --no-interaction

db-update: ## Update database
db-update: flush .env vendor
	$(SYMFONY) doctrine:cache:clear-metadata
	$(SYMFONY) doctrine:schema:update --dump-sql
	$(SYMFONY) doctrine:schema:update --force --no-interaction

db-validate-schema: ## Validate the doctrine ORM mapping
db-validate-schema: .env vendor
	$(SYMFONY) doctrine:schema:validate

assets: ## Run Yarn to compile assets
assets: node_modules
	rm -rf public/build/*
	$(YARN) run build

assets-prod: ## Run Yarn to compile and minified assets
assets-prod: node_modules
	rm -rf public/build/*
	$(YARN) run build-prod

watch: ## Run Yarn in watch mode
watch: node_modules
	$(YARN) run watch

clear: ## clear cache
clear: .env vendor
	$(SYMFONY) cache:clear --env=dev

flush: ## Flush db
flush: .env vendor
	-$(SYMFONY) doctrine:cache:clear-query
	-$(SYMFONY) doctrine:cache:clear-metadata
	$(SYMFONY) doctrine:cache:clear-result

console: ## Console symfony
console: .env vendor
	$(SYMFONY) $(filter-out $@,$(MAKECMDGOALS))

sync-letsencrypt:
	ssh -i $(PEM_FILE_PATH) ubuntu@ec2-52-47-148-183.eu-west-3.compute.amazonaws.com sudo cp /etc/letsencrypt/live/disko.love/fullchain.pem /tmp/fullchain.pem
	ssh -i $(PEM_FILE_PATH) ubuntu@ec2-52-47-148-183.eu-west-3.compute.amazonaws.com sudo cp /etc/letsencrypt/live/disko.love/privkey.pem /tmp/privkey.pem
	scp -i $(PEM_FILE_PATH) ubuntu@ec2-52-47-148-183.eu-west-3.compute.amazonaws.com:/tmp/fullchain.pem docker/nginx/letsencrypt/fullchain.pem
	scp -i $(PEM_FILE_PATH) ubuntu@ec2-52-47-148-183.eu-west-3.compute.amazonaws.com:/tmp/privkey.pem docker/nginx/letsencrypt/privkey.pem

.PHONY: db assets watch clear flush console sync-letsencrypt assets-prod

##
## Tests
## -----
##

test: ## Run unit and functional tests
test: tu tf

tu: ## Run unit tests
tu: vendor
	$(EXEC_PHP) ./vendor/bin/phpunit tests --exclude-group functional

tf: ## Run functional tests
tf: vendor
	$(EXEC_PHP) ./vendor/bin/phpunit tests --group functional

.PHONY: tests tu tf

.env: 
	@if [ -f .env ]; \
	then\
		echo '\033[1;41m/!\ The .env.dist file has changed. Please check your .env file (this message will not be displayed again).\033[0m';\
		touch .env;\
		exit 1;\
	else\
		echo cp .env.dist .env;\
		cp .env.dist .env;\
		make install;\
		exit 1;\
	fi

.env-symfony: ./symfony/.env.dist
	@if [ -f symfony/.env ]; \
	then\
		echo '\033[0;32m The symfony .env already exist.\033[0m';\
	else\
		echo '\033[0;33m The symfony .env not exist.\033[0m';\
		cp symfony/.env.dist symfony/.env;\
		echo '\033[0;32m The symfony .env copy from .env.dist\033[0m';\
	fi


# rules based on files
composer.lock: symfony/composer.json
	$(COMPOSER) update --lock --no-scripts --no-interaction

vendor: symfony/composer.lock
	$(COMPOSER) install

node_modules: symfony/yarn.lock
	$(YARN) install
	@touch -c node_modules

yarn.lock: symfony/package.json
	$(YARN) install
	$(YARN) upgrade

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help

##
## Quality assurance
## -----------------
##

QA        = docker run --rm -v `pwd`:/project mykiwi/phaudit:7.1
ARTEFACTS = var/artefacts

lint: ## Lints twig and yaml files
lint: lt ly

lt: vendor
	$(SYMFONY) lint:twig templates

ly: vendor
	$(SYMFONY) lint:yaml config

security: ## Check security of your dependencies (https://security.sensiolabs.org/)
security: vendor
	$(EXEC_PHP) ./vendor/bin/security-checker security:check

phploc: ## PHPLoc (https://github.com/sebastianbergmann/phploc)
	$(QA) phploc src/

pdepend: ## PHP_Depend (https://pdepend.org)
pdepend: artefacts
	$(QA) pdepend \
		--summary-xml=$(ARTEFACTS)/pdepend_summary.xml \
		--jdepend-chart=$(ARTEFACTS)/pdepend_jdepend.svg \
		--overview-pyramid=$(ARTEFACTS)/pdepend_pyramid.svg \
		src/

phpmd: ## PHP Mess Detector (https://phpmd.org)
	$(QA) phpmd src text .phpmd.xml

php_codesnifer: ## PHP_CodeSnifer (https://github.com/squizlabs/PHP_CodeSniffer)
	$(QA) phpcs -v --standard=.phpcs.xml src

phpcpd: ## PHP Copy/Paste Detector (https://github.com/sebastianbergmann/phpcpd)
	$(QA) phpcpd src

phpdcd: ## PHP Dead Code Detector (https://github.com/sebastianbergmann/phpdcd)
	$(QA) phpdcd src

phpmetrics: ## PhpMetrics (http://www.phpmetrics.org)
phpmetrics: artefacts
	$(QA) phpmetrics --report-html=$(ARTEFACTS)/phpmetrics src

php-cs-fixer: ## php-cs-fixer (http://cs.sensiolabs.org)
	bin/php-cs-fixer fix src --dry-run --using-cache=no --verbose --diff

hook-git: ## Hook git to install
	@if [ -f .git/hooks/prepare-commit-msg ]; \
	then\
		echo '\033[0;32m Hook PSR2 already exist.\033[0m';\
	else\
	    ln  hooks/prepare-commit-msg .git/hooks/prepare-commit-msg;\
	fi

apply-php-cs-fixer: ## apply php-cs-fixer fixes
	@echo  "\x1b[32;01m####################################"
	@echo  "\x1b[33;01m     PSR2\x1b[32;01m PROGRAM PROCESSING"
	@echo  "\x1b[32;01m####################################\x1b[0m"
	@php symfony/bin/php-cs-fixer fix symfony/src --using-cache=no --verbose --diff --rules=@PSR2

twigcs: ## twigcs (https://github.com/allocine/twigcs)
	$(QA) twigcs lint templates

artefacts:
	mkdir -p $(ARTEFACTS)

.PHONY: lint lt ly phploc pdepend phpmd php_codesnifer phpcpd phpdcd phpmetrics php-cs-fixer apply-php-cs-fixer artefacts