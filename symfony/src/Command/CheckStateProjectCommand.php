<?php

namespace App\Command;

use App\Command\AbstractCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\Project;
use App\Event\ProjectEvent;
use App\Utils\Managers\ProjectManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

class CheckStateProjectCommand extends AbstractCommand
{
    protected static $defaultName = 'disko:check:projects';
    /**
     * @var ProjectManager
     */
    private $projectManager;

    /**
     * checkStateProjectCommand constructor.
     *
     * @param ProjectManager $projectManager
     */
    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, ProjectManager $projectManager)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->projectManager = $projectManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('lock accepted projects (7 days before start).')
        ;
    }

    /**
     * @return int|void|null
     * @throws \Exception
     */
    protected function executeCommand()
    {
        $projects = $this->projectManager->findBy(['state' => Project::STATE_HUB_ACCEPTED]);
        $dateCheck = new \DateTime();
        $dateCheck->modify('+7 day');

        /** @var Project $project */
        foreach ($projects as $project) {
            if ($project->getDateStart()->getTimestamp() <= $dateCheck->getTimestamp()) {
                $project->setState(Project::STATE_HUB_ACCEPTED_FIXED);
                $this->projectManager->save($project);
            }
        }
    }
}
