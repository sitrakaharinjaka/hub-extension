<?php

namespace App\Repository;

use App\Entity\MyBoxesBox;
use App\Repository\BaseRepository;

/**
 * Class MyBoxesBoxRepository
 */
class MyBoxesBoxRepository extends BaseRepository
{

    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'e';
                if ($key == 'id') {
                    $alias = 'e';
                }

                if ($key != 'email') {
                    $qb->andWhere($alias . '.' . $key . ' LIKE :' . $key);
                    $qb->setParameter($key, '%' . $filter . '%');
                } else {
                    $qb->leftJoin('e.boxUsers', 'u');
                    $qb->leftJoin('u.user', 'us');
                    $qb->andWhere($qb->expr()->in('us.email', array($filter)));
                }
            }
        }

        $query = $qb->getQuery();

        return $query;
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function queryBuilderAll($user)
    {
        $qb = $this->createQueryBuilder('b');

        $qb
            ->select('b')
            ->leftJoin('b.boxUsers', 'bu')
            ->leftJoin('bu.user', 'u')
            ->andWhere('u.id = :id')
            ->setParameter('id', $user->getId())
            ;
        return $qb;
    }

    /**
     * Find all boxes it can be select for bring home
     *
     * @return mixed
     */
    public function queryBuilderBringable($user)
    {
        $qb = $this->createQueryBuilder('b');

        $qb
            ->select('b')
            ->leftJoin('b.boxUsers', 'bu')
            ->leftJoin('bu.user', 'u')
            ->andWhere('u.id = :id')
            ->andWhere($qb->expr()->in('b.state', [MyBoxesBox::STATE_BOX_VEOLOG_STOCK]))
            ->setParameter('id', $user->getId())
            ;
        return $qb;
    }

    public function isBringable($id)
    {
        $qb = $this->createQueryBuilder('b');

        $qb
            ->select('b')
            ->andWhere('b.id = :id')
            ->andWhere($qb->expr()->in('b.state', [MyBoxesBox::STATE_BOX_VEOLOG_STOCK]))
            ->setParameter('id', $id)
            ;
        return $qb->getQuery()->getOneOrNullResult() !== null;
    }

    /**
     * Find all boxes it can be select for bring home
     *
     * @return mixed
     */
    public function queryBuilderBackable($user)
    {
        $qb = $this->createQueryBuilder('b');

        $qb
            ->select('b')
            ->leftJoin('b.boxUsers', 'bu')
            ->leftJoin('bu.user', 'u')
            ->andWhere('u.id = :id')
            ->andWhere($qb->expr()->in('b.state', [MyBoxesBox::STATE_BOX_USER_STOCK, MyBoxesBox::STATE_BOX_VEOLOG_SENT]))
            ->setParameter('id', $user->getId())
            ;
        return $qb;
    }

    public function isBackable($id)
    {
        $qb = $this->createQueryBuilder('b');

        $qb
            ->select('b')
            ->andWhere('b.id = :id')
            ->andWhere($qb->expr()->in('b.state', [MyBoxesBox::STATE_BOX_USER_STOCK, MyBoxesBox::STATE_BOX_VEOLOG_SENT]))
            ->setParameter('id', $id)
            ;
        return $qb->getQuery()->getOneOrNullResult() !== null;
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAllPerso($user)
    {
        $qb = $this->createQueryBuilder('e');

        $qb
            ->select('e')
            ->leftJoin('e.boxUsers', 'bu')
            ->leftJoin('e.boxUsers', 'buCount')
            ->groupBy('e.id')
            ->having('COUNT(buCount.id) = 1')
            ;

        $qb->andWhere($qb->expr()->in('bu.user', [$user->getId()]));

        $query = $qb->getQuery();
        return $query->getResult();
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAllTeam($user)
    {
        $qb = $this->createQueryBuilder('e');

        $qb
            ->select('e')
            ->leftJoin('e.boxUsers', 'bu')
            ->leftJoin('e.boxUsers', 'buCount')
            ->groupBy('e.id')
            ->having('COUNT(buCount.id) > 1');

        $qb->andWhere($qb->expr()->in('bu.user', [$user->getId()]));

        $query = $qb->getQuery();
        return $query->getResult();
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findForView($id)
    {
        $qb = $this->createQueryBuilder('e');

        $qb
            ->select('e', 'bm', 'bu')
            ->leftJoin('e.boxMedias', 'bm')
            ->leftJoin('e.boxUsers', 'bu')
            ->where('e.id= :id')
            ->setParameter('id', $id)
            ;

        $query = $qb->getQuery();
        return $query->getSingleResult();
    }

    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'p')
            ->leftJoin('e.product', 'p')
            ->where('e.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        return $query->getOneOrNullResult();
    }

    /**
     *
     * @return mixed
     */
    public function checkBarcodeFree($code)
    {
        $qb = $this->createQueryBuilder('b');
        return $qb
            ->select('count(b.id)')
            ->where('b.barcode = :barcode')
            ->setParameter('barcode', $code)
            ->andWhere($qb->expr()->notIn('b.state', [MyBoxesBox::STATE_BOX_USER_STOCK, MyBoxesBox::STATE_BOX_VEOLOG_SENT]))
            ->getQuery()
            ->getSingleScalarResult()
            == 0;
    }

    /**
     *
     * @return mixed
     */
    public function findBoxMemberSince($nbHour)
    {
        $qb = $this->createQueryBuilder('b');
        $qb ->select('b')
            ->leftJoin('b.boxUsers', 'bu')
            ->where('b.state = :state')
            ->andWhere("b.stateDate <= DATE_ADD(CURRENT_DATE(), '-7', 'day')")
            ->setParameter('state', MyBoxesBox::STATE_BOX_USER_STOCK);
        ;
        $query = $qb->getQuery();
        return $query->getResult();
    }

    private function getQueryNbBox($start, $end, $stockInVeolog = true)
    {
      if($stockInVeolog == true){
        return $this->createQueryBuilder('b')
          ->select('count(distinct(b.id)) as cpt')
          ->innerJoin('b.boxUsers', 'bu')
          ->leftJoin('b.boxUsers', 'bu2', 'WITH', 'bu.id <> bu2.id')
          ->andWhere('b.state = :state')
          ->andWhere("b.created >= :start")
          ->andWhere("b.created <= :end")
          ->setParameter('state', 'stock_in_veolog')
          ->setParameter('start', $start)
          ->setParameter('end', $end);
      }else{
        return $this->createQueryBuilder('b')
          ->select('count(distinct(b.id)) as cpt')
          ->innerJoin('b.boxUsers', 'bu')
          ->leftJoin('b.boxUsers', 'bu2', 'WITH', 'bu.id <> bu2.id');
          //->andWhere('b.state = :state')
          //->andWhere("b.created >= :start")
          //->andWhere("b.created <= :end")
          //->setParameter('state', 'stock_in_veolog')
          //->setParameter('start', $start)
          //->setParameter('end', $end);
      }

    }

    /**
     * Count alone
     */
    public function nbBoxAlone($start, $end, $stockInVeolog = true)
    {
        $qb = $this->getQueryNbBox($start, $end, $stockInVeolog);
        $qb->andWhere("bu2.id IS NULL");
        $query = $qb->getQuery();
        return $query->getSingleScalarResult();
    }

    /**
     * Count team
     */
    public function nbBoxTeam($start, $end, $stockInVeolog)
    {
        $qb = $this->getQueryNbBox($start, $end, $stockInVeolog);
        $qb->andWhere("bu2.id IS NOT NULL");
        $query = $qb->getQuery();
        return $query->getSingleScalarResult();
    }

    /**
     * Count team
     */
    public function nbBoxByType($start, $end, $stockInVeolog = true)
    {
        $qb = $this->createQueryBuilder('b')
            ->select('count(distinct(b.id)) as cpt, p.sku as type')
            ->innerJoin('b.product', 'p');

            if($stockInVeolog == true){
              $qb->andWhere('b.state = :state');
              $qb->setParameter('state', 'stock_in_veolog');
            }
            $qb->groupBy('p.sku');

      if($stockInVeolog == true) {
        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("b.created >= :start");
        $qb->andWhere("b.created <= :end");
      }

        $query = $qb->getQuery();

        $result = $query->getScalarResult();
        return $result;
    }


    /**
     * Box in sleep
     */
    public function nbBoxInSleep($limit = 15, $barcode = "")
    {
        $qb = $this->createQueryBuilder('b')
            ->select('b, p')
            ->innerJoin('b.product', 'p')
            ->andWhere('b.state = :state')
            ->setParameter('state', 'stock_in_veolog')
            ->addOrderBy('b.updated', 'asc')
            ->setMaxResults($limit);

        if (!empty($barcode)) {
            $qb->andWhere('b.barcode LIKE :barcode');
            $qb->setParameter('barcode', '%'.$barcode.'%');
        }


        $query = $qb->getQuery();

        $result = $query->getResult();
        return $result;
    }


    /**
     * Box movement
     */
    public function nbBoxMovement($start, $end, $limit = 15, $barcode = "", $type = "", $sort = "desc")
    {
        $qb = $this->createQueryBuilder('b')
            ->select('distinct(b.id) as id, b.barcode, u.email as email, count(l.id) as cpt')
            ->leftJoin('b.user', 'u')
            ->groupBy('b');

        if (!empty($type)) {
            $qb->leftJoin('b.logs', 'l', 'WITH', 'l.info = :info');
            $qb->setParameter('info', $type);
        } else {
            $qb->leftJoin('b.logs', 'l');
        }

        if (!empty($barcode)) {
            $qb->andWhere('b.barcode LIKE :barcode OR u.email LIKE :email');
            $qb->setParameter('barcode', '%'.$barcode.'%');
            $qb->setParameter('email', '%'.$barcode.'%');
        }

        $qb->setMaxResults($limit);
        $qb->orderBy('cpt', $sort);

        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("b.created >= :start");
        $qb->andWhere("b.created <= :end");

        $query = $qb->getQuery();

        $result = $query->getScalarResult();
        return $result;
    }

    /**
     * Top boxes by storage delay at 142
     */
    public function topByStorageDelay($start, $end, $limit = 15)
    {
        $qb = $this->createQueryBuilder('b');
        $qb ->select('b.barcode, u.email as email, DATEDIFF(:now, b.stateDate) as delay')
            ->leftJoin('b.user', 'u')
            ->where('b.state = :state')
            ->setParameter('state', MyBoxesBox::STATE_BOX_USER_STOCK)
            ->setParameter('now', new \DateTime())
            ->addOrderBy('b.stateDate', 'desc')
            ->setMaxResults($limit);

        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("b.created >= :start");
        $qb->andWhere("b.created <= :end");

        $query = $qb->getQuery();
        return $query->getResult();
    }
}
