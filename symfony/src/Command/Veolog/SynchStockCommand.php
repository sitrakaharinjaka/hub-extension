<?php

namespace App\Command\Veolog;

use App\Command\Veolog\AbstractVeologCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\ImageGallery;
use App\Entity\Media;
use App\Entity\Product;
use App\Entity\ProductTranslation;
use App\Entity\Project;
use App\Utils\Managers\UserManager;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\MediaManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockLineManager;
use App\Utils\Managers\StockManager;
use App\Utils\Managers\ProjectItemManager;
use App\Utils\Managers\StockEntryManager;
use DateTimeZone;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputOption;
use Exception;

/**
 * @property  mailer
 */
class SynchStockCommand extends AbstractVeologCommand
{

  /** @var  EntityManager */
    private $productManager;
    private $mediaManager;
    // private $projectManager;
    // private $projectItemManager;
    private $stockLineManager;
    private $stockEntryManager;
    private $stockManager;
    private $rootDir;

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, EntityManagerInterface $entityManager, ProductManager $productManager, StockLineManager $stockLineManager, StockEntryManager $stockEntryManager, MediaManager $mediaManager, StockManager $stockManager, KernelInterface $kernel)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->productManager   = $productManager;
        $this->stockLineManager = $stockLineManager;
        $this->stockEntryManager = $stockEntryManager;
        $this->stockManager     = $stockManager;
        $this->mediaManager     = $mediaManager;
        $this->entityManager    = $entityManager;
        $this->rootDir          = $kernel->getProjectDir();
    }

    protected static $defaultName = 'veolog:stock-update';

    protected function configure()
    {
        $this
            ->setDescription('Synch veolog stock product')
            ->addOption(
                'vlgenv',
                null,
                InputOption::VALUE_OPTIONAL,
                'Env veolog prod?',
                false
            )
            ->addOption(
                'force',
                null,
                InputOption::VALUE_OPTIONAL,
                'Force stock variation?',
                false
            )
            ->setHelp('This command allows you to synch veolog stock');
    }

    protected function convert($size)
    {
        $unit=array('b','kb','mb','gb','tb','pb');
        return @round($size/pow(1024, ($i=floor(log($size, 1024)))), 2).' '.$unit[$i];
    }

    /**
     * On veux les fichiers
     * - stock_AAMMJJHHMMSS.csv
     * - /XXX/stock_AAMMJJHHMMSS.csv
     * On ne veux pas :
     * - XXXstock_AMMJJHHMMSS.csv
     * - stock_AMMJJHHMMSS.xls
     * - stock_BOX_AAMMJJHHMMSS.csv
     */
    private function isCommandFile($filename)
    {
        $pattern='~^/?([a-zA-Z0-9_]*/)*stock_[0-9]*\.csv$~';
        return preg_match($pattern, $filename);
    }

    private function getFiles()
    {
        $this->log('info', ' --> Connecting FTP server ...');
        $ftpHandle = $this->connectFTP();
        $this->log('info', ' --> Connect FTP server : <info>success</info>');
        $this->log('info', ' --> Scaning directory to find csv ... ');
        $dirVlg = $this->getDirVlgOUT();
        $listFiles = ftp_nlist($ftpHandle, $dirVlg);
        $availableFiles = [];
        foreach ($listFiles as $file) {
            if ($this->isCommandFile($file)) {
                $availableFiles[] = $file;
            }
        }
        $this->log('info', ' --> Scan directory to find csv : <info>success</info> : ' . count($availableFiles) . ' file(s) find into directory');
        ftp_close($ftpHandle);
        return $availableFiles;
    }

    private function getFile($file)
    {
        $file = $this->getDirVlgOUT() . '/' . basename($file);

        $this->log('info', ' --> Connecting FTP server ...');
        $ftpHandle = $this->connectFTP();
        $this->log('info', ' --> Connect FTP server : <info>success</info>');
        $tmpHandle = fopen('php://temp', 'r+');
        $this->log('info', ' --> File ' . $file);
        $this->log('info', '    --> Downloading ...');
        if (ftp_fget($ftpHandle, $tmpHandle, $file, FTP_ASCII)) {
            $this->log('info', '    --> Download : <info>success</info>');
            rewind($tmpHandle);
            return $tmpHandle;
        }

        $this->log('info', '    --> Download : <error>error</error>');
        return false;
    }

    private function deleteFile($file)
    {
        $file = $this->getDirVlgOUT() . '/' . basename($file);

        $this->log('info', ' --> Connecting FTP server ...');
        $ftpHandle = $this->connectFTP();
        $this->log('info', ' --> Connect FTP server : <info>success</info>');
        $this->log('info', ' --> Deleting files ...');
        // Tente d'effacer le fichier $file
        if (ftp_delete($ftpHandle, $file)) {
            $this->log('info', ' --> Delete files : <info>success</info>');
            return true;
        }
        
        $this->log('info', ' --> Delete files : <error>error</error>');
        return false;
    }

    private function productMVS($filters, $product, $productDetail, $stock)
    {
        $this->log('info', '      --> Product MVS');
        $filters['state'] = 'out_of_use';
        //$filters['stock'] = $stock;
        $productMvsExist = $this->productManager->findOneTranslationBy($filters);
        //$productMvsExist = $this->productManager->findOneTranslationByStockEanState($filters['stock'], $filters['code'], $filters['state']);
        if (! empty($product)) {
            if (empty($productMvsExist)) {
                $this->log('info', '      --> Creating product with status out of use');
                $productTmp = $product[0];
                $productTmp->clearStockLines();
                $this->entityManager->detach($productTmp);
                $arrayCat         = [];
                $arrayImageGallery = new ArrayCollection();
                foreach ($productTmp->getTranslations() as $translation) {
                    $galleries = $translation->getImageGallery();
                    $translation->setImageGallery([]);
                    foreach ($galleries as $gallery) {
                        $this->entityManager->detach($gallery);
                        $this->entityManager->detach($gallery->getImage());
                        $mediaTmp = $gallery->getImage();
                        $mediaTmp->setId(null);
                        $this->mediaManager->save($mediaTmp);
                        $imageGallery = new ImageGallery();
                        $imageGallery->setImage($mediaTmp);
                        $imageGallery->setProductTranslation($translation);
                        $arrayImageGallery->add($imageGallery);
                    }
                    foreach ($translation->getCategories() as $category) {
                        $arrayCat[] = $category;
                    }
                    $translation->setImageGallery($arrayImageGallery);
                    $translation->setCategories($arrayCat);
                    $translation->setId(null);
                    $translation->setSlug($productTmp->translate('fr')->getName() . ' - ' . $productTmp->translate('fr')->getEanPart(). ' - ' . $productTmp->translate('fr')->getCode() . ' - non neuf');
                    $translation->setState('out_of_use');
                    $this->entityManager->detach($translation);
                }
                $product    = [];
                $product[0] = $this->productManager->save($productTmp);
                $stock = $this->findStock($productDetail[1]);

                return [
                    'stock' => $stock,
                    'product' => $product,
                ];
            }

            $product = $this->findProduct($productDetail[2], 'out_of_use');

            return [
                'stock' => $stock,
                'product' => $product,
            ];
        }

        $product = $this->findProduct($productDetail[2], 'out_of_use');
        $stock = $this->findStock($productDetail[1]);

        return [
            'stock' => $stock,
            'product' => $product,
        ];
    }

    private function findProduct($code, $state)
    {
        return $this->productManager->findOneTranslationBy(array(
            'code'  => $code,
            'state' => $state
        ));
    }

    private function findStock($code)
    {
        return $this->stockManager->findOneStockBy(array(
            'code' => $code
        ));
    }

    private function findProductAndStock($productDetail)
    {
        $product              = '';
        $filters              = [];
        $filtersStock         = [];
        $filters['code']      = $productDetail[2];
        $product = $this->productManager->findOneTranslationBy($filters);
        $filtersStock['code'] = $productDetail[1];
        $stock = $this->stockManager->findOneStockBy($filtersStock);

        if ($productDetail[5] == 'MVS') {
            $response = $this->productMVS($filters, $product, $productDetail, $stock);
            $stock = $response['stock'];
            $product = $response['product'];

            return [
                'stock' => $stock,
                'product' => $product,
            ];
        }

        if (empty($product)) {
            $product = $this->findProduct($productDetail[2], 'new');
        }
        if (empty($stock)) {
            $this->findStock($productDetail[1]);
        }

        return [
            'stock' => $stock,
            'product' => $product,
        ];
    }

    protected function executeCommand()
    {
        $this->entityManager->getConnection()->getConfiguration()->setSQLLogger(null);

        $this->log('info', 'Synchronisation de stock VEOLOG');

        //Check if we need to force validate the variation
        $forceValidation = $this->input == null ? false : $this->input->getOption('force', false);

        if($forceValidation){
            $this->log('info', '*** Ecrasement du stock forcé ***');
        }

        /**
         * @see https://bugkiller.disko.fr/issues/72765
         * All products not present in Veolog stock file should be considered out of stock (0)
         */
        $productsInStockAtVeolog = [];
        $hasStockFile = false;

        try {
            $avalaibleFiles = $this->getFiles();
            $stockVariations = [];
            $numFiles = count($avalaibleFiles);
            
            while ($numFiles > 0) {
                $file = $avalaibleFiles[0];
                $tmpHandle = $this->getFile($file);

                if ($tmpHandle) {
                    $hasStockFile = true;

                    while ($csvRow = fgetcsv($tmpHandle)) {
                        $this->entityManager->clear();
                        $this->log('info', '    --> New row (memory usage : ' . $this->convert(memory_get_usage(true)) . ')');
                        $typeLine = $csvRow[0][0];
                        
                        if ($typeLine == 'L') {
                            $productDetail = explode(';', $csvRow[0]);
                           
                            $this->log('info', '      --> Product ' . $productDetail[2] . ' in Stock ' . $productDetail[1]);
                            
                            $response = $this->findProductAndStock($productDetail);
                            $stock = $response['stock'];
                            $product = $response['product'];

                            if (empty($stock)) {
                                $this->log('info', '      --> <error>Stock does not exist</error>');
                                continue;
                            } elseif (empty($product)) {
                                $this->log('info', '      --> <error>Product does not exist</error>');
                                continue;
                            }

                            /**
                             * @see https://bugkiller.disko.fr/issues/72765
                             * All products not present in Veolog stock file should be considered out of stock (0)
                             */
                            if(!isset($productsInStockAtVeolog[$product[0]->getId()])){
                                $productsInStockAtVeolog[$product[0]->getId()] = [];
                            }

                            $productsInStockAtVeolog[$product[0]->getId()][] = $productDetail[1];

                            $dateNow = new DateTime('now');
                            $timezone = new DateTimeZone('Europe/Paris');
                            $dateNow->setTimezone($timezone);
                            $qtyReceivedStockEntry = (int) $this->stockEntryManager->findQuantityReceived($product[0], $dateNow, $stock[0]);
                            $qtyStock = (int) $this->stockLineManager->quantityOf($product[0], Project::CAVE_PANTIN, $dateNow, $stock[0]);

                            $qtyBefore = $qtyReceivedStockEntry + $qtyStock;
                            /**
                             * Archivage
                             */
                            // $this->stockLineManager->updateByStockAndProduct(
                            //         $stock[0],
                            //         $product[0],
                            //         Project::CAVE_PANTIN,
                            //         true
                            // );

                            //if ($productDetail[4] != "0") {
                                /**
                                 * @see https://bugkiller.disko.fr/issues/70744
                                 * If Veolog's stock is not equal to our's, create a stock variation
                                 */

                                if ($qtyBefore != $productDetail[4]) {
                                    $this->log('info', '[' . $productDetail[1] . '] ' . $productDetail[2] . " - Variation de stock - Stock Hub142 : " . $qtyBefore . " / Stock Veolog : " . $productDetail[4]);

                                    $variation = (int) $productDetail[4] - (int) $qtyBefore;

                                    $this->stockLineManager->addLine(
                                        $stock[0],
                                        $product[0],
                                        "Stock Hub142 : " . $qtyBefore . " / Stock Veolog : " . $productDetail[4],
                                        $variation,
                                        $dateNow,
                                        Project::CAVE_PANTIN,
                                        null,
                                        'Veolog',
                                        false,
                                        true, // isVariation
                                        $forceValidation
                                    );

                                    $stockVariations[$productDetail[2]] = [
                                        'product' => $product[0]->translate('fr')->getName(),
                                        'stock' => $productDetail[1],
                                        'variation' => "Stock Hub142 : " . $qtyBefore . " / Stock Veolog : " . $productDetail[4]
                                    ];
                                }else{
                                    $this->log('info', '[' . $productDetail[1] . '] ' . $productDetail[2] . " - Stocks Hub142 et Veolog ISO : " . $productDetail[4]);
    
                                    $this->stockLineManager->addLine(
                                        $stock[0],
                                        $product[0],
                                        "Stocks Hub142 et Veolog ISO : " . $qtyBefore,
                                        0,
                                        $dateNow,
                                        Project::CAVE_PANTIN,
                                        null,
                                        'Veolog',
                                        false,
                                        true
                                    );
                                }

                            //}
                        }
                    }
                }

                fclose($tmpHandle);
                $this->deleteFile($file);
                $avalaibleFiles = $this->getFiles();
                $numFiles = count($avalaibleFiles);
            }

            /**
             * @see https://bugkiller.disko.fr/issues/72765
             * All products not present in Veolog stock file should be considered out of stock (0)
             */

            $hasStockFile = false;//Disable temporarily
            if ($hasStockFile) {
                $this->log('info', "      --> Traitement des produits hors stock");
                $allProducts = $this->productManager->findAll();
                $allStocks = $this->stockManager->findAllCodes(Project::CAVE_PANTIN);
                $productsOutOfStock = [];

                foreach ($allProducts as $hubProduct) {
                    $stocksAffected = [];

                    if (!array_key_exists($hubProduct->getId(), $productsInStockAtVeolog)) {
                        $stocksAffected = $allStocks;
                    } else {
                        foreach ($allStocks as $hubStock) {
                            if (!in_array($hubStock, $productsInStockAtVeolog[$hubProduct->getId()])) {
                                $stocksAffected[] = $hubStock;
                            }
                        }
                    }

                    $productsOutOfStock[$hubProduct->getId()] = $stocksAffected;

                    foreach ($stocksAffected as $stockAffected) {
                        $stockObj = $this->stockManager->findOneStockBy(['code' => $stockAffected]);
                        $stockObj = is_array($stockObj) && isset($stockObj[0])? $stockObj[0]: $stockObj;

                        //Check if product belongs to this stock - @see https://bugkiller.disko.fr/issues/72765#note-14
                        if(!$this->stockLineManager->checkLinkedToStock($hubProduct, $stockObj)){
                            //$this->log('info', '[' . $stockAffected . '] ' . $hubProduct->translate('fr')->getCode() . " pas associé à cette entité");
                            continue;
                        }

                        $currentQtyReceivedStockEntry = (int) $this->stockEntryManager->findQuantityReceived($hubProduct, $dateNow, $stockObj);
                        $currentQtyStock = (int) $this->stockLineManager->quantityOf($hubProduct, Project::CAVE_PANTIN, $dateNow, $stockObj);

                        $currentQtyBefore = $currentQtyReceivedStockEntry + $currentQtyStock;

                        if ($currentQtyBefore != 0) {
                            $this->log('info', '[' . $stockAffected . '] ' . $hubProduct->translate('fr')->getCode() . " - Variation de stock - Stock Hub142 : " . $currentQtyBefore . " / Stock Veolog : 0");

                            $currentVariation = 0 - (int) $currentQtyBefore;

                            $this->stockLineManager->addLine(
                                $stockObj,
                                $hubProduct,
                                "Stock Hub142 : " . $currentQtyBefore . " / Stock Veolog : 0",
                                $currentVariation,
                                $dateNow,
                                Project::CAVE_PANTIN,
                                null,
                                'Veolog',
                                false,
                                true, // isVariation
                                $forceValidation
                            );

                            $stockVariations[$hubProduct->translate('fr')->getCode()] = [
                                'product' => $hubProduct->translate('fr')->getName(),
                                'stock' => $stockAffected,
                                'variation' => "Stock Hub142 : " . $currentQtyBefore . " / Stock Veolog : 0"
                            ];
                        } else {
                            //$this->log('info', '[' . $stockAffected . '] ' . $hubProduct->translate('fr')->getCode() . " - Stocks Hub142 et Veolog ISO : 0");

                            $this->stockLineManager->addLine(
                                $stockObj,
                                $hubProduct,
                                "Stocks Hub142 et Veolog ISO : 0",
                                0,
                                $dateNow,
                                Project::CAVE_PANTIN,
                                null,
                                'Veolog',
                                false,
                                true
                            );
                        }
                    }
                }

                $this->log('info', count($productsOutOfStock) . ' produit(s) non présent dans le fichier');
            }

            if(count($stockVariations) > 0){
                $this->log('info', count($stockVariations) . ' produit(s) avec des variations de stock. Envoi de l\'email');

                $this->mailer->sendSynchStockErrors($stockVariations);
            }
            
        } catch (Exception $e) {
            echo "Failure: " . $e->getMessage();
        }
    }
}
