<?php

namespace App\Command\Veolog\MyBoxes;

use App\Utils\Managers\MyBoxesBoxManager;
use App\Utils\Services\MailerService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @property  mailer
 */
class Reminder72Command extends Command
{
    private $mailer;
    private $boxManager;

    protected static $defaultName = 'myboxes:reminder-72';

    public function __construct(MyBoxesBoxManager $boxManager, MailerService $mailer)
    {
        parent::__construct();
        $this->mailer = $mailer;
        $this->boxManager = $boxManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Synch veolog stock boxes')
            ->setHelp('This command allows you to synch veolog stock');
    }

    private function getBoxInError()
    {
        return $this->boxManager->findBoxMemberSince(72);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $output->writeln('Envoi de mail de reminder aux utilisateurs ayant des boxes depuis plus de 72h.');
            $boxes = $this->getBoxInError();
            foreach ($boxes as $box) {
                $output->writeln('Traitement de la box <comment>#' . $box->getId() . '</comment>');
                $this->mailer->sendMailMyBoxesReminder72Sent($box);
            }
            $output->writeln('<info>Fin</info>');
        } catch (\Exception $e) {
            $output->writeln('<error>ERREUR</error> : ' . $e->getMessage());
        }
    }
}
