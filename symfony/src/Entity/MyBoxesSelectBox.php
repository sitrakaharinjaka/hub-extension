<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 */
class MyBoxesSelectBox
{

    /**
     * @var MyBoxesBox|null
     */
    protected $box;

    /**
     * @return MyBoxesBox
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * @param MyBoxesBox $box
     */
    public function setBox(?MyBoxesBox $box)
    {
        $this->box = $box;
    }
}
