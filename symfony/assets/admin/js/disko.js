var lastSelectedFile = null;
var currentDropZoneSelect = null;
$( document ).ready(function() {
  $eventSelect = $("#product_translations_fr_categories");
  $eventSelect.on("change", function (e) {
    var selectedCategories = $('#product_translations_fr_categories').select2('data')
    console.log(selectedCategories)
    var catBouteilleSelected = false
    selectedCategories.forEach((item, index) => {
      if(item.id == 5){
        catBouteilleSelected = true
      }
    })

    if(catBouteilleSelected == true){
      jQuery('#product_translations_fr_isExpendable').prop('checked', true);
      $('[for="product_translations_fr_wineColor"]').addClass('required');
      $('[for="product_translations_fr_wineCategory"]').addClass('required');
    }else{
      jQuery('#product_translations_fr_isExpendable').prop('checked', false);
      $('[for="product_translations_fr_wineColor"]').removeClass('required');
      $('[for="product_translations_fr_wineCategory"]').removeClass('required');
    }

  });

    $('.btn-search-open').click(function(e){
        e.preventDefault();
        $('.search-zone').fadeToggle();
    });

    // CHANGE DEPARTMENT
    $("body").on("change", "#department", function(e){
        var option = $('#department option:selected');
        var url = $(this).data('url');
        $.ajax({
            url: url,
            method: 'POST',
            data: {departmentNumber: option.val()},
            success: function(data){
                if(data.code == 200) {
                    $('#region_list option[value="'+ data.region.regionName +'"]').prop('selected', true);
                }
            }
        })
    });

    $("body").on("change", "#region_list", function(e){
        var option = $('#region_list option:selected');
        var url = $(this).data('url');
        $.ajax({
            url: url,
            method: 'POST',
            data: {regionCode: option.val()},
            success: function(data){
                if(data.code == 200) {
                    $('#department').remove();
                    for(var i = 0; i < data.departments.length; i++){
                        $('#department option').remove();
                        $('#department').append('<option value="'+ data.departments.i.departmentNumber +'">'+ data.departments.i.departmentName +'</option>');
                    }
                }
            }
        })
    });

    $('body').on('click', '.delete-button-media', function(e){
        e.preventDefault();
        e.stopPropagation();

        if (confirm("La suppression d'un média affectera l'ensemble des éléments ou il est utilisé, les rendant sans média. Confirmez-vous l'opération ?"))
        {
            var $_this = $(this);
            $_this.parents('.widget-products-item').find('.img-select-info').css('background', "mistyrose").find('*').each(function () {
                this.style.setProperty('color', 'red', 'important');
            });
            $_this.parents('.widget-products-item').find('.widget-products-image').css('border', '1px solid mistyrose');
            $_this.parents('.widget-products-item').css('opacity', '1');
            $_this.parents('.widget-products-item').find('.supp-media-actions').remove();
            $.ajax({
                url: $(this).attr('href'),
            }).done(function(html) {
            });
        }
    });

    $('.delete-button').click(function(){
        return confirm('Confirmez cette suppression ?');
    });

    $('.duplicate-button').click(function(){
        return confirm('Confirmez la duplication de cet élément ?');
    });

    $('.confirm-button').click(function(){
        return confirm('Confirmez cette action ?');
    });



    setTimeout(function(){
        $('.alert-layout').fadeOut(1000, function(){
        });
    }, 10000);


    reloadPlugins();

    $('body').on('click', '.photoPreview img', function(){
        var parent = $(this).parents('.photoSpace');
        parent.find('.photoInput').click();
    });

    $('body').on('change', '.photoInput', function(){
        var reader = new FileReader();
        var parent = $(this).parents('.photoSpace');
        reader.onload = function (e) {
            console.log(parent.find('.photoPreview img').length);
            parent.find('.photoPreview img').attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
    });

    $("body").on('change', '.urlinput', function() {
        if ($(this).val().indexOf("http://") !== 0 && $(this).val().indexOf("https://") !== 0) {
            $(this).val("http://" + $(this).val());
        }
    });

    $("body").on('change', '.gifinput', function() {
        if ($(this).val().indexOf("http://") !== 0 && $(this).val().indexOf("https://") !== 0) {
            $(this).val("http://" + $(this).val());
        }
    });


    $('.btn-delete-confirm').on('click', function (e) {
        e.preventDefault();
        var href = $(this).data('href');
        $('#link-delete').attr('href', href);
    });


    $('.btn-choice-locale').on('click', function (e) {
        e.preventDefault();
        var href = $(this).data('href');
        $('#modals-alerts-info .body-modal-ajax').html("<div class='modal-body'><img style=\"max-height: 50px\" src=\"/assets/admin/images/ajax-loader.svg?4\"></div>");
        $('#modals-alerts-info').find('form').attr('action', $(this).attr('href'));
        $.ajax({
            url: href,
        }).done(function(html) {
            $('#modals-alerts-info .body-modal-ajax').html(html);
        });

    });



    /**
     * MULTILNAGUAL TRANSVERSES
     */
    $('body').on('click', '.btn-multilingual', function(e){
        e.preventDefault();

        if($(this).hasClass('btn-warning'))
        {
            $(this).removeClass('btn-warning');
            $(this).parents('.form-group').removeClass('alert').removeClass('alert-warning');
        }
        else{
            $(this).addClass('btn-warning');
            $(this).parents('.form-group').addClass('alert').addClass('alert-warning');
        }

        var parents = $(this).parents(".well");
        var finder = parents.find('.btn-multilingual.btn-warning');
        if (parents.length <= 0)
        {
            parents = $(this).parents('form');
            finder = parents.find('.btn-multilingual.btn-warning').not('.well .btn-multilingual.btn-warning');
        }
        var val = parents.find('.transverseFields').val();
        val = {};

        finder.each(function(){
            var fields = $(this).data('fields');
            fields = fields.split('|');
            for(var j = 0; j< fields.length; j++)
            {
                val[fields[j]] = 1;
            }
        });

        val = JSON.stringify(val);
        parents.find('.transverseFields').val(val);
    });


    /**
     * END MULTILNAGUAL TRANSVERSES
     */
    $('.panel-filters-toggle').click(function(e){
        e.preventDefault();
        if (!$(this).hasClass('open'))
        {
            $('.panel-filters-toggle').parents('.panel').find('.panel-body').fadeIn();
            $(this).addClass('open');
        }
        else{
            $('.panel-filters-toggle').parents('.panel').find('.panel-body').hide();
            $(this).removeClass('open');
        }
    });


    // Video event youtube link
    $('body').on('change', '.video-link', function(e) { verifyVideoLink($(this)) });
    $('body').on('keyup', '.video-link', function(e) { verifyVideoLink($(this)) });

    $('body').on('change', '.select2-locales' ,function(e){
        e.preventDefault();
        var currentVal = $(this).val();

        $(this).find('option').each(function(){
            if ($(this).val() == currentVal)
            {
                window.location.replace($(this).data('href'));
            }
        });
    });


    // Map event gmap link
    $('body').on('change', '.map-link', function(e) { verifyMapLink($(this)) });
    $('body').on('keyup', '.map-link', function(e) { verifyMapLink($(this)) });


    // Path input
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };

    $('.path-input').each(function(){
        verifyPathInput($(this));
    });
    $('body').on('change', '.path-input', function(e) { verifyPathInput($(this)) });
    $('body').on('keyup', '.path-input', function(e) { verifyPathInput($(this)) });


    $('body').on('change', '.class-input', function(e) { verifyClassInput($(this)) });
    $('body').on('keyup', '.class-input', function(e) { verifyClassInput($(this)) });

    $('body').on('change', '.nows-input', function(e) { verifyNowsInput($(this)) });
    $('body').on('keyup', '.nows-input', function(e) { verifyNowsInput($(this)) });

    $('body').on('change', '.icon-preview-input', function(e) { verifyIconInput($(this)) });
    $('body').on('keyup', '.icon-preview-input', function(e) { verifyIconInput($(this)) });



    // IMG ADD
    $("body").on("click", ".media-add-disko", function(e) {
        e.preventDefault();
        var link = $(this);
        $("#mediaAddModal").modal("show");
        $('#mediaAddModal').hide();
        $("#mediaAddModal .modal-body").html('<p style="text-align:center"><img style="max-height: 50px" src="/assets/admin/images/ajax-loader.svg?4"></p>');

        $.ajax({
            url: link.attr('href'),
            type: 'GET'
        }).done(function(html){
            $('#mediaAddModal .modal-body').html(html).hide().fadeIn();
            reloadDropzone();
        });
    });

    // IMG SEO
    var $_this_after = null;
    $("body, .zone-update > .content").on("click", ".dz-update-disko, .media-seo-disko", function(e) {
        e.preventDefault();
        var link = $(this);
        $_this_after = $(this);
        $("#seoModal").modal("show");
        $('#seoModication').hide();
        $("#seoModal .modal-body").html('<p style="text-align:center"><img style="max-height: 50px" src="/assets/admin/images/ajax-loader.svg?4"></p>');

        $.ajax({
            url: link.attr('href'),
            type: 'GET'
        }).done(function(html){
            $('#seoModal .modal-body').html(html).hide().fadeIn();
            $('#seoModication').fadeIn();
        });
    });


    $('body').on('click', '#seoModication', function(e){
        e.preventDefault();

        var form = $('#mediaSaveSeo');
        $("#seoModal .modal-body").html('<p style="text-align:center"><img style="max-height: 50px" src="/assets/admin/images/ajax-loader.svg?4"></p>');

        setTimeout(function(){$('#closeMedia').click();}, 500);

        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: form.serialize(),
            success:function(response){
                $_this_after.parents('.dropzone-box').find('.input-dropzone-disko').find('input.url').val(response.url);
                $_this_after.parents('.dropzone-box').find('.input-dropzone-disko').find('input.title').val(response.title);
                $_this_after.parents('.dropzone-box').find('.input-dropzone-disko').find('input.alt').val(response.alt);
                $_this_after.parents('.dropzone-box').find('.input-dropzone-disko').find('input.legend').val(response.legend);

            }
        })
    });


    // SELECT MEDIATHEQUE
    $("body, .zone-update > .content").on("click", ".btn-upload", function(e) {
        e.preventDefault();
        var event = new Event('click');

        if ($(this).parents('.form-group').find('.dz-remove').length > 0)
        {
            if ($(this).parents('.form-group').find('.dz-remove')[0]){
                $(this).parents('.form-group').find('.dz-remove')[0].dispatchEvent(event);
            }
            $(this).parents('.form-group').find('.dz-remove').click();
        }

        $(this).parents('.form-group').find('.dropzone-box').click();
    });


    var $_this_after = null;
    $("body, .zone-update > .content").on("click", ".btn-mediatheque", function(e) {
        e.preventDefault();
        e.stopPropagation();

        currentDropZoneSelect = $(this);
        var link = $(this);
        $_this_after = $(this);
        $("#mediaModal").modal("show");
        $("#mediaModal .modal-body").html('<p style="text-align:center"><img style="max-height: 50px" src="/assets/admin/images/ajax-loader.svg?4"></p>');

        $.ajax({
            url: link.attr('href'),
            type: 'GET'
        }).done(function(html){
            $('#mediaModal .modal-body').html(html).hide().fadeIn();
        });
    });


    $("body").on("click", ".btn-mediacenter-change", function(e) {

        e.preventDefault();

        $('#mediaModal .modal-body .img-container-mediacenter').html('<p style="text-align:center"><img style="max-height: 50px" src="/assets/admin/images/ajax-loader.svg?4"></p>');
        $.ajax({
            url: $(this).attr('href'),
            type: 'PUT'
        }).done(function(html){
            $('#mediaModal .modal-body .img-container-mediacenter').html(html).hide().fadeIn();
        });

    });


    $("body").on("submit", "#mediacenter_search", function(e) {

        e.preventDefault();

        $('#mediaModal .modal-body .img-container-mediacenter').html('<p style="text-align:center"><img style="max-height: 50px" src="/assets/admin/images/ajax-loader.svg?4"></p>');
        $.ajax({
            url: $(this).attr('action'),
            type: 'GET',
            data: $(this).serialize()
        }).done(function(html){
            $('#mediaModal .modal-body .img-container-mediacenter').html(html).hide().fadeIn();
        });

    });

    $('body').on("click", ".btn-save, .btn-save-withcolor", function(){
        if ($(this).attr('disabled') != 'disabled')
        {
            if ($(this).hasClass("btn-save-withcolor"))
            {
                $('.state-action').val($(this).data('action'));

            }


            if ($(this).hasClass("btn-save-preview"))
            {
                if (confirm("Souhaitez-vous être redirigé vers une prévisualisation après l'enregistrement du brouillon ?"))
                {
                    $(this).parents('form').append('<input type="hidden" value="1" name="preview" />');
                }
            }

            $('.btn-save-withcolor').attr('disabled', 'disabled');
            $('.btn-save-withcolor').css('opacity', '0.5');
            $(this).attr('disabled', 'disabled');
            $(this).parents('form').submit();
            $(this).attr('style', 'background:transparent!important; border: none !important; opacity: 1 !important;');
            $(this).html('<img width="50px" src="/assets/admin/images/ajax-loader.svg?4" />');

        }
    });



    $('body').on("click", ".delete-menu, .delete-demo-menu", function(e){
        e.preventDefault();

        if (confirm("Confirmer la suppression ?"))
        {

            $("#modals-cat").modal("show");
            $('#modals-cat .body-modal-ajax').append('<p style="text-align: center;margin-bottom: 30px;"><img src="https://media1.giphy.com/media/99xodUrJqFGww/giphy.gif?cid=790b761155e25ee7fed9d0a482894ceb9a22335363c088ed&rid=giphy.gif" style="height: 400px;"/><br><br><em>Merci de patienter, <b>Grisoux</b> mange les fichiers...</em></p>')

            if ($(this).find('a').length > 0)
            {
                window.location.href = $(this).find('a').attr('href');
            }
            else
            {
                window.location.href = $(this).attr('href');
            }
        }
    });


    $('body').on("click", '.createin-menu', function (e) {
        e.preventDefault();
        $("#modals-info").modal("show")
        var href = $(this).data('href');
        var action = $(this).attr('href');
        $('#modals-info .body-modal-ajax').html("<div class='modal-body'><img style=\"max-height: 50px\" src=\"/assets/admin/images/ajax-loader.svg?4\"></div>");
        $('#modals-info form').attr('action', action);

        $.ajax({
            url: href,
        }).done(function(html) {
            $('#modals-info .body-modal-ajax').html(html);
        });

    });



    $('body').on("click", ".disko-group-intl", function(e){
        e.preventDefault();
        e.stopPropagation();

        var input = $(this).parents('.form-group').find('.disko-group');
        var diskogroup = input.data('group');


        applyTransverse($(this), diskogroup, input);

    });



    $('body').on("click", ".disko-group-alone", function(e){
        e.preventDefault();
        e.stopPropagation();

        var input = $(this).parents('.form-group').find('.disko-group');
        var diskogroup = input.data('group');
        diskogroup = diskogroup.replaceAll("_disko_", '_'+$(this).data('locale')+'_');

        applyTransverse($(this), diskogroup, input);
    });



    $("body").on("submit", "#generate", function(e) {
        if ($(this)[0].checkValidity())
        {
            $('#modals-info .btn-generate').hide();
            $('#form-gen').hide();
            $('#modals-info .btn-default');
            $('#modals-info .body-modal-ajax').append('<p style="text-align: center;margin-bottom: 30px;"><img src="https://media2.giphy.com/media/JIX9t2j0ZTN9S/giphy.gif?cid=790b76115d43eb8d6d784d7777bfccbb&rid=giphy.gif" style="height: 400px;"/><br><br><em>Merci de patienter, <b>Félix</b> s\'occupe de tout...</em></p>')
        }
        else
        {
            e.preventDefault();
        }
    });


    $('body').on("click", ".btn-versions-open", function(e){
        e.preventDefault();
        $('.versions-zone').addClass('active');
        $('.versions-zone .versions-zone-body').html('<p style="text-align:center"><img style="max-height: 50px" src="/assets/admin/images/ajax-loader.svg?4"></p>');

        $.ajax({
            url: $(this).attr('href'),
        }).done(function(html) {
            $('.versions-zone .versions-zone-body').html(html);
        });
    });

    $('body').on("click", ".versions-zone-close", function(e){
        e.preventDefault();
        $('.versions-zone').removeClass('active');
        $('.versions-zone .versions-zone-body').html('');
    });



});

function applyTransverse(element, diskogroup, input)
{
    var mediaDropzone = false;
    if (input.hasClass('input-dropzone-disko'))
    {
        input = input.find('input');
        diskogroup = diskogroup+' input';
        mediaDropzone = true;
    }

    // Manage Select
    if (input.prop("tagName").toLowerCase() == 'select')
    {
        var val = input.val();
        if (Array.isArray(val))
        {
            $('.'+diskogroup+' option').prop('selected', false);
            for(var i = 0; i < val.length; i++)
            {
                $('.'+diskogroup+' option[value="'+val[i]+'"]').prop('selected', true);
            }
        }
        else
        {
            var val = input.val();
            $('.'+diskogroup+' option').prop('selected', false);
            $('.'+diskogroup+' option[value="'+val+'"]').prop('selected', true);
        }
        reloadPlugins();
    }
    else if (input.prop("tagName").toLowerCase() == 'textarea')
    {

        if (input.hasClass('editor-wysiwyg'))
        {
            $('.'+diskogroup).each(function (e) {
                if (CKEDITOR.instances[this.id]) {
                    CKEDITOR.instances[this.id].setData(input.val());
                }
            });
        }
        else {

            $('.'+diskogroup).val(input.val());
        }
    }
    else {
        switch(input.attr('type'))
        {
            case 'checkbox':
                $('.'+diskogroup).prop('checked', input.prop('checked'));
            break;
            default:
                $('.'+diskogroup).val(input.val());

                if (mediaDropzone)
                {
                    if (input.val() == "")
                    {
                        $('.'+diskogroup).parents('.dropzone-box').find('.dz-preview').remove();
                        $('.'+diskogroup).parents('.dropzone-box').removeClass("dz-started");
                    }
                    else
                    {
                        var html = input.parents('.dropzone-box').find('.dz-preview').html();
                        $('.'+diskogroup).parents('.dropzone-box').find('.dz-preview').remove();
                        $('.'+diskogroup).parents('.dropzone-box').addClass("dz-started");
                        $('.'+diskogroup).parents('.dropzone-box').append("<div class=\"dz-preview dz-processing dz-image-preview dz-success\">"+html+"</div>");
                    }
                }
            break;
        }
        reloadPlugins();
    }

    var _this = element;
    _this.find('.load').fadeIn();
    setTimeout(function(){
        _this.find('.load').fadeOut();
    }, 1000);
}



function loadWysiwyg(element, onChange)
{
    element.each(function (e) {
        if (CKEDITOR.instances[this.id]) {
            CKEDITOR.instances[this.id].destroy();
        }
    });
    element.each(function (e) {
        var config = "/assets/admin/ckeditor/ckeditor_config.js";

        var instance = CKEDITOR.replace(this.id, {
            customConfig: config
        });
        if (onChange) {
            instance.on('change', function() { instance.updateElement() });
        }
    });

    return true;
}


function reloadPlugins()
{
    if($('.editor-wysiwyg').length) {
        loadWysiwyg($('.editor-wysiwyg'), true);
    }

    // Single select
    $(".select2-single").select2({
        allowClear: true
    });

    $(".select2-single-version").select2({
        allowClear: true
    });

    $(".select2-multiple").select2({
        allowClear: true
    });

    $('.date').datepicker({
        format: 'dd/mm/yyyy'
    });
}

function imageExists(image_url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', image_url, false);
    http.send();
    return http.status != 404;

}

function verifyIconInput(element)
{
    var iconClass = element.val();
    element.parents('.row').find('.menu-icon').attr('class', 'menu-icon px-nav-icon '+iconClass);
}

function verifyPathInput(element)
{
    var link = element.val();
    link = link.replaceAll("https://", "");
    link = link.replaceAll("http://", "");
    link = link.toLowerCase();
    link = link.replaceAll(" ", "-");

    if (link.charAt(0) == "/")
    {
        link = link.substring(1);
    }

    element.val(link);
}



function verifyClassInput(element)
{
    var link = element.val();
    link = link.replaceAll(" ", "");
    link = link.replaceAll("_", "");
    link = link.replaceAll("-", "");
    link = link.replaceAll("/", "");


    if (link != '')
    {
        link = link.charAt(0).toUpperCase() + link.slice(1);
    }

    element.val(link);
}

function verifyNowsInput(element)
{
    var link = element.val();
    link = link.replaceAll(" ", "_");
    link = link.replaceAll("-", "_");
    link = link.replaceAll("/", "");

    if (link != '')
    {
        link = link.charAt(0).toLowerCase() + link.slice(1);
    }

    element.val(link);
}

function verifyMapLink(element)
{
    var link = element.val();

    link = link.split('"');
    if (link.length > 1)
    {
        link = link[1];
    }
    element.parents('.map-link-parents').find('.preview').find('iframe').attr('src', link);
    element.val(link);
}


function verifyVideoLink(element)
{
    var link = element.val();
    link = link.replace("www.youtube.com/watch?v=", "www.youtube.com/embed/");
    element.parents('.video-link-parents').find('.preview').find('iframe').attr('src', link);
    element.val(link);
}

