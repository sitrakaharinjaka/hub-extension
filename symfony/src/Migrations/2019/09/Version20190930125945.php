<?php declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190930125945 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE lexik_maintenance');
        $this->addSql('ALTER TABLE dk_product DROP acquisition_year, DROP place, DROP material, DROP color, DROP state, DROP reference_sap, DROP reference_provider, DROP production_year, DROP packaging, DROP valorization, DROP is_fragile, DROP is_expendable, DROP code, DROP weight, DROP width, DROP height, DROP depth');
        $this->addSql('ALTER TABLE dk_product_translation ADD code VARCHAR(255) NOT NULL, ADD acquisition_year VARCHAR(255) NOT NULL, ADD place VARCHAR(255) DEFAULT NULL, ADD material VARCHAR(255) NOT NULL, ADD color VARCHAR(255) NOT NULL, ADD state VARCHAR(255) NOT NULL, ADD reference_sap VARCHAR(255) DEFAULT NULL, ADD reference_provider VARCHAR(255) DEFAULT NULL, ADD production_year VARCHAR(255) NOT NULL, ADD packaging VARCHAR(255) NOT NULL, ADD valorization VARCHAR(255) DEFAULT NULL, ADD is_fragile TINYINT(1) DEFAULT \'0\' NOT NULL, ADD is_expendable TINYINT(1) DEFAULT \'0\' NOT NULL, ADD weight DOUBLE PRECISION DEFAULT NULL, ADD width DOUBLE PRECISION DEFAULT NULL, ADD height DOUBLE PRECISION DEFAULT NULL, ADD depth DOUBLE PRECISION DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE lexik_maintenance (ttl DATETIME DEFAULT NULL) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE dk_product ADD acquisition_year VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD place VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD material VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD color VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD state VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD reference_sap VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD reference_provider VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD production_year VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD packaging VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD valorization VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD is_fragile TINYINT(1) DEFAULT \'0\' NOT NULL, ADD is_expendable TINYINT(1) DEFAULT \'0\' NOT NULL, ADD code VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD weight DOUBLE PRECISION DEFAULT NULL, ADD width DOUBLE PRECISION DEFAULT NULL, ADD height DOUBLE PRECISION DEFAULT NULL, ADD depth DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE dk_product_translation DROP code, DROP acquisition_year, DROP place, DROP material, DROP color, DROP state, DROP reference_sap, DROP reference_provider, DROP production_year, DROP packaging, DROP valorization, DROP is_fragile, DROP is_expendable, DROP weight, DROP width, DROP height, DROP depth');
    }
}
