<?php

/**
 * Form type
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Type\Project;

use App\Entity\BillingCenter;
use App\Entity\Project;
use App\Entity\Service;
use App\Entity\ServiceTranslation;
use App\Utils\Managers\LocaleManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\Fo;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 *  Form Type
 */
class CreateProjectType extends AbstractType
{
    protected $localeManager;

    /**
     * @param LocaleManager $localeManager
     */
    public function __construct(SessionInterface $session, LocaleManager $localeManager)
    {
        $this->localeManager = $localeManager;
        $this->session = $session;
    }


    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('cave', HiddenType::class);
        $builder ->add('name', TextType::class, [
            'label' => "Nom de votre commande",
            'attr' => [
                'class' => 'field'
            ]
        ]);


        $stock = $this->session->get('current_stock', null);
        $builder->add('billingCenter', EntityType::class, [
            'class' => BillingCenter::class,
            'query_builder' => function (EntityRepository $erb) use ($stock) {
                $qb = $erb->createQueryBuilder('u');
                $qb->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale')
                    ->setParameter('locale', 'fr')
                    ->leftJoin('u.stocks', 's')
                    ->andWhere('s.id = :id_stock ')
                    ->setParameter('id_stock', $stock->getId())
                    ->orderBy('t.name', 'ASC');

                return $qb;
            },
            "placeholder" => "Sélectionner un centre de facturation et/ou centre de coût",
            'choice_label' => 'name',
            'attr' => [
                'class' => 'field'
            ],
            'label' => 'Facturation et/ou centre de coût',
            'required' => true,
            'multiple' => false
        ]);

        $builder->add('dateStart', DateType::class, [
            'widget' => 'single_text',
            'label' => 'Date de livraison demandée',
            'html5' => false,
            'format' => 'dd/MM/yyyy',
            'attr' => [
                'class' => 'field datepicker jsDateStart no-weekend',
                'autocomplete' => 'off'
            ],
            'form_date' => true
        ]);

        $builder->add('hourStart', ChoiceType::class, [
            'label' => 'Heure de livraison demandée',
            'choices' => [
                '9h - 10h' => new \DateTime('2020/01/01 10:00:00.000'),
               '10h - 11h' => new \DateTime('2020/01/01 11:00:00.000'),
               '11h - 12h' => new \DateTime('2020/01/01 12:00:00.000'),
               '12h - 13h' => new \DateTime('2020/01/01 13:00:00.000'),
               '13h - 14h' => new \DateTime('2020/01/01 14:00:00.000'),
               '14h - 15h' => new \DateTime('2020/01/01 15:00:00.000'),
               '15h - 16h' => new \DateTime('2020/01/01 16:00:00.000'),
               '16h - 17h' => new \DateTime('2020/01/01 17:00:00.000'),
               '17h - 18h' => new \DateTime('2020/01/01 18:00:00.000'),
               '18h - 19h' => new \DateTime('2020/01/01 19:00:00.000'),
            ],
            'empty_data' => '5',
            'placeholder' => false,
            'attr' => [
                'class' => 'field'
            ]
        ]);

        $builder->add('needReturn', CheckboxType::class, [
            'required' => false,
            'label' => "Cochez si besoin d'un retour de marchandises ?",
            'form_focus_label' => false,
            'form_checkbox' => true
        ]);

        $builder->add('dateEnd', DateType::class, [
            'widget' => 'single_text',
            'label' => "Date de fin de commande",
            'html5' => false,
            'format' => 'dd/MM/yyyy',
            'attr' => [
                'class' => 'field datepicker jsDateEnd no-weekend',
                'autocomplete' => 'off'
            ],
            'form_date' => true
        ]);

        $builder->add('hourEnd', TimeType::class, [
            'label' => "Heure d'enlèvement souhaitée pour le retour de marchandises",
            'attr' => [
                'class' => 'hide'
            ],
            'form_time' => true,
            'minutes' => [
                0,
                30
            ],
            'hours' => range(8, 17),
        ]);

        $builder ->add('infoReturn', TextType::class, [
            'label' => "Information de contact",
            'required' => false,
            'attr' => [
                'class' => 'field',
            ]
        ]);

        $builder ->add('emailReturn', TextType::class, [
            'label' => "Email de contact",
            'required' => false,
            'attr' => [
                'class' => 'field',
            ]
        ]);

        $builder ->add('phoneReturn', TextType::class, [
            'label' => "Téléphone de contact *", // Required mais que si NeedReturn.
            'required' => false,
            'attr' => [
                'class' => 'field',
            ]
        ]);

        $builder ->add('recipientSociety', TextType::class, [
            'label' => "Société",
            'required' => false,
            'attr' => [
                'class' => 'field',
            ]
        ]);

        $builder ->add('recipientCountry', ChoiceType::class, [
            'label' => "Pays",
            'choices'  => [
                'France' => 'FR'
            ],
            'data' => 'FR',
            'attr' => [
                'class' => 'custom-select field-country'
            ],
            'label_attr' => array(
                'class' => 'form__label',
            )
        ]);
        $builder ->add('recipientLastName', TextType::class, [
            'label' => "Nom",
            'attr' => [
                'class' => 'field'
            ]
        ]);
        $builder ->add('recipientFirstName', TextType::class, [
            'label' => "Prénom",
            'attr' => [
                'class' => 'field'
            ]
        ]);
        $builder ->add('recipientAddress', TextType::class, [
            'label' => "Adresse",
            'attr' => [
                'class' => 'field'
            ],
        ]);
        $builder ->add('recipientAddressComplement', TextareaType::class, [
            'label' => "Adresse complémentaire (bâtiment, étage, etc.)",
            'attr' => [
                'class' => 'field'
            ],
            'required'  => false,
            'form_textarea' => true
        ]);
        $builder ->add('recipientPhone', TextType::class, [
            'label' => "Téléphone",
            'required' => false,
            'attr' => [
                'class' => 'field'
            ]
        ]);
        $builder ->add('recipientMobilePhone', TextType::class, [
            'label' => "Mobile",
            'attr' => [
                'class' => 'field'
            ]
        ]);
        $builder ->add('recipientZip', TextType::class, [
            'label' => "Code postal",
            'attr' => [
                'class' => 'field'
            ]
        ]);

        $builder ->add('recipientCity', TextType::class, [
            'label' => "Ville",
            'attr' => [
                'class' => 'field'
            ]
        ]);
        $builder ->add('recipientEmail', EmailType::class, [
            'label' => "E-mail",
            'required' => false,
            'attr' => [
                'class' => 'field'
            ]
        ]);
        $builder ->add('recipientInformation', TextType::class, [
            'label' => "Informations complémentaires",
            'required' => false,
            'attr' => [
                'class' => 'field'
            ],
            'form_textarea' => true
        ]);


        // Services
        $builder ->add('beforeQuote', TextType::class, [
            'label' => "Numéro de devis format AAMM_XXX ",
            'attr' => [
                'class' => 'field jsNumDevis',
                'autocomplete' => 'off',
                'maxlength' => 8
            ],
            'required' => false,
        ]);

        $builder->add('needQuote', CheckboxType::class, [
            'label' => 'app.project.form.need_quote',
            'required' => false,
            'form_focus_label' => false,
            'form_checkbox' => true
        ]);

        $builder->add('beforeServices', EntityType::class, [
            'class' => Service::class,
            'query_builder' => function (EntityRepository $erb) {
                return $erb->createQueryBuilder('u')
                    ->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale')
                    ->setParameter('locale', 'fr')
                    ->andWhere('t.choiceMomentService LIKE :before')
                    ->setParameter('before', ServiceTranslation::CHOICE_SERVICE_BEFORE)
                    ->orderBy('t.name', 'ASC');
            },
            "placeholder" => "",
            'choice_label' => 'nameCave',
            'choice_attr' => function($choice, $key, $value) {
                return ['data-description' => $choice->getDescription()];
            },
            'attr' => [
                'class' => 'field'
            ],
            'label' => '',
            'required' => false,
            'expanded' => true,
            'multiple' => true
        ]);

        $builder ->add('beforeOther', TextType::class, [
            'label' => "Si autre veuillez précisez",
            'attr' => [
                'class' => 'field'
            ],
            'required' => false,
        ]);

        $builder->add('afterServices', EntityType::class, [
            'class' => Service::class,
            'query_builder' => function (EntityRepository $erb) {
                return $erb->createQueryBuilder('u')
                    ->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale')
                    ->setParameter('locale', 'fr')
                    ->andWhere('t.choiceMomentService LIKE :after')
                    ->setParameter('after', ServiceTranslation::CHOICE_SERVICE_AFTER)
                    ->orderBy('t.name', 'ASC');
            },
            "placeholder" => "",
            'choice_label' => 'nameCave',
            'attr' => [
                'class' => 'field'
            ],
            'label' => '',
            'required' => false,
            'expanded' => true,
            'multiple' => true
        ]);

        $builder ->add('afterOther', TextType::class, [
            'label' => "Si autre veuillez précisez",
            'attr' => [
                'class' => 'field'
            ],
            'required' => false,
        ]);

        $builder->add('adressType', ChoiceType::class, [
        'label' => "Adresse de destination",
        'choices'  => [
          Project::AD_BAC => Project::AD_BAC_CODE,
          Project::AD_ARMEE => Project::AD_ARMEE_CODE,
        ],
        'attr' => [
          'class' => 'box-type',
        ],
        'mapped' => false,
        'required' => true,
      ]);

        $builder ->add('type', ChoiceType::class, [
            'label' => "",
            'choices' => [
                'basic' => Project::TYPE_BASIC,
                'urgente' => Project::TYPE_URGENCY,
                'navette' => Project::TYPE_SHUTTLE],
            'placeholder' => false,
            'attr' => [
                'class' => 'field'
            ]
        ]);
    }

    /**
     * Configure Options
     *
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
            'data_class' => Project::class,
            'validation_groups' => function (FormInterface $form) {
                $data = $form->getData();
                $group = ['Create', 'Edit'];
                if ($data->getRecipientCountry() == 'FR') {
                    array_push($group, 'cp');
                }
                if ($data->getNeedReturn()) {
                    array_push($group, "HasNeedReturn");
                }
                if ($data->getCave() == '115') {
                    //array_push($group, "Create");
                }
                return $group;
            }
        ));
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'project';
    }
}
