<?php

namespace App\Twig;

use App\Entity\StockLine;
use Doctrine\ORM\EntityManagerInterface;
use App\Utils\Managers\StockLineManager;

class IsVariationValidatableExtension extends \Twig_Extension
{
    private $stockLineManager;

    /**
     * @param StockLineManager $stockLineManager
     */
    public function __construct(StockLineManager $stockLineManager)
    {
        $this->stockLineManager = $stockLineManager;
    }

    /**
     * Expose filters
     *
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('is_variation_validatable', array($this, 'isVariationValidatable')),
        );
    }

    /**
     * Check if stock variation can be validated
     *
     * @param $obj
     * @return mixed
     */
    public function isVariationValidatable($idStockLine)
    {
        return $this->stockLineManager->isVariationValidatable((int) $idStockLine);
    }
}
