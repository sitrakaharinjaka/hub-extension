<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Front;

use App\Controller\BaseController;
use App\Entity\Project;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 *
 * @package App\Controller
 */
class HomeController extends BaseController
{

    /**
     * Homepage
     * @Route("/", name="homepage")
     * @return Response
     */
    public function index(Request $request, StockManager $stockManager)
    {
        if ($this->isGranted('ROLE_RUNNER') && !$this->isGranted('ROLE_SUPER_ADMIN') && !$this->isGranted('ROLE_GENERAL_ADMIN')) {
            return $this->redirect($this->generateUrl('runner_homepage'));
        }

        // Get Stocks
        $stocks = $stockManager->findByUser($this->getUser());

        return $this->render('hub/home/index.html.twig', [
            'stocks' => $stocks
        ]);
    }

    /**
     * Homepage
     * @Route("/choix/{id}", name="homepage_choice")
     * @return Response
     */
    public function choice(Request $request, StockManager $stockManager, $id)
    {

        // Get Stocks
        $stocks = $stockManager->findByUser($this->getUser());

        $find = false;
        $stockFind = null;
        foreach ($stocks as $stock) {
            if ($id == $stock->getId()) {
                $find = true;
                $stockFind = $stock;
            }
        }

        if ($find) {
            $this->get('session')->set('current_stock', $stockFind);
        } else {
            throw $this->createAccessDeniedException("This stock is not accessbile for you");
        }

        // View
        return $this->redirect($this->generateUrl('project_index'));
    }
}
