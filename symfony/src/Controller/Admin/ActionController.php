<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Form\Model\SearchLogFile;
use App\Form\Type\LogFile\SearchLogFileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ActionController
 * @Route("/admin/actions")
 * @package App\Controller
 */
class ActionController extends BaseController
{
    /**
     * Info about
     * @Route("/check-projects", name="admin_actions_check")
     * @param Request $request Request
     */
    public function index(Request $request)
    {
        $pathRootDir = $this->getParameter('kernel.root_dir');

        // Lock
        $process = new Process(
            'php bin/console disko:check:projects',
            $pathRootDir . '/../'
        );
        $process->run();


        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "La commande <b>gestion des réservations</b> a bien été réalisée !"
        );

        return $this->redirect($request->headers->get('referer'));
    }
}
