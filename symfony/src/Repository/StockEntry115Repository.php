<?php

namespace App\Repository;

use App\Entity\Stock;
use App\Entity\StockEntry;
use App\Entity\StockEntry115;
use App\Repository\BaseRepository;

/**
 * Class StockEntry115Repository
 */
class StockEntry115Repository extends BaseRepository
{



  /**
   * ================= FRONT ===================
   */


    /**
     * ================= BACK ===================
     */


    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
      ->select('e')
      ->orderBy('e.created', 'desc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'e';
                if ($key == 'id') {
                    $alias = 'e';
                }

                if ($key == 'stock') {
                    $qb->andWhere($alias . '.' . $key . ' = :' . $key);
                    $qb->setParameter($key, $filter);
                } elseif (is_array($filter)) {
                    $qb->andWhere($qb->expr()->in($alias . '.' . $key, $filter));
                } else {
                    $qb->andWhere($alias . '.' . $key . ' LIKE :' . $key);
                    $qb->setParameter($key, '%' . $filter . '%');
                }
            }
        }

        $query = $qb->getQuery();

        return $query;
    }


    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
      ->select('e', 'i')
//            ->select('e', 'et', 'bc', 'bct', 'u')
//            ->leftJoin('e.translations', 'et')
//            ->leftJoin('e.billingCenters', 'bc')
//            ->leftJoin('bc.translations', 'bct')
//            ->leftJoin('e.referentUser', 'u')
      ->leftJoin('e.stockEntryItems', 'i')
      ->where('e.id = :id')
      ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }

    /**
     * Find one for update & mail
     *
     * @param $id
     * @return mixed
     */
    public function findOneToUpdate($id)
    {
        $qb = $this->createQueryBuilder('e')
      ->select('e', 'i', 'u', 'p', 'pt')
      ->leftJoin('e.user', 'u')
      ->leftJoin('e.stockEntryItems', 'i')
      ->leftJoin('i.product', 'p')
      ->leftJoin('p.translations', 'pt')
      ->where('e.id = :id')
      ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }

    /**
     * Find one for confirmation screen
     *
     * @param $id
     * @return mixed
     */
    public function findOneToConfirm($id)
    {
        $qb = $this->createQueryBuilder('e')
      ->select('e', 'i', 'u', 'p', 'pt', 's', 'st')
      ->leftJoin('e.user', 'u')
      ->leftJoin('e.stock', 's')
      ->leftJoin('s.translations', 'st')
      ->leftJoin('e.stockEntry115Items', 'i')
      ->leftJoin('i.product', 'p')
      ->leftJoin('p.translations', 'pt')
      ->where('e.id = :id')
      ->setParameter('id', $id);
        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
        return $query->getOneOrNullResult();
    }

    /**
     * Find all for synch
     *
     * @return mixed
     */
    public function findAllToSynch()
    {
        $qb = $this->createQueryBuilder('e')
      ->where('e.dateSendToVeolog IS NULL')
    ;
        return $qb->getQuery()->getResult();
    }

    /**
     * Find one for confirmation screen
     *
     * @param $id
     * @return mixed
     */
    public function findNonArchivedStock($stock)
    {
        // Set archived date limit for stock. Currently archived if more than 12 months based on updated
        $date = new \DateTime();
        $date->modify('-12 months');


        $status = [
            StockEntry::STATE_ITEM_UPDATE,
            StockEntry115::STATE_ITEM_CREATE,
            StockEntry115::STATE_REQUEST_TAKEN,
            StockEntry115::STATE_ITEM_VALIDATE,
            StockEntry115::STATE_ITEM_REJECT
        ];

        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->andWhere('e.stock = :stock')
            ->setParameter('stock', $stock)
            ->andWhere('e.updated >= :last')
            ->setParameter('last', $date);

        $qb->andWhere($qb->expr()->in('e.status', $status));
        $qb->orderBy('e.pickUpDate', 'DESC');

        return $qb->getQuery()->getResult();
    }
}
