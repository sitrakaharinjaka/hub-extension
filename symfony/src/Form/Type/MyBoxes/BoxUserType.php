<?php
/**
 * Created by PhpStorm.
 * User: Luk
 * Date: 20/05/2017
 * Time: 20:35
 */

namespace App\Form\Type\MyBoxes;

use App\Entity\User;
use App\Entity\MyBoxesBoxUser;
use App\Form\Transformer\UserTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BoxUserType extends AbstractType
{
    protected $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            $builder->create('user', FormType::class, [
                'required' => false,
                'data_class' => User::class,
                'by_reference' => false,
                'label' => "User",
                'attr' => [
                    'class' => '',
                ],
            ])
            ->add('email', TextType::class, ['required' => false])
            ->addModelTransformer(new UserTransformer($this->em))
        );
    }

    /**
     * Configure Options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => MyBoxesBoxUser::class,
            'validation_groups' => 'cart'
        ));
    }
}
