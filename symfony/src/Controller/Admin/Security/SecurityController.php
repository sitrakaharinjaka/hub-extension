<?php

namespace App\Controller\Admin\Security;

use App\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class SecurityController
 * @package App\Controller\Core
 */
class SecurityController extends BaseController
{
    /**
     * @Route("/login", name="admin_login")
     * @param Request $request
     * @param AuthenticationUtils $authUtils
     * @return Response
     */
    public function login(Request $request, AuthenticationUtils $authUtils)
    {
        if (null !== $this->getUser()) {
            return $this->redirectToRoute('homepage');
        }
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return  $this->render('admin/security/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
            'target_path'  => $request->get('target_path')
        ]);
    }
}
