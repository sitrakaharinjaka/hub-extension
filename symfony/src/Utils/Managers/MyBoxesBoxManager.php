<?php

/**
 * Manager
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;
use App\Entity\MyBoxesBox;
use App\Repository\MyBoxesBoxRepository;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class MyBoxesBoxManager
 *
 * Object manager of user
 */
class MyBoxesBoxManager extends BaseManager
{
    /**
    * @var MyBoxesBoxRepository
    */
    protected $boxRepository;

    /**
     * BaseManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager = null)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('boxRepository', MyBoxesBox::class);
    }

    /**
     * Get all example
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->boxRepository->queryForSearch($filters);
    }


    /**
     * Save a myBoxesBox
     *
     * @param MyBoxesBox $myBoxesBox
     */
    public function save(MyBoxesBox $myBoxesBox)
    {
        foreach ($myBoxesBox->getBoxMedias() as $boxMedia) {
            $boxMedia->setUpdated(new \DateTime());
            $this->getEntityManager()->persist($boxMedia);
        }

        foreach ($myBoxesBox->getBoxUsers() as $boxUser) {
            $boxUser->setUpdated(new \DateTime());
            $this->getEntityManager()->persist($boxUser);
        }

        $myBoxesBox->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($myBoxesBox);
        $this->getEntityManager()->flush();

        return $myBoxesBox;
    }

    /**
     * Remove one myBoxesBox
     *
     * @param MyBoxesBox $myBoxesBox
     */
    public function remove(MyBoxesBox $myBoxesBox)
    {
        $this->getEntityManager()->remove($myBoxesBox);
        $this->getEntityManager()->flush();
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAllPerso($user)
    {
        return $this->boxRepository->findAllPerso($user);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAllTeam($user)
    {
        return $this->boxRepository->findAllTeam($user);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findForView($id)
    {
        return $this->boxRepository->findForView($id);
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $box = $this->boxRepository->findOneToEdit($id);

        return $box;
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->boxRepository->findOneBy($filters);
    }

    public function findBy($filters = array())
    {
        return $this->boxRepository->findBy($filters);
    }

    public function isBringable($id)
    {
        return $this->boxRepository->isBringable($id);
    }

    public function findBoxMemberSince($nbHour)
    {
        return $this->boxRepository->findBoxMemberSince($nbHour);
    }


    public function nbBoxAlone($start, $end, $stockInVeolog)
    {
        return $this->boxRepository->nbBoxAlone($start, $end, $stockInVeolog);
    }
    public function nbBoxTeam($start, $end, $stockInVeolog)
    {
        return $this->boxRepository->nbBoxTeam($start, $end, $stockInVeolog);
    }
    public function nbBoxByType($start, $end, $stockInVeolog)
    {
        return $this->boxRepository->nbBoxByType($start, $end, $stockInVeolog);
    }

    public function nbBoxInSleep($limit = 15, $barcode = "")
    {
        return $this->boxRepository->nbBoxInSleep($limit, $barcode);
    }

    public function nbBoxMovement($start, $end, $limit = 15, $barcode= '', $type = '')
    {
        return $this->boxRepository->nbBoxMovement($start, $end, $limit, $barcode, $type);
    }

    public function topByStorageDelay($start, $end, $limit = 15)
    {
        return $this->boxRepository->topByStorageDelay($start, $end, $limit);
    }
}
