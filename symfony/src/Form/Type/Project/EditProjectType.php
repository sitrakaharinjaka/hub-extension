<?php

/**
 * Form type
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Type\Project;

use App\Entity\Project;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 *  Form Type
 */
class EditProjectType extends CreateProjectType
{
    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('dateStart', DateType::class, [
            'widget' => 'single_text',
            'label' => 'Date du jour pour livraison',
            'html5' => false,
            'format' => 'dd/MM/yyyy',
            'attr' => [
                'class' => 'field datepicker jsDateStart selectric-disabled',
                'autocomplete' => 'off'
            ],
            'disabled' => true,
            'readonly' => true,
            'form_date' => true
        ]);

        $builder->add('hourStart', ChoiceType::class, [
            'label' => 'Heure de livraison demandée',
            'choices' => [
                '9h - 10h' => new DateTime('2020/01/01 09:00:00.000'),
               '10h - 11h' => new DateTime('2020/01/01 10:00:00.000'),
               '11h - 12h' => new DateTime('2020/01/01 11:00:00.000'),
               '12h - 13h' => new DateTime('2020/01/01 12:00:00.000'),
               '13h - 14h' => new DateTime('2020/01/01 13:00:00.000'),
               '14h - 15h' => new DateTime('2020/01/01 14:00:00.000'),
               '15h - 16h' => new DateTime('2020/01/01 15:00:00.000'),
               '16h - 17h' => new DateTime('2020/01/01 16:00:00.000'),
               '17h - 18h' => new DateTime('2020/01/01 17:00:00.000'),
               '18h - 19h' => new DateTime('2020/01/01 18:00:00.000'),
            ],
            'placeholder' => false,
            'attr' => [
                'class' => 'field'
            ]
        ]);

        $builder->add('needReturn', CheckboxType::class, [
            'required' => false,
            'label' => "Besoin d'un retour de marchandises ?",
            'form_focus_label' => false,
            'disabled' => true,
            'readonly' => true,
            'attr' => [
                'class' => 'selectric-disabled',
            ],
            'form_checkbox' => true
        ]);

        $builder->add('dateEnd', DateType::class, [
            'widget' => 'single_text',
            'label' => "Date du retour en stock",
            'html5' => false,
            'format' => 'dd/MM/yyyy',
            'disabled' => true,
            'readonly' => true,
            'attr' => [
                'class' => 'field datepicker jsDateEnd selectric-disabled',
                'autocomplete' => 'off'
            ],
            'form_date' => true
        ]);

        $builder ->add('recipientCountry', ChoiceType::class, [
            'label' => "Pays",
            'choices'  => [
                'France' => 'FR'
            ],
            'data' => 'FR',
            'disabled' => true,
            'attr' => [
                'class' => 'custom-select field-country selectric-disabled'
            ],
            'label_attr' => array(
                'class' => 'form__label',
            )
        ]);
        $builder ->add('recipientAddress', TextType::class, [
            'label' => "Adresse",
            'disabled' => true,
            'attr' => [
                'class' => 'field selectric-disabled'
            ]
        ]);
        $builder ->add('recipientAddressComplement', TextareaType::class, [
            'label' => "Adresse complémentaire (bâtiment, étage, etc.)",
            'attr' => [
                'class' => 'field selectric-disabled'
            ],
            'disabled' => true,
            'required'  => false,
            'form_textarea' => true
        ]);
        $builder ->add('recipientZip', TextType::class, [
            'label' => "Code postal",
            'disabled' => true,
            'attr' => [
                'class' => 'field selectric-disabled'
            ]
        ]);

        $builder ->add('recipientCity', TextType::class, [
            'label' => "Ville",
            'disabled' => true,
            'attr' => [
                'class' => 'field selectric-disabled'
            ]
        ]);
    }

  /**
   * Configure Options
   * @param OptionsResolver $resolver
   */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
            'data_class' => Project::class
        ));
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'project';
    }
}
