<?php

namespace App\EventListener;

use App\Entity\StockLine;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\Common\EventSubscriber as EventSubscriberInterface;
use Doctrine\ORM\Events;

class RemoveStockLineListener implements EventSubscriberInterface
{
    // this method can only return the event names; you cannot define a
    // custom method name to execute when each event triggers
    public function getSubscribedEvents(): array
    {
        return [
            Events::onFlush,
        ];
    }

    public function onFlush(OnFlushEventArgs $onFlushEventArgs)
    {
        $entityManager = $onFlushEventArgs->getEntityManager();
        $unitOfWork = $entityManager->getUnitOfWork();

        foreach ($unitOfWork->getScheduledEntityDeletions() as $entity) {
            if ($entity instanceof StockLine && is_null($entity->getProject())) {
                //throw new \Exception("Cannot delete StockLine");
                $entityManager->persist($entity);
            }
        }
    }
}
