<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConnectionRepository")
 * @ORM\Table(name="dk_connection")
 * @ORM\HasLifecycleCallbacks
 */
class Connection
{
    use GedmoTrait;
    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User|null
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="connections", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var \DateTimeInterface|null $date
     * @ORM\Column(name="date", type="date")
     */
    public $date;
    
    /**
     * @ORM\Column(name="doy", type="string")
     */
    public $doy;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTimeInterface|null $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getDoy()
    {
        return $this->doy;
    }

    /**
     * @param mixed $doy
     */
    public function setDoy($doy)
    {
        $this->doy = $doy;
    }
}
