<?php

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;

use App\Utils\Managers\LocaleManager;
use App\Entity\Example;
use App\Entity\ExampleTranslation;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class ExampleManager
 * `
 * Object manager of example
 *
 * @package Disko\PageBundle\Managers
 */
class ExampleManager extends BaseManager
{

    /**
     * Repository
     *
     * @var ExampleRepository
     */
    protected $exampleRepository;

    /**
     * ExampleManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('exampleRepository', Example::class);
    }

    /**
     * Save a example
     *
     * @param Example $example
     */
    public function save(Example $example)
    {
        $example->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($example);
        $this->getEntityManager()->flush();

        return $example;
    }

    /**
     * Remove one example
     *
     * @param Example $example
     */
    public function remove(Example $example)
    {
        $this->getEntityManager()->remove($example);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one Active
     *
     * @param $slugWl
     * @param $locale
     * @return mixed
     */
    public function findOneActive($slugWl, $locale)
    {
        $example = $this->exampleRepository->findOneActive($slugWl, $locale);

        return $example;
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $example = $this->exampleRepository->findOneToEdit($id);

        return $example;
    }

    /**
     * Get all example
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->exampleRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->exampleRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->exampleRepository->findOneBy($filters);
    }
}
