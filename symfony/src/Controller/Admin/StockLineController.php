<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\StockLine;
use App\Entity\Stock;
use App\Form\Model\SearchProduct;
use App\Form\Model\SearchProductStockLine;
use App\Form\Model\SearchStockVariation;
use App\Form\Model\SearchProject;
use App\Form\Model\SearchStockLine;
use App\Form\Type\Product\SearchProductStockLineType;
use App\Form\Type\StockLine\SearchStockVariationType;
use App\Form\Type\Product\SearchProductType;
use App\Form\Type\Project\SearchProjectType;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockLineManager;
use App\Utils\Managers\LocaleManager;
use App\Utils\Managers\StockManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;

/**
 * Class StockLineController
 * @Route("/admin/stock-lines")
 * @package Disko\AdminBundle\Controller
 */
class StockLineController extends BaseController
{
    /**
     * StockLine's list
     * @Route("/{cave}/list", name="admin_stockLine_list", requirements={"cave"="115|pantin"})
     * @param Request $request Request
     */
    public function listAction(Request $request, StockLineManager $stockLineManager, StockManager $stockManager, ProductManager $productManager, $cave)
    {
        $stockLineManager->checkAndUpdateFullGroupOnlyOption();

        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchProductStockLineType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des stocks');

        $stock = $this->get('session')->get('current_admin_stock');
        $stock = $stockManager->findOneBy(['id' => $stock->getId()]);

        // Generate query
        $query = $productManager->queryForSearchWithTotal($data->getSearchData(), $stock, $cave);

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        $paginationItems = [];
        $paginationItemsTmp = $pagination->getItems();

        foreach($paginationItemsTmp as $key => $paginationItem){
            $paginationItemProduct = $paginationItem[0];
            $paginationItems[$key] = [
                0 => $paginationItemProduct,
                'total' => $paginationItem['total'],
                'totalWithHold' => $stockLineManager->quantityOf($paginationItemProduct, $cave, new DateTime('now'), $stock),
                'totalHoldVariation' => null //$paginationItem['totalHoldVariation']
            ];
        }

        $pagination->setItems($paginationItems);

        // Render view
        return $this->render('@Admin/StockLine/index.html.twig', array(
            'pagination' => $pagination,
            'cave' => $cave,
            'dataSearch' => $data,
            'form' => $form->createView()
        ));
    }

    /**
     * StockLine add
     * @Route("/{cave}/ajouter/{id}", name="admin_stockLine_add", requirements={"cave"="115|pantin"})
     * @param Request $request Request
     */
    public function add(Request $request, ProductManager $productManager, StockManager $stockManager, StockLineManager $stockLineManager, $cave, $id)
    {
        $product = $productManager->findOneBy(['id' => $id]);

        if (!$product) {
            throw $this->createNotFoundException("Product not found");
        }


        $stock = $this->get('session')->get('current_admin_stock');
        $stock = $stockManager->findOneBy(['id' => $stock->getId()]);

        $requestedStockId = $request->get('stockId', null);
        $requestedStock = $stockManager->findOneBy(['id' => $requestedStockId]);
        
        if($requestedStock instanceof Stock){
            $stock = $requestedStock;
        }

        if ($request->getMethod() == "POST") {
            $operation = $request->get('operation', null);
            if (is_numeric($operation) && $operation != 0) {
                $stockLineManager->addLine($stock, $product, "Action via l'administration", $operation, new DateTime('now'), $cave, null, 'admin');

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                   'success',
                   "L'opération a bien été prise en compte"
               );
            }
        }

        if($request->query->has('validateVariation')){
            $stockLineId = (int) $request->query->get('validateVariation');
            $stockLine = $stockLineManager->findOneBy(['id' => $stockLineId]);

            if (is_object($stockLine)) {
                $stockLineManager->validateStockVariation($stockLine, $this->getUser());
                //$stockLineManager->updateByStockAndProduct($stock, $product, $cave, false, [$stockLine->getId()]);

                $this->get('session')->getFlashBag()->set(
                    'success',
                    "La variation de stock a été validée"
                );

                return $this->redirectToRoute('admin_stockLine_add', ['cave' => $cave, 'id' => $id]);
            }
        }

        if($request->query->has('challengeVariation')){
            $stockLineId = (int) $request->query->get('challengeVariation');
            $stockLine = $stockLineManager->findOneBy(['id' => $stockLineId]);

            if (is_object($stockLine)) {
                $stockLineManager->challengeStockVariation($stockLine, $this->getUser());

                $this->get('session')->getFlashBag()->set(
                    'success',
                    "La variation de stock a été contestée"
                );

                return $this->redirectToRoute('admin_stockLine_add', ['cave' => $cave, 'id' => $id]);
            }
        }

        //$total = $stockLineManager->totalStockOnProduct($stock, $cave, $product->getId());
        $total = $stockLineManager->quantityOf($product, $cave, new DateTime('now'), $stock);
        $lines = $stockLineManager->findByStockAndProduct($stock, $cave, $product->getId());

        // Render view
        return $this->render('@Admin/StockLine/view.html.twig', array(
            'stock' => $stock,
            'product' => $product,
            'cave' => $cave,
            'lines' => $lines,
            'total' => $total
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchProductStockLine();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setName((isset($filters['name']))   ? $filters['name'] : '');
        $data->setSku((isset($filters['sku']))   ? $filters['sku'] : '');
        $data->setEan((isset($filters['ean']))   ? $filters['ean'] : '');
        $data->setDescription((isset($filters['description'])) ? $filters['description'] : '');
        $data->setBrand((isset($filters['brand'])) ? $filters['brand'] : '');

        $data->setDateStart((isset($filters['dateStart']))   ? date_create_from_format("d/m/Y", $filters['dateStart']) :  $this->get('session')->get('admin_dateStart', null));
        $data->setDateEnd((isset($filters['dateEnd']))   ? date_create_from_format("d/m/Y", $filters['dateEnd']) : $this->get('session')->get('admin_dateEnd', null));

        $data->setFilterQty((isset($filters['filterQty'])) ? $filters['filterQty'] : 'stock');

        if (!$data->getDateStart()) {
            $date = new DateTime();
            $date->modify("+1 days");
            $data->setDateStart($date);
            $this->get('session')->set('admin_dateStart', $data->getDateStart());
        }

        if (!$data->getDateEnd()) {
            $dateEnd = new DateTime();
            $dateEnd->modify("+3 days");
            $data->setDateEnd($dateEnd);
            $this->get('session')->set('admin_dateEnd', $data->getDateStart());
        }

        if (!$data->getDateEnd() > $data->getDateStart()) {
            $data->setDateEnd($data->getDateStart());
        }

        $this->get('session')->set('admin_dateStart', $data->getDateStart());
        $this->get('session')->set('admin_dateEnd', $data->getDateStart());

        return $data;
    }

    /**
     * Export csv
     * @Route("/{cave}/export/{locale}", name="admin_stockLine_export")
     *
     * @param  Request  $request
     */
    public function exportAction(Request $request, StockManager $stockManager, ProductManager $productManager, $cave, $locale)
    {
        $stock = $this->get('session')->get('current_admin_stock');
        $stock = $stockManager->findOneBy(['id' => $stock->getId()]);

        // Generate query
        $data    = $this->initSearch($request);
        $query   = $productManager->queryForSearchWithTotal($data->getSearchData(), $stock, $cave);
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Nom',
            'Sku',
            'Catégories',
            'Etat',
            'Stock Réél',
            'Stock',
            'Origine',
            'Consommable',
            'Description',
            'Date de création',
        ], ';');

        $locale = $request->get('locale', $request->getDefaultLocale());
        foreach ($results as $result) {
            $product = $result[0];
            $total = $result['total'];
            $totalWithHold = $result['totalWithHold'] - $result['totalHoldVariation'];

            $categories = '';
            foreach ($product->translate($locale)->getCategories() as $category) {
                if ($categories!='') {
                    $categories.= ', ';
                }
                $categories.= $category->getName();
            }

            $fileRow = [
                $product->getId(),
                $product->translate($locale)->getName(),
                $product->translate($locale)->getCode(),
                $categories,
                ($product->translate($locale)->getState() == 'out_of_use')
                    ? 'Non Neuf'
                    : 'Neuf',
                $total,
                $totalWithHold,
                ($product->translate($locale)->getOrigin() == 0)
                    ? "SAP"
                    : "NON SAP",
                ($product->translate($locale)->isExpendable())
                    ? 'Consommable'
                    : 'Non consommable',
                $product->translate($locale)->getDescription(),
                ($product->translate($locale)->getCreated())
                    ? $product->translate($locale)->getCreated()->format('d/m/Y')
                    : $product->getCreated()->format('d/m/Y'),
            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type'        => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'
                                    . date('Y-m-d')
                                    . '_'
                                    . $locale . '_stock-lines_export.csv"'
        ));
    }

    /**
     * Stock Variation list
     * @Route("/{cave}/variations", name="admin_variation_list", requirements={"cave"="115|pantin"})
     * @param Request $request Request
     */
    public function stockVariations(Request $request, StockManager $stockManager, StockLineManager $stockLineManager, $cave)
    {
        $stock = $this->get('session')->get('current_admin_stock');
        $stock = $stockManager->findOneBy(['id' => $stock->getId()]);

        $data = [];

        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchStockVariation();
        $data->setProductId((isset($filters['productId'])) ? $filters['productId'] : '');
        $data->setProductName((isset($filters['productName']))   ? $filters['productName'] : '');
        $data->setProductSku((isset($filters['productSku']))   ? $filters['productSku'] : '');
        $data->setStatus((isset($filters['status']))   ? $filters['status'] : 'all');

        // Init form
        $form = $this->createForm(SearchStockVariationType::class, $data);

        if($request->query->has('validateVariation')){
            $stockLineId = (int) $request->query->get('validateVariation');
            $stockLine = $stockLineManager->findOneBy(['id' => $stockLineId]);

            if (is_object($stockLine)) {
                $stockLineManager->validateStockVariation($stockLine, $this->getUser());
                //$stockLineManager->updateByStockAndProduct($stock, $stockLine->getProduct(), $cave, false, [$stockLine->getId()]);

                $this->get('session')->getFlashBag()->set(
                    'success',
                    "La variation de stock a été validée"
                );

                return $this->redirectToRoute('admin_variation_list', ['cave' => $cave, 'page' => $request->query->get('page', 1), '_fragment' => 'stockLine-' . $stockLineId]);
            }
        }

        if($request->query->has('challengeVariation')){
            $stockLineId = (int) $request->query->get('challengeVariation');
            $stockLine = $stockLineManager->findOneBy(['id' => $stockLineId]);

            if (is_object($stockLine)) {
                $stockLineManager->challengeStockVariation($stockLine, $this->getUser());

                $this->get('session')->getFlashBag()->set(
                    'success',
                    "La variation de stock a été contestée"
                );

                return $this->redirectToRoute('admin_variation_list', ['cave' => $cave, 'page' => $request->query->get('page', 1)]);
            }
        }

        $intervalDate = new DateTime();
        $intervalDate->modify('-12 months');

        $query = $stockLineManager->queryForSearch(
            ['e.stockVariation' => true] + $data->getSearchData(),
            $intervalDate
        );

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/StockLine/variations.html.twig', array(
            'cave' => $cave,
            'lines' => $pagination,
            'dataSearch' => $data,
            'form' => $form->createView(),
            'currentPage' => $request->query->get('page', 1)
        ));
    }
}
