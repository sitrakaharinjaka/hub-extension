<?php

namespace App\Entity;

use App\Entity\Traits\AuthorableTrait;
use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\SeoTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;


// Use For Algolia Or Api
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExampleRepository")
 * @ORM\Table(name="dk_example_translation")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ExampleTranslation implements NormalizableInterface
{
    use ORMBehaviors\Translatable\Translation;
    use GedmoTrait;
    use SeoTrait;
    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Name
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $name;

    /**
     * Slug
     *
     * @Gedmo\Slug(fields={"name"}, unique=false)
     * @ORM\Column(length=255, unique=false)
     */
    protected $slug;

    /**
     * Mantra
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $mantra;

    /**
     * Category
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $category;

    /**
     * Multiples
     *
     * @ORM\Column(type="array", nullable=true)
     */
    protected $multiples;

    /**
     * Description
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * DescriptionUi
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $descriptionUi;


    /**
     * Edit
     *
     * @var datetime $publishDate
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $publishDate;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active = false;

    /**
     * Cover
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="cover_id", referencedColumnName="id", onDelete="SET NULL")
     * @var Media
     */
    protected $cover;

    /**
     * Crop
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="crop_id", referencedColumnName="id", onDelete="SET NULL")
     * @var Media
     */
    protected $crop;

    /**
     * File
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id", onDelete="SET NULL")
     * @var Media
     */
    protected $pdf;

    /**
     * Set id
     *
     * @param integer $id
     * @return Example
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Example
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set mantra
     *
     * @param string $mantra
     * @return Example
     */
    public function setMantra($mantra)
    {
        $this->mantra = $mantra;

        return $this;
    }

    /**
     * Get mantra
     *
     * @return string
     */
    public function getMantra()
    {
        return $this->mantra;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Example
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Example
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Example
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return mixed
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * @param mixed $cover
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    }

    /**
     * @return datetime
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * @param datetime $publishDate
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;
    }

    /**
     * @return mixed
     */
    public function getDescriptionUi()
    {
        return $this->descriptionUi;
    }

    /**
     * @param mixed $descriptionUi
     */
    public function setDescriptionUi($descriptionUi)
    {
        $this->descriptionUi = $descriptionUi;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getMultiples()
    {
        return $this->multiples;
    }

    /**
     * @param mixed $multiples
     */
    public function setMultiples($multiples)
    {
        $this->multiples = $multiples;
    }

    /**
     * @return Media
     */
    public function getCrop()
    {
        return $this->crop;
    }

    /**
     * @param Media $crop
     */
    public function setCrop($crop)
    {
        $this->crop = $crop;
    }

    /**
     * @return Media
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * @param Media $pdf
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    }


    /**
     * Use to serialize object for Algolia or Api
     *
     * @param NormalizerInterface $normalizer
     * @param null $format
     * @param array $context
     * @return array|bool|float|int|string
     */
    public function normalize(NormalizerInterface $normalizer, $format = null, array $context = array())
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'locale' => $this->getLocale(),
            'description' => $this->getDescription(),
            'descriptionUi' => $this->getDescriptionUi(),
            'slug' => $this->getSlug(),
            'active' => $this->getActive(),
            'translatable_id' => $this->getTranslatable()->getId(),
        ];
    }
}
