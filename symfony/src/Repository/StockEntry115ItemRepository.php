<?php

namespace App\Repository;

use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Class StockEntry115ItemRepository
 * @package App\Repository
 */
class StockEntry115ItemRepository extends BaseRepository
{



  /**
   * ================= FRONT ===================
   */


    /**
     * ================= BACK ===================
     */


    /**
     * Get all stock entry 115 items query, using for pagination
     */
    public function queryForSearch(string $locale = 'fr', array $filters = []): Query
    {
        $qb = $this->createQueryBuilder('e')
            ->select(
                'e.id', 'e.status',
                'se.id as stockEntryId', 'se.created', 'se.username', 'se.pickUpDate',
                'st.name',
                'pt.code', 'pt.name as productName', 'pt.eanPart', 'pt.eanBox',
                'u.username', 'u.firstName', 'u.lastName'
            )
            ->innerJoin('e.stockEntry115', 'se')
            ->innerJoin('se.stock', 's')
            ->innerJoin('e.product', 'p')
            ->leftJoin('se.user', 'u')
            ->leftJoin('s.translations', 'st', Join::WITH, 'st.locale = :locale')
            ->leftJoin('p.translations', 'pt', Join::WITH, 'pt.locale = :locale')
            ->setParameter('locale', $locale)
            ->orderBy('se.created', 'DESC')
            ->addOrderBy('se.id', 'ASC')
            ;

        if (count($filters)) {
            foreach ($filters as $key => $filter) {
                switch ($key) {
                    case 'id':
                        $qb->andWhere('se.id = :id')
                            ->setParameter('id', $filter);
                        break;
                    case 'stock':
                        $qb->andWhere('LOWER(st.name) LIKE :name')
                            ->setParameter('name', '%' . $filter . '%');
                        break;
                    case 'requester':
                        $qb->andWhere('LOWER(u.username) LIKE :requester')
                            ->orWhere('LOWER(u.firstName) LIKE :requester')
                            ->orWhere('LOWER(u.lastName) LIKE :requester')
                            ->setParameter('requester', '%' . $filter . '%');
                        break;
                    case 'code':
                        $qb->andWhere('LOWER(pt.code) LIKE :code')
                            ->setParameter('code', '%' . $filter . '%');
                        break;
                    case 'status':
                        $qb->andWhere('LOWER(e.status) LIKE :status')
                            ->setParameter('status', '%' . $filter . '%');
                        break;
                    case 'createdAt':
                        $start = new \DateTime($filters['createdAt']->format('Y-m-d H:i:s'));
                        $end = new \DateTime($filters['createdAt']->format('Y-m-d H:i:s'));
                        $start->setTime(0,0);
                        $end->setTime(23,59);
                        $qb->andWhere('se.created >= :start')
                        ->andWhere('se.created <= :end')
                        ->setParameter('start',$start)
                        ->setParameter('end',$end);
                        break;
                    case 'delivery':
                            $qb->andWhere('se.pickUpDate = :delivery')
                            ->setParameter('delivery', $filters['delivery']);
                        break;
                }
            }
        }

        return $qb->getQuery();
    }


    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
      ->select('e', 'i')
//            ->select('e', 'et', 'bc', 'bct', 'u')
//            ->leftJoin('e.translations', 'et')
//            ->leftJoin('e.billingCenters', 'bc')
//            ->leftJoin('bc.translations', 'bct')
//            ->leftJoin('e.referentUser', 'u')
      ->leftJoin('e.stockEntryItems', 'i')
      ->where('e.id = :id')
      ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }

    /**
     * Find one for update & mail
     *
     * @param $id
     * @return mixed
     */
    public function findOneToUpdate($id)
    {
        $qb = $this->createQueryBuilder('e')
      ->select('e', 'i', 'u', 'p', 'pt')
      ->leftJoin('e.user', 'u')
      ->leftJoin('e.stockEntryItems', 'i')
      ->leftJoin('i.product', 'p')
      ->leftJoin('p.translations', 'pt')
      ->where('e.id = :id')
      ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }

    /**
     * Find one for confirmation screen
     *
     * @param $id
     * @return mixed
     */
    public function findOneToConfirm($id)
    {
        $qb = $this->createQueryBuilder('e')
      ->select('e', 'i', 'u', 'p', 'pt', 's', 'st')
      ->leftJoin('e.user', 'u')
      ->leftJoin('e.stock', 's')
      ->leftJoin('s.translations', 'st')
      ->leftJoin('e.stockEntryItems', 'i')
      ->leftJoin('i.product', 'p')
      ->leftJoin('p.translations', 'pt')
      ->where('e.id = :id')
      ->setParameter('id', $id);
        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
        return $query->getOneOrNullResult();
    }

    /**
     * Find all for synch
     *
     * @return mixed
     */
    public function findAllToSynch()
    {
        $qb = $this->createQueryBuilder('e')
      ->where('e.dateSendToVeolog IS NULL')
    ;
        return $qb->getQuery()->getResult();
    }
}
