<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\Fail2Ban;
use App\Form\Model\SearchFail2Ban;
use App\Form\Type\User\SearchFail2BanType;
use App\Utils\Managers\Fail2BanManager;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Process\Process;

/**
 * Class MaintenanceController
 * @Route("/admin/maintenance")
 * @package App\Controller
 */
class MaintenanceController extends BaseController
{
    /**
     * Maintenance activation page
     *
     * @Route("/list", name="admin_maintenance")
     * @param Request $request Request
     */
    public function index(Request $request)
    {
        // Render view
        return $this->render('@Admin/Maintenance/index.html.twig', array(

        ));
    }


    /**
     * Maintenance activation page
     *
     * @Route("/toogle/{state}", name="admin_maintenance_toggle")
     * @param Request $request Request
     */
    public function toggle(Request $request, $state)
    {
        $pathRootDir = $this->getParameter('kernel.root_dir');
        if ($state) {
            // Lock
            $process = new Process(
                'php bin/console lexik:maintenance:lock 86400 --no-interaction',
                $pathRootDir . '/../'
            );
            $process->run();


            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'notice',
                "La mise en maintenance vient d'être activée"
            );
        } else {

            // Unlock
            $process = new Process(
                'php bin/console lexik:maintenance:unlock --no-interaction',
                $pathRootDir . '/../'
            );
            $process->run();

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'notice',
                "La mise en maintenance vient d'être désactivée"
            );
        }

        return $this->redirect($request->headers->get('referer'));
    }
}
