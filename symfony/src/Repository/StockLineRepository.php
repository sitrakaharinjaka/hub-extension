<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\Project;
use App\Entity\Stock;
use App\Entity\StockLine;
use App\Repository\BaseRepository;
use DateTime;
use DateTimeZone;

/**
 * Class StockLineRepository
 */
class StockLineRepository extends BaseRepository
{





    /**
     * ================= BACK ===================
     */


    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }

    /**
     * @param $stock
     * @param $idProduct
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByStockAndProduct($stock, $cave, $idProduct)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->leftJoin('e.product', 'p')
            ->leftJoin('e.stock', 's')
            ->andWhere('p.id = :idProduct')
            ->setParameter('idProduct', $idProduct)
            ->andWhere('s.id = :idStock')
            ->setParameter('idStock', $stock->getId())
            ->andWhere('e.cave = :cave')
            //->andWhere('e.number != 0')
            ->setParameter('cave', $cave)
            //->addOrderBy('e.archived', 'asc')
            ->addOrderBy('e.date', 'desc')
            ->setMaxResults(40);


        $query = $qb->getQuery();

        return $query->getResult();
    }

    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array(), $interval = null)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->leftJoin('e.product', 'p')
            ->leftJoin('p.translations', 'pt')
            ->orderBy('e.id', 'desc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $placeholder = str_replace('.', '', $key);

                if($key == 'pt.name'){
                    $qb->andWhere($key . ' LIKE :' . $placeholder);
                    $qb->setParameter($placeholder, '%' . $filter . '%');
                }else{
                    $qb->andWhere($key . ' = :' . $placeholder);
                    $qb->setParameter($placeholder, $filter);
                }
            }
        }
        if (!is_null($interval) && $interval instanceof DateTime) {
            $qb->andWhere('e.date >= :interval');
            $qb->setParameter('interval', $interval);
        }

        $qb->andWhere('e.number != 0');

        $query = $qb->getQuery();

        return $query;
    }

    /**
     * @param Stock $stock
     * @param Product $product
     * @param string $cave
     * @param bool $onlyVariations
     * @param array $exceptions
     *
     * @return mixed
     */
    public function updateByStockAndProduct(Stock $stock, Product $product, $cave = 'pantin', $onlyVariations = false, $exceptions = [])
    {

        $dateNow = new DateTime('now');
        $timezone = new DateTimeZone('Europe/Paris');
        $dateNow->setTimezone($timezone);
                            
        $qb = $this->createQueryBuilder('e')
        ->update()
        ->set('e.archived', '1');

        if($onlyVariations){
            $qb->set('e.stockVariationValid', '1')
                ->andWhere('e.stockVariation = true');
        }else{
            $qb->andWhere('e.archived = false');
        }
        
        $qb->set('e.label', "CONCAT(e.label, ' (mise à jour veolog)')")
        ->andWhere('e.product = :product')
        ->setParameter('product', $product)
        ->andWhere('e.stock = :stock')
        ->setParameter('stock', $stock)
        ->andWhere('e.cave = :cave')
        ->setParameter('cave', $cave)
        ->andWhere('e.date < :now')
        ->setParameter('now', $dateNow);

        if(is_array($exceptions) && count($exceptions) > 0){
            $qb->andWhere('e.id NOT IN(:exceptions)')
            ->setParameter('exceptions', $exceptions);
        }

        $query = $qb->getQuery()->execute();
    }

    /**
     * @param $stock
     * @param $idProduct
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function totalStockOnProduct($stock, $cave, $idProduct, $hold = false)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('SUM(e.number) as total')
            ->leftJoin('e.product', 'p')
            ->leftJoin('e.stock', 's')
            ->andWhere('e.archived = false')
            ->andWhere('p.id = :idProduct')
            ->setParameter('idProduct', $idProduct)
            ->andWhere('s.id = :idStock')
            ->setParameter('idStock', $stock->getId())

            ->andWhere('e.cave = :cave')
            ->setParameter('cave', $cave);

        $qb->andWhere('e.holdForProject = :hold')
            ->setParameter('hold', $hold);

        $query = $qb->getQuery();

        $result = $query->getSingleResult();
        return $result['total'];
    }




    /**
     * @param $stock
     * @param $idProduct
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function totalStockDateOnProduct($stock, $cave, $idProduct, $dateStart, $dateEnd, $hold = false)
    {
        // Start
        $qb = $this->createQueryBuilder('e')
            ->select('SUM(e.number) as total, p.id')
            ->leftJoin('e.product', 'p')
            ->leftJoin('p.translations', 'pt')
            ->leftJoin('e.stock', 's');
        $qb->add('where', $qb->expr()->in('p.id', [$idProduct]))
            ->andWhere('e.archived = false')
            ->andWhere('s.id = :idStock')
            ->setParameter('idStock', $stock->getId())
            ->andWhere('e.cave = :cave')
            ->setParameter('cave', $cave);
        if (!$hold) {
            $qb->andWhere('e.holdForProject = :hold')
                ->setParameter('hold', $hold);
        }
        //$qb->andWhere("IFELSE( pt.isExpendable = TRUE OR e.label = 'Action via veolog', e.date,  DATEADD(e.date, 2, 'DAY')) <= :dateStart")
        if ($cave == Project::CAVE_115) {
            $qb->andWhere("DATE(e.date) <= DATE(:dateStart)")
            ->setParameter('dateStart', $dateStart);
        }
        $qb->groupBy("p.id");
        $query = $qb->getQuery();
        $results = $query->getArrayResult();

        if ($hold) {
            // Negative variation
            $qb = $this->createQueryBuilder('e')
                ->select('SUM(e.number) as total, p.id')
                ->leftJoin('e.product', 'p')
                ->leftJoin('p.translations', 'pt')
                ->leftJoin('e.stock', 's');
            $qb->add('where', $qb->expr()->in('p.id', [$idProduct]))
                ->andWhere('e.archived = false')
                ->andWhere('s.id = :idStock')
                ->setParameter('idStock', $stock->getId())
                ->andWhere('e.cave = :cave')
                ->setParameter('cave', $cave);
            $qb->andWhere('e.holdForProject = :hold')
                ->setParameter('hold', $hold);
            //$qb->andWhere("IFELSE(pt.isExpendable = TRUE OR e.label = 'Action via veolog', e.date,  DATESUB(e.date, 2, 'DAY')) <= :dateEnd")
            if ($cave == Project::CAVE_115) {
                $qb->andWhere("e.date <= :dateEnd")
                ->setParameter('dateEnd', $dateEnd);
            }
            //$qb->andWhere("IFELSE(pt.isExpendable = TRUE OR e.label = 'Action via veolog', e.date,  DATEADD(e.date, 2, 'DAY')) > :dateStart")
            if ($cave == Project::CAVE_115) {
                $qb->andWhere("e.date > :dateStart")
                ->setParameter('dateStart', $dateStart);
            }
            $qb->andWhere('e.number < 0');

            $qb->groupBy("p.id");
            $query = $qb->getQuery();
            $variationResults = $query->getArrayResult();

            foreach ($results as $key => $result) {
                foreach ($variationResults as $keyv => $variationResult) {
                    if ($results[$key]['id'] == $variationResults[$keyv]['id']) {
                        $results[$key]['total'] =  $results[$key]['total'] + $variationResults[$keyv]['total'];
                    }
                }
            }
        }

        $total = 0;
        if (isset($results[0]) && isset($results[0]['total'])) {
            $total = $results[0]['total'];
        }

        return $total;
    }

    /**
     * @param $stock
     * @param $idProduct
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function totalStockOnProducts($stock, $cave, $ids, $dateStart, $dateEnd, $hold = false)
    {
        if (empty($ids)) {
            $ids = [0];
        }

        // Start
        $qb = $this->createQueryBuilder('e')
            ->select('SUM(e.number) as total, p.id')
            ->leftJoin('e.product', 'p')
            ->leftJoin('p.translations', 'pt')
            ->leftJoin('e.stock', 's');
        $qb->add('where', $qb->expr()->in('p.id', $ids))
            ->andWhere('e.archived = false')
            ->andWhere('s.id = :idStock')
            ->setParameter('idStock', $stock->getId())
            ->andWhere('e.cave = :cave')
            ->setParameter('cave', $cave);
        if (!$hold) {
            $qb->andWhere('e.holdForProject = :hold')
                ->setParameter('hold', $hold);
        }
        //$qb->andWhere("IFELSE(pt.isExpendable = TRUE OR e.label = 'Action via veolog', e.date,  DATEADD(e.date, 2, 'DAY')) <= :dateStart")
        if ($cave == Project::CAVE_115) {
            $qb->andWhere("DATE(e.date) <= DATE(:dateStart)")
            ->setParameter('dateStart', $dateStart);
        }
        $qb->groupBy("p.id");
        $query = $qb->getQuery();
        $results = $query->getArrayResult();


        if ($hold) {
            // Negative variation
            $qb = $this->createQueryBuilder('e')
                ->select('SUM(e.number) as total, p.id')
                ->leftJoin('e.product', 'p')
                ->leftJoin('p.translations', 'pt')
                ->leftJoin('e.stock', 's');
            $qb->add('where', $qb->expr()->in('p.id', $ids))
                ->andWhere('e.archived = false')
                ->andWhere('s.id = :idStock')
                ->setParameter('idStock', $stock->getId())
                ->andWhere('e.cave = :cave')
                ->setParameter('cave', $cave);
            $qb->andWhere('e.holdForProject = :hold')
                ->setParameter('hold', $hold);
            //$qb->andWhere("IFELSE(pt.isExpendable = TRUE OR e.label = 'Action via veolog', e.date,  DATESUB(e.date, 2, 'DAY')) <= :dateEnd")
            if ($cave == Project::CAVE_115) {
                $qb->andWhere("DATE(e.date) <= DATE(:dateEnd)")
                ->setParameter('dateEnd', $dateEnd);
            }
            //$qb->andWhere("IFELSE(pt.isExpendable = TRUE OR e.label = 'Action via veolog', e.date,  DATEADD(e.date, 2, 'DAY')) > :dateStart")
            if ($cave == Project::CAVE_115) {
                $qb->andWhere("DATE(e.date) > DATE(:dateStart)")
                ->setParameter('dateStart', $dateStart);
            }
            $qb->andWhere('e.number < 0');

            $qb->groupBy("p.id");
            $query = $qb->getQuery();
            $variationResults = $query->getArrayResult();


            foreach ($results as $key => $result) {
                foreach ($variationResults as $keyv => $variationResult) {
                    if ($results[$key]['id'] == $variationResults[$keyv]['id']) {
                        $results[$key]['total'] =  $results[$key]['total'] + $variationResults[$keyv]['total'];
                    }
                }
            }
        }

        return $results;
    }

    /**
     * Get qty of item
     *
     * @param Product $project
     * @param string $cave
     * @param DateTime $date
     * @param Stock $stock
     * 
     * @return mixed
     */
    public function quantityOf($product, $cave, $date, $stock = null, $realStock = true)
    {
        if($cave == Project::CAVE_115){
            $qb = $this->createQueryBuilder('e')
            ->select('SUM(e.number) as total')
            ->andWhere('e.archived = false')
            ->leftJoin('e.product', 'p')
            ->andWhere('p.id = :idProduct')
            ->andWhere('e.cave = :cave')
            ->andWhere("e.date <= :date")
            ->setParameter('date', $date)
            ->setParameter('idProduct', $product->getId())
            ->setParameter('cave', $cave);

            if ($stock instanceof Stock) {
                $qb->leftJoin('e.stock', 's')
                ->andWhere('s.id = :idStock')
                ->setParameter('idStock', $stock->getId());
            }

            return $qb->getQuery()->getSingleScalarResult();

        }

        //$quantity = 0;
        $validProjectStates = [Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_INT, Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE, Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP];

        if(!$realStock){
            $validProjectStates[] = Project::STATE_HUB_ACCEPTED;
            $validProjectStates[] = Project::STATE_HUB_ACCEPTED_FIXED;
        }

        $qb = $this->createQueryBuilder('e');

        $qb->select('SUM(e.number) as total')
            ->andWhere('e.archived = false')
            ->leftJoin('e.product', 'p')
            ->leftJoin('e.project', 'pr')
            ->andWhere('pr.state IN(:validProjectStates)')
            ->setParameter('validProjectStates', $validProjectStates)
            ->andWhere('p.id = :idProduct')
            ->andWhere('e.cave = :cave')
            ->andWhere('e.project IS NOT NULL')
            ->setParameter('idProduct', $product->getId())
            ->setParameter('cave', $cave);

        if ($stock instanceof Stock) {
            $qb->leftJoin('e.stock', 's')
            ->andWhere('s.id = :idStock')
            ->setParameter('idStock', $stock->getId());
        }

        $quantity = $qb->getQuery()->getSingleScalarResult();

        $qb2 = $this->createQueryBuilder('e');

        $qb2->select('SUM(e.number) as total')
            ->andWhere('e.archived = false')
            ->leftJoin('e.product', 'p')
            ->andWhere('p.id = :idProduct')
            ->andWhere('e.cave = :cave')
            ->andWhere('e.project IS NULL')
            ->setParameter('idProduct', $product->getId())
            ->setParameter('cave', $cave);

        if ($stock instanceof Stock) {
            $qb2->leftJoin('e.stock', 's')
            ->andWhere('s.id = :idStock')
            ->setParameter('idStock', $stock->getId());
        }

        $quantity += $qb2->getQuery()->getSingleScalarResult();

        return $quantity;
    }

    /**
     * Remove from project
     *
     * @param Project $project
     */
    public function removeFromProject(Project $project)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete(StockLine::class, 's');
        $qb->andWhere('s.project = :project');
        $qb->setParameter('project', $project);

        return $qb->getQuery()->execute();
    }

    /**
     */
    public function checkAndUpdateFullGroupOnlyOption()
    {
        $connexion = $this->getEntityManager()->getConnection();
        $statment = $connexion->prepare("SELECT @@SESSION.sql_mode;");
        $statment->execute();
        $all = $statment->fetchAll();

        if (sizeof($all) > 0 && strpos($all[0]['@@SESSION.sql_mode'], "ONLY_FULL_GROUP_BY") !== false) {
            //$statment = $connexion->prepare("SET GLOBAL sql_mode=(SELECT REPLACE(@@GLOBAL.sql_mode,'ONLY_FULL_GROUP_BY',''))");
            //$statment->execute();
            $statment = $connexion->prepare("SET SESSION sql_mode=(SELECT REPLACE(@@SESSION.sql_mode,'ONLY_FULL_GROUP_BY',''))");
            $statment->execute();
        }
    }

    /**
     * Archived items in project
     *
     * @param Project $project
     */
    public function archiveItemsForProject(Project $project)
    {
        $qb = $this->createQueryBuilder('s')
            ->select('s')
            ->andWhere('s.project = :project')
            ->setParameter('project', $project);

        return $qb->getQuery()->getResult();
    }

    public function checkLinkedToStock($product, $stock, $cave = 'pantin')
    {
        $qb = $this->createQueryBuilder('e')
      ->select('COUNT(DISTINCT(e.id))')
      ->andWhere('e.product = :product')
      ->setParameter('product', $product)
      ->andWhere('e.stockVariation = false AND e.archived = false AND e.number > 0')
      ->andWhere('e.stock = :stock')
      ->setParameter('stock', $stock);

        if (!empty($cave)) {
            $qb ->andWhere('e.cave = :cave')
                ->setParameter('cave', $cave);
        }

        $results = $qb->getQuery()->getSingleScalarResult();

        return $results > 0? true: false;
    }

}
