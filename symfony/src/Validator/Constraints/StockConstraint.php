<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class StockConstraint extends Constraint
{
    public $message = 'sylius.ui.constraint.out_of_stock';

    public function validatedBy()
    {
        return 'stock_constraint';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
