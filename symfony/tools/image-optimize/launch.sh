#!/bin/bash

DIRECTORY=$(cd `dirname $0` && pwd)
find ${DIRECTORY}/../../public/uploads/ -type f -name "*.jpg" | xargs jpegoptim --strip-all
find ${DIRECTORY}/../../public/uploads/ -type f -name "*.jpeg" | xargs jpegoptim --strip-all
find ${DIRECTORY}/../../public/uploads/* -type f -name "*.png" | xargs optipng -i1 -nc -nb -o7 -full