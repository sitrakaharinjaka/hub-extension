<?php

/**
 * Repository
 *
 * @author Pierre Baumes <pierre.b@disko.fr>
 */

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class ServiceTranslationRepository
 *
 * @package App\Repository
 */
class ServiceTranslationRepository extends BaseRepository
{
}
