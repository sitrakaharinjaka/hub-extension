<?php

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class MenuItemRepository
 */
class MenuItemRepository extends BaseRepository
{



    /**
     * ================= FRONT ===================
     */


    /**
     * ================= BACK ===================
     */


    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et')
            ->leftJoin('e.translations', 'et')
            ->orderBy('et.label', 'asc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'et';
                if ($key == 'id') {
                    $alias = 'e';
                }
                $qb->andWhere($alias.'.'.$key.' LIKE :'.$key);
                $qb->setParameter($key, '%'.$filter.'%');
            }
        }

        $query = $qb->getQuery();

        return $query;
    }

    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et', 'c')
            ->leftJoin('e.translations', 'et')
            ->leftJoin('et.cover', 'c')
            
            ->where('e.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }


    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findAllTranslations($menuitem)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->from('App\Entity\MenuItemTranslation', 't')
            ->select('t')
            ->andWhere('t.menuitem = :menuitem')
            ->setParameter('menuitem', $menuitem);

        $query = $qb->getQuery();
        return $query->getResult();
    }


    /**
     * Find all by pos deep
     *
     * @param $id
     * @return mixed
     */
    public function findAllByPosDeep()
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->addOrderBy('e.deepness', 'asc')
            ->addOrderBy('e.position', 'asc');

        $query = $qb->getQuery();
        return $query->getResult();
    }
}
