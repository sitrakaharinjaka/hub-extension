<?php

namespace App\Utils\Managers;

use App\Entity\StockEntry;
use App\Entity\StockEntry115;
use App\Entity\StockEntry115Item;
use App\Entity\StockEntryItem;
use App\Form\Model\StockEntry115Model;
use App\Form\Model\StockEntryModel;
use App\Repository\StockEntryRepository;
use App\Utils\Managers\LocaleManager;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class StockManager
 * `
 * Object manager of stock
 *
 * @package Disko\PageBundle\Managers
 */
class StockEntry115Manager extends BaseManager
{
    private $productManager;
    protected $stockEntry115Repository;

    /**
     * StockManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager, ProductManager $productManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('stockEntry115Repository', StockEntry115::class);
        $this->productManager = $productManager;
    }

    /**
     * Save a stock entry
     *
     * @param StockEntry115 $stockEntry115
     */
    public function save(StockEntry115 $stockEntry115)
    {
        $stockEntry115->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($stockEntry115);
        $this->getEntityManager()->flush();
        return $stockEntry115;
    }

    /**
     * Remove one stock entry 115
     *
     * @param StockEntry115 StockEntry115
     */
    public function remove(StockEntry115 $stockEntry115)
    {
        $this->getEntityManager()->remove($stockEntry115);
        $this->getEntityManager()->flush();
    }

    public function createFromModel(StockEntry115Model $stockEntry115Model, $user)
    {
        $stockEntry115 = new StockEntry115();
        $stockEntry115->setUserName($stockEntry115Model->getUsername());
        $stockEntry115->setStock($stockEntry115Model->getStock());
        $stockEntry115->setEmail($stockEntry115Model->getEmail());
        $stockEntry115->setPhone($stockEntry115Model->getPhone());
        $stockEntry115->setBillingCenter($stockEntry115Model->getBillingCenter());
        $stockEntry115->setPickUpDate($stockEntry115Model->getPickUpDate());
        $stockEntry115->setTimeSlots($stockEntry115Model->getTimeSlots());
        $stockEntry115->setPickUpLocation($stockEntry115Model->getPickUpLocation());
        $stockEntry115->setFloor($stockEntry115Model->getFloor());
        $stockEntry115->setOfficeNumber($stockEntry115Model->getOfficeNumber());
        $stockEntry115->setUser($user);
        $stockEntry115->setDateSendToVeolog(null);
        $stockEntry115->setStatus(StockEntry115::STATE_REQUEST_TAKEN);
        foreach ($stockEntry115Model->getStockEntry115Items() as $itemModel) {
            $stockEntry115Item = $this->createItemFromModel($itemModel);
            $stockEntry115Item->setStockEntry115($stockEntry115);
            $stockEntry115->addStockEntry115Item($stockEntry115Item);
        }
        //dd($stockEntry115);
        return $stockEntry115;
    }

    protected function createItemFromModel($model)
    {
        $stockEntry115Item = new StockEntry115Item();
        $stockEntry115Item->setProduct($this->productManager->findOneBy(['id' => $model->getProductId()]));
        $stockEntry115Item->setStockEntry115(null);
        $stockEntry115Item->setStatus(StockEntry115::STATE_ITEM_CREATE);
        $stockEntry115Item->setProductState($model->getProductState());
        $stockEntry115Item->setQuantity($model->getQuantity());
        $stockEntry115Item->setQuantityReceived(0);
        return $stockEntry115Item;
    }

    public function findOneToUpdate($id)
    {
        return $this->stockEntry115Repository->findOneToUpdate($id);
    }

    public function findOneToConfirm($id)
    {
        return $this->stockEntry115Repository->findOneToConfirm($id);
    }

    public function findAllToSynch()
    {
        return $this->stockEntry115Repository->findAllToSynch();
    }

    /**
     * Get all stock entries
     *
     * @param  array  $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->stockEntry115Repository->queryForSearch($filters);
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->stockEntry115Repository->findOneBy($filters);
    }

    /**
     * Find by
     *
     * @param  array  $filters
     *
     * @return mixed
     */
    public function findBy($filters = array())
    {
        return $this->stockEntry115Repository->findBy($filters);
    }

    public function count($filters)
    {
        return $this->stockEntry115Repository->count($filters);
    }

    public function findNonArchivedStock($stock)
    {
        return $this->stockEntry115Repository->findNonArchivedStock($stock);
    }
}
