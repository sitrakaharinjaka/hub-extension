<?php

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;

use App\Utils\Managers\LocaleManager;
use App\Entity\ServiceProvider;
use App\Entity\ServiceProviderTranslation;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class ServiceProviderManager
 * `
 * Object manager of serviceprovider
 *
 * @package Disko\PageBundle\Managers
 */
class ServiceProviderManager extends BaseManager
{

    /**
     * Repository
     *
     * @var ServiceProviderRepository
     */
    protected $serviceproviderRepository;

    /**
     * ServiceProviderManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('serviceproviderRepository', ServiceProvider::class);
    }

    /**
     * Save a serviceprovider
     *
     * @param ServiceProvider $serviceprovider
     */
    public function save(ServiceProvider $serviceprovider)
    {
        $serviceprovider->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($serviceprovider);
        $this->getEntityManager()->flush();

        return $serviceprovider;
    }

    /**
     * Remove one serviceprovider
     *
     * @param ServiceProvider $serviceprovider
     */
    public function remove(ServiceProvider $serviceprovider)
    {
        $this->getEntityManager()->remove($serviceprovider);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one Active
     *
     * @param $slugWl
     * @param $locale
     * @return mixed
     */
    public function findOneActive($slugWl, $locale)
    {
        $serviceprovider = $this->serviceproviderRepository->findOneActive($slugWl, $locale);

        return $serviceprovider;
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $serviceprovider = $this->serviceproviderRepository->findOneToEdit($id);

        return $serviceprovider;
    }

    /**
     * Get all serviceprovider
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->serviceproviderRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->serviceproviderRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->serviceproviderRepository->findOneBy($filters);
    }
}
