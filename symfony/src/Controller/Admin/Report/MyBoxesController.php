<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin\Report;

use App\Entity\Connection;
use App\Entity\MyBoxesBox;
use App\Entity\User;
use App\Form\Model\SearchDateToDate;
use App\Form\Model\SearchUser;
use App\Form\Type\SearchDateToDateType;
use App\Form\Type\User\SearchUserType;
use App\Form\Type\User\UserCreateType;
use App\Form\Type\User\UserType;
use App\Utils\Managers\ConnectionManager;
use App\Utils\Managers\MyBoxesBoxManager;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockLineManager;
use App\Utils\Managers\StockManager;
use App\Utils\Managers\UserManager;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 *
 * @Route("/admin/report")
 * @package App\Controller
 */
class MyBoxesController extends BaseController
{
    /**
     * @Route("/my-boxes", name="admin_myBoxes_report")
     */
    public function indexAction(Request $request, MyBoxesBoxManager $myBoxesBoxManager,ProjectManager $projectManager, UserManager $userManager, StockLineManager $stockLineManager)
    {
        // init search
        $data = $this->initSearch($request);
        $form = $this->createForm(SearchDateToDateType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Rapports MyBoxes');

        $stockLineManager->checkAndUpdateFullGroupOnlyOption();

        $nbBoxAlone = $myBoxesBoxManager->nbBoxAlone($data->getStart(), $data->getEnd(), true);
        $nbBoxTeam = $myBoxesBoxManager->nbBoxTeam($data->getStart(), $data->getEnd(), true);
        $nbBoxTypes = $myBoxesBoxManager->nbBoxByType($data->getStart(), $data->getEnd(), true);

        $nbBoxAloneNoState = $myBoxesBoxManager->nbBoxAlone($data->getStart(), $data->getEnd(), false);
        $nbBoxTeamNoState = $myBoxesBoxManager->nbBoxTeam($data->getStart(), $data->getEnd(), false);
        $nbBoxTypesNoState = $myBoxesBoxManager->nbBoxByType($data->getStart(), $data->getEnd(), false);

        $boxUsers = $userManager->topBox(15);
        $boxInSleeps = $myBoxesBoxManager->nbBoxInSleep(15);
        $boxInMovements = $myBoxesBoxManager->nbBoxMovement($data->getStart(), $data->getEnd(), 15);

        $topDelay = $projectManager->topByDelay($data->getStart(), $data->getEnd(), 15);

        $topByStorageDelay = $myBoxesBoxManager->topByStorageDelay($data->getStart(), $data->getEnd(), 15);

      // Render view
        return $this->render('@Admin/Report/MyBoxes/index.html.twig', array(
            'data' => $data,
            'nbBoxAlone' => $nbBoxAlone,
            'nbBoxAloneNoState' => $nbBoxAloneNoState,
            'nbBoxTeam' => $nbBoxTeam,
            'nbBoxTeamNoState' => $nbBoxTeamNoState,
            'nbBoxTypes' => $nbBoxTypes,
            'nbBoxTypesNoState' => $nbBoxTypesNoState,
            'boxUsers' => $boxUsers,
            'boxInSleeps' => $boxInSleeps,
            'topDelay' => $topDelay,
            'topByStorageDelay' => $topByStorageDelay,
            'form' => $form->createView(),
            'boxInMovements' => $boxInMovements
        ));
    }

    /**
     * @Route("/my-boxes/top-user/ajax", name="admin_myBoxes_report_top_ajax")
     */
    public function topUserAction(Request $request, UserManager $userManager)
    {
        $search = $request->get('search', '');
        $search = trim($search);

        $boxUsers = $userManager->topBox(15, $search);

        return $this->render('@Admin/Report/MyBoxes/partials/table-top-user.html.twig', array(
            'boxUsers' => $boxUsers
        ));
    }


    /**
     * @Route("/my-boxes/sleep-box/ajax", name="admin_myBoxes_report_sleep_box_ajax")
     */
    public function sleepBoxAction(Request $request, MyBoxesBoxManager $myBoxesBoxManager)
    {
        $search = $request->get('search', '');
        $search = trim($search);

        $boxInSleeps = $myBoxesBoxManager->nbBoxInSleep(15, $search);

        return $this->render('@Admin/Report/MyBoxes/partials/table-sleep-box.html.twig', array(
            'boxInSleeps' => $boxInSleeps
        ));
    }



    /**
     * @Route("/my-boxes/movement-box/ajax", name="admin_myBoxes_report_movement_box_ajax")
     */
    public function movementBoxAction(Request $request, MyBoxesBoxManager $myBoxesBoxManager)
    {
        $search = $request->get('searchmovement', '');
        $search = trim($search);

        $searchStart = $request->get('searchdatestart', '');
        $searchEnd = $request->get('searchdateend', '');
        if (empty($searchStart) or empty($searchEnd)) {
            $boxInMovements = [];
        } else {
            $searchStart = \DateTime::createFromFormat("d/m/Y", $searchStart);
            $searchEnd = \DateTime::createFromFormat("d/m/Y", $searchEnd);
            $boxInMovements = $myBoxesBoxManager->nbBoxMovement($searchStart, $searchEnd, 15, $search);
        }

        return $this->render('@Admin/Report/MyBoxes/partials/table-movement-box.html.twig', array(
            'boxInMovements' => $boxInMovements,
        ));
    }

    /**
     * Init Search object
     *
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        $dateTimeDefaultStart = new \DateTime();
        $dateTimeDefaultStart->modify('-14 day');

        $dateTimeDefaultEnd = new \DateTime();

        // Init form
        $data = new SearchDateToDate();
        $data->setStart((isset($filters['start'])) ? \DateTime::createFromFormat("d/m/Y", $filters['start']) : $dateTimeDefaultStart);
        $data->setEnd((isset($filters['end']))   ? \DateTime::createFromFormat("d/m/Y", $filters['end']) : $dateTimeDefaultEnd);

        return $data;
    }
}
