<?php

namespace App\DataFixtures;

use App\Entity\Contact;
use App\Entity\ContactTranslation;
use App\Utils\Managers\ContactManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * @package Disko\UserBundle\DataFixtures\ORM
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class LoadContactData extends Fixture implements DependentFixtureInterface
{
    private $contactManager;
    private $localeManager;

    public function __construct(ContactManager $contactManager, LocaleManager $localeManager)
    {
        $this->contactManager = $contactManager;
        $this->localeManager = $localeManager;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
//        $contacts = array(
//      [
//        'name' => 'Passouvent',
//        'firstname' => 'Henry',
//        'email' => 'henry@passouvent.fr',
//        'phone' => '0404040404'
//      ],
//      [
//        'name' => 'Manvussa',
//        'firstname' => 'Gérard',
//        'email' => 'gerard@manvussa.fr',
//        'phone' => '0505050505'
//      ]
//    );
//
//        $locales = $this->localeManager->findAll();
//
//        foreach ($contacts as $contact) {
//            $obj = new Contact();
//            foreach ($locales as $locale) {
//                $trans = new ContactTranslation();
//                $trans->setLocale($locale->getSlug());
//                $trans->setName($contact['name']);
//                $trans->setFirstname($contact['firstname']);
//                $trans->setEmail($contact['email']);
//                $trans->setPhone($contact['phone']);
//                $obj->addTranslation($trans);
//            }
//            $this->contactManager->save($obj);
//            $this->addReference('admin-contact-'.$obj->getId(), $obj);
//        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

    public function getDependencies()
    {
        return [
      LoadLocaleData::class
    ];
    }
}
