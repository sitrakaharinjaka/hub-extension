<?php

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class ServiceRepository
 */
class ServiceRepository extends BaseRepository
{



    /**
     * ================= FRONT ===================
     */


    /**
     * ================= BACK ===================
     */


    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et')
            ->leftJoin('e.translations', 'et')
            ->orderBy('et.name', 'asc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'et';
                if ($key == 'id') {
                    $alias = 'e';
                }
                $qb->andWhere($alias.'.'.$key.' LIKE :'.$key);
                $qb->setParameter($key, '%'.$filter.'%');
            }
        }

        $query = $qb->getQuery();

        return $query;
    }

    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et', 'sp', 'spt')
            ->leftJoin('e.translations', 'et')
            ->leftJoin('et.serviceProvider', 'sp')
            ->leftJoin('sp.translations', 'spt')
            ->where('e.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }


    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findAllTranslations($service)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->from('App\Entity\ServiceTranslation', 't')
            ->select('t')
            ->andWhere('t.service = :service')
            ->setParameter('service', $service);

        $query = $qb->getQuery();
        return $query->getResult();
    }
}
