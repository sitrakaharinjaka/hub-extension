<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Form\Type\User\EmailTestSendType;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\Model\EmailTestSend;

/**
 * Class EmailController
 * @Route("/admin/email")
 * @package App\Controller
 */
class EmailController extends BaseController
{
    /**
     * Email test form
     * @Route("/test-form", name="admin_email_test_form")
     * @param Request $request Request
     */
    public function form(Request $request)
    {
        // Init form
        $model = new EmailTestSend();
        $model->setFrom($this->getParameter('mailer_from'));
        $model->setTo($this->getParameter('kernel.environment') == 'prod' ? $this->getParameter('mailer_to') :  $this->getParameter('mailer_delivery_address'));
        $form = $this->createForm(EmailTestSendType::class, $model, [
            'em' => $this->getDoctrine()->getManager(),
            'validation_groups' => ['Default']
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                $message = (new \Swift_Message($model->getSubject()))
                    ->setFrom($model->getFrom())
                    ->setTo($model->getTo())
                    ->setBody(
                        $model->getContent(),
                        'text/html'
                    )
                ;

                $this->get('mailer')->send($message);

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'notice',
                    "Email de test envoyé"
                );

                return $this->redirect($request->headers->get('referer'));
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        } else {
            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'info',
                "Vous êtes en environnement de <b>".$this->getParameter('kernel.environment')."</b>"
            );
        }

        // View
        return $this->render(
            '@Admin/Email/test-form.html.twig',
            array(
                'form'           => $form->createView(),
            )
        );
    }
}
