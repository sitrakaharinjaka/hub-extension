<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\Locale;
use App\Form\Model\SearchLocale;
use App\Form\Type\Locale\SearchLocaleType;
use App\Form\Type\Locale\LocaleType;
use App\Utils\Managers\LocaleManager;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LocaleController
 *
 * @Route("/admin/locales")
 * @package Disko\AdminBundle\Controller
 */
class LocaleController extends BaseController
{
    /**
     * Locale's list
     *
     * @Route("/list", name="admin_locale_list")
     * @param Request $request Request
     */
    public function listAction(Request $request, LocaleManager $localeManager)
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchLocaleType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des langues');

        // Generate query
        $query = $localeManager->getQueryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20
        );

        // Render view
        return $this->render('@Admin/Locale/index.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     *
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchLocale();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setCode((isset($filters['code']))   ? $filters['code'] : '');

        return $data;
    }

    /**
     * Create a locale
     *
     * @Route("/create", name="admin_locale_create")
     *
     * @param Request $request Request
     *
     * @return Response
     */
    public function createAction(Request $request, LocaleManager $localeManager)
    {
        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");

        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des locales", $this->get("router")->generate("admin_locale_list"));
        $breadcrumbs->addItem("Create one locale");


        // Init form
        $locale = new Locale();

        $form = $this->createForm(LocaleType::class, $locale);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {

                // Save
                $localeManager->save($locale);

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'notice',
                    "La langue a bien été créée"
                );

                return $this->redirect($this->generateUrl('admin_locale_edit', array(
                    'id' => $locale->getId()
                )));
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/Locale/create.html.twig',
            array(
                'form'           => $form->createView(),
                'locale'           => $locale,
            )
        );
    }

    /**
     * Update a locale
     *
     * @Route("/edit/{id}", name="admin_locale_edit")
     *
     * @param Request $request Request
     * @param integer $id      Id of locale
     *
     * @return Response
     */
    public function editAction(Request $request, LocaleManager $localeManager, $id)
    {
        // Find it
        $locale =  $localeManager->findOneToEdit($id);
        if (!$locale) {
            throw $this->createNotFoundException('Locale not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des langues", $this->get("router")->generate("admin_locale_list"));
        $breadcrumbs->addItem("Edition d'une langue");

        // Init form
        $form = $this->createForm(LocaleType::class, $locale);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {

                // Save
                $localeManager->save($locale);

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'notice',
                    "La langue a bien été mise à jour"
                );

                return $this->redirect($request->headers->get('referer'));
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/Locale/edit.html.twig',
            array(
                'form'           => $form->createView(),
                'locale'           => $locale,
            )
        );
    }

    /**
     * Delete one locale
     *
     * @Route("/delete/{id}", name="admin_locale_delete")
     *
     * @param Request $request Request
     * @param integer $id      Id of locale
     */
    public function deleteAction(Request $request, LocaleManager $localeManager, $id)
    {
        // Find it
        /** @var Locale $locale */
        $locale =  $localeManager->findOneToEdit($id);
        if (!$locale) {
            throw $this->createNotFoundException('Locale not found');
        }

        $localeManager->remove($locale);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            'La langue "'.$locale->getCode().'" a bien été supprimée'
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Export csv
     *
     * @Route("/export", name="admin_locale_export")
     *
     * @param Request $request
     */
    public function exportAction(Request $request, LocaleManager $localeManager)
    {
        $data = $this->initSearch($request);
        $query = $localeManager->getQueryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Code',
            'Par défaut',
            'Mise à jour le',
            'Créé le'
        ], ';');

        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->getCode(),
                $result->getDefault(),
                $result->getUpdated()->format('m/d/Y h:i'),
                $result->getCreated()->format('m/d/Y h:i')
            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_locales_export.csv"'
        ));
    }


    /**
     * Export choice locale of Example
     *
     * @Route("/choice/{routename}", name="admin_locale_export_choice")
     * @param Request $request Request
     * @param integer $id      Id of KeywordLink
     */
    public function exportChoiceAction(Request $request, LocaleManager $localeManager, $routename)
    {
        // Find it
        /** @var Example $tag */
        $locales =  $localeManager->findAll();

        $routeparams = $request->get('routeparams');
        if ($routeparams == null) {
            $routeparams = [];
        }

        // View
        return $this->render(
            '@Admin/Locale/choice.html.twig',
            array(
                'routename' => $routename,
                'routeparams' => $routeparams,
                'locales' => $locales,
            )
        );
    }
}
