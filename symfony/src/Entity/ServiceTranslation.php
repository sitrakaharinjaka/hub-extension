<?php

namespace App\Entity;

use App\Entity\Traits\AuthorableTrait;
use App\Entity\Traits\GedmoTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

// Use For Algolia Or Api

/**
 * @ORM\Entity(repositoryClass="App\Repository\ServiceRepository")
 * @ORM\Table(name="dk_service_translation")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ServiceTranslation implements NormalizableInterface
{
    use ORMBehaviors\Translatable\Translation;
    use GedmoTrait;

    use AuthorableTrait;

    public const CHOICE_SERVICE_BEFORE = 'before';
    public const CHOICE_SERVICE_AFTER = 'after';

    public const LIST_CHOICE_SERVICE = [
        'app.service.'.self::CHOICE_SERVICE_BEFORE => self::CHOICE_SERVICE_BEFORE,
        'app.service.'.self::CHOICE_SERVICE_AFTER => self::CHOICE_SERVICE_AFTER
    ];

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Name
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $name;

    /**
     * Description
     *
     * @ORM\Column(length=255, name="description", nullable=true)
     */
    protected $description;

    /**
     * Slug
     *
     * @Gedmo\Slug(fields={"name"}, unique=false)
     * @ORM\Column(length=255, unique=false)
     */
    protected $slug;

    /**
     * @ORM\Column(length=3, name="code")
     */
    protected $code;

    /**
     * @ORM\Column(type="string", name="deadline", nullable=true)
     */
    protected $deadline;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $choiceMomentService = self::CHOICE_SERVICE_BEFORE;

    /**
     * @ORM\Column(type="string", name="stock")
     */
    protected $stock;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServiceProvider", cascade={"all"})
     * @var ServiceProvider
     */
    protected $serviceProvider;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="is_service_standard", options={"default":1})
     */
    protected $isServiceStandard = true;

    /**
     * Set id
     *
     * @param integer $id
     * @return Service
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Service
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param mixed $deadline
     */
    public function setDeadline($deadline): void
    {
        $this->deadline = $deadline;
    }

    /**
     * @return string
     */
    public function getChoiceMomentService(): string
    {
        return $this->choiceMomentService;
    }

    /**
     * @param string $choiceMomentService
     */
    public function setChoiceMomentService(string $choiceMomentService): void
    {
        $this->choiceMomentService = $choiceMomentService;
    }

    /**
     * @return mixed
     */
    public function getServiceProvider()
    {
        return $this->serviceProvider;
    }

    /**
     * @param mixed $serviceProvider
     */
    public function setServiceProvider(ServiceProvider $serviceProvider): void
    {
        $this->serviceProvider = $serviceProvider;
    }

    /**
     * @return string
     */
    public function getStock(): ?string
    {
        return $this->stock;
    }

    /**
     * @param string $stock
     */
    public function setStock(string $stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @return null|string
     */
    public function getCave()
    {
        return $this->getStock();
    }

    /**
     * @return bool
     */
    public function isServiceStandard(): ?bool
    {
        return $this->isServiceStandard;
    }

    /**
     * @param bool $isServiceStandard
     */
    public function setIsServiceStandard(?bool $isServiceStandard): void
    {
        $this->isServiceStandard = $isServiceStandard;
    }

    /**
     * Use to serialize object for Algolia or Api
     *
     * @param NormalizerInterface $normalizer
     * @param null $format
     * @param array $context
     * @return array|bool|float|int|string
     */
    public function normalize(NormalizerInterface $normalizer, $format = null, array $context = array())
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'locale' => $this->getLocale(),
            'slug' => $this->getSlug(),
            'translatable_id' => $this->getTranslatable()->getId(),
        ];
    }

    /**
     * Get description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }
}
