<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin\Report;

use App\Entity\Project;
use App\Form\Model\SearchDateToDate;
use App\Form\Type\SearchDateToDateType;
use App\Utils\Managers\ConnectionManager;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Controller\BaseController;
use App\Utils\Managers\RunnerTracingManager;
use App\Utils\Managers\StockLineManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class Stock115Controller
 *
 * @Route("/admin/report")
 * @package App\Controller
 */
class Stock115Controller extends BaseController
{
    public const COLORS = ['red', 'green', 'blue', 'gold'];

    /**
     * @Route("/stock115", name="admin_stock115_report")
     */
    public function indexAction(Request $request, ProjectManager $projectManager, StockLineManager $stockLineManager, RunnerTracingManager $runnerTracingManager)
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchDateToDateType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Rapports Stock115');

        $stockLineManager->checkAndUpdateFullGroupOnlyOption();

        $statsProjectCreated      = $projectManager->statsCreated($data->getStart(), $data->getEnd(), Project::CAVE_115);
        $statsProductByProject    = $projectManager->statsCreatedProject($data->getStart(), $data->getEnd(), Project::CAVE_115);
        $statsProjectDeliveryTime = $projectManager->statsDeliveryTimeProject($data->getStart(), $data->getEnd());

        $tracings = $runnerTracingManager->findAll();

        return $this->render('@Admin/Report/Stock115/index.html.twig', array(
            'statsProjectCreated'      => $this->transformTableSQL2TableGoogleChart($data->getStart(), $data->getEnd(), $statsProjectCreated),
            'statsProductByProject'    => $this->transformTableSQL2TableGoogleChart($data->getStart(), $data->getEnd(), $statsProductByProject),
            'statsProjectDeliveryTime' => $statsProjectDeliveryTime,
            'tracings'                 => $tracings,

            'data' => $data,
            'form' => $form->createView()
        ));
    }

    private function transformTableSQL2TableGoogleChart($start, $stop, $results)
    {
        $response = [];
        $response['labels'] = [];
        $response['data'] = [];

        // Ici on a :
        // $response = [
        //  'labels' => []
        //  'data' => [
        //      ],
        // ];

        // On initialise les données par stock
        $keysStock = [];
        foreach ($results as $result) {
            $index = isset($keysStock[$result['stock_name']]) ? $keysStock[$result['stock_name']] : sizeof($response['data']);
            $keysStock[$result['stock_name']] = $index;
            $response['data'][$index] = [
                'name'=> $result['stock_name'],
                'color' => self::COLORS[ $index % sizeof(self::COLORS)],
                'numbers' => [],
            ];
        }

        // Ici on a :
        // $response = [
        //  'labels' => []
        //  'data' => [
        //          ['name'=>'Stock 1', 'numbers' => []],
        //          ['name'=>'Stock 2', 'numbers' => []],
        //          ['name'=>'Stock 3', 'numbers' => []],
        //      ],
        // ];

        // On initialise les données des stocks par jour à 0
        $keysDay = [];
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($start, $interval, $stop);
        foreach ($period as $index => $dt) {
            $response['labels'][$index] = $dt->format("Y/m/d");
            $keysDay[$dt->format("Y/n/j")] = $index;
            foreach ($response['data'] as $key => $data) {
                $response['data'][$key]['numbers'][$index] = 0;
            }
        }

        // Ici on a :
        // $response = [
        //  'labels' => ['2020/01/01', '2020/01/02', '2020/01/03', '2020/01/04']
        //  'data' => [
        //          ['name'=>'Stock 1', 'numbers' => [0,0,0,0]],
        //          ['name'=>'Stock 2', 'numbers' => [0,0,0,0]],
        //          ['name'=>'Stock 3', 'numbers' => [0,0,0,0]],
        //      ],
        // ];

        foreach ($results as $result) {
            if (!isset($keysDay[$result['date']])) {
                // ERROR
                continue;
            }
            $indexDay = $keysDay[$result['date']];
            $indexStock = $keysStock[$result['stock_name']];
            $response['data'][$indexStock]['numbers'][$indexDay] = $result['cpt'];
        }

        return $response;
    }

    /**
     * Init Search object
     *
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        $dateTimeDefaultStart = new \DateTime();
        $dateTimeDefaultStart->modify('-14 day');

        $dateTimeDefaultEnd = new \DateTime();

        // Init form
        $data = new SearchDateToDate();
        $data->setStart((isset($filters['start'])) ? \DateTime::createFromFormat("d/m/Y", $filters['start']) : $dateTimeDefaultStart);
        $data->setEnd((isset($filters['end']))   ? \DateTime::createFromFormat("d/m/Y", $filters['end']) : $dateTimeDefaultEnd);

        return $data;
    }
}
