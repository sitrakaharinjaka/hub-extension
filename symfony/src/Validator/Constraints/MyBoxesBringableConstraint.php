<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MyBoxesBringableConstraint extends Constraint
{
    public $message = 'Cette Box n\'est pas disponible pour le rapatriement.';

    public function validatedBy()
    {
        return 'myboxes_bringable_constraint';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
