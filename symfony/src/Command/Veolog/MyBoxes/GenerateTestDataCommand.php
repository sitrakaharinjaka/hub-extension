<?php

namespace App\Command\Veolog\MyBoxes;

use App\Entity\MyBoxesBox;
use App\Entity\MyBoxesProduct;
use App\Entity\MyBoxesCommand;
use App\Entity\Product;
use App\Entity\Project;
use App\Utils\Managers\UserManager;
use App\Utils\Managers\MyBoxesBoxManager;
use App\Utils\Managers\MyBoxesCommandManager;
use App\Utils\Managers\MyBoxesProductManager;
use App\Utils\Managers\MyBoxesItemManager;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\ServiceManager;
use App\Utils\Managers\ProjectItemManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class GenerateTestDataCommand extends Command
{
    public const STATE_CMD_USER_PREPARED_REMOVE_FOR_TEST = 'test_waiting';
    public const STATE_CMD_USER_ASKED_REMOVE_FOR_TEST = 'test_ask';
    public const STATE_CMD_VEOLOG_REFUSED_REMOVE_FOR_TEST = 'test_refused';
    public const STATE_CMD_VEOLOG_ACCEPTED_REMOVE_FOR_TEST = 'test_accepted';
    public const STATE_CMD_VEOLOG_PREPARED_REMOVE_FOR_TEST = 'test_prepared';
    public const STATE_CMD_VEOLOG_SENT_REMOVE_FOR_TEST = 'test_sent';

    /** @var  EntityManager */
    private $productManager;
    private $boxManager;
    private $commandManager;
    private $itemManager;
    private $userManager;
    private $rootDir;
    private $ENVIRONNEMENT;

    private const EXPEDIE = "X";
    private const PREPARE = "P";
    private const DELIMITER = ";";


    public function __construct(MyBoxesProductManager $productManager, MyBoxesItemManager $itemManager, UserManager $userManager, MyBoxesCommandManager $commandManager, MyBoxesBoxManager $boxManager, KernelInterface $kernel, ParameterBagInterface $parameterBag)
    {
        parent::__construct();
        $this->productManager = $productManager;
        $this->boxManager = $boxManager;
        $this->commandManager = $commandManager;
        $this->itemManager = $itemManager;
        $this->userManager = $userManager;
        $this->ENVIRONNEMENT = $parameterBag->get('server_mode');
    }

    protected static $defaultName = 'myboxes:generate:test';

    protected function configure()
    {
        $this
            ->setDescription('Synch veolog order ask')
            ->addOption(
                'vlgenv',
                null,
                InputOption::VALUE_OPTIONAL,
                'Env veolog prod?',
                false
            )
            ->addOption(
                'rollback',
                null,
                InputOption::VALUE_OPTIONAL,
                'Rollback data',
                false
            )
            ->setHelp('This command allows you to synch veolog order ask evry 5minutes');
    }

    private function makeDataCommand()
    {
        $command = new MyBoxesCommand();
        $command->setState(MyBoxesBox::STATE_CMD_USER_PREPARED);
        $command->setFirstname("Prenom");
        $command->setLastname("Nom");
        $command->setStage("Etage TEST");
        $command->setOfficeNumber("Bureau");
        $command->setUser($this->userManager->findOneBy(['email'=>'mateo@disko.fr']));
        return $command;
    }

    private function makeDataCommandProducts()
    {
        $command = $this->makeDataCommand();
        $products = $this->productManager->findAll();
        foreach ($products as $product) {
            $item = $this->itemManager->create($command, $product);
            $item->setQuantity(10);
            $command->addItem($item);
        }
        return $command;
    }

    private function makeDataCommandBox()
    {
        $command = $this->makeDataCommand();
        $box = new MyBoxesBox();
        $box->setName("BoxName");
        $box->setContent("Contenu");
        $box->setBarcode("AZERTY-123");
        $box->setSeal("SEAL0000123");
        $box->setUser($this->userManager->findOneBy(['email'=>'mateo@disko.fr']));
        $box->addUser($this->userManager->findOneBy(['email'=>'mateo@disko.fr']));
        $command->setBox($box);
        return $command;
    }

    /**
     * On veux les fichiers
     * - stock_BOX_AAMMJJHHMMSS.csv
     * - /XXX/stock_BOX_AAMMJJHHMMSS.csv
     * On ne veux pas :
     * - XXXstock_BOX_AMMJJHHMMSS.csv
     * - stock_BOX_AMMJJHHMMSS.xls
     * - stock_AAMMJJHHMMSS.csv
     */
    private function isStockCommandFile($filename)
    {
        $pattern='~^/?([a-zA-Z0-9_]*/)*stock_BOX_[0-9]*\.csv$~';
        return preg_match($pattern, $filename);
    }

    /**
     * On veux les fichiers
     * - status_orders_BOX_AAMMJJHHMMSS.csv
     * - /XXX/status_orders_BOX_AAMMJJHHMMSS.csv
     * On ne veux pas :
     * - XXXstatus_orders_BOX_AMMJJHHMMSS.csv
     * - status_orders_BOX_AMMJJHHMMSS.xls
     * - status_orders_AAMMJJHHMMSS.csv
     */
    private function isOrderConfirmCommandFile($filename)
    {
        $pattern='~^/?([a-zA-Z0-9_]*/)*status_orders_BOX_[0-9]*\.csv$~';
        return preg_match($pattern, $filename);
    }

    private function generateDataForOrderAskCommand($output)
    {
        $previous = $this->commandManager->findAllBy();
        foreach ($previous as $command) {
            $output->writeln("REMOVING Command of " . $command->getFirstName() . " " . $command->getLastName());
            if ($command->getState() == MyBoxesBox::STATE_CMD_USER_PREPARED) {
                $command->setState(GenerateTestDataCommand::STATE_CMD_USER_PREPARED_REMOVE_FOR_TEST);
            } elseif ($command->getState() == MyBoxesBox::STATE_CMD_USER_ASKED) {
                $command->setState(GenerateTestDataCommand::STATE_CMD_USER_ASKED_REMOVE_FOR_TEST);
            } elseif ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_PREPARED) {
                $command->setState(GenerateTestDataCommand::STATE_CMD_VEOLOG_PREPARED_REMOVE_FOR_TEST);
            } elseif ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_SENT) {
                $command->setState(GenerateTestDataCommand::STATE_CMD_VEOLOG_SENT_REMOVE_FOR_TEST);
            } elseif ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_REFUSED) {
                $command->setState(GenerateTestDataCommand::STATE_CMD_VEOLOG_REFUSED_REMOVE_FOR_TEST);
            } elseif ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_ACCEPTED) {
                $command->setState(GenerateTestDataCommand::STATE_CMD_VEOLOG_ACCEPTED_REMOVE_FOR_TEST);
            } else {
                $output->writeln("    ==> <comment>Status not found</comment>");
            }
            $this->commandManager->save($command);
        }

        $commandProducts = $this->makeDataCommandProducts();
        $output->writeln("CREATING Command Products of " . $commandProducts->getFirstName() . " " . $commandProducts->getLastName());
        $this->commandManager->save($commandProducts);

        $commandBox = $this->makeDataCommandBox();
        $output->writeln("CREATING Command Box of " . $commandBox->getFirstName() . " " . $commandBox->getLastName());
        $this->commandManager->save($commandBox);
    }

    private function rollbackDataForOrderAskCommand($output)
    {
        $previous = $this->commandManager->findAllBy();
        foreach ($previous as $command) {
            $output->writeln("ROLLBACK Command of " . $command->getFirstName() . " " . $command->getLastName());
            if ($command->getState() == GenerateTestDataCommand::STATE_CMD_USER_PREPARED_REMOVE_FOR_TEST) {
                $command->setState(MyBoxesBox::STATE_CMD_USER_PREPARED);
                $this->commandManager->save($command);
            } elseif ($command->getState() == GenerateTestDataCommand::STATE_CMD_USER_ASKED_REMOVE_FOR_TEST) {
                $command->setState(MyBoxesBox::STATE_CMD_USER_ASKED);
                $this->commandManager->save($command);
            } elseif ($command->getState() == GenerateTestDataCommand::STATE_CMD_VEOLOG_PREPARED_REMOVE_FOR_TEST) {
                $command->setState(MyBoxesBox::STATE_CMD_VEOLOG_PREPARED);
                $this->commandManager->save($command);
            } elseif ($command->getState() == GenerateTestDataCommand::STATE_CMD_VEOLOG_SENT_REMOVE_FOR_TEST) {
                $command->setState(MyBoxesBox::STATE_CMD_VEOLOG_SENT);
                $this->commandManager->save($command);
            } elseif ($command->getState() == GenerateTestDataCommand::STATE_CMD_VEOLOG_REFUSED_REMOVE_FOR_TEST) {
                $command->setState(MyBoxesBox::STATE_CMD_VEOLOG_REFUSED);
                $this->commandManager->save($command);
            } elseif ($command->getState() == GenerateTestDataCommand::STATE_CMD_VEOLOG_ACCEPTED_REMOVE_FOR_TEST) {
                $command->setState(MyBoxesBox::STATE_CMD_VEOLOG_ACCEPTED);
                $this->commandManager->save($command);
            } else {
                $this->commandManager->remove($command);
            }
        }
    }

    private function generateDataForStockCommand($output)
    {
        $source = __DIR__.'/../../../../imports/';

        $filenames = scandir($source);
        foreach ($filenames as $filename) {
            if ($this->isStockCommandFile($filename)) {
                if (rename($source.$filename, $source.'REMOVE_FOR_TEST_'.$filename)) {
                    $output->writeln('RENAMING file ' . $filename . ' <info>success</info>');
                } else {
                    $output->writeln('RENAMING file ' . $filename . ' <comment>error</comment>');
                }
            }
        }

        $filename = 'stock_BOX_20190101000000.csv';
        $fp = fopen($source.$filename, 'c');

        $products = $this->productManager->findAll();
        foreach ($products as $product) {
            $fields = array('L', 'MYBOXES', $product->getSku(), 'EAN', 100);
            fputcsv($fp, $fields, GenerateTestDataCommand::DELIMITER);
        }
        $codes = ['CODEBARRE-0001', 'CODEBARRE-0002', 'CODEBARRE-0003', 'CODEBARRE-0004', 'CODEBARRE-0005'];
        foreach ($codes as $code) {
            $fields = array('L', 'MYBOXES', $code, 'EAN', 0);
            fputcsv($fp, $fields, GenerateTestDataCommand::DELIMITER);
        }
        fputcsv($fp, ['F', sizeof($products) + sizeof($codes)], GenerateTestDataCommand::DELIMITER);
        fclose($fp);
        $output->writeln('CREATING file ' . $filename . ' <info>success</info>');
    }

    private function rollbackDataForStockCommand($output)
    {
        $source = __DIR__.'/../../../../imports/';

        $filenames = scandir($source);
        foreach ($filenames as $filename) {
            if ($this->isStockCommandFile($filename)) {
                if (unlink($source.$filename)) {
                    $output->writeln('REMOVING file ' . $filename . ' <info>success</info>');
                } else {
                    $output->writeln('REMOVING file ' . $filename . ' <comment>error</comment>');
                }
            } elseif ($this->isStockCommandFile(substr($filename, strlen('REMOVE_FOR_TEST_')))) {
                if (rename($source.$filename, $source.substr($filename, strlen('REMOVE_FOR_TEST_')))) {
                    $output->writeln('RENAMING file ' . $filename . ' <info>success</info>');
                } else {
                    $output->writeln('RENAMING file ' . $filename . ' <comment>error</comment>');
                }
            }
        }
    }

    private function generateDataForOrderConfirmCommand($output)
    {
        $source = __DIR__.'/../../../../imports/';

        $filenames = scandir($source);
        foreach ($filenames as $filename) {
            if ($this->isOrderConfirmCommandFile($filename)) {
                if (rename($source.$filename, $source.'REMOVE_FOR_TEST_'.$filename)) {
                    $output->writeln('RENAMING file ' . $filename . ' <info>success</info>');
                } else {
                    $output->writeln('RENAMING file ' . $filename . ' <comment>error</comment>');
                }
            }
        }

        $previous = $this->commandManager->findAllBy(['stage'=>'Etage TEST']);
        foreach ($previous as $key => $command) {
            $filename = 'status_orders_BOX_2019010100000' . $key . '.csv';
            $fp = fopen($source.$filename, 'c');

            $options = $command->getBox() ? ['filesuffix'=>'CMDBOXES', 'idprefix'=>'BOX'] : ['filesuffix'=>'CMDPRODUCTS', 'idprefix'=>'PRD'];

            if ($key == 0) {
                $fields = array('E', 'MYBOXES', $this->createIdToVelogCode($command->getId(), $options), 'CODE_VEOLOG', '20200101', '0000', GenerateTestDataCommand::EXPEDIE);
                fputcsv($fp, $fields, GenerateTestDataCommand::DELIMITER);
                $fields = array('C', 'COLIS_NUMBER_0000000', 'ID_TRACKING_TRANSPORTEUR', 'http://tracker.com');
                fputcsv($fp, $fields, GenerateTestDataCommand::DELIMITER);
            } else {
                $fields = array('E', 'MYBOXES', $this->createIdToVelogCode($command->getId(), $options), 'CODE_VEOLOG', '20200101', '0000', GenerateTestDataCommand::PREPARE);
                fputcsv($fp, $fields, GenerateTestDataCommand::DELIMITER);
            }

            $fields = array('L', 1, 123, 'LIBELLE COURT', 1, '');
            fputcsv($fp, $fields, GenerateTestDataCommand::DELIMITER);

            $fields = array('F', 0);
            fputcsv($fp, $fields, GenerateTestDataCommand::DELIMITER);

            fclose($fp);
            $output->writeln('CREATING file ' . $filename . ' <info>success</info>');
        }
    }

    private function rollbackDataForOrderConfirmCommand($output)
    {
        $source = __DIR__.'/../../../../imports/';

        $filenames = scandir($source);
        foreach ($filenames as $filename) {
            if ($this->isOrderConfirmCommandFile($filename)) {
                if (unlink($source.$filename)) {
                    $output->writeln('REMOVING file ' . $filename . ' <info>success</info>');
                } else {
                    $output->writeln('REMOVING file ' . $filename . ' <comment>error</comment>');
                }
            } elseif ($this->isOrderConfirmCommandFile(substr($filename, strlen('REMOVE_FOR_TEST_')))) {
                if (rename($source.$filename, $source.substr($filename, strlen('REMOVE_FOR_TEST_')))) {
                    $output->writeln('RENAMING file ' . $filename . ' <info>success</info>');
                } else {
                    $output->writeln('RENAMING file ' . $filename . ' <comment>error</comment>');
                }
            }
        }
    }

    private function createIdToVelogCode($id, $options)
    {
        $code = '00000'.$id;
        $code = substr($code, strlen($code)-5);
        $code = $options['idprefix'].'_'.$code;
        $code = strtoupper($code);
        return $code;
    }



    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $env = $input->getOption('vlgenv');
        if ($env !== false) {
            $output->writeln('<error>NO TEST DATA GENERATION ON vlgenv</error>');
            die();
        }

        $rollback = $input->getOption('rollback');
        if ($rollback) {
            $this->rollbackDataForOrderAskCommand($output);
            $this->rollbackDataForStockCommand($output);
            $this->rollbackDataForOrderConfirmCommand($output);
        } else {
            $this->generateDataForOrderAskCommand($output);
            $this->generateDataForStockCommand($output);
            $this->generateDataForOrderConfirmCommand($output);
        }
    }
}
