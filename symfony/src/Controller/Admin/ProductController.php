<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\Brand;
use App\Entity\BrandTranslation;
use App\Entity\Product;
use App\Entity\ProductTranslation;
use App\Entity\ImportProduct;
use App\Form\Model\SearchProduct;
use App\Form\Type\Product\ProductTranslationType;
use App\Form\Type\Product\SearchProductType;
use App\Form\Type\Product\ProductType;
use App\Form\Type\Product\ImportProductType;
use App\Utils\Managers\BrandManager;
use App\Utils\Managers\CategoryManager;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\LocaleManager;
use App\Utils\Managers\StockManager;
use App\Utils\Managers\ToolboxManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Process\Process;
use App\Utils\Managers\ProductImportManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class ProductController
 * @Route("/admin/products")
 *
 * @package Disko\AdminBundle\Controller
 */
class ProductController extends BaseController
{
    /**
     * Product's list
     * @Route("/list/{askLocale}", name="admin_product_list", defaults={"askLocale": "fr"})
     *
     * @param  Request  $request  Request
     */
    public function listAction(
      Request $request,
      ProductManager $productManager,
      LocaleManager $localeManager,
      $askLocale = "fr"
    ) {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchProductType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem(
            'Tableau de bord',
            $this->get("router")->generate("admin_homepage")
        );
        $breadcrumbs->addItem('Liste des produits');

        // Generate query
        $query = $productManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries' => true)
        );

        // Render view
        return $this->render('@Admin/Product/index.html.twig', array(
            'pagination' => $pagination,
            'locales'    => $localeManager->findAllSlug(),
            'askLocale'  => $askLocale,
            'form'       => $form->createView()
        ));
    }

    /**
     * Init Search object
     *
     * @param  Request  $request  Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchProduct();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setName((isset($filters['name'])) ? $filters['name'] : '');
        $data->setCode((isset($filters['code'])) ? $filters['code'] : '');
        $data->setDescription((isset($filters['description'])) ? $filters['description'] : '');
        $data->setBrand((isset($filters['brand'])) ? $filters['brand'] : '');

        return $data;
    }

    /**
     * Create a product
     *
     * @Route("/create", name="admin_product_create")
     * @param  Request  $request  Request
     * @return Response
     */
    public function createAction(
        Breadcrumbs $breadcrumbs,
        Request $request,
        ProductManager $productManager,
        LocaleManager $localeManager,
        BrandManager $brandManager,
        ToolboxManager $toolboxManager,
        ValidatorInterface $validator,
        CategoryManager $categoryManager
    ) {
        if ($this->isGranted('ROLE_VEOLOG_ADMIN')) {
            throw $this->createAccessDeniedException("Veolog not access");
        }

        $breadcrumbs->addItem("Tableau de bord", $this->generateUrl('admin_homepage'));
        $breadcrumbs->addItem("Liste des produits", $this->generateUrl('admin_product_list'));
        $breadcrumbs->addItem("Création");

        // Init form
        $product = new Product();

        $data   = $request->get('product', []);
        $locale = $request->get('locale', $request->getDefaultLocale());

        $validationGroups = ['Default'];

        if (isset($data['translations'])) {
            if (isset($data['translations'][$locale]['categories'])) {
                foreach ($data['translations'][$locale]['categories'] as $category) {
                    $category = $categoryManager->findOneToEdit($category);
                    if ($category->translate('fr')->getSlug() == 'bouteille') {
                        array_push($validationGroups, 'Bottle');

                        if (isset($data['translations'][$locale]['wineCategory'])) {
                            $wineBottles = ['wine', 'sparkling_wine', 'champagne'];

                            if (in_array($data['translations'][$locale]['wineCategory'], $wineBottles)) {
                                array_push($validationGroups, 'Wine');
                            }
                        }
                    }
                }
            }
        }

        $form = $this->createForm(ProductType::class, $product, [
            'validation_groups' => $validationGroups
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {
            // Bind value with form
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                if (count($product->getTranslations()) > 0) {
                    $codesPrefix = $productManager->findByPrefixHubCode();
                    $code = $this->uniq_rand($codesPrefix);

                    $stock = $this->get('session')->get('current_admin_stock');
                    $product->translate($request->getLocale())->setOrigin($stock->getId());

                    $product->translate('fr')->setCode($code);
                    $product = $this->brandOtherManage($product, $brandManager);
                    $product->setDateSynch(null);
                    $productManager->save($product);

                    // Launch the message flash
                    $this->addFlash('notice', "Création terminée");

                    return $this->redirectToRoute('admin_product_show', [
                        'id' => $product->getId()
                    ]);
                } else {
                    // Launch the message flash
                    $this->addFlash('error', "Le formulaire est entièrement vide");
                }
            } else {
                // Launch the message flash
                $this->addFlash(
                    'error',
                    "Le formulaire contient des erreurs ou est vide"
                );
            }
        }

        $douaneToolBox = $toolboxManager->findOneByCategory('Nomenclature douanière');

        // View
        return $this->render('admin/Product/create.html.twig', [
            'form'    => $form->createView(),
            'product' => $product,
            'douaneToolBox' => $douaneToolBox
        ]);
    }

    public function uniq_rand($tab)
    {
        do {
            $rand='HUB_'.random_int(0000000000, 9999999999);
        } while (in_array($rand, $tab));
        return $rand;
    }

    /**
     * Import a file of products
     * @Route("/import", name="admin_product_import")
     *
     * @param  Request  $request  Request
     *
     * @return Response
     */
    public function importAction(Request $request, ProductManager $productManager)
    {
        if ($this->isGranted('ROLE_VEOLOG_ADMIN')) {
            throw $this->createAccessDeniedException("Veolog not access");
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");

        $breadcrumbs->addItem(
            'Tableau de bord',
            $this->get("router")->generate("admin_homepage")
        );
        $breadcrumbs->addItem(
            "Liste des produits",
            $this->get("router")->generate("admin_product_list")
        );
        $breadcrumbs->addItem("Import");

        // Init form
        $import = new ImportProduct();

        $form = $this->createForm(ImportProductType::class, $import, [
            'validation_groups' => ['Default']
        ]);
        $mktime = null;
        // Update method
        if ('POST' === $request->getMethod()) {
            // Bind value with form
            $form->handleRequest($request);
            if ($form->isValid()) {
                $mktime = mktime();
                $id = $this->getUser()->getId();
                $source = $import->getFile()->getPathName();
                $target =  $this->getImportFileName($mktime);
                $shandle = fopen($source, "r");
                $thandle = fopen($target, "w");
                while ($line = trim(mb_convert_encoding(fgets($shandle), "UTF-8", "UTF-8, ISO-8859-1, Windows-1252"))) {
                    fwrite($thandle, $line . "\n");
                }
                fclose($shandle);
                fclose($thandle);
                $this->get('session')->getFlashBag()->set('success', "Le fichier a été accepté et il est en cours de traitement.");
            } else {
                // Launch the message flash
                $this->get('session')->getFlashBag()->set('error', "Le fichier contient des erreurs");
            }
        }

        return $this->render(
            '@Admin/Product/import.html.twig',
            array(
                'form'    => $form->createView(),
                'mktime'  => $mktime,
            )
        );
    }

    /**
     * Import a file of products
     * @Route("/import/{mktime}/{page}", name="admin_product_import_ajax")
     *
     * @param  Request  $request  Request
     *
     * @return JsonResponse|Response
     */
    public function importAjaxAction($mktime, $page, Request $request, ProductManager $productManager, ProductImportManager $productImportManager)
    {
        if ($this->isGranted('ROLE_VEOLOG_ADMIN')) {
            throw $this->createAccessDeniedException("Veolog not access");
        }
        $filename = $this->getImportFileName($mktime);
        if (!file_exists($filename)) {
            return new JsonResponse(array(
                'status'  => 500,
                'message' => 'Erreur majeure : Le fichier fourni a été perdu.',
                'nb'      => 0,
                'nextURL' => null,
            ));
        }
        $handle = fopen($filename, "r");
        $entete = fgetcsv($handle, 0, ';', '"');

        $index = 0;
        $PAR_PAGE = 10;
        $startIndex = $page * $PAR_PAGE;
        while ($index < $startIndex) {
            $line = fgetcsv($handle, 0, ';', '"');
            $index++;
        }
        set_time_limit(0);
        $categories = $productImportManager->getExistingCategories();
        $brands = $productImportManager->getExistingBrands();
        $logs = [];
        while ($index < ($page+1) * $PAR_PAGE) {
            $line = fgetcsv($handle, 0, ';', '"');
            if ($line) {
                $error = $productImportManager->importProduct($line, $categories, $brands);
                $logs[] = [
                    'sku'   => $line[0],
                    'label-SAP' => $line[1],
                    'label' => $line[2],
                    'error' => $error,
                ];
                $index++;
            } else {
                return new JsonResponse(array(
                    'status'  => 200,
                    'nb'      => $index - $startIndex,
                    'logs'    => $logs,
                    'nextURL' => null,
                ));
            }
        }
        $hasNext = fgetcsv($handle, 0, ';', '"') ? true : false;
        return new JsonResponse(array(
            'status'  => 200,
            'nb'      => $index - $startIndex,
            'logs'    => $logs,
            'nextURL' => $hasNext ? $this->get('router')
                                         ->generate('admin_product_import_ajax', [
                                                'mktime' => $mktime,
                                                'page'   => $page+1,
                                            ], UrlGeneratorInterface::ABSOLUTE_URL)
                                    : null,
        ));
        // PATH => \/admin\/products\/import\/1584621544\/3
        // URL  => https:\/\/hub142.local\/admin\/products\/import\/1584621544\/3
    }

    private function getImportFileName($mktime)
    {
        $id = $this->getUser()->getId();
        return $this->getParameter('kernel.root_dir') . '/../imports/importProduct_' . $id . '_' . $mktime . '.csv';
    }

    /**
     * Update a product
     * @Route("/edit/{id}", name="admin_product_edit")
     *
     * @param  Request  $request  Request
     * @param  integer  $id  Id of product
     *
     * @return Response
     */
    public function editAction(
        Request $request,
        ProductManager $productManager,
        LocaleManager $localeManager,
        BrandManager $brandManager,
        ToolboxManager $toolboxManager,
        $id
    ) {
        // Find it
        /** @var Product $product */
        $product = $productManager->findOneToEdit($id);
        if (! $product) {
            throw $this->createNotFoundException('Product not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem(
            'Tableau de bord',
            $this->get("router")->generate("admin_homepage")
        );
        $breadcrumbs->addItem(
            "Liste des produits",
            $this->get("router")->generate("admin_product_list")
        );
        $breadcrumbs->addItem("Modification");

        $form = $this->createForm(ProductType::class, $product, [
            'validation_groups' => ['Default']
        ]);


        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($product->getTranslations()) > 0) {
                    $product = $this->brandOtherManage($product, $brandManager);
                    //$product->setDateSynch(null);

                    $productManager->save($product);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Mise à jour effectuée"
                    );

                    return $this->redirect($request->headers->get('referer'));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        $douaneToolBox = $toolboxManager->findOneByCategory('Nomenclature douanière');

        // View
        return $this->render(
            '@Admin/Product/edit.html.twig',
            array(
                'form'    => $form->createView(),
                'product' => $product,
                'douaneToolBox' => $douaneToolBox
            )
        );
    }

    /**
     * @Route("/show/{id}", name="admin_product_show")
     * @param  ProductManager  $productManager
     * @param $id
     * @param  string  $askLocale
     * @param  StockManager  $stockManager
     *
     * @return Response
     */
    public function showAction(ProductManager $productManager, $id, $askLocale = "fr", StockManager $stockManager)
    {
        // Find it
        /** @var Product $product */
        $product = $productManager->findOneToEdit($id);
        if (! $product) {
            throw $this->createNotFoundException('Product not found');
        }

        // info for veolog
        $vlgInfo                 = [];
        $vlgInfo['code_article'] = $product->translate('fr')->getCode();
        $vlgInfo['libelle_long'] = $product->translate('fr')->getDescription();
        $vlgInfo['libelle_long'] = $product->translate('fr')->getDescription();
        if (empty($product->translate('fr')->getName())) {
            $vlgInfo['libelle_court'] = 'NIL';
        } else {
            $vlgInfo['libelle_court'] = strtoupper($product->translate('fr')
                                                       ->getName());
        }
        $vlgInfo['ean'] = $product->translate('fr')->getEanPart();
        $vlgInfo['conditionement_unite'] = $product->translate('fr')->getUnitConditioning();
        $vlgInfo['conditionement_unite'] = $product->translate('fr')->getUnitConditioning();

        if (!empty($product->translate('fr')->getCategories()[0])) {
            $vlgInfo['famille_produit'] = strtoupper($product->translate('fr')->getCategories()[0]->getName());
        } else {
            $vlgInfo['famille_produit'] = '';
        }

        $vlgInfo['longueur'] = strtoupper($product->translate('fr')->getWidth());
        $vlgInfo['largeur'] = strtoupper($product->translate('fr')->getDepth());
        $vlgInfo['hauteur'] = strtoupper($product->translate('fr')->getHeight());
        $vlgInfo['poids'] = strtoupper($product->translate('fr')->getWeight());
        $vlgInfo['unite_par_carton'] = strtoupper($product->translate('fr')->getTotalUnitsByBox());

        if (empty($product->translate('fr')->getEanBox())) {
            $vlgInfo['ean_carton'] = strtoupper($product->translate('fr')->getEanPart());
        } else {
            $vlgInfo['ean_carton'] = strtoupper($product->translate('fr')->getEanBox());
        }
        $vlgInfo['longueur_carton'] = strtoupper($product->translate('fr')->getWidthBox());
        $vlgInfo['largeur_carton'] = strtoupper($product->translate('fr')->getDepthBox());
        $vlgInfo['hauteur_carton'] = strtoupper($product->translate('fr')->getHeightBox());
        $vlgInfo['poids_carton'] = strtoupper($product->translate('fr')->getWeightBox());
        $vlgInfo['informations_suppl'] = strtoupper($product->translate('fr')->getOtherInformation());
        $vlgInfo['produit_consommable'] = strtoupper($product->translate('fr')->isExpendable());
        $vlgInfo['nomenclature_douaniere'] = strtoupper($product->translate('fr')->getCustomsNomenclature());
        $vlgInfo['crd'] = strtoupper($product->translate('fr')->isCrd());
        $vlgInfo['iso2'] = strtoupper($product->translate('fr')->getCountry());
        $vlgInfo['litrage'] = strtoupper($product->translate('fr')->getLitrageBottle());
        $vlgInfo['effervescent'] = strtoupper($product->translate('fr')->isEffervescent());
        $vlgInfo['region'] = strtoupper($product->translate('fr')->getDesignationBottle());

        if ($product->translate('fr')->getVintage() == 'novintage' || empty($product->translate('fr')->getVintage())) {
            $vlgInfo['millesime'] = 0;
        } else {
            $vlgInfo['millesime'] = strtoupper($product->translate('fr')->getVintage());
        }

        $vlgInfo['couleur_du_vin'] = strtoupper($product->translate('fr')->getWineColor());
        $vlgInfo['categorie'] = strtoupper($product->translate('fr')->getWineCategory());

        if (!empty($product->translate('fr')->getDegreeAlcohol())) {
            $vlgInfo['pourcentage_alcool'] = floatval($product->translate('fr')->getDegreeAlcohol()) * 1000;
        } else {
            $vlgInfo['pourcentage_alcool'] = '';
        }

        $vlgInfoMail = '';
        foreach ($vlgInfo as $key => $info) {
            $vlgInfoMail .= $key.' : '.$info.'\n      ';
        }
        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem(
            'Tableau de bord',
            $this->get("router")->generate("admin_homepage")
        );
        $breadcrumbs->addItem(
            "Liste des produits",
            $this->get("router")->generate("admin_product_list")
        );
        $breadcrumbs->addItem("Fiche produit");

        $stock = $this->get('session')->get('current_admin_stock');
        $stock = $stockManager->findOneBy(['id' => $stock->getId()]);

        // View
        return $this->render(
            '@Admin/Product/show.html.twig',
            array(
                'product'   => $product,
                'askLocale' => $askLocale,
                'stock'     => $stock,
                'vlgInfoMail' => rawurlencode(htmlspecialchars_decode($vlgInfoMail)),
                'subjectVlgInfoMail' => rawurlencode(htmlspecialchars_decode('Infos produit mh -> ref : '.$product->translate('fr')->getName().' - '.$product->translate('fr')->getEanPart())),
                'toVlgInfoMail' => 'hub142@veolog.fr',
            )
        );
    }

    /**
     * Delete one product
     * @Route("/delete/{id}", name="admin_product_delete")
     *
     * @param  Request  $request  Request
     * @param  integer  $id  Id of product
     */
    public function deleteAction(Request $request, ProductManager $productManager, $id)
    {
        if ($this->isGranted('ROLE_VEOLOG_ADMIN')) {
            throw $this->createAccessDeniedException("Veolog not access");
        }

        // Find it
        /** @var Product $product */
        $product = $productManager->findOneToEdit($id);
        if (! $product) {
            throw $this->createNotFoundException('Product not found');
        }

        $productManager->remove($product);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été supprimé"
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Export csv
     * @Route("/export/{locale}", name="admin_product_export")
     *
     * @param  Request  $request
     */
    public function exportAction(Request $request, ProductManager $productManager, $locale)
    {
        $data    = $this->initSearch($request);
        $query   = $productManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Sku',
            'Nom',
            'Description',
            'EAN Bte/Pce',
            'Conditionnement de l\'unité',
            'Catégories',
            'Longueur unité',
            'Largeur unité',
            'Hauteur unité',
            'Poids unité',
            'EAN carton/caisse',
            'Nombre unité par carton',
            'Longueur carton',
            'Largeur carton',
            'Hauteur carton',
            'Poids carton',
            'Information',
            'CRD',
            'Iso2 Pays',
            'Litrage',
            'Effervescent',
            'REGION / APPELATION',
            'MILLESIME',
            'Couleur du vin',
            'Catégorie du vin',
            '% d\'alcool',
            'Produits consommable',
            'Nomenclature Douanière',
            'Media',
            'Marque',
            'Valeur Comptable',
            'Etat',
            'Origine',
            'Date de création',
            'Image (oui ou non)',
        ], ';');

        $locale = $request->get('locale', $request->getDefaultLocale());
        foreach ($results as $result) {
            $categories = [];
            foreach ($result->translate($locale)->getCategories() as $category) {
                $categories[] = $category->translate($locale)->getName();
            }
            $categories = implode($categories, ', ');
            $fileRow = [
                $result->getId(),
                $result->translate($locale)->getCode(),
                $result->translate($locale)->getName(),
                $result->translate($locale)->getDescription(),
                $result->translate($locale)->getEanPart(),
                $result->translate($locale)->getUnitConditioning(),
                $result->translate($locale)->getCategoriesString(),
                $result->translate($locale)->getDepth(),
                $result->translate($locale)->getWidth(),
                $result->translate($locale)->getHeight(),
                $result->translate($locale)->getWeight(),
                $result->translate($locale)->getEanBox(),
                $result->translate($locale)->getTotalUnitsByBox(),
                $result->translate($locale)->getDepthBox(),
                $result->translate($locale)->getWidthBox(),
                $result->translate($locale)->getHeightBox(),
                $result->translate($locale)->getWeightBox(),
                $result->translate($locale)->getOtherInformation(),
                $result->translate($locale)->isCrd() ? 'Oui' : 'Non',
                $result->translate($locale)->getCountry(),
                $result->translate($locale)->getLitrageBottle(),
                $result->translate($locale)->isEffervescent() ? 'Oui' : 'Non',
                $result->translate($locale)->getDesignationBottle(),
                $result->translate($locale)->getVintage(),
                $result->translate($locale)->getWineColor(), // getColor()
                $result->translate($locale)->getWineCategory(),
                $result->translate($locale)->getDegreeAlcohol(),
                $result->translate($locale)->isExpendable() ? 'Oui' : 'Non',
                $result->translate($locale)->getCustomsNomenclature(),
                '',
                $result->translate($locale)->getBrand() ? $result->translate($locale)->getBrand()->getName() : '',
                $result->translate($locale)->getValorization(),
                $result->translate($locale)->getState(),
                $result->translate($locale)->getOrigin() == 0 ? "SAP" : "NON SAP",
                $result->translate($locale)->getCreated()
                    ? $result->translate($locale)->getCreated()->format('d/m/Y')
                    : $result->getCreated()->format('d/m/Y'),
                $result->translate($locale)->getCover() ? 'OUI' : 'NON',
            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type'        => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'
                                    . date('Y-m-d')
                                    . '_'
                                    . $locale . '_products_export.csv"'
        ));
    }

    /**
     * Manage brand of product
     */
    protected function brandOtherManage(Product $product, BrandManager $brandManager)
    {
        $brandOther = $product->translate('fr')->getBrandOther();
        $brandOther = trim($brandOther);

        if (empty($brandOther)) {
            return $product;
        }

        $brand = $brandManager->findOneByName($brandOther);
        if ($brand) {
            $product->translate('fr')->setBrand($brand);

            return $product;
        }

        $brand       = new Brand();
        $translation = new BrandTranslation();
        $translation->setLocale('fr');
        $translation->setName($brandOther);
        $brand->addTranslation($translation);

        $brand = $brandManager->save($brand);
        $product->translate('fr')->setBrand($brand);

        return $product;
    }
}
