<?php

/**
 * Repository
 *
 * @author Pierre Baumes <pierre.b@disko.fr>
 */

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class ProductTranslationRepository
 *
 * @package App\Repository
 */
class ProductTranslationRepository extends BaseRepository
{
}
