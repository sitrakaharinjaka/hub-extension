<?php

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;

use App\Entity\Localisation;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class LocalisationManager
 * `
 * Object manager of localisation
 *
 * @package Disko\PageBundle\Managers
 */
class LocalisationManager extends BaseManager
{
    /**
     * @var LocalisationRepository
     */
    protected $localisationRepository;

    /**
     * LocalisationManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager = null)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('localisationRepository', Localisation::class);
    }


    /**
     * Save a localisation
     *
     * @param Localisation $localisation
     */
    public function save(Localisation $localisation)
    {
        $this->getEntityManager()->persist($localisation);
        $this->getEntityManager()->flush();

        return $localisation;
    }
    /**
     * @param Localisation $localisation
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(Localisation $localisation)
    {
        $this->getEntityManager()->remove($localisation);
        $this->getEntityManager()->flush();
    }

    /**
     * Get all localisation
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->localisationRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->localisationRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->localisationRepository->findOneBy($filters);
    }
}
