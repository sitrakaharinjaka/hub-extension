<?php
/**
 * Created by PhpStorm.
 * User: jerphagnon
 * Date: 25/06/2018
 * Time: 09:47
 */

namespace App\Event;

use App\Utils\Managers\Fail2BanManager;
use App\Entity\Fail2Ban;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;

class PreFail2Ban implements EventSubscriberInterface
{
    private $fail2BanManager;

    public function __construct(Fail2BanManager $fail2BanManager, RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->fail2BanManager = $fail2BanManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure',
            AuthenticationEvents::AUTHENTICATION_SUCCESS => 'onAuthenticationSuccess',
        ];
    }

    public function onAuthenticationFailure(AuthenticationFailureEvent $event)
    {
        if ($this->fail2BanManager->countBadLogBefore($this->request->getClientIp()) < PostFail2Ban::MAX_LOGIN_FAILURE_ATTEMPTS) {
            $fail2ban = new Fail2Ban();
            $fail2ban->setUsername($event->getAuthenticationToken()->getUsername());
            $fail2ban->setIp($this->request->getClientIp());
            $fail2ban->setRoute($this->request->headers->get('referer'));
            $fail2ban->setSuccess(false);
            $this->fail2BanManager->save($fail2ban);
        }
    }

    public function onAuthenticationSuccess(AuthenticationEvent $event)
    {
        if ($event->getAuthenticationToken()->getUsername() != "anon.") {
            $fail2ban = new Fail2Ban();
            $fail2ban->setUsername($event->getAuthenticationToken()->getUsername());
            $fail2ban->setIp($this->request->getClientIp());
            $fail2ban->setRoute($this->request->headers->get('referer'));
            $fail2ban->setSuccess(true);
            $this->fail2BanManager->save($fail2ban);
        }
    }
}
