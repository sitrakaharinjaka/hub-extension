<?php

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class LocaleRepository
 *
 * @package App\Repository
 */
class LocaleRepository extends BaseRepository
{

    /**
     * Get all user query, using for pagination
     *
     * @param array $filtersfindAllSlug
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('l')
            ->select('l')
            ->orderBy('l.code', 'asc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $qb->andWhere('l.'.$key.' LIKE :'.$key);
                $qb->setParameter($key, '%'.$filter.'%');
            }
        }

        return $qb->getQuery();
    }

    /**
     * Find one for edit profile
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('l')
            ->select('l')
            ->where('l.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * Find all list
     *
     * @return mixed
     */
    public function findAllList()
    {
        $qb = $this->createQueryBuilder('l')
            ->select('l')
            ->orderBy('l.code', 'ASC');

        $query = $qb->getQuery();

        $query->useResultCache(true, 600, 'LocaleRepository::findAllList');

        return $query->getResult();
    }

    public function findAllSlug()
    {
        return $this->createQueryBuilder('l')
                   ->select('l.id, l.slug')
                   ->orderBy('l.code', 'ASC')
                   ->getQuery()
                   ->getResult('ListHydrator');
    }

    public function findDefaultSlug()
    {
        return $this->createQueryBuilder('l')
            ->select('l.id, l.slug')
            ->orderBy('l.code', 'ASC')
            ->where('l.default = TRUE')
            ->getQuery()
            ->getResult('ListHydrator');
    }
}
