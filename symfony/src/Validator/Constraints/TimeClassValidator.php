<?php

namespace App\Validator\Constraints;

use App\Entity\ProductVariant;
use App\Entity\Project;
use App\Entity\ProjectItem;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class TimeClassValidator extends ConstraintValidator
{
    /**
     * @param mixed      $value
     * @param Constraint $constraint
     *
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var Project $project */
        $project = $value;


        if (!$project->getNeedReturn()) {
            return;
        }

        if ($project->getDateStart()->getTimestamp() !== $project->getDateEnd()->getTimestamp()) {
            return;
        }


        if ($project->getDateStart() >= $project->getDateEnd()) {
            $this->context->buildViolation($constraint->message)->atPath('dateEnd')->addViolation();
        }
    }
}
