<?php

namespace App\Interfaces;

interface WorkflowInterface
{
    const WORKFLOW_DRAFT = "draft";
    const WORKFLOW_WAIT = "wait";
    const WORKFLOW_PUBLISH = "publish";
    const WORKFLOW_UNPUBLISH = "unpublish";

    public function setState($state);
    public function setStateAction($stateAction);
    public function setRef($ref);
    public function setRefBase($refBase);
    public function setMaster($master);

    public function getMaster();
    public function getState();
    public function getStateAction();
    public function getRef();
    public function getRefBase();
}
