<?php

namespace App\Command\Veolog\MyBoxes;

use App\Command\Veolog\AbstractVeologCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\MyBoxesBox;
use App\Entity\MyBoxesProduct;
use App\Entity\MyBoxesCommand;
use App\Entity\Product;
use App\Entity\Project;
use App\Utils\Managers\UserManager;
use App\Utils\Managers\MyBoxesBoxManager;
use App\Utils\Managers\MyBoxesCommandManager;
use App\Utils\Managers\MyBoxesProductManager;
use App\Utils\Managers\MyBoxesItemManager;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\ServiceManager;
use App\Utils\Managers\ProjectItemManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Input\InputOption;

class SynchOrderAskCommand extends AbstractVeologCommand
{

  /** @var  EntityManager */
    private $productManager;
    private $boxManager;
    private $commandManager;
    private $itemManager;
    private $userManager;
    private $rootDir;

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, MyBoxesProductManager $productManager, MyBoxesItemManager $itemManager, UserManager $userManager, MyBoxesCommandManager $commandManager, MyBoxesBoxManager $boxManager, KernelInterface $kernel)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->productManager = $productManager;
        $this->boxManager = $boxManager;
        $this->commandManager = $commandManager;
        $this->itemManager = $itemManager;
        $this->userManager = $userManager;
        $this->rootDir = $kernel->getProjectDir();
    }

    protected static $defaultName = 'veolog:myboxes:order-ask';

    protected function configure()
    {
        $this
            ->setDescription('Synch veolog order ask')
            ->addOption(
                'vlgenv',
                null,
                InputOption::VALUE_OPTIONAL,
                'Env veolog prod?',
                false
            )
            ->addOption(
                'test',
                null,
                InputOption::VALUE_OPTIONAL,
                'Generate data for test',
                false
            )
            ->setHelp('This command allows you to synch veolog order ask evry 5minutes');
    }

    protected function executeCommand()
    {
        if ($this->ENVIRONNEMENT == 'DEVELOPEMENT') {
            $plus = '_DEV';
        } elseif ($this->ENVIRONNEMENT == 'RECETTE') {
            $plus = '_REC';
        } elseif ($this->ENVIRONNEMENT == 'PREPROD') {
            $plus = '_PP';
        } else {
            $plus = '';
        }
        ################################################################################
        ##################### COMMAND EMPTY BOXES ONLY (red, blue, yellow)
        ################################################################################
        $this->log('info', "=============================================================");
        $this->log('info', "== TRAITEMENT DES COMMANDES DE BOX VIDES ====================");
        $this->log('info', "=============================================================");
        $commands = $this->commandManager->findbyStateNoSendingToVlg(MyBoxesBox::STATE_CMD_USER_PREPARED, 'product');
        $this->createFileFTP($commands, ['filesuffix'=>'CMDPRODUCTS', 'idprefix'=>'PRD'.$plus, 'type' => 'PRODUIT']);


        ################################################################################
        ##################### COMMAND NOT EMPTY BOX
        ################################################################################
        $this->log('info', "=============================================================");
        $this->log('info', "== TRAITEMENT DES COMMANDES DE BOX PLEINES ==================");
        $this->log('info', "=============================================================");
        $commands = $this->commandManager->findbyStateNoSendingToVlg(MyBoxesBox::STATE_CMD_USER_PREPARED, 'box');
        $this->createFileFTP($commands, ['filesuffix'=>'CMDBOXES', 'idprefix'=>'BOX'.$plus, 'type' => 'MYBOX']);
    }

    private function createIdToVelogCode($id, $options)
    {
        $code = '00000'.$id;
        $code = substr($code, strlen($code)-5);
        $code = $options['idprefix'].'_'.$code;
        $code = strtoupper($code);
        return $code;
    }

    private function createFilename($command, $options)
    {
        $currentDate = date("ymdhis");
        return 'orders_BOX_' . $currentDate . '_' . $command->getId() . '_'.$options['filesuffix'].'.csv';
    }

    private function getFullFilename($dirVlg, $filename)
    {
        return $this->rootDir . "/veolog/".$dirVlg."/" . $filename;
    }

    private function createLineE($command, $options)
    {
        $service = [];
        $listService = implode(",", array_merge($service)).',';
        $array_command['type_ligne'] = 'E';
        $array_command['entite_de_stocks'] = "MYBOXES";
        $array_command['ref_commande_donneur_ordre'] = $this->createIdToVelogCode($command->getId(), $options);
        $array_command['ref_commande_mh_livre'] = "";
        $array_command['destinataire_livraison'] = strtoupper(substr($command->getFirstname().' '.$command->getLastname(), 0, 27));

        if ($command->getAdressType() == MyBoxesCommand::AD_ARMEE) {
            // ATTENTION : l'adresse de livraison peut être différente de l'adresse d'affichage au user !!
            $array_command['addr_livraison_1'] = "4 rue Pargolèse";
            $array_command['addr_livraison_2'] = "";
            $array_command['addr_livraison_3'] = '';
            $array_command['cp_livraison'] = "75116";
            $array_command['ville_livraison'] = "Paris";
            $array_command['pays_livraison'] = "FR";
        } else {
            // ATTENTION : l'adresse de livraison est différente de l'adresse d'affichage au user !!
            $array_command['addr_livraison_1'] = "115 rue du bac";
            $array_command['addr_livraison_2'] = "";
            $array_command['addr_livraison_3'] = '';
            $array_command['cp_livraison'] = "75007";
            $array_command['ville_livraison'] = "Paris";
            $array_command['pays_livraison'] = "FR";
        }

        $phone = ""; // Pour le moment pas de téléphone dans l'interface.
        $phone = strtoupper(trim(substr($phone, 0, 18)));
        $phone = str_replace(" ", "", $phone);
        $phone = str_replace(".", "", $phone);
        $array_command['tel_livraison'] = $phone;

        $array_command['email_livraison'] = strtoupper($command->getUser()->getEmail());
        $array_command['date_livraison'] = "";
        $array_command['heure_livraison'] = "";
        $array_command['transporteur'] = 3;

        $RecipientInformation = $command->getStage().' '.$command->getOfficeNumber();
        $RecipientInformation = str_replace("\n", "", $RecipientInformation);
        $RecipientInformation = str_replace("\r", "", $RecipientInformation);
        $RecipientInformation = str_replace("\t", "", $RecipientInformation);
        $array_command['commentaire_long'] = strtoupper($RecipientInformation);

        $array_command['presta_services'] = "";
        $array_command['retour'] = "";
        $array_command['urgence'] = '';


        $array_command['centre_facturation'] = "103";
        $array_command['date_retour'] = "";
        $array_command['heure_retour'] = "";

        $command_to_csv = str_replace('"', '', $array_command);

        return $command_to_csv;
    }

    private function createLineF($count_num_product, $qty_product)
    {
        return array('F', $count_num_product, $qty_product);
    }

    public function createFileFTP($commands, $options)
    {
        $dirVlg = $this->getDirVlgIN();

        $this->log('info', '    Commands <comment>'.$options['filesuffix'].'</comment> to synch into veolog ');

        $total = count($commands);

        if ($total > 0) {
            $this->log('info', '    <comment>' . $total . ' commands</comment> ready for synch');

            try {
                $ftp_handle = $this->connectFTP();
                $this->log('info', '    Connect FTP : <info>success</info>');

                foreach ($commands as $key => $command) {
                    $filename = $this->createFilename($command, $options);
                    $fullfilename = $this->getFullFilename($dirVlg, $filename);

                    $this->log('info', '    Creating file : ' . $fullfilename);

                    $fp = fopen($fullfilename, "w");

                    $this->log('info', '        Creating line E');
                    fputcsv($fp, $this->createLineE($command, $options), ';');

                    $count_num_product = 0;
                    $qty_product = 0;
                    if ($options['type'] == 'PRODUIT') {
                        $items = $command->getItems();
                        $array_items = [];
                        $count_num_product = 0;
                        foreach ($items as $keyItem => $item) {
                            $filter['id'] = $item->getProduct()->getId();
                            $product = $this->productManager->findOneBy($filter);
                            if (!empty($product) && $item->getQuantity() > 0) {
                                $count_num_product++;
                                $array_items[$keyItem]['type_ligne'] = 'L';
                                $array_items[$keyItem]['num_ligne'] = strtoupper($keyItem+1);
                                $array_items[$keyItem]['code_article'] = strtoupper($product->getSku());
                                $array_items[$keyItem]['label_article'] = strtoupper($product->getName());
                                $array_items[$keyItem]['qte'] = $item->getQuantity();
                                $qty_product += $item->getQuantity();
                                $array_items[$keyItem]['code_etat'] = '';
                                $this->log('info', '        Creating line L');
                                fputcsv($fp, $array_items[$keyItem], ';');
                            }
                        }
                    } elseif ($options['type'] == 'MYBOX') {
                        $box = $command->getBox();
                        if (!empty($box)) {
                            $array_box = [];
                            $array_box['type_ligne'] = 'L';
                            $array_box['num_ligne'] = 1;
                            $array_box['code_article'] = $box->getBarcode();
                            $array_box['label_article'] = $box->getName();
                            $array_box['qte'] = 1;
                            $array_box['code_etat'] = '';
                            $qty_product += 1;
                            $count_num_product += 1;
                            $this->log('info', '        Creating line L');
                            fputcsv($fp, $array_box, ';');
                        }
                    }

                    $this->log('info', '        Creating line F');
                    fputcsv($fp, $this->createLineF($count_num_product, $qty_product), ';');

                    $this->log('info', '    Creating file : ' . $fullfilename . ' <info>success</info>');

                    $this->log('info', '    Sending file : ' . $filename);

                    if (ftp_put($ftp_handle, "/".$dirVlg."/" . $filename, $fullfilename, FTP_ASCII)) {
                        $this->log('info', '    Sent file : ' . $filename . ' <info>success</info>');

                        $this->log('info', '    Updating commands : ' . $command->getId());
                        //add date of adding command to veolog
                        $command = $this->commandManager->findOneBy(['id' => $command->getId()]);
                        $command->setState(MyBoxesBox::STATE_CMD_USER_ASKED);
                        $command->setDateAddVlg(new \DateTime('@'.strtotime('now')));
                        $this->commandManager->save($command);
                        $this->log('info', '    Updated commands : ' . $command->getId() . ' <info>success</info>');
                    } else {
                        $this->log('info', '    Sent file : ' . $filename . ' <error>error</error>');
                    }
                }

                ftp_close($ftp_handle);
            } catch (Exception $e) {
                $this->log('info', '    <error>Failure</error> : ' . $e->getMessage());
            }
        } else {
            $this->log('info', '    No command find to synch');
        }
    }
}
