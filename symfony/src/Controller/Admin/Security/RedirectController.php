<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin\Security;

use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RedirectController
 *
 * @package Disko\FrontBundle\Controller
 */
class RedirectController extends BaseController
{

    /**
     * Admin view content
     *
     * @Route("/admin", name="admin_redirect")
     * @return Response
     */
    public function index()
    {
        return $this->redirectToRoute('admin_homepage');
    }
}
