<?php

namespace App\DataFixtures;

use App\Entity\BillingCenter;
use App\Entity\BillingCenterTranslation;
use App\Utils\Managers\BillingCenterManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * @package Disko\UserBundle\DataFixtures\ORM
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class LoadBillingCenterData extends Fixture implements DependentFixtureInterface
{
    private $billingCenterManager;
    private $localeManager;

    public function __construct(BillingCenterManager $billingCenterManager, LocaleManager $localeManager)
    {
        $this->billingCenterManager = $billingCenterManager;
        $this->localeManager = $localeManager;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
//        $billingCenters = array(
//      [
//        'name' => 'Centre de coût 1',
//        'company' => 'Société 1',
//        'zipCode' => '75000',
//        'city' => 'Paris'
//      ],
//      [
//        'name' => 'Centre de coût 2',
//        'company' => 'Société 2',
//        'zipCode' => '34000',
//        'city' => 'Montpellier'
//      ]
//    );
//
//        $locales = $this->localeManager->findAll();
//
//        foreach ($billingCenters as $billingCenter) {
//            $obj = new BillingCenter();
//            foreach ($locales as $locale) {
//                $trans = new BillingCenterTranslation();
//                $trans->setLocale($locale->getSlug());
//                $trans->setName($billingCenter['name']);
//                $trans->setCompany($billingCenter['company']);
//                $trans->setZipCode($billingCenter['zipCode']);
//                $trans->setCity($billingCenter['city']);
//                $obj->addTranslation($trans);
//            }
//            $this->billingCenterManager->save($obj);
//            $this->addReference('admin-billing-center-'.$obj->getId(), $obj);
//        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

    public function getDependencies()
    {
        return [
      LoadLocaleData::class
    ];
    }
}
