<?php


namespace App\Controller\Admin;


use App\Controller\BaseController;
use App\Entity\StockEntry115;
use App\Form\Model\SearchStockEntry;
use App\Form\Type\StockEntry115\SearchStockEntryType;
use App\Utils\Managers\StockEntry115ItemManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\Pdf;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class StockEntryItem115Controller
 * @package App\Controller\Admin
 * @Route("/admin/stock-entry-item-115", name="admin_stock_entry_item_115_")
 */
class StockEntryItem115Controller extends BaseController
{
    /**
     * Stock entry list
     * @Route("/list/{askLocale}", name="list", defaults={"askLocale": "fr"})
     */
    public function list(
        Breadcrumbs $breadcrumbs,
        PaginatorInterface $paginator,
        Request $request,
        StockEntry115ItemManager $manager,
        $askLocale = 'fr'
    ): Response {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchStockEntryType::class, $data, [
            'method' => 'get'
        ]);

        // Breadcrumb
        $breadcrumbs->addItem('Tableau de bord', $this->generateUrl('admin_homepage'));
        $breadcrumbs->addItem('Liste des entrées du stock');

        // Generate query
        $filters = $data->getSearchData();

        $pagination = $paginator->paginate(
            $manager->queryForSearch($askLocale, $filters),
            $request->query->get('page', 1),
            20
        );

        return $this->render('admin/StockEntry115/index.html.twig', [
            'askLocale' => $askLocale,
            'pagination' => $pagination,
            'form' => $form->createView(),
            'is115' => true
        ]);
    }

    /**
     * Generate Stock Packing Slip PDF
     *
     * @Route(
     *     "/stock-entry-packing-slip-pdf/{id}",
     *     options={"expose"=true},
     *     name="stock_entry_packing_slip_pdf"
     * )
     */
    public function generatePackingSlipPdf(Pdf $pdf, KernelInterface $kernel, StockEntry115 $stockEntry): PdfResponse
    {
        $logo = 'file:///' . $kernel->getProjectDir() . '/public/assets/myboxes/images/logo@2x.png';

        $html = $this->renderView('hub/simple/bordereau_envoi_entree_stock_115_pdf.html.twig', [
            'stockEntry' => $stockEntry,
            'logo' => $logo
        ]);

        return new PdfResponse($pdf->getOutputFromHtml($html), 'packing_slip.pdf', [
            'lowquality' => false,
            'images'     => true
        ]);
    }

    /**
     * Init Search Stock Entry
     */
    protected function initSearch(Request $request): SearchStockEntry
    {
        // Filters get
        $filters = $request->query->get('search', []);

        // Init form
        $data = new SearchStockEntry();
        $data->setId(isset($filters['id']) ? is_int($filters['id']) ? $filters['id'] : null : null)
            ->setStock(isset($filters['stock']) ? strtolower($filters['stock']) : '')
            ->setRequester(isset($filters['requester']) ? strtolower($filters['requester']) : '')
            ->setCode(isset($filters['code']) ? strtolower($filters['code']) : '')
            ->setCreatedAt(isset($filters['createdAt']) && !empty($filters['createdAt']) ?  date_create_from_format("d/m/Y", $filters['createdAt']) : null)
            ->setDelivery(isset($filters['delivery']) && !empty($filters['delivery']) ?  date_create_from_format("d/m/Y H:i:s", $filters['delivery'] . ' 00:00:00') : null)
            ->setStatus(isset($filters['status']) ? strtolower($filters['status']) : '')
        ;

        return $data;
    }

    /**
     * Delete Stock Entry 115
     * @Route("/{id}/", methods={"DELETE"}, name="delete_stock_entry_115")
     */
    public function deleteStockEntry(EntityManagerInterface $em, Request $request, StockEntry115 $stockEntry115): Response
    {
        if (
        $this->isCsrfTokenValid(
            'delete_stock_entry_115_' . $stockEntry115->getId(),
            (string) $request->request->get('_token')
        )
        ) {
            $id = $stockEntry115->getId();
            $em->remove($stockEntry115);
            $em->flush();

            $this->addFlash('success', "L'entrée de stock 115 n°$id a été supprimée avec succès");
        }

        return $this->redirectToRoute('admin_stock_entry_item_115_list', [
            'askLocale' => $request->request->get('askLocale')
        ]);
    }
}
