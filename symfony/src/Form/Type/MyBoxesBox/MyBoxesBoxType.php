<?php

namespace App\Form\Type\MyBoxesBox;

use App\Entity\MyBoxesBox;
use App\Entity\MyBoxesBoxUser;
use App\Entity\User;
use App\Form\Type\MyBoxes\BoxUserType;
use App\Utils\Managers\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MyBoxesBoxType extends AbstractType
{

    protected $userManager;

    /**
     * @param LocaleManager $localeManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => "Nom de votre box",
            'required' => true
        ]);

        $builder->add('barcode', TextType::class, [
            'required' => true
        ]);


        $builder->add('content', TextareaType::class, [
            'label' => "Contenu de votre box",
            'required' => true,
        ]);

        $builder->add('seal', TextType::class, [
            'label' => "Numéro de scellé",
            'required' => true,
        ]);

        $builder->add('boxUsers', EntityType::class, [
            'class' => MyBoxesBoxUser::class,
            'placeholder' => 'Sélectionner un utilisateur',
            'multiple' => true,
            'required'  => false,
            'choice_label' => 'user.email',
            'choice_value' => 'user.id'
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MyBoxesBox::class,
            'validation_groups' => 'Edit',
        ]);
    }
}
