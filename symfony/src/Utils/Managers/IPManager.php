<?php

/**
 * Manager
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;
use App\Entity\IP;
use App\Repository\IPRepository;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class IPManager
 * `
 * Object manager of user
 */
class IPManager extends BaseManager
{
    /**
    * @var IPRepository
    */
    protected $ipRepository;

    /**
     * BaseManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager = null)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('ipRepository', IP::class);
    }

    /**
     * Save a ip
     *
     * @param IP $ip
     */
    public function save(IP $ip)
    {
        $ip->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($ip);
        $this->getEntityManager()->flush();

        return $ip;
    }

    /**
     * Remove one ip
     *
     * @param IP $ip
     */
    public function remove(IP $ip)
    {
        $this->getEntityManager()->remove($ip);
        $this->getEntityManager()->flush();
    }

    /**
     * Get all ip
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->ipRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->ipRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->ipRepository->findOneBy($filters);
    }

    /**
     * Have access by ip restrict
     *
     * @param array $filters
     * @return mixed
     */
    public function haveAccess($clientIp)
    {
        return $this->ipRepository->haveAccess($clientIp);
    }

    /**
     * Off all
     *
     * @return mixed
     */
    public function offAll()
    {
        return $this->ipRepository->offAll();
    }

    /**
     * On all
     *
     * @return mixed
     */
    public function onAll()
    {
        return $this->ipRepository->onAll();
    }
}
