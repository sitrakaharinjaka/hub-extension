<?php

namespace App\Command;

use App\Command\AbstractCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\Project;
use App\Utils\Managers\ProjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

// use Symfony\Component\EventDispatcher\EventDispatcher;

class ProjectReturnCommand extends AbstractCommand
{
    protected static $defaultName = 'disko:project:return';

    private $projectManager;

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, ProjectManager $projectManager)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->projectManager = $projectManager;
    }


    protected function configure()
    {
        $this
            ->setDescription('Send mail for project with return (and email return set)')
        ;
    }

    /**
     * @return int|void|null
     * @throws \Exception
     */
    protected function executeCommand()
    {
        $this->log('info', "<info>Searching</info> list of project to warn up ...");

        $projects = $this->projectManager->findProjectsWithReturnToWarn();
        $count = count($projects);
        $this->log('info', "Found <info>" . $count . "</info> project(s)");

        foreach ($projects as $project) {
            $email = $project->getEmailReturn();
            $dateReturn = $project->getDateReturn()->format('Y/m/d');
            $name = $project->getName();

            $this->log('info', " <info>Sending</info> email to " . $email . " for project " . $name . " with return date on ". $dateReturn . " ... ");

            if ($this->mailer->sendWarnNearProjectDateReturnEmailMessage($project)) {
                $this->log('info', " <info>Sending</info> email to " . $email . " for project " . $name . " with return date on ". $dateReturn . " <info>done</info>");
            } else {
                $this->log('info', " <info>Sending</info> email to " . $email . " for project " . $name . " with return date on ". $dateReturn . " <error>error</error>");
            }

            $project->setReturnMail(1);
            $this->projectManager->save($project);
        }
        $this->log('info', "<info>END</info>");
    }
}
