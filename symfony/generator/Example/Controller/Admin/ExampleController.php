<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\Example;
use App\Entity\ExampleTranslation;
use App\Form\Model\SearchExample;
use App\Form\Type\Example\ExampleTranslationType;
use App\Form\Type\Example\SearchExampleType;
use App\Form\Type\Example\ExampleType;
use App\Utils\Managers\ExampleManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ExampleController
 * @Route("/admin/examples")
 * @package Disko\AdminBundle\Controller
 */
class ExampleController extends BaseController
{
    /**
     * Example's list
     * @Route("/list/{askLocale}", name="admin_example_list", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listAction(Request $request, ExampleManager $exampleManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchExampleType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des exemples');

        // Generate query
        $query = $exampleManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/Example/index.html.twig', array(
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchExample();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setName((isset($filters['name']))   ? $filters['name'] : '');

        return $data;
    }

    /**
     * Create a example
     * @Route("/create", name="admin_example_create")
     * @param Request $request Request
     *
     * @return Response
     */
    public function createAction(Request $request, ExampleManager $exampleManager, LocaleManager $localeManager)
    {
        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");

        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des exemples", $this->get("router")->generate("admin_example_list"));
        $breadcrumbs->addItem("Création");


        // Init form
        $example = new Example();
        $form = $this->createForm(ExampleType::class, $example, [
            'validation_groups' => ['Default']
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($example->getTranslations()) > 0) {
                    $exampleManager->save($example);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Création terminée"
                    );

                    return $this->redirect($this->generateUrl('admin_example_edit', array(
                        'id' => $example->getId()
                    )));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            } else {

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'error',
                    "Le formulaire contient des erreurs ou est vide"
                );
            }
        }

        // View
        return $this->render(
            '@Admin/Example/create.html.twig',
            array(
                'form'           => $form->createView(),
                'example'           => $example,
            )
        );
    }

    /**
     * Update a example
     * @Route("/edit/{id}", name="admin_example_edit")
     * @param Request $request Request
     * @param integer $id      Id of example
     *
     * @return Response
     */
    public function editAction(Request $request, ExampleManager $exampleManager, LocaleManager $localeManager, $id)
    {
        // Find it
        /** @var Example $example */
        $example = $exampleManager->findOneToEdit($id);
        if (!$example) {
            throw $this->createNotFoundException('Example not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des exemples", $this->get("router")->generate("admin_example_list"));
        $breadcrumbs->addItem("Modification");

        $form = $this->createForm(ExampleType::class, $example, [
            'validation_groups' => ['Default']
        ]);


        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($example->getTranslations()) > 0) {
                    $exampleManager->save($example);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Mise à jour effectuée"
                    );

                    return $this->redirect($request->headers->get('referer'));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/Example/edit.html.twig',
            array(
                'form'           => $form->createView(),
                'example'           => $example,
            )
        );
    }

    /**
     * Delete one example
     * @Route("/delete/{id}", name="admin_example_delete")
     * @param Request $request Request
     * @param integer $id      Id of example
     */
    public function deleteAction(Request $request, ExampleManager $exampleManager, $id)
    {
        // Find it
        /** @var Example $example */
        $example = $exampleManager->findOneToEdit($id);
        if (!$example) {
            throw $this->createNotFoundException('Example not found');
        }

        $exampleManager->remove($example);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été supprimé"
        );

        return $this->redirect($request->headers->get('referer'));
    }



    /**
     * Duplicate one example
     * @Route("/duplicate/{id}", name="admin_example_duplicate")
     * @param Request $request Request
     * @param integer $id      Id of example
     */
    public function duplicateAction(Request $request, ExampleManager $exampleManager, $id)
    {
        // Find it
        /** @var Example $example */
        $example = $exampleManager->findOneToEdit($id);
        if (!$example) {
            throw $this->createNotFoundException('Example not found');
        }

        $this->getDoctrine()->getManager()->detach($example);
        foreach ($example->getTranslations() as $translation) {
            $translation->setId(null);
            $translation->setSlug(null);
            $this->getDoctrine()->getManager()->detach($translation);
        }
        $exampleManager->save($example);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été dupliqué"
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Export csv
     * @Route("/export/{locale}", name="admin_example_export")
     * @param Request $request
     */
    public function exportAction(Request $request, ExampleManager $exampleManager, $locale)
    {
        $data = $this->initSearch($request);
        $query = $exampleManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Nom',
            'Date de création',
            /**ACTIVE**/'En ligne'/**ACTIVE**/
        ], ';');

        $locale = $request->get('locale', $request->getDefaultLocale());
        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->translate($locale)->getName(),
                ($result->translate($locale)->getCreated()) ? $result->translate($locale)->getCreated()->format('d/m/Y') : $result->getCreated()->format('d/m/Y'),
                /**ACTIVE**/$result->translate($locale)->getActive()/**ACTIVE**/
            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_'.$locale.'_examples_export.csv"'
        ));
    }
}
