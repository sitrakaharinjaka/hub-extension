<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DeliveryRepository")
 * @ORM\Table(name="dk_delivery")
 * @ORM\HasLifecycleCallbacks
 */
class Delivery
{
    use GedmoTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Country
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $country;

    /**
     * Zip
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $zip;

    /**
     * Deliver
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $deliver;

    /**
     * Type project
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $type = Project::TYPE_BASIC;

    /**
     * Hour delay min
     *
     * @ORM\Column(type="integer",nullable=true)
     */
    protected $hour = 24;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return mixed
     */
    public function getHour()
    {
        return $this->hour;
    }

    /**
     * @param mixed $hour
     */
    public function setHour($hour)
    {
        $this->hour = $hour;
    }

    /**
     * @return mixed
     */
    public function getDeliver()
    {
        return $this->deliver;
    }

    /**
     * @param mixed $deliver
     */
    public function setDeliver($deliver)
    {
        $this->deliver = $deliver;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}
