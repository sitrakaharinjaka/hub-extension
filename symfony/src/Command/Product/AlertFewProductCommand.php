<?php

namespace App\Command\Product;

use App\Entity\Project;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\StockManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Utils\Services\MailerService;

class AlertFewProductCommand extends Command
{
    protected static $defaultName = 'disko:product:alert-few';

    private $productManager;
    private $stockManager;
    private $mailerService;

    public function __construct(ProductManager $productManager, StockManager $stockManager, MailerService $mailerService)
    {
        parent::__construct();
        $this->productManager = $productManager;
        $this->stockManager   = $stockManager;
        $this->mailerService  = $mailerService;
    }


    protected function configure()
    {
        $this
            ->setDescription('Send mail for project with return (and email return set)')
            ->addOption('level', null, InputOption::VALUE_OPTIONAL, 'Nombre de produit limite pour alerte', 10);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write('Recherche et alerte sur quantité de produit faible');
        $stocks = $this->stockManager->findAll();
        $output->writeln('<comment>(x' . sizeof($stocks) . ' stocks)</comment>');
        foreach ($stocks as $stock) {
            $level = (int) $input->getOption('level');
            $fews = $this->productManager->findProductsToWarnFew($stock, Project::CAVE_PANTIN, $level);
            if (sizeof($fews) > 0) {
                $output->writeln('Stock : ' . $stock->translate('fr')->getName());
                $list = [];
                foreach ($fews as $item) {
                    $product = $item[0]->translate('fr');
                    $total = $item['total'];
                    $output->writeln('----> Produit niveau bas : ' . $total . " - " . $product->getName());
                    $list[]= [
                        'total' => $total,
                        'name'  => $product->getName(),
                        'code'  => $product->getCode(),
                    ];
                }
                if ($this->mailerService->sendWarnFewProducts($stock, $list)) {
                    $output->writeln('----> <info>Mail sent</info>');
                } else {
                    $output->writeln('----> <error>Mail NOT SENT</error>');
                }
            }
        }
        $output->writeln('<info>Terminé</info>');
    }
}
