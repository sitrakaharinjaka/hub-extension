<?php

/**
 * Model for search
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Model;

/**
 * Class SearchIP
 *
 * @package App\Form\Model
 */
class SearchIP
{
    /**
     * Id
     *
     * @var string
     */
    protected $id;

    /**
     * Name
     *
     * @var string
     */
    protected $name;

    /**
     * V4 IP
     *
     * @var string $v4
     */
    protected $v4;

    /**
     * V6 IP
     *
     * @var string $v6
     */
    protected $v6;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get $v4
     *
     * @return  string
     */
    public function getV4()
    {
        return $this->v4;
    }

    /**
     * Set $v4
     *
     * @param  string  $v4  $v4
     *
     * @return  self
     */
    public function setV4(string $v4)
    {
        $this->v4 = $v4;

        return $this;
    }

    /**
     * Get $v6
     *
     * @return  string
     */
    public function getV6()
    {
        return $this->v6;
    }

    /**
     * Set $v6
     *
     * @param  string  $v6  $v6
     *
     * @return  self
     */
    public function setV6(string $v6)
    {
        $this->v6 = $v6;

        return $this;
    }

    /**
     * Get search data
     *
     * @return array
     */
    public function getSearchData()
    {
        $tab = array();

        if (!empty($this->id)) {
            $tab['id'] = $this->id;
        }
        if (!empty($this->name)) {
            $tab['name'] = $this->name;
        }
        if (!empty($this->v4)) {
            $tab['v4'] = $this->v4;
        }
        if (!empty($this->v6)) {
            $tab['v6'] = $this->v6;
        }

        return $tab;
    }
}
