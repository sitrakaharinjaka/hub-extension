<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MyBoxesBoxUserConstraint extends Constraint
{
    public $message = 'Email inconnu';

    public function validatedBy()
    {
        return 'myboxes_boxuser_constraint';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
