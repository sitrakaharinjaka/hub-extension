<?php

namespace App\Utils\Managers;

use App\Entity\Contact;
use App\Repository\ContactRepository;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class ContactManager
 * `
 * Object manager of contact
 *
 * @package Disko\PageBundle\Managers
 */
class ContactManager extends BaseManager
{

    /**
     * Repository
     *
     * @var ContactRepository
     */
    protected $contactRepository;

    /**
     * ContactManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('contactRepository', Contact::class);
    }

    /**
     * Save a contact
     *
     * @param Contact $contact
     */
    public function save(Contact $contact)
    {
        $contact->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($contact);
        $this->getEntityManager()->flush();

        return $contact;
    }

    /**
     * Remove one contact
     *
     * @param Contact $contact
     */
    public function remove(Contact $contact)
    {
        $this->getEntityManager()->remove($contact);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one Active
     *
     * @param $slugWl
     * @param $locale
     * @return mixed
     */
    public function findOneActive($slugWl, $locale)
    {
        $contact = $this->contactRepository->findOneActive($slugWl, $locale);

        return $contact;
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $contact = $this->contactRepository->findOneToEdit($id);

        return $contact;
    }

    /**
     * Get all contact
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->contactRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->contactRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->contactRepository->findOneBy($filters);
    }
}
