<?php

namespace App\Command\Import;

use App\Entity\MyBoxesBarcode;
use App\Utils\Managers\MyBoxesBarcodeManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ImportMyBoxesBarcodeCommand extends Command
{


    /**
     * @var string
     */
    protected static $defaultName = 'disko:insert:my-boxes-bar-code';


    /**
     * @var MyBoxesBarcodeManager
     */
    protected $myBoxesBarcodeManager;

    /**
     * ImportBarcodeCommand constructor.
     *
     * @param MyBoxesBarcodeManager $myBoxesBarcodeManager
     */
    public function __construct(MyBoxesBarcodeManager $myBoxesBarcodeManager)
    {
        parent::__construct();
        $this->myBoxesBarcodeManager = $myBoxesBarcodeManager;
    }

    private function createBarcode($code)
    {
        $barcode = new MyBoxesBarcode();
        $barcode->setCode($code);
        $this->myBoxesBarcodeManager->save($barcode);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $codes = [];

        for ($index = 1 ; $index < 1000 ; $index++) {
            $str = "0000000000000000" . $index;
            $str = substr($str, strlen($str)-4);
            $codes[] = "TEST-DISKO-" . $str;
        }
        $count = sizeof($codes);

        $output->writeln('Création de code barre de test');

        $progress = new ProgressBar($output, $count);
        $progress->start();

        foreach ($codes as $code) {
            $output->write(" > $code : insertion");
            $this->createBarcode($code);
            $progress->advance(1);
            $output->write(" > $code : inséré");
        }
        $progress->finish();
        $output->writeln("");

        $output->writeln("<info>Fin : $count code-barres insérés </info>");
    }
}
