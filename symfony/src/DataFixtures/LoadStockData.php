<?php

/**
 * Fixture
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\DataFixtures;

use App\Entity\Locale;
use App\Entity\StockTranslation;
use App\Utils\Managers\LocaleManager;
use App\Utils\Managers\StockManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\Stock;

/**
 * Class LoadStockData
 *
 * @package Disko\UserBundle\DataFixtures\ORM
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class LoadStockData extends Fixture implements DependentFixtureInterface
{
    private $stockManager;
    private $localeManager;

    public function __construct(StockManager $stockManager, LocaleManager $localeManager)
    {
        $this->stockManager = $stockManager;
        $this->localeManager = $localeManager;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
//        $stocks = array(
//            ['name' => 'Krug > Département A', 'code' => 'KDA'],
//            ['name' => 'Krug > Département B', 'code' => 'KDB'],
//            ['name' => 'Hennessy > Département A', 'code' => 'HDA'],
//            ['name' => 'Hennessy > Département B', 'code' => 'HDB'],
//            ['name' => 'Hennessy > Département C', 'code' => 'HDC'],
//            ['name' => 'Hennessy > Département D', 'code' => 'HDD'],
//            ['name' => 'Hennessy > Département E', 'code' => 'HDE'],
//            ['name' => 'Mercier > Département B', 'code' => 'MDB'],
//            ['name' => 'Mercier > Département C', 'code' => 'MDC'],
//        );
//
//        $locales = $this->localeManager->findAll();
//
//        foreach ($stocks as $stock) {
//            $obj = new Stock();
//            foreach ($locales as $locale) {
//                $trans = new StockTranslation();
//                $trans->setLocale($locale->getSlug());
//                $trans->setName($stock['name']);
//                $trans->setCode($stock['code']);
//                $obj->addTranslation($trans);
//            }
//            $this->stockManager->save($obj);
//            $this->addReference('admin-stock-'.$obj->getId(), $obj);
//        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

    public function getDependencies()
    {
        return [
          LoadLocaleData::class
      ];
    }
}
