<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Form\Model\SearchLogFile;
use App\Form\Type\LogFile\SearchLogFileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AboutController
 * @Route("/admin/about")
 * @package App\Controller
 */
class AboutController extends BaseController
{
    /**
     * Info about
     * @Route("/info", name="admin_about_info")
     * @param Request $request Request
     */
    public function info(Request $request)
    {
        $path =  $this->getParameter('kernel.root_dir');
        $content = file_get_contents($path.'/../composer.json');
        $json = json_decode($content, true);

        // View
        return $this->render(
            '@Admin/About/info.html.twig',
            array(
                'json' => $json['require'],
            )
        );
    }

    /**
     * @Route("/logs", name="admin_about_log")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function log()
    {
        $form = $this->createForm(SearchLogFileType::class, new SearchLogFile());
        // View
        return $this->render(
            '@Admin/About/logs.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/logs/list", name="admin_about_log_list")
     * @return Response
     */
    public function logfile(Request $request)
    {
        $searchLogsFile = new SearchLogFile();
        $form = $this->createForm(SearchLogFileType::class, $searchLogsFile);
        $form->handleRequest($request);
        $file = $searchLogsFile->getName();
        $nbLine = $searchLogsFile->getNbLine();
        $tail = `tail -n $nbLine $file`;
        $tail = '<div style="padding: 10px;background-color: #f9f9f9;">'.$tail.'</div>';

        return new Response(str_replace('<br />', '</div><hr/><div style="padding: 10px;background-color: #f9f9f9;">', nl2br($tail)));
    }
}
