<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\GedmoTrait;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockEntryRejectionRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(name="stock_entry_rejection")
 */
class StockEntryRejection
{
    use GedmoTrait;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /** 
     * @ORM\ManyToOne(targetEntity="App\Entity\StockEntry", inversedBy="rejections")
     */
    private $stockEntry;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StockEntryItem", inversedBy="itemRejections")
     */
    private $stockEntryItem;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rejectionMsg;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStockEntry()
    {
        return $this->stockEntry;
    }

    public function setStockEntry($stockEntry): self
    {
        $this->stockEntry = $stockEntry;

        return $this;
    }

    public function getStockEntryItem()
    {
        return $this->stockEntryItem;
    }

    public function setStockEntryItem($stockEntryItem): self
    {
        $this->stockEntryItem = $stockEntryItem;

        return $this;
    }

    public function getRejectionMsg(): ?string
    {
        return $this->rejectionMsg;
    }

    public function setRejectionMsg(?string $rejectionMsg): self
    {
        $this->rejectionMsg = $rejectionMsg;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
