<?php


namespace App\Utils\Managers;


use App\Entity\StockEntryItem;
use App\Repository\StockEntryItemRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Gaufrette\Filesystem;

/**
 * Pantin Stock Manager
 *
 * Class StockEntryItemManager
 * @package App\Utils\Managers
 */
class StockEntryItemManager extends BaseManager
{
    /** @var StockEntryItemRepository $stockEntryItemRepository */
    protected $stockEntryItemRepository;

    /**
     * StockEntryItemManager constructor.
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('stockEntryItemRepository', StockEntryItem::class);
    }

    /**
     * Query for Search
     */
    public function queryForSearch(string $locale, array $filters = []): Query
    {
        return $this->stockEntryItemRepository->queryForSearch($locale, $filters);
    }
}
