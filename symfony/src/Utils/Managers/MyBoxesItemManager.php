<?php

namespace App\Utils\Managers;

use App\Entity\MyBoxesProduct;
use App\Entity\MyBoxesCommand;
use App\Repository\MyBoxesItemRepository;
use App\Utils\Managers\BaseManager;

use App\Utils\Managers\LocaleManager;
use App\Entity\MyBoxesItem;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class MyBoxesItemManager
 * `
 * Object manager of myBoxesItem
 *
 * @package Disko\PageBundle\Managers
 */
class MyBoxesItemManager extends BaseManager
{

    /**
     * Repository
     *
     * @var MyBoxesItemRepository
     */
    protected $myBoxesItemRepository;

    /**
     * MyBoxesItemManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager, EventDispatcherInterface $dispatcher)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('myBoxesItemRepository', MyBoxesItem::class);
    }

    /**
     * Save a myBoxesItem
     *
     * @param MyBoxesItem $myBoxesItem
     */
    public function save(MyBoxesItem $myBoxesItem)
    {
        $myBoxesItem->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($myBoxesItem);
        $this->getEntityManager()->flush();

        return $myBoxesItem;
    }

    /**
     * Remove one myBoxesItem
     *
     * @param MyBoxesItem $myBoxesItem
     */
    public function remove(MyBoxesItem $myBoxesItem)
    {
        $this->getEntityManager()->remove($myBoxesItem);
        $this->getEntityManager()->flush();
    }


    /**
     * Create base
     *
     * @param MyBoxesCommand $command
     * @param MyBoxesProduct $product
     * @return MyBoxesItem
     */
    public function create($command, MyBoxesProduct $product)
    {
        $myBoxesItem = new MyBoxesItem();
        $myBoxesItem->setCommand($command);
        $myBoxesItem->setProduct($product);

        return $myBoxesItem;
    }

    /**
     * @param MyBoxesCommand $command
     * @param MyBoxesProduct $product
     * @return int
     */
    public function countInCommand($command, MyBoxesProduct $product)
    {
        $result = 0;

        if (!$command) {
            return $result;
        }

        $items = $command->getItems();
        foreach ($items as $item) {
            if ($item->getProduct()->getId() == $product->getId()) {
                $result = $result + $item->getQuantity();
            }
        }

        return $result;
    }


    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->myBoxesItemRepository->findOneBy($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->myBoxesItemRepository->findAll();
    }
}
