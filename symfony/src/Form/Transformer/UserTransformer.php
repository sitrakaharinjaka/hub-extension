<?php

// src/Form/DataTransformer/UserToNumberTransformer.php
namespace App\Form\Transformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use App\Entity\User;

class UserTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function transform($user)
    {
        if (null === $user) {
            return null;
        }

        return $user;
    }

    public function reverseTransform($user)
    {
        if (!$user || $user == null) {
            return null;
        }
        $userfind = $this->entityManager
            ->getRepository(User::class)
            ->findOneBy(['email' => $user->getEmail()]);

        if ($userfind) {
            return $userfind;
        }

        return $user;
    }
}
