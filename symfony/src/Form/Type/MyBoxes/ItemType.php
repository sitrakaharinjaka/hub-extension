<?php
/**
 * Created by PhpStorm.
 * User: Luk
 * Date: 20/05/2017
 * Time: 20:35
 */

namespace App\Form\Type\MyBoxes;

use App\Entity\MyBoxesItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('quantity', IntegerType::class, [
            'attr' => ['min' => 0, 'max' => 100000, 'class' => 'qty-field'],
            'label' => false,
        ]);
    }

    /**
     * Configure Options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => MyBoxesItem::class,
            'validation_groups' => 'cart'
        ));
    }
}
