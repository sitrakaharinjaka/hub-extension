<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Paweł Jędrzejewski
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Sylius\Component\Product\Model\ProductInterface;
use Sylius\Component\Product\Model\ProductVariantInterface;
use Doctrine\ORM\Mapping as ORM;
use App\Validator\Constraints as ProjectAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectItemRepository")
 * @ORM\Table(name="project_item")
 */
class ProjectItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var integer
     */
    protected $id;

    /**
     * @var Project|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="items")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    protected $project;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    protected $quantity = 1;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $runnerStatus;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    protected $runnerComment;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Project|null
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project|null $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string|null
     */
    public function getRunnerStatus()
    {
        return $this->runnerStatus;
    }

    /**
     * @param string|null $status
     */
    public function setRunnerStatus($status)
    {
        $this->runnerStatus = $status;
    }

    /**
     * @return string|null
     */
    public function getRunnerComment()
    {
        return $this->runnerComment;
    }

    /**
     * @param string|null $comment
     */
    public function setRunnerComment($comment)
    {
        $this->runnerComment = $comment;
    }
}
