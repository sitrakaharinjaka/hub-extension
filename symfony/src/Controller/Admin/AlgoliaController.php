<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\Fail2Ban;
use App\Form\Model\SearchFail2Ban;
use App\Form\Type\User\SearchFail2BanType;
use App\Utils\Managers\Fail2BanManager;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Process\Process;

/**
 * Class AlgoliaController
 * @Route("/admin/maintenance")
 * @package App\Controller
 */
class AlgoliaController extends BaseController
{

    /**
     * Algolia index launch
     *
     * @Route("/index", name="admin_algolia_index")
     * @param Request $request Request
     */
    public function index(Request $request)
    {
        $pathRootDir = $this->getParameter('kernel.root_dir');

        // Lock
        $process = new Process(
            'php bin/console search:import',
            $pathRootDir . '/../'
        );
        $process->run();


        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'indexage a bien été réalisé : <br/>".$process->getOutput()
        );

        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * Algolia index clean
     *
     * @Route("/clean", name="admin_algolia_clean")
     * @param Request $request Request
     */
    public function clean(Request $request)
    {
        $pathRootDir = $this->getParameter('kernel.root_dir');

        // Lock
        $process = new Process(
            'php bin/console search:clear',
            $pathRootDir . '/../'
        );
        $process->run();


        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'index a bien été vidé : <br/>".$process->getOutput()
        );

        return $this->redirect($request->headers->get('referer'));
    }
}
