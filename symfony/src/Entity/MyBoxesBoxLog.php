<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MyBoxesBoxLogRepository")
 * @ORM\Table(name="dk_myboxes_box_log")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class MyBoxesBoxLog
{
    use GedmoTrait;
    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Box
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\MyBoxesBox")
     * @ORM\JoinColumn(name="box_id", referencedColumnName="id")
     * @var MyBoxesProduct
     */
    protected $box;

    /**
     * info
     *
     * @ORM\Column(type="text", nullable=false)
     */
    protected $info;

    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return MyBoxesProduct
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * @param MyBoxesProduct $box
     */
    public function setBox($box)
    {
        $this->box = $box;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }
}
