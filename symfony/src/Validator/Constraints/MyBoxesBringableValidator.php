<?php

namespace App\Validator\Constraints;

use App\Utils\Managers\MyBoxesBoxManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MyBoxesBringableValidator extends ConstraintValidator
{
    protected $myBoxesBoxManager;

    public function __construct(MyBoxesBoxManager $myBoxesBoxManager)
    {
        $this->myBoxesBoxManager = $myBoxesBoxManager;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $command = $value;
        $box = $command->getBox();
        if ($box) {
            if (!$this->myBoxesBoxManager->isBringable($box->getId())) {
                $this
                    ->context
                    ->buildViolation($constraint->message)
                    ->addViolation()
                ;
            }
        }
    }
}
