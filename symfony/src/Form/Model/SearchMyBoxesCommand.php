<?php

/**
 * Model for search
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Model;

/**
 * Class SearchMyBoxesCommand
 *
 * @package App\Form\Model
 */
class SearchMyBoxesCommand
{
    /**
     * Id
     *
     * @var string
     */
    protected $id;

    /**
     * Firstname
     *
     * @var string
     */
    protected $firstname;

    /**
     * Lastname
     *
     * @var string
     */
    protected $lastname;

    /**
     * Office number
     *
     * @var string
     */
    protected $officeNumber;

    /**
     * Stage
     *
     * @var string
     */
    protected $stage;


    /**
     * State
     *
     * @var string
     */
    protected $state;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getOfficeNumber()
    {
        return $this->officeNumber;
    }

    /**
     * @param $officeNumber
     */
    public function setOfficeNumber($officeNumber)
    {
        $this->officeNumber = $officeNumber;
    }

    /**
     * @return string
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * @param $stage
     */
    public function setStage($stage)
    {
        $this->stage = $stage;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

  

    /**
     * Get search data
     *
     * @return array
     */
    public function getSearchData()
    {
        $tab = array();

        if (!empty($this->id)) {
            $tab['id'] = $this->id;
        }
        if (!empty($this->stage)) {
            $tab['stage'] = $this->stage;
        }
        if (!empty($this->officeNumber)) {
            $tab['officeNumber'] = $this->officeNumber;
        }
        if (!empty($this->firstname)) {
            $tab['firstname'] = $this->firstname;
        }
        if (!empty($this->lastname)) {
            $tab['lastname'] = $this->lastname;
        }
        if (!empty($this->state)) {
            $tab['state'] = $this->state;
        }

        return $tab;
    }
}
