<?php

/**
 * Form type
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Type\Stock;

use A2lix\TranslationFormBundle\Form\Type\TranslationsFormsType;
use App\Entity\BillingCenter;
use App\Entity\Stock;
use App\Utils\Managers\LocaleManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\Fo;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Utils\Managers\UserManager;

/**
 *  Form Type
 */
class StockRestrictType extends StockType
{
    protected $localeManager;

    /**
     * @param LocaleManager $localeManager
     */
    public function __construct(LocaleManager $localeManager, UserManager $userManager)
    {
        parent::__construct($localeManager, $userManager);
    }


    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $defaultLocales = $this->localeManager->findDefaultSlug();
        $builder->remove('billingCenters');
        $builder->add('translations', TranslationsFormsType::class, [
            'label' => false,
            'default_locale' => $defaultLocales,
            'required_locales' => $defaultLocales,
            'locales' => $this->localeManager->findAllSlug(),
            'form_type' => StockRestrictTranslationType::class
        ]);
    }

    /**
     * Configure Options
     *
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
            'data_class' => Stock::class
        ));
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'stock';
    }
}
