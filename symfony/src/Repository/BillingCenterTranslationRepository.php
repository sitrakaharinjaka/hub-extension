<?php

/**
 * Repository
 *
 * @author Pierre Baumes <pierre.b@disko.fr>
 */

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class BillingCenterTranslationRepository
 *
 * @package App\Repository
 */
class BillingCenterTranslationRepository extends BaseRepository
{
}
