<?php

namespace App\Utils\Managers;

use App\Entity\BillingCenter;
use App\Entity\RunnerTracing;
use App\Repository\RunnerTracingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class RunnerTracingManager
 * `
 * Object manager of runner tracing
 *
 * @package Disko\PageBundle\Managers
 */
class RunnerTracingManager extends BaseManager
{
    /**
     * Repository
     *
     * @var RunnerTracingRepository
     */
    protected $runnerTracingRepository;

    /**
     * RunnerTracingManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('runnerTracingRepository', RunnerTracing::class);
    }

    /**
     * Save a runnerTracing
     *
     * @param  RunnerTracing  $runnerTracing
     */
    public function save(RunnerTracing $runnerTracing)
    {
        $runnerTracing->setUpdated(new \DateTime());

        $this->getEntityManager()->persist($runnerTracing);
        $this->getEntityManager()->flush();

        return $runnerTracing;
    }

    public function findAll()
    {
        return $this->runnerTracingRepository->findAll();
    }
}
