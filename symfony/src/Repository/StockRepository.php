<?php

namespace App\Repository;

use App\Entity\Stock;
use App\Entity\Project;
use App\Repository\BaseRepository;

/**
 * Class StockRepository
 */
class StockRepository extends BaseRepository
{



    /**
     * ================= FRONT ===================
     */


    /**
     * ================= BACK ===================
     */


    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et')
            ->leftJoin('e.translations', 'et')
            ->orderBy('et.name', 'asc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'et';
                if ($key == 'id') {
                    $alias = 'e';
                }
                $qb->andWhere($alias.'.'.$key.' LIKE :'.$key);
                $qb->setParameter($key, '%'.$filter.'%');
            }
        }

        $query = $qb->getQuery();

        return $query;
    }

    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et', 'bc', 'bct', 'u')
            ->leftJoin('e.translations', 'et')
            ->leftJoin('e.billingCenters', 'bc')
            ->leftJoin('bc.translations', 'bct')
            ->leftJoin('e.referentUser', 'u')

            ->where('e.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }


    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findAllTranslations($stock)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->from('App\Entity\StockTranslation', 't')
            ->select('t')
            ->andWhere('t.stock = :stock')
            ->setParameter('stock', $stock);

        $query = $qb->getQuery();
        return $query->getResult();
    }


    /**
     * Find stocks by users rights
     *
     * @param $user
     */
    public function findByUser($user, $locale = 'fr')
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('c', 't');
        $qb->leftJoin('c.translations', 't', 'WITH', 't.locale = :locale');
        $qb->setParameter('locale', $locale);
        $qb->orderBy('t.name', 'ASC');

      if (!$user->hasRole('ROLE_GENERAL_ADMIN') and !$user->hasRole('ROLE_SUPER_ADMIN')) {
            $qb->leftJoin('c.users', 'u');
            $qb->andWhere("u.id = :user");
            $qb->setParameter('user', $user->getId());
        }

        $query = $qb->getQuery();
        $results = $query->getResult();

        return $results;
    }


    /**
     * Find stocks by users rights
     *
     * @param $user
     */
    public function findByAdminUser($user, $locale = 'fr')
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('c', 't');
        $qb->leftJoin('c.translations', 't', 'WITH', 't.locale = :locale');
        $qb->setParameter('locale', $locale);

        if (!$user->hasRole('ROLE_GENERAL_ADMIN') and !$user->hasRole('ROLE_SUPER_ADMIN')) {
            $qb->leftJoin('c.adminUsers', 'u');
            $qb->andWhere("u.id = :user");
            $qb->setParameter('user', $user->getId());
        }

        $query = $qb->getQuery();
        $results = $query->getResult();

        return $results;
    }


    public function findOneStockBy($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
      ->select('e', 'et')
      ->leftJoin('e.translations', 'et');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'et';
                $qb->andWhere($alias . '.' . $key . ' = :' . $key);
                $qb->setParameter($key, $filter);
            }
        }

        $query = $qb->getQuery();
        return $query->getResult();
    }

    public function findAdminsStock(Stock $stock)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
                   ->from('App\Entity\User', 'u')
                   ->select('u', 'uas')
                   ->join('u.adminStocks', 'uas')
                   ->andWhere('uas.id = :stockId')
                   ->setParameter('stockId', $stock->getId());

        $query = $qb->getQuery();
        return $query->getResult();
    }


    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsCreated($start, $end)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('IFNULL(count(c.id), 0) as cpt, CONCAT(YEAR(c.created), \'/\' ,MONTH(c.created), \'/\' , DAY(c.created)) AS date, DAYOFYEAR(c.created) as dayofyear');
        $qb->andWhere("c.created IS NOT NULL");
        $qb->groupBy("dayofyear");
        $qb->addOrderBy('dayofyear', 'asc');

        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("c.created >= :start");
        $qb->andWhere("c.created <= :end");


        $results = $qb->getQuery()->getScalarResult();

        return $results;
    }

    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsServicesByStock($start, $end)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('c as stock, t.name as stockname, IFNULL(count(c.id), 0) as countprojects');
        $qb->leftJoin('c.translations', 't', 'WITH', 't.locale = :locale');
        $qb->leftJoin('c.projects', 'p');
        $qb->andWhere("c.created IS NOT NULL");
        $qb->andWhere("p IS NOT NULL");
        $qb->groupBy("c.id");
        $qb->addOrderBy('t.name', 'asc');
        $qb->setParameter('locale', 'fr');
        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("p.created >= :start");
        $qb->andWhere("p.created <= :end");
        $qb->andWhere("p.state = :state");
        $qb->setParameter('state', Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP);

        $results = $qb->getQuery()->getResult();

        $datas = [];
        if (count($results) > 0) {
            foreach ($results as $result) {
                $stock = $result['stock'];
                if (is_object($stock)) {
                    unset($result['stock']);

                    $projects = $stock->getProjects();

                    $stockServices = [];

                    foreach($projects as $project){
                        $dateEnd = $project->getDateEnd() instanceof \DateTime? $project->getDateEnd(): $project->getDateStart()->modify('+1 week');

                        if($project->getDateStart() >= $start && $dateEnd <= $end){
                            $beforeServices = $project->getBeforeServices();
                            $afterServices = $project->getAfterServices();

                            $services = array_merge($beforeServices->getValues(), $afterServices->getValues());

                            if (is_array($services) && count($services) > 0) {
                                foreach($services as $service){
                                    if(isset($stockServices[$service->getId()])){
                                        $stockServices[$service->getId()]['count'] += 1;
                                    }else{
                                        $stockServices[$service->getId()]['name'] = $service->translate('fr')->getName();
                                        $stockServices[$service->getId()]['count'] = 1;
                                    }
                                }
                            }
                        }
                    }

                    if(count($stockServices) > 0){
                        $datas[$result['stockname']] = $stockServices;
                    }

                }
            }
        }

        //dump($datas);

        return $datas;
    }
}
