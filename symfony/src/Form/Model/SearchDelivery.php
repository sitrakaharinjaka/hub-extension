<?php

/**
 * Model for search
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Model;

/**
 * Class SearchDelivery
 *
 * @package App\Form\Model
 */
class SearchDelivery
{
    /**
     * Id
     *
     * @var string
     */
    protected $id;

    /**
     * Zip
     *
     * @var string
     */
    protected $zip;

    /**
     * Country
     *
     * @var string
     */
    protected $country;

    /**
     * Deliver
     *
     * @var string
     */
    protected $deliver;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getDeliver()
    {
        return $this->deliver;
    }

    /**
     * @param $deliver
     */
    public function setDeliver($deliver)
    {
        $this->deliver = $deliver;
    }

    /**
     * Get search data
     *
     * @return array
     */
    public function getSearchData()
    {
        $tab = array();

        if (!empty($this->id)) {
            $tab['id'] = $this->id;
        }
        if (!empty($this->zip)) {
            $tab['zip'] = $this->zip;
        }
        if (!empty($this->deliver)) {
            $tab['deliver'] = $this->deliver;
        }
        if (!empty($this->country)) {
            $tab['country'] = $this->country;
        }

        return $tab;
    }
}
