<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class StockEntry115ModelValidator extends ConstraintValidator
{
    private $translator;

    /**
     * StockClassValidator constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator    = $translator;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var StockEntry115Model $stockEntry115Model */
        $stockEntry115Model = $value;

        $stockEntry115Items    = $stockEntry115Model->getStockEntry115Items();
        $stockEntry115ItemsIds = array();

        foreach ($stockEntry115Items as $stockEntry115Item) {
            $stockEntry115ItemsIds []= $stockEntry115Item->getProductId();
        }

        if (self::has_dupes($stockEntry115ItemsIds)) {
            $this
                ->context
                ->buildViolation($this->translator->trans('Vous avez sélectionné au moins deux produits identiques'))
                ->atPath('stockEntry115Items')
                ->addViolation();
        }

        if (!$stockEntry115Model->getStockEntry115Items() || sizeof($stockEntry115Model->getStockEntry115Items()) ==0) {
            $this
                ->context
                ->buildViolation($this->translator->trans('Vous devez saisir au moins 1 article'))
                ->atPath('stockEntry115Items')
                ->addViolation();
        }

    }

    public function has_dupes($array)
    {
        return count($array) !== count(array_unique($array));
    }
}
