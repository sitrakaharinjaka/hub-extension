<?php

namespace App\Utils\Managers;

use App\Entity\Brand;
use App\Entity\BrandTranslation;
use App\Entity\Category;
use App\Entity\CategoryTranslation;
use App\Entity\ImageGallery;
use App\Entity\Media;
use App\Entity\Product;
use App\Entity\ProductTranslation;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class ProductImportManager
 * @package App\Utils\Managers
 */
class ProductImportManager
{
    const BATCH_SIZE = 10;
    const YES = 'Oui';
    const MEDIA_UPLOAD_FOLDER = 'media/media/';
    const LIMIT_WIDTH_IMAGE = 1000;
    const FORMAT_IMAGE = ['jpeg', 'jpg', 'png'];
    const ID_ATTRIBUTE_FOR_PRODUCT_CODE = 0;
    const ID_ATTRIBUTE_FOR_PRODUCT_MEDIA = 28;
    const ID_ATTRIBUTE_FOR_PRODUCT_NAME = 2;
    const ID_ATTRIBUTE_FOR_PRODUCT_EAN_PART = 3;
    const ID_ATTRIBUTE_FOR_PRODUCT_EAN_BOX = 11;

    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    /**
     * @var ObjectRepository
     */
    private $productTranslationRepository;

    /**
     * @var ObjectRepository
     */
    private $categoryTranslationRepository;

    /**
     * @var MediaManager
     */
    private $mediaManager;

    /**
     * @var ObjectRepository
     */
    private $brandTranslationRepository;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @var string
     */
    private $mediaTargetFolder;

    /**
     * ProductImportManager constructor.
     *
     * @param ObjectManager $em
     * @param MediaManager $mediaManager
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(ObjectManager $em, MediaManager $mediaManager, ParameterBagInterface $parameterBag)
    {
        $this->em = $em;
        $this->productTranslationRepository = $this->em->getRepository('App:ProductTranslation');
        $this->categoryTranslationRepository = $this->em->getRepository('App:CategoryTranslation');
        $this->brandTranslationRepository = $this->em->getRepository('App:BrandTranslation');
        $this->mediaManager = $mediaManager;
        $this->parameterBag = $parameterBag;
        $this->mediaTargetFolder = $this->parameterBag->get('kernel.project_dir').'/public/uploads/'.self::MEDIA_UPLOAD_FOLDER;
    }

    /**
     * @param ProductTranslation $productTranslation
     * @param array $values
     * @param bool $isNewProduct
     *
     * @return ProductTranslation
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function updateProduct(ProductTranslation $productTranslation, $values = [], bool $isNewProduct = false)
    {
        $productTranslation->setCode($values[self::ID_ATTRIBUTE_FOR_PRODUCT_CODE]);
        $productTranslation->setDescription(htmlspecialchars($values[1]));
        $productTranslation->setName($values[self::ID_ATTRIBUTE_FOR_PRODUCT_NAME]);
        $productTranslation->setSlug($values[self::ID_ATTRIBUTE_FOR_PRODUCT_NAME].'-'.$values[self::ID_ATTRIBUTE_FOR_PRODUCT_EAN_PART].'-'.$values[self::ID_ATTRIBUTE_FOR_PRODUCT_CODE]);
        $productTranslation->setEanPart($values[self::ID_ATTRIBUTE_FOR_PRODUCT_EAN_PART]);
        $productTranslation->setUnitConditioning($values[4]);
        $this->setCategories($productTranslation, $values[5]);
        $productTranslation->setDepth(floatval(str_replace(',', '.', $values[6])));
        $productTranslation->setWidth(floatval(str_replace(',', '.', $values[7])));
        $productTranslation->setHeight(floatval(str_replace(',', '.', $values[8])));
        $productTranslation->setWeight(floatval(str_replace(',', '.', $values[9])));
        if ($values[10]) {
            $productTranslation->setTotalUnitsByBox(intval($values[10]));
        }
        $productTranslation->setEanBox($values[self::ID_ATTRIBUTE_FOR_PRODUCT_EAN_BOX]);
        $productTranslation->setDepthBox(floatval(str_replace(',', '.', $values[12])));
        $productTranslation->setWidthBox(floatval(str_replace(',', '.', $values[13])));
        $productTranslation->setHeightBox(floatval(str_replace(',', '.', $values[14])));
        $productTranslation->setWeightBox(floatval(str_replace(',', '.', $values[15])));
        $productTranslation->setOtherInformation($values[16]);
        $productTranslation->setCrd(($values[17]) === self::YES ? true : false);
        $productTranslation->setCountry($values[18]);
        $productTranslation->setLitrageBottle($values[19]);
        $productTranslation->setIsEffervescent(($values[20]) === self::YES ? true : false);
        $productTranslation->setDesignationBottle($values[21]);
        $productTranslation->setVintage($values[22]);
        $productTranslation->setWineColor($values[23]);
        $productTranslation->setWineCategory($values[24]);
        $productTranslation->setDegreeAlcohol($values[25]);
        if ($isNewProduct) {
            $productTranslation->setIsExpendable(($values[26]) === self::YES ? true : false);
        }
        $productTranslation->setCustomsNomenclature($values[27]);
        if ($isNewProduct) {
            //$this->setMedias($productTranslation, $values[28]);
        }
        $this->setBrand($productTranslation, $values[29]);
        $productTranslation->setValorization($values[30]);
        if ($isNewProduct) {
            $state = empty($values[31]) ? ProductTranslation::STATE_NEW : $values[31] == 'Neuf' ? ProductTranslation::STATE_NEW : $values[31];
            if ($state != ProductTranslation::STATE_NEW && $state != ProductTranslation::STATE_OUT_OF_USE) {
                throw new \Exception('Etat de produit incorrect :' . $state);
            }
            $productTranslation->setState($state);
        }
        $productTranslation->setColor($values[32]);
        $productTranslation->setMaterial($values[33]);
        if ($isNewProduct) {
            $productTranslation->setIsFragile(($values[34]) === self::YES ? true : false);
        }

        if (isset($values[35])) {
            $termesSAP = ['', 'SAP', 'OUI', '0'];
            $productTranslation->setOrigin(array_search(strtoupper($values[35]), $termesSAP) === false ? 0 : 1);
        }

        return $productTranslation;
    }

    /**
     * @param ProductTranslation $productTranslation
     * @param string $categories
     *
     * @return ProductTranslation
     */
    protected function setCategories(ProductTranslation $productTranslation, string $categories) : ProductTranslation
    {
        if ($categories) {
            $arrayCategories = new ArrayCollection();
            $arrayCategoriesNameFromSap = array_filter(explode(',', $categories), 'strlen');
            $slugify = new Slugify();

            foreach ($arrayCategoriesNameFromSap as $categoryName) {
                $slugCategory = $slugify->slugify($categoryName);
                $categoryTranslation = $this->categoryTranslationRepository->findOneBy(['slug' => $slugCategory]);
                if ($categoryTranslation instanceof CategoryTranslation) {
                    $category = $categoryTranslation->getTranslatable();
                    if (!$arrayCategories->contains($category)) {
                        $arrayCategories->add($category);
                    }
                }
            }
            $productTranslation->setCategories($arrayCategories);
        } else {
            $productTranslation->setCategories(null);
        }
        return $productTranslation;
    }

    /**
     * @param ProductTranslation $productTranslation
     * @param string $brandName
     *
     * @return ProductTranslation
     */
    protected function setBrand(ProductTranslation $productTranslation, string $brandName) : ProductTranslation
    {
        if ($brandName) {
            $slugify = new Slugify();
            $slugBrand = $slugify->slugify($brandName);
            $brandTranslation = $this->brandTranslationRepository->findOneBy(['slug' => $slugBrand]);
            if ($brandTranslation instanceof BrandTranslation) {
                $brand = $brandTranslation->getTranslatable();
                $productTranslation->setBrand($brand);
            }
        } else {
            $productTranslation->setBrand(null);
        }
        return $productTranslation;
    }

    /**
     * @param ProductTranslation $productTranslation
     * @param string $medias
     *
     * @return ProductTranslation
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function setMedias(ProductTranslation $productTranslation, string $medias) : ProductTranslation
    {
        if ($medias) {
            $arrayImageGallery = new ArrayCollection();
            $arrayMediasFromSap = array_filter(explode(',', $medias), 'strlen');
            $i = 1;
            foreach ($arrayMediasFromSap as $mediaUrl) {
                $contentMedia = file_get_contents($mediaUrl);
                $imageInfo = getimagesize($mediaUrl);
                $imageMime = $imageInfo['mime'];
                $imageExt = str_replace('image/', '', $imageMime);

                // We limit our actions at PNG or JPG image formats.
                if ($contentMedia && in_array($imageExt, self::FORMAT_IMAGE)) {
                    $slugify = new Slugify();
                    $slugName = $slugify->slugify($productTranslation->getName());
                    $mediaName = $slugName.'_'.$i.'.'.$imageExt;
                    file_put_contents($this->mediaTargetFolder.$mediaName, $contentMedia);

                    // If the width of image source is too big whe resize it
                    if ($imageInfo[0] > self::LIMIT_WIDTH_IMAGE) {
                        $ratio = (self::LIMIT_WIDTH_IMAGE / $imageInfo[0]);
                        $newHeight = ($imageInfo[1] * $ratio);
                        $imageResized = imagecreatetruecolor(self::LIMIT_WIDTH_IMAGE, $newHeight);
                        $imageSource = ($imageExt == 'png') ? imagecreatefrompng($this->mediaTargetFolder.$mediaName) : imagecreatefromjpeg($this->mediaTargetFolder.$mediaName);
                        imagecopyresampled($imageResized, $imageSource, 0, 0, 0, 0, self::LIMIT_WIDTH_IMAGE, $newHeight, $imageInfo[0], $imageInfo[1]);
                        ($imageExt == 'png') ? imagepng($imageResized, $this->mediaTargetFolder.$mediaName) : imagejpeg($imageResized, $this->mediaTargetFolder.$mediaName);
                        imagedestroy($imageResized);
                    }

                    $media = new Media();
                    $media->setBase64Media($contentMedia);
                    $media->setExtension($imageExt);
                    $media->setMimetype($imageMime);
                    $media->setSize(filesize($this->mediaTargetFolder.$mediaName));
                    $media->setPathMedia(self::MEDIA_UPLOAD_FOLDER.$mediaName);
                    $this->mediaManager->save($media);

                    if ($i === 1) {
                        $productTranslation->setCover($media);
                    } else {
                        $imageGallery = new ImageGallery();
                        $imageGallery->setImage($media);
                        $imageGallery->setProductTranslation($productTranslation);
                        $arrayImageGallery->add($imageGallery);
                    }
                    $i++;
                }
            }
            $productTranslation->setImageGallery($arrayImageGallery);
        }

        unset($contentMedia);
        return $productTranslation;
    }

    /**
     * @param array $data
     * @param ProgressBar $progress
     * @param OutputInterface $output
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function importProducts(array $data, ?ProgressBar $progress, ?OutputInterface $output)
    {
        $i = 1;

        $stopwatch = new Stopwatch();
        $stopwatch->start('import');

        $categories = $this->getExistingCategories();
        $brands = $this->getExistingBrands();
        $errors = [];
        foreach ($data as $row) {
            $values = array_values($row);
            $error = $this->importProduct($values, $categories, $brands);
            if ($error) {
                $errors[] = $error;
                if ($output) {
                    $output->writeln('<error>ERREUR : </error>' . $error);
                }
            }

            if (($i % self::BATCH_SIZE) === 0) {
                $event = $stopwatch->lap('import');
                if ($progress) {
                    $progress->advance(self::BATCH_SIZE);
                }
                if ($output) {
                    $output->writeln(' of products imported ... | - Memory : ' .number_format($event->getMemory() / 1048576, 2) . ' MB - Time : ' . number_format($event->getDuration() / 1000, 2) .' seconds');
                }
                $this->em->flush();
                $this->em->clear();
            }
            $i++;
        }

        $stopwatch->stop('import');
        $this->em->flush();
        $this->em->clear();
        return $errors;
    }

    public function importProduct($values, $categories, $brands)
    {
        $this->createNotExistingCategories($this->getCategories($values), $categories);
        $this->createNotExistingBrand($this->getBrand($values), $brands);

        // Vérification des EAN ([0-9]*)
        if (preg_match('/^[0-9]*$/', $values[self::ID_ATTRIBUTE_FOR_PRODUCT_EAN_PART]) == 0) {
            return 'L\'EAN ' . $values[self::ID_ATTRIBUTE_FOR_PRODUCT_EAN_PART] . ' n\'est pas correct sur le produit "' . $values[self::ID_ATTRIBUTE_FOR_PRODUCT_CODE] . '"';
        }
        if (preg_match('/^[0-9]*$/', $values[self::ID_ATTRIBUTE_FOR_PRODUCT_EAN_BOX]) == 0) {
            return 'L\'EAN carton ' . $values[self::ID_ATTRIBUTE_FOR_PRODUCT_EAN_BOX] . ' n\'est pas correct sur le produit "' . $values[self::ID_ATTRIBUTE_FOR_PRODUCT_CODE] . '"';
        }
        if (strlen($values[27]) > 10) {
            return 'La nomenclature douanière ' . $values[27] . ' contient plus de 10 caractères';
        }

        //Check EAN box for duplicate (Ticket #67023)
        $productsTranslationByEanBox = $this->productTranslationRepository->findOneBy(['eanBox' => $values[self::ID_ATTRIBUTE_FOR_PRODUCT_EAN_BOX]]);
        if ($productsTranslationByEanBox instanceof ProductTranslation) {
            return 'L\'EAN carton ' . $values[self::ID_ATTRIBUTE_FOR_PRODUCT_EAN_BOX] . ' sur le produit "' . $values[self::ID_ATTRIBUTE_FOR_PRODUCT_CODE] . '" est déjà présent dans la base article';
        }

        $productsTranslation = $this->productTranslationRepository->findBy(['code' => $values[self::ID_ATTRIBUTE_FOR_PRODUCT_CODE]]);

        // If there are several product with same code (case of duplicate product with other state)
        if (is_array($productsTranslation) && count($productsTranslation) > 1) {
            foreach ($productsTranslation as $productTranslation) {
                $this->editProduct($productTranslation, $values);

                //$this->setMedias($productTranslation, $values[self::ID_ATTRIBUTE_FOR_PRODUCT_MEDIA]);
            }
        } else {
            $productTranslation = null;
            if (is_array($productsTranslation) && count($productsTranslation) > 0) {
                $productTranslation = $productsTranslation[0];

                //$this->setMedias($productTranslation, $values[self::ID_ATTRIBUTE_FOR_PRODUCT_MEDIA]);
            }
            $this->editProduct($productTranslation, $values);
        }
        return false;
    }

    /**
     * @param array $data
     * @param ProgressBar $progress
     * @param OutputInterface $output
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function importMedias(array $data, ?ProgressBar $progress, ?OutputInterface $output)
    {
        $i = 1;

        $stopwatch = new Stopwatch();
        $stopwatch->start('import_medias');


        foreach ($data as $row) {
            $values = array_values($row);
            $productsTranslation = $this->productTranslationRepository->findBy(['code' => $values[0]]);

            // If there are several product with same code (case of duplicate product with other state)
            if (is_array($productsTranslation) && count($productsTranslation) > 1) {
                foreach ($productsTranslation as $productTranslation) {
                    $this->editProductMedias($productTranslation, $values);
                }
            } else {
                $productTranslation = null;
                if (is_array($productsTranslation) && count($productsTranslation) > 0) {
                    $productTranslation = $productsTranslation[0];
                    $this->editProductMedias($productTranslation, $values);
                }
            }

            if (($i % self::BATCH_SIZE) === 0) {
                $event = $stopwatch->lap('import_medias');
                if ($progress) {
                    $progress->advance(self::BATCH_SIZE);
                }
                if ($output) {
                    $output->writeln(' of products medias imported ... | - Memory : ' .number_format($event->getMemory() / 1048576, 2) . ' MB - Time : ' . number_format($event->getDuration() / 1000, 2) .' seconds');
                }
                $this->em->flush();
                $this->em->clear();
            }
            $i++;
        }

        $stopwatch->stop('import_medias');
        $this->em->flush();
        $this->em->clear();
    }

    /**
     * @param $productTranslation
     * @param $values
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function editProduct($productTranslation, $values)
    {
        // Only if attribute for name is not empty
        if (!empty(trim($values[self::ID_ATTRIBUTE_FOR_PRODUCT_NAME]))) {
            $isNewProduct = false;
            if (!$productTranslation instanceof ProductTranslation) {
                $product = new Product();
                $productTranslation = new ProductTranslation();
                $productTranslation->setLocale('fr');
                $product->addTranslation($productTranslation);
                $isNewProduct = true;
            }
            $productTranslation = $this->updateProduct($productTranslation, $values, $isNewProduct);
            if ($isNewProduct) {
                $this->em->persist($product);
            }

            $this->em->persist($productTranslation);
            $this->em->flush();
        }
    }

    /**
     * @param $productTranslation
     * @param $values
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function editProductMedias($productTranslation, $values)
    {
        // Only if attribute for name is not empty
        if (!empty(trim($values[self::ID_ATTRIBUTE_FOR_PRODUCT_NAME]))) {
            $cover = $productTranslation->getCover();
            if (empty($cover)) {
                $productTranslation = $this->setMedias($productTranslation, $values[28]);
            }
            $this->em->persist($productTranslation);
        }
    }

    public function getExistingCategories()
    {
        $existingCategoriesNames = [];
        $allCategoriesTranslation = $this->categoryTranslationRepository->findAll();
        if (is_array($allCategoriesTranslation) && count($allCategoriesTranslation) > 0) {
            foreach ($allCategoriesTranslation as $categoryTranslation) {
                array_push($existingCategoriesNames, $categoryTranslation->getName());
            }
        }
        return $existingCategoriesNames;
    }

    private function getCategories($values)
    {
        $array = explode(',', $values[5]);
        $array = array_map('trim', $array);
        $array = array_filter($array, 'strlen');
        return $array;
    }

    private function createNotExistingCategories($categories, &$existing)
    {
        // We keep only new categories
        $news = array_diff($categories, $existing);
        foreach ($news as $name) {
            $categoryTranslation = new CategoryTranslation();
            $categoryTranslation->setLocale('fr');
            $categoryTranslation->setName($name);
            $category = new Category();
            $category->addTranslation($categoryTranslation);
            $this->em->persist($category);
            $existing[] = $name;
        }
    }

    public function getExistingBrands()
    {
        $existingBrandsNames = [];
        $allBrandsTranslation = $this->brandTranslationRepository->findAll();
        if (is_array($allBrandsTranslation) && count($allBrandsTranslation) > 0) {
            foreach ($allBrandsTranslation as $brandTranslation) {
                array_push($existingBrandsNames, $brandTranslation->getName());
            }
        }
        return $existingBrandsNames;
    }

    private function getBrand($values)
    {
        $brand = $values[29];
        $brand = trim($brand);
        return $brand;
    }

    private function createNotExistingBrand($brand, &$existing)
    {
        // We keep only new categories
        $news = array_diff([$brand], $existing);
        foreach ($news as $name) {
            $brandTranslation = new BrandTranslation();
            $brandTranslation->setLocale('fr');
            $brandTranslation->setName($name);
            $brand = new Brand();
            $brand->addTranslation($brandTranslation);
            $this->em->persist($brand);
            $existing[] = $name;
        }
    }
}
