<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BrandRepository")
 * @ORM\Table(name="dk_brand")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Brand
{
    use GedmoTrait;
    use AuthorableTrait;
    use ORMBehaviors\Translatable\Translatable;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductTranslation", mappedBy="brand", cascade={"all"})
     */
    protected $productTranslation;

    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->translate('fr')->getName();
    }

    /**
     * @return ProductTranslation
     */
    public function getProductTranslation(): ?ProductTranslation
    {
        return $this->productTranslation;
    }

    /**
     * @param ProductTranslation $productTranslation
     */
    public function setProductTranslation(ProductTranslation $productTranslation): void
    {
        $this->productTranslation = $productTranslation;
    }
}
