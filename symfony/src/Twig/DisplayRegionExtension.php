<?php
namespace App\Twig;

class DisplayRegionExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('displayRegion', array($this, 'displayRegion')),
            new \Twig_SimpleFilter('displayCountry', array($this, 'displayCountry')),
        );
    }

    public function displayRegion($locale)
    {
        if (strtolower($locale) == 'jp') {
            $locale = 'jpn';
        }
        if (strtolower($locale) == 'cn') {
            $locale = 'zh';
        }

        $result = "Undefined";
        try {
            $result = locale_get_display_language(strtolower($locale), strtolower($locale));
        } catch (\Exception $e) {
        }


        return $result;
    }

    public function getName()
    {
        return 'display_region_extension';
    }
}
