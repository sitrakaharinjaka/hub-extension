<?php

namespace App\Command\Veolog;

use App\Command\Veolog\AbstractVeologCommand;
use App\Utils\Managers\StockLineManager;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\Product;
use App\Entity\Project;
use App\Entity\ProjectRejection;
use App\Utils\Managers\UserManager;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\ProjectItemManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * @property  mailer
 */
class SynchOrderConfirmCommand extends AbstractVeologCommand
{

  /** @var  EntityManager */
    private $productManager;
    private $projectManager;
    private $projectItemManager;
    private $stockLineManager;
    private $rootDir;

  /**
   * SynchOrderConfirmCommand constructor.
   * @param MailerService $mailer
   * @param ParameterBagInterface $parameterBag
   * @param LoggerInterface $logger
   * @param ProductManager $productManager
   * @param ProjectManager $projectManager
   * @param ProjectItemManager $projectItemManager
   * @param StockLineManager $stockLineManager
   * @param UserManager $userManager
   * @param KernelInterface $kernel
   */
  public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, ProductManager $productManager, ProjectManager $projectManager, ProjectItemManager $projectItemManager, StockLineManager $stockLineManager, UserManager $userManager, KernelInterface $kernel)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->productManager = $productManager;
        $this->projectManager = $projectManager;
        $this->projectItemManager = $projectItemManager;
        $this->stockLineManager = $stockLineManager;

        $this->rootDir = $kernel->getProjectDir();
    }

    protected static $defaultName = 'veolog:order-confirm';

    protected function configure()
    {
        $this
            ->setDescription('Synch veolog order confirm')
            ->addOption(
                'vlgenv',
                null,
                InputOption::VALUE_OPTIONAL,
                'Env veolog prod?',
                false
            )
            ->setHelp('This command allows you to synch veolog order confirm evry 5minutes');
    }

    /**
     * Connexion et Login FTP sur le serveur distant (ftp://vlgcli18@ftp.veolog.fr/OUT/)
     * Exception en cas d'erreur.
     */
    private function tryConnect()
    {
        $ftp_handle = ftp_connect('ftp.veolog.fr');

        if (false === $ftp_handle) {
            throw new \Exception('Unable to connect');
        }

        $loggedIn = ftp_login($ftp_handle, 'vlgcli18', '1L8vJv2U75');
        ftp_pasv($ftp_handle, true);

        if (true === $loggedIn) {
            return $ftp_handle;
        } else {
            throw new \Exception('Unable to log in');
        }
    }

    /**
     * On veux les fichiers
     * - status_orders_XXX.csv
     * - /XXX/status_orders_XXX.csv
     * On ne veux pas :
     * - XXXstatus_orders_XXX.csv
     * - status_orders_XXX.xls
     * - status_orders_BOX_XXX.csv
     */
    private function isCommandFile($filename)
    {
        $pattern='~^/?([a-zA-Z0-9_]*/)*status_orders_[0-9]*\.csv$~';
        return preg_match($pattern, $filename);
    }

    /**
     * Get all files for status order on FTP directory
     */
    private function getFTPFiles($ftp_handle, $directory)
    {
        $filenames = ftp_nlist($ftp_handle, $directory);
        sort($filenames);

        $avalaible_file = [];
        //check to take only status_order_files
        foreach ($filenames as $filename) {
            if ($this->isCommandFile($filename)) {
                $avalaible_file[] = $filename;
            }
        }

        return $avalaible_file;
    }

    /**
     * Do all the actions to take into account the project:
     * - saving
     * - sending mail
     */
    private function closeProject($project, $entete)
    {
        if ($project) {
            $this->projectManager->save($project);
        }

        if ($project && $entete[6] == 'X') {
            $this->log('info', '    Sending email to project recipient');
            $contacts = $project->getStock()->translate('fr');
            $mails    = [];
            if ($contacts && $contacts->getTranslatable() && $contacts->getTranslatable()->getAdminUsers() && $contacts->getTranslatable()->getAdminUsers()->getValues()) {
                foreach ($contacts->getTranslatable()->getAdminUsers()->getValues() as $contact) {
                    $mails[] = $contact->getEmail();
                }
            }
            if ($contacts && $contacts->getTranslatable() && $contacts->getTranslatable()->getReferentUser()) {
                $mails[] = $contacts->getTranslatable()->getReferentUser()->getEmail();
            }

            /**
             * #66689 - Send a copy of the email to the project's user
             */
            if ($project && $project->getUser()) {
                $mails[] = $project->getUser()->getEmail();
            }

            $this->mailer->sendMailExpeditionProjectToRef($project, $mails);
        }

        if ($project && $entete[6] == 'A') {
            $this->log('info', '    Sending email to project recipient');
            $contacts = $project->getStock()->translate('fr');
            $mails    = [];
            foreach ($contacts->getTranslatable()->getAdminUsers()->getValues() as $contact) {
                $mails[] = $contact->getEmail();
            }
            if ($contacts && $contacts->getTranslatable() && $contacts->getTranslatable()->getAdminUsers() && $contacts->getTranslatable()->getAdminUsers()->getValues()) {
                if ($contacts && $contacts->getTranslatable() && $contacts->getTranslatable()->getReferentUser()) {
                    $mails[] = $contacts->getTranslatable()->getReferentUser()->getEmail();
                }
            }
            if ($project && $project->getUser()) {
                $mails[] = $project->getUser()->getEmail();
            }
            $this->mailer->sendMailCancelByVeolog($project, $mails);
        }

        if ($project && $entete[6] == 'R') {
            $this->log('info', '    Sending email to project recipient');
            $mails    = [];

            if ($project && $project->getUser()) {
                $mails[] = $project->getUser()->getEmail();
            }

            $this->mailer->sendMailRejectByVeolog($project, $mails);
        }
    }

    protected function executeCommand()
    {
        $directory = $this->getDirVlgOUT();
        $delimiter = ";";

        $this->log('info', 'Scan directory ' . $directory . ' to find csv');

        try {
            $ftp_handle = $this->tryConnect();
            $this->log('info', 'Connexion <info>success</info>');

            $avalaible_file = $this->getFTPFiles($ftp_handle, $directory);
            $this->log('info', count($avalaible_file) . ' file(s) find into directory '.$directory);

            foreach ($avalaible_file as $file) {
                $file = $this->getDirVlgOUT() . '/' . basename($file);

                $tmp_handle = fopen('php://temp', 'r+');
                if (ftp_fget($ftp_handle, $tmp_handle, $file, FTP_ASCII)) {
                    rewind($tmp_handle);

                    $project = null;
                    $entete = null;

                    while ($csv_row = fgetcsv($tmp_handle, 0, $delimiter)) {
                        $type_line = $csv_row[0];

                        if ($type_line == 'E') {
                            if ($project) {
                                // Je suis sur un nouveau project.
                                // Pour le moment, ne devrait pas arriver
                                // Mais au cas ou; je clos le précédent.
                                $this->closeProject($project, $entete);
                            }
                            $entete = $csv_row;
                            $this->log('info', 'Project '.$entete[2]);
                            // $items = [];
                            $project = $this->projectManager->findOneToEdit(preg_replace('/^[0]*/', '', $entete[2]));
                            if (!empty($project)) {
                                //Clear project rejections
                                $this->log('info', 'Clearing current project rejections..');
                                $this->projectManager->clearRejections($project);

                                switch ($entete[6]) {
                                    case 'I':
                                        $project->setState(Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_INT);
                                        $this->log('info', '    updating to '.$project->getState());
                                    break;
                                    case 'A':
                                        $this->stockLineManager->removeFromProject($project);
                                        $project->setState(Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_CANCEL);
                                        $this->log('info', '    updating to '.$project->getState());
                                        break;
                                    case 'R':
                                        // Archive each stockline having this project id
                                        // $stockLines = $this->stockLineManager->archiveItemsForProject($project);
                                        // foreach ($stockLines as $stockLine) {
                                        //     $stockLine->setArchived(true);
                                        //     $stockLine->setDeletedAt(new DateTime('now'));
                                        //     $this->stockLineManager->save($stockLine);
                                        // }
                                        $this->stockLineManager->removeFromProject($project);
                                        $project->setState(Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_REJECTED);
                                        $this->log('info', '    updating to '.$project->getState());
                                        break;
                                    case 'P':
                                        $project->setState(Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_PREPARE);
                                        $this->log('info', '    updating to '.$project->getState());
                                        break;
                                    case 'X':
                                        // Expedition date for stats
                                        $dateexp = $entete[4];
                                        $timeexp = $entete[5];
                                        $yearexp = substr($dateexp, 0, 4);
                                        $monthexp = substr($dateexp, 4, 2);
                                        $dayexp = substr($dateexp, 6, 2);
                                        $hourexp = substr($timeexp, 0, 2);
                                        $minexp = substr($timeexp, 2, 2);
                                        $dateexp = \DateTime::createFromFormat("Y-m-d H:i", $yearexp."-".$monthexp."-".$dayexp." ".$hourexp.":".$minexp);
                                        $project->setExpeditionDate($dateexp);


                                        $project->setState(Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP);
                                        $this->log('info', '    updating to '.$project->getState());
                                        break;
                                    }
                            } else {
                                $this->log('info', '    <comment>NOT FOUND</comment>');
                            }
                        }

                        if ($project && $type_line == 'C') {
                            $this->log('info', '    tracker : '.$csv_row[3]);
                            $project->addTracker($csv_row[3]);
                        }
                        if ($type_line == 'L') {
                            // Rien à faire
                        }

                        if ($type_line == 'R') {
                            $rejectionReason = $csv_row[2];
                            $rejectedProduct = $this->productManager->checkExist(trim($csv_row[2]));

                            $rejection = new ProjectRejection;

                            if($rejectedProduct instanceof Product){
                                $rejection->setProduct($rejectedProduct);
                                $rejection->setRejectionMessage(ProjectRejection::PRODUCT_NOT_FOUND_VEOLOG_REJECTION);

                                $this->log('info', 'Projet #' . $project->getId() . ' - Rejet Veolog : SKU ' . trim($csv_row[2]) . ' inconnu');
                            }else{
                                $rejection->setRejectionMessage($rejectionReason);
                                $this->log('info', 'Projet #' . $project->getId() . ' - Rejet Veolog : ' . $rejectionReason);
                            }
                            
                            $rejection->setStatus(ProjectRejection::STATUS_PENDING);

                            $project->addRejectionMessage($rejection);
                        }
                    }

                    if(isset($entete) && is_array($entete)){
                        $this->closeProject($project, $entete);
                    }
                    
                } else {
                    $this->log('info', 'erreur while get csv file');
                }
                fclose($tmp_handle);

               // Effacement du fichier $file
               if (ftp_delete($ftp_handle, $file)) {
                   $this->log('info', '    Deleting files ' . $file . ' <info>success</info>');
               } else {
                   $this->log('info', '    <comment>Error during Deleting files ' . $file . '</comment>');
               }
            }

            ftp_close($ftp_handle);
        } catch (\Exception $e) {
            $this->log('info', "<error>Failure: " . $e->getMessage() . "</error>");
        }
    }
}
