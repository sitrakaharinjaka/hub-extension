<?php

namespace App\Form\Model;

use App\Entity\BillingCenter;
use App\Entity\Project;
use App\Entity\Stock;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;

class BulkOrder
{
    /**
     * Excel File
     *
     * @var File
     */
    protected $excelFile;

    /**
     *
     * @var ArrayCollection
     */
    protected $beforeServices;

    /**
     * @var bool
     */
    protected $needQuote = false;

    /**
     * @var string
     */
    protected $beforeQuote;

    /**
     * Project Name
     *
     */
    protected $projectName;

    /**
     * @var \DateTimeInterface|null $deliveryDate
     */
    protected $deliveryDate;

    /**
     * @var Stock|null
     */
    protected $stock;

    /**
     * @var BillingCenter|null
     */
    protected $billingCenter;

    /**
     * @var Collection|Project[]
     */
    protected $projects;

    public function __construct()
    {
        $this->projects       = new ArrayCollection();
        $this->beforeServices = new ArrayCollection();
    }

    /**
     * @return File
     */
    public function getExcelFile()
    {
        return $this->excelFile;
    }

    /**
     * @param File $excelFile
     */
    public function setExcelFile($excelFile)
    {
        $this->excelFile = $excelFile;
    }

    /**
     * @return string
     */
    public function getBeforeQuote()
    {
        return $this->beforeQuote;
    }

    /**
     * @param $beforeQuote
     */
    public function setBeforeQuote($beforeQuote)
    {
        $this->beforeQuote = $beforeQuote;
    }

    /**
     * @return mixed
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * @param mixed $projectName
     */
    public function setProjectName($projectName): void
    {
        $this->projectName = $projectName;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDeliveryDate(): ?\DateTimeInterface
    {
        return $this->deliveryDate;
    }

    /**
     * @param \DateTimeInterface|null $deliveryDate
     */
    public function setDeliveryDate(?\DateTimeInterface $deliveryDate): void
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return Stock|null
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param  Stock|null  $stock
     */
    public function setStock(?Stock $stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return BillingCenter|null
     */
    public function getBillingCenter(): ?BillingCenter
    {
        return $this->billingCenter;
    }

    /**
     * @param BillingCenter|null $billingCenter
     */
    public function setBillingCenter(?BillingCenter $billingCenter): void
    {
        $this->billingCenter = $billingCenter;
    }

    /**
     * @return bool
     */
    public function isNeedQuote(): bool
    {
        return $this->needQuote;
    }

    /**
     * @param bool $needQuote
     */
    public function setNeedQuote(bool $needQuote): void
    {
        $this->needQuote = $needQuote;
    }

    /**
     * {@inheritdoc}
     */
    public function addProject(Project $project)
    {
        $this->projects->add($project);
    }

    /**
     * {@inheritdoc}
     */
    public function removeProject(Project $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * {@inheritdoc}
     */
    public function getProjects()
    {
        return $this->projects;
    }


    /**
     * @return ArrayCollection
     */
    public function getBeforeServices()
    {
        return $this->beforeServices;
    }

    /**
     * {@inheritdoc}
     */
    public function addBeforeService($service)
    {
        if ($this->hasBeforeService($service)) {
            return;
        }

        $this->beforeServices->add($service);
    }

    /**
     * {@inheritdoc}
     */
    public function removeBeforeService($service)
    {
        if ($this->hasBeforeService($service)) {
            $this->beforeServices->removeElement($service);
        }
    }


    /**
     * {@inheritdoc}
     */
    public function clearBeforeServices()
    {
        $this->beforeService->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function hasBeforeService($service)
    {
        return $this->beforeService->contains($service);
    }
}
