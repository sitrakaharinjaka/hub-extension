<?php

namespace App\Command\Veolog\MyBoxes;

use App\Command\Veolog\AbstractVeologCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Utils\Managers\MyBoxesBoxManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @property  mailer
 */
class BackReminderCommand extends AbstractVeologCommand
{
    private $boxManager;

    private const NB_HOURS = 72;

    protected static $defaultName = 'myboxes:back-reminder';

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, MyBoxesBoxManager $boxManager)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->boxManager = $boxManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Synch veolog stock boxes')
            ->setHelp('This command allows you to synch veolog stock');
    }

    private function getBoxInError()
    {
        return $this->boxManager->findBoxMemberSince(self::NB_HOURS);
    }

    protected function executeCommand()
    {
        try {
            $this->log('info', 'Envoi de mail de reminder aux utilisateurs ayant des boxes depuis plus de ' . self::NB_HOURS . 'h.');
            $boxes = $this->getBoxInError();
            foreach ($boxes as $box) {
                $this->log('info', 'Traitement de la box <comment>#' . $box->getId() . '</comment>');
                $this->mailer->sendMailMyBoxesBackReminderSent($box, self::NB_HOURS);
            }
            $this->log('info', '<info>Fin</info>');
        } catch (\Exception $e) {
            $this->log('info', '<error>ERREUR</error> : ' . $e->getMessage());
        }
    }
}
