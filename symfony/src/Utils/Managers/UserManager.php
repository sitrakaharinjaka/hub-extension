<?php

/**
 * Manager
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Utils\Managers;

use App\Repository\UserRepository;
use App\Utils\Managers\BaseManager;

use Doctrine\ORM\Query;
use Gaufrette\Filesystem;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserManager
 * `
 * Object manager of user
 *
 * @package App\Utils\Managers\User
 */
class UserManager extends BaseManager
{
    private $passwordEncoder;

    /** @var UserRepository $userRepository */
    protected $userRepository;

    /**
     * UserManager constructor.
     *
     * @param EntityManagerInterface       $em
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager = null, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('userRepository', User::class);
        $this->passwordEncoder = $passwordEncoder;
    }


    /**
     * Save a user
     *
     * @param User $user
     */
    public function save(User $user)
    {
        // Save user
        $user->setUpdated(new \DateTime());

        $plainpassword = $user->getPlainPassword();
        if (!empty($plainpassword)) {
            $password = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
        }

        if (!$user->hasRole('ROLE_USER')) {
            $user->addRole('ROLE_USER');
        }

        $this->em->persist($user);
        $this->em->flush();

        $this->clearResultCache();

        return $user;
    }

    /**
     * @param User $user
     *
     */
    public function remove(User $user)
    {
        $this->getEntityManager()->remove($user);
        $this->getEntityManager()->flush();
    }

    /**
     * Query all users
     * @param array<string, mixed> $filters
     */
    public function queryForSearch(array $filters = []): Query
    {
        return $this->userRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->userRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->userRepository->findOneBy($filters);
    }

    /**
     * Count all page
     */
    public function countAll($mode)
    {
        return $this->userRepository->countAll($mode);
    }

    /**
     * Get one user by filters
     *
     * @param string $id
     */
    public function findOneById($id)
    {
        return $this->userRepository->findOneById($id);
    }


    /**
     * Get one user by email
     *
     * @param string $email
     */
    public function findOneByEmail($email)
    {
        return $this->userRepository->findOneByEmail($email);
    }

    /**
     * Get one user by token
     *
     * @param string $token
     */
    public function findOneByToken($token)
    {
        return $this->userRepository->findOneByToken($token);
    }

    public function findLikeSearch($search, $noemail)
    {
        $users = $this->userRepository->findLikeSearch($search, $noemail);

        $tabs = [];
        foreach ($users as $user) {
            $tabs[]['name'] = $user->getEmail();
        }

        return $tabs;
    }


    private function debug($user)
    {
        echo $user->getUsername() . " # "
            . $user->getEmail() . " # "
            . $user->getLastName() . " # "
            . $user->getDisplayName() . " # "
            . $user->getDepartment() . " # "
            . $user->getDivision() . " # "
            . $user->getCompany() . " # "
            . $user->getPhone() . " # "
            . $user->getOrganizationUnit() . "\n";
    }

    public function updateFromAD($data, $user, $debug)
    {
        if ($debug) {
            echo "--------------------------------------------------------------------------------\n";
            $this->debug($user);
        }
        if (!$user->getFirstName()) {
            $user->setFirstName($data['First Name']);
        }
        if (!$user->getLastName()) {
            $user->setLastName($data['Last Name']);
        }
        if (!$user->getDisplayName()) {
            $user->setDisplayName($data['First Name'] . " " . $data['Last Name']);
        }
        if (!$user->getDepartment()) {
            $user->setDepartment($data['Departement']);
        }
        if (!$user->getDivision()) {
            $user->setDivision($data['Division']);
        }
        if (!$user->getCompany()) {
            $user->setCompany($data['Company']);
        }
        if (!$user->getPhone()) {
            $user->setPhone($data['Phone']);
        }
        if (!$user->getOrganizationUnit()) {
            $user->setOrganizationUnit(null);
        }
        if ($debug) {
            $this->debug($user);
        } else {
            $this->save($user);
        }
    }

    public function createFromAD($data, $debug)
    {
        if ($debug) {
            echo "================================================================================\n";
        }
        $user = new User();
        $user->setEmail($data['Email Address']);
        $user->setUsername($data['Email Address']);
        $user->setRoles(['ROLE_USER']);
        $user->setActive(true);
        $user->setPlainPassword('pswd'); // Ce password ne servira pas puisque on se connecte via l'AD.
        $user->setFirstName($data['First Name']);
        $user->setLastName($data['Last Name']);
        $user->setDisplayName($data['First Name'] . " " . $data['Last Name']);
        $user->setDepartment($data['Departement']);
        $user->setDivision($data['Division']);
        $user->setCompany($data['Company']);
        $user->setPhone($data['Phone']);
        $user->setOrganizationUnit(null);
        if ($debug) {
            $this->debug($user);
        } else {
            $this->save($user);
        }
    }


    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsCreated($start, $end)
    {
        return $this->userRepository->statsCreated($start, $end);
    }


    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsFirstConnect($start, $end)
    {
        return $this->userRepository->statsFirstConnect($start, $end);
    }

    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function topBox($limit = 10, $emailpart = '')
    {
        return $this->userRepository->topBox($limit, $emailpart);
    }

    public function findAllExceptAdmins($term = null)
    {
        return $this->userRepository->findAllExceptAdmins($term);
    }
}
