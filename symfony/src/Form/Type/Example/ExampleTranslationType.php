<?php

/**
 * Form type
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Type\Example;

use App\Entity\ExampleTranslation;
use App\Entity\Media;
use App\Form\Transformer\MediaTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\Fo;

/**
 *  Form Type
 */
class ExampleTranslationType extends AbstractType
{
    protected $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    
    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class);
        $builder->add('mantra', TextType::class, ['required' => false]);
        $builder->add(
            'category',
            ChoiceType::class,
            array(
            'choices'  => array(
                'A' => 'A',
                'B' => 'B',
                'C' => 'C',
                'D' => 'D',
                'E' => 'E',
                'F' => 'F',
                'G' => 'G',
            ),
            'required' => false)
        );
        $builder->add(
            'multiples',
            ChoiceType::class,
            array(
                'choices'  => array(
                    'A' => 'A',
                    'B' => 'B',
                    'C' => 'C',
                    'D' => 'D',
                    'E' => 'E',
                    'F' => 'F',
                    'G' => 'G',
                ),
                'multiple' => true,
                'required' => false)
        );
        $builder->add('description', TextareaType::class, array('required' => false));
        $builder->add('descriptionUi', TextareaType::class, array('required' => false));
        $builder->add('active', CheckboxType::class, array('required' => false));


        $builder->add('publishDate', DateType::class, array(
            'required' => false,
            'widget' => 'single_text',
            'input' => 'datetime',
            'format' => 'dd/MM/y',
            'required' => false,
        ));

        $builder->add(
            $builder->create('cover', FormType::class, [
                'required' => false,
                'data_class' => Media::class,
                'by_reference' => false,
            ])
            ->add('id', TextType::class, ['required' => false])
            ->addModelTransformer(new MediaTransformer($this->em))
        );

        // Seo
        $builder->add('seoMetaTitle', TextType::class, array('required' => false));
        $builder->add('seoMetaDescription', TextareaType::class, array('required' => false));
        $builder->add('seoMetaKeywords', TextType::class, array('required' => false));

        $builder->add('ogTitle', TextType::class, array('required' => false));
        $builder->add('ogDescription', TextareaType::class, array('required' => false));


        $builder->add(
            $builder->create('ogCover', FormType::class, [
                'required' => false,
                'data_class' => Media::class,
                'by_reference' => false,
            ])
                ->add('id', TextType::class, ['required' => false])
                ->addModelTransformer(new MediaTransformer($this->em))
        );

        $builder->add(
            $builder->create('crop', FormType::class, [
                'required' => false,
                'data_class' => Media::class,
                'by_reference' => false,
            ])
                ->add('id', TextType::class, ['required' => false])
                ->addModelTransformer(new MediaTransformer($this->em))
        );
        $builder->add(
            $builder->create('pdf', FormType::class, [
                'required' => false,
                'data_class' => Media::class,
                'by_reference' => false,
            ])
                ->add('id', TextType::class, ['required' => false])
                ->addModelTransformer(new MediaTransformer($this->em))
        );
    }

    /**
     * Configure Options
     *
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
            'data_class' => ExampleTranslation::class
        ));
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'example';
    }
}
