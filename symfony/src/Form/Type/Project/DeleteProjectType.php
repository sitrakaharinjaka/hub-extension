<?php
/**
 * Created by PhpStorm.
 * User: Luk
 * Date: 20/05/2017
 * Time: 20:35
 */

namespace App\Form\Type\Project;

use App\Entity\Project;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeleteProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('reasonOfDelete', TextType::class, [
            'label' => false,
            'attr' => [
                'class' => 'field field--simple',
                'placeholder' => 'Merci de préciser la raison de la suppression *'
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'edit_admin' => false,
            'data_class' => Project::class,
            'validation_groups' => 'delete'
        ]);
    }
}
