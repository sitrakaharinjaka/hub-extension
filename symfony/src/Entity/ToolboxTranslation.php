<?php

namespace App\Entity;

use App\Entity\Traits\AuthorableTrait;
use App\Entity\Traits\GedmoTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

// Use For Algolia Or Api
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ToolboxRepository")
 * @ORM\Table(name="dk_toolbox_translation")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ToolboxTranslation implements NormalizableInterface
{
    use ORMBehaviors\Translatable\Translation;
    use GedmoTrait;

    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Name
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $title;

    /**
     * Slug
     *
     * @Gedmo\Slug(fields={"title"}, unique=false)
     * @ORM\Column(length=255, unique=false)
     */
    protected $slug;

    /**
     * Category
     * @var ?string
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $category;

    /**
     * Cover
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="cover_id", referencedColumnName="id", onDelete="SET NULL")
     * @var Media
     */
    protected $cover;


    /**
     * Set id
     *
     * @param integer $id
     * @return ToolboxTranslation
     */
    public function setId(int $id): ToolboxTranslation
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * Set title
     *
     * @param string $title
     * @return ToolboxTranslation
     */
    public function setTitle(string $title): ToolboxTranslation
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return ToolboxTranslation
     */
    public function setSlug(string $slug): ToolboxTranslation
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Media
     */
    public function getCover(): Media
    {
        return $this->cover;
    }

    /**
     * @param mixed $cover
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    }

    /**
     * Use to serialize object for Algolia or Api
     *
     * @param NormalizerInterface $normalizer
     * @param null $format
     * @param array $context
     * @return array
     */
    public function normalize(NormalizerInterface $normalizer, $format = null, array $context = []): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'locale' => $this->getLocale(),
            'slug' => $this->getSlug(),
            'translatable_id' => $this->getTranslatable()->getId(),
        ];
    }
}
