<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\GedmoTrait;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRejectionRepository")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ProjectRejection
{
    use GedmoTrait;

    public const PRODUCT_NOT_FOUND_VEOLOG_REJECTION = 'sku_not_found';
    public const DUPLICATE_PROJECT_VEOLOG_REJECTION = 'duplicate';
    public const ADDRESS_VEOLOG_REJECTION = 'invalid_address';
    public const ZIP_VEOLOG_REJECTION = 'invalid_zip';
    public const CITY_VEOLOG_REJECTION = 'invalid_city';
    public const COUNTRY_VEOLOG_REJECTION = 'invalid_country';

    public const STATUS_PENDING = 'pending';
    public const STATUS_CLEARED = 'cleared';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="rejectionMessages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="projectRejections")
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rejectionMessage;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getRejectionMessage(): ?string
    {
        return $this->rejectionMessage;
    }

    public function setRejectionMessage(string $rejectionMessage): self
    {
        $this->rejectionMessage = $rejectionMessage;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
