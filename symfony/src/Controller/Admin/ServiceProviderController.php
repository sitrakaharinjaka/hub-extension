<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\ServiceProvider;
use App\Entity\ServiceProviderTranslation;
use App\Form\Model\SearchServiceProvider;
use App\Form\Type\ServiceProvider\ServiceProviderTranslationType;
use App\Form\Type\ServiceProvider\SearchServiceProviderType;
use App\Form\Type\ServiceProvider\ServiceProviderType;
use App\Utils\Managers\ServiceProviderManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ServiceProviderController
 * @Route("/admin/serviceproviders")
 * @package Disko\AdminBundle\Controller
 */
class ServiceProviderController extends BaseController
{
    /**
     * ServiceProvider's list
     * @Route("/list/{askLocale}", name="admin_serviceprovider_list", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listAction(Request $request, ServiceProviderManager $serviceproviderManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchServiceProviderType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des fournisseurs de services');

        // Generate query
        $query = $serviceproviderManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/ServiceProvider/index.html.twig', array(
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchServiceProvider();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setName((isset($filters['name']))   ? $filters['name'] : '');

        return $data;
    }

    /**
     * Create a serviceprovider
     * @Route("/create", name="admin_serviceprovider_create")
     * @param Request $request Request
     *
     * @return Response
     */
    public function createAction(Request $request, ServiceProviderManager $serviceproviderManager, LocaleManager $localeManager)
    {
        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");

        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des fournisseur de servicess", $this->get("router")->generate("admin_serviceprovider_list"));
        $breadcrumbs->addItem("Création");


        // Init form
        $serviceprovider = new ServiceProvider();
        $form = $this->createForm(ServiceProviderType::class, $serviceprovider, [
            'validation_groups' => ['Default']
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($serviceprovider->getTranslations()) > 0) {
                    $serviceproviderManager->save($serviceprovider);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Création terminée"
                    );

                    return $this->redirect($this->generateUrl('admin_serviceprovider_edit', array(
                        'id' => $serviceprovider->getId()
                    )));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            } else {

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'error',
                    "Le formulaire contient des erreurs ou est vide"
                );
            }
        }

        // View
        return $this->render(
            '@Admin/ServiceProvider/create.html.twig',
            array(
                'form'           => $form->createView(),
                'serviceprovider'           => $serviceprovider,
            )
        );
    }

    /**
     * Update a serviceprovider
     * @Route("/edit/{id}", name="admin_serviceprovider_edit")
     * @param Request $request Request
     * @param integer $id      Id of serviceprovider
     *
     * @return Response
     */
    public function editAction(Request $request, ServiceProviderManager $serviceproviderManager, LocaleManager $localeManager, $id)
    {
        // Find it
        /** @var ServiceProvider $serviceprovider */
        $serviceprovider = $serviceproviderManager->findOneToEdit($id);
        if (!$serviceprovider) {
            throw $this->createNotFoundException('ServiceProvider not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des fournisseur de servicess", $this->get("router")->generate("admin_serviceprovider_list"));
        $breadcrumbs->addItem("Modification");

        $form = $this->createForm(ServiceProviderType::class, $serviceprovider, [
            'validation_groups' => ['Default']
        ]);


        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($serviceprovider->getTranslations()) > 0) {
                    $serviceproviderManager->save($serviceprovider);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Mise à jour effectuée"
                    );

                    return $this->redirect($request->headers->get('referer'));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/ServiceProvider/edit.html.twig',
            array(
                'form'           => $form->createView(),
                'serviceprovider'           => $serviceprovider,
            )
        );
    }

    /**
     * Delete one serviceprovider
     * @Route("/delete/{id}", name="admin_serviceprovider_delete")
     * @param Request $request Request
     * @param integer $id      Id of serviceprovider
     */
    public function deleteAction(Request $request, ServiceProviderManager $serviceproviderManager, $id)
    {
        // Find it
        /** @var ServiceProvider $serviceprovider */
        $serviceprovider = $serviceproviderManager->findOneToEdit($id);
        if (!$serviceprovider) {
            throw $this->createNotFoundException('ServiceProvider not found');
        }

        $serviceproviderManager->remove($serviceprovider);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été supprimé"
        );

        return $this->redirect($request->headers->get('referer'));
    }



    /**
     * Duplicate one serviceprovider
     * @Route("/duplicate/{id}", name="admin_serviceprovider_duplicate")
     * @param Request $request Request
     * @param integer $id      Id of serviceprovider
     */
    public function duplicateAction(Request $request, ServiceProviderManager $serviceproviderManager, $id)
    {
        // Find it
        /** @var ServiceProvider $serviceprovider */
        $serviceprovider = $serviceproviderManager->findOneToEdit($id);
        if (!$serviceprovider) {
            throw $this->createNotFoundException('ServiceProvider not found');
        }

        $this->getDoctrine()->getManager()->detach($serviceprovider);
        foreach ($serviceprovider->getTranslations() as $translation) {
            $translation->setId(null);
            $translation->setSlug(null);
            $this->getDoctrine()->getManager()->detach($translation);
        }
        $serviceproviderManager->save($serviceprovider);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été dupliqué"
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Export csv
     * @Route("/export/{locale}", name="admin_serviceprovider_export")
     * @param Request $request
     */
    public function exportAction(Request $request, ServiceProviderManager $serviceproviderManager, $locale)
    {
        $data = $this->initSearch($request);
        $query = $serviceproviderManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Nom',
            'Date de création',

        ], ';');

        $locale = $request->get('locale', $request->getDefaultLocale());
        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->translate($locale)->getName(),
                ($result->translate($locale)->getCreated()) ? $result->translate($locale)->getCreated()->format('d/m/Y') : $result->getCreated()->format('d/m/Y'),

            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_'.$locale.'_serviceproviders_export.csv"'
        ));
    }
}
