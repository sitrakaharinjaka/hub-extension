<?php

namespace App\Command;

use App\Utils\Services\MailerService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractCommand extends Command
{
    protected $mailer;
    protected $logger;
    protected $ENVIRONNEMENT;
    protected $input;
    protected $output;

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger)
    {
        parent::__construct();
        $this->mailer = $mailer;
        $this->logger = $logger;
        $this->ENVIRONNEMENT = $parameterBag->get('server_mode');
    }

    final protected function log($level, $message)
    {
        if ($this->output) {
            $this->output->writeln($message);
        }
        if ($level == 'info') {
            $this->logger->info($message);
        } else {
            $this->logger->error($message);
        }
    }

    final protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;

        $now = new \DateTime('now', new \DateTimeZone(date_default_timezone_get()));
        $currentTime = $now->format('d/m/Y H:i:s');

        try {
            $this->log('info', $currentTime . ' - Lancement de la commande: ' . $this::$defaultName . ' (machine : ' .gethostname() . ')');
            $this->executeCommand();
            $this->log('info', $currentTime . ' - Commande terminée <info>sans erreur majeure</info>: ' . $this::$defaultName);
        } catch (\Exception $e) {
            $this->log('info', $currentTime . ' - Commande terminée <error>avec erreur(s)</error>: ' . $this::$defaultName);
            $this->log('info', '<error>Failure</error> : ' . $e->getMessage());
        }
    }

    abstract protected function executeCommand();
}
