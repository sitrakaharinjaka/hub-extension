CKEDITOR.dialog.add( 'quoteDialog', function( editor ) {
    return {
        title: 'Quote Properties',
        minWidth: 400,
        minHeight: 200,

        contents: [
            {
                id: 'tab-basic',
                label: 'Basic Settings',
                elements: [
                    {
		                type: 'text',
		                id: 'quote',
		                label: 'Quote',
		                validate: CKEDITOR.dialog.validate.notEmpty( "Title field cannot be empty." ),
		                setup: function( element ) {
                            this.setValue( element.getText() );
                        },

                        commit: function( element ) {
                            element.setText( this.getValue() );
                        }
		            },
                    {
                        type: 'text',
                        id: 'author',
                        label: 'Author',
                        setup: function( element ) {
                            this.setValue( element.getAttribute( "href" ) );
                        },
                        commit: function( element ) {
                            element.setAttribute( "href", this.getValue() );
                        }
                    }
                ]
            }
        ],

        onShow: function() {
            var selection = editor.getSelection();
            var element = selection.getStartElement();

            if ( element ) {
                element = element.getAscendant( 'blockquote', true );
            }
            

            if ( !element || element.getName() != 'blockquote' ) {
                element = editor.document.createElement( 'blockquote' );
                this.insertMode = true;
            }
            else {
                this.insertMode = false;
            }

            this.element = element;
            if ( !this.insertMode ) {
                this.setupContent( this.element );
            }
        },
        onOk: function() {
		    var dialog = this;
            var quote = this.element;
            quote.setAttribute('class', 'cta-btn');
            this.commitContent( quote );

            if ( this.insertMode ) {
                editor.insertElement( quote );
            }
		}
    };
});