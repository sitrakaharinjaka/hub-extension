<?php
// src/Manager/PasswordRequestManager.php
namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;

use App\Entity\PasswordRequest;
use Doctrine\ORM\EntityManagerInterface;

use Gaufrette\Filesystem;

class PasswordRequestManager extends BaseManager
{
    protected $em;
    protected $repository;

    /**
     * PasswordRequestManager constructor.
     *
     * @param EntityManagerInterface $em by dependency injection
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager = null)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('repository', PasswordRequest::class);
    }


    /**
     * @param $email
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByEmail($email)
    {
        return $this->repository->findOneByEmail($email);
    }


    /**
     * @param $token
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByToken($token)
    {
        return $this->repository->findOneByToken($token);
    }

    /**
     * Delete a passwordRequest object in bdd
     *
     * @param PasswordRequest $passwordRequest
     */
    public function delete(PasswordRequest $passwordRequest)
    {
        $this->em->remove($passwordRequest);
        $this->em->flush();
    }


    /**
     * Save a passwordRequest object in bdd
     *
     * @param PasswordRequest $passwordRequest
     */
    public function save(PasswordRequest $passwordRequest)
    {
        $this->em->persist($passwordRequest);
        $this->em->flush();
    }
}
