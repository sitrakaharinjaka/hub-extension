<?php

/**
 * Model for search
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Model;

/**
 * Class SearchProductStockLine
 *
 * @package App\Form\Model
 */
class SearchStockVariation
{
    /**
     * Product Id
     *
     * @var string
     */
    protected $productId;

    /**
     * productSku
     *
     * @var string
     */
    protected $productSku;

    /**
     * productSku
     *
     * @var string
     */
    protected $productName;

    /**
     * productSku
     *
     * @var string
     */
    protected $status = 'all';

    /**
     * Get search data
     *
     * @return array
     */
    public function getSearchData()
    {
        $tab = array();

        if (!empty($this->productId)) {
            $tab['p.id'] = $this->productId;
        }
        if (!empty($this->productName)) {
            $tab['pt.name'] = $this->productName;
        }
        if (!empty($this->productSku)) {
            $tab['pt.code'] = $this->productSku;
        }
        if (!empty($this->status) && $this->status != 'all') {
            if($this->status == 'valid')
            $tab['e.stockVariationValid'] = $this->status == 'valid'? true: false;

            switch ($this->status) {
                case 'valid':
                    $tab['e.stockVariationValid'] = true;
                    break;
                case 'not-valid':
                    $tab['e.stockVariationValid'] = false;
                    break;
                case 'challenged':
                    $tab['e.stockVariationChallenged'] = true;
                    break;
            }
        }
        return $tab;
    }


    /**
     * Get product Id
     *
     * @return  string
     */ 
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set product Id
     *
     * @param  string  $productId  Product Id
     *
     * @return  self
     */ 
    public function setProductId(string $productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productSku
     *
     * @return  string
     */ 
    public function getProductSku()
    {
        return $this->productSku;
    }

    /**
     * Set productSku
     *
     * @param  string  $productSku  productSku
     *
     * @return  self
     */ 
    public function setProductSku(string $productSku)
    {
        $this->productSku = $productSku;

        return $this;
    }

    /**
     * Get productSku
     *
     * @return  string
     */ 
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * Set productSku
     *
     * @param  string  $productName  productSku
     *
     * @return  self
     */ 
    public function setProductName(string $productName)
    {
        $this->productName = $productName;

        return $this;
    }

    /**
     * Get productSku
     *
     * @return  string
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set productSku
     *
     * @param  string  $status  productSku
     *
     * @return  self
     */ 
    public function setStatus(string $status)
    {
        $this->status = $status;

        return $this;
    }
}
