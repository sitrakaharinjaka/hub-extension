<?php

namespace App\Form\Model;

use App\Entity\StockEntryItem;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class BulkStockEntryValidation
{
    /**
     * @var Collection|StockEntryItem[]
     */
    protected $stockEntryItems;

    public function __construct()
    {
        $this->stockEntryItems       = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function addStockEntryItem(StockEntryItem $stockEntryItem)
    {
        $this->stockEntryItems->add($stockEntryItem);
    }

    /**
     * {@inheritdoc}
     */
    public function removeStockEntryItem(StockEntryItem $stockEntryItem)
    {
        $this->stockEntryItems->removeElement($stockEntryItem);
    }

    /**
     * {@inheritdoc}
     */
    public function getStockEntryItems()
    {
        return $this->stockEntryItems;
    }
}
