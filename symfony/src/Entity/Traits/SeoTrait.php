<?php

namespace App\Entity\Traits;

trait SeoTrait
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $seoMetaTitle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $seoMetaDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $seoMetaKeywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $ogTitle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $ogDescription;

    /**
     * Cover
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", cascade={"all"})
     * @ORM\JoinColumn(name="og_cover_id", referencedColumnName="id", onDelete="SET NULL")
     * @var Media
     */
    protected $ogCover;
    /**
     * @return mixed
     */
    public function getSeoMetaTitle()
    {
        return $this->seoMetaTitle;
    }

    /**
     * @param mixed $seoMetaTitle
     */
    public function setSeoMetaTitle($seoMetaTitle)
    {
        $this->seoMetaTitle = $seoMetaTitle;
    }

    /**
     * @return mixed
     */
    public function getSeoMetaDescription()
    {
        return $this->seoMetaDescription;
    }

    /**
     * @param mixed $seoMetaDescription
     */
    public function setSeoMetaDescription($seoMetaDescription)
    {
        $this->seoMetaDescription = $seoMetaDescription;
    }

    /**
     * @return mixed
     */
    public function getSeoMetaKeywords()
    {
        return $this->seoMetaKeywords;
    }

    /**
     * @param mixed $seoMetaKeywords
     */
    public function setSeoMetaKeywords($seoMetaKeywords)
    {
        $this->seoMetaKeywords = $seoMetaKeywords;
    }

    /**
     * @return mixed
     */
    public function getOgTitle()
    {
        return $this->ogTitle;
    }

    /**
     * @param mixed $ogTitle
     */
    public function setOgTitle($ogTitle)
    {
        $this->ogTitle = $ogTitle;
    }

    /**
     * @return mixed
     */
    public function getOgDescription()
    {
        return $this->ogDescription;
    }

    /**
     * @param mixed $ogDescription
     */
    public function setOgDescription($ogDescription)
    {
        $this->ogDescription = $ogDescription;
    }

    /**
     * @return Media
     */
    public function getOgCover()
    {
        return $this->ogCover;
    }

    /**
     * @param Media $ogCover
     */
    public function setOgCover($ogCover)
    {
        $this->ogCover = $ogCover;
    }
}
