<?php

namespace App\Utils\Managers;

use App\Entity\Project;
use App\Entity\ProjectItem;
use App\Repository\ProductRepository;
use App\Repository\ProjectRepository;
use App\Utils\Managers\BaseManager;

use App\Utils\Managers\LocaleManager;
use App\Entity\Product;
use App\Entity\ProductTranslation;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class ProductManager
 * `
 * Object manager of product
 *
 * @package Disko\PageBundle\Managers
 */
class ProductManager extends BaseManager
{

    /**
     * Repository
     *
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * ProductManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('productRepository', Product::class);
    }

    /**
     * Save a product
     *
     * @param Product $product
     */
    public function save(Product $product)
    {
        $product->setUpdated(new \DateTime('now', new \DateTimeZone(date_default_timezone_get())));
        $this->getEntityManager()->persist($product);
        $this->getEntityManager()->flush();

        return $product;
    }

    /**
     * Remove one product
     *
     * @param Product $product
     */
    public function remove(Product $product)
    {
        $this->getEntityManager()->remove($product);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one Active
     *
     * @param $slugWl
     * @param $locale
     * @return mixed
     */
    public function findOneActive($slugWl, $locale)
    {
        $product = $this->productRepository->findOneActive($slugWl, $locale);

        return $product;
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $product = $this->productRepository->findOneToEdit($id);

        return $product;
    }

    /**
     * Get all product
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->productRepository->queryForSearch($filters);
    }

    /**
     * Get all product
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearchWithTotal($filters = array(), $stockId = null, $cave = null)
    {
        return $this->productRepository->queryForSearchWithTotal($filters, $stockId, $cave);
    }

    /**
     * Get all product front
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearchInFront($stock, $cave = null, $name = null, $category = null, $project = null)
    {
        return $this->productRepository->queryForSearchInFront($stock, $cave, $name, $category, $project);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->productRepository->findAll();
    }

    /**
     * Find all with less than $level
     *
     * @param integer $level
     * @return mixed
     */
    public function findProductsToWarnFew($stock, $cave, $level)
    {
        return $this->productRepository->findProductsToWarnFew($stock, $cave, $level);
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->productRepository->findOneBy($filters);
    }


    /**
     * findByPrefixHubCoide
     *
     * @param array $filters
     * @return mixed
     */
    public function findByPrefixHubCode()
    {
        $tab = [];
        $results = $this->productRepository->findByPrefixHubCode();

        foreach ($results as $result) {
            $tab[] =  $result->translate('fr')->getCode();
        }

        return $tab;
    }


    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneTranslationBy($filters = array())
    {
        return $this->productRepository->findOneTranslationBy($filters);
    }

    /**
     * Find one by stock ean and state
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneTranslationByStockEanState($stock, $code, $state)
    {
        return $this->productRepository->findOneTranslationByStockEanState($stock, $code, $state);
    }

    public function countProductsInCategories($stock, $project)
    {
        return $this->productRepository->countProductsInCategories($stock, $project);
    }


    public function findBySlug($stock)
    {
        return $this->productRepository->findBySlug($stock);
    }

    public function getSimilars($stock, $product, $count = 3)
    {
        return $this->productRepository->getSimilars($stock, $product, $count);
    }

    public function getProductNoSynch($mode, $cave)
    {
        return $this->productRepository->getProductNoSynch($mode, $cave);
    }

    /**
     * Check if code exists
     *
     * @return mixed
     */
    public function checkExist($code)
    {
        return $this->productRepository->checkExist($code);
    }

    /**
     * Ceck Code and Name Correspondance validity
     *
     * @return mixed
     */
    public function checkCodeAndName($code, $name)
    {
        return $this->productRepository->checkCodeAndName($code, $name);
    }


    public function statsCreated($start, $end)
    {
        return $this->productRepository->statsCreated($start, $end);
    }

    public function statsCreatedHub($start, $end)
    {
        return $this->productRepository->statsCreatedHub($start, $end);
    }

    public function statsCreatedNoHub($start, $end)
    {
        return $this->productRepository->statsCreatedNoHub($start, $end);
    }

    public function countProductsByStock()
    {
        return $this->productRepository->countProductsByStock();
    }

    /**
     * @param array $projects
     * @return array
     */
    public function checkProductQuantity(array $projects)
    {
        $quantities = array();
        $errors     = array();

        foreach ($projects as $project) {
            foreach ($project->getItems() as $item) {
                if (!isset($quantities[$item->getProduct()->getId()])) {
                    $quantities[$item->getProduct()->getId()] = 0;
                }
                $quantities[$item->getProduct()->getId()] += $item->getQuantity();

                foreach ($quantities as $id => $qty) {
                    if ($qty > $item->getProduct()->getStock($project->getStock()->getId())) {
                        if ($item->getProduct()->getStock($project->getStock()->getId()) > 0) {
                            $errors [$item->getProduct()->getId()]= "Vous avez demandé " . $qty . " produits " . $item->getProduct()->translate('fr')->getCode() . " mais vous ne disposez pas des quantités suffisantes dans votre stock. Veuillez vérifier votre catalogue de produits et ressaisir votre commande";
                        } else {
                            $errors [$item->getProduct()->getId()]= "Il y a besoin de " . $qty . " produits " . $item->getProduct()->translate('fr')->getCode() . ", mais plus aucun ne sont disponibles";
                        }
                    }
                }
            }
        }

        return $errors;
    }
}
