<?php

namespace App\Entity;

use App\Entity\Traits\AuthorableTrait;
use App\Entity\Traits\GedmoTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

// Use For Algolia Or Api

/**
 * @ORM\Entity(repositoryClass="App\Repository\BillingCenterRepository")
 * @ORM\Table(name="dk_billingcenter_translation")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class BillingCenterTranslation implements NormalizableInterface
{
    use ORMBehaviors\Translatable\Translation;
    use GedmoTrait;

    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Name
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $name;

    /**
     * Slug
     *
     * @Gedmo\Slug(fields={"name"}, unique=false)
     * @ORM\Column(length=255, unique=false)
     */
    protected $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="billingCenterTranslation")
     * @var User
     */
    protected $contact;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $house;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $department;

    /**
     * @ORM\Column(length=255, nullable=true)
     */
    protected $company;

    /**
     * @ORM\Column(length=255, nullable=true)
     */
    protected $address1;

    /**
     * @ORM\Column(length=255, nullable=true)
     */
    protected $address2;

    /**
     * @ORM\Column(length=255, nullable=true)
     */
    protected $zipCode;

    /**
     * @ORM\Column(length=255, nullable=true)
     */
    protected $city;

    /**
       * Set id
       *
       * @param integer $id
       * @return BillingCenter
       */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BillingCenter
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return BillingCenter
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return ArrayCollection
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param ArrayCollection $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * @param mixed $house
     */
    public function setHouse($house): void
    {
        $this->house = $house;
    }

    /**
     * @return mixed
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param mixed $department
     */
    public function setDepartment($department): void
    {
        $this->department = $department;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company): void
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param mixed $address1
     */
    public function setAddress1($address1): void
    {
        $this->address1 = $address1;
    }

    /**
     * @return mixed
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param mixed $address2
     */
    public function setAddress2($address2): void
    {
        $this->address2 = $address2;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param mixed $zipCode
     */
    public function setZipCode($zipCode): void
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * Use to serialize object for Algolia or Api
     *
     * @param NormalizerInterface $normalizer
     * @param null $format
     * @param array $context
     * @return array|bool|float|int|string
     */
    public function normalize(NormalizerInterface $normalizer, $format = null, array $context = array())
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'locale' => $this->getLocale(),
            'slug' => $this->getSlug(),
            'translatable_id' => $this->getTranslatable()->getId(),
        ];
    }
}
