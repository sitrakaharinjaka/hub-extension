<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 * @ORM\Table(name="dk_media_translation")
 * @ORM\HasLifecycleCallbacks
 */
class MediaTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * Title
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $title;

    /**
     * Alt
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $alt;


    /**
     * Legend
     *
     * @ORM\Column(nullable=true)
     */
    protected $legend;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * @param mixed $alt
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
    }

    /**
     * @return mixed
     */
    public function getLegend()
    {
        return $this->legend;
    }

    /**
     * @param mixed $legend
     */
    public function setLegend($legend)
    {
        $this->legend = $legend;
    }
}
