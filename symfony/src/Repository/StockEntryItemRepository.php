<?php

namespace App\Repository;

use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Class StockEntryRepository
 */
class StockEntryItemRepository extends BaseRepository
{



    /**
     * ================= FRONT ===================
     */


    /**
     * ================= BACK ===================
     */
    public function queryForSearch(string $locale = 'fr', array $filters = []): Query
    {
        $qb = $this->createQueryBuilder('e')
            ->select(
                'e.id', 'e.status',
                'se.id as stockEntryId', 'se.created', 'se.username', 'se.deliveryDate',
                'st.name',
                'pt.code', 'pt.name as productName', 'pt.eanPart', 'pt.eanBox'
            )
            ->innerJoin('e.stockEntry', 'se')
            ->innerJoin('se.stock', 's')
            ->innerJoin('e.product', 'p')
            ->leftJoin('s.translations', 'st', Join::WITH, 'st.locale = :locale')
            ->leftJoin('p.translations', 'pt', Join::WITH, 'pt.locale = :locale')
            ->setParameter('locale', $locale)
            ->orderBy('se.created', 'DESC')
            ->addOrderBy('se.id', 'ASC')
        ;

        if (count($filters)) {
            foreach ($filters as $key => $filter) {
                switch ($key) {
                    case 'id':
                        $qb->andWhere('se.id = :id')
                            ->setParameter('id', $filter);
                        break;
                    case 'stock':
                        $qb->andWhere('LOWER(st.name) LIKE :name')
                            ->setParameter('name', '%' . $filter . '%');
                        break;
                    case 'requester':
                        $qb->andWhere('LOWER(se.username) LIKE :requester')
                            ->orWhere('LOWER(se.firstName) LIKE :requester')
                            ->orWhere('LOWER(se.lastName) LIKE :requester')
                            ->setParameter('requester', '%' . $filter . '%');
                        break;
                    case 'code':
                        $qb->andWhere('LOWER(pt.code) LIKE :code')
                            ->setParameter('code', '%' . $filter . '%');
                        break;
                    case 'status':
                        $qb->andWhere('LOWER(e.status) LIKE :status')
                            ->setParameter('status', '%' . $filter . '%');
                        break;
                    case 'createdAt':
                        $start = new \DateTime($filters['createdAt']->format('Y-m-d H:i:s'));
                        $end = new \DateTime($filters['createdAt']->format('Y-m-d H:i:s'));
                        $start->setTime(0,0);
                        $end->setTime(23,59);
                        $qb->andWhere('se.created >= :start')
                        ->andWhere('se.created <= :end')
                        ->setParameter('start',$start)
                        ->setParameter('end',$end);
                        break;
                    case 'delivery':
                        $qb->andWhere('se.deliveryDate = :delivery')
                            ->setParameter('delivery', $filters['delivery']);
                        break;
                }
            }
        }

        return $qb->getQuery();
    }
}
