<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\Services\MailerService;

class TestMailDiskoController extends AbstractController
{

    /**
     * @Route("/test/mail/disko", name="test_mail_disko")
     */
    public function index(MailerService $mailer)
    {
        $emails ="edson.galina@disko.fr,dorian@disko.fr";
        $mailer->sendMailTestCron($emails);

        return $this->render('test_mail_disko/index.html.twig', [
            'controller_name' => 'TestMailDiskoController',
        ]);
    }
}
