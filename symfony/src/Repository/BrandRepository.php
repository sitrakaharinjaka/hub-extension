<?php

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class BrandRepository
 */
class BrandRepository extends BaseRepository
{



    /**
     * ================= FRONT ===================
     */


    /**
     * ================= BACK ===================
     */


    /**
     * Find one by
     *
     * @param string $name
     * @return mixed
     */
    public function findOneByName($name)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et')
            ->leftJoin('e.translations', 'et')
            ->andWhere('et.name = :name')
            ->setParameter('name', strtolower($name))
            ->setMaxResults(1);

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et')
            ->leftJoin('e.translations', 'et')
            ->orderBy('et.name', 'asc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'et';
                if ($key == 'id') {
                    $alias = 'e';
                }
                $qb->andWhere($alias.'.'.$key.' LIKE :'.$key);
                $qb->setParameter($key, '%'.$filter.'%');
            }
        }

        $query = $qb->getQuery();

        return $query;
    }

    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et')
            ->leftJoin('e.translations', 'et')
            
            
            ->where('e.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }


    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findAllTranslations($brand)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->from('App\Entity\BrandTranslation', 't')
            ->select('t')
            ->andWhere('t.brand = :brand')
            ->setParameter('brand', $brand);

        $query = $qb->getQuery();
        return $query->getResult();
    }
}
