<?php

namespace App\Controller\Admin;

use App\Entity\Media;
use App\Form\Type\Media\MediaType;
use App\Utils\Managers\MediaManager;
use App\Utils\Services\SerializerService;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class MediaController
 *
 * @Route("/admin")
 * @package App\Controller
 */
class MediaController extends BaseController
{
    /**
     * @Route("/media-info/{id}", name="media_info")
     * @param Request $request
     * @param         $id
     *
     * @return JsonResponse|Response
     */
    public function info(Request $request, MediaManager $mediaManager, $id)
    {
        // Call media
        $media = $mediaManager->findOneBy(['id' => $id]);
        if (!$media) {
            throw $this->createNotFoundException();
        }

        return new JsonResponse(array(
            'id' => $media->getId(),
            'url' => $this->get('router')->generate('media_view', ['id' => $media->getId()], UrlGeneratorInterface::ABSOLUTE_PATH),
            'title' => $media->getTitle(),
            'legend' => $media->getLegend(),
            'extension' => $media->getExtension(),
            'mimetype' => $media->getMimetype(),
            'alt' => $media->getAlt(),
        ));
    }

    /**
     * @Route("/image-seo/{id}", name="image_seo")
     * @param Request $request
     * @param         $id
     *
     * @return JsonResponse|Response
     */
    public function seo(Request $request, MediaManager $mediaManager, $id)
    {
        // Call media
        $media = $mediaManager->findOneBy(['id' => $id]);
        if (!$media) {
            throw $this->createNotFoundException();
        }

        // Create form
        $form = $this->createForm(MediaType::class, $media);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $media = $mediaManager->save($media);

                return new JsonResponse(array(
                    'id' => $media->getId(),
                    'url' => $this->get('router')->generate('media_view', ['id' => $media->getId()], UrlGeneratorInterface::ABSOLUTE_PATH),
                    'title' => $media->getTitle(),
                    'legend' => $media->getLegend(),
                    'alt' => $media->getAlt(),
                ));
            }
        }

        return new Response($this->renderView('@Admin/Media/partials/mediaForm.html.twig', array(
            'form' => $form->createView(),
            'id' => $id
        )));
    }

    /**
     * @Route("/media/upload", name="admin_media_upload")
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request, MediaManager $mediaManager, SerializerService $serializerService)
    {
        $response = new Response();
        $response->setStatusCode(400);

        // Get File to upload
        $file = $request->files->get('file', null);
        if (!$file) {
            return $response;
        }

        $media = new Media();
        $media->setFileMedia($file);
        $media = $mediaManager->save($media);

        if ($media) {
            $response->setStatusCode(200);
            $serializer = $this->get('serializer');
            $tab = array(
                'id' => $media->getId(),
                'extension' => $media->getExtension(),
                'mimetype' => $media->getMimetype(),
                'legend' => $media->getLegend(),
                'url' => $this->get('router')->generate('media_view', ['id' => $media->getId()], UrlGeneratorInterface::ABSOLUTE_PATH),
                'title' => $media->getTitle(),
                'alt' => $media->getAlt(),
            );
            $datas = $serializerService->serializeObjectToJson($tab, 'json');
            $response->setContent($datas);
        }

        return $response;
    }


    /**
     * @Route("/media-delete/{id}", name="media_delete")
     * @param Request $request
     * @param         $id
     *
     * @return JsonResponse|Response
     */
    public function delete(Request $request, MediaManager $mediaManager, $id)
    {
        // Call media
        $media = $mediaManager->findOneBy(['id' => $id]);
        if (!$media) {
            throw $this->createNotFoundException();
        }

        $mediaManager->remove($media);

        return new Response();
    }



    /**
     * @Route("/media-add", name="media_add")
     * @param Request $request
     *
     * @return JsonResponse|Response
     */
    public function add(Request $request)
    {
        return new Response($this->renderView('@Admin/Media/partials/add.html.twig', array()));
    }
}
