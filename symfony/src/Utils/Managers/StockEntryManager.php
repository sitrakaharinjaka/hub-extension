<?php

namespace App\Utils\Managers;

use App\Entity\StockEntry;
use App\Entity\StockEntryItem;
use App\Form\Model\StockEntryModel;
use App\Repository\StockEntryRepository;
use App\Utils\Managers\LocaleManager;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class StockManager
 * `
 * Object manager of stock
 *
 * @package Disko\PageBundle\Managers
 */
class StockEntryManager extends BaseManager
{
    private $productManager;
    protected $stockEntryRepository;

    /**
     * StockManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager, ProductManager $productManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('stockEntryRepository', StockEntry::class);
        $this->productManager = $productManager;
    }

    /**
     * Save a stock entry
     *
     * @param StockEntry $stockEntry
     */
    public function save(StockEntry $stockEntry)
    {
        $stockEntry->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($stockEntry);
        $this->getEntityManager()->flush();
        return $stockEntry;
    }

    /**
     * Remove one stock entry
     *
     * @param StockEntry $stockEntry
     */
    public function remove(StockEntry $stockEntry)
    {
        $this->getEntityManager()->remove($stockEntry);
        $this->getEntityManager()->flush();
    }

    public function createFromModel(StockEntryModel $stockEntryModel, $user)
    {
        $stockEntry = new StockEntry();
        $stockEntry->setUserName($stockEntryModel->getUsername());
        $stockEntry->setStock($stockEntryModel->getStock());
        $stockEntry->setEmail($stockEntryModel->getEmail());
        $stockEntry->setPhone($stockEntryModel->getPhone());
        $stockEntry->setCarrier($stockEntryModel->getCarrier());
        $stockEntry->setBillingCenter($stockEntryModel->getBillingCenter());
        $stockEntry->setDeliveryDate($stockEntryModel->getDeliveryDate());
        $stockEntry->setTimeSlots($stockEntryModel->getTimeSlots());
        $stockEntry->setUser($user);
        $stockEntry->setDateSendToVeolog(null);
        foreach ($stockEntryModel->getStockEntryItems() as $itemModel) {
            $stockEntryItem = $this->createItemFromModel($itemModel);
            $stockEntryItem->setStockEntry($stockEntry);
            $stockEntry->addStockEntryItem($stockEntryItem);
        }
        return $stockEntry;
    }

    protected function createItemFromModel($model)
    {
        $stockEntryItem = new StockEntryItem();
        $stockEntryItem->setProduct($this->productManager->findOneBy(['id' => $model->getProductId()]));
        $stockEntryItem->setStockEntry(null);
        $stockEntryItem->setQuantity($model->getQuantity());
        $stockEntryItem->setQuantityReceived(0);
        $stockEntryItem->setStatus(StockEntry::STATE_ITEM_CREATE);
        $stockEntryItem->setProductState($model->getProductState());
        $stockEntryItem->setWashBasins($model->getWashBasins());
        $stockEntryItem->setDryCleaning($model->getDryCleaning());
        $stockEntryItem->setWashGlasses($model->getWashGlasses());
        $stockEntryItem->setInvoiceFile($model->getInvoiceFilename());
        return $stockEntryItem;
    }

    public function findOneToUpdate($id)
    {
        return $this->stockEntryRepository->findOneToUpdate($id);
    }

    public function findOneToConfirm($id)
    {
        return $this->stockEntryRepository->findOneToConfirm($id);
    }

    public function findAllToSynch($since = null)
    {
        return $this->stockEntryRepository->findAllToSynch($since);
    }

    public function findNonArchivedStock($stock)
    {
        return $this->stockEntryRepository->findNonArchivedStock();
    }

    public function findQuantityReceived($product, $date, $stock = null)
    {
        return $this->stockEntryRepository->findQuantityReceived($product, $date, $stock);
    }

    /**
     * Find by
     *
     * @param  array  $filters
     *
     * @return mixed
     */
    public function findBy($filters = array())
    {
        return $this->stockEntryRepository->findBy($filters);
    }
}
