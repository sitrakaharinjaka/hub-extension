var gulp = require('gulp');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var livereload = require('gulp-livereload');
var env = process.env.env;

var appRootPath = 'public/assets/admin/';

var paths = {
    app: {
        js: [
            'assets/admin/js/vendor/jquery.min.js',
            'assets/admin/js/vendor/bootstrap.min.js',
            'assets/admin/js/vendor/pixeladmin.min.js',
            'assets/admin/js/vendor/jquery.mCustomScrollbar.concat.min.js',
            'assets/admin/js/app.js',
            'assets/admin/js/disko.js',
            'assets/admin/js/collection-form.js',
            'assets/admin/js/vendor/jquery-ui/jquery-ui.min.js'
        ],
        easteregg: [
            'assets/admin/js/easteregg.js'
        ],
        city: [
            'assets/admin/js/city.js'
        ],
        css: [
            'assets/admin/js/vendor/ckeditor/contents.css',
            'assets/admin/css/vendor/bootstrap.min.css',
            'assets/admin/css/vendor/flag-icon.min.css',
            'assets/admin/css/vendor/pixeladmin.min.css',
            'assets/admin/css/vendor/widgets.min.css',
            'assets/admin/css/vendor/themes/clean.min.css',
            'assets/admin/css/disko.css',
            'assets/admin/css/mCustomScrollbar.css',
            'assets/admin/css/pagebuilder.css'
        ],
        img: [
            'assets/admin/images/**'
        ],
        ckeditor: [
            'assets/admin/js/vendor/ckeditor/**'
        ],
        flags: [
            'assets/admin/flags/**'
        ]
    }
};

gulp.task('app-js-city', function () {
    return gulp.src(paths.app.easteregg)
        .pipe(concat('city.js'))
        .pipe(gulpif(env === 'prod', uglify()))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(appRootPath + 'js/'))
        ;
});

gulp.task('app-js-easteregg', function () {
    return gulp.src(paths.app.easteregg)
        .pipe(concat('easteregg.js'))
        .pipe(gulpif(env === 'prod', uglify()))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(appRootPath + 'js/'))
        ;
});

gulp.task('app-js', function () {
    return gulp.src(paths.app.js)
        .pipe(concat('app.js'))
        .pipe(gulpif(env === 'prod', uglify()))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(appRootPath + 'js/'))
        ;
});


gulp.task('app-css', function() {
    return gulp.src(paths.app.css)
        .pipe(concat('style.css'))
        .pipe(gulpif(env === 'prod', uglifycss()))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(appRootPath + 'css/'))
        ;
});


gulp.task('app-img', function() {
    return gulp.src(paths.app.img)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(appRootPath + 'images/'))
        ;
});

gulp.task('app-ckeditor', function() {
    return gulp.src(paths.app.ckeditor)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(appRootPath + 'ckeditor/'))
        ;
});

gulp.task('app-flags', function() {
    return gulp.src(paths.app.flags)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(appRootPath + 'flags/'))
        ;
});


gulp.task('app-watch', function() {
    livereload.listen();

    gulp.watch(paths.app.js, ['app-js']);
    gulp.watch(paths.app.easteregg, ['app-js-easteregg'])
    gulp.watch(paths.app.easteregg, ['app-js-city']);
    gulp.watch(paths.app.css, ['app-css']);
    gulp.watch(paths.app.img, ['app-img']);
    gulp.watch(paths.app.ckeditor, ['app-ckeditor']);
    gulp.watch(paths.app.flags, ['app-flags']);
});

gulp.task('build', ['app-js', 'app-js-easteregg', 'app-js-city','app-css', 'app-img', 'app-ckeditor', 'app-flags']);
gulp.task('watch', ['build', 'app-watch']);