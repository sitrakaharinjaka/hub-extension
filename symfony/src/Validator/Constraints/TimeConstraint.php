<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class TimeConstraint extends Constraint
{
    public $message = 'La date de retour doit être supérieur à la date de livraison';

    public function validatedBy()
    {
        return 'time_constraint';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
