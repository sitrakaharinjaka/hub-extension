<?php
/**
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote 
 */
$metadata['https://ppd-saml.moet-hennessy.net/adfs/services/trust'] = array (
    'entityid' => 'https://ppd-saml.moet-hennessy.net/adfs/services/trust',
    'contacts' =>
        array (
            0 =>
                array (
                    'contactType' => 'support',
                ),
        ),
    'metadata-set' => 'saml20-idp-remote',
    'SingleSignOnService' =>
        array (
            0 =>
                array (
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                    'Location' => 'https://ppd-saml.moet-hennessy.net/adfs/ls/',
                ),
            1 =>
                array (
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                    'Location' => 'https://ppd-saml.moet-hennessy.net/adfs/ls/',
                ),
        ),
    'SingleLogoutService' =>
        array (
            0 =>
                array (
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                    'Location' => 'https://ppd-saml.moet-hennessy.net/adfs/ls/',
                ),
            1 =>
                array (
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                    'Location' => 'https://ppd-saml.moet-hennessy.net/adfs/ls/',
                ),
        ),
    'ArtifactResolutionService' =>
        array (
        ),
    'NameIDFormats' =>
        array (
            0 => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
            1 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
            2 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
        ),
    'keys' =>
        array (
            0 =>
                array (
                    'encryption' => true,
                    'signing' => false,
                    'type' => 'X509Certificate',
                    'X509Certificate' => 'MIIHpzCCBo+gAwIBAgIKFxM4JAAAAAAS2zANBgkqhkiG9w0BAQUFADBcMRQwEgYKCZImiZPyLGQBGRYEbHZtaDESMBAGCgmSJomT8ixkARkWAm1zMRIwEAYKCZImiZPyLGQBGRYCd3MxHDAaBgNVBAMTE1dTIHN1Ym9yZGluYXRlIDIgQ0EwHhcNMTYwNDE4MDgxNTU1WhcNMjEwNDE3MDgxNTU1WjB9MQswCQYDVQQGEwJGUjEOMAwGA1UECBMFTWFybmUxEDAOBgNVBAcTB0VwZXJuYXkxDTALBgNVBAoTBE1IQ1MxDTALBgNVBAsTBE1ISVMxLjAsBgNVBAMTJUFERlMgRW5jcnlwdGlvbi1wcGQtbW9ldC1oZW5uZXNzeS5uZXQwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQC6zrDiockMJnL/PXqfkkdV/0lWU8Uxgs96J/FmNFGNJ1TjBDoJAu8W74LL8Snd1Ra1G+CpH1LuP+2rNMp8av01VJX1YMX1vuusH+TxFe4n5aEWQeYUEr6hkl4wFcULufvOjSTKEe2cqFeEWD9PcQikbVFCdBvPcYH/Bt1YwXwjDUAu9ztGfnK6Bm1YYpQM/rLJ3qhG0ieZUwvoMRGE35ROLPpd0ZZXOA0rMNDVHvr6h8Jp0sMq6X3In2qz83a90pToA4oHkNwDlLTOV4J8PxgW4S3NYjsYjRv0Tl8tnTQwfd3qslxCpVGO99CyZMO8OcS/5hbVtwXtNrZ/7ZWGvoPFUoTGOZx0zPt/nIw9vJFDYGhvWm7JFSmM+xaA4PCsxRE6GB+RWX/sdKKeG20dRsmP2VtqRNZKQl716y10F6mgt7yjDSGfDAQGp70/LM5yo9Iqzhr13bKpv3oCVTHl1SawDzxzx+gKlWBwWSq6KjeZjTUYlvqRr6sW3vgCofIxDDG3KkMsC71n2uq2S8LZ30sSNdDJzDkUjHEOP8/R7Qs767UlWBbfIwNRkeIm/NDVF/mLWdO+LAfhhUbWinVA5H+obgDQV9LHTvwyTLCJwdY3Y/Lj58HEarJn/UNb10HtlwVfdnUkABmMA2wbf7wkZ06BfjaI/2stU168pXI0hlwgzQIDAQABo4IDSDCCA0QwCwYDVR0PBAQDAgWgMD0GCSsGAQQBgjcVBwQwMC4GJisGAQQBgjcVCIP1ry+BpeQahKmfIYOuswHYij+BBoSSh3iHjro3AgFkAgENMB0GA1UdDgQWBBSlGl5DDxPe8GlqR1rWNX0cuQUc5jAfBgNVHSMEGDAWgBT/fs1Yze1VRKg/ZVadBNNHL9fe2zCCARoGA1UdHwSCAREwggENMIIBCaCCAQWgggEBhoHFbGRhcDovLy9DTj1XUyUyMHN1Ym9yZGluYXRlJTIwMiUyMENBLENOPU1ISVMzODlDQUQsQ049Q0RQLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENOPUNvbmZpZ3VyYXRpb24sREM9d3MsREM9bXMsREM9bHZtaD9jZXJ0aWZpY2F0ZVJldm9jYXRpb25MaXN0P2Jhc2U/b2JqZWN0Q2xhc3M9Y1JMRGlzdHJpYnV0aW9uUG9pbnSGN2h0dHA6Ly9jcmwud3MubXMubHZtaC9jYWQvV1MlMjBzdWJvcmRpbmF0ZSUyMDIlMjBDQS5jcmwwggEqBggrBgEFBQcBAQSCARwwggEYMIG6BggrBgEFBQcwAoaBrWxkYXA6Ly8vQ049V1MlMjBzdWJvcmRpbmF0ZSUyMDIlMjBDQSxDTj1BSUEsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2VydmljZXMsQ049Q29uZmlndXJhdGlvbixEQz13cyxEQz1tcyxEQz1sdm1oP2NBQ2VydGlmaWNhdGU/YmFzZT9vYmplY3RDbGFzcz1jZXJ0aWZpY2F0aW9uQXV0aG9yaXR5MFkGCCsGAQUFBzAChk1odHRwOi8vY3JsLndzLm1zLmx2bWgvY2FkL01ISVMzODlDQUQud3MubXMubHZtaF9XUyUyMHN1Ym9yZGluYXRlJTIwMiUyMENBLmNydDATBgNVHSUEDDAKBggrBgEFBQcDATAbBgkrBgEEAYI3FQoEDjAMMAoGCCsGAQUFBwMBMDgGA1UdEQQxMC+CDU1ISVNBREZTUFBEMDGCHk1ISVNBREZTUFBEMDEud3Mtd3cud3MubXMubHZtaDANBgkqhkiG9w0BAQUFAAOCAQEAhbaPiiGgs3xENxtSnBPEHME42K4Lj1RuTce1gHvRpkvx4KAbQvWssNkx3mSOrS8Gt2jc1vpqcBkVD0cfxSKdZIWdq1/ptmTzmjYJLIecIo/K8+zI3OK1KR122D4gpyirMWDDbw6JyIhMHGMLUeD/mN7PBNnBWwqhWrT0x7L7nw/VnOUkQ0XlQPdsmGJATj2I8dDJwOlj8rR1WzlUqg4ImzlyEYHuOuggMsibRJJswjiKgvGkqiCOcLffz18BnJ4icpHJzuQQXqkpnmi9Nfr5Aa5ObZmS63GpDWWa8Jjs2/TNC23yC+0D08nQ3yc1JjxazqH1VXbZA2+mncRK9oTPaw==',
                ),
            1 =>
                array (
                    'encryption' => false,
                    'signing' => true,
                    'type' => 'X509Certificate',
                    'X509Certificate' => 'MIIHpDCCBoygAwIBAgIKFxRWPwAAAAAS3DANBgkqhkiG9w0BAQUFADBcMRQwEgYKCZImiZPyLGQBGRYEbHZtaDESMBAGCgmSJomT8ixkARkWAm1zMRIwEAYKCZImiZPyLGQBGRYCd3MxHDAaBgNVBAMTE1dTIHN1Ym9yZGluYXRlIDIgQ0EwHhcNMTYwNDE4MDgxNzA5WhcNMjEwNDE3MDgxNzA5WjB6MQswCQYDVQQGEwJGUjEOMAwGA1UECBMFTWFybmUxEDAOBgNVBAcTB0VwZXJuYXkxDTALBgNVBAoTBE1IQ1MxDTALBgNVBAsTBE1ISVMxKzApBgNVBAMTIkFERlMgU2lnbmluZy1wcGQtbW9ldC1oZW5uZXNzeS5uZXQwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQCWtiXaMl1yITOlJYNDCDDIbyjaiw0FUwtsSrGo98EpeGd0x9nNlue/wzkdJvDb0bY1Dj+0y4XNE8mH0OGF5RnW6nGHbTKz9eYKMPLQ4FMcVxWNIEmDYCiCatRJ0XvPX+YKi8zy9tvVjedl/wUcdNnxBRSe8/YdIZ0VSgyL4BankIewqYJu/8tNMAzz3xQDMgqvQmVfiXd+PmcAnDWHFSfxsSynH9ZFTD4puyOWgUrpPoAwkBWP1UDiqB8jfP9iZMOlAgxyl7XaO+HKYhAR7SdoxuFFIYX/1WcdKnuleL2XuyGEJ7zjYRcD5Xx3fRcxKjmqhkB73NaDTWkxCt0L0s5mWP80E49eTY74ImJW9xCrNwvSgEQBpR9feFpX/uPphJOhAyhKXx34SWs5zOEwyabsfgHUFgKBuvkCDJe1wk09QaTDWa3m5u7PqeMzZyFeyRIBTt1r85B59UfnP6ozyhf69wOjEvVaFA0DH+NApn/lehsHosszRducQP+VFgg9KmGbLCfCpBG4rkWqUIgIASR4CZkUqG2K6tKqTBzHn22AB2r/yljJhHzCNKZKsC/EqXPYqO4a3GYO3phr92vsxHpOERBg+Jhfgb6Ap4E/2t5FjW72yM+2OVi6ADTA7BpzWRWernQmMsnNDp6hqV2YAmJRUGmLTszanu5Y97NvcFYRnQIDAQABo4IDSDCCA0QwCwYDVR0PBAQDAgWgMD0GCSsGAQQBgjcVBwQwMC4GJisGAQQBgjcVCIP1ry+BpeQahKmfIYOuswHYij+BBoSSh3iHjro3AgFkAgENMB0GA1UdDgQWBBQ9SpR9kc1W99/QmOpXTCOdWIMPyDAfBgNVHSMEGDAWgBT/fs1Yze1VRKg/ZVadBNNHL9fe2zCCARoGA1UdHwSCAREwggENMIIBCaCCAQWgggEBhoHFbGRhcDovLy9DTj1XUyUyMHN1Ym9yZGluYXRlJTIwMiUyMENBLENOPU1ISVMzODlDQUQsQ049Q0RQLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENOPUNvbmZpZ3VyYXRpb24sREM9d3MsREM9bXMsREM9bHZtaD9jZXJ0aWZpY2F0ZVJldm9jYXRpb25MaXN0P2Jhc2U/b2JqZWN0Q2xhc3M9Y1JMRGlzdHJpYnV0aW9uUG9pbnSGN2h0dHA6Ly9jcmwud3MubXMubHZtaC9jYWQvV1MlMjBzdWJvcmRpbmF0ZSUyMDIlMjBDQS5jcmwwggEqBggrBgEFBQcBAQSCARwwggEYMIG6BggrBgEFBQcwAoaBrWxkYXA6Ly8vQ049V1MlMjBzdWJvcmRpbmF0ZSUyMDIlMjBDQSxDTj1BSUEsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2VydmljZXMsQ049Q29uZmlndXJhdGlvbixEQz13cyxEQz1tcyxEQz1sdm1oP2NBQ2VydGlmaWNhdGU/YmFzZT9vYmplY3RDbGFzcz1jZXJ0aWZpY2F0aW9uQXV0aG9yaXR5MFkGCCsGAQUFBzAChk1odHRwOi8vY3JsLndzLm1zLmx2bWgvY2FkL01ISVMzODlDQUQud3MubXMubHZtaF9XUyUyMHN1Ym9yZGluYXRlJTIwMiUyMENBLmNydDATBgNVHSUEDDAKBggrBgEFBQcDATAbBgkrBgEEAYI3FQoEDjAMMAoGCCsGAQUFBwMBMDgGA1UdEQQxMC+CDU1ISVNBREZTUFBEMDGCHk1ISVNBREZTUFBEMDEud3Mtd3cud3MubXMubHZtaDANBgkqhkiG9w0BAQUFAAOCAQEAknqqFZwSJlF5Q5QTmurErvZQUxn7DKUYt6i76K7vvoncMgUtqWINTjz1ezB9JyknH3Jk3nr5eZVsz9FaItW50srUfGgcJpUGufOrFairUMb+cQ58RF0fXeHnmSPSdN3cGo2y8HPEg2m95PImiwx/lL8IaALQki9zF9oSvT7IItV18BxnQUf+3uArZU+o0CVUEkfKN2lXEmV050fJbTxo5oxiRaKNhgjeZXqUx7GLM+SyqtI8YrMaCTtJKiC/Ywetlh0pVci6GJRbX/rDHvPlSbaVB9JnMb6iL+71swOPY1buxZPcncHniT68W5DZZTLqX0DD4M+BuUq509KSObxm3A==',
                ),
        ),
);

