<?php
/**
 * Created by PhpStorm.
 * User: Luk
 * Date: 20/05/2017
 * Time: 20:35
 */

namespace App\Form\Type\MyBoxes;

use App\Entity\Media;
use App\Entity\MyBoxesBoxMedia;
use App\Entity\MyBoxesProduct;
use App\Form\Transformer\MediaTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BoxMediaType extends AbstractType
{
    protected $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            $builder->create('cover', FormType::class, [
                'required' => false,
                'data_class' => Media::class,
                'by_reference' => false,
                'label' => "Photo",
                'attr' => [
                    'class' => 'input-upload-id',
                ],
            ])
            ->add('id', TextType::class, ['required' => false])
            ->addModelTransformer(new MediaTransformer($this->em))
        );
    }

    /**
     * Configure Options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => MyBoxesBoxMedia::class,
            'validation_groups' => 'cart'
        ));
    }
}
