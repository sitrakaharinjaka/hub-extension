<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BillingCenterRepository")
 * @ORM\Table(name="dk_billingcenter")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class BillingCenter
{
    use GedmoTrait;
    use AuthorableTrait;
    use ORMBehaviors\Translatable\Translatable;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Collection|Project[]
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="billingCenter")
     */
    protected $projects;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Stock", mappedBy="billingCenters", cascade={"persist"})
     * @var Stock
     */
    protected $stocks;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
        $this->stocks = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * {@inheritdoc}
     */
    public function addProject(Project $project)
    {
        if ($this->hasProject($project)) {
            return;
        }

        $this->projects->add($project);
        $project->setUser($this);
    }

    /**
     * {@inheritdoc}
     */
    public function removeProject(Project $project)
    {
        if ($this->hasProject($project)) {
            $this->projects->removeElement($project);
            $project->setUser(null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * {@inheritdoc}
     */
    public function clearProjects()
    {
        $this->projects->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function hasProject(Project $project)
    {
        return $this->projects->contains($project);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->translate('fr')->getName();
    }

    /**
     * @return ArrayCollection
     */
    public function getStocks()
    {
        return $this->stocks;
    }

    /**
     * @param $stocks
     */
    public function setStocks($stocks)
    {
        $this->stocks = $stocks;
    }

    /**
     * @param Stock $stock
     * @return $this|void
     */
    public function addStock(Stock $stock)
    {
        if ($this->stocks->contains($stock)) {
            return;
        }
        $this->stocks->add($stock);
        $stock->addBillingCenter($this);
    }

    /**
     * @param Stock $stock
     */
    public function removeStock(Stock $stock)
    {
        if (!$this->stocks->contains($stock)) {
            return;
        }
        $this->stocks->removeElement($stock);
        $stock->removeBillingCenter($this);
    }
}
