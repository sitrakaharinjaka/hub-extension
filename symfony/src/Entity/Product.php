<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(name="dk_product")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Product
{
    use GedmoTrait;
    use AuthorableTrait;
    use ORMBehaviors\Translatable\Translatable;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Collection|Project[]
     * @ORM\OneToMany(targetEntity="App\Entity\StockLine", mappedBy="product", cascade={"all"})
     */
    protected $stockLines;

    /**
     * @var \DateTimeInterface|null $dateSynch
     * @ORM\Column(name="date_synch", type="date", nullable=true)
     */
    public $dateSynch;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectRejection", mappedBy="product")
     */
    private $projectRejections;

    public function __construct()
    {
        $this->stockLines = new ArrayCollection();
        $this->projectRejections = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * {@inheritdoc}
     */
    public function addStockLine(StockLine $stockLine)
    {
        if ($this->hasStockLine($stockLine)) {
            return;
        }

        $this->stockLines->add($stockLine);
    }

    /**
     * {@inheritdoc}
     */
    public function removeStockLine(StockLine $stockLine)
    {
        if ($this->hasStockLine($stockLine)) {
            $this->stockLines->removeElement($stockLine);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getStockLines()
    {
        return $this->stockLines;
    }

    /**
     * {@inheritdoc}
     */
    public function clearStockLines()
    {
        $this->stockLines->clear();
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateSynch(): ?\DateTimeInterface
    {
        return $this->dateSynch;
    }

    /**
     * @param \DateTimeInterface|null $dateSynch
     */
    public function setDateSynch($dateSynch)
    {
        $this->dateSynch = $dateSynch;
    }

    /**
     * {@inheritdoc}
     */
    public function hasStockLine(StockLine $stockLine)
    {
        return $this->stockLines->contains($stockLine);
    }

    /**
     * @return array
     */
    public function getImages() : array
    {
        $allImages = [];
        if ($cover = $this->translate('fr')->getCover()) {
            array_push($allImages, $cover);
        }
        $imagesGallery = $this->translate('fr')->getImageGallery();
        if ($imagesGallery) {
            foreach ($imagesGallery as $imageGallery) {
                array_push($allImages, $imageGallery->getImage());
            }
        }
        return $allImages;
    }

    public function getStock($stockId)
    {
        $counter = 0;
        foreach ($this->stockLines as $stockLine) {
            if (!$stockLine->getArchived() && $stockLine->getStock()->getId() == $stockId) {
                $counter+= $stockLine->getNumber();
            }
        }
        return $counter;
    }

    /**
     * @return Collection|ProjectRejection[]
     */
    public function getProjectRejections(): Collection
    {
        return $this->projectRejections;
    }

    public function addProjectRejection(ProjectRejection $projectRejection): self
    {
        if (!$this->projectRejections->contains($projectRejection)) {
            $this->projectRejections[] = $projectRejection;
            $projectRejection->setProduct($this);
        }

        return $this;
    }

    public function removeProjectRejection(ProjectRejection $projectRejection): self
    {
        if ($this->projectRejections->contains($projectRejection)) {
            $this->projectRejections->removeElement($projectRejection);
            // set the owning side to null (unless already changed)
            if ($projectRejection->getProduct() === $this) {
                $projectRejection->setProduct(null);
            }
        }

        return $this;
    }
}
