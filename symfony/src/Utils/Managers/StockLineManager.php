<?php

namespace App\Utils\Managers;

use App\Entity\Project;
use App\Repository\StockLineRepository;
use App\Utils\Managers\BaseManager;

use App\Utils\Managers\LocaleManager;
use App\Entity\StockLine;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;
use App\Utils\Services\MailerService;

/**
 * Class StockLineManager
 * `
 * Object manager of stockLine
 *
 * @package Disko\PageBundle\Managers
 */
class StockLineManager extends BaseManager
{

    /**
     * Repository
     *
     * @var StockLineRepository
     */
    protected $stockLineRepository;

    /**
     * Mailer service
     * 
     * @var MailerService
     */
    protected $mailerService;

    /**
     * StockLineManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager, MailerService $mailerService)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('stockLineRepository', StockLine::class);
        $this->mailerService = $mailerService;
    }

    /**
     * Save a stockLine
     *
     * @param StockLine $stockLine
     */
    public function save(StockLine $stockLine)
    {
        $stockLine->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($stockLine);
        $this->getEntityManager()->flush();

        return $stockLine;
    }

    /**
     * Remove one stockLine
     *
     * @param StockLine $stockLine
     */
    public function remove(StockLine $stockLine)
    {
        $this->getEntityManager()->remove($stockLine);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $stockLine = $this->stockLineRepository->findOneToEdit($id);

        return $stockLine;
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->stockLineRepository->findAll();
    }

    /**
     * Finds entities by a set of criteria.
     *
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int|null   $limit
     * @param int|null   $offset
     *
     * @return array The objects.
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->stockLineRepository->findBy($criteria, $orderBy, $limit, $offset );
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->stockLineRepository->findOneBy($filters);
    }

    /**
     * Find all by stock and product
     * @param $stock
     */
    public function findByStockAndProduct($stock, $cave, $productId)
    {
        return $this->stockLineRepository->findByStockAndProduct($stock, $cave, $productId);
    }

    /**
     * Query for search and pagination
     */
    public function queryForSearch($filters = array(), $interval = null)
    {
        return $this->stockLineRepository->queryForSearch($filters, $interval);
    }

    /**
     * Update all by stock and product
     * @param Stock $stock
     * @param Product $product
     * @param string $cave
     * @param bool $onlyVariations
     * @param array $exceptions
     * 
     */
    public function updateByStockAndProduct($stock, $product, $cave = 'pantin', $onlyVariations = false, $exceptions = [])
    {
        return $this->stockLineRepository->updateByStockAndProduct($stock, $product, $cave, $onlyVariations, $exceptions);
    }

    public function isVariationValidatable($idStockLine)
    {
        $stockLine = $this->stockLineRepository->find($idStockLine);

        if(!$stockLine->getStockVariation()){
            return true;
        }

        if($stockLine instanceof StockLine){
            $stockLinesByProduct = $this->stockLineRepository->findBy(
                [
                    'product' => $stockLine->getProduct(),
                    'stock' => $stockLine->getStock(),
                    'stockVariation' => true
                ],
                [
                    'created' => 'DESC'
                ],
                1
            );

            if(is_array($stockLinesByProduct) && isset($stockLinesByProduct[0])){
                return $stockLinesByProduct[0]->getId() == $stockLine->getId()? true: false;
            }

            return false;
        }

        return false;
    }

    /**
     * Validate a stock variation
     */
    public function validateStockVariation(StockLine $stockLine, $user)
    {
        if ($this->isVariationValidatable($stockLine->getId())) {
            $stockLine->setStockVariationValid(1);
            $stockLine->setStockVariationValidDate(new \DateTime());
            $stockLine->setStockVariationChallenged(0);
            $stockLine->setArchived(false);
            $stockLine->setUpdatedBy($user->getUsername());
            $this->save($stockLine);
        }
    }

    /**
     * Challenge a stock variation
     */
    public function challengeStockVariation(StockLine $stockLine, $user)
    {
        if(!$stockLine->getStockVariationChallenged()){
            $stockLine->setStockVariationValid(0);
            $stockLine->setStockVariationChallenged(1);
            $stockLine->setStockVariationChallengedDate(new \DateTime());
            $stockLine->setArchived(true);
            $stockLine->setUpdatedBy($user->getUsername());
            $this->save($stockLine);
            $this->mailerService->sendChallengeSynchStockErrors($stockLine);
        }
    }

    /**
     * Total on stock
     *
     * @param $stock
     * @param $productId
     * @return mixed
     */
    public function totalStockOnProduct($stock, $cave, $productId, $hold = false)
    {
        return $this->stockLineRepository->totalStockOnProduct($stock, $cave, $productId, $hold);
    }


    /**
     * @param $stock
     */
    public function totalStockDateOnProduct($stock, $cave, $productIds, $dateStart, $dateEnd, $hold = false)
    {
        return $this->stockLineRepository->totalStockDateOnProduct($stock, $cave, $productIds, $dateStart, $dateEnd, $hold);
    }

    /**
     * @param $stock
     */
    public function totalStockOnProducts($stock, $cave, $productIds, $dateStart, $dateEnd, $hold = false)
    {
        return $this->stockLineRepository->totalStockOnProducts($stock, $cave, $productIds, $dateStart, $dateEnd, $hold);
    }

    /**
     * @param $stock
     */
    public function quantityOf($product, $cave, $date, $stock = null, $realStock = true)
    {
        return $this->stockLineRepository->quantityOf($product, $cave, $date, $stock, $realStock);
    }


    /**
     * Find all by stock and products
     * @param $stock
     */
    public function addLine($stock, $product, $label, $operation, $date, $cave, $project = null, $from = "admin", $hold = false, $isVariation = false, $force = false)
    {
        if (!in_array($cave, [Project::CAVE_PANTIN, Project::CAVE_115])) {
            return false;
        }

        $stockLine = new StockLine();
        $stockLine->setStock($stock);
        $stockLine->setProduct($product);
        $stockLine->setProject($project);
        if ($cave == Project::CAVE_PANTIN && $isVariation && $force) {
            $stockLine->setLabel($label . ' [Ecrasement du stock]');
            $stockLine->setStockVariationValidDate(new \DateTime());
        }else{
            $stockLine->setLabel($label);
        }
        $stockLine->setFrom($from);
        $stockLine->setDate($date);
        $stockLine->setNumber($operation);
        $stockLine->setCave($cave);
        $stockLine->setHoldForProject($hold);
        if($isVariation){
            $stockLine->setArchived($force? false: true);
            $stockLine->setStockVariation(true);
            $stockLine->setStockVariationValid($force? true: false);
        }
        $this->save($stockLine);
    }

    /**
     * @param Project $project
     */
    public function generateFromProject(Project $project)
    {
        // Clean all
        $this->removeFromProject($project);

        // Add generation
        $items = $project->getItems();
        if (count($items) > 0) {
            foreach ($items as $item) {
                // $dateRemoveStock = $project->getDateStart();
                // if ($item->getProduct()->translate('fr')->isExpendable()) {
                //     $dateRemoveStock = new \DateTime();
                // }

                $dateRemoveStock = new \DateTime();

                $this->addLine(
                    $project->getStock(),
                    $item->getProduct(),
                    "Prévision - Projet #".$project->getId(),
                    -1 * $item->getQuantity(),
                    $dateRemoveStock,
                    $project->getCave(),
                    $project,
                    'generate',
                    true
                );

                if ($project->getNeedReturn() && !$item->getProduct()->translate('fr')->isExpendable()) {
                    $this->addLine(
                        $project->getStock(),
                        $item->getProduct(),
                        "Retour - Projet #".$project->getId(),
                        $item->getQuantity(),
                        $project->getDateEnd(),
                        $project->getCave(),
                        $project,
                        'generate',
                        true
                    );
                }
            }
        }
    }


    /**
     * @param Project $project
     */
    public function removeFromProject(Project $project)
    {
        $this->stockLineRepository->removeFromProject($project);
    }

    /**
     */
    public function checkAndUpdateFullGroupOnlyOption()
    {
        $this->stockLineRepository->checkAndUpdateFullGroupOnlyOption();
    }

    /**
     */
    public function archiveItemsForProject(Project $project)
    {
        return $this->stockLineRepository->archiveItemsForProject($project);
    }

    public function checkLinkedToStock($product, $stock, $cave = 'pantin')
    {
        return $this->stockLineRepository->checkLinkedToStock($product, $stock, $cave);
    }

}
