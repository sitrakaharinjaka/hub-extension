<?php

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class ExampleRepository
 */
class IPRepository extends BaseRepository
{

    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->orderBy('e.name', 'asc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $qb->andWhere('e.'.$key.' LIKE :'.$key);
                $qb->setParameter($key, '%'.$filter.'%');
            }
        }

        return $qb->getQuery();
    }

    /**
     * Have Access to connect admin
     *
     * @param $clientIp
     * @return bool
     */
    public function haveAccess($clientIp)
    {
        if (!$this->isEnable()) {
            return true;
        }

        $qb = $this->createQueryBuilder('c');
        $qb->where($qb->expr()->orX(
                $qb->expr()->eq('c.v4', ':value'),
                $qb->expr()->eq('c.v6', ':value')
            ))
            ->andWhere('c.active = TRUE')
            ->setParameter('value', $clientIp);

        $query = $qb->getQuery();

        return count($query->getResult()) > 0;
    }


    /**
     * Is Enabled
     * @param $clientIp
     */
    public function isEnable()
    {
        $qb = $this->createQueryBuilder('c');
        $qb->select('COUNT(c) as cpt')
            ->where('c.active = TRUE')
            ->setMaxResults(1);

        $tab = $qb->getQuery()->getSingleScalarResult();

        return $tab != 0;
    }

    /**
     * Off all ip
     * @param $clientIp
     */
    public function offAll()
    {
        $qb = $this->createQueryBuilder('c');
        $qb->update()->set('c.active', "0");

        return $qb->getQuery()->execute();
    }

    /**
     * On all ip
     * @param $clientIp
     */
    public function onAll()
    {
        $qb = $this->createQueryBuilder('c');
        $qb->update()->set('c.active', "1");

        return $qb->getQuery()->execute();
    }
}
