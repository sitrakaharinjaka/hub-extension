#!/bin/bash

ORANGE='\033[0;33m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

DIRECTORY=$(cd `dirname $0` && pwd)

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac



#[ -f templates/bundles/FrontBundle/Project ]  && rm templates/bundles/FrontBundle/Project
#ln -s front/src/ templates/bundles/FrontBundle/Project

echo ${GREEN}Generate new files${NC}
for filename in ${DIRECTORY}/../../front/src/*.html; do

    if [ -f "$filename.twig" ]; then
        echo "    ${RED}ALREADY EXIST${NC} $filename.twig"
    else
        echo "    ${GREEN}ADD${NC} $filename.twig"
        cp $filename $filename.twig
    fi
done
for filename in ${DIRECTORY}/../../front/src/**/*.html; do

    if [ -f "$filename.twig" ]; then
        echo "    ${RED}ALREADY EXIST${NC} $filename.twig"
    else
        echo "    ${GREEN}ADD${NC} $filename.twig"
        cp $filename $filename.twig
    fi
done

echo " "
echo ${GREEN}Tranform include tags${NC}
for filename in ${DIRECTORY}/../../front/src/*.twig; do
    echo "    ${ORANGE}TRANSFORM${NC} $filename"

    if [ ${machine} = "Mac" ]; then
        sed -i "" 's/<include src="/{% include "partials\//g' $filename
        sed -i "" 's/><\/include>/ with {} %}/g' $filename
    else
        sed -i  's/<include src="/{% include "partials\//g' $filename
        sed -i  's/><\/include>/ with {} %}/g' $filename
    fi
done
for filename in ${DIRECTORY}/../../front/src/**/*.twig; do
    echo "    ${ORANGE}TRANSFORM${NC} $filename"

    if [ "${machine}" = "Mac" ]; then
        sed -i "" 's/<include src="/{% include "partials\//g' $filename
        sed -i "" 's/><\/include>/ with {} %}/g' $filename
    else
        sed -i  's/<include src="/{% include "partials\//g' $filename
        sed -i  's/><\/include>/ with {} %}/g' $filename
    fi
done


echo " "