<?php

namespace App\Command\Export;

use App\Entity\Project;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\StockLineManager;
use App\Utils\Managers\StockManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Utils\Services\MailerService;
use Symfony\Component\HttpKernel\KernelInterface;

class ExportProductCommand extends Command
{
    protected static $defaultName = 'disko:product:export';

    private $productManager;
    private $stockManager;
    private $mailerService;
    private $projectDir;
    private $stockLineManager;
    protected $mailer;

    public function __construct(ProductManager $productManager, StockManager $stockManager, MailerService $mailerService, KernelInterface $kernel, StockLineManager $stockLineManager, MailerService $mailer)
    {
        parent::__construct();
        $this->productManager   = $productManager;
        $this->stockManager     = $stockManager;
        $this->mailerService    = $mailerService;
        $this->projectDir       = $kernel->getProjectDir();
        $this->stockLineManager = $stockLineManager;
        $this->mailer           = $mailer;
    }


    protected function configure()
    {
        $this->setDescription('Send mail with attached csv file products');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Exécution de ' . self::$defaultName);

        $this->stockLineManager->checkAndUpdateFullGroupOnlyOption();

        $results = $this->productManager->countProductsByStock();

        $fileName = $this->projectDir.'/public/assets/admin/products.csv';

        $output->writeln('Création de ' . $fileName);

        $handle = fopen($fileName, 'w');

        fputcsv($handle, [
            'Sku',
            'Nom',
            'Cave',
            'Stock',
            'Quantité'
        ], ';');


        foreach ($results as $result) {
            $fileRow = [
                $result["sku"],
                $result["product"],
                $result["cave"],
                $result["stock"],
                $result["quantity"],
            ];

            fputcsv($handle, $fileRow, ';');
        }

        fclose($handle);

        $output->writeln('<info>Terminé</info>');

        $this->mailer->sendMailExportProducts($fileName);
    }
}
