<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin\Report;

use App\Entity\Connection;
use App\Entity\Project;
use App\Entity\User;
use App\Form\Model\SearchDateToDate;
use App\Form\Model\SearchUser;
use App\Form\Type\SearchDateToDateType;
use App\Form\Type\User\SearchUserType;
use App\Form\Type\User\UserCreateType;
use App\Form\Type\User\UserType;
use App\Utils\Managers\ConnectionManager;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockLineManager;
use App\Utils\Managers\StockManager;
use App\Utils\Managers\UserManager;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 *
 * @Route("/admin/report")
 * @package App\Controller
 */
class MyHubController extends BaseController
{
    /**
     * @Route("/my-hub", name="admin_myHub_report")
     */
    public function indexAction(Request $request, StockManager $stockManager, ProjectManager $projectManager, ProductManager $productManager, StockLineManager $stockLineManager)
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchDateToDateType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Rapports MyHub');

        $stockLineManager->checkAndUpdateFullGroupOnlyOption();

        $statsStockCreated = $stockManager->statsCreated($data->getStart(), $data->getEnd());
        $statsByHouse = $projectManager->statsByHouse($data->getStart(), $data->getEnd());

        $statsByServiceAfter = $projectManager->statsByService($data->getStart(), $data->getEnd(), 'after');
        $statsByServiceBefore = $projectManager->statsByService($data->getStart(), $data->getEnd(), 'before');
        $statsByService = array_merge($statsByServiceAfter, $statsByServiceBefore);

        $statsServicesByStock = $stockManager->statsServicesByStock($data->getStart(), $data->getEnd());

        $statsByCountry = $projectManager->statsByCountry($data->getStart(), $data->getEnd());

        $statsProductCreated = $productManager->statsCreated($data->getStart(), $data->getEnd());
        $statsProductCreatedHub = $productManager->statsCreatedHub($data->getStart(), $data->getEnd());
        $statsProductCreatedNoHub = $productManager->statsCreatedNoHub($data->getStart(), $data->getEnd());

        $statsByDelayBasic = $projectManager->statsByDelay($data->getStart(), $data->getEnd(), Project::TYPE_BASIC);
        $statsByDelayUrgency = $projectManager->statsByDelay($data->getStart(), $data->getEnd(), Project::TYPE_URGENCY);
        $statsByDelayShuttle = $projectManager->statsByDelay($data->getStart(), $data->getEnd(), Project::TYPE_SHUTTLE);

        $statsByBasic = $projectManager->statsByType($data->getStart(), $data->getEnd(), Project::TYPE_BASIC);
        $statsUrgency = $projectManager->statsByType($data->getStart(), $data->getEnd(), Project::TYPE_URGENCY);
        $statsShuttle = $projectManager->statsByType($data->getStart(), $data->getEnd(), Project::TYPE_SHUTTLE);

        //dd('les stats');
        $topDelay = $projectManager->topByDelay($data->getStart(), $data->getEnd(), 15);

        // Render view
        return $this->render('@Admin/Report/MyHub/index.html.twig', array(
            'statsStockCreated' => $statsStockCreated,
            'statsByHouse' => $statsByHouse,
            'statsByService' => $statsByService,
            'statsByCountry' => $statsByCountry,
            'statsProductCreated' => $statsProductCreated,
            'statsProductCreatedHub' => $statsProductCreatedHub,
            'statsProductCreatedNoHub' => $statsProductCreatedNoHub,

            'statsByDelayBasic' => $statsByDelayBasic,
            'statsByDelayUrgency' => $statsByDelayUrgency,
            'statsByDelayShuttle' => $statsByDelayShuttle,

            'statsByBasic' => $statsByBasic,
            'statsUrgency' => $statsUrgency,
            'statsShuttle' => $statsShuttle,

            'topDelay' => $topDelay,

            'statsServicesByStock' => $statsServicesByStock,

            'data' => $data,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/my-hub/top-delay/ajax", name="admin_myHub_report_top_delay_ajax")
     */
    public function topDelayAction(Request $request, ProjectManager $projectManager)
    {
        $search = $request->get('search', '');
        $search = trim($search);

        $searchStart = $request->get('searchdatestart', '');
        $searchEnd = $request->get('searchdateend', '');
        if (empty($searchStart) or empty($searchEnd)) {
            $topDelay = [];
        } else {
            $searchStart = \DateTime::createFromFormat("d/m/Y", $searchStart);
            $searchEnd = \DateTime::createFromFormat("d/m/Y", $searchEnd);
            $topDelay = $projectManager->topByDelay($searchStart, $searchEnd, 15, $search);
        }

        return $this->render('@Admin/Report/MyHub/partials/table-top-delay.html.twig', array(
            'topDelay' => $topDelay,
        ));
    }


    /**
     * Init Search object
     *
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        $dateTimeDefaultStart = new \DateTime();
        $dateTimeDefaultStart->modify('-14 day');

        $dateTimeDefaultEnd = new \DateTime();

        // Init form
        $data = new SearchDateToDate();
        $data->setStart((isset($filters['start'])) ? \DateTime::createFromFormat("d/m/Y", $filters['start']) : $dateTimeDefaultStart);
        $data->setEnd((isset($filters['end']))   ? \DateTime::createFromFormat("d/m/Y", $filters['end']) : $dateTimeDefaultEnd);

        return $data;
    }
}
