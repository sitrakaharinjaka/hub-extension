<?php

namespace App\Repository;

use App\Repository\BaseRepository;
use App\Entity\MyBoxesBox;

/**
 * Class MyBoxesBarcodeRepository
 */
class MyBoxesBarcodeRepository extends BaseRepository
{

    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e as barcode');

        $qb
            ->addSelect('box.id as boxId')
            ->addSelect('box.name as boxName')
            ->leftJoin(
                'App\Entity\MyBoxesBox',
                'box',
                'WITH',
                $qb->expr()->andX(
                    'e.code = box.barcode',
                    $qb->expr()->notIn('box.state', [MyBoxesBox::STATE_BOX_USER_STOCK])
                )
            )
            ;

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'e';
                if ($key == 'id') {
                    $alias = 'e';
                }

                $qb->andWhere($alias . '.' . $key . ' LIKE :' . $key);
                $qb->setParameter($key, '%' . $filter . '%');
            }
        }

        $query = $qb->getQuery();

        return $query;
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function checkExist($code)
    {
        $qb = $this->createQueryBuilder('bc');
        $qb
            ->select('COUNT(bc.id)')
            ->andWhere('bc.code = :code')
            ->setParameter('code', $code)
            ;
        return $qb->getQuery()->getSingleScalarResult() > 0;
    }
}
