<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MyBoxesNumberMemberConstraint extends Constraint
{
    public $message_min = 'Il faut au minimum 2 membres pour une team box';

    public function validatedBy()
    {
        return 'myboxes_number_member_constraint';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
