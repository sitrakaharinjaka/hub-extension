<?php

namespace App\Validator\Constraints;

use App\Entity\ProductTranslation;
use App\Entity\Project;
use App\Entity\ProjectItem;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockLineManager;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Entity\Product;

class ImportProjectValidator extends ConstraintValidator
{
    private const REQUIRED_COLUMN = 39;
    private const REQUIRED_INDEXES_CONDITION = [
        0 => [
            'pattern' => '/^[A-Z0-9]*$/',
            'message' => "Erreur ligne %INDEX% : le champ 'SKU' doit être un alpha-numérique or il a pour valeur %VALUE%",
        ],
        3 => [
            'pattern' => '/^[0-9]*$/',
            'message' => "Erreur ligne %INDEX% : le champ 'EAN Bte/Pce' doit être un numérique or il a pour valeur %VALUE%",
        ],
        11 => [
            'pattern' => '/^[0-9]*$/',
            'message' => "Erreur ligne %INDEX% : le champ 'EAN carton/caisse' doit être un numérique or il a pour valeur %VALUE%",
        ],
    ];
    private const REQUIRED_MIME_TYPES = [
        'text/csv',
        'application/csv',
        'application/vnd.ms-excel',
    ];
    private const REQUIRED_HEADERS =  "SKU;"                                            //  0
                                    . "Nom SAP;"                                        //  1
                                    . "Nom Marketing;"                                  //  2
                                    . "EAN Bte/Pce;"                                    //  3
                                    . "Conditionnement de l'unité;"                     //  4
                                    . "Catégories;"                                     //  5
                                    . "Longueur unité;"                                 //  6
                                    . "Largeur unité;"                                  //  7
                                    . "Hauteur unité;"                                  //  8
                                    . "Poids unité;"                                    //  9
                                    . "Nombre unité par carton;"                        // 10
                                    . "EAN carton/caisse;"                              // 11
                                    . "Longueur carton;"                                // 12
                                    . "Largeur carton;"                                 // 13
                                    . "Hauteur carton;"                                 // 14
                                    . "Poids carton;"                                   // 15
                                    . "Information;"                                    // 16
                                    . "CRD;"                                            // 17
                                    . "Iso2 Pays;"                                      // 18
                                    . "Litrage;"                                        // 19
                                    . "Effervescent;"                                   // 20
                                    . "REGION / APPELATION;"                            // 21
                                    . "MILLESIME;"                                      // 22
                                    . "Couleur du vin;"                                 // 23
                                    . "Catégorie du vin;"                               // 24
                                    . "% d'alcool;"                                     // 25
                                    . "Produits consommable;"                           // 26
                                    . "Nomenclature Douanière;"                         // 27
                                    . "Media;"                                          // 28
                                    . "Marque;"                                         // 29
                                    . "valeur Comptable;"                               // 30
                                    . "Etat;"                                           // 31
                                    . "Matière;"                                        // 32
                                    . "Fragile;"                                        // 33
                                    . "Année de production;"                            // 34
                                    . "Valorisation;"                                   // 35
                                    . "Département / Stock en entité (ex : krug-RH);"   // 36
                                    . "Maison;"                                         // 37
                                    . "Image de couverture"                             // 38
                                    ;

    private const ERROR_FILE_NOFILE = "Erreur Fichier : merci d'upload un fichier avant de lancer l'import";
    private const ERROR_FILE_NOTCSV = "Erreur Fichier : merci d'upload un fichier CSV (%MIME TYPE%)";
    private const ERROR_FILE_BADHEADERS = "Erreur Fichier : Le nombre de colonnes ne correspond pas";
    private const ERROR_FILE_NOTUTF8 = "Erreur Fichier : L'encodage du fichier n'est pas correct (Requis : UTF-8).";
    private const ERROR_FILE_HEADERS_HELP = "Les colonnes attendues sont " . ImportProjectValidator::REQUIRED_HEADERS;

    private $em;

    private $translator;

    private $productManager;

    /**
     * @var ObjectRepository
     */
    private $productTranslationRepository;

    /**
     * StockClassValidator constructor.
     * @param ObjectManager $em
     * @param TranslatorInterface $translator
     * @param ProductManager $productManager
     */
    public function __construct(ObjectManager $em, TranslatorInterface $translator, ProductManager $productManager)
    {
        $this->em = $em;
        $this->translator    = $translator;
        $this->productManager = $productManager;
        $this->productTranslationRepository = $this->em->getRepository('App:ProductTranslation');
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {

        /** @var ImportProduct $importProduct */
        $importProduct = $value;

        if (!$this->checkFile($importProduct)) {
            return;
        }

        $handle = fopen($importProduct->getFile()->getPathName(), "r");

        if (!$this->checkColumns($handle)) {
            return;
        }

        if (!$this->checkRows($handle)) {
            return;
        }
    }

    private function checkFile($importProduct)
    {
        if (!$importProduct || !$importProduct->getFile()) {
            $this->context->buildViolation($this->translator->trans(self::ERROR_FILE_NOFILE, []))->atPath('file')->addViolation();
            return false;
        }
        $mimetype = $importProduct->getFile()->getClientMimeType();
        if (!in_array($mimetype, self::REQUIRED_MIME_TYPES)) {
            $this->context->buildViolation($this->translator->trans(self::ERROR_FILE_NOTCSV, ['%MIME TYPE%' => $mimetype]))->atPath('file')->addViolation();
            return false;
        }
        return true;
    }

    private function checkColumns($handle)
    {
        $enteteSTR = trim(mb_convert_encoding(fgets($handle), "UTF-8", "UTF-8, ISO-8859-1, Windows-1252"));
        $enteteARR = explode(";", $enteteSTR);

        if (sizeof($enteteARR) != self::REQUIRED_COLUMN) {
            $this->context->buildViolation($this->translator->trans(self::ERROR_FILE_BADHEADERS, []))->atPath('file')->addViolation();
            $this->context->buildViolation($this->translator->trans(self::ERROR_FILE_HEADERS_HELP, []))->atPath('file')->addViolation();
            return false;
        }

        if ($enteteSTR != self::REQUIRED_HEADERS) {
            $this->context->buildViolation($this->translator->trans(self::ERROR_FILE_NOTUTF8, []))->atPath('file')->addViolation();
            $this->context->buildViolation($this->translator->trans(self::ERROR_FILE_HEADERS_HELP, []))->atPath('file')->addViolation();
            return false;
        }
        return true;
    }

    private function checkRows($handle)
    {
        $check = true;
        $lineIndex = 2; // On commence à compter les lignes à 1, et la ligne 1 est la ligne d'entête ...
        while ($lineContent = fgetcsv($handle, 0, ";", '"')) {
            $lineContent = mb_convert_encoding($lineContent, "UTF-8", "UTF-8, ISO-8859-1, Windows-1252");
            $check = $this->checkRow($lineIndex, $lineContent) && $check;
            $lineIndex++;
        }
        return $check;
    }

    private function checkRow($lineIndex, $lineContent)
    {
        $check = true;
        foreach (self::REQUIRED_INDEXES_CONDITION  as $index => $condition) {
            $pattern = $condition['pattern'];
            $match = preg_match($pattern, $lineContent[$index]);

            if (!$match) {
                $message = $this->translator->trans($condition['message'], ['%INDEX%'=>$lineIndex, '%VALUE%'=>$lineContent[$index]]);
                $check = false;
                $this->context->buildViolation($message)->atPath('file')->addViolation();
            }
        }
        return $check;
    }
}
