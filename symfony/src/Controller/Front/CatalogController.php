<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Front;

use App\Controller\BaseController;
use App\Entity\Project;
use App\Utils\Managers\CategoryManager;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockLineManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CatalogController
 *
 * @Route("/")
 * @package App\Controller
 */
class CatalogController extends BaseController
{


    /**
     * Catalog
     * @Route("/catalogue", name="catalog")
     * @return Response
     */
    public function index(Request $request, ProductManager $productManager, ProjectManager $projectManager, StockLineManager $stockLineManager, CategoryManager $categoryManager)
    {
        $project = $this->get('session')->get('current_project', null);
        $stock = $this->get('session')->get('current_stock');


        $keyword = $request->get('keyword', null);
        $category = $request->get('category', null);
        $limit = $request->get('limit', null);

        if (!in_array($limit, [12, 24])) {
            $limit = 12;
        }

        $datas = $projectManager->getParameters($project);

        // Generate query
        $query = $productManager->queryForSearchInFront($stock, $datas['cave'], $keyword, $category, $project);

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            $limit,
            array('wrap-queries'=>true)
        );

        $ids = [];
        $objects = [];
        foreach ($pagination as $product) {
            $ids[] = $product->getId();
            $objects[] = $product;
        }

        // Get stock (large view or specific view)
        $totalWithHolds = $total115WithHolds = $totalPantinWithHolds =null;
        if ($datas['cave']) {
            $totalWithHolds = $stockLineManager->totalStockOnProducts($stock, $datas['cave'], $ids, $datas['dateStart'], $datas['dateEnd'], true);

            if($datas['cave'] == Project::CAVE_PANTIN){
                $totalWithHolds = [];
                foreach($objects as $object){
                    $totalWithHolds[] = [
                        'total' => $stockLineManager->quantityOf($object, Project::CAVE_PANTIN, new \DateTime('now'), $stock, false),
                        'id' => $object->getId()
                    ];
                }
            }
        } else {
            $total115WithHolds = $stockLineManager->totalStockOnProducts($stock, Project::CAVE_115, $ids, $datas['dateStart'], $datas['dateEnd'], true);

            //$totalPantinWithHolds = $stockLineManager->totalStockOnProducts($stock, Project::CAVE_PANTIN, $ids, $datas['dateStart'], $datas['dateEnd'], true);
            $totalPantinWithHolds = [];
            foreach($objects as $object){
                $totalPantinWithHolds[] = [
                    'total' => $stockLineManager->quantityOf($object, Project::CAVE_PANTIN, new \DateTime('now'), $stock, false),
                    'id' => $object->getId()
                ];
            }
        }

        // Find all categories active
        $categories = $categoryManager->findAllActive();
        $categoriesCounts = $productManager->countProductsInCategories($stock, $project);


        $template = 'hub/catalog/index.html.twig';
        if ($keyword) {
            $template = 'hub/catalog/result.html.twig';
        }

        return $this->render($template, [
            'search' => [
                'keyword' => $keyword,
                'category' => $category,
                'limit' => $limit,
            ],
            'categories' => $categories,
            'categoriesCounts' => $categoriesCounts,
            'pagination' => $pagination,
            'totalWithHolds' => $totalWithHolds,
            'total115WithHolds' => $total115WithHolds,
            'totalPantinWithHolds' => $totalPantinWithHolds,
        ]);
    }
}
