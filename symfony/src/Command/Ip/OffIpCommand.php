<?php

namespace App\Command\Ip;

use App\Utils\Managers\IPManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class OffIpCommand extends Command
{
    /** @var  EntityManager */
    private $ipManager;

    public function __construct(IPManager $ipManager)
    {
        $this->ipManager = $ipManager;
        parent::__construct();
    }

    protected static $defaultName = 'disko:ip:offall';

    protected function configure()
    {
        $this->setDescription('Off all ip restrict');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->ipManager->offAll();
    }
}
