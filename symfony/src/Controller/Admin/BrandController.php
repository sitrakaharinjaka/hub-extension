<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\Brand;
use App\Entity\BrandTranslation;
use App\Form\Model\SearchBrand;
use App\Form\Type\Brand\BrandTranslationType;
use App\Form\Type\Brand\SearchBrandType;
use App\Form\Type\Brand\BrandType;
use App\Utils\Managers\BrandManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BrandController
 * @Route("/admin/brands")
 * @package Disko\AdminBundle\Controller
 */
class BrandController extends BaseController
{
    /**
     * Brand's list
     * @Route("/list/{askLocale}", name="admin_brand_list", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listAction(Request $request, BrandManager $brandManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchBrandType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des marques');

        // Generate query
        $query = $brandManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/Brand/index.html.twig', array(
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchBrand();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setName((isset($filters['name']))   ? $filters['name'] : '');

        return $data;
    }

    /**
     * Create a brand
     * @Route("/create", name="admin_brand_create")
     * @param Request $request Request
     *
     * @return Response
     */
    public function createAction(Request $request, BrandManager $brandManager, LocaleManager $localeManager)
    {
        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");

        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des marques", $this->get("router")->generate("admin_brand_list"));
        $breadcrumbs->addItem("Création");


        // Init form
        $brand = new Brand();
        $form = $this->createForm(BrandType::class, $brand, [
            'validation_groups' => ['Default']
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($brand->getTranslations()) > 0) {
                    $brandManager->save($brand);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Création terminée"
                    );

                    return $this->redirect($this->generateUrl('admin_brand_edit', array(
                        'id' => $brand->getId()
                    )));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            } else {

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'error',
                    "Le formulaire contient des erreurs ou est vide"
                );
            }
        }

        // View
        return $this->render(
            '@Admin/Brand/create.html.twig',
            array(
                'form'           => $form->createView(),
                'brand'           => $brand,
            )
        );
    }

    /**
     * Update a brand
     * @Route("/edit/{id}", name="admin_brand_edit")
     * @param Request $request Request
     * @param integer $id      Id of brand
     *
     * @return Response
     */
    public function editAction(Request $request, BrandManager $brandManager, LocaleManager $localeManager, $id)
    {
        // Find it
        /** @var Brand $brand */
        $brand = $brandManager->findOneToEdit($id);
        if (!$brand) {
            throw $this->createNotFoundException('Brand not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des marques", $this->get("router")->generate("admin_brand_list"));
        $breadcrumbs->addItem("Modification");

        $form = $this->createForm(BrandType::class, $brand, [
            'validation_groups' => ['Default']
        ]);


        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($brand->getTranslations()) > 0) {
                    $brandManager->save($brand);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Mise à jour effectuée"
                    );

                    return $this->redirect($request->headers->get('referer'));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/Brand/edit.html.twig',
            array(
                'form'           => $form->createView(),
                'brand'           => $brand,
            )
        );
    }

    /**
     * Delete one brand
     * @Route("/delete/{id}", name="admin_brand_delete")
     * @param Request $request Request
     * @param integer $id      Id of brand
     */
    public function deleteAction(Request $request, BrandManager $brandManager, $id)
    {
        // Find it
        /** @var Brand $brand */
        $brand = $brandManager->findOneToEdit($id);
        if (!$brand) {
            throw $this->createNotFoundException('Brand not found');
        }

        $brandManager->remove($brand);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été supprimé"
        );

        return $this->redirect($request->headers->get('referer'));
    }



    /**
     * Duplicate one brand
     * @Route("/duplicate/{id}", name="admin_brand_duplicate")
     * @param Request $request Request
     * @param integer $id      Id of brand
     */
    public function duplicateAction(Request $request, BrandManager $brandManager, $id)
    {
        // Find it
        /** @var Brand $brand */
        $brand = $brandManager->findOneToEdit($id);
        if (!$brand) {
            throw $this->createNotFoundException('Brand not found');
        }

        $this->getDoctrine()->getManager()->detach($brand);
        foreach ($brand->getTranslations() as $translation) {
            $translation->setId(null);
            $translation->setSlug(null);
            $this->getDoctrine()->getManager()->detach($translation);
        }
        $brandManager->save($brand);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été dupliqué"
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Export csv
     * @Route("/export/{locale}", name="admin_brand_export")
     * @param Request $request
     */
    public function exportAction(Request $request, BrandManager $brandManager, $locale)
    {
        $data = $this->initSearch($request);
        $query = $brandManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Nom',
            'Date de création',
            
        ], ';');

        $locale = $request->get('locale', $request->getDefaultLocale());
        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->translate($locale)->getName(),
                ($result->translate($locale)->getCreated()) ? $result->translate($locale)->getCreated()->format('d/m/Y') : $result->getCreated()->format('d/m/Y'),
                
            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_'.$locale.'_brands_export.csv"'
        ));
    }
}
