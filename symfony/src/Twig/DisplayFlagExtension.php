<?php
namespace App\Twig;

class DisplayFlagExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('displayFlag', array($this, 'displayFlag')),
        );
    }

    public function displayFlag($locale)
    {
        if (strtolower($locale) == 'en') {
            $locale = 'gb';
        }

        return $locale;
    }

    public function getName()
    {
        return 'display_flag_extension';
    }
}
