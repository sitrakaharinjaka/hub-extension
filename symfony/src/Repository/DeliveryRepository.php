<?php

namespace App\Repository;

use App\Entity\Delivery;
use App\Repository\BaseRepository;

/**
 * Class DeliveryRepository
 */
class DeliveryRepository extends BaseRepository
{

    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->orderBy('e.type', 'asc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $qb->andWhere('e.'.$key.' LIKE :'.$key);
                $qb->setParameter($key, '%'.$filter.'%');
            }
        }

        return $qb->getQuery();
    }


    /**
     * Remove all
     */
    public function removeAll()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete(Delivery::class, 's');

        return $qb->getQuery()->execute();
    }
}
