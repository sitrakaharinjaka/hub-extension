<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Front\MyBoxes;

use App\Controller\BaseController;
use App\Entity\MyBoxesBox;
use App\Entity\MyBoxesCommand;
use App\Entity\Project;
use App\Form\Type\MyBoxes\CommandType;
use App\Utils\Managers\MyBoxesBoxManager;
use App\Utils\Managers\MyBoxesCommandManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockManager;
use App\Utils\Managers\ToolboxManager;
use App\Utils\Managers\UserManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 *
 * @Route("/myboxes")
 *
 * @package App\Controller
 */
class UserController extends BaseController
{
    /**
     * Homepage
     *
     * @Route("/search", name="myboxes_user_search")
     * @return Response
     */
    public function index(Request $request, UserManager $userManager)
    {
        $phrase = $request->get('phrase', '');
        return new JsonResponse($userManager->findLikeSearch($phrase, $this->getUser()->getEmail()));
    }
}
