<?php

namespace App\Repository;

use App\Entity\Stock;
use App\Entity\StockEntry;
use App\Repository\BaseRepository;

/**
 * Class StockEntryRepository
 */
class StockEntryRepository extends BaseRepository
{



    /**
     * ================= FRONT ===================
     */


    /**
     * ================= BACK ===================
     */


    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et')
            ->leftJoin('e.translations', 'et')
            ->orderBy('et.name', 'asc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'et';
                if ($key == 'id') {
                    $alias = 'e';
                }
                $qb->andWhere($alias.'.'.$key.' LIKE :'.$key);
                $qb->setParameter($key, '%'.$filter.'%');
            }
        }

        return $qb->getQuery();
    }

    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'i')
//            ->select('e', 'et', 'bc', 'bct', 'u')
//            ->leftJoin('e.translations', 'et')
//            ->leftJoin('e.billingCenters', 'bc')
//            ->leftJoin('bc.translations', 'bct')
//            ->leftJoin('e.referentUser', 'u')
            ->leftJoin('e.stockEntryItems', 'i')
            ->where('e.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }

    /**
     * Find one for update & mail
     *
     * @param $id
     * @return mixed
     */
    public function findOneToUpdate($id)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'i', 'u', 'p', 'pt', 's')
            ->leftJoin('e.user', 'u')
            ->leftJoin('e.stockEntryItems', 'i')
            ->leftJoin('i.product', 'p')
            ->leftJoin('p.translations', 'pt')
            ->leftJoin('e.stock', 's')
            ->where('e.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }

    /**
     * Find one for confirmation screen
     *
     * @param $id
     * @return mixed
     */
    public function findOneToConfirm($id)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'i', 'u', 'p', 'pt', 's', 'st')
            ->leftJoin('e.user', 'u')
            ->leftJoin('e.stock', 's')
            ->leftJoin('s.translations', 'st')
            ->leftJoin('e.stockEntryItems', 'i')
            ->leftJoin('i.product', 'p')
            ->leftJoin('p.translations', 'pt')
            ->where('e.id = :id')
            ->setParameter('id', $id);
        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
        return $query->getOneOrNullResult();
    }

    /**
     * Find all for synch
     *
     * @return mixed
     */
    public function findAllToSynch($since = null)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e','s')
            ->leftJoin('e.stock', 's')
            ->where('e.dateSendToVeolog IS NULL')
            ->andWhere('s.cave != 115');

        if($since instanceof \DateTime){
            $qb->andWhere('e.created >= :since')
            ->setParameter('since', $since, \Doctrine\DBAL\Types\Type::DATETIME);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Find one for confirmation screen
     *
     * @param $id
     * @return mixed
     */
    public function findNonArchivedStock($stock)
    {
        // Set archived date limit for stock. Currently archived if more than 12 months based on validated
        $date = new \DateTime();
        $date->modify('-12 months');

        //$notDisplayed = [StockEntry::STATE_ITEM_CREATE];

        $qb = $this->createQueryBuilder('e');

        $qb->select('e', 'i', 'u', 'p', 'pt', 's', 'st')
            ->leftJoin('e.user', 'u')
            ->leftJoin('e.stock', 's')
            ->leftJoin('s.translations', 'st')
            ->leftJoin('e.stockEntryItems', 'i')
            ->leftJoin('i.product', 'p')
            ->leftJoin('p.translations', 'pt')
            // ->where($qb->expr()->notIn('i.status', $notDisplayed))
            //->andWhere('i.updated >= :last')
            ->setParameter('last', $date->format('Y-m-d H:i:s'))
            ->andWhere('e.stock = :stock')
            ->setParameter('stock', $stock)
            ->orderBy('e.deliveryDate', 'DESC');

        $qb->andWhere($qb->expr()->orX(
            $qb->expr()->andX($qb->expr()->gte('i.updated', ':last'), $qb->expr()->in('i.status', ['created', 'updated', 'validated', 'rejected', 'integrated_by_veolog', 'rejected_by_veolog'])),
            $qb->expr()->andX($qb->expr()->lte('i.updated', ':last'), $qb->expr()->in('i.status', ['updated']))
        ));

        return $qb->getQuery()->getResult();
    }

    /**
     * Find one for confirmation screen
     *
     * @param $id
     * @return mixed
     */
    public function findQuantityReceived($product, $date, $stock = null)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(i.quantityReceived) as total')
            ->leftJoin('e.stockEntryItems', 'i')
            ->andWhere('i.product = :product')
            ->setParameter('product', $product)
            ->andWhere('i.status = :updated')
            ->setParameter('updated', StockEntry::STATE_ITEM_UPDATE)
            ->andWhere("i.updated <= :date")
            ->setParameter('date', $date);


        if (is_object($stock)) {
            $qb->andWhere('e.stock = :stock')
                ->setParameter('stock', $stock);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

}
