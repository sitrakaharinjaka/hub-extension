<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Front;

use App\Controller\BaseController;
use App\Entity\Project;
use App\Entity\ProjectItem;
use App\Event\ProjectEvent;
use App\Form\Model\ProjectSwitch;
use App\Form\Type\Project\CartType;
use App\Form\Type\Project\DeleteProjectType;
use App\Form\Type\Project\ProjectItemType;
use App\Form\Type\Project\ProjectSwitchType;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectItemManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockLineManager;
use App\Utils\Managers\StockManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\Services\MailerService;

/**
 * Class ProjectItemController
 *
 * @Route("/paniers")
 * @package App\Controller
 */
class ProjectItemController extends BaseController
{

    /**
     * @Route(path="/courant", name="project_summary", methods={"GET"})
     */
    public function index(ProjectManager $projectManager)
    {
        $project = $this->get('session')->get('current_project', null);

        if (!$project) {
            return $this->redirectToRoute('catalog');
        }

        $project = $projectManager->findOneBy(['id' => $project->getId()]);
        if (!$project) {
            throw $this->createNotFoundException("No project found");
        }

        $form = $this->createForm(CartType::class, $project, [
          'white_checkbox' => true
        ]);

        $subProjects = $projectManager->findSubProjectsByParentId($project->getId());

        return $this->render('hub/project/summary.html.twig', [
            'form'        => $form->createView(),
            'project'     => $project,
            'subprojects' => $subProjects,
    ]);
    }

    /**
     * @param Request $request
     * @param                     $productId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Route(path="/form-add-item/{productId}", name="add_project_item")
     */
    public function form(Request $request, ProductManager $productManager, StockLineManager $stockLineManager, ProjectManager $projectManager, ProjectItemManager $projectItemManager, $productId)
    {
        $user = $this->getUser();


        $product = $productManager->findOneBy(['id' => $productId]);
        if (!$product) {
            throw $this->createNotFoundException("Product not found");
        }

        $project = $this->get('session')->get('current_project', null);
        if ($project) {
            $project = $projectManager->findOneBy(['id' => $project->getId()]);
            if (!$project or $project->getUser()->getId() != $user->getId()) {
                $this->get('session')->set('current_project', null);
            }
        }


        if ($request->isXmlHttpRequest() && !in_array($project->getState(), Project::STATE_EDITABLE)) {
            return new JsonResponse([
                'error' => true,
                'content' => ""
            ]);
        }


        $projectItem = $projectItemManager->create($project, $product);
        $form = $this->createForm(ProjectItemType::class, $projectItem);

        $stock = $this->get('session')->get('current_stock');
        $datas = $projectManager->getParameters($project);

        $totalInCart = $projectItemManager->countInProject($project, $product);

        $total115Available = $totalPantinAvailable = $totalAvailable = null;
        if ($project) {
            $totalAvailable = $stockLineManager->totalStockDateOnProduct($stock, $datas['cave'], $product->getId(), $datas['dateStart'], $datas['dateEnd'], true);

            if($datas['cave'] == Project::CAVE_PANTIN){
                $totalAvailable = $stockLineManager->quantityOf($product, Project::CAVE_PANTIN, new \DateTime('now'), $stock, false);
            }
        } else {
            if ($stock->getCave() == Project::CAVE_115) {
                $total115Available = $stockLineManager->totalStockDateOnProduct($stock, Project::CAVE_115, $product->getId(), $datas['dateStart'], $datas['dateEnd'], true);
            } elseif ($stock->getCave() == Project::CAVE_PANTIN) {
                //$totalPantinAvailable = $stockLineManager->totalStockDateOnProduct($stock, Project::CAVE_PANTIN, $product->getId(), $datas['dateStart'], $datas['dateEnd'], true);
                $totalPantinAvailable = $stockLineManager->quantityOf($product, Project::CAVE_PANTIN, new \DateTime('now'), $stock, false);
            }
        }

        $form->handleRequest($request);
        $parameters = [
            'product' => $product,
            'project' => $project,
            'form' => $form->createView(),
            'totalAvailable' => $totalAvailable,
            'total115Available' => $total115Available,
            'totalPantinAvailable' => $totalPantinAvailable,
            'totalInCart' => $totalInCart
        ];


        if ($form->isSubmitted() && $form->isValid()) {
            $base = $project->getTotalQuantity();
            $projectManager->addToProject($project, $projectItem);

            $parameters['form'] = $this->createForm(ProjectItemType::class, $projectItemManager->create($project, $product))->createView();
            $parameters['totalInCart'] = $totalInCart + $projectItem->getQuantity();
            $this->get('session')->set('current_project', $project);
            return new JsonResponse([
                'error' => false,
                'success' => $this->renderView('hub/product/partials/_addSuccess.html.twig'),
                'quantity_label' => $base + $projectItem->getQuantity(),
                'quantity_diff' => ($project->getState() != Project::STATE_HUB_DRAFT) ? $projectItem->getQuantity() : 0,
                'content' => $this->renderView('hub/product/partials/_addToCart.html.twig', $parameters),
            ]);
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            return new JsonResponse([
                'error' => true,
                'content' => $this->renderView('hub/product/partials/_addToCart.html.twig', $parameters)
            ]);
        }

        return $this->render('hub/product/partials/_addToCart.html.twig', $parameters);
    }

    /**
     * @param Request $request
     * @param EventDispatcherInterface $dispatcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route(path="/update", name="project_cart_save", methods={"PUT", "PATCH", "POST"})
     */

    public function update(Request $request, ProjectManager $projectManager, EventDispatcherInterface $dispatcher, MailerService $mailerService)
    {
        $user = $this->getUser();
        $project = $this->get('session')->get('current_project', null);
        $project = $projectManager->findOneBy(['id' => $project->getId()]);
        if (!$project or $project->getUser()->getId() != $user->getId()) {
            throw $this->createNotFoundException("No project found");
        }

        $form = $this->createForm(CartType::class, $project, [
          'validation_groups' => 'cart',
          'method' => 'PATCH'
        ]);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $value = 'draft';
            if ($project->getButton() == 'cart_submit') {
                $value = 'submit';

                $this->get('session')->set('current_project', null);
                $projectManager->book($project);

                /**
                 * Commande presidence
                 * https://bugkiller.disko.fr/issues/67966
                 */
                if($this->getUser()->getEmail() == 'ejubin@moethennessy.com'){
                    $stock = $this->get('session')->get('current_stock');

                    if($stock->translate('fr')->getCode() == 'MHPRES' || $stock->translate('fr')->getCode() == 'MHPRES115'){
                        $mailerService->sendNotifyPresidenceProject($project);
                    }
                }
            } else {

        // @todo : Perte de projet quand on sauvegarde (très perturbant)
        /**
         * $dispatcher->dispatch(
         * ProjectEvent::PROJECT_SAVED_DRAFT,
         * new ProjectEvent($project)
         * );**/
            }
            $this->get('session')->set('flash_project', $value);

            return $this->redirectToRoute('account');
        }

        return $this->render('hub/project/summary.html.twig', [
      'form' => $form->createView(),
      'project' => $project
    ]);
    }

    /**
     * @param ProjectManager $projectManager
     * @return Response
     */
    public function cartHead(ProjectManager $projectManager)
    {
        $project = $this->get('session')->get('current_project', null);
        if ($project) {
            $project = $projectManager->findOneBy(['id' => $project->getId()]);
        }

        return $this->render('hub/product/partials/_headCart.html.twig', [
      'project' => $project
    ]);
    }

    /**
     * @param ProjectItem $projectItem
     * @Route(path="/{id}/remove", name="project_ajax_cart_item_remove")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function remove($id, ProjectItemManager $projectItemManager)
    {
        $item = $projectItemManager->findOneby(['id' => $id]);
        if (!$item) {
            throw $this->createNotFoundException("Item not found");
        }

        $project = $item->getProject();
        if (!$project or $project->getUser()->getId() != $this->getUser()->getId()) {
            throw $this->createNotFoundException("No project found");
        }

        // @todo manage stocklines
        $projectItemManager->remove($item);

        return $this->redirectToRoute('project_summary');
    }

    /**
     * @Route(path="/ajaxItemStatus/", name="dashboard_project_item_ajax_status")
     *
     * @param Request          $request
     * @param ProjectManager $projectManager
     *
     * @return Response
     */
    public function AjaxItemProjectStatus(Request $request, ProjectManager $projectManager)
    {
      if ($request->isXmlHttpRequest() && 'POST' === $request->getMethod()) {

        $project = $request->request->get('data');

        $item = $projectManager->findStateById($project);

        return new JsonResponse($item, 200);
      }

      return new JsonResponse(array('code' => 500));
    }

}
