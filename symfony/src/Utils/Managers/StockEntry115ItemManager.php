<?php

namespace App\Utils\Managers;

use App\Entity\StockEntry115Item;
use App\Repository\StockEntry115ItemRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Gaufrette\Filesystem;

/**
 * Class StockManager
 * `
 * Object manager of stock
 *
 * @package Disko\PageBundle\Managers
 */
class StockEntry115ItemManager extends BaseManager
{
    private $productManager;

    /** @var StockEntry115ItemRepository */
    protected $stockEntry115ItemRepository;

    /**
     * StockManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager, ProductManager $productManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('stockEntry115ItemRepository', StockEntry115Item::class);
        $this->productManager = $productManager;
    }

    /**
     * Save a stock entry
     *
     * @param StockEntry115Item $stockEntry115Item
     */
    public function save(StockEntry115Item $stockEntry115Item)
    {
        $stockEntry115Item->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($stockEntry115Item);
        $this->getEntityManager()->flush();
        return $stockEntry115Item;
    }

    /**
     * Remove one stock entry 115
     *
     * @param StockEntry115Item StockEntry115Item
     */
    public function remove(StockEntry115Item $stockEntry115Item)
    {
        $this->getEntityManager()->remove($stockEntry115Item);
        $this->getEntityManager()->flush();
    }


    public function findOneToUpdate($id)
    {
        return $this->stockEntry115ItemRepository->findOneToUpdate($id);
    }

    public function findOneToConfirm($id)
    {
        return $this->stockEntry115ItemRepository->findOneToConfirm($id);
    }

    public function findAllToSynch()
    {
        return $this->stockEntry115ItemRepository->findAllToSynch();
    }

    /**
     * Get all stock entries
     */
    public function queryForSearch(string $locale, array $filters = []): Query
    {
        return $this->stockEntry115ItemRepository->queryForSearch($locale, $filters);
    }

    /**preparationEntryStockEdit
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->stockEntry115ItemRepository->findOneBy($filters);
    }
}
