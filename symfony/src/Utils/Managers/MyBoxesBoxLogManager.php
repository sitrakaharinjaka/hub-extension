<?php

/**
 * Manager
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;
use App\Entity\MyBoxesBoxLog;
use App\Repository\MyBoxesBoxLogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class MyBoxesBoxLogManager
 *
 * Object manager of user
 */
class MyBoxesBoxLogManager extends BaseManager
{
    /**
    * @var MyBoxesBoxLogRepository
    */
    protected $boxRepository;

    public function log($box, $info = 'send', $changes = [])
    {
        $log = new MyBoxesBoxLog();
        $log->setBox($box);

        if(is_array($changes) && count($changes) > 0){
            $info = 'Modifications :' . PHP_EOL;
            foreach($changes as $field => $change){
                if(is_array($change) && isset($change[0]) && isset($change[1])){
                    $info .= '<strong>[' . $field . ']</strong> <span style="color:red;">' . $change[0] . '</span> <strong>=></strong> ' . $change[1] . PHP_EOL;
                }
            }
        }
        
        $log->setInfo($info);

        return $this->save($log);
    }

    /**
     * BaseManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager = null)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('boxRepository', MyBoxesBoxLog::class);
    }

    /**
     * Save a myBoxesBoxLog
     *
     * @param MyBoxesBoxLog $myBoxesBoxLog
     */
    public function save(MyBoxesBoxLog $myBoxesBoxLog)
    {
        $myBoxesBoxLog->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($myBoxesBoxLog);
        $this->getEntityManager()->flush();

        return $myBoxesBoxLog;
    }

    /**
     * Remove one myBoxesBoxLog
     *
     * @param MyBoxesBoxLog $myBoxesBoxLog
     */
    public function remove(MyBoxesBoxLog $myBoxesBoxLog)
    {
        $this->getEntityManager()->remove($myBoxesBoxLog);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->boxRepository->findOneBy($filters);
    }

    public function findBy($filters = array(), $orderBy = array())
    {
        return $this->boxRepository->findBy($filters, $orderBy);
    }
}
