<?php

/**
 * Manager
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@bigyouth.fr>
 */

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;
use App\Utils\Managers\LocaleManager;
use App\Entity\ExamplePage;
use App\Repository\ExamplePageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class ExamplePageManager
 * `
 * Object manager of simple pages
 *
 * @package Disko\ConfiguratorBundle\Managers
 */
class ExamplePageManager extends BaseManager
{
    /**
     * Repository
     *
     * @var ExamplePageRepository
     */
    protected $examplePageRepository;

    /**
     * ExampleManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('examplePageRepository', ExamplePage::class);
    }

    /**
     * Find the only one page
     *
     * @return ExamplePage
     */
    public function findTheOnlyOne()
    {
        $results = $this->examplePageRepository->findAll();

        if (count($results) <= 0) {
            $examplePage = new ExamplePage();
        } else {
            $examplePage = $results[0];
        }

        return $examplePage;
    }

    /**
     * Save
     *
     * @param ExamplePage $examplePage
     *
     * @return ExamplePage
     */
    public function save(ExamplePage $examplePage)
    {
        $examplePage->setUpdated(new \Datetime());
        $this->getEntityManager()->persist($examplePage);
        $this->getEntityManager()->flush();

        $this->clearResultCache();

        return $examplePage;
    }

    /**
     * Remove one examplePage
     *
     * @param ExamplePage $examplePage
     */
    public function remove(ExamplePage $examplePage)
    {
        $this->getEntityManager()->remove($examplePage);
        $this->getEntityManager()->flush();
    }
}
