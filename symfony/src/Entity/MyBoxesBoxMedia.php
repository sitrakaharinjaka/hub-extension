<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MyBoxesBoxMediaRepository")
 * @ORM\Table(name="dk_myboxes_box_media")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class MyBoxesBoxMedia
{
    use GedmoTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Box
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\MyBoxesBox", cascade={"persist"}, inversedBy="boxMedias")
     * @ORM\JoinColumn(name="box_id", referencedColumnName="id", onDelete="SET NULL")
     * @var MyBoxesBox
     */
    protected $box;

    /**
     * Cover
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="cover_id", referencedColumnName="id", onDelete="SET NULL")
     * @var Media
     */
    protected $cover;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Media
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * @param MyBoxesBox $box
     */
    public function setBox($box)
    {
        $this->box = $box;
    }

    /**
     * @return Media
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * @param Media $cover
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    }
}
