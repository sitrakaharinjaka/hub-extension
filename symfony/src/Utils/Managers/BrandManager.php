<?php

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;

use App\Utils\Managers\LocaleManager;
use App\Entity\Brand;
use App\Entity\BrandTranslation;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class BrandManager
 * `
 * Object manager of brand
 *
 * @package Disko\PageBundle\Managers
 */
class BrandManager extends BaseManager
{

    /**
     * Repository
     *
     * @var BrandRepository
     */
    protected $brandRepository;

    /**
     * BrandManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('brandRepository', Brand::class);
    }

    /**
     * Save a brand
     *
     * @param Brand $brand
     */
    public function save(Brand $brand)
    {
        $brand->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($brand);
        $this->getEntityManager()->flush();

        return $brand;
    }

    /**
     * Remove one brand
     *
     * @param Brand $brand
     */
    public function remove(Brand $brand)
    {
        $this->getEntityManager()->remove($brand);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one Active
     *
     * @param $slugWl
     * @param $locale
     * @return mixed
     */
    public function findOneActive($slugWl, $locale)
    {
        $brand = $this->brandRepository->findOneActive($slugWl, $locale);

        return $brand;
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $brand = $this->brandRepository->findOneToEdit($id);

        return $brand;
    }

    /**
     * Get all brand
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->brandRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->brandRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->brandRepository->findOneBy($filters);
    }



    /**
     * Find one by
     *
     * @param string $name
     * @return mixed
     */
    public function findOneByName($name)
    {
        return $this->brandRepository->findOneByName($name);
    }
}
