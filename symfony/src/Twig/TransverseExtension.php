<?php
namespace App\Twig;

class TransverseExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('transverse', array($this, 'getTransverse'))
        );
    }

    public function getName()
    {
        return 'transverse_extension';
    }

    public function getTransverse($object, $field, $class)
    {
        if ($object && $object->getTransverseFieldsToJson()) {
            $configFields = $object->getTransverseFieldsToJson();

            if (is_array($field) && !empty($field)) {
                if (array_key_exists($field[0], $configFields)) {
                    return $class;
                }
            } else {
                if (array_key_exists($field, $configFields)) {
                    return $class;
                }
            }
        }
        return "";
    }
}
