<?php

namespace App\Event;

use App\Utils\Managers\Fail2BanManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;

class PostFail2Ban implements EventSubscriberInterface
{
    const MAX_LOGIN_FAILURE_ATTEMPTS = 5;

    private $fail2BanManager;
    private $router;

    public function __construct(
        Fail2BanManager $fail2BanManager,
        SessionInterface $session,
        RouterInterface $router
    ) {
        $this->fail2BanManager = $fail2BanManager;
        $this->router = $router;
        $this->session = $session;
    }


    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['beforeFirewall', 10]
        ];
    }

    public function beforeFirewall(GetResponseEvent $event)
    {
        $request = $event->getRequest();


        if ($request->isMethod(Request::METHOD_POST)) {
            $routeInfos = $this->router->matchRequest($request);
            if (
                isset($routeInfos['_route']) &&
                ($routeInfos['_route'] === 'fos_user_security_login') or ($routeInfos['_route'] === 'fos_user_security_check')
            ) {
                if ($this->fail2BanManager->countBadLogBefore($request->getClientIp()) >= PostFail2Ban::MAX_LOGIN_FAILURE_ATTEMPTS) {
                    $this->session->getFlashBag()->add('error', 'Ban');
                    $response = new RedirectResponse($this->router->generate('fos_user_security_login'));
                    $event->setResponse($response);
                }
            }
        }
    }
}
