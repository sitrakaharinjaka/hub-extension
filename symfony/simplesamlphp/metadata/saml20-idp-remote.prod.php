<?php
/**
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote 
 */
$metadata['https://saml.moet-hennessy.net/adfs/services/trust'] = array (
    'entityid' => 'https://saml.moet-hennessy.net/adfs/services/trust',
    'contacts' =>
        array (
            0 =>
                array (
                    'contactType' => 'support',
                ),
        ),
    'metadata-set' => 'saml20-idp-remote',
    'SingleSignOnService' =>
        array (
            0 =>
                array (
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                    'Location' => 'https://saml.moet-hennessy.net/adfs/ls/',
                ),
            1 =>
                array (
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                    'Location' => 'https://saml.moet-hennessy.net/adfs/ls/',
                ),
        ),
    'SingleLogoutService' =>
        array (
            0 =>
                array (
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                    'Location' => 'https://saml.moet-hennessy.net/adfs/ls/',
                ),
            1 =>
                array (
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                    'Location' => 'https://saml.moet-hennessy.net/adfs/ls/',
                ),
        ),
    'ArtifactResolutionService' =>
        array (
        ),
    'NameIDFormats' =>
        array (
            0 => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
            1 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
            2 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
        ),
    'keys' =>
        array (
            0 =>
                array (
                    'encryption' => true,
                    'signing' => false,
                    'type' => 'X509Certificate',
                    'X509Certificate' => 'MIIHJjCCBQ6gAwIBAgITIwAAALoTVBQbmj1s+wAAAAAAujANBgkqhkiG9w0BAQsFADBIMRQwEgYKCZImiZPyLGQBGRYEbHZtaDESMBAGCgmSJomT8ixkARkWAm1oMRwwGgYDVQQDExNNSCBTdWJvcmRpbmF0ZSBDQSAxMB4XDTE5MDIwNDEwMjA0N1oXDTIzMDIwMzEwMjA0N1owWDELMAkGA1UEBhMCRlIxEDAOBgNVBAcTB0VwZXJuYXkxDTALBgNVBAoTBE1IQ1MxDjAMBgNVBAsTBU1hcm5lMRgwFgYDVQQDEw9BREZTIEVuY3J5cHRpb24wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQClZ/D4gMsuXgU+MCwftYP+Bf4MGgodTtTqK2UDjXp3iExqkfjfCW5/xOKlU7zOOClHdGu2IlhLj3iMonhHx6bQ9Y+pJBG98iMEZQY++E4I434i6+J3rileR/fTzkldB5WJhc3FAJ8vcJTpXPpwilteykmVGmyipVahdwqfuM53S0X1yLdhNd0LUKhIiXhWKpWm2PQd97WBAGQNw5T8oSVWfPOx0SeAiQGOEk01Jsc7pWVE+tEhVoh20VcDmcThLo3WZpgnqmKNw5y0+RdX0OfK6WgdMlwtcE0OrgSOh/gUcn/BzZPSsHZP2OfQCi2wXvW+uSkp6/dmP92KMFQpgGDJAgMBAAGjggL3MIIC8zAOBgNVHQ8BAf8EBAMCBaAwPQYJKwYBBAGCNxUHBDAwLgYmKwYBBAGCNxUIg8LHJIeKqFyFzYsQhL7oVYGhrAqBEoG1mVmM02oCAWQCAQ0wHQYDVR0OBBYEFJ8so08MZKN8H+ke+t5skl38cpTtMB8GA1UdIwQYMBaAFLLW7TB5rVAQkyuZXR+JS2hbNuX/MIIBDQYDVR0fBIIBBDCCAQAwgf2ggfqggfeGgb5sZGFwOi8vL0NOPU1IJTIwU3Vib3JkaW5hdGUlMjBDQSUyMDEsQ049TUhTVUJDQTAxLENOPUNEUCxDTj1QdWJsaWMlMjBLZXklMjBTZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9uLERDPW1oLERDPWx2bWg/Y2VydGlmaWNhdGVSZXZvY2F0aW9uTGlzdD9iYXNlP29iamVjdENsYXNzPWNSTERpc3RyaWJ1dGlvblBvaW50hjRodHRwOi8vY3JsLm1oLmx2bWgvcGtpL01IJTIwU3Vib3JkaW5hdGUlMjBDQSUyMDEuY3JsMIIBHQYIKwYBBQUHAQEEggEPMIIBCzCBtAYIKwYBBQUHMAKGgadsZGFwOi8vL0NOPU1IJTIwU3Vib3JkaW5hdGUlMjBDQSUyMDEsQ049QUlBLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENOPUNvbmZpZ3VyYXRpb24sREM9bWgsREM9bHZtaD9jQUNlcnRpZmljYXRlP2Jhc2U/b2JqZWN0Q2xhc3M9Y2VydGlmaWNhdGlvbkF1dGhvcml0eTBSBggrBgEFBQcwAoZGaHR0cDovL2NybC5taC5sdm1oL3BraS9NSFNVQkNBMDEubWgubHZtaF9NSCUyMFN1Ym9yZGluYXRlJTIwQ0ElMjAxLmNydDATBgNVHSUEDDAKBggrBgEFBQcDATAbBgkrBgEEAYI3FQoEDjAMMAoGCCsGAQUFBwMBMA0GCSqGSIb3DQEBCwUAA4ICAQAwoUqwpl+p5H3M0caAdqB3cxyZMR14Ie1hQX6KEEGRJLiYeHVndpBrHDm14roQLY4dzby19/ABIwdlMGKeFcbV8LRSTNnPhmcVji+DNCLaEnAz6K/jHwGqazLqIwZxvBrlYil4+z1AyLCvcEF5o+OsPIVxmmoUodmrYHLfNGlIwk2K3ofN/BIgeDNSgwSHLb5BOCGRCFTJEqGJCHiB9ruTquxgAMMxdfaCY8172ZxY9dSChiEoumX8CSAyxOfJSOA2d1wnUGxTakM/Xy83aXN/3Ow6A1+VX1VG5VY1xL89wuj89+vNyfNFY346KrkWWM0nrvXZ1W5IUCilJi8q0/j1xkEI7ewEei7b0tzSOPPwB6VEcVLrIwkMqeQUNs7JhnSwwNPqmRx27MnWt53XoZZbc4MUNCe6tIv6BgZg9e8Js7ugiQ3gzVFBNTQOEPO6jdogd7lhgBVfDKUFdQEOMY5gHzTe4Ho15f/KuB4M8SrWs/LP8XG9mJdfjekfr3kbzwzJfTwV7FUJ4Ppcq2SYbIARJJXlvWGI6Z1ZPQXmBt5iX02W7AyFv2K9BY0NeQyz23Of6bOvPJ+ioxR13fFYqaFrcr/eZsp6hGzIwJWqnBi18tKO4/coBhAdxLAR9awiDr3FiJFy6GO0R/1DYU74vxIPm8RJ/sG2nr5mC5FzgZPp3A==',
                ),
            1 =>
                array (
                    'encryption' => false,
                    'signing' => true,
                    'type' => 'X509Certificate',
                    'X509Certificate' => 'MIIHIzCCBQugAwIBAgITIwAAALsl/TobiWFWGwAAAAAAuzANBgkqhkiG9w0BAQsFADBIMRQwEgYKCZImiZPyLGQBGRYEbHZtaDESMBAGCgmSJomT8ixkARkWAm1oMRwwGgYDVQQDExNNSCBTdWJvcmRpbmF0ZSBDQSAxMB4XDTE5MDIwNDEwMjI0NVoXDTIzMDIwMzEwMjI0NVowVTELMAkGA1UEBhMCRlIxEDAOBgNVBAcTB0VwZXJuYXkxDTALBgNVBAoTBE1IQ1MxDjAMBgNVBAsTBU1hcm5lMRUwEwYDVQQDEwxBREZTIFNpZ25pbmcwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCRyIiBN71wieOk7yIL8DseKDTtw5gkxjMnCZX6axZatbbO6jsI7X9dZ2c5bTpts8wo+f4Ku64m4qiWSzfM0q8Yd7AWqlhOL0aQEgDSYi8/jpbd/FhHCcwG39jIXa1Rp/E+YXWH/54WG8DDxNw36Kaq/H0XweDj8Sor/ixMdnNJ3CltLIxmhdvMV7J9rW/9XZsBgEUV5QS6e8zPbRtp3vIgPT2FcfDj7s5EVWJ9BChmhc7OCtOl3mWjUecsP6+vhyEU6JKdYRO0lo5TyLR8IHyun3vDFiczlFoi1/H+pZBc78FWVVfyw2bU0PnnTDkYmGcPLnNMV+vq+OQNbkDK4Pf1AgMBAAGjggL3MIIC8zAOBgNVHQ8BAf8EBAMCBaAwPQYJKwYBBAGCNxUHBDAwLgYmKwYBBAGCNxUIg8LHJIeKqFyFzYsQhL7oVYGhrAqBEoG1mVmM02oCAWQCAQ0wHQYDVR0OBBYEFLu4nXymhT8uCZZtxJZVYutlYeP4MB8GA1UdIwQYMBaAFLLW7TB5rVAQkyuZXR+JS2hbNuX/MIIBDQYDVR0fBIIBBDCCAQAwgf2ggfqggfeGgb5sZGFwOi8vL0NOPU1IJTIwU3Vib3JkaW5hdGUlMjBDQSUyMDEsQ049TUhTVUJDQTAxLENOPUNEUCxDTj1QdWJsaWMlMjBLZXklMjBTZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9uLERDPW1oLERDPWx2bWg/Y2VydGlmaWNhdGVSZXZvY2F0aW9uTGlzdD9iYXNlP29iamVjdENsYXNzPWNSTERpc3RyaWJ1dGlvblBvaW50hjRodHRwOi8vY3JsLm1oLmx2bWgvcGtpL01IJTIwU3Vib3JkaW5hdGUlMjBDQSUyMDEuY3JsMIIBHQYIKwYBBQUHAQEEggEPMIIBCzCBtAYIKwYBBQUHMAKGgadsZGFwOi8vL0NOPU1IJTIwU3Vib3JkaW5hdGUlMjBDQSUyMDEsQ049QUlBLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENOPUNvbmZpZ3VyYXRpb24sREM9bWgsREM9bHZtaD9jQUNlcnRpZmljYXRlP2Jhc2U/b2JqZWN0Q2xhc3M9Y2VydGlmaWNhdGlvbkF1dGhvcml0eTBSBggrBgEFBQcwAoZGaHR0cDovL2NybC5taC5sdm1oL3BraS9NSFNVQkNBMDEubWgubHZtaF9NSCUyMFN1Ym9yZGluYXRlJTIwQ0ElMjAxLmNydDATBgNVHSUEDDAKBggrBgEFBQcDATAbBgkrBgEEAYI3FQoEDjAMMAoGCCsGAQUFBwMBMA0GCSqGSIb3DQEBCwUAA4ICAQCOkIwCyGpiXBBWhY4JNpApCMKk8OKFLpR16GRGZRFvhgnfDyora5hfxmKoFrx0FZi7dcQdYoWZuNrJS7PbE3kPNOdo4ZhLRMEQi60ycok/11lESHvha9ZQnKGBHCWqbvzMMgEJ2AV/yfVVSZh61fzJ4toXQDCkdtrlCJ6ziLj3fja17i+yqMi94ax85KDLFMcZXJlpGmnH4NHheFBT+Ug9X7aGLhDR6zefy9xGhxAMYab2rCV2vyYyNrFnRkep+Nk6G/XLvDxwaT8gMA+djGapsXdn6eEd3SlXY/fIFx3f0boJYWHKt1SHGF95hw7cychsO489ciXitLpjzOkkiq/zF3Okau1U5w+RXkwGKh8l03VQuaKBdwdbgA7U1xsYC5HyE/47ubSRVGq6nUVK4L2HOIS1OzpdBI+lBYV0cxovzUjsZgcXH+2VlwobadC0jqMYPseFnXmLl2mogB+tiFEc9/7p+ehLkPhQM51sX0DX5uRUgHnzIhOqEx2GcDS/0duG3g3BKN+vKZxCkbOPP4tFC90DxtWWqCwRACX6G6qBgqWJQNAGCrZrbru4+UPrKhU07MnpFlv11pgySfw1lmeMRSm4oyWK+pswC+Cl4oPKI7dBf+/s2IQubG5lruTOmwoyTDn8ho7HOUs3ve622i9MiqYTxnUbTyRvYuqJ/4sg9w==',
                ),
        ),
);

