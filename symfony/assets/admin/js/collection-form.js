/**
 * Collections form
 */
function addFormDeleteLink($formLi, $collectionHolder) {
    var $removeFormA = $('<div style="display: inline-block;"><a href="#" class="btn btn-danger delete-button"><i class="fa fa-trash-o pull-left"></i>' + delete_label + '</a></div>');
    if($formLi.find('.delete-button').length === 0) {
        $formLi.append($removeFormA);
        $removeFormA.on('click', function(e) {
            e.preventDefault();
            var parent = $(this).parent('div').parent('div.well').parent('div.collections');
            var nbElement = parent.children('div.well').length;
            $formLi.remove();
            var limit = parseInt(parent.data('limit'));
            if (limit !== 0 && nbElement === limit) {
                addFormAddLink($collectionHolder);
            }
        });
    }
}

function addForm($collectionHolder, $newLinkLi) {
    var prototype = $collectionHolder.data('prototype');
    var index = parseInt($collectionHolder.data('index')) + 1;
    var nbElement = $collectionHolder.children('div.well').length;
    var limit = parseInt($collectionHolder.data('limit'));
    var newForm = prototype;
    newForm = newForm.replace(/__name__/g, index);
    $collectionHolder.data('index', index);
    var $newFormLi = $('<div class="well" style="display: inline-block; width: 100%; margin-left: 5px; vertical-align: top; padding: 19px 0 0 0;"></div>').append(newForm);

    $newLinkLi.before($newFormLi);
    if (limit !== 0 && nbElement === limit) {
        $newLinkLi.remove();
    }
    addFormDeleteLink($newFormLi, $collectionHolder);
    loadWysiwyg($newFormLi.find('.editor-wysiwyg'), true);
    $(".select2-single:not(.hide)").select2({
        allowClear: true
    });
    if ($newFormLi.find('div.subcollections').length > 0) {
        var $collectionHolderSub = $newFormLi.find('div.subcollections');
        addFormAddLink($collectionHolderSub);
        $collectionHolderSub.data('index', $collectionHolderSub.children('div.well').length);
    }
}

function addFormAddLink($collectionHolder)
{
    var $addLink = $('<a href="#" class="add_link btn btn-success"><i class="fa fa-plus"></i> ' + add_label + '</a>');
    var $newLinkLi = $('<div class="addbtn"></div>').append($addLink);
    $addLink.on('click', function(e) {
        e.preventDefault();
        addForm($collectionHolder, $newLinkLi);
    });
    $collectionHolder.append($newLinkLi);

}

function initCollection()
{
    $('div.collections').each(function() {
        var $collectionHolder = $(this);
        $collectionHolder.find('div.well').each(function() {
            addFormDeleteLink($(this));
        });
        var limit = parseInt($collectionHolder.data('limit'));
        var linkAdd = true;
        if (limit !== 0 && $collectionHolder.children('div.well').length === limit) {
            linkAdd = false;
        }
        if (linkAdd) {
            addFormAddLink($collectionHolder);
        }
        $collectionHolder.data('index', $collectionHolder.children('div.well').length);
    });
}

jQuery(document).ready(function() {
    initCollection();
});