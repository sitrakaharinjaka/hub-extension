<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@Disko.fr>
 */

namespace App\Controller\Admin;

use App\Utils\Managers\LocaleManager;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MenuController
 *
 * @package Disko\AdminBundle\Controller
 */
class MenuController extends BaseController
{
    /**
    /**
     * Versions
     */
    public function version(Request $request, LocaleManager $localeManager, $route = 'admin_homepage', $routeParams = [], $queryParams = [])
    {
        // Get Restaurant for menu
        $locales = $localeManager->findAllList();

        // Render view
        return $this->render('@Admin/partials/version.html.twig', array(
            'locales' => $locales,
            'route' => $route,
            'routeParams' => $routeParams,
            'queryParams' => $queryParams,
        ));
    }
}
