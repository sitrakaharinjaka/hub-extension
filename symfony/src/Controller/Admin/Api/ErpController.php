<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin\Api;

use App\Controller\BaseController;
use App\Form\Model\SearchLogFile;
use App\Form\Type\LogFile\SearchLogFileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ErpController
 * @Route("/api/products")
 * @package App\Controller
 */
class ErpController extends BaseController
{
    /**
     * Info about
     * @Route("/update", name="admin_erp_api_update")
     * @param Request $request Request
     */
    public function update(Request $request)
    {
        // @todo
    }
}
