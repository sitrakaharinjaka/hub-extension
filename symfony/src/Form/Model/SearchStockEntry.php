<?php


namespace App\Form\Model;


class SearchStockEntry
{
    /**
     * @var ?int
     */
    private $id;

    /**
     * @var ?string
     */
    private $stock;

    /**
     * @var ?string
     */
    private $requester;

    /**
     * @var ?string
     */
    private $code;

    /**
     * @var ?string
     */
    private $status;

    /**
     * @var
     */
    private $createdAt;

    /**
     * @var
     */
    private $delivery;

    /**
     * Get search data
     */
    public function getSearchData(): array
    {
        $tab = [];

        if (!empty($this->id)) {
            $tab['id'] = $this->id;
        }
        if (!empty($this->stock)) {
            $tab['stock'] = $this->stock;
        }
        if (!empty($this->requester)) {
            $tab['requester'] = $this->requester;
        }
        if (!empty($this->code)) {
            $tab['code'] = $this->code;
        }
        if (!empty($this->status)) {
            $tab['status'] = $this->status;
        }
        if (!empty($this->createdAt)) {
          $tab['createdAt'] = $this->createdAt;
        }
        if (!empty($this->delivery)) {
          $tab['delivery'] = $this->delivery;
        }

        return $tab;
    }

  /**
   * @return int|null
   */
  public function getId(): ?int
    {
        return $this->id;
    }

  /**
   * @param int|null $id
   * @return $this
   */
  public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

  /**
   * @return string|null
   */
  public function getStock(): ?string
    {
        return $this->stock;
    }

  /**
   * @param string|null $stock
   * @return $this
   */
  public function setStock(?string $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

  /**
   * @return string|null
   */
  public function getRequester(): ?string
    {
        return $this->requester;
    }

  /**
   * @param string|null $requester
   * @return $this
   */
  public function setRequester(?string $requester): self
    {
        $this->requester = $requester;

        return $this;
    }

  /**
   * @return string|null
   */
  public function getCode(): ?string
    {
        return $this->code;
    }

  /**
   * @param string|null $code
   * @return $this
   */
  public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

  /**
   * @return string|null
   */
  public function getStatus(): ?string
    {
        return $this->status;
    }

  /**
   * @param string|null $status
   * @return $this
   */
  public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

      /**
       * @return mixed
       */
      public function getCreatedAt(): ?\DateTimeInterface
      {
        return $this->createdAt;
      }

      /**
       * @param mixed $createdAt
       * @return SearchStockEntry
       */
      public function setCreatedAt(?\DateTimeInterface $createdAt)
      {
        $this->createdAt = $createdAt;
        return $this;
      }


      /**
       * @return mixed
       */
      public function getDelivery()
      {
        return $this->delivery;
      }

      /**
       * @param mixed $delivery
       * @return SearchStockEntry
       */
      public function setDelivery($delivery)
      {
        $this->delivery = $delivery;
        return $this;
      }
}
