<?php

namespace App\Command;

use App\Command\AbstractCommand;
use App\Utils\Managers\StockManager;
use App\Utils\Services\MailerService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\Project;
use App\Utils\Managers\ProjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

// use Symfony\Component\EventDispatcher\EventDispatcher;

class StockFixCommand extends AbstractCommand
{
    protected static $defaultName = 'disko:stock:fix';

    private $projectManager;

    public function __construct(EntityManagerInterface $em, MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, StockManager $stockManager)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->stockManager     = $stockManager;
        $this->em = $em;
    }


    protected function configure()
    {
        $this
            ->setDescription('Fix stock issues')
        ;
    }

    /**
     * @return int|void|null
     * @throws \Exception
     */
    protected function executeCommand()
    {
        $this->log('info', "<info>Fix</info> stock issues");

        $this->em->getFilters()->disable('softdeleteable');
        //$this->em->getFilters()->disable('softdeleteable');
        $stock = $this->stockManager->findOneToEdit(36);
        //$fr = $stock->getTranslations('fr');

      $originalEventListeners = [];

// cycle through all registered event listeners
        foreach ($this->em->getEventManager()->getListeners() as $eventName => $listeners) {
          foreach ($listeners as $listener) {
            if ($listener instanceof \Gedmo\SoftDeleteable\SoftDeleteableListener) {

              // store the event listener, that gets removed
              $originalEventListeners[$eventName] = $listener;

              // remove the SoftDeletableSubscriber event listener
              $this->em->getEventManager()->removeEventListener($eventName, $listener);
            }
          }
        }
        $this->em->remove($stock);
        $this->em->flush();
        //$fr->setName('test');

        $this->log('info', "<info>END</info>");
    }
}
