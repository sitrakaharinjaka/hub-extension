<?php

namespace App\Command\Veolog\MyBoxes;

use App\Command\Veolog\AbstractVeologCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\MyBoxesBox;
use App\Entity\MyBoxesCommand;
use App\Utils\Managers\MyBoxesBoxManager;
use App\Utils\Managers\MyBoxesCommandManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SynchOrderConfirmCommand extends AbstractVeologCommand
{

    /** @var  EntityManager */
    private $boxManager;
    private $commandManager;

    protected static $defaultName = 'veolog:myboxes:order-confirm';

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, MyBoxesBoxManager $boxManager, MyBoxesCommandManager $commandManager)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->boxManager = $boxManager;
        $this->commandManager = $commandManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Synch veolog order confirm for My Boxes')
            ->addOption(
                'vlgenv',
                null,
                InputOption::VALUE_OPTIONAL,
                'Env veolog prod?',
                false
            )
            ->setHelp('This command allows you to synch veolog order confirm evry 5minutes');
    }

    /**
     * On veux les fichiers
     * - status_orders_BOX_AAMMJJHHMMSS.csv
     * - /XXX/status_orders_BOX_AAMMJJHHMMSS.csv
     * On ne veux pas :
     * - XXXstatus_orders_BOX_AMMJJHHMMSS.csv
     * - status_orders_BOX_AMMJJHHMMSS.xls
     * - status_orders_AAMMJJHHMMSS.csv
     */
    private function isCommandFile($filename)
    {
        $pattern='~^/?([a-zA-Z0-9_]*/)*status_orders_BOX_[0-9]*\.csv$~';
        return preg_match($pattern, $filename);
    }

    private function getErrorFile($filename)
    {
        return str_replace(".csv", "_ERROR.csv", $filename);
    }

    /**
     * Get all files for status order on FTP directory
     */
    private function getFTPFiles($ftp_handle, $directory)
    {
        $filenames = ftp_nlist($ftp_handle, $directory);
        sort($filenames);

        $avalaible_file = [];
        //check to take only status_order_files
        foreach ($filenames as $filename) {
            if ($this->isCommandFile($filename)) {
                $avalaible_file[] = $filename;
            }
        }

        return $avalaible_file;
    }

    /**
     * Do all the actions to take into account the box:
     * - saving
     */
    private function closeCommand($command)
    {
        $box = null;
        if ($command !== null) {
            $box = $command->getBox();
            $this->commandManager->save($command);
        }

        if ($box !== null) {
            $this->boxManager->save($box);
        }

        if ($box != null) {
            $this->log('info', '    Sending email to BOX recipient');
            if ($box->getState() == MyBoxesBox::STATE_BOX_VEOLOG_ACCEPTED) {
                $this->mailer->sendMailMyBoxesBoxAccepted($box);
            } elseif ($box->getState() == MyBoxesBox::STATE_BOX_VEOLOG_SENT) {
                $this->mailer->sendMailMyBoxesBoxSent($box);
            }
        } elseif ($command !== null) {
            $this->log('info', '    Sending email to COMMAND recipient');
            if ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_ACCEPTED) {
                $this->mailer->sendMailMyBoxesCommandAccepted($command);
            } elseif ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_SENT) {
                $this->mailer->sendMailMyBoxesCommandSent($command);
            }
        }
    }

    private function getIdFromVeologCode($code)
    {
        $exploded = explode('_', $code);
        return intval($exploded[sizeof($exploded)-1]);
    }

    protected function executeCommand()
    {
        $delimiter = ";";

        $directory = '/' . $this->getDirVlgOUT() . '/';

        $errors = [];

        try {
            $this->log('info', 'Scan directory ' . $directory . ' to find csv');
            $ftp_handle = $this->connectFTP();
            $this->log('info', 'Connexion <info>success</info>');

            $avalaible_file = $this->getFTPFiles($ftp_handle, $directory);
            $this->log('info', count($avalaible_file) . ' file(s) find into directory '.$directory);
            foreach ($avalaible_file as $file) {
                $tmp_handle = fopen('php://temp', 'r+');
                if (ftp_fget($ftp_handle, $tmp_handle, $file, FTP_ASCII)) {
                    rewind($tmp_handle);

                    $box = null;
                    $command = null;
                    $items = null;
                    while ($csv_row = fgetcsv($tmp_handle, 0, $delimiter)) {
                        $type_line = $csv_row[0];
                        if ($type_line == 'E') {
                            if ($box != null) {
                                // Je suis sur une nouvelle box.
                                // Pour le moment, ne devrait pas arriver
                                // Mais au cas ou; je clos la précédente.
                                $this->closeCommand($command);
                            }
                            $entete = $csv_row;
                            $id = $this->getIdFromVeologCode($entete[2]);
                            $this->log('info', 'Commande '.$entete[2]. '(#' . $id . ')');
                            $command = $this->commandManager->findOneToEdit($id);
                            if ($command == null) {
                                $errors[] = "Commande ".$entete[2]." non trouvée";
                                $this->log('info', '    <comment>NOT FOUND</comment>');
                                $box = null;
                                $items = null;
                            } else {
                                $box = $command->getBox();
                                $items = $command->getItems();
                                if ($items !==null && sizeof($items)==0) {
                                    $items = null;
                                }
                            }

                            if ($command !== null) {
                                switch ($entete[6]) {
                                    case 'I':
                                        if ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_ACCEPTED) {
                                            // Bizarre, mais Rien A Faire
                                            $this->log('info', '    command already on state '.$command->getState().' <info>sucess</info>');
                                        } elseif ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_PREPARED || $command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_SENT) {
                                            // Il y a un soucis d'ordre, mais a priori Rien A Faire.
                                            $this->log('info', '    command already on state '.$command->getState().' (no update to ' . MyBoxesBox::STATE_CMD_VEOLOG_ACCEPTED . ') <info>sucess</info>');
                                        } elseif ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_REFUSED || $command->getState() == MyBoxesBox::STATE_CMD_USER_ASKED || $command->getState() == MyBoxesBox::STATE_CMD_USER_PREPARED) {
                                            $command->setState(MyBoxesBox::STATE_CMD_VEOLOG_ACCEPTED);
                                            $this->log('info', '    updating command to '.$command->getState().' <info>sucess</info>');
                                        } else {
                                            $errors[] = "La commande " . $command->getId() . " a reçu un statut " . $entete[6] . " alors qu'elle est dans une statut " . $command->getState();
                                            $this->log('info', '    Unable to set command state to ' . MyBoxesBox::STATE_CMD_VEOLOG_ACCEPTED . ' from '.$command->getState().' <error>error</error>');
                                        }
                                        break;
                                    case 'A':
                                        if ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_REFUSED) {
                                            // Bizarre, mais Rien A Faire
                                            $this->log('info', '    command already on state '.$command->getState().' <info>sucess</info>');
                                        } elseif ($command->getState() == MyBoxesBox::STATE_CMD_USER_ASKED || $command->getState() == MyBoxesBox::STATE_CMD_USER_PREPARED) {
                                            $command->setState(MyBoxesBox::STATE_CMD_VEOLOG_REFUSED);
                                            $this->log('info', '    updating command to '.$command->getState().' <info>sucess</info>');
                                        } else {
                                            $errors[] = "La commande " . $command->getId() . " a reçu un statut " . $entete[6] . " alors qu'elle est dans une statut " . $command->getState();
                                            $this->log('info', '    Unable to set command state to ' . MyBoxesBox::STATE_CMD_VEOLOG_REFUSED . ' from '.$command->getState().' <error>error</error>');
                                        }
                                        break;
                                    case 'P':
                                        if ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_PREPARED) {
                                            // Bizarre, mais Rien A Faire
                                            $this->log('info', '    command already on state '.$command->getState().' <info>sucess</info>');
                                        } elseif ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_SENT) {
                                            // Il y a un soucis d'ordre, mais a priori Rien A Faire.
                                            $this->log('info', '    command already on state '.$command->getState().' (no update to ' . MyBoxesBox::STATE_CMD_VEOLOG_PREPARED . ') <info>sucess</info>');
                                        } elseif ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_ACCEPTED || $command->getState() == MyBoxesBox::STATE_CMD_USER_ASKED || $command->getState() == MyBoxesBox::STATE_CMD_USER_PREPARED) {
                                            $command->setState(MyBoxesBox::STATE_CMD_VEOLOG_PREPARED);
                                            $this->log('info', '    updating command to '.$command->getState().' <info>sucess</info>');
                                        } else {
                                            $errors[] = "La commande " . $command->getId() . " a reçu un statut " . $entete[6] . " alors qu'elle est dans une statut " . $command->getState();
                                            $this->log('info', '    Unable to set command state to ' . MyBoxesBox::STATE_CMD_VEOLOG_PREPARED . ' from '.$command->getState().' <error>error</error>');
                                        }
                                        break;
                                    case 'X':
                                        if ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_SENT) {
                                            // Bizarre, mais Rien A Faire
                                            $this->log('info', '    command already on state '.$command->getState().' <info>sucess</info>');
                                        } elseif ($command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_ACCEPTED || $command->getState() == MyBoxesBox::STATE_CMD_VEOLOG_PREPARED || $command->getState() == MyBoxesBox::STATE_CMD_USER_ASKED || $box->getState() == MyBoxesBox::STATE_CMD_USER_PREPARED) {
                                            $command->setState(MyBoxesBox::STATE_CMD_VEOLOG_SENT);
                                            $this->log('info', '    updating command to '.$command->getState().' <info>sucess</info>');
                                        } else {
                                            $errors[] = "La commande " . $command->getId() . " a reçu un statut " . $entete[6] . " alors qu'elle est dans une statut " . $command->getState();
                                            $this->log('info', '    Unable to set command state to ' . MyBoxesBox::STATE_CMD_VEOLOG_SENT . ' from '.$command->getState().' <error>error</error>');
                                        }
                                        break;
                                }
                            }

                            switch ($command->getState()) {
                                case MyBoxesBox::STATE_BOX_VEOLOG_ACCEPTED:
                                    $statutBox = 'Integrée';
                                    break;
                                    // Integré
                                case MyBoxesBox::STATE_BOX_VEOLOG_REFUSED:
                                    $statutBox = 'Abandonnée';
                                    break;
                                    // Abandonné
                                case MyBoxesBox::STATE_BOX_VEOLOG_PREPARED:
                                    $statutBox = 'En Préparation';
                                    break;
                                    // Préparé
                                case MyBoxesBox::STATE_BOX_VEOLOG_SENT:
                                    $statutBox = 'Expédiée';
                                    break;
                                    // Expédié
                                case MyBoxesBox::STATE_BOX_VEOLOG_STOCK:
                                    $statutBox = 'En stock chez veolog';
                                    break;
                                    // En stock chez veolog
                                case MyBoxesBox::STATE_BOX_USER_STOCK:
                                    $statutBox = 'Chez le collaborateur';
                                    break;
                                    // Chez le user
                                case MyBoxesBox::STATE_BOX_USER_ASK:
                                    $statutBox = 'Rapatriment demandé par le user';
                                    break;
                                    // Rapatriment demandé par le user
                                case MyBoxesBox::STATE_BOX_USER_SENT:
                                    $statutBox = 'Envoyé par le user';
                                    break;
                                    // Envoyé par le user
                                default:
                                    $statutBox = $command->getState();
                                    break;
                            }

                            if ($box !== null) {
                                switch ($entete[6]) {
                                    case 'I':
                                        $statutEntete = 'Integrée';
                                        if ($box->getState() == MyBoxesBox::STATE_BOX_VEOLOG_ACCEPTED) {
                                            // Bizarre, mais Rien A Faire
                                            $this->log('info', '    box already on state '.$box->getState().' <info>sucess</info>');
                                        } elseif ($box->getState() == MyBoxesBox::STATE_BOX_VEOLOG_PREPARED || $box->getState() == MyBoxesBox::STATE_BOX_VEOLOG_SENT) {
                                            // Il y a un soucis d'ordre, mais a priori Rien A Faire.
                                            $this->log('info', '    box already on state '.$box->getState().' (no update to ' . MyBoxesBox::STATE_BOX_VEOLOG_ACCEPTED . ') <info>sucess</info>');
                                        } elseif ($box->getState() == MyBoxesBox::STATE_BOX_VEOLOG_REFUSED || $box->getState() == MyBoxesBox::STATE_BOX_USER_ASK || $box->getState() == MyBoxesBox::STATE_BOX_USER_SENT) {
                                            $box->setState(MyBoxesBox::STATE_BOX_VEOLOG_ACCEPTED);
                                            $this->log('info', '    updating box to '.$box->getState().' <info>sucess</info>');
                                        } else {
                                            $errors[] = "La box " . $box->getBarcode() . " a reçu un statut \"" . $statutEntete . "\" alors qu'elle est dans une statut \"" . $statutBox . "\"";
                                            $this->log('info', '    Unable to set box state to ' . MyBoxesBox::STATE_BOX_VEOLOG_ACCEPTED . ' from '.$box->getState().' <error>error</error>');
                                        }
                                        break;
                                    case 'A':
                                        $statutEntete = 'Abandonnée';
                                        if ($box->getState() == MyBoxesBox::STATE_BOX_VEOLOG_REFUSED) {
                                            // Bizarre, mais Rien A Faire
                                            $this->log('info', '    box already on state '.$box->getState().' <info>sucess</info>');
                                        } elseif ($box->getState() == MyBoxesBox::STATE_BOX_USER_ASK || $box->getState() == MyBoxesBox::STATE_BOX_USER_SENT) {
                                            $box->setState(MyBoxesBox::STATE_BOX_VEOLOG_REFUSED);
                                            $this->log('info', '    updating command to '.$box->getState().' <info>sucess</info>');
                                        } else {
                                            $errors[] = "La box " . $box->getBarcode() . " a reçu un statut \"" . $statutEntete . "\" alors qu'elle est dans une statut \"" . $statutBox . "\"";
                                            $this->log('info', '    Unable to set box state to ' . MyBoxesBox::STATE_BOX_VEOLOG_REFUSED . ' from '.$box->getState().' <error>error</error>');
                                        }
                                        break;
                                    case 'P':
                                        $statutEntete = 'En Préparation';
                                        if ($box->getState() == MyBoxesBox::STATE_BOX_VEOLOG_PREPARED) {
                                            // Bizarre, mais Rien A Faire
                                            $this->log('info', '    box already on state '.$box->getState().' <info>sucess</info>');
                                        } elseif ($box->getState() == MyBoxesBox::STATE_BOX_VEOLOG_SENT) {
                                            // Il y a un soucis d'ordre, mais a priori Rien A Faire.
                                            $this->log('info', '    box already on state '.$box->getState().' (no update to ' . MyBoxesBox::STATE_BOX_VEOLOG_PREPARED . ') <info>sucess</info>');
                                        } elseif ($box->getState() == MyBoxesBox::STATE_BOX_VEOLOG_ACCEPTED || $box->getState() == MyBoxesBox::STATE_BOX_USER_ASK || $box->getState() == MyBoxesBox::STATE_BOX_USER_SENT) {
                                            $box->setState(MyBoxesBox::STATE_BOX_VEOLOG_PREPARED);
                                            $this->log('info', '    updating box to '.$box->getState().' <info>sucess</info>');
                                        } else {
                                            $errors[] = "La box " . $box->getBarcode() . " a reçu un statut \"" . $statutEntete . "\" alors qu'elle est dans une statut \"" . $statutBox . "\"";
                                            $this->log('info', '    Unable to set box state to ' . MyBoxesBox::STATE_BOX_VEOLOG_PREPARED . ' from '.$box->getState().' <error>error</error>');
                                        }
                                        break;
                                    case 'X':
                                        $statutEntete = 'Expédiée';
                                        if ($box->getState() == MyBoxesBox::STATE_BOX_VEOLOG_SENT) {
                                            // Bizarre, mais Rien A Faire
                                            $this->log('info', '    box already on state '.$box->getState().' <info>sucess</info>');
                                        } elseif ($box->getState() == MyBoxesBox::STATE_BOX_VEOLOG_ACCEPTED || $box->getState() == MyBoxesBox::STATE_BOX_VEOLOG_PREPARED || $box->getState() == MyBoxesBox::STATE_BOX_USER_ASK || $box->getState() == MyBoxesBox::STATE_BOX_USER_SENT) {
                                            $box->setState(MyBoxesBox::STATE_BOX_VEOLOG_SENT);
                                            $this->log('info', '    updating box to '.$box->getState().' <info>sucess</info>');
                                        } else {
                                            $errors[] = "La box " . $box->getBarcode() . " a reçu un statut \"" . $statutEntete . "\" alors qu'elle est dans une statut \"" . $statutBox . "\"";
                                            $this->log('info', '    Unable to set box state to ' . MyBoxesBox::STATE_BOX_VEOLOG_SENT . ' from '.$box->getState().' <error>error</error>');
                                        }
                                        break;
                                }
                            }
                            if ($items !== null) {
                                // Rien de plus à mettre à jour
                            }
                        }
                        if ($type_line == 'C') {
                            $this->log('info', '    tracker : '.$csv_row[3].' <info>sucess</info>');
                            if ($command !== null) {
                                $command->addTracker($csv_row[3]);
                            }
                            if ($box !== null) {
                                $box->addTracker($csv_row[3]);
                            }
                            if ($items !== null) {
                                // Rien de plus à mettre à jour
                            }
                        }
                        if ($type_line == 'L') {
                            // Rien à mettre à jour
                        }
                    }
                    $this->closeCommand($command);
                } else {
                    $this->log('info', 'erreur while get csv file');
                }
                fclose($tmp_handle);

                // Effacement du fichier $file
                if (sizeof($errors)>0) {
                    ftp_rename($ftp_handle, $file, $this->getErrorFile($file));
                    $this->log('info', ' <comment>renamed</comment>');
                } elseif (ftp_delete($ftp_handle, $file)) {
                    $this->log('info', '    Deleting file ' . $file . ' <info>success</info>');
                } else {
                    $this->log('info', '    <comment>Error during Deleting files ' . $file . '</comment>');
                }
            }
            ftp_close($ftp_handle);
        } catch (\Exception $e) {
            $this->log('info', "<error>Failure: " . $e->getMessage() . "</error>");
        }
        if (sizeof($errors)>0) {
            $this->log('info', 'Sending mail with errors');
            $this->mailer->sendMailMyBoxesErrorConfirm($errors);
            $this->log('info', ' <info>success</info>');
        }
    }
}
