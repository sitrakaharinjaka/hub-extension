<?php

/**
 * Repository
 *
 * @author Pierre Baumes <pierre.b@disko.fr>
 */

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class ServiceProviderTranslationRepository
 *
 * @package App\Repository
 */
class ServiceProviderTranslationRepository extends BaseRepository
{
}
