<?php

namespace App\Listener;

use App\Entity\IP;
use App\Utils\Managers\IPManager;
use App\Utils\Managers\StockManager;
use App\Utils\Managers\UserManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Listener to check stock
 */
class StockListener implements EventSubscriberInterface
{

    /**
     * @var StockManager
     */
    private $stockManager;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;


    /**
     * Constructor
     */
    public function __construct(
        UserManager $userManager,
        StockManager $stockManager,
        RouterInterface $router,
        SessionInterface $session,
        TokenStorageInterface $token,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->userManager = $userManager;
        $this->stockManager = $stockManager;
        $this->router = $router;
        $this->session = $session;
        $this->token = $token;
        $this->authorizationChecker = $authorizationChecker;
    }

    private function getOneStockOrNull()
    {
        $stocks = $this->stockManager->findAll();
        if (count($stocks) > 0) {
            return $stocks[0];
        }
        return null;
    }

    private function getStockOrDefaultStockOrNull($key, $user)
    {
        $stock = $this->session->get($key, null);
        if (!$stock) {
            $stock = $user->getDefaultAdminStock();
        }
        return $stock;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $user = false;
        $token = $this->token->getToken();
        if ($token) {
            $user = $token->getUser();
        }

        if ($event->isMasterRequest() && is_object($user)) {
            $isGeneralAdmin = $this->authorizationChecker->isGranted('ROLE_GENERAL_ADMIN');
            $isVeologAdmin = $this->authorizationChecker->isGranted('ROLE_VEOLOG_ADMIN');
            $isStockAdmin = count($user->getAdminStocks()) > 0;
            $isRunner = $this->authorizationChecker->isGranted('ROLE_RUNNER');
            $isUser = $this->authorizationChecker->isGranted('ROLE_USER');

            $isAdminURL = preg_match('/\/admin/', $event->getRequest()->getRequestUri()) == 1;
            $isRunnerURL = preg_match('/\/admin\/runner/', $event->getRequest()->getRequestUri()) == 1;
            $isLoginURL = preg_match('/\/login/', $event->getRequest()->getRequestUri()) == 1;
            $isSAMLURL = preg_match('/\/saml/', $event->getRequest()->getRequestUri()) == 1;
            $isRightURL = preg_match('/\/droits/', $event->getRequest()->getRequestUri()) == 1;
            $isMyBoxesURL = preg_match('/\/myboxes/', $event->getRequest()->getRequestUri()) == 1;

            if ($isSAMLURL or $isLoginURL) {
                # Do nothing, this URL no require right
            } elseif ($isRightURL or $isMyBoxesURL) {
                if ($isGeneralAdmin or $isVeologAdmin or $isStockAdmin) {
                    // DO Nothing : le super admin est un runner, mais qui a les droits d'aller sur myboxes.
                } elseif ($isRunner) {
                    // Il est runner, on redirige vers sa homepage.
                    $response = new RedirectResponse($this->router->generate('runner_homepage'));
                    $event->setResponse($response);
                }
            } elseif ($isAdminURL) {
                if ($isGeneralAdmin or $isVeologAdmin or $isStockAdmin) {
                    $stock = $this->getStockOrDefaultStockOrNull('current_admin_stock', $user);
                    if (!$stock) {
                        if ($isGeneralAdmin || $isVeologAdmin) {
                            $stock = $this->getOneStockOrNull();
                        } else {
                            $response = new RedirectResponse($this->router->generate('right'));
                            $event->setResponse($response);
                        }
                    }
                    $this->session->set('current_admin_stock', $stock);
                } elseif ($isRunner) {
                    // Il est runner, on redirige vers sa homepage.
                    if (!$isRunnerURL) {
                        $response = new RedirectResponse($this->router->generate('runner_homepage'));
                        $event->setResponse($response);
                    }
                } else {
                    $response = new RedirectResponse($this->router->generate('right'));
                    $event->setResponse($response);
                }
            } else {
                $stock = $this->getStockOrDefaultStockOrNull('current_stock', $user);
                if (!$stock) {
                    if ($isGeneralAdmin) {
                        $stock = $this->getOneStockOrNull();
                    } elseif ($isRunner) {
                        // Il est runner, on redirige vers sa homepage.
                        $response = new RedirectResponse($this->router->generate('runner_homepage'));
                        $event->setResponse($response);
                    } elseif (!$isUser) {
                        // Il n'a pas de droit de user, donc pas accès ...
                        $response = new RedirectResponse($this->router->generate('right'));
                        $event->setResponse($response);
                    }
                }
                $this->session->set('current_stock', $stock);
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            //'kernel.request' => 'onKernelRequest',
            KernelEvents::REQUEST => array(
                array('onKernelRequest', 1),
            ),
        );
    }
}
