<?php

namespace App\Listener;

use App\Utils\Managers\LocaleManager;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\RouterInterface;

class SitemapListener implements EventSubscriberInterface
{

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var LocaleManager
     */
    private $localeManager;

    /**
     * SitemapSubscriber constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router, LocaleManager $localeManager)
    {
        $this->router = $router;
        $this->localeManager = $localeManager;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            SitemapPopulateEvent::ON_SITEMAP_POPULATE => 'populate',
        ];
    }

    /**
     * @param SitemapPopulateEvent $event
     */
    public function populate(SitemapPopulateEvent $event): void
    {
        $container = $event->getUrlContainer();

        $locales = $this->localeManager->findAll();

        foreach ($locales as $locale) {
            $container->addUrl(
                new UrlConcrete(
                    $this->router->generate(
                        'homepage',
                        ['_locale' =>  strtolower($locale->getCode())],
                        RouterInterface::ABSOLUTE_URL
                    )
                ),
                strtolower($locale->getCode())
            );
        }
    }
}
