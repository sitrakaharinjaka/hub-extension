<?php

namespace App\Utils\Managers;

use App\Entity\ProjectItem;
use App\Entity\User;
use App\Form\Model\BulkOrder;
use App\Repository\ProjectRepository;
use App\Repository\ProjectRejectionRepository;

use App\Entity\Project;
use App\Entity\ProjectRejection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Utils\Services\MailerService;

/**
 * Class ProjectManager
 * `
 * Object manager of project
 *
 * @package Disko\PageBundle\Managers
 */
class ProjectManager extends BaseManager
{

  /**
   * Repository
   *
   * @var ProjectRepository
   */
    protected $projectRepository;

  /**
   * Repository
   *
   * @var ProjectRejectionRepository
   */
  protected $projectRejectionRepository;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var StockLineManager
     */
    protected $stockLineManager;

    /**
     * @property  mailer
     */
    private $mailer;

    protected $ENVIRONNEMENT;

    /**
     * ProjectManager constructor.
     *
     * @param  EntityManagerInterface  $em
     */
    public function __construct(
    EntityManagerInterface $em,
    Filesystem $fileSystem,
    LocaleManager $localeManager,
    EventDispatcherInterface $dispatcher,
    StockLineManager $stockLineManager,
    MailerService $mailer,
    ParameterBagInterface $parameter
  ) {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('projectRepository', Project::class);
        $this->addRepository('projectRejectionRepository', ProjectRejection::class);
        $this->dispatcher       = $dispatcher;
        $this->stockLineManager = $stockLineManager;
        $this->mailer           = $mailer;
        $this->ENVIRONNEMENT = $parameter->get('server_mode');
    }

    /**
     * Save a project
     *
     * @param  Project  $project
     */
    public function save(Project $project)
    {
        $project->setUpdated(new \DateTime());

        foreach ($project->getItems() as $item) {
            $this->getEntityManager()->persist($item);
        }

        $this->getEntityManager()->persist($project);
        $this->getEntityManager()->flush();

        return $project;
    }

    /**
     * @param $projects
     */
    public function saveAll($projects)
    {
        foreach ($projects as $project) {
            $this->save($project);

            //Create stockline entry for bulk order children. @see https://bugkiller.disko.fr/issues/71430
            $this->stockLineManager->generateFromProject($project);
        }
    }

    /**
     * Remove one project
     *
     * @param  Project  $project
     */
    public function remove(Project $project)
    {
        $this->stockLineManager->removeFromProject($project);
        $this->getEntityManager()->remove($project);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one Active
     *
     * @param $slugWl
     * @param $locale
     *
     * @return mixed
     */
    public function findOneActive($slugWl, $locale)
    {
        $project = $this->projectRepository->findOneActive($slugWl, $locale);

        return $project;
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $project = $this->projectRepository->findOneToEdit($id);

        return $project;
    }

    /**
     * Find all project with return (and email return) in 1 day
     *
     * @return mixed
     */
    public function findProjectsWithReturnToWarn()
    {
        return $this->projectRepository->findProjectsWithReturnToWarn();
    }

    /**
     * Get all project
     *
     * @param  array  $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->projectRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->projectRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array<string, mixed> $filters
     * @return ?Project
     */
    public function findOneBy(array $filters = []): ?Project
    {
        return $this->projectRepository->findOneBy($filters);
    }

    /**
     * Find by
     *
     * @param  array  $filters
     *
     * @return mixed
     */
    public function findBy($filters = array())
    {
        return $this->projectRepository->findBy($filters);
    }

    /**
     * @param $user
     * @param $stock
     *
     * @return null
     */
    public function findOneByUserStockLatest($user, $stock)
    {
        if (! $stock) {
            return null;
        }

        return $this->projectRepository->findOneByUserStockLatest($user, $stock);
    }


    /**
     * @param $user
     * @param $stock
     *
     * @return null
     */
    public function findProjectAvailableByUser($user, $stock)
    {
        return $this->projectRepository->findProjectAvailableByUser($user, $stock);
    }


    /**
     * @param $user
     * @param $stock
     * @param $projects_id
     *
     * @return null
     */
    public function findProjectStateByUser($user, $stock, $projects_id)
    {
        return $this->projectRepository->findProjectStateByUser($user, $stock, $projects_id);
    }


    /**
     * @param $id
     *
     * @return null
     */
    public function findStateById($id)
    {
      return $this->projectRepository->findStateById($id);
    }


  /**
     * @param $user
     * @param $stock
     *
     * @return null
     */
    public function findProjectbyState($state, $cave)
    {
        return $this->projectRepository->findProjectbyState($state, $cave);
    }

    public function findProjectsToWarnVeolog($since = null)
    {
        return $this->projectRepository->findProjectsToWarnVeolog($since);
    }

    /**
     * @param $user
     * @param $stock
     *
     * @return null
     */
    public function findProjectbyStateNoSendingToVlg($state, $cave)
    {
        return $this->projectRepository->findProjectbyStateNoSendingToVlg($state, $cave);
    }

    /**
     * @param $user
     * @param $stock
     *
     * @return null
     */
    /*public function findProjectNoIntegrateByVeolog()
    {
      return $this->projectRepository->findProjectNoIntegrateByVeolog();
    }*/

    /**
     * Delete project but not really
     *
     * @param  Project  $project
     * @param  null  $from
     */
    public function delete(Project $project, $from = null)
    {
        $this->remove($project);

        /**
         * $this->dispatcher->dispatch(
         * ProjectEvent::PROJECT_DELETE,
         * new ProjectEvent($project, $from)
         * );**/
    }


    /**
     * Delete project but not really
     *
     * @param  Project  $project
     * @param  null  $from
     */
    public function deleteInState(Project $project, $from = null)
    {
        $this->stockLineManager->removeFromProject($project);

        $contacts = $project->getStock()->translate('fr');

        $mails    = [];
        foreach ($contacts->getTranslatable()->getAdminUsers()->getValues() as $contact) {
            $mails[] = $contact->getEmail();
        }
        if ($contacts->getTranslatable()->getReferentUser() != null) {
            $mails[] = $contacts->getTranslatable()->getReferentUser()->getEmail();
        }
        $emails = implode(",", $mails);
        //$emails .=",edson.galina@disko.fr,mateo@disko.fr";
        if ($this->ENVIRONNEMENT == 'PREPROD' || $this->ENVIRONNEMENT == 'PP' || $this->ENVIRONNEMENT == 'RECETTE' || $this->ENVIRONNEMENT == 'REC') {
            $emails .= ",a.bertolino@veolog.fr";
        }else{
            $emails .= ",hub142@veolog.fr";
        }

        if ($project->getCave() == Project::CAVE_PANTIN) {

            $statesHubCancelAsk = [
                Project::STATE_HUB_DRAFT,
                Project::STATE_HUB_ACCEPTED,
                Project::STATE_HUB_REFUSED,
                Project::STATE_HUB_ACCEPTED_FIXED
            ];

            if (in_array($project->getState(), $statesHubCancelAsk)) {
                $project->setState(Project::STATE_HUB_DRAFT_CANCEL_ASK);
            } else {
                $project->setState(Project::STATE_HUB_CANCEL_ASK);
            }

            $this->mailer->sendMailDeleteProjectToRefAndVeolog($project, $emails);
            $this->save($project);
        } elseif ($project->getCave() == Project::CAVE_115) {
            // TODO CAVE_115 : Qui prévenir comment ?
            $project->setState(Project::STATE_HUB_CANCEL);
            $this->save($project);
        }
    }

    /**
     * @param $project
     *
     * @return array
     */
    public function getParameters($project)
    {
        $cave      = null;
        $dateStart = new \DateTime();
        $dateStart->modify('+1 days');

        $dateEnd = new \DateTime();
        $dateEnd->modify('+2 days');

        if ($project) {
            $cave      = $project->getCave();
            $dateStart = $project->getDateStart();
            $dateEnd   = $project->getDateEnd();
        }

        return [
      'cave'      => $cave,
      'dateStart' => $dateStart,
      'dateEnd'   => $dateEnd
    ];
    }

    /**
     * @param  Project  $project
     * @param  ProjectItem  $projectItem
     */
    public function addToProject(Project $project, ProjectItem $projectItem)
    {
        foreach ($project->getItems() as $key => $existingItem) {
            if ($projectItem->getProduct()->getId() == $existingItem->getProduct()
                                                              ->getId()
      ) {
                $existingItem->setQuantity($existingItem->getQuantity()
                                   + $projectItem->getQuantity());


                $project = $this->save($project);
                if ($project->getState() != Project::STATE_HUB_DRAFT) {
                    $this->stockLineManager->generateFromProject($project);
                }

                return;
            }
        }
        $project->addItem($projectItem);
        $project = $this->save($project);
        if ($project->getState() != Project::STATE_HUB_DRAFT) {
            $this->stockLineManager->generateFromProject($project);
        }
    }


    /**
     * @param  Project  $project
     */
    public function book(Project $project)
    {
        if (! in_array($project->getState(), [Project::STATE_HUB_ACCEPTED_FIXED])) {
            if ($project->getCave() == Project::CAVE_115) {
                $project->setState(Project::STATE_HUB_ACCEPTED_FIXED);
                $this->mailer->sendMailCave115ValidateProject($project);
                $this->save($project);
                $this->stockLineManager->generateFromProject($project);
            } else {
                $project->setState(Project::STATE_HUB_ACCEPTED);
                $contacts = $project->getStock()->translate('fr');
                $mails    = [];
                foreach ($contacts->getTranslatable()->getAdminUsers()->getValues() as $contact) {
                    $mails[] = $contact->getEmail();
                }

                if ($contacts->getTranslatable()->getReferentUser() != null) {
                    $mails[] = $contacts->getTranslatable()->getReferentUser()->getEmail();
                }
                if ($project->getUser() != null) {
                    $mails[] = $project->getUser()->getEmail();
                }

                $emails = "" . implode(",", $mails) . "";

                switch ($project->getType()) {
                    case Project::TYPE_BASIC:
                        $dateDelay = clone $project->getDateStart();
                        $dateDelay->modify(Project::DELAY_VEOLOG_STUN);
                        $today = new \DateTime();
                        if ($dateDelay < $today) {
                            $project->setState(Project::STATE_HUB_ACCEPTED_FIXED);
                        }
                        $this->mailer->sendMailValidateProjectToRef($project, $emails);
                        break;

                    case Project::TYPE_SHUTTLE:
                        //$project->setState(Project::STATE_HUB_ACCEPTED_FIXED);
                        $dateDelay = clone $project->getDateStart();
                        $dateDelay->modify(Project::DELAY_VEOLOG_STUN);
                        $today = new \DateTime();
                        if ($dateDelay < $today) {
                            $project->setState(Project::STATE_HUB_ACCEPTED_FIXED);
                        }
                        $this->mailer->sendMailValidateProjectToRef($project, $emails);
                        break;

                    case Project::TYPE_URGENCY:
                        $project->setState(Project::STATE_HUB_ACCEPTED_FIXED);
                        $this->mailer->sendMailValidateProjectToRef($project, $emails);
                        break;
                }
                $this->save($project);
                $this->stockLineManager->generateFromProject($project);
            }
        }
    }

    /**
     * @param Project $project
     * @param $row
     * @return Project|mixed
     */
    public function createOrFindProject(array $previouslyCreated, $row)
    {
        foreach ($previouslyCreated as $subProject) {
            if (
                $subProject->getRecipientLastName() == $row[0]
                && $subProject->getRecipientFirstname() == $row[1]
                && $subProject->getRecipientPhone() == $row[2]
                && $subProject->getRecipientEmail() == $row[3]
                && $subProject->getRecipientSociety() == $row[4]
                && $subProject->getRecipientAddress() == $row[5]
                && $subProject->getRecipientAddressComplement() == $row[6]
                && $subProject->getRecipientZip() == $row[7]
                && $subProject->getRecipientCity() == $row[8]
            ) {
                return $subProject;
            }
        }
        return new Project();
    }

    /**
     * @param Project $projectParent
     * @param $row
     * @param ProductManager $productManager
     * @return Project|null
     */
    public function createBulkChildProject(Project $projectParent, $row, ProductManager $productManager, $previouslyCreated)
    {
        $product = null;

        foreach ($row as $key => $value) {
            if ($key == 12) {
                $product = $productManager->checkExist($row[12]);

                if (null == $product) {
                    return null;
                };

                $product = $productManager->checkCodeAndName($value, $row[11]);

                if (null == $product) {
                    return null;
                };
            }

            // if ($key != 3 && $key != 4 && $key != 6 && $key != 14 && $value == null) {
            //     return null;
            // }

            // Check required cells
            $requiredCells = [0, 1, 2, 5, 7, 8, 9, 10, 13];
            if (in_array($key, $requiredCells)) {
                if(empty($value) || is_null($value)){
                    return null;
                }
            }

            //Validate phone number
            if($key == 2) {
                $value = preg_replace('/\s+/', '', $value);

                $phoneFirstCharacter = substr($value, 0, 1);

                $value = (int) $phoneFirstCharacter != 0? 0 . $value: $value;
            }

            //Trim whitespaces and remove newlines
            $value = trim($value);
            $value = preg_replace("/\r|\n/", "", $value);
            //Trim '"'
            $row[$key] = trim($value, '"');
        }

        $projectChild = self::createOrFindProject($previouslyCreated, $row);
        $item         = new ProjectItem();

        $projectChild->setName($projectParent->getName());
        $projectChild->setType(Project::TYPE_BASIC);
        $projectChild->setDateStart($projectParent->getDateStart());
        $projectChild->setNeedReturn($projectParent->getNeedReturn());
        $projectChild->setDateEnd($projectParent->getDateEnd());
        $projectChild->setUser($projectParent->getUser());
        $projectChild->setBillingCenter($projectParent->getBillingCenter());
        $projectChild->setStock($projectParent->getStock());
        $projectChild->setNeedQuote($projectParent->isNeedQuote());
        $projectChild->setBeforeQuote($projectParent->getBeforeQuote());
        $projectChild->setState($projectParent->getState());
        foreach ($projectParent->getBeforeServices() as $beforeService) {
            $projectChild->addBeforeService($beforeService);
        }

        $projectChild->setRecipientLastName($row[0]);
        $projectChild->setRecipientFirstName($row[1]);
        $projectChild->setRecipientPhone($row[2]);
        $projectChild->setRecipientEmail($row[3]);
        $projectChild->setRecipientSociety($row[4]);
        $projectChild->setRecipientAddress($row[5]);
        $projectChild->setRecipientAddressComplement($row[6]);
        $projectChild->setRecipientZip($row[7]);
        $projectChild->setRecipientCity($row[8]);
        $projectChild->setRecipientCountry($row[9]);
        $item->setQuantity(intval($row[13]));
        $item->setProduct($product);

        $projectChild->addItem($item);

        $projectChild->setParent($projectParent);

        return $projectChild;
    }

    /**
     * @param BulkOrder $bulkOrder
     * @param User $user
     * @return Project
     */
    public function createBulkParentProject(BulkOrder $bulkOrder, User $user)
    {
        $project = new Project();

        $project->setUser($user);
        $project->setName($bulkOrder->getProjectName());
        $project->setDateStart($bulkOrder->getDeliveryDate());
        $project->setDateEnd($project->getDateStart());
        $project->setNeedReturn(false);
        $project->setBillingCenter($bulkOrder->getBillingCenter());
        $project->setStock($bulkOrder->getStock());
        $project->setState(Project::STATE_HUB_ACCEPTED_FIXED);
        $project->setNeedQuote($bulkOrder->isNeedQuote());
        $project->setBeforeQuote($bulkOrder->getBeforeQuote());
        $project->setRecipientLastName($user->getLastName());
        $project->setRecipientFirstName($user->getFirstName());
        $project->setRecipientPhone($user->getPhone());
        $project->setRecipientEmail($user->getEmail());
        $project->setRecipientCountry($user->getCountry());

        foreach ($bulkOrder->getBeforeServices() as $beforeService) {
            $project->addBeforeService($beforeService);
        }

        $project->setType(Project::TYPE_BULK);

        return $project;
    }

    /**
     * @param $parentProjectId
     * @return mixed
     */
    public function findSubProjectsByParentId($parentProjectId)
    {
        $subProjects = $this->projectRepository->findSubProjectsByParentId($parentProjectId);

        return $subProjects;
    }

    public function statsByHouse($start, $end)
    {
        return $this->projectRepository->statsByHouse($start, $end);
    }


    public function statsByService($start, $end, $type = 'before')
    {
        return $this->projectRepository->statsByService($start, $end, $type);
    }

    public function statsByCountry($start, $end)
    {
        return $this->projectRepository->statsByCountry($start, $end);
    }

    public function statsByDelay($start, $end, $type = 'urgency')
    {
        return $this->projectRepository->statsByDelay($start, $end, $type);
    }

    public function statsByType($start, $end, $type = 'urgency')
    {
        return $this->projectRepository->statsByType($start, $end, $type);
    }


    public function topByDelay($start, $end, $limit = 15, $name = '')
    {
        return $this->projectRepository->topByDelay($start, $end, $limit, $name);
    }

    public function findByCave($start, $end, $cave)
    {
        return $this->projectRepository->findByCave($start, $end, $cave);
    }

    public function statsCreated($start, $end, $cave)
    {
        return $this->projectRepository->statsCreated($start, $end, $cave);
    }

    public function statsDeliveryTimeProject($start, $end)
    {
        return $this->projectRepository->statsDeliveryTimeProject($start, $end);
    }

    public function statsCreatedProject($start, $end, $cave)
    {
        return $this->projectRepository->statsCreatedProject($start, $end, $cave);
    }

    public function count($filters)
    {
        return $this->projectRepository->count($filters);
    }

    public function clearRejections($project)
    {
        return $this->projectRejectionRepository->clearByProject($project);
    }
}
