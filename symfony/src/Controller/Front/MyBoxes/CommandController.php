<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Front\MyBoxes;

use App\Controller\BaseController;
use App\Entity\MyBoxesBox;
use App\Entity\MyBoxesCommand;
use App\Entity\Project;
use App\Form\Type\MyBoxes\CommandType;
use App\Form\Type\MyBoxes\BringHomeType;
use App\Utils\Managers\MyBoxesBoxLogManager;
use App\Utils\Managers\MyBoxesBoxManager;
use App\Utils\Managers\MyBoxesCommandManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\Services\MailerService;

/**
 * Class CommandController
 *
 * @Route("/myboxes")
 *
 * @package App\Controller
 */
class CommandController extends BaseController
{

    /**
     * @Route("/commande", name="myboxes_command_create")
     *
     * @return Response
     */
    public function create(Request $request, MyBoxesCommandManager $boxesCommandManager, MailerService $mailerService)
    {
        $command = new MyBoxesCommand();
        $command->setUser($this->getUser());
        $form = $this->createForm(CommandType::class, $command, ["validation_groups" => "Create"]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                $boxesCommandManager->extractQuantityBoxes($command);
                $command->setState(MyBoxesBox::STATE_CMD_USER_PREPARED);
                $boxesCommandManager->save($command);

                /**
                 * Check box stock
                 * https://bugkiller.disko.fr/issues/66407
                 */
                foreach ($command->getItems() as $item) {
                   if(is_object($item)){
                       if($item->getProduct()->getStock() < 30){
                           $mailerService->sendWarnFewBoxProducts($item->getProduct());
                       }
                   }
                }

                $this->addFlash('flash_mb', "Votre demande de box vide a bien été prise en compte. Elle vous sera livrée lors du passage de la prochaine navette Hub142.");

                return $this->redirect($this->generateUrl('myboxes_homepage'));
            }
        }

        return $this->render('hub/myboxes/command/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/box/{id}/rapatrier", name="myboxes_command_box")
     *
     * @return Response
     */
    public function bringHome(Request $request, MyBoxesCommandManager $boxesCommandManager, MyBoxesBoxManager $boxManager, MyBoxesBoxLogManager $myBoxesBoxLogManager, $id)
    {
        $box = $boxManager->findForView($id);

        $command = new MyBoxesCommand();
        $command->setUser($this->getUser());
        $command->setBox($box);
        $form = $this->createForm(BringHomeType::class, $command, ["validation_groups" => "BringHome"]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                $boxesCommandManager->extractQuantityBoxes($command);
                $command->setState(MyBoxesBox::STATE_CMD_USER_PREPARED);
                $boxesCommandManager->save($command);

                $box->setState(MyBoxesBox::STATE_BOX_USER_ASK);
                $boxManager->save($box);

                $myBoxesBoxLogManager->log($box, 'repatriation');

                $this->addFlash('flash_mb', "Votre demande a bien été envoyée");

                return $this->redirect($this->generateUrl('myboxes_homepage'));
            }
        }

        return $this->render('hub/myboxes/command/bring-home.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
