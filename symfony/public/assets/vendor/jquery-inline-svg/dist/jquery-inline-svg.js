(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    (factory());
}(this, (function () { 'use strict';

    var cache = {};

    (function wrapJquery($) {
        $.fn.inlineSvg = function fnInlineSvg() {
            this.each(imgToSvg);

            return this;
        };

        function imgToSvg() {
            var $img = $(this);
            var src = $img.attr('src');

            // fill cache by src with promise
            if (!cache[src]) {
                var d = $.Deferred();
                $.get(src, function (data) {
                    d.resolve($(data).find('svg'));
                });
                cache[src] = d.promise();
            }

            // replace img with svg when cached promise resolves
            cache[src].then(function (svg) {
                var $svg = $(svg).clone();

                if ($img.attr('id')) $svg.attr('id', $img.attr('id'));
                if ($img.attr('class')) $svg.attr('class', $img.attr('class'));
                if ($img.attr('style')) $svg.attr('style', $img.attr('style'));

                if ($img.attr('width')) {
                    $svg.attr('width', $img.attr('width'));
                    if (!$img.attr('height')) $svg.removeAttr('height');
                }
                if ($img.attr('height')) {
                    $svg.attr('height', $img.attr('height'));
                    if (!$img.attr('width')) $svg.removeAttr('width');
                }

                $svg.insertAfter($img);
                $img.trigger('svgInlined', $svg[0]);
                $img.remove();
            });
        }
    })(jQuery);

})));
