<?php

namespace App\Validator\Constraints;

use App\Entity\Project;
use App\Entity\ProjectItem;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockLineManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProjectServicesValidator extends ConstraintValidator
{
    /**
     * @var ProjectManager
     */
    private $projectManager;

    /**
     * StockClassValidator constructor.
     */
    public function __construct(ProjectManager $projectManager)
    {
        $this->projectManager = $projectManager;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var Project $project */
        $project = $value;

        $quote = $project->getBeforeQuote();
        $needQuote = $project->isNeedQuote();

        if (empty($quote) && $needQuote === true) {
            $this->context->buildViolation($constraint->message)->atPath('beforeQuote')->addViolation();
        }

        if (!empty($quote) && $needQuote === true) {
            $exp = explode('_', $quote);
            if (count($exp) != 2) {
                $this->context->buildViolation($constraint->messageFormat)->atPath('beforeQuote')->addViolation();
            } else {
                if (strlen($exp[0]) != 4) {
                    $this->context->buildViolation($constraint->messageFormat)->atPath('beforeQuote')->addViolation();
                } elseif (strlen($exp[1]) != 3) {
                    $this->context->buildViolation($constraint->messageFormat)->atPath('beforeQuote')->addViolation();
                }
            }
        }
    }
}
