<?php

namespace App\Validator\Constraints;

use App\Utils\Managers\MyBoxesBarcodeManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MyBoxesNumberMemberValidator extends ConstraintValidator
{

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($box, Constraint $constraint)
    {
        if ($box->getType() == "group") {
            $bu = $box->getBoxUsers();
            if (count($bu) < 2) {
                $this
                    ->context
                    ->buildViolation($constraint->message_min)
                    ->atPath('type')
                    ->addViolation()
                ;
            }
        }
    }
}
