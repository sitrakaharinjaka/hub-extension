<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\BillingCenter;
use App\Form\Model\SearchBillingCenter;
use App\Form\Type\BillingCenter\BillingCenterType;
use App\Form\Type\BillingCenter\SearchBillingCenterType;
use App\Utils\Managers\BillingCenterManager;
use App\Utils\Managers\LocaleManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BillingCenterController
 * @Route("/admin/billingcenters")
 * @package Disko\AdminBundle\Controller
 */
class BillingCenterController extends BaseController
{
    /**
     * BillingCenter's list
     * @Route("/list/{askLocale}", name="admin_billingcenter_list", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listAction(Request $request, BillingCenterManager $billingcenterManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchBillingCenterType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des centres de coûts');

        // Generate query
        $query = $billingcenterManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/BillingCenter/index.html.twig', array(
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchBillingCenter();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setName((isset($filters['name']))   ? $filters['name'] : '');

        return $data;
    }

    /**
     * Create a billingcenter
     * @Route("/create", name="admin_billingcenter_create")
     * @param Request $request Request
     *
     * @return Response
     */
    public function createAction(Request $request, BillingCenterManager $billingcenterManager, LocaleManager $localeManager)
    {
        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");

        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des centre de facturations", $this->get("router")->generate("admin_billingcenter_list"));
        $breadcrumbs->addItem("Création");


        // Init form
        $billingcenter = new BillingCenter();
        $form = $this->createForm(BillingCenterType::class, $billingcenter, [
            'validation_groups' => ['Default']
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($billingcenter->getTranslations()) > 0) {
                    $billingcenterManager->save($billingcenter);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Création terminée"
                    );

                    return $this->redirect($this->generateUrl('admin_billingcenter_edit', array(
                        'id' => $billingcenter->getId()
                    )));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            } else {

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'error',
                    "Le formulaire contient des erreurs ou est vide"
                );
            }
        }

        // View
        return $this->render(
            '@Admin/BillingCenter/create.html.twig',
            array(
                'form'           => $form->createView(),
                'billingcenter'           => $billingcenter,
            )
        );
    }

    /**
     * Update a billingcenter
     * @Route("/edit/{id}", name="admin_billingcenter_edit")
     * @param Request $request Request
     * @param integer $id      Id of billingcenter
     *
     * @return Response
     */
    public function editAction(Request $request, BillingCenterManager $billingcenterManager, LocaleManager $localeManager, $id)
    {
        // Find it
        /** @var BillingCenter $billingcenter */
        $billingcenter = $billingcenterManager->findOneToEdit($id);
        if (!$billingcenter) {
            throw $this->createNotFoundException('BillingCenter not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des centre de facturations", $this->get("router")->generate("admin_billingcenter_list"));
        $breadcrumbs->addItem("Modification");

        $form = $this->createForm(BillingCenterType::class, $billingcenter, [
            'validation_groups' => ['Default']
        ]);


        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($billingcenter->getTranslations()) > 0) {
                    $billingcenterManager->save($billingcenter);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Mise à jour effectuée"
                    );

                    return $this->redirect($request->headers->get('referer'));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/BillingCenter/edit.html.twig',
            array(
                'form'           => $form->createView(),
                'billingcenter'           => $billingcenter,
            )
        );
    }

    /**
     * Delete one billingcenter
     * @Route("/delete/{id}", name="admin_billingcenter_delete")
     * @param Request $request Request
     * @param integer $id      Id of billingcenter
     */
    public function deleteAction(Request $request, BillingCenterManager $billingcenterManager, $id)
    {
        // Find it
        /** @var BillingCenter $billingcenter */
        $billingcenter = $billingcenterManager->findOneToEdit($id);
        if (!$billingcenter) {
            throw $this->createNotFoundException('BillingCenter not found');
        }

        $billingcenterManager->remove($billingcenter);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été supprimé"
        );

        return $this->redirect($request->headers->get('referer'));
    }



    /**
     * Duplicate one billingcenter
     * @Route("/duplicate/{id}", name="admin_billingcenter_duplicate")
     * @param Request $request Request
     * @param integer $id      Id of billingcenter
     */
    public function duplicateAction(Request $request, BillingCenterManager $billingcenterManager, $id)
    {
        // Find it
        /** @var BillingCenter $billingcenter */
        $billingcenter = $billingcenterManager->findOneToEdit($id);
        if (!$billingcenter) {
            throw $this->createNotFoundException('BillingCenter not found');
        }

        $this->getDoctrine()->getManager()->detach($billingcenter);
        foreach ($billingcenter->getTranslations() as $translation) {
            $translation->setId(null);
            $translation->setSlug(null);
            $this->getDoctrine()->getManager()->detach($translation);
        }
        $billingcenterManager->save($billingcenter);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été dupliqué"
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Export csv
     * @Route("/export/{locale}", name="admin_billingcenter_export")
     * @param Request $request
     */
    public function exportAction(Request $request, BillingCenterManager $billingcenterManager, $locale)
    {
        $data = $this->initSearch($request);
        $query = $billingcenterManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Nom',
            'Date de création',
            'En ligne'
        ], ';');

        $locale = $request->get('locale', $request->getDefaultLocale());
        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->translate($locale)->getName(),
                ($result->translate($locale)->getCreated()) ? $result->translate($locale)->getCreated()->format('d/m/Y') : $result->getCreated()->format('d/m/Y'),
                $result->translate($locale)->getActive()
            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_'.$locale.'_billingcenters_export.csv"'
        ));
    }
}
