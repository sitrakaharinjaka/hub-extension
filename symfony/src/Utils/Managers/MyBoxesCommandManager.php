<?php

namespace App\Utils\Managers;

use App\Entity\MyBoxesItem;
use App\Repository\MyBoxesCommandRepository;
use App\Utils\Managers\BaseManager;

use App\Utils\Managers\LocaleManager;
use App\Entity\MyBoxesCommand;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Utils\Services\MailerService;

/**
 * Class MyBoxesCommandManager
 * `
 * Object manager of command
 */
class MyBoxesCommandManager extends BaseManager
{

  /**
   * Repository
   *
   * @var MyBoxesCommandRepository
   */
    protected $myBoxesCommandRepository;
    protected $myBoxesProductManager;

    /**
     * @property  mailer
     */
    private $mailer;

    /**
     * MyBoxesCommandManager constructor.
     *
     * @param  EntityManagerInterface  $em
     */
    public function __construct(
    EntityManagerInterface $em,
    Filesystem $fileSystem,
    LocaleManager $localeManager,
    MyBoxesProductManager $myBoxesProductManager,
    MailerService $mailer
  ) {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('myBoxesCommandRepository', MyBoxesCommand::class);
        $this->mailer           = $mailer;
        $this->myBoxesProductManager = $myBoxesProductManager;
    }

    /**
     * Get all example
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->myBoxesCommandRepository->queryForSearch($filters);
    }


    /**
     * Save a myBoxesCommand
     *
     * @param  MyBoxesCommand  $myBoxesCommand
     */
    public function save(MyBoxesCommand $myBoxesCommand)
    {
        $myBoxesCommand->setUpdated(new \DateTime());

        foreach ($myBoxesCommand->getItems() as $item) {
            $this->getEntityManager()->persist($item);
        }

        $this->getEntityManager()->persist($myBoxesCommand);
        $this->getEntityManager()->flush();

        return $myBoxesCommand;
    }

    /**
     * Remove one myBoxesCommand
     *
     * @param  MyBoxesCommand  $myBoxesCommand
     */
    public function remove(MyBoxesCommand $myBoxesCommand)
    {
        $this->getEntityManager()->remove($myBoxesCommand);
        $this->getEntityManager()->flush();
    }

    /**
     * @param MyBoxesCommand $myBoxesCommand
     */
    public function extractQuantityBoxes(MyBoxesCommand $myBoxesCommand)
    {
        foreach ($myBoxesCommand->getItems() as $item) {
            $quantity = $item->getQuantity();
            $product = $item->getProduct();

            $total = $product->getStock() - $quantity;
            if ($total <= 0) {
                $total = 0;
            }
            $product->setStock($total);
            $this->myBoxesProductManager->save($product);
        }
    }

    /**
     * @param $user
     * @param $stock
     *
     * @return null
     */
    public function findbyStateNoSendingToVlg($state, $type = 'product')
    {
        return $this->myBoxesCommandRepository->findbyStateNoSendingToVlg($state, $type);
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $myBoxesCommand = $this->myBoxesCommandRepository->findOneToEdit($id);

        return $myBoxesCommand;
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->myBoxesCommandRepository->findOneBy($filters);
    }

    /**
     * Find all by
     *
     * @param array $filters
     * @return mixed
     */
    public function findAllBy($filters = array())
    {
        return $this->myBoxesCommandRepository->findBy($filters);
    }

    /**
     * Find last command by user
     *
     * @param array $filters
     * @return mixed
     */
    public function findListableByUser($user)
    {
        return $this->myBoxesCommandRepository->findListableByUser($user);
    }
}
