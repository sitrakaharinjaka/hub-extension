<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MyBoxesBoxUserRepository")
 * @ORM\Table(name="dk_myboxes_box_user")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class MyBoxesBoxUser
{
    use GedmoTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Box
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\MyBoxesBox", cascade={"persist"}, inversedBy="boxUsers")
     * @ORM\JoinColumn(name="box_id", referencedColumnName="id", onDelete="SET NULL")
     * @var MyBoxesBox
     */
    protected $box;

    /**
     * Cover
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     * @var User
     */
    protected $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return MyBoxesBox
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * @param MyBoxesBox $box
     */
    public function setBox($box)
    {
        $this->box = $box;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}
