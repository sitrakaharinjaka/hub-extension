<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockEntryRepository")
 * @ORM\Table(name="dk_stock_entry")
 * @ORM\HasLifecycleCallbacks
 */
class StockEntry
{
    use GedmoTrait;
    use AuthorableTrait;

    public const STATE_ITEM_CREATE   = 'created';   // L'utilisateur à créé la demande pour le produit.
    public const STATE_ITEM_UPDATE   = 'updated';   // VEOLOG à reçu des produits.
    public const STATE_ITEM_VALIDATE = 'validated'; // L'utilisateur a validé la quantité reçue.
    public const STATE_ITEM_REJECT   = 'rejected';  // L'utilisateur a rejeté la quantité reçue.
    public const STATE_INT_VEOLOG   = 'integrated_by_veolog';  // VEOLOG a intégré l’annonce de réception.
    public const STATE_REJECT_VEOLOG   = 'rejected_by_veolog';  // VEOLOG a rejeté l’annonce de réception.

    public const PRODUCT_NOT_FOUND_VEOLOG_REJECTION = 'sku_not_found';
    public const DUPLICATE_STOCK_ENTRY_VEOLOG_REJECTION = 'duplicate';

    public const URL_STATE_ITEM_VALIDATE = 'validation'; // Status passé dans l'URL de confirmation "VALIDE"
    public const URL_STATE_ITEM_REJECT   = 'rejet'; // Status passé dans l'URL de confirmation "REJET"

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Username
     *
     * @ORM\Column(length=255, nullable=false)
     */
    protected $username;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", cascade={"persist"})
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $stock;

    /**
     * Email
     *
     * @ORM\Column(length=255, nullable=false)
     */
    protected $email;

    /**
     * Phone
     *
     * @ORM\Column(length=255, nullable=false)
     */
    protected $phone;

    /**
     * Carrier
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $carrier;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BillingCenter", cascade={"persist"})
     * @ORM\JoinColumn(name="billing_center_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $billingCenter;

    /**
     * DeliveryDate
     *
     * @var DateTime $deliveryDate
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $deliveryDate;

    /**
     * Time Slots
     *
     * @ORM\Column(length=255, nullable=false)
     */
    protected $timeSlots;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $user;

    /**
     * @var Collection|StockEntryItem[]
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\StockEntryItem",
     *     mappedBy="stockEntry",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     */
    protected $stockEntryItems;

    /**
     * @var \DateTimeInterface|null $dateSendToVeolog
     * @ORM\Column(name="date_send_to_veolog", type="date", nullable=true)
     */
    public $dateSendToVeolog;

    /**
     * @var Collection|StockEntryRejection[]
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\StockEntryRejection",
     *     mappedBy="stockEntry",
     *     cascade={"persist"},
     *     orphanRemoval=true
     * )
     */
    protected $rejections;


    public function __construct()
    {
        $this->stockEntryItems = new ArrayCollection();
        $this->rejections = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUserName($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock($stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * @param mixed $carrier
     */
    public function setCarrier($carrier): void
    {
        $this->carrier = $carrier;
    }

    /**
     * @return mixed
     */
    public function getBillingCenter()
    {
        return $this->billingCenter;
    }

    /**
     * @param mixed $billingCenter
     */
    public function setBillingCenter($billingCenter): void
    {
        $this->billingCenter = $billingCenter;
    }

    /**
     * @return DateTime
     */
    public function getDeliveryDate(): ?DateTime
    {
        return $this->deliveryDate;
    }

    /**
     * @param DateTime $deliveryDate
     */
    public function setDeliveryDate(DateTime $deliveryDate): void
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return mixed
     */
    public function getTimeSlots()
    {
        return $this->timeSlots;
    }

    /**
     * @param mixed $timeSlots
     */
    public function setTimeSlots($timeSlots): void
    {
        $this->timeSlots = $timeSlots;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getDateSendToVeolog()
    {
        return $this->dateSendToVeolog;
    }

    /**
     * @param mixed $dateSendToVeolog
     */
    public function setDateSendToVeolog($dateSendToVeolog): void
    {
        $this->dateSendToVeolog = $dateSendToVeolog;
    }

    public function addStockEntryItem(StockEntryItem $stockEntryItem)
    {
        $stockEntryItem->setStockEntry($this);

        $this->stockEntryItems->add($stockEntryItem);
    }

    public function removeStockEntryItem(StockEntryItem $stockEntryItem)
    {
        if ($this->stockEntryItems->removeElement($stockEntryItem)) {
            // set the owning side to null (unless already changed)
            if ($stockEntryItem->getStockEntry() === $this) {
                $stockEntryItem->setStockEntry(null);
            }
        }
    }

    public function getStockEntryItems()
    {
        return $this->stockEntryItems;
    }

    public function getRejections()
    {
        return $this->rejections;
    }

    public function setRejections($rejections): void
    {
        $this->rejections = $rejections;
    }

    public function addRejection(StockEntryRejection $rejection): self
    {
        if (!$this->rejections->contains($rejection)) {
            $rejection->setStockEntry($this);
            $this->rejections[] = $rejection;
        }

        return $this;
    }

    public function removeRejection(StockEntryRejection $rejection): self
    {
        if ($this->rejections->contains($rejection)) {
            $this->rejections->removeElement($rejection);
            // set the owning side to null (unless already changed)
            if ($rejection->getStockEntry() === $this) {
                $rejection->setStockEntry(null);
            }
        }

        return $this;
    }

    /**
     * Check if a stock entry has been rejected
     *
     * @return mixed
     */
    public function getStockEntryRejections()
    {
        $stockEntryRejections = [];

        foreach ($this->rejections as $stockEntryRejection) {
            if (is_null($stockEntryRejection->getStockEntryItem())) {
                $stockEntryRejections[] = $stockEntryRejection;
            }
        }

        return $stockEntryRejections;
    }

    public function getStockEntryItemByCode($code)
    {
        foreach ($this->stockEntryItems as $item) {
            if ($item->getProduct() && $item->getProduct()->translate('fr')->getCode() == $code) {
                return $item;
            }
        }
        return null;
    }

    public function getStockEntryItemById($id)
    {
        foreach ($this->stockEntryItems as $item) {
            if ($item->getId() == $id) {
                return $item;
            }
        }
        return null;
    }

    public function isValidated()
    {
        $validated = true;
        foreach ($this->stockEntryItems as $item) {
            if ($item->getStatus() != self::STATE_ITEM_VALIDATE) {
                $validated = false;
            }
        }
        return $validated;
    }

    public function awaitingValidation()
    {
      $awaitingValidation = true;
      foreach ($this->stockEntryItems as $item) {
        if ($item->getStatus() != self::STATE_ITEM_UPDATE) {
          $awaitingValidation = false;
        }
      }
      return $awaitingValidation;
    }
}
