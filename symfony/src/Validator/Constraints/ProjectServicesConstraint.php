<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ProjectServicesConstraint extends Constraint
{
    public $message = 'Le numéro de devis est obligatoire si la prestation nécessite un devis';
    public $messageFormat = "Le format est invalide, il doit être de type AAMM_XXX";

    public function validatedBy()
    {
        return 'project_services_constraint';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
