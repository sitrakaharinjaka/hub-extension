<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MyBoxesCommandRepository")
 * @ORM\Table(name="dk_myboxes_command")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class MyBoxesCommand
{
    use GedmoTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * firstname
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $firstname;

    /**
     * lastname
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lastname;

    /**
     * Office number
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $officeNumber;

    /**
     * Stage
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $stage;

    /**
     * @var Collection|MyBoxesItem[]
     * @ORM\OneToMany(targetEntity="App\Entity\MyBoxesItem", mappedBy="command", cascade={"all"})
     */
    protected $items;


    /**
     * @var MyBoxesBox|null
     * @ORM\ManyToOne(targetEntity="App\Entity\MyBoxesBox", inversedBy="commands", cascade={"persist"})
     * @ORM\JoinColumn(name="box_id", referencedColumnName="id")
     */
    protected $box;


    /**
     * @var User|null
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="commands")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * State
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $state = "";

    /**
     * Date of current state
     *
     * @var datetime $stateDate
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $stateDate;

    /**
     * Date of state : STATE_CMD_USER_PREPARED
     *
     * @var datetime $stateUserPreparedDate
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $stateUserPreparedDate;

    /**
     * Date of state : STATE_CMD_USER_ASKED
     *
     * @var datetime $stateUserAskedDate
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $stateUserAskedDate;

    /**
     * Date of state : STATE_CMD_VEOLOG_ACCEPTED
     *
     * @var datetime $stateVeologAcceptedDate
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $stateVeologAcceptedDate;

    /**
     * Date of state : STATE_CMD_VEOLOG_REFUSED
     *
     * @var datetime $stateVeologRefusedDate
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $stateVeologRefusedDate;

    /**
     * Date of state : STATE_CMD_VEOLOG_PREPARED
     *
     * @var datetime $stateVeologPreparedDate
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $stateVeologPreparedDate;

    /**
     * Date of state : STATE_CMD_VEOLOG_SENT
     *
     * @var datetime $stateVeologSentDate
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $stateVeologSentDate;

    /**
     * @var \DateTimeInterface|null $dateAddVlg
     * @ORM\Column(name="date_add_vlg", type="datetime", nullable=true)
     */
    public $dateAddVlg;

    /**
     * AdressType
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $adressType = "142 rue du bac, 75007 Paris";
    const AD_ARMEE = "65 avenue de la Grande-Armée, 75116 Paris";
    const AD_BAC = "142 rue du bac, 75007 Paris";

    /**
     * @ORM\Column(type="array")
     */
    protected $trackers = [];

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->trackers = [];
    }


    /**
     * {@inheritdoc}
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * {@inheritdoc}
     */
    public function clearItems()
    {
        $this->items->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function countItems()
    {
        return $this->items->count();
    }

    /**
     * {@inheritdoc}
     */
    public function addItem(MyBoxesItem $item)
    {
        if ($this->hasItem($item)) {
            return;
        }

        $this->items->add($item);
        $item->setCommand($this);
    }

    /**
     * {@inheritdoc}
     */
    public function removeItem(MyBoxesItem $item)
    {
        if ($this->hasItem($item)) {
            $this->items->removeElement($item);
            $item->setCommand(null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasItem(MyBoxesItem $item)
    {
        return $this->items->contains($item);
    }

    public function getTotalQuantity()
    {
        $total = 0;
        $items = $this->getItems();

        foreach ($items as $item) {
            $total += $item->getQuantity();
        }

        return $total;
    }

    public function isEmpty()
    {
        return $this->getTotalQuantity() <= 0;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getOfficeNumber()
    {
        return $this->officeNumber;
    }

    /**
     * @param mixed $officeNumber
     */
    public function setOfficeNumber($officeNumber)
    {
        $this->officeNumber = $officeNumber;
    }

    /**
     * @return mixed
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * @param mixed $stage
     */
    public function setStage($stage)
    {
        $this->stage = $stage;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
        $this->stateDate = new \DateTime();
        if ($state == MyBoxesBox::STATE_CMD_USER_PREPARED) {
            $this->stateUserPreparedDate = new \DateTime();
        }
        if ($state == MyBoxesBox::STATE_CMD_USER_ASKED) {
            $this->stateUserAskedDate = new \DateTime();
        }
        if ($state == MyBoxesBox::STATE_CMD_VEOLOG_ACCEPTED) {
            $this->stateVeologAcceptedDate = new \DateTime();
        }
        if ($state == MyBoxesBox::STATE_CMD_VEOLOG_REFUSED) {
            $this->stateVeologRefusedDate = new \DateTime();
        }
        if ($state == MyBoxesBox::STATE_CMD_VEOLOG_PREPARED) {
            $this->stateVeologPreparedDate = new \DateTime();
        }
        if ($state == MyBoxesBox::STATE_CMD_VEOLOG_SENT) {
            $this->stateVeologSentDate = new \DateTime();
        }
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateAddVlg()
    {
        return $this->dateAddVlg;
    }

    /**
     * @param \DateTimeInterface|null $dateAddVlg
     */
    public function setDateAddVlg($dateAddVlg)
    {
        $this->dateAddVlg = $dateAddVlg;
    }

    /**
     * @return MyBoxesBox|null
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * @param MyBoxesBox|null $box
     */
    public function setBox($box)
    {
        $this->box = $box;
    }

    /**
     * @return mixed
     */
    public function getTrackers()
    {
        return !$this->trackers ? [] : $this->trackers;
    }

    /**
     * @param mixed $trackers
     */
    public function setTrackers($trackers)
    {
        $this->trackers = $trackers;
    }

    /**
     * @return mixed
     */
    public function getAdressType()
    {
        return $this->adressType;
    }

    /**
     * @param mixed $adressType
     */
    public function setAdressType($adressType)
    {
        $this->adressType = $adressType;
    }

    /**
     * @param mixed $tracker
     */
    public function addTracker($tracker)
    {
        if (!$this->trackers) {
            $this->trackers = [];
        }
        if (false === array_search($tracker, $this->trackers)) {
            $this->trackers[] = $tracker;
        }
    }

    /**
     * @return datetime
     */
    public function getStateDate()
    {
        return $this->stateDate;
    }

    /**
     * @return datetime
     */
    public function getStateXDate($state)
    {
        if ($state == MyBoxesBox::STATE_CMD_USER_PREPARED) {
            return $this->stateUserPreparedDate;
        }
        if ($state == MyBoxesBox::STATE_CMD_USER_ASKED) {
            return $this->stateUserAskedDate;
        }
        if ($state == MyBoxesBox::STATE_CMD_VEOLOG_ACCEPTED) {
            return $this->stateVeologAcceptedDate;
        }
        if ($state == MyBoxesBox::STATE_CMD_VEOLOG_REFUSED) {
            return $this->stateVeologRefusedDate;
        }
        if ($state == MyBoxesBox::STATE_CMD_VEOLOG_PREPARED) {
            return $this->stateVeologPreparedDate;
        }
        if ($state == MyBoxesBox::STATE_CMD_VEOLOG_SENT) {
            return $this->stateVeologSentDate;
        }
        return null;
    }

    /**
     * @return datetime
     */
    public function getStateUserPreparedDate()
    {
        return $this->stateUserPreparedDate;
    }

    /**
     * @return datetime
     */
    public function getStateUserAskedDate()
    {
        return $this->stateUserAskedDate;
    }

    /**
     * @return datetime
     */
    public function getStateVeologAcceptedDate()
    {
        return $this->stateVeologAcceptedDate;
    }

    /**
     * @return datetime
     */
    public function getStateVeologRefusedDate()
    {
        return $this->stateVeologRefusedDate;
    }

    /**
     * @return datetime
     */
    public function getStateVeologPreparedDate()
    {
        return $this->stateVeologPreparedDate;
    }

    /**
     * @return datetime
     */
    public function getStateVeologSentDate()
    {
        return $this->stateVeologSentDate;
    }
}
