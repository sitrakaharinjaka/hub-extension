<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Front\MyBoxes;

use App\Controller\BaseController;
use App\Entity\MyBoxesBox;
use App\Entity\MyBoxesCommand;
use App\Entity\Project;
use App\Form\Type\MyBoxes\CommandType;
use App\Utils\Managers\MyBoxesBoxLogManager;
use App\Utils\Managers\MyBoxesBoxManager;
use App\Utils\Managers\MyBoxesCommandManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndexController
 *
 * @Route("/myboxes")
 *
 * @package App\Controller
 */
class IndexController extends BaseController
{
    /**
     * Homepage
     *
     * @Route("/", name="myboxes_homepage")
     * @return Response
     */
    public function index(Request $request, MyBoxesBoxLogManager $myBoxesBoxLogManager, MyBoxesBoxManager $boxManager)
    {
        return $this->redirect($this->generateUrl('myboxes_box_list'));
    }
}
