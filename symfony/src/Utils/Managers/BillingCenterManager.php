<?php

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;

use App\Utils\Managers\LocaleManager;
use App\Entity\BillingCenter;
use App\Entity\BillingCenterTranslation;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class BillingCenterManager
 * `
 * Object manager of billingcenter
 *
 * @package Disko\PageBundle\Managers
 */
class BillingCenterManager extends BaseManager
{

    /**
     * Repository
     *
     * @var BillingCenterRepository
     */
    protected $billingcenterRepository;

    /**
     * BillingCenterManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('billingcenterRepository', BillingCenter::class);
    }

    /**
     * Save a billingcenter
     *
     * @param BillingCenter $billingcenter
     */
    public function save(BillingCenter $billingcenter)
    {
        $billingcenter->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($billingcenter);
        $this->getEntityManager()->flush();

        return $billingcenter;
    }

    /**
     * Remove one billingcenter
     *
     * @param BillingCenter $billingcenter
     */
    public function remove(BillingCenter $billingcenter)
    {
        $this->getEntityManager()->remove($billingcenter);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one Active
     *
     * @param $slugWl
     * @param $locale
     * @return mixed
     */
    public function findOneActive($slugWl, $locale)
    {
        $billingcenter = $this->billingcenterRepository->findOneActive($slugWl, $locale);

        return $billingcenter;
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $billingcenter = $this->billingcenterRepository->findOneToEdit($id);

        return $billingcenter;
    }

    /**
     * Get all billingcenter
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->billingcenterRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->billingcenterRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->billingcenterRepository->findOneBy($filters);
    }
}
