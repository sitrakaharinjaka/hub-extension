PROD :
10 02 2020 - 13:50


DISKO Starters - Symfony 4.x
===


## New Project
Copy paste all files without the folder .git/ in your empty project just cloned

## Install with DOCKER
Install docker: https://docs.docker.com/compose/install/#prerequisites
```
$ make          # self documented makefile
$ make install  # install and start the project
```
If you have problem with db, please relaunch **make install** again

#### Update your hosts
```
$ sudo nano  /etc/hosts
127.0.0.1 [servername].local www.[servername].local
```

### Test the website
````
http[s]://[servername].local/
````



Starting hub-142-sf_mailhog_1 ... done
Starting hub-142-sf_redis_1   ... done
Starting hub-142-sf_db_1      ... done
Starting hub-142-sf_php_1     ... done
Starting hub-142-sf_nginx_1   ... done




docker-compose exec php bin/console cache:clear


docker-compose exec php bin/console doctrine:schema:update --dump-sql --force


docker stop $(docker ps -a -q)
docker-compose stop

docker-compose up -d



docker exec  -ti 2cfbf9eedf78 sh
    bin/console disko:project:return




docker-compose exec php bin/console disko:users:change-password guillaume.berard@disko.fr dev123


docker-compose exec db sh
mysql -u root -p disko
=> disko123



docker-compose exec php bin/console disko:project:return


docker-compose exec php bin/console veolog:order-confirm








docker-compose exec db bash
mysql -u root -p
disko123
SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));
DELETE FROM dk_stockLine where archived=1;

### Install wkhtmltopdf:

docker-compose exec wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.stretch_amd64.deb
docker-compose sudo dpkg -i wkhtmltox_0.12.5-1.stretch_amd64.deb
docker-compose sudo apt-get install xfonts-75dpi
docker-compose sudo dpkg -i wkhtmltox_0.12.5-1.stretch_amd64.deb
docker-compose sudo apt -f install