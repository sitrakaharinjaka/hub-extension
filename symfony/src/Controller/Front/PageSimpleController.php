<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Front;

use App\Controller\BaseController;
use App\Entity\ProductTranslation;
use App\Entity\Project;
use App\Entity\StockEntry;
use App\Entity\StockEntryItem;
use App\Entity\StockEntry115;
use App\Form\Model\StockEntry115Model;
use App\Form\Model\StockEntryModel;
use App\Form\Model\BulkStockEntryValidation;
use App\Form\Type\Stock\BulkStockEntryValidationType;
use App\Form\Type\Product\ProductType;
use App\Form\Type\Stock\StockEntry115Type;
use App\Form\Type\Stock\StockEntryType;
use App\Repository\StockRepository;
use App\Utils\Managers\StockEntry115Manager;
use App\Utils\Managers\StockEntryManager;
use App\Utils\Managers\StockLineManager;
use App\Utils\Managers\StockManager;
use App\Utils\Managers\UserManager;
use App\Utils\Services\SerializerService;
use App\Utils\Services\MailerService;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Form\FormError;

/**
 * Class PageSimpleController
 *
 * @package App\Controller
 */
class PageSimpleController extends BaseController
{
    protected $kernel;
    protected $pdf;
    public function __construct(KernelInterface $kernel, Pdf $pdf)
    {
        $this->kernel = $kernel;
        $this->pdf = $pdf;
    }

    /**
     * Index
     *
     * @Route("/mentions-legales", name="legal_mention")
     * @return Response
     */
    public function mentions(Request $request)
    {
        return $this->render('hub/simple/mentions.html.twig');
    }

    /**
     * Index
     *
     * @Route("/informations-prestations", name="services")
     * @return Response
     */
    public function services(Request $request)
    {
        return $this->render('hub/simple/informations_prestations.html.twig');
    }

    /**
     * Index
     *
     * @Route("/procedure-entree-stock", name="procedure_entree_stock")
     * @return Response
     */
    public function procedureEntreeStock(Request $request, StockEntryManager $stockEntryManager, StockEntry115Manager $stockEntry115Manager, StockManager $stockManager, MailerService $mailer)
    {
        $stock = $this->get('session')->get('current_stock');

        if(!is_object($stock)){
            $stocks = $stockManager->findByUser($this->getUser());

            if(count($stocks) < 1){
                throw $this->createNotFoundException('Vous devez sélectionner un stock');
            }

            $stock = reset($stocks);
        }

        if ($stock->getCave() == Project::CAVE_PANTIN) {
            $stockEntryModel = new StockEntryModel();
        } elseif ($stock->getCave() == Project::CAVE_115) {
            $stockEntryModel = new StockEntry115Model();
        }

        $user = $this->getUser();
        $stockEntryModel->setUsername($user->getFirstName() . ' ' . $user->getLastName());
        $stockEntryModel->setEmail($user->getEmail());

        if (!$stock) {
            throw $this->createNotFoundException('Vous devez sélectionner un stock');
        }
        $stock = $stockManager->findOneBy(['id' => $stock->getId()]);
        if (!$stock) {
            throw $this->createNotFoundException('Vous devez sélectionner un stock');
        }
        $stockEntryModel->setStock($stock);

        if ($stock->getCave() == Project::CAVE_PANTIN) {
            $form = $this->createForm(
                StockEntryType::class,
                $stockEntryModel,
                [
                    'stocks_query_builder' => function (StockRepository $er) {
                        $connect = $this->getUser();
                        $qb = $er->createQueryBuilder('u');
                        if (!$connect->hasRole('ROLE_GENERAL_ADMIN') and !$connect->hasRole('ROLE_SUPER_ADMIN')) {
                            $qb->leftJoin('u.adminUsers', 'us');
                            $qb->andWhere("us.id = :user");
                            $qb->setParameter('user', $connect->getId());
                        }
                        $qb->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale');
                        $qb->setParameter('locale', 'fr');
                        $qb->orderBy('t.name', 'ASC');
                        return $qb;
                    },
                ]
            );
        } elseif ($stock->getCave() == Project::CAVE_115) {
            $form = $this->createForm(
                StockEntry115Type::class,
                $stockEntryModel,
                [
                    'stocks_query_builder' => function (StockRepository $er) {
                        $connect = $this->getUser();
                        $qb = $er->createQueryBuilder('u');
                        if (!$connect->hasRole('ROLE_GENERAL_ADMIN') and !$connect->hasRole('ROLE_SUPER_ADMIN')) {
                            $qb->leftJoin('u.adminUsers', 'us');
                            $qb->andWhere("us.id = :user");
                            $qb->setParameter('user', $connect->getId());
                        }
                        $qb->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale');
                        $qb->setParameter('locale', 'fr');
                        $qb->orderBy('t.name', 'ASC');
                        return $qb;
                    },
                    'validation_groups' => ['Default']
                ]
            );

            $data   = $request->get('stock_entry115', []);

            if (isset($data['pickUpLocation']) && $data['pickUpLocation'] == '142') {
                $form = $this->createForm(
                    StockEntry115Type::class,
                    $stockEntryModel,
                    [
                        'stocks_query_builder' => function (StockRepository $er) {
                            $connect = $this->getUser();
                            $qb = $er->createQueryBuilder('u');
                            if (!$connect->hasRole('ROLE_GENERAL_ADMIN') and !$connect->hasRole('ROLE_SUPER_ADMIN')) {
                                $qb->leftJoin('u.adminUsers', 'us');
                                $qb->andWhere("us.id = :user");
                                $qb->setParameter('user', $connect->getId());
                            }
                            $qb->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale');
                            $qb->setParameter('locale', 'fr');
                            $qb->orderBy('t.name', 'ASC');
                            return $qb;
                        },
                        'validation_groups' => ['Default', 'pickup_142']
                    ]
                );
            }
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($stock->getCave() == Project::CAVE_PANTIN) {

                $hasCrdError = false;
                $hasDocuments = false;
                $documentPaths = [];

                foreach($stockEntryModel->getStockEntryItems() as $itemModel){
                    if ($itemModel->getInvoiceFile() instanceof UploadedFile && $itemModel->getProductCategory()->getId() == 5) {
                        $newFilename = $itemModel->getProductCode() . '-' . date('YmdHis') . '.' . $itemModel->getInvoiceFile()->guessExtension();
                        $itemModel->setInvoiceFilename($newFilename);

                        $uploadPath = $this->getParameter('kernel.root_dir') . '/../public/uploads/' . $newFilename;

                        move_uploaded_file($itemModel->getInvoiceFile()->getPathname(), $uploadPath);

                        $documentPaths[] = $uploadPath;

                        $hasDocuments = true;
                    }else{
                        if(!$itemModel->getCrd() && $itemModel->getProductCategory()->getId() == 5){
                            $hasCrdError = true;
                        }
                    }
                }

                if ($hasCrdError) {
                    $form->get('stockEntryItems')->getIterator()[0]->get('invoiceFile')->addError(new FormError('Les bouteilles sans CRD doivent être accompagnées d\'une facture ou document DSAC'));
                }else{
                    $stockEntry = $stockEntryManager->createFromModel($stockEntryModel, $this->getUser());
                    $stockEntryManager->save($stockEntry);

                    $mailer->sendStocketEntryCreate($stockEntry);

                    if($hasDocuments){
                        $mailer->sendStockEntryDocuments($stockEntry, $documentPaths);
                    }
                    return $this->render('hub/simple/verification_entree_stock_pantin.html.twig', array(
                        'stockEntry' => $stockEntry,
                    ));
                }
            } elseif ($stock->getCave() == Project::CAVE_115) {
                $stockEntry = $stockEntry115Manager->createFromModel($stockEntryModel, $this->getUser());
                $stockEntry->setCave('115');
                //$stockEntry->setstatus(StockEntry115::STATE_REQUEST_TAKEN);
                $stockEntry115Manager->save($stockEntry);

                $mailer->sendStocketEntryCreate($stockEntry);
                $mailer->sendStockEntry115PackingSlip($stockEntry, $this->generatePackingSlipPdf($stockEntry->getId()));
                return $this->render('hub/simple/verification_entree_stock_115.html.twig', array(
                    'stockEntry' => $stockEntry,
                ));
            }
        }

        if ($stock->getCave() == Project::CAVE_PANTIN) {
            return $this->render('hub/simple/procedure_entree_stock.html.twig', array(
                'form' => $form->createView(),
            ));
        } elseif ($stock->getCave() == Project::CAVE_115) {
            return $this->render('hub/simple/procedure_entree_stock_115.html.twig', array(
                'form' => $form->createView(),
            ));
        }
    }

    /**
     * Index
     *
     * @Route("/entree-stock/{id}/{cave}", defaults={"cave"=null}, name="entree_stock")
     * @return Response
     */
    public function validationEntreeStock($id, $cave, Request $request, StockEntryManager $stockEntryManager, StockEntry115Manager $stockEntry115Manager, UserManager $userManager)
    {
        $stock = $this->get('session')->get('current_stock');

        if(!is_null($cave)){
            if ($cave == Project::CAVE_PANTIN) {
                $stockEntry = $stockEntryManager->findOneToConfirm($id);
            } elseif ($cave == Project::CAVE_115) {
                $stockEntry = $stockEntry115Manager->findOneToConfirm($id);
            }
        }else{
            if ($stock->getCave() == Project::CAVE_PANTIN) {
                $stockEntry = $stockEntryManager->findOneToConfirm($id);
            } elseif ($stock->getCave() == Project::CAVE_115) {
                $stockEntry = $stockEntry115Manager->findOneToConfirm($id);
            }
        }

        if (!$stockEntry) {
            throw $this->createNotFoundException("Stock entry not found");
        }

        $user = $this->getUser();

        if (!$user->hasStock($stockEntry->getStock()) && !$this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createNotFoundException("Stock entry not found");
        }

        if ($stockEntry->getStock()->getCave() == Project::CAVE_PANTIN) {

          $this->get('session')->set('current_stock', $stockEntry->getStock());

          return $this->render('hub/simple/entree_stock_confirmation.html.twig', array(
            "stockEntry" => $stockEntry,
          ));
        } elseif ($stockEntry->getStock()->getCave() == Project::CAVE_115) {

          $this->get('session')->set('current_stock', $stockEntry->getStock());

          return $this->render('hub/simple/entree_stock115_confirmation.html.twig', array(
            "stockEntry" => $stockEntry,
          ));
        }
    }

    /**
     * Index
     *
     * @Route("/entree-stock/{id}/{status}/{cave}/{idItem}", defaults={"cave"=null, "idItem"=null}, name="entree_stock_confirm")
     * @return Response
     */
    public function confirmEntreeStock($id, $status, $cave, $idItem, Request $request, StockEntryManager $stockEntryManager, StockEntry115Manager $stockEntry115Manager, MailerService $mailer, StockLineManager $stockLineManager, StockManager $stockManager)
    {
        // $stock = $this->get('session')->get('current_stock');

        $stockEntry = null;

        if (!is_null($cave)) {
            if ($cave == Project::CAVE_PANTIN) {
                $stockEntry = $stockEntryManager->findOneToConfirm($id);
            } elseif ($cave == Project::CAVE_115) {
                $stockEntry = $stockEntry115Manager->findOneToConfirm($id);
            }
        }
        // }else{
        //     if ($stock->getCave() == Project::CAVE_PANTIN) {
        //         $stockEntry = $stockEntryManager->findOneToConfirm($id);
        //     } elseif ($stock->getCave() == Project::CAVE_115) {
        //         $stockEntry = $stockEntry115Manager->findOneToConfirm($id);
        //     }
        // }

        if (!is_object($stockEntry)) {
            throw $this->createNotFoundException("Stock entry not found");
        }

        $user = $this->getUser();
        if (!$user->hasStock($stockEntry->getStock()) && !$this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createNotFoundException("Stock entry not found");
        }

        $this->get('session')->set('current_stock', $stockEntry->getStock());
        $stock = $this->get('session')->get('current_stock');

        if ($stock->getCave() == Project::CAVE_PANTIN) {
            $item = null;

            if (!is_null($idItem)) {
                $item = $stockEntry->getStockEntryItemById($idItem);
            }

            if (!is_object($item)) {
                throw $this->createNotFoundException("Stock entry item not found");
            }
        } elseif ($stock->getCave() == Project::CAVE_115) {
            $isValidation = false;

            if (!is_null($idItem)) {
                $item = $stockEntry->getStockEntry115ItemById($idItem);

                if (!is_object($item)) {
                    throw $this->createNotFoundException("Stock entry item not found");
                }
            } else {
                $isValidation = true;
            }
        }

        if ($stock->getCave() == Project::CAVE_PANTIN) {
            if ($item->getStatus() == StockEntry::STATE_ITEM_UPDATE || $item->getStatus() == StockEntry::STATE_ITEM_REJECT) {
                if ($status === StockEntry::URL_STATE_ITEM_VALIDATE) {
                    if($item->getStatus() == StockEntry::STATE_ITEM_VALIDATE){
                        throw $this->createAccessDeniedException("Already validated");
                    }

                    $realStock = $stockManager->findOneBy(['id' => $stock->getId()]);
                    $item->setStatus(StockEntry::STATE_ITEM_VALIDATE);
                    $stockLineManager->addLine($realStock, $item->getProduct(), 'Entrée de stock #' . $stockEntry->getId(), $item->getQuantityReceived(), new \DateTime('now'), Project::CAVE_PANTIN, null, 'stockEntry');
                } elseif ($status === StockEntry::URL_STATE_ITEM_REJECT) {
                    if($item->getStatus() == StockEntry::STATE_ITEM_REJECT){
                        throw $this->createAccessDeniedException("Already rejected");
                    }

                    $item->setStatus(StockEntry::STATE_ITEM_REJECT);
                    $mailer->sendStocketEntryReject($stockEntry, $item, $user);
                }
            }else{
                throw $this->createAccessDeniedException("Unable to change status");
            }

        } elseif ($stock->getCave() == Project::CAVE_115) {
            if ($isValidation) {
                if ($status === StockEntry::URL_STATE_ITEM_VALIDATE) {
                    $realStock = $stockManager->findOneBy(['id' => $stock->getId()]);
                    $stockEntry->setStatus(StockEntry::STATE_ITEM_VALIDATE);

                    foreach ($stockEntry->getStockEntry115Items() as $stockItem) {
                        $stockLineManager->addLine($realStock, $stockItem->getProduct(), 'Entrée de stock de ' . $this->getUser()->getEmail(), $stockItem->getQuantityReceived(), new \DateTime('now'), Project::CAVE_115, null, 'stockEntry');
                    }
                } else {
                    throw $this->createAccessDeniedException("Unable to change status");
                }
            } else {
                if ($item->getStatus() !== 'ok' && $item->getStatus() !== 'empty' && $item->getStatus() !== 'partial') {
                    throw $this->createAccessDeniedException("Unable to change status");
                }

                if ($status === StockEntry::URL_STATE_ITEM_REJECT) {
                    $item->setChallenged(true);
                    $mailer->sendStocketEntryReject($stockEntry, $item, $user);
                } else {
                    throw $this->createAccessDeniedException("Unable to change status");
                }
            }
        }

        if ($stock->getCave() == Project::CAVE_PANTIN) {
            $stockEntryManager->save($stockEntry);
        } elseif ($stock->getCave() == Project::CAVE_115) {
            $stockEntry115Manager->save($stockEntry);
        }

        return $this->redirect($this->generateUrl('entree_stock', ['id' => $id]));
    }

    /**
     * Index
     *
     * @Route("/search-for-products-json", options = { "expose" = true }, name="procedure_entree_stock_ajax")
     * @return Response
     */
    public function searchForProducts(Request $request, SerializerService $serializerService)
    {
        $response = new Response();
        $response->setStatusCode(400);

        // Get Category Id
        $categoryId = $request->get('categoryId');
        $brandId    = $request->get('brandId');
        $name       = $request->get('name');
        $code       = $request->get('code');
        $ean        = $request->get('ean');

        $em         = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(ProductTranslation::class);
        $products   = $repository->getProductsByFilters($categoryId, $brandId, $name, $code, $ean, 20);
        if (null !== $products) {
            $response->setStatusCode(200);
            $coverPath = '';
            $tab       = array();

            foreach ($products as $product) {
                if (null !== $product->getCover()) {
                    $coverPath = $product->getCover()->getPathMedia();
                }

                $tab []= array(
                    'id'      => $product->getTranslatable()->getId(),
                    'name'    => $product->getName(),
                    'image'   => $coverPath,
                    'code'    => $product->getCode(),
                    'eanPart' => $product->getEanPart(),
                    'eanBox'  => $product->getEanBox(),
                    'state'   => $product->getState(),
                    'crd'     => $product->isCrd(),
                );
            }
        }

        return new JsonResponse($tab);
    }


    /**
     * Generate Stock Packing Slip PDF
     *
     * @Route("/stock-entry-packing-slip-pdf/{stockEntryId}", options = { "expose" = true }, name="stock_entry_packing_slip_pdf")
     *
     * @param $stockEntryId
     * @param Pdf $pdf
     * @return PdfResponse
     */
    public function generatePackingSlipPdf($stockEntryId)
    {
        $entityManager           = $this->getDoctrine()->getManager();
        $stockEntryRepository    = $entityManager->getRepository(StockEntry::class);
        $stockEntry115Repository = $entityManager->getRepository(StockEntry115::class);
        $stock                   = $this->get('session')->get('current_stock');

        if ($stock->getCave() == Project::CAVE_PANTIN) {
            $stockEntry = $stockEntryRepository->find($stockEntryId);
        } elseif ($stock->getCave() == Project::CAVE_115) {
            $stockEntry = $stockEntry115Repository->find($stockEntryId);
        }

        $user = $this->getUser();

        if (!$user->hasStock($stockEntry->getStock()) && !$this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createNotFoundException("Stock entry not found");
        }

        $logo = 'file:///' .  $this->kernel->getProjectDir() . '/public/assets/myboxes/images/logo@2x.png';

        if ($stock->getCave() == Project::CAVE_PANTIN) {
            $html = $this->renderView('hub/simple/bordereau_envoi_entree_stock_pdf.html.twig', array(
                'stockEntry' => $stockEntry,
                'logo' => $logo
            ));
        } elseif ($stock->getCave() == Project::CAVE_115) {
            $html = $this->renderView('hub/simple/bordereau_envoi_entree_stock_115_pdf.html.twig', array(
                'stockEntry' => $stockEntry,
                'logo' => $logo
            ));
        }

        return new PdfResponse(
            $this->pdf->getOutputFromHtml($html),
            'packing_slip.pdf',
            [
                'lowquality' => false,
                'images'     => true]
        );
    }

    /**
     * Show stock entries by the current user
     *
     * @Route("/entrees-stock-liste", name="stock_entries_listing")
     * @param Request $request
     * @param StockManager $stockManager
     *
     */
    public function showStockEntriesByUser(Request $request, StockManager $stockManager)
    {
        $stock = $this->get('session')->get('current_stock');
        if (!$stock) {
            throw $this->createNotFoundException('Vous devez sélectionner un stock');
        }
        $stock = $stockManager->findOneBy(['id' => $stock->getId()]);
        if (!$stock) {
            throw $this->createNotFoundException('Vous devez sélectionner un stock');
        }

        $entityManager        = $this->getDoctrine()->getManager();

        if ($stock->getCave() == Project::CAVE_PANTIN) {
            $stockEntryRepository = $entityManager->getRepository(StockEntry::class);
            $stockEntryItemRepository = $entityManager->getRepository(StockEntryItem::class);

            $bulkStockEntryValidation = new BulkStockEntryValidation();

            $form = $this->createForm(BulkStockEntryValidationType::class, $bulkStockEntryValidation);

            if ('POST' === $request->getMethod()) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    if($request->request->has('stockEntryItemsToValidate')){
                        $stockEntryItemsToValidate = $request->request->get('stockEntryItemsToValidate');

                        if(is_array($stockEntryItemsToValidate)){
                            $countValidatedStockEntryItems = 0;

                            foreach($stockEntryItemsToValidate as $stockEntryItemCombined){
                                $stockEntryItemCombined = explode(":", $stockEntryItemCombined);

                                if(is_array($stockEntryItemCombined) && count($stockEntryItemCombined) >= 2){
                                    $stockEntryObj = $stockEntryRepository->find((int) $stockEntryItemCombined[0]);
                                    $stockEntryItemObj = $stockEntryItemRepository->find((int) $stockEntryItemCombined[1]);

                                    if(is_object($stockEntryObj) && is_object($stockEntryItemObj)){
                                        $validationResponse = $this->forward('App\Controller\Front\PageSimpleController::confirmEntreeStock', [
                                            'id' => $stockEntryObj->getId(),
                                            'status'  => 'validation',
                                            'idItem' => $stockEntryItemObj->getId(),
                                            'cave' => 'pantin',
                                        ]);

                                        if($validationResponse instanceof Response && $validationResponse->getStatusCode() == Response::HTTP_OK){
                                            $countValidatedStockEntryItems++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $stockEntries = $stockEntryRepository->findNonArchivedStock($stock);

            return $this->render('hub/simple/entree_stock_listing.html.twig', [
                'stockEntries' => $stockEntries,
                'form' => $form->createView(),
            ]);
        } elseif ($stock->getCave() == Project::CAVE_115) {
            $stockEntryRepository = $entityManager->getRepository(StockEntry115::class);
            $stockEntries = $stockEntryRepository->findNonArchivedStock($stock);

            return $this->render('hub/simple/entree_stock_115_listing.html.twig', [
                'stockEntries' => $stockEntries,
            ]);
        }
    }
}
