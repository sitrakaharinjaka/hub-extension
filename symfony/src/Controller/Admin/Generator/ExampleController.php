<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin\Generator;

use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ExampleController
 *
 * @Route("/admin/generator/example")
 * @package App\Controller
 */
class ExampleController extends BaseController
{

    /**
     * Give form
     *
     * @Route("/form", name="admin_generator_example_form")
     * @return Response
     */
    public function form(Request $request)
    {
        return $this->render("@Admin/Generator/Example/partials/form.html.twig");
    }


    /**
     * Launch generation
     *
     * @Route("/launch", name="admin_generator_example_launch")
     * @return Response
     */
    public function launch(Request $request)
    {
        $gen = $request->get('gen', []);

        if (empty($gen)) {
            throw $this->createNotFoundException("Not datas found");
        }


        $entity = ucfirst($gen['entity']);
        $trad = ucfirst($gen['trad']);
        $desc = $gen['trad_desc'];
        $icon = (isset($gen['trad_icon'])) ? $gen['trad_icon'] : 'ion-ios-nutrition';


        $pathRootDir = $this->getParameter('kernel.root_dir');

        // Controller
        $pathdest = $pathRootDir.'/../src/Controller/Admin/'.$entity.'Controller.php';
        copy($pathRootDir.'/../generator/Example/Controller/Admin/ExampleController.php', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);
        self::replaceAttributesController($gen, $pathdest);
        self::replaceSection($gen, $pathdest);

        // Entity
        $pathdest = $pathRootDir.'/../src/Entity/'.$entity.'.php';
        copy($pathRootDir.'/../generator/Example/Entity/Example.php', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);
        self::replaceSection($gen, $pathdest);
        self::replaceExtra($gen, $pathdest);

        $pathdest = $pathRootDir.'/../src/Entity/'.$entity.'Translation.php';
        copy($pathRootDir.'/../generator/Example/Entity/ExampleTranslation.php', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);
        self::replaceSection($gen, $pathdest);
        self::replaceAttributesEntity($gen, $pathdest);
        self::replaceExtra($gen, $pathdest);


        // Repository
        $pathdest = $pathRootDir.'/../src/Repository/'.$entity.'Repository.php';
        copy($pathRootDir.'/../generator/Example/Repository/ExampleRepository.php', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);
        self::replaceSection($gen, $pathdest);
        self::replaceAttributesRepository($gen, $pathdest);
        self::replaceExtra($gen, $pathdest);

        $pathdest = $pathRootDir.'/../src/Repository/'.$entity.'TranslationRepository.php';
        copy($pathRootDir.'/../generator/Example/Repository/ExampleTranslationRepository.php', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);

        // Utils/Managers
        $pathdest = $pathRootDir.'/../src/Utils/Managers/'.$entity.'Manager.php';
        copy($pathRootDir.'/../generator/Example/Utils/Managers/ExampleManager.php', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);

        // Form
        $pathdest = $pathRootDir.'/../src/Form/Model/Search'.$entity.'.php';
        copy($pathRootDir.'/../generator/Example/Form/Model/SearchExample.php', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);
        self::replaceAttributesEntity($gen, $pathdest);

        if (!file_exists($pathRootDir.'/../src/Form/Type/'.$entity)) {
            mkdir($pathRootDir.'/../src/Form/Type/'.$entity);
        }

        $pathdest = $pathRootDir.'/../src/Form/Type/'.$entity.'/Search'.$entity.'Type.php';
        copy($pathRootDir.'/../generator/Example/Form/Type/Example/SearchExampleType.php', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);
        self::replaceAttributesEntity($gen, $pathdest);

        $pathdest = $pathRootDir.'/../src/Form/Type/'.$entity.'/'.$entity.'Type.php';
        copy($pathRootDir.'/../generator/Example/Form/Type/Example/ExampleType.php', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);

        $pathdest = $pathRootDir.'/../src/Form/Type/'.$entity.'/'.$entity.'TranslationType.php';
        copy($pathRootDir.'/../generator/Example/Form/Type/Example/ExampleTranslationType.php', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);
        self::replaceSection($gen, $pathdest);
        self::replaceAttributesEntity($gen, $pathdest);
        self::replaceExtra($gen, $pathdest);

        // validation.yaml
        $pathdest = $pathRootDir.'/../config/validator/validation.yaml';
        if (strpos(file_get_contents($pathdest), $entity." Entity") === false) {
            $content = file_get_contents($pathRootDir.'/../generator/Example/validation.yaml');
            $content = str_replace("Example", $entity, $content);
            $content = str_replace("example", strtolower($entity), $content);
            $content = str_replace("exemple", strtolower($trad), $content);
            $content = str_replace("Exemple", ucfirst($trad), $content);
            $content = self::replaceAttributesValidation($gen, $content);
            self::replaceInFileTab("# GENERATOR #", $pathdest, $content."\n\r# GENERATOR #\n\r# GENERATOR #");
        }
        // menu.html.twig
        $pathdest = $pathRootDir.'/../templates/admin/partials/menu.html.twig';
        $content = file_get_contents($pathRootDir.'/../generator/Example/templates/menu.html.twig');
        if (strpos(file_get_contents($pathdest), "nav-".strtolower($entity)."s") === false) {
            $content = str_replace("Example", $entity, $content);
            $content = str_replace("example", strtolower($entity), $content);
            $content = str_replace("exemple", strtolower($trad), $content);
            $content = str_replace("Exemple", ucfirst($trad), $content);
            $content = str_replace("ion-ios-nutrition", $icon, $content);
            $content = str_replace("Description", $desc, $content);
            self::replaceInFileTab("{# GENERATOR #}", $pathdest, $content."\n\r{# GENERATOR #}\n\r{# GENERATOR #}");
        }
        // templates
        if (!file_exists($pathRootDir.'/../templates/admin/'.$entity)) {
            mkdir($pathRootDir.'/../templates/admin/'.$entity);
            mkdir($pathRootDir.'/../templates/admin/'.$entity.'/partials');
        }

        $pathdest = $pathRootDir.'/../templates/admin/'.$entity.'/index.html.twig';
        copy($pathRootDir.'/../generator/Example/templates/Example/index.html.twig', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);
        self::replaceSection($gen, $pathdest);
        self::replaceAttributesTwig($gen, $pathdest);

        $pathdest = $pathRootDir.'/../templates/admin/'.$entity.'/edit.html.twig';
        copy($pathRootDir.'/../generator/Example/templates/Example/edit.html.twig', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);

        $pathdest = $pathRootDir.'/../templates/admin/'.$entity.'/create.html.twig';
        copy($pathRootDir.'/../generator/Example/templates/Example/create.html.twig', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);

        $pathdest = $pathRootDir.'/../templates/admin/'.$entity.'/partials/form.html.twig';
        copy($pathRootDir.'/../generator/Example/templates/Example/partials/form.html.twig', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);
        self::replaceSection($gen, $pathdest);
        self::replaceExtra($gen, $pathdest);
        self::replaceAttributesTwig($gen, $pathdest);

        $pathdest = $pathRootDir.'/../templates/admin/'.$entity.'/partials/searchList.html.twig';
        copy($pathRootDir.'/../generator/Example/templates/Example/partials/searchList.html.twig', $pathdest);
        self::replaceExample($entity, $trad, $pathdest);
        self::replaceSection($gen, $pathdest);
        self::replaceExtra($gen, $pathdest);
        self::replaceAttributesTwig($gen, $pathdest);

        // Update DB
        try {
            $process = new Process(
                'php bin/console doctrine:schema:update --force',
                $pathRootDir.'/../'
            );
            $process->run();
        } catch (\Exception $e) {
        }

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "Votre nouveau CRUD pour l'entité <b>".$entity."</b> a bien été générée ! Good Job Félix ! 🐱"
        );

        return $this->redirect($this->generateUrl("admin_homepage"));
    }


    /**
     *
     * Replace section in file
     *
     * @param $gen
     * @param $pathdest
     */
    public static function replaceExample($entity, $trad, $pathdest)
    {
        self::replaceInFile("Example", $pathdest, $entity);
        self::replaceInFile("example", $pathdest, strtolower($entity));
        self::replaceInFile("exemple", $pathdest, strtolower($trad));
        self::replaceInFile("Exemple", $pathdest, ucfirst($trad));
    }


    /**
     *
     * Replace section in file
     *
     * @param $gen
     * @param $pathdest
     */
    public static function replaceAttributesRepository($gen, $pathdest)
    {
        foreach ($gen['attributes'] as $key => $attribute) {
            if (isset($attribute['field']) && !empty($attribute['field']) && $attribute['field'] != $key) {
                self::replaceInFile(".".$key."'", $pathdest, ".".$attribute['field']."'");
            }
        }
    }

    /**
     *
     * Replace section in file
     *
     * @param $gen
     * @param $pathdest
     */
    public static function replaceAttributesEntity($gen, $pathdest)
    {
        foreach ($gen['attributes'] as $key => $attribute) {
            if (isset($attribute['field']) && !empty($attribute['field']) && $attribute['field'] != $key) {
                self::replaceInFile('$'.$key.')', $pathdest, '$'.$attribute['field'].')');
                self::replaceInFile('$'.$key.';', $pathdest, '$'.$attribute['field'].';');
                self::replaceInFile('$'.$key.' =', $pathdest, '$'.$attribute['field'].' =');
                self::replaceInFile("'".$key."' =>", $pathdest, "'".$attribute['field']."' =>");
                self::replaceInFile("'".$key."',", $pathdest, "'".$attribute['field']."',");
                self::replaceInFile('->'.$key.';', $pathdest, '->'.$attribute['field'].';');
                self::replaceInFile('->'.$key.' =', $pathdest, '->'.$attribute['field'].' =');
                self::replaceInFile('{"'.$key.'"}', $pathdest, '{"'.$attribute['field'].'"}');
                self::replaceInFile('get'.ucfirst($key).'(', $pathdest, 'get'.ucfirst($attribute['field']).'(');
                self::replaceInFile('set'.ucfirst($key).'(', $pathdest, 'set'.ucfirst($attribute['field']).'(');
                self::replaceInFile($key.'_', $pathdest, $attribute['field'].'_');
                self::replaceInFile("['".$key."']", $pathdest, "['".$attribute['field']."']");
                self::replaceInFile($attribute['field'].'space', $pathdest, 'namespace');
                self::replaceInFile('Table('.$attribute['field'], $pathdest, 'Table(name');
            }
        }


        foreach ($gen['attributes'] as $key => $attribute) {
            if (isset($attribute['field']) && !empty($attribute['field']) && $attribute['field'] != $key) {
                self::replaceInFile(' '.$key, $pathdest, ' '.$attribute['field']);
                self::replaceInFile(' $'.$key, $pathdest, ' $'.$attribute['field']);
            }
        }
    }



    /**
     *
     * Replace section in file
     *
     * @param $gen
     * @param $pathdest
     */
    public static function replaceAttributesController($gen, $pathdest)
    {
        foreach ($gen['attributes'] as $key => $attribute) {
            if (isset($attribute['field']) && !empty($attribute['field']) && $attribute['field'] != $key) {
                self::replaceInFile("filters['".$key."']", $pathdest, "filters['".$attribute['field']."']");
                self::replaceInFile('get'.ucfirst($key).'(', $pathdest, 'get'.ucfirst($attribute['field']).'(');
                self::replaceInFile('set'.ucfirst($key).'(', $pathdest, 'set'.ucfirst($attribute['field']).'(');
                self::replaceInFile("'".$attribute['baseLabel']."',", $pathdest, "'".$attribute['label']."',");
            }
        }
    }

    /**
     *
     * Replace section in file
     *
     * @param $gen
     * @param $pathdest
     */
    public static function replaceAttributesTwig($gen, $pathdest)
    {
        foreach ($gen['attributes'] as $key => $attribute) {
            if (isset($attribute['field']) && !empty($attribute['field']) && $attribute['field'] != $key) {
                self::replaceInFile('.'.$key.' ', $pathdest, '.'.$attribute['field'].' ');
                self::replaceInFile('.'.$key.'|', $pathdest, '.'.$attribute['field'].'|');
                self::replaceInFile('.'.$key.'\')', $pathdest, '.'.$attribute['field'].'\')');
                self::replaceInFile('form.'.$key.',', $pathdest, 'form.'.$attribute['field'].',');
                self::replaceInFile('form.'.$key.'.', $pathdest, 'form.'.$attribute['field'].'.');
                self::replaceInFile("'label': ".$attribute['baseLabel'], $pathdest, "'label': ".$attribute['label']);
                self::replaceInFile("'label': \"".$attribute['baseLabel'].'"', $pathdest, "'label': \"".$attribute['label'].'"');
                self::replaceInFile('"'.$attribute['baseLabel'].'",', $pathdest, '"'.$attribute['label'].'",');
                self::replaceInFile("'".$attribute['baseLabel']."',", $pathdest, "'".$attribute['label']."',");
            }
        }

        foreach ($gen['attributes'] as $key => $attribute) {
            if (isset($attribute['field']) && !empty($attribute['field']) && $attribute['field'] != $key) {
                self::replaceInFile('vars.'.$attribute['field'].' ', $pathdest, 'vars.name ');
            }
        }
    }


    /**
     *
     * Replace section in file
     *
     * @param $gen
     * @param $pathdest
     */
    public static function replaceAttributesValidation($gen, $content)
    {
        foreach ($gen['attributes'] as $key => $attribute) {
            if (isset($attribute['field']) && !empty($attribute['field']) && $attribute['field'] != $key) {
                $content = str_replace($key.':', $attribute['field'].':', $content);
            }
        }

        return $content;
    }
    
    /**
     *
     * Replace section in file
     *
     * @param $gen
     * @param $pathdest
     */
    public static function replaceSection($gen, $pathdest)
    {
        foreach ($gen['attributes'] as $key => $attribute) {
            if (!isset($gen['attributes'][$key]['case']) or $gen['attributes'][$key]['case'] != 1) {
                self::replaceInFileTab("/**".strtoupper($key)."**/", $pathdest, "");
                self::replaceInFileTab("{# ".strtoupper($key)." #}", $pathdest, "");
            } else {
                self::replaceInFile("/**".strtoupper($key)."**/", $pathdest, "");
                self::replaceInFile("{# ".strtoupper($key)." #}", $pathdest, "");
            }
        }
    }


    /**
     *
     * Replace section in file
     *
     * @param $gen
     * @param $pathdest
     */
    public static function replaceExtra($gen, $pathdest)
    {
        if (isset($gen['seo']) and $gen['seo'] == 1) {
            self::replaceInFile("/**SEO**/", $pathdest, "");
            self::replaceInFile("{# SEO #}", $pathdest, "");
        } else {
            self::replaceInFileTab("/**SEO**/", $pathdest, "");
            self::replaceInFileTab("{# SEO #}", $pathdest, "");
        }
    }


    /**
     * Replace in file
     *
     * @param $keyword
     * @param $pathFile
     */
    public static function replaceInFile($keyword, $pathFile, $replace = "")
    {
        if (!file_exists($pathFile)) {
            return false;
        }

        $datas = file_get_contents($pathFile);
        $datas = str_replace($keyword, $replace, $datas);
        file_put_contents($pathFile, $datas);
    }

    /**
     * Replace in file tab
     *
     * @param $keyword
     * @param $pathFile
     */
    public static function replaceInFileTab($keyword, $pathFile, $replace = "")
    {
        if (!file_exists($pathFile)) {
            return false;
        }

        $datas = file_get_contents($pathFile);
        $datas = explode($keyword, $datas);

        $cpt = count($datas);
        if ($cpt > 0) {
            for ($i = 0; $i < $cpt; $i++) {
                if ($i != 0 && $i % 2) {
                    unset($datas[$i]);
                }
            }
        }
        $datas = implode($replace, $datas);
        file_put_contents($pathFile, $datas);
    }
}
