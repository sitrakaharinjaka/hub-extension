<?php

namespace App\Repository;

use App\Entity\Project;
use App\Repository\BaseRepository;

/**
 * Class ProjectRepository
 */
class ProjectRepository extends BaseRepository
{
    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
      ->select('e')
      ->orderBy('e.dateStart', 'desc');

        if (isset($filters['dateAddVlg'])) {
            $filters['dateAddVlg'] = $filters['dateAddVlg']->format('Y-m-d');
        }

        if (isset($filters['dateStart'])) {
            $filters['dateStart'] = $filters['dateStart']->format('Y-m-d');
        }

        if (isset($filters['dateEnd'])) {
            $filters['dateEnd'] = $filters['dateEnd']->format('Y-m-d');
        }

        if (isset($filters['created'])) {
            $filters['created'] = $filters['created']->format('Y-m-d');
        }
        
        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'e';
                if ($key == 'id') {
                    $alias = 'e';
                }

                if ($key == 'stock' || ($key == 'state' && !is_array($filter))) {
                    $qb->andWhere($alias . '.' . $key . ' = :' . $key);
                    $qb->setParameter($key, $filter);
                } elseif (is_array($filter)) {
                    $qb->andWhere($qb->expr()->in($alias . '.' . $key, $filter));
                } elseif ($key == 'userFullName') {
                    $qb->join('e.user', 'u');
                    $qb->andWhere('CONCAT(u.lastName, \' \', u.firstName)' . ' LIKE :' . $key);
                    $qb->setParameter($key, '%' . $filter . '%');
                } else {
                    $qb->andWhere($alias . '.' . $key . ' LIKE :' . $key);
                    $qb->setParameter($key, '%' . $filter . '%');
                }
            }
        }

        $query = $qb->getQuery();

        return $query;
    }

    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
      ->select('e')
      ->where('e.id = :id')
      ->setParameter('id', $id);

        $query = $qb->getQuery();
        return $query->getOneOrNullResult();
    }

    /**
     * @param $user
     * @param $stock
     * @return null
     */
    public function findOneByUserStockLatest($user, $stock)
    {
        $qb = $this->createQueryBuilder('e')
      ->select('e')
      ->where('e.stock = :stock')
      ->andWhere('e.user = :user')
      ->setParameter('stock', $stock)
      ->setParameter('user', $user)
      ->orderBy('e.updated', 'DESC')
      ->setMaxResults(1);


        $qb->andWhere($qb->expr()->notIn('e.state', ':states'))
      ->setParameter('states', [Project::STATE_HUB_REFUSED, Project::STATE_HUB_ACCEPTED_FIXED]);

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }


    /**
     * @param $user
     * @param $stock
     */
    public function findProjectAvailableSwitch($user, $stock)
    {
        $qb = $this->createQueryBuilder('p')
      ->select('p')
      ->where('p.stock = :stock')
      ->andWhere('p.user = :user')
      ->setParameter('stock', $stock)
      ->setParameter('user', $user)
      ->orderBy('p.updated', 'DESC');

        $qb->andWhere($qb->expr()->in('p.state', ':states'))
      ->setParameter('states', [Project::STATE_HUB_DRAFT, Project::STATE_HUB_ACCEPTED]);

        return $qb;
    }

    /**
     * @param $user
     * @param $stock
     */
    public function findProjectAvailableByUser($user, $stock)
    {
        // Set archived date limit for stock. Currently archived if more than 12 months based on updated
        $date = new \DateTime();
        $date->modify('-10 years');

        $qb = $this->createQueryBuilder('p')
      ->select('p')
      ->where('p.stock = :stock')
      ->andWhere('p.parent IS NULL')
      ->setParameter('stock', $stock)
      ->andWhere('p.updated >= :last')
      ->setParameter('last', $date)
      ->orderBy('p.dateStart', 'DESC');

        $qb->andWhere($qb->expr()->notIn('p.state', ':states'));
        $qb->setParameter('states', [Project::STATE_HUB_CANCEL]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $user
     * @param $stock
     * @param $projects_id
     */
    public function findProjectStateByUser($user, $stock, $projectsId)
    {
      $qb = $this->createQueryBuilder('p');

        $qb->select('p.id, p.state, p.trackers')
        ->where('p.stock = :stock')
        ->andWhere($qb->expr()->in('p.id', ':projectIds'))
        ->setParameter('stock', $stock)
        ->setParameter('projectIds', $projectsId)
        ->orderBy('p.dateStart', 'DESC');

      return $qb->getQuery()->getResult();
    }

    /**
     * @param $id
     * @return null
     */
    public function findStateById($id)
    {
      $qb = $this->createQueryBuilder('e')
        ->select('e.id, e.state, e.trackers')
        ->where('e.id = :id')
        ->setParameter('id', $id);

      $query = $qb->getQuery();

      return $query->getOneOrNullResult();
    }

    /**
     * Get all project by state
     *
     * @return mixed
     */
    public function findProjectbyState($state, $cave)
    {
        $qb = $this->createQueryBuilder('p')
      ->select('p')
      ->where('p.state = :state')
      ->andWhere('p.cave = :cave')
      ->setParameter('state', $state)
      ->setParameter('cave', $cave)
      ->orderBy('p.updated', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * Get all project by state
     *
     * @return mixed
     */
    public function findProjectbyStateNoSendingToVlg($state, $cave)
    {
        $qb = $this->createQueryBuilder('p')
      ->select('p')
      ->where('p.state = :state')
      ->andWhere('p.dateAddVlg IS NULL')
      ->andWhere('p.cave = :cave')
      ->setParameter('state', $state)
      ->setParameter('cave', $cave)
      ->orderBy('p.updated', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * Get all projects sent to Veolog but not integrated yet
     *
     * @return mixed
     */
    public function findProjectsToWarnVeolog($since = null)
    {
        $now = new \DateTime();

        $qb = $this->createQueryBuilder('p')
      ->select('p')
      ->where('p.state = :state')
      ->andWhere('p.dateAddVlg IS NOT NULL')
      ->andWhere('p.cave = :cave')
      ->andWhere('p.type != :typeBulk')
      ->andWhere('p.updated < :intThreshold')
      ->setParameter('state', Project::STATE_HUB_ACCEPTED_FIXED)
      ->setParameter('cave', Project::CAVE_PANTIN)
      ->setParameter('typeBulk', Project::TYPE_BULK)
      ->setParameter('intThreshold', $now->modify('-45 minutes'))
      ->orderBy('p.updated', 'DESC');

        if($since instanceof \DateTime){
            $qb->andWhere('p.created >= :since')
            ->setParameter('since', $since);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Get all project not integrate by veolog
     *
     * @return mixed
     */
    public function findProjectNoIntegrateByVeolog($state)
    {
        $qb = $this->createQueryBuilder('p')
      ->select('p')
      ->where('p.state = :state')
      ->andWhere('p.dateA = :state')
      ->setParameter('state', Project::STATE_HUB_ACCEPTED_FIXED)
      ->orderBy('p.updated', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * Find all project with
     * - has return : true
     * - email return : not null
     * - date return : today + 1 day
     */
    public function findProjectsWithReturnToWarn()
    {
        $dateReturn = new \DateTime();
        $now = new \DateTime();
        // $dateReturn->modify('+1 day');
        // $dateReturn->settime(0, 0);

        $dow = $now->format('N');

        //Ignore if weekend
        if($dow > 5){
            return [];
        }

        if ($dow == 5) {
            $dateReturnStart = new \DateTime();
            $dateReturnStart->modify('next monday');
            $dateReturnStart->settime(0, 0);

            $dateReturn->modify('next monday');
            $dateReturn->settime(23, 59);
        } else {
            $dateReturnStart = new \DateTime();
            $dateReturnStart->modify('+1 day');
            $dateReturnStart->settime(0, 0);

            $dateReturn->modify('+1 day');
            $dateReturn->settime(23, 59);
        }

        $qb = $this->createQueryBuilder('p')
        ->select('p')
        ->andWhere('p.needReturn = 1')
        ->andWhere('p.returnMail = 0')
        ->andWhere('p.emailReturn IS NOT NULL')
        ->andWhere('p.dateReturn >= :dateReturnStart')
        ->andWhere('p.dateReturn <= :dateReturn')
        ->setParameter('dateReturnStart', $dateReturnStart)
        ->setParameter('dateReturn', $dateReturn);

        return $qb->getQuery()->getResult();
    }

    /**
     * Find Subprojects by the parent project Id
     *
     * @param $id
     * @return mixed
     */
    public function findSubProjectsByParentId($id)
    {
        $qb = $this->createQueryBuilder('p')
      ->select('p')
      ->where('p.parent = :id')
      ->setParameter('id', $id);

        $query = $qb->getQuery();
        return $query->getResult();
    }


    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsByHouse($start, $end)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('IFNULL(count(c.id), 0) as cpt, st.name as stock, CONCAT(YEAR(c.updated), \'/\' ,MONTH(c.updated), \'/\' , DAY(c.updated)) AS date, DAYOFYEAR(c.updated) as dayofyear');
        $qb->leftJoin('c.stock', 's');
        $qb->leftJoin('s.translations', 'st', 'WITH', 'st.locale = :locale');
        $qb->andWhere("c.updated IS NOT NULL");
        $qb->groupBy("dayofyear, stock");
        $qb->addOrderBy('stock', 'asc');
        $qb->addOrderBy('dayofyear', 'asc');

        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("c.updated >= :start");
        $qb->andWhere("c.updated <= :end");
        $qb->setParameter('locale', 'fr');
        $qb->andWhere("c.state = :state");
        $qb->setParameter('state', Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP);

        $results = $qb->getQuery()->getScalarResult();

        $datas = [];
        if (count($results) > 0) {
            foreach ($results as $result) {
                $stock = $result['stock'];
                if (!empty($stock)) {
                    unset($result['stock']);
                    $datas[$stock][] = $result;
                }
            }
        }

        return $datas;
    }

    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsByService($start, $end, $type = "after")
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('IFNULL(count(c.id), 0) as cpt,
        ast.name as service,
        CONCAT(YEAR(c.updated), \'/\' ,MONTH(c.updated), \'/\' , DAY(c.updated)) AS date,
        DAYOFYEAR(c.updated) as dayofyear');

        if ($type == "after") {
            $qb->leftJoin('c.afterServices', 's');
        } else {
            $qb->leftJoin('c.beforeServices', 's');
        }
        $qb->leftJoin('s.translations', 'ast', 'WITH', 'ast.locale = :locale');
        $qb->andWhere("c.updated IS NOT NULL");
        $qb->groupBy("dayofyear, service");
        $qb->addOrderBy('service', 'asc');
        $qb->addOrderBy('dayofyear', 'asc');

        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("c.updated >= :start");
        $qb->andWhere("c.updated <= :end");
        $qb->setParameter('locale', 'fr');
        $qb->andWhere("c.state = :state");
        $qb->setParameter('state', Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP);

        $results = $qb->getQuery()->getScalarResult();

        $datas = [];
        if (count($results) > 0) {
            foreach ($results as $result) {
                $service = $result['service'];
                if (!empty($service)) {
                    unset($result['service']);
                    $datas[$service][] = $result;
                }
            }
        }

        return $datas;
    }


    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsByCountry($start, $end)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('IFNULL(count(c.id), 0) as cpt, UPPER(c.recipientCountry) as country, CONCAT(YEAR(c.updated), \'/\' ,MONTH(c.updated), \'/\' , DAY(c.updated)) AS date, DAYOFYEAR(c.updated) as dayofyear');
        $qb->andWhere("c.updated IS NOT NULL");
        $qb->groupBy("dayofyear, country");
        $qb->addOrderBy('country', 'asc');
        $qb->addOrderBy('dayofyear', 'asc');

        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("c.updated >= :start");
        $qb->andWhere("c.updated <= :end");
        $qb->andWhere("c.state = :state");
        $qb->setParameter('state', Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP);

        $results = $qb->getQuery()->getScalarResult();

        $datas = [];
        if (count($results) > 0) {
            foreach ($results as $result) {
                $country = $result['country'];
                if (!empty($country)) {
                    unset($result['country']);
                    $datas[$country][] = $result;
                }
            }
        }

        return $datas;
    }


    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsByDelay($start, $end, $type = "urgency")
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('AVG(DATEDIFF(c.expeditionDate, c.waitDate)) as delay');
        $qb->addOrderBy('delay', 'desc');

        $qb->andWhere("c.expeditionDate IS NOT NULL");
        $qb->andWhere("c.waitDate IS NOT NULL");
        $qb->andWhere("c.type = :type");
        $qb->setParameter('type', $type);
        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("c.updated >= :start");
        $qb->andWhere("c.updated <= :end");
        $qb->andWhere("c.state = :state");
        $qb->setParameter('state', Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP);

        $results = $qb->getQuery()->getScalarResult();
        return (!empty($results) ? $results['0']['delay'] : 0);
    }

    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsByType($start, $end, $type = "urgency")
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('c');
        //$qb->addOrderBy('delay', 'desc');

        $qb->andWhere("c.expeditionDate IS NOT NULL");
        $qb->andWhere("c.waitDate IS NOT NULL");
        $qb->andWhere("c.type = :type");
        $qb->setParameter('type', $type);
        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("c.updated >= :start");
        $qb->andWhere("c.updated <= :end");
        $qb->andWhere("c.state = :state");
        $qb->setParameter('state', Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP);

        $results = $qb->getQuery()->getScalarResult();
        return count($results);
    }

    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function topByDelay($start, $end, $limit = 15, $name = '')
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('c.name as name, c.type as type, u.email as email, DATEDIFF(c.expeditionDate, c.waitDate) as delay');
        $qb->leftJoin('c.user', 'u');
        $qb->addOrderBy('delay', 'desc');

        $qb->andWhere("c.expeditionDate IS NOT NULL");
        $qb->andWhere("c.waitDate IS NOT NULL");
        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("c.updated >= :start");
        $qb->andWhere("c.updated <= :end");
        $qb->andWhere("c.state = :state");
        $qb->setParameter('state', Project::STATE_HUB_ACCEPTED_FIXED_VEOLOG_EXP);
        $qb->setMaxResults($limit);


        if (!empty($name)) {
            $qb->andWhere('c.name LIKE :name OR u.email LIKE :email OR u.type LIKE :type');
            $qb->setParameter('name', '%' . $name . '%');
            $qb->setParameter('email', '%' . $name . '%');
            $qb->setParameter('type', '%' . $name . '%');
        }

        $results = $qb->getQuery()->getScalarResult();

        return $results;
    }

    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsCreated($start, $end, $cave)
    {
        $qb = $this->createQueryBuilder('p');

        $qb->select('IFNULL(count(p.id), 0) as cpt, st.name as stock_name, CONCAT(YEAR(p.created), \'/\' ,MONTH(p.created), \'/\' , DAY(p.created)) AS date');
        $qb->leftJoin('p.stock', 's');
        $qb->leftJoin('s.translations', 'st', 'WITH', 'st.locale = :locale');

        $qb->where('p.cave = :cave');
        $qb->andWhere("p.created IS NOT NULL");
        $qb->andWhere("p.created >= :start");
        $qb->andWhere("p.created <= :end");

        $qb->setParameter('locale', 'fr');
        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->setParameter('cave', $cave);

        $qb->groupBy("stock_name, date");

        $results = $qb->getQuery()->getResult();

        return $results;
    }

    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsDeliveryTimeProject($start, $end)
    {
        $qb = $this->createQueryBuilder('p');

        $qb->andWhere("p.created IS NOT NULL");

        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("p.created >= :start");
        $qb->andWhere("p.created <= :end");
        $qb->andWhere("p.cave = :cave");
        $qb->setParameter('cave', Project::CAVE_115);

        $results = $qb->getQuery()->getResult();

        return $results;
    }

    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsCreatedProject($start, $end, $cave)
    {
        $qb = $this->createQueryBuilder('project');

        $qb->select('IFNULL(sum(item.quantity), 0) as cpt, stocktranslation.name as stock_name, CONCAT(YEAR(project.created), \'/\' ,MONTH(project.created), \'/\' , DAY(project.created)) AS date');

        $qb->leftJoin('project.items', 'item');
        $qb->leftJoin('item.product', 'product');
        $qb->leftJoin('project.stock', 'stock');
        $qb->leftJoin('stock.translations', 'stocktranslation', 'WITH', 'stocktranslation.locale = :locale');

        $qb->where('project.cave = :cave');
        $qb->andWhere("project.created IS NOT NULL");
        $qb->andWhere("project.created >= :start");
        $qb->andWhere("project.created <= :end");

        $qb->setParameter('locale', 'fr');
        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->setParameter('cave', $cave);

        $qb->groupBy("stock_name, date");

        $results = $qb->getQuery()->getResult();

        return $results;
    }
}
