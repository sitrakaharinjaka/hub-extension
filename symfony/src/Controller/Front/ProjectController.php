<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Front;

use App\Controller\BaseController;
use App\Entity\Project;
use App\Entity\User;
use App\Event\ProjectEvent;
use App\Form\Model\BulkOrder;
use App\Form\Model\ProjectSwitch;
use App\Form\Type\BulkOrder\BulkOrderType;
use App\Form\Type\Project\CartType;
use App\Form\Type\Project\CreateProjectType;
use App\Form\Type\Project\DeleteProjectType;
use App\Form\Type\Project\EditProjectType;
use App\Form\Type\Project\ProjectSwitchType;
use App\Utils\Managers\BulkOrderManager;
use App\Utils\Managers\MediaManager;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockManager;
use App\Utils\Managers\UserManager;
use App\Utils\Services\SerializerService;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet;

/**
 * Class ProjectController
 *
 * @Route("/projets")
 * @package App\Controller
 */
class ProjectController extends BaseController
{


    /**
     * Index
     *
     * @Route("/", name="project_index")
     * @return Response
     */
    public function index(Request $request)
    {
        return $this->redirect($this->generateUrl('account'));
    }

    /**
     * Page view
     *
     * @Route("/voir/{id}", name="project_view")
     * @return Response
     */
    public function view(Project $project): Response
    {
        $form = $this->createForm(CartType::class, $project, [
          'show_action' => true
        ]);

        return $this->render('hub/project/show.html.twig', [
            'form' => $form->createView(),
            'project' => $project,
            'show_action' => true,
        ]);
    }

    /**
     * Homepage
     * @Route("/creer", name="project_create")
     * @return Response
     */
    public function create(Request $request, ProjectManager $projectManager, StockManager $stockManager)
    {
        $project = new Project();
        $project->setUser($this->getUser());
        $project->setDateStart(new \DateTime());
        $project->setDateEnd(new \DateTime());

        $daylight_savings_offset_in_hours_for_paris = timezone_offset_get( timezone_open( "Europe/Paris" ), new \DateTime() )/3600;

        $type = $request->get('type', Project::TYPE_BASIC);
        $project->setType($type);


        $this->updateProjectType($request, $project);

        $stock = $this->get('session')->get('current_stock');
        $stock = $stockManager->findOneBy(['id' => $stock->getId()]);
        $project->setStock($stock);
        $project->setCave($project->getStock()->getCave());
        if ($project->getStock()->getCave() == Project::CAVE_115) {
            $date = $today = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
            if ($date->format('H') >= 19) {
                $dateStart = new \DateTime('tomorrow');
            } else {
                $dateStart = new \DateTime();
            }

            $dateStart->setTime(8, 0, 0);
            $today->setTime(8, 0, 0);

            $project->setDateStart($dateStart);
            $project->setDateReturn($today);
            $project->setDateEnd($today);
            $project->setHourEnd($today);
            $project->setRecipientZip("75016");
            $project->setRecipientCity("Paris");
            $project->setRecipientCountry("FR");
        }
        $form = $this->createForm(CreateProjectType::class, $project);

        // Update method
        if ('POST' === $request->getMethod()) {
            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                $type = $project->getType();
                if ($type == Project::TYPE_SHUTTLE) {
                $project->setCave(Project::CAVE_PANTIN);

                if($request->request->get('project')['adressType'] == Project::AD_ARMEE_CODE){
                    $project->setRecipientAddress("4 rue Pergolèse");
                    $project->setRecipientAddressComplement(null);
                    $project->setRecipientZip("75016");
                    $project->setRecipientCity("Paris");
                    $project->setRecipientCountry("France");
                }else{
                    $project->setRecipientAddress("115 Rue du bac");
                    $project->setRecipientAddressComplement(null);
                    $project->setRecipientZip("75007");
                    $project->setRecipientCity("Paris");
                    $project->setRecipientCountry("France");
                }

                }

                //Validate phone number
                if (!empty($project->getRecipientPhone())) {
                    $phoneFirstCharacter = substr($project->getRecipientPhone(), 0, 1);
                    $recipientPhone = (int) $phoneFirstCharacter != 0? 0 . $project->getRecipientPhone(): $project->getRecipientPhone();
                    $project->setRecipientPhone($recipientPhone);
                }

                if (!empty($project->getRecipientMobilePhone())) {
                    $mobileFirstCharacter = substr($project->getRecipientMobilePhone(), 0, 1);
                    $recipientMobile = (int) $mobileFirstCharacter != 0? 0 . $project->getRecipientMobilePhone(): $project->getRecipientMobilePhone();
                    $project->setRecipientMobilePhone($recipientMobile);
                }

                $projectManager->save($project);

                $this->get('session')->set('current_project', $project);

                return $this->redirect($this->generateUrl('catalog'));
            }
        }

        return $this->render('hub/project/create.html.twig', [
          'form' => $form->createView(),
          'daylight_savings_offset_in_hours_for_paris' => $daylight_savings_offset_in_hours_for_paris
        ]);
    }

    protected function updateProjectType($request, $project)
    {
        $type = $project->getType();
        if (in_array($type, [Project::TYPE_BASIC, Project::TYPE_SHUTTLE, Project::TYPE_URGENCY])) {
            if ($type == Project::TYPE_SHUTTLE) {
              $project->setCave(Project::CAVE_PANTIN);

              if($request->request->get('project')['adressType'] == Project::AD_ARMEE_CODE){

                $project->setRecipientAddress("4 rue Pergolèse");
                $project->setRecipientAddressComplement(null);
                $project->setRecipientZip("75016");
                $project->setRecipientCity("Paris");
                $project->setRecipientCountry("France");
              }else{
                $project->setRecipientAddress("142 Rue du bac");
                $project->setRecipientAddressComplement(null);
                $project->setRecipientZip("75007");
                $project->setRecipientCity("Paris");
                $project->setRecipientCountry("France");
              }

            }
        }

        return $project;
    }


    /**
     * @param Request $request
     * @param Project $project
     *
     * @return JsonResponse
     * @Route(path="/{id}/get-remove-form", name="project_remove_form")
     */
    public function removeForm(Request $request, ProjectManager $projectManager, $id)
    {
        $user = $this->getUser();
        /** @var Project $project */
        $project = $projectManager->findOneBy(['id' => $id]);

        if (!$project or $project->getUser()->getId() != $user->getId()) {
            throw $this->createNotFoundException("No project found");
        }

        if (!$project->isDeletable()) {
            throw $this->createNotFoundException("Bad state for remove it");
        }

        $form = $this->createForm(DeleteProjectType::class, $project);

        return new JsonResponse([
            'content' => $this->renderView('hub/project/partials/delete_form.html.twig', [
                'form' => $form->createView()
            ])
        ]);
    }

    /**
     * @param Request $request
     * @param ProjectManager $projectManager
     * @param $id
     * @return Response
     * @Route("/{id}/remove", name="project_remove")
     */
    public function remove(Request $request, ProjectManager $projectManager, $id)
    {
        /** @var User $user */
        $user = $this->getUser();
        $project = $projectManager->findOneBy(['id' => $id]);

        if (!$project or $project->getUser()->getId() != $user->getId()) {
            throw $this->createNotFoundException("No project found");
        }

        if (!$project->isDeletable()) {
            throw $this->createNotFoundException("Bad state for remove it");
        }

        $form = $this->createForm(DeleteProjectType::class, $project);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $projectManager->deleteInState($project, ProjectEvent::FROM_FRONT);
            if ($project->getType() === Project::TYPE_BULK && $project->getSubProjects() && $project->getSubProjects()->count()) {
                /** @var Project $subProject */
                foreach ($project->getSubProjects()->getValues() as $subProject) {
                    // if (($subProject->isDeletable() || $subProject->getState() == Project::STATE_HUB_CANCEL_ASK || $project->subProject() == Project::STATE_HUB_DRAFT_CANCEL_ASK)
                    //     && $subProject->getUser()->getId() === $user->getId()
                    //     && $subProject->getType() !== Project::TYPE_BULK
                    // ) {
                    //     $projectManager->deleteInState($subProject, ProjectEvent::FROM_FRONT);
                    // }
                    if ($subProject->isDeletable()
                        && $subProject->getUser()->getId() === $user->getId()
                        && $subProject->getType() !== Project::TYPE_BULK
                    ) {
                        $subProject->setReasonOfDelete($project->getReasonOfDelete());
                        $projectManager->deleteInState($subProject, ProjectEvent::FROM_FRONT);
                    }
                }
            }
        }

        return $this->redirectToRoute('project_view', [
            'id' => $project->getId()
        ]);
    }


    /**
     * @Route("/switch/{id}", name="project_switch")
     * @return Response
     */
    public function switchOne(Request $request, ProjectManager $projectManager, $id)
    {
        $user = $this->getUser();
        $project = $projectManager->findOneBy(['id' => $id]);

        if (!$project or $project->getUser()->getId() != $user->getId()) {
            throw $this->createNotFoundException("No project found");
        }

        $this->get('session')->set('current_project', $project);

        return $this->redirectToRoute("catalog");
    }


    /**
     * @Route("/switch-and-see/{id}", name="project_switch_and_see")
     * @return Response
     */
    public function switchOneAndSee(Request $request, ProjectManager $projectManager, $id)
    {
        $project = $projectManager->findOneBy(['id' => $id]);

        if (!$project) {
            throw $this->createNotFoundException("No project found");
        }

        $this->get('session')->set('current_project', $project);

        return $this->redirectToRoute("project_summary");
    }


    /**
     * @Route("/switch", name="project_switch_sticky_menu")
     * @return Response
     */
    public function switchMenu(Request $request, ProjectManager $projectManager, StockManager $stockManager)
    {
        $user = $this->getUser();
        $projectSwitch = new ProjectSwitch();
        $form = $this->createForm(ProjectSwitchType::class, $projectSwitch, [
            'user' => $user,
            'stock' => $this->get('session')->get('current_stock'),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $project = $projectSwitch->getProject();
            $this->get('session')->set('current_project', $project);

            return new RedirectResponse($request->headers->get('referer'));
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @return Response
     */
    public function stickyMenu(ProjectManager $projectManager)
    {
        $user = $this->getUser();

        $projectSwitch = new ProjectSwitch();
        $project = $this->get('session')->get('current_project', null);

        if ($project) {
            $project = $projectManager->findOneBy(['id' => $project->getId()]);
            if ($project) {
                $projectSwitch->setProject($project);
            }
        }

        $form = $this->createForm(ProjectSwitchType::class, $projectSwitch, [
            'user' => $user,
            'stock' => $this->get('session')->get('current_stock'),
        ]);

        return $this->render('hub/project/partials/sticky_menu.html.twig', [
            'project' => $this->get('session')->get('current_project', null),
            'form' => $form->createView()
        ]);
    }


    /**
     * @param Project $project
     * @param Request $request
     * @Route(path="/edit/{id}", name="project_update_information", methods={"GET", "POST", "PATCH"})
     * @return JsonResponse
     */
    public function edit(Request $request, $id, ProjectManager $projectManager, StockManager $stockManager)
    {
        $messageCart = $this->get('session')->get('flash_project');
        if (null !== $messageCart) {
            $this->get('session')->set('flash_project', null);
        }


        $user = $this->getUser();
        $project = $projectManager->findOneBy(['id' => $id]);

        if (!$project or $project->getUser()->getId() != $user->getId()) {
            throw $this->createNotFoundException("No project found");
        }

        $type = $project->getType();
        $stock = $this->get('session')->get('current_stock');
        $stock = $stockManager->findOneBy(['id' => $stock->getId()]);
        $form = $this->createForm(EditProjectType::class, $project, ["validation_groups" => "Edit"]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                $project->setType($type);

                //Validate phone number
                if (!empty($project->getRecipientPhone())) {
                    $phoneFirstCharacter = substr($project->getRecipientPhone(), 0, 1);
                    $recipientPhone = (int) $phoneFirstCharacter != 0? 0 . $project->getRecipientPhone(): $project->getRecipientPhone();
                    $project->setRecipientPhone($recipientPhone);
                }

                if (!empty($project->getRecipientMobilePhone())) {
                    $mobileFirstCharacter = substr($project->getRecipientMobilePhone(), 0, 1);
                    $recipientMobile = (int) $mobileFirstCharacter != 0? 0 . $project->getRecipientMobilePhone(): $project->getRecipientMobilePhone();
                    $project->setRecipientMobilePhone($recipientMobile);
                }

                $projectManager->save($project);

                $this->get('session')->set('flash_project', "Votre commande a bien été modifiée");

                return $this->redirect($this->generateUrl('project_update_information', ['id' => $id]));
            }
        }

        return $this->render('hub/project/edit.html.twig', [
            'form' => $form->createView(),
            'message_cart' => $messageCart,
        ]);
    }

    /**
     * Create Bulk Order
     * @Route("/commande-groupee", name="bulk_order_create")
     * @param Request $request
     * @param MediaManager $mediaManager
     * @param ProductManager $productManager
     * @param ProjectManager $projectManager
     * @param BulkOrderManager $bulkOrderManager
     * @return Response
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function createBulkOrder(Request $request, MediaManager $mediaManager, ProductManager $productManager, ProjectManager $projectManager, BulkOrderManager $bulkOrderManager)
    {
        $bulkOrder = new BulkOrder();
        $rows      = [];
        $toSave    = [];

        $form = $this->createForm(BulkOrderType::class, $bulkOrder);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $user = $this->getUser();

                $projectParent = $projectManager->createBulkParentProject($bulkOrder, $user);

                $file   = $request->files->get('bulkOrder')['excelFile'];
                $errors = [];

                if (null != $file) {
                    $readerXlsx  = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
                    $spreadsheet = $readerXlsx->load($file->getPathName());

                    $worksheet = $spreadsheet->getActiveSheet();

                    $toSave[] = $projectParent;

                    foreach ($worksheet->getRowIterator() as $row) {

                        // Ignore empty rows
                        if ($this->isRowEmpty($row)) {
                            continue;
                        }

                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(false);
                        $cells = [];

                        foreach ($cellIterator as $cell) {
                            $cells[] = $cell->getValue();
                        }

                        $rows[] = $cells;
                    }

                    // Remove columns name
                    $rows = array_slice($rows, 1);

                    $projectExists = false;

                    foreach ($rows as $key => $row) {
                        $projectChild = $projectManager->createBulkChildProject($projectParent, $row, $productManager, $toSave);

                        if (null == $projectChild) {
                            //Logiquement, bloqué en amont par le Validator.
                            $errors[] = "Impossible de créer la sous commande en ligne " . $key;
                        } else {
                            // Check if the project child with the same user, exists in '$toSave' array :
                            foreach ($toSave as $elt) {
                                if ($projectChild->getRecipientLastName() == $elt->getRecipientLastName()
                                    && $projectChild->getRecipientFirstname() == $elt->getRecipientFirstname()
                                    && $projectChild->getRecipientPhone() == $elt->getRecipientPhone()
                                    && $projectChild->getRecipientEmail() == $elt->getRecipientEmail()
                                    && $projectChild->getRecipientSociety() == $elt->getRecipientSociety()
                                    && $projectChild->getRecipientAddress() == $elt->getRecipientAddress()
                                    && $projectChild->getRecipientAddressComplement() == $elt->getRecipientAddressComplement()
                                    && $projectChild->getRecipientZip() == $elt->getRecipientZip()
                                    && $projectChild->getRecipientCity() == $elt->getRecipientCity()) {
                                    $projectExists = true;
                                }
                            }

                            if (false == $projectExists) {
                                $toSave[] = $projectChild;
                            }

                            $projectExists = false;
                        }
                    }

                    $errors = array_merge($errors, $productManager->checkProductQuantity($toSave));

                    if (sizeof($errors) == 0) {
                        $projectManager->saveAll($toSave);
                        $bulkOrder->addProject($projectParent);
                    } else {
                        return $this->render('hub/project/bulk_order.html.twig', [
                            'form'   => $form->createView(),
                            'errors' => $errors,
                        ]);
                    }
                }

                return $this->render('hub/project/bulk_order_confirmation.html.twig', [
                    'bulkOrder'    => $bulkOrder,
                    'numberOfRows' => count($rows),
                    'projects'     => $toSave,
                ]);
            }
        }

        return $this->render('hub/project/bulk_order.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * Validate Bulk Order
     * @Route("/commande-groupee-validation", name="bulk_order_validator")
     * @param Request $request
     * @param MediaManager $mediaManager
     * @param ProductManager $productManager
     * @param SerializerService $serializerService
     * @return Response
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function bulkOrderValidator(Request $request, MediaManager $mediaManager, ProductManager $productManager, SerializerService $serializerService)
    {
        $response = new Response();
        $response->setStatusCode(400);

        $errors            = 0;
        $errorsDelta       = $errors;
        $errorsDeltaOnName = $errors;
        $rowWithErrors     = array();
        $nonExistentCode   = array();
        $realName          = array();
        $alpha             = array('A','B','C','D','E','F','G','H','I','J','K', 'L','M','N','O','P','Q','R','S','T','U','V','W','X ','Y','Z');
        $cellsWithLengthError = array();
        $cellsWithSpaceError = array();

        $validCountries = [
            'France',
        ];

        // Get File to upload
        $file = $request->files->get('file', null);

        if (null != $file) {
            $response->setStatusCode(200);

            $readerXlsx  = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
            $spreadsheet = $readerXlsx->load($file->getPathName());

            $worksheet = $spreadsheet->getActiveSheet();

            $rows = [];

            foreach ($worksheet->getRowIterator() as $index => $row) {

                // Ignore empty rows
                if ($this->isRowEmpty($row)) {
                    continue;
                }

                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                $cells = [];

                foreach ($cellIterator as $cell) {
                    $cells[] = $cell->getValue();
                }

                $rows[] = $cells;
            }

            // Remove columns name
            $rows = array_slice($rows, 1);

            foreach ($rows as $index => $row) {
                //dump(count($row));
                if (count($row) >= 15) {
                    foreach ($row as $key => $value) {
                        if ($key == 12) {
                            $productExist = $productManager->checkExist($value);

                            if (null == $productExist) {
                                if ($value !== null) {
                                    $nonExistentCode []= $value;
                                }
                                $errorsDelta++;
                            };

                            $productCodeAndName = $productManager->checkCodeAndName($value, $row[11]);

                            if (null == $productCodeAndName) {
                                $errorsDeltaOnName++;
                                if (null !== $productExist) {
                                    $realName []= [ $value => $productExist->translate('fr')->getName() ];
                                }
                            };
                        }

                        // if ($key != 3 && $key != 4 && $key != 6 && $key != 14 && $value == null) {
                        //     $errorsDelta++;
                        // }

                        // Check required cells
                        $requiredCells = [0, 1, 2, 5, 7, 8, 9, 10, 13];
                        if (in_array($key, $requiredCells)) {
                            if(empty($value) || is_null($value)){
                                $errorsDelta++;
                            }
                        }

                        //Check cell value length
                        $lengthError = strlen($value) > 30? true: false;

                        if($lengthError && !in_array($key, [11, 12])){
                            $lengthErrorMsg = " (La cellule ne doit pas excéder 30 caractères)";
                            $cellsWithLengthError[] = [$index + 4 => $alpha[$key] . $lengthErrorMsg];
                            $errors++;
                        }

                        //Validate phone number
                        if($key == 2) {
                            if(!preg_match('/^[0-9 ]*$/', $value)){
                                $cellsWithSpaceError[] = [$index + 4 => $alpha[$key] . " (La cellule doit contenir des caractères numériques)"];
                                $errors++;
                            }
                        }

                        //Check country
                        if($key == 9 && !in_array($value, $validCountries)){
                            $errorsDelta++;
                        }

                      // Get line numbers and columns from file Excel with errors :
                        if ($errorsDelta > 0) {
                            $rowWithErrors []= [$index + 4 => $alpha[$key]];
                            $errors += $errorsDelta;
                        } elseif ($errorsDeltaOnName > 0) {
                            $rowWithErrors []= [$index + 4 => $alpha[$key - 1]];
                            $errors += $errorsDeltaOnName;
                        }

                        $errorsDelta       = 0;
                        $errorsDeltaOnName = 0;
                    }
                } else {
                    $errors = count($rows);
                    $rowWithErrors = 'fichier erroné';
                }
            }

            $rowsWithErrorsFinal = [];

            if(is_array($rowWithErrors)){
                foreach($rowWithErrors as $row){
                    $rowsWithErrorsFinal[] = $row;
                }
            }

            foreach($cellsWithLengthError as $row){
                $rowsWithErrorsFinal[] = $row;
            }

            foreach($cellsWithSpaceError as $row){
              $rowsWithErrorsFinal[] = $row;
            }

            $tab = array(
                'numberOfRows'      => count($rows),
                'errors'            => $errors,
                'rowsWithErrors'    => is_array($rowsWithErrorsFinal) && count($rowsWithErrorsFinal) > 0? $rowsWithErrorsFinal: $rowWithErrors,
                'nonExistentCode'   => $nonExistentCode,
                'realName'          => $realName,
            );

            $data = $serializerService->serializeObjectToJson($tab, 'json');

            $response->setContent($data);
        }

        return $response;
    }

    /**
     * Check if a row is empty in a Excel file
     *
     * @param $row
     * @return bool
     */
    public function isRowEmpty($row)
    {
        foreach ($row->getCellIterator() as $cell) {
            if ($cell->getValue()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Generate BulkOrder Packing Slip PDF
     *
     * @Route("/bulk-order-packing-slip-pdf/{bulkOrderId}", options = { "expose" = true }, name="bulk_order_packing_slip_pdf")
     *
     * @param $bulkOrderId
     * @param Request $request
     * @param Pdf $pdf
     * @param KernelInterface $kernel
     * @param ProjectManager $projectManager
     * @param UserManager $userManager
     * @return PdfResponse
     */
    public function generatePackingSlipPdf($bulkOrderId, Request $request, Pdf $pdf, KernelInterface $kernel, ProjectManager $projectManager, UserManager $userManager)
    {
        $entityManager     = $this->getDoctrine()->getManager();
        $projectRepository = $entityManager->getRepository(Project::class);
        $project           = $projectRepository->find($bulkOrderId);
        $subprojects       = $projectManager->findSubProjectsByParentId($bulkOrderId);
        $numberOfRows      = count($subprojects);
        //$user = $userManager->findOneByEmail($project->getCreatedBy());
        $user = $project->getUser();

        $logo = 'file:///' . $kernel->getProjectDir() . '/public/assets/myboxes/images/logo@2x.png';

        $html = $this->renderView('hub/project/bordereau_envoi_commande_groupee_pdf.html.twig', array(
            'project'      => $project,
            'subprojects'  => $subprojects,
            'numberOfRows' => $numberOfRows,
            'logo'         => $logo,
            'user'         => $user
        ));

        return new PdfResponse(
            $pdf->getOutputFromHtml($html),
            'packing_slip.pdf',
            [
                'lowquality' => false,
                'images'     => true]
        );
    }

    /**
     * Generate Single Order Packing Slip PDF
     *
     * @Route("/single-order-packing-slip-pdf/{orderId}", options = { "expose" = true }, name="single_order_packing_slip_pdf")
     *
     * @param $orderId
     * @param Request $request
     * @param Pdf $pdf
     * @param KernelInterface $kernel
     * @param ProjectManager $projectManager
     * @param UserManager $userManager
     * @return PdfResponse
     */
    public function generatePackingSlipPdfSingle($orderId, Request $request, Pdf $pdf, KernelInterface $kernel, ProjectManager $projectManager, UserManager $userManager)
    {
        $entityManager     = $this->getDoctrine()->getManager();
        $projectRepository = $entityManager->getRepository(Project::class);
        $project           = $projectRepository->find($orderId);
        //$user = $userManager->findOneByEmail($project->getCreatedBy());
        $user = $project->getUser();

        $logo = 'file:///' . $kernel->getProjectDir() . '/public/assets/myboxes/images/logo@2x.png';

        $html = $this->renderView('hub/project/bordereau_envoi_commande_pdf.html.twig', array(
            'project'      => $project,
            'logo'         => $logo,
            'user'         => $user
        ));

        return new PdfResponse(
            $pdf->getOutputFromHtml($html),
            'packing_slip.pdf',
            [
                'lowquality' => false,
                'images'     => true]
        );
    }

    /**
     * Show authorized countries
     *
     * @Route("/authorized-countries", options = { "expose" = true }, name="authorized_countries")
     */
    public function showAuthorizedCountries()
    {
        return $this->render('hub/project/authorized_countries.html.twig');
    }
}
