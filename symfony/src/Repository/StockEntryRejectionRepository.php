<?php

namespace App\Repository;

use App\Entity\StockEntryRejection;
use App\Entity\StockEntry;
use App\Repository\BaseRepository;

/**
 * @method StockEntryRejection|null find($id, $lockMode = null, $lockVersion = null)
 * @method StockEntryRejection|null findOneBy(array $criteria, array $orderBy = null)
 * @method StockEntryRejection[]    findAll()
 * @method StockEntryRejection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockEntryRejectionRepository extends BaseRepository
{

    public function removeByStockEntry(StockEntry $stockEntry)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete(StockEntryRejection::class, 'r');
        $qb->andWhere('r.stockEntry = :stockEntry');
        $qb->setParameter('stockEntry', $stockEntry);

        return $qb->getQuery()->execute();
    }
}
