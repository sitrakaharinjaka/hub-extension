<?php

/**
 * Repository
 *
 * @author Pierre Baumes <pierre.b@disko.fr>
 */

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class CategoryTranslationRepository
 *
 * @package App\Repository
 */
class CategoryTranslationRepository extends BaseRepository
{
}
