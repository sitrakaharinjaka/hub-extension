<?php

namespace App\Command\Veolog;

use App\Command\Veolog\AbstractVeologCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\Product;
use App\Entity\Project;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\ServiceManager;
use App\Utils\Managers\ProjectItemManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Input\InputOption;

class SynchOrderAskCommand extends AbstractVeologCommand
{

  /** @var  EntityManager */
    private $productManager;
    private $projectManager;
    private $serviceManager;
    private $projectItemManager;
    private $rootDir;

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, ProductManager $productManager, ProjectManager $projectManager, ProjectItemManager $projectItemManager, ServiceManager $serviceManager, KernelInterface $kernel)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->productManager = $productManager;
        $this->projectManager = $projectManager;
        $this->projectItemManager = $projectItemManager;
        $this->serviceManager = $serviceManager;

        $this->rootDir = $kernel->getProjectDir();
    }

    protected static $defaultName = 'veolog:order-ask';

    protected function configure()
    {
        $this
      ->setDescription('Synch veolog order ask')
          ->addOption(
            'vlgenv',
            null,
            InputOption::VALUE_OPTIONAL,
            'Env veolog prod?',
            false
          )
      ->setHelp('This command allows you to synch veolog order ask evry 5minutes');
    }

    protected function executeCommand()
    {
        $dirVlg = $this->getDirVlgIN();

        $this->log('info', 'Find projects to synch into veolog');

        /*Hack to reset state project for dev !!!!*/
        //for ($i = 1; $i <= 20; $i++) {
        //    /** @var Project $project */
        //    $project = $this->projectManager->findOneToEdit($i);
        //    if (!empty($project)) {
        //        $project->setState(Project::STATE_HUB_ACCEPTED_FIXED);
        //        $this->projectManager->save($project);
        //    }
        //}
        $projects = $this->projectManager->findProjectbyStateNoSendingToVlg(Project::STATE_HUB_ACCEPTED_FIXED, Project::CAVE_PANTIN);
        $total_projects = count($projects);

        if ($total_projects > 0) {
            $this->log('info', $total_projects . ' projects ready for synch');
            $currentDate = date("ymdhis");
            $fileName = 'orders_' . $currentDate . '.csv';
            $localfilename = $this->rootDir . "/veolog/" . $dirVlg . "/" . $fileName;
            $serverfilename = "/" . $dirVlg . "/" . $fileName;

            $this->log('info', 'Creating file : ' . $localfilename);
            $fp = fopen($localfilename, "w");
            $array_project = [];

            foreach ($projects as $key => $project) {
                if (
                    $project->getType() != Project::TYPE_BULK or ($project->getType() == Project::TYPE_BULK && $project->getParent() != null)
                ) {
                    $this->log('info', $project->getName() . ' - ' . $project->getId());
                    $service = [];
                    $beforeService = $project->getBeforeServices()->getValues();
                    $afterService = $project->getAfterServices()->getValues();

                    foreach ($beforeService as $before) {
                        $before = $this->serviceManager->findOneToEdit($before->getId());
                        $before = $before->translate('fr')->getCode();
                        $service[] = $before;
                    }

                    foreach ($afterService as $after) {
                        $after = $this->serviceManager->findOneToEdit($after->getId());
                        $after = str_replace("P", "R", $after->translate('fr')->getCode());
                        $service[] = $after;
                    }

                    $listService = implode(",", array_merge($service)) . ',';
                    $array_project[$key]['type_ligne'] = 'E';
                    $array_project[$key]['entite_de_stocks'] = $project->getStock()->translate('fr')->getCode(); //ask mateo
                    $array_project[$key]['ref_commande_donneur_ordre'] = strtoupper('0' . $project->getId());
                    $array_project[$key]['ref_commande_mh_livre'] = strtoupper(trim(substr($project->getName(), 0, 13)));
                    $array_project[$key]['destinataire_livraison'] = strtoupper(substr($project->getRecipientFirstName() . ' ' . $project->getRecipientLastName(), 0, 27));

                    $RecipientAddress = str_replace("\n", "", $project->getRecipientAddress());
                    $RecipientAddress = str_replace("\r", "", $RecipientAddress);
                    $RecipientAddress = str_replace("\t", "", $RecipientAddress);

                    $array_project[$key]['addr_livraison_1'] = strtoupper($RecipientAddress);

                    $RecipientAddressComplement = str_replace("\n", "", $project->getRecipientAddressComplement());
                    $RecipientAddressComplement = str_replace("\r", "", $RecipientAddressComplement);
                    $RecipientAddressComplement = str_replace("\t", "", $RecipientAddressComplement);
                    $array_project[$key]['addr_livraison_2'] = strtoupper($RecipientAddressComplement);
                    $array_project[$key]['addr_livraison_3'] = '';
                    $array_project[$key]['cp_livraison'] = strtoupper($project->getRecipientZip());
                    $array_project[$key]['ville_livraison'] = strtoupper($project->getRecipientCity());
                    $array_project[$key]['pays_livraison'] = strtoupper($project->getRecipientCountry());

                    $phone = $project->getRecipientPhone();
                    if (empty($phone)) {
                        $phone = $project->getRecipientMobilePhone();
                    }
                    $phone = strtoupper(trim(substr($phone, 0, 18)));
                    $phone = str_replace(" ", "", $phone);
                    $phone = str_replace(".", "", $phone);
                    $array_project[$key]['tel_livraison'] = $phone;

                    $array_project[$key]['email_livraison'] = strtoupper($project->getRecipientEmail());
                    $array_project[$key]['date_livraison'] = $project->getDateStart()->format('Ymd');
                    $array_project[$key]['heure_livraison'] = $project->getHourStart()->format('Hi');
                    switch ($project->getType()) {
                        case Project::TYPE_BASIC:
                            $array_project[$key]['transporteur'] = 2;
                            break;
                        case Project::TYPE_URGENCY:
                            $array_project[$key]['transporteur'] = 1;
                            break;
                        case Project::TYPE_SHUTTLE:
                            $array_project[$key]['transporteur'] = 3;
                            break;
                    }
                    $RecipientInformation = str_replace("\n", "", $project->getRecipientInformation());
                    $RecipientInformation = str_replace("\r", "", $RecipientInformation);
                    $RecipientInformation = str_replace("\t", "", $RecipientInformation);
                    $array_project[$key]['commentaire_long'] = strtoupper($RecipientInformation);

                    $array_project[$key]['presta_services'] = $listService . strtoupper($project->getBeforeQuote());
                    $array_project[$key]['retour'] = strtoupper($project->getNeedReturn());

                    if ($project->getType() == Project::TYPE_URGENCY) {
                        $array_project[$key]['urgence'] = '1';
                    } else {
                        $array_project[$key]['urgence'] = '0';
                    }

                    $array_project[$key]['centre_facturation'] = strtoupper($project->getBillingCenter()->getName());
                    $array_project[$key]['date_retour'] = $project->getDateEnd()->format('Ymd');
                    $array_project[$key]['heure_retour'] = $project->getHourEnd()->format('Hi');
                    $project_to_csv = str_replace('"', '', $array_project[$key]);
                    fputcsv($fp, $project_to_csv, ';');
                    $items = $project->getItems();

                    $array_items = [];
                    $count_num_product = 0;
                    $qty_product = 0;
                    $count_num_product += count($items);
                    foreach ($items as $keyItem => $item) {
                        $filter['id'] = $item->getProduct()->getId();
                        $product = $this->productManager->findOneBy($filter);
                        if (!empty($product)) {
                            $array_items[$keyItem]['type_ligne'] = 'L';
                            $array_items[$keyItem]['num_ligne'] = strtoupper($keyItem + 1);
                            $array_items[$keyItem]['code_article'] = strtoupper($product->translate('fr')->getCode());
                            $array_items[$keyItem]['label_article'] = strtoupper($product->translate('fr')->getName());
                            $array_items[$keyItem]['qte'] = $item->getQuantity();
                            $qty_product += $item->getQuantity();

                            // if ($product->translate('fr')->getState() == 'out_of_use') {
                            //     $array_items[$keyItem]['code_etat'] = 'MVS';
                            // } else {
                            //     $array_items[$keyItem]['code_etat'] = '';
                            // }
                            $array_items[$keyItem]['code_etat'] = '';

                            fputcsv($fp, $array_items[$keyItem], ';');
                        }
                    }

                    $this->log('info', 'Updating and add project ' . $project->getName() . ' - ' . $project->getId());
                    fputcsv($fp, array('F', $count_num_product, $qty_product), ';');
                }
            }


            $this->log('info', 'Creating file : ' . $localfilename . ' success !!!!');

            $this->log('info', 'Try to upload file : ' . $localfilename . ' to veolog ftp');

            try {
                $ftp_handle = ftp_connect('ftp.veolog.fr');
                if (false === $ftp_handle) {
                    throw new \Exception('Unable to connect');
                }

                $loggedIn = ftp_login($ftp_handle, 'vlgcli18', '1L8vJv2U75');
                ftp_pasv($ftp_handle, true);
                if (true === $loggedIn) {
                    $this->log('info', 'Connexion success');

                    if (ftp_put($ftp_handle, $serverfilename, $localfilename, FTP_ASCII)) {
                        $this->log('info', "Successfully uploaded $fileName.");

                        $this->log('info', 'Updating projects date ajout veolog');
                        //add date of adding project to veolog
                        foreach ($projects as $key => $project) {
                            $project = $this->projectManager->findOneToEdit($project->getId());
                            $project->setDateAddVlg(new \DateTime());
                            $this->projectManager->save($project);
                        }
                    } else {
                        $this->log('info', "Error uploading $fileName.");
                    }
                } else {
                    throw new \Exception('Unable to log in');
                }

                ftp_close($ftp_handle);
            } catch (Exception $e) {
                echo "Failure: " . $e->getMessage();
            }
        } else {
            $this->log('info', 'No project find to synch');
        }
    }
}
