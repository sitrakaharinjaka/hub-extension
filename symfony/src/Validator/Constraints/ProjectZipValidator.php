<?php

namespace App\Validator\Constraints;

use App\Entity\Project;
use App\Entity\ProjectItem;
use App\Utils\Managers\DeliveryManager;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockLineManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProjectZipValidator extends ConstraintValidator
{
    /**
     * @var ProjectManager
     */
    private $projectManager;

    /**
     * StockClassValidator constructor.
     */
    public function __construct(ProjectManager $projectManager, DeliveryManager $deliveryManager)
    {
        $this->deliveryManager = $deliveryManager;
        $this->projectManager = $projectManager;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {

        /** @var Project $project */
        $project = $value;


        $zip = $project->getRecipientZip();
        $dept = mb_substr(trim($zip), 0, 2);

        $today = new \DateTime();
        if ($project->getType() != Project::TYPE_URGENCY) {
            $today->setTime(0, 0, 0);
        }
        $dateStart = $project->getDateStart();
        $dateStart->setTime(0, 0, 0);
        $diff = $today->diff($dateStart);
        $days =  $diff->format('%d');

        $hour = ($days*24) + $diff->format('%h');

        if ($project->getCave() == Project::CAVE_PANTIN) {
            if ($project->getType() == Project::TYPE_URGENCY) {
                if (strtolower($project->getRecipientCountry()) == strtolower('FR')) {
                    $delivery = $this->deliveryManager->findOneBy(['zip' => $dept, 'type' => Project::TYPE_URGENCY]);

                    if ($delivery) {
                        /** Not need by cutoff
                        if ($hour < $delivery->getHour()) {
                            $this->context->buildViolation("Pour le code postal ".$project->getRecipientZip().", un delais de ".$delivery->getHour()."h minimum doit être respecté pour la livraison.")->atPath('recipientZip')->addViolation();
                        }**/
                    } else {
                        $this->context->buildViolation("Département non autorisé pour les commandes urgentes (Départements couverts : 75, 92, 93, 94 )")->atPath('recipientZip')->addViolation();
                    }
                } else {
                    $this->context->buildViolation("Uniquement la France est autorisé pour les commandes urgentes")->atPath('recipientCountry')->addViolation();
                }
            } elseif ($project->getType() == Project::TYPE_BASIC) {
                if (strtolower($project->getRecipientCountry()) == strtolower('FR')) {
                    $delivery = $this->deliveryManager->findOneBy(['zip' => $dept, 'type' => Project::TYPE_BASIC]);
                    if ($delivery) {
                        if ($hour < $delivery->getHour()) {
                            $this->context->buildViolation("Pour le code postal ".$project->getRecipientZip().", un delais de ".($delivery->getHour()/24)." jour(s) minimum doit être respecté pour la livraison.")->atPath('recipientZip')->addViolation();
                        }
                    } else {
                        $this->context->buildViolation("Département non autorisé")->atPath('recipientZip')->addViolation();
                    }
                } else {
                    $delivery = $this->deliveryManager->findOneBy(['country' => strtoupper($project->getRecipientCountry()), 'type' => Project::TYPE_BASIC]);
                    if ($delivery) {
                        if ($hour < $delivery->getHour()) {
                            $this->context->buildViolation("Pour le pays selectionné, un délai de ".($delivery->getHour()/24)." jour(s) minimum doit être respecté pour la livraison.")->atPath('recipientCountry')->addViolation();
                        }
                    } else {
                        $this->context->buildViolation("Pays non autorisé")->atPath('recipientCountry')->addViolation();
                    }
                }
            }
        } elseif ($project->getCave() == Project::CAVE_115) {
            // Pas de contrainte sur la date de livraison du CAVE_115 puisque la date est fixé à la date du jour et bloqué en dur.
        }
    }


    public function getCsv($filename, $delimiter = ',')
    {
        $data = $this->convert($filename, $delimiter);
        return $data;
    }

    public function convert($filename, $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }
        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }
        return $data;
    }
}
