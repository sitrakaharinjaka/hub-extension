<?php

/**
 * Model for search
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Model;

/**
 * Class SearchProduct
 *
 * @package App\Form\Model
 */
class SearchProduct
{
    /**
     * Id
     *
     * @var string
     */
    protected $id;

    /**
     * Name
     *
     * @var string
     */
    protected $name;

    /**
     * Code
     *
     * @var string
     */
    protected $code;

    /**
     * Description
     *
     * @var string
     */
    protected $description;

    /**
     * Marque
     *
     * @var string
     */
    protected $brand;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * Get search data
     *
     * @return array
     */
    public function getSearchData()
    {
        $tab = array();

        if (!empty($this->id)) {
            $tab['id'] = $this->id;
        }
        if (!empty($this->name)) {
            $tab['name'] = $this->name;
        }
        if (!empty($this->code)) {
            $tab['code'] = $this->code;
        }
        if (!empty($this->description)) {
            $tab['description'] = $this->description;
        }
        if (!empty($this->brand)) {
            $tab['brand'] = $this->brand;
        }

        return $tab;
    }

    /**
     * Get description
     *
     * @return  string
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param  string  $description  Description
     *
     * @return  self
     */ 
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get marque
     *
     * @return  string
     */ 
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set marque
     *
     * @param  string  $brand  Marque
     *
     * @return  self
     */ 
    public function setBrand(string $brand)
    {
        $this->brand = $brand;

        return $this;
    }
}
