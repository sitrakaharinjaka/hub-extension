<?php

namespace App\Utils\Managers;

use App\Repository\StockRepository;
use App\Utils\Managers\BaseManager;

use App\Utils\Managers\LocaleManager;
use App\Entity\Stock;
use App\Entity\StockTranslation;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class StockManager
 * `
 * Object manager of stock
 *
 * @package Disko\PageBundle\Managers
 */
class StockManager extends BaseManager
{

    /**
     * Repository
     *
     * @var StockRepository
     */
    protected $stockRepository;

    /**
     * StockManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('stockRepository', Stock::class);
    }

    /**
     * Save a stock
     *
     * @param Stock $stock
     */
    public function save(Stock $stock)
    {
        $stock->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($stock);
        $this->getEntityManager()->flush();

        return $stock;
    }

    /**
     * Remove one stock
     *
     * @param Stock $stock
     */
    public function remove(Stock $stock)
    {
        $this->getEntityManager()->remove($stock);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one Active
     *
     * @param $slugWl
     * @param $locale
     * @return mixed
     */
    public function findOneActive($slugWl, $locale)
    {
        $stock = $this->stockRepository->findOneActive($slugWl, $locale);

        return $stock;
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $stock = $this->stockRepository->findOneToEdit($id);

        return $stock;
    }

    /**
     * Get all stock
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->stockRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->stockRepository->findAll();
    }

    /**
     * Find all stock codes
     *
     * @return mixed
     */
    public function findAllCodes($cave)
    {   
        $codes = [];
        $stocks = $this->queryForSearch()->getResult();

        foreach($stocks as $stock){
            if ($stock->getCave() == $cave) {
                $codes[] = $stock->translate('fr')->getCode();
            }
        }

        return $codes;
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->stockRepository->findOneBy($filters);
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneStockBy($filters = array())
    {
        return $this->stockRepository->findOneStockBy($filters);
    }

    /**
     * Find stock for user
     *
     * @param $user
     * @return mixed
     */
    public function findByUser($user)
    {
        return $this->stockRepository->findByUser($user);
    }

    /**
     * Find stock for user
     *
     * @param $user
     * @return mixed
     */
    public function findByAdminUser($user)
    {
        return $this->stockRepository->findByAdminUser($user);
    }

    public function findAdminsStock(Stock $stock)
    {
        return $this->stockRepository->findAdminsStock($stock);
    }

    public function statsCreated($start, $end)
    {
        return $this->stockRepository->statsCreated($start, $end);
    }

    public function statsServicesByStock($start, $end)
    {
        return $this->stockRepository->statsServicesByStock($start, $end);
    }
}
