<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MyBoxesItemConstraint extends Constraint
{
    public $message = 'Merci de choisir au minimum 1 caisse';

    public function validatedBy()
    {
        return 'myboxes_item_constraint';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
