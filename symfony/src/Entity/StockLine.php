<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockLineRepository")
 * @ORM\Table(name="dk_stockLine")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class StockLine
{
    use GedmoTrait;
    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Label
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $label;

    /**
     * Cave
     *
     * @ORM\Column(length=255, name="cave", nullable=true)
     */
    protected $cave;

    /**
     * From
     *
     * @ORM\Column(length=255, name="origin", nullable=true)
     */
    protected $from;

    /**
     * Number in opération
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $number;


    /**
     * @var \DateTimeInterface|null $dateStart
     * @ORM\Column(name="date", type="datetime")
     */
    public $date;

    /**
     * @var Project|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="stockLines")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    protected $project;

    /**
     * @var Stock|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", inversedBy="stockLines")
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
     */
    protected $stock;

    /**
     * @var Product|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="stockLines")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $holdForProject = false;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $archived = false;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $stockVariation = false;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $stockVariationValid = false;

    /**
     * @var \DateTimeInterface|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $stockVariationValidDate;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $stockVariationChallenged = false;

    /**
     * @var \DateTimeInterface|null $stockVariationChallengedDate
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $stockVariationChallengedDate;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTimeInterface|null $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }
    
    /**
     * @return Project|null
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project|null $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return Stock|null
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param Project|null $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return Product|null
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product|null $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getCave()
    {
        return $this->cave;
    }

    /**
     * @param mixed $cave
     */
    public function setCave($cave)
    {
        $this->cave = $cave;
    }

    /**
     * @return mixed
     */
    public function getHoldForProject()
    {
        return $this->holdForProject;
    }

    /**
     * @param mixed $holdForProject
     */
    public function setHoldForProject($holdForProject)
    {
        $this->holdForProject = $holdForProject;
    }

    /**
     * @return mixed
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * @param mixed $archived
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;
    }

    /**
     * Get the value of stockVariation
     */ 
    public function getStockVariation()
    {
        return $this->stockVariation;
    }

    /**
     * Set the value of stockVariation
     *
     * @return  self
     */ 
    public function setStockVariation($stockVariation)
    {
        $this->stockVariation = $stockVariation;

        return $this;
    }

    /**
     * Get the value of stockVariationValid
     */ 
    public function getStockVariationValid()
    {
        return $this->stockVariationValid;
    }

    /**
     * Set the value of stockVariationValid
     *
     * @return  self
     */ 
    public function setStockVariationValid($stockVariationValid)
    {
        $this->stockVariationValid = $stockVariationValid;

        return $this;
    }

    /**
     * Get the value of stockVariationChallenged
     */ 
    public function getStockVariationChallenged()
    {
        return $this->stockVariationChallenged;
    }

    /**
     * Set the value of stockVariationChallenged
     *
     * @return  self
     */ 
    public function setStockVariationChallenged($stockVariationChallenged)
    {
        $this->stockVariationChallenged = $stockVariationChallenged;

        return $this;
    }

    /**
     * Get $stockVariationChallengedDate
     *
     * @return  \DateTimeInterface|null
     */ 
    public function getStockVariationChallengedDate()
    {
        return $this->stockVariationChallengedDate;
    }

    /**
     * Set $stockVariationChallengedDate
     *
     * @param  \DateTimeInterface|null
     *
     * @return  self
     */ 
    public function setStockVariationChallengedDate($stockVariationChallengedDate)
    {
        $this->stockVariationChallengedDate = $stockVariationChallengedDate;

        return $this;
    }

    /**
     * Get the value of stockVariationValidDate
     *
     * @return  \DateTimeInterface|null
     */ 
    public function getStockVariationValidDate()
    {
        return $this->stockVariationValidDate;
    }

    /**
     * Set the value of stockVariationValidDate
     *
     * @param  \DateTimeInterface|null  $stockVariationValidDate
     *
     * @return  self
     */ 
    public function setStockVariationValidDate($stockVariationValidDate)
    {
        $this->stockVariationValidDate = $stockVariationValidDate;

        return $this;
    }
}
