<?php

/**
 * Form type
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Type\Stock;

use A2lix\TranslationFormBundle\Form\Type\TranslationsFormsType;
use App\Entity\BillingCenter;
use App\Entity\Stock;
use App\Entity\Project;
use App\Entity\User;
use App\Utils\Managers\LocaleManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\Fo;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Repository\UserRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use App\Utils\Managers\UserManager;
/**
 *  Form Type
 */
class StockType extends AbstractType
{
    protected $localeManager;

    protected $userManager;

    /**
     * @param LocaleManager $localeManager
     */
    public function __construct(LocaleManager $localeManager, UserManager $userManager)
    {
        $this->localeManager = $localeManager;
        $this->userManager = $userManager;
    }


    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $defaultLocales = $this->localeManager->findDefaultSlug();

        $builder->add('billingCenters', EntityType::class, [
          'class' => BillingCenter::class,
          'query_builder' => function (EntityRepository $erc) {
              return $erc->createQueryBuilder('u')
              ->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale')
              ->setParameter('locale', 'fr')
              ->orderBy('t.name', 'ASC');
          },
          'choice_label' => 'name',
          'by_reference' => false,
          'multiple' => true,
          'required'  => false
        ]);

        $builder->add('referentUser', EntityType::class, [
            'class' => User::class,
            'placeholder' => 'Sélectionner un utilisateur',
            'multiple' => false,
            'required'  => false,
            'choice_label' => 'fullName',
            'choices' => []
        ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();
            if (!is_null($data->getReferentUser())) {
                $options = $form->get('referentUser')->getConfig()->getOptions();
                $options['choices'] = [$data->getReferentUser()];
                $form->add('referentUser', EntityType::class, $options);
            }
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            $referentUser = $this->userManager->findOneById($data['referentUser']);
            $form = $event->getForm();
            $data['referentUser'] = $referentUser;
            $options = $form->get('referentUser')->getConfig()->getOptions();
            $options['choices'] = [$referentUser];
            $form->add('referentUser', EntityType::class, $options);
            $event->getForm()->get('referentUser')->setData($referentUser);
            $formData = $form->getData();
            $formData->setReferentUser($referentUser);
            $form->setData($formData);
        });

        // $builder->add('referentUser', EntityType::class, [
        //     'class' => User::class,
        //     'query_builder' => function (EntityRepository $erc) {
        //         return $erc->createQueryBuilder('u')
        //                    ->andWhere('u.roles NOT LIKE :role')
        //                    ->select('u')
        //                    ->setParameter('role', '%ROLE_SUPER_ADMIN%')
        //                    ->orderBy('u.lastName', 'ASC');
        //     },
        //     'placeholder' => 'Sélectionner un utilisateur',
        //     'choice_label' => 'fullName',
        //     'multiple' => false,
        //     'required'  => false
        // ]);

        $builder ->add('cave', ChoiceType::class, [
            'label' => "Votre cave/stock",
            'choices' => [
                'Cave Pantin'   => Project::CAVE_PANTIN,
                'Cave 115'      => Project::CAVE_115,
            ],
            'required'  => true
        ]);

        $builder->add('translations', TranslationsFormsType::class, [
            'label' => false,
            'default_locale' => $defaultLocales,
            'required_locales' => $defaultLocales,
            'locales' => $this->localeManager->findAllSlug(),
            'form_type' => StockTranslationType::class
        ]);
    }

    /**
     * Configure Options
     *
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
            'data_class' => Stock::class
        ));
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'stock';
    }
}
