<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\IP;
use App\Form\Model\SearchIP;
use App\Form\Type\IP\IPType;
use App\Form\Type\IP\SearchIPType;
use App\Utils\Managers\IPManager;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class IPController
 * @Route("/admin/ip")
 * @package App\Controller
 */
class IPController extends BaseController
{
    /**
     * IP's list
     * @Route("/list", name="admin_ip_list")
     * @param Request $request Request
     */
    public function index(Request $request, IPManager $ipManager)
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchIPType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des exemples');

        // Generate query
        $query = $ipManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20
        );

        // Render view
        return $this->render('@Admin/IP/index.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchIP();
        $data->setName((isset($filters['name']))   ? $filters['name'] : '');
        $data->setV4((isset($filters['v4']))   ? $filters['v4'] : '');
        $data->setV6((isset($filters['v6']))   ? $filters['v6'] : '');

        return $data;
    }

    /**
     * Create a example
     * @Route("/create", name="admin_ip_create")
     * @param Request $request Request
     *
     * @return Response
     */
    public function create(Request $request, IPManager $ipManager)
    {
        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");

        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des exemples", $this->get("router")->generate("admin_ip_list"));
        $breadcrumbs->addItem("Création");


        // Init form
        $ip = new IP();

        $form = $this->createForm(IPType::class, $ip);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {

                // Save
                $ipManager->save($ip);

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'notice',
                    "Création terminée"
                );

                return $this->redirect($this->generateUrl('admin_ip_edit', array(
                    'id' => $ip->getId()
                )));
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/IP/create.html.twig',
            array(
                'form'      => $form->createView(),
                'example'   => $ip,
            )
        );
    }

    /**
     * Update a example
     * @Route("/edit/{id}", name="admin_ip_edit")
     * @param Request $request Request
     * @param integer $id      Id of example
     *
     * @return Response
     */
    public function edit(Request $request, IPManager $ipManager, $id)
    {
        // Find it
        $ip = $ipManager->findOneBy(['id' => $id]);
        if (!$ip) {
            throw $this->createNotFoundException('IP not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des exemples", $this->get("router")->generate("admin_ip_list"));
        $breadcrumbs->addItem("Modification");

        // Init form
        $form = $this->createForm(IPType::class, $ip);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {

                // Save
                $ipManager->save($ip);

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'notice',
                    "Mise à jour effectuée"
                );

                return $this->redirect($request->headers->get('referer'));
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/IP/edit.html.twig',
            array(
                'form'           => $form->createView(),
                'example'           => $ip,
            )
        );
    }

    /**
     * Delete one example
     * @Route("/delete/{id}", name="admin_ip_delete")
     * @param Request $request Request
     * @param integer $id      Id of example
     */
    public function delete(Request $request, IPManager $ipManager, $id)
    {
        // Find it
        /** @var IP $ip */
        $ip = $ipManager->findOneBy(['id' => $id]);
        if (!$ip) {
            throw $this->createNotFoundException('IP not found');
        }

        $ipManager->remove($ip);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            'Le exemple "'.$ip->getName().'" a bien été supprimé'
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Export csv
     * @Route("/export", name="admin_ip_export")
     * @param Request $request
     */
    public function export(Request $request, IPManager $ipManager)
    {
        $data = $this->initSearch($request);
        $query = $ipManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Nom',
            'Date de création',
            'Activé'
        ], ';');

        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->getName(),
                $result->getCreated()->format('d/m/Y'),
                $result->getActive()
            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_examples_export.csv"'
        ));
    }


    /**
     * Off all
     * @Route("/off/all", name="admin_ip_off")
     * @param Request $request Request
     */
    public function off(Request $request, IPManager $ipManager)
    {
        $ipManager->offAll();

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            'Les restrictions sont toutes désactivées'
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * On all
     * @Route("/on/all", name="admin_ip_on")
     * @param Request $request Request
     */
    public function on(Request $request, IPManager $ipManager)
    {
        $ipManager->onAll();

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            'Les restrictions sont toutes désactivées'
        );

        return $this->redirect($request->headers->get('referer'));
    }
}
