<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\CategoryTranslation;
use App\Form\Model\SearchCategory;
use App\Form\Type\Category\CategoryTranslationType;
use App\Form\Type\Category\SearchCategoryType;
use App\Form\Type\Category\CategoryType;
use App\Utils\Managers\CategoryManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoryController
 * @Route("/admin/categorys")
 * @package Disko\AdminBundle\Controller
 */
class CategoryController extends BaseController
{
    /**
     * Category's list
     * @Route("/list/{askLocale}", name="admin_category_list", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listAction(Request $request, CategoryManager $categoryManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchCategoryType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des catégories');

        // Generate query
        $query = $categoryManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/Category/index.html.twig', array(
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchCategory();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setName((isset($filters['name']))   ? $filters['name'] : '');

        return $data;
    }

    /**
     * Create a category
     * @Route("/create", name="admin_category_create")
     * @param Request $request Request
     *
     * @return Response
     */
    public function createAction(Request $request, CategoryManager $categoryManager, LocaleManager $localeManager)
    {
        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");

        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des catégories", $this->get("router")->generate("admin_category_list"));
        $breadcrumbs->addItem("Création");


        // Init form
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category, [
            'validation_groups' => ['Default']
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($category->getTranslations()) > 0) {
                    $categoryManager->save($category);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Création terminée"
                    );

                    return $this->redirect($this->generateUrl('admin_category_edit', array(
                        'id' => $category->getId()
                    )));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            } else {

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'error',
                    "Le formulaire contient des erreurs ou est vide"
                );
            }
        }

        // View
        return $this->render(
            '@Admin/Category/create.html.twig',
            array(
                'form'           => $form->createView(),
                'category'           => $category,
            )
        );
    }

    /**
     * Update a category
     * @Route("/edit/{id}", name="admin_category_edit")
     * @param Request $request Request
     * @param integer $id      Id of category
     *
     * @return Response
     */
    public function editAction(Request $request, CategoryManager $categoryManager, LocaleManager $localeManager, $id)
    {
        // Find it
        /** @var Category $category */
        $category = $categoryManager->findOneToEdit($id);
        if (!$category) {
            throw $this->createNotFoundException('Category not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des catégories", $this->get("router")->generate("admin_category_list"));
        $breadcrumbs->addItem("Modification");

        $form = $this->createForm(CategoryType::class, $category, [
            'validation_groups' => ['Default']
        ]);


        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($category->getTranslations()) > 0) {
                    $categoryManager->save($category);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Mise à jour effectuée"
                    );

                    return $this->redirect($request->headers->get('referer'));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/Category/edit.html.twig',
            array(
                'form'           => $form->createView(),
                'category'           => $category,
            )
        );
    }

    /**
     * Delete one category
     * @Route("/delete/{id}", name="admin_category_delete")
     * @param Request $request Request
     * @param integer $id      Id of category
     */
    public function deleteAction(Request $request, CategoryManager $categoryManager, $id)
    {
        // Find it
        /** @var Category $category */
        $category = $categoryManager->findOneToEdit($id);
        if (!$category) {
            throw $this->createNotFoundException('Category not found');
        }

        $categoryManager->remove($category);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été supprimé"
        );

        return $this->redirect($request->headers->get('referer'));
    }



    /**
     * Duplicate one category
     * @Route("/duplicate/{id}", name="admin_category_duplicate")
     * @param Request $request Request
     * @param integer $id      Id of category
     */
    public function duplicateAction(Request $request, CategoryManager $categoryManager, $id)
    {
        // Find it
        /** @var Category $category */
        $category = $categoryManager->findOneToEdit($id);
        if (!$category) {
            throw $this->createNotFoundException('Category not found');
        }

        $this->getDoctrine()->getManager()->detach($category);
        foreach ($category->getTranslations() as $translation) {
            $translation->setId(null);
            $translation->setSlug(null);
            $this->getDoctrine()->getManager()->detach($translation);
        }
        $categoryManager->save($category);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été dupliqué"
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Export csv
     * @Route("/export/{locale}", name="admin_category_export")
     * @param Request $request
     */
    public function exportAction(Request $request, CategoryManager $categoryManager, $locale)
    {
        $data = $this->initSearch($request);
        $query = $categoryManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Nom',
            'Date de création',
            
        ], ';');

        $locale = $request->get('locale', $request->getDefaultLocale());
        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->translate($locale)->getName(),
                ($result->translate($locale)->getCreated()) ? $result->translate($locale)->getCreated()->format('d/m/Y') : $result->getCreated()->format('d/m/Y'),
                
            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_'.$locale.'_categorys_export.csv"'
        ));
    }
}
