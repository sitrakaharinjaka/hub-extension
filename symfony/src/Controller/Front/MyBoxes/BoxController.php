<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Front\MyBoxes;

use App\Controller\BaseController;
use App\Entity\MyBoxesBox;
use App\Entity\Project;
use App\Entity\Media;
use App\Entity\MyBoxesSelectBox;
use App\Entity\MyBoxesBringHome;
use App\Form\Type\MyBoxes\BoxType;
use App\Form\Type\MyBoxes\BoxEditType;
use App\Form\Type\MyBoxes\SelectBoxType;
use App\Form\Type\MyBoxes\BringHomeType;
use App\Utils\Managers\MyBoxesBoxLogManager;
use App\Utils\Managers\MyBoxesBoxManager;
use App\Utils\Managers\MyBoxesCommandManager;
use App\Utils\Services\SerializerService;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\Managers\MediaManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Utils\Services\MailerService;

/**
 * Class BoxController
 *
 * @Route("/myboxes")
 *
 * @package App\Controller
 */
class BoxController extends BaseController
{
    private function checkBoxUserList(MyBoxesBox $box)
    {
        $type = $box->getType();
        // On vire tous les BoxUser qui n'ont pas de user.
        //$box->cleanBoxUsers();

        if ($type != 'group') {
            // Si ce n'est pas un groupe alors on vire tout le monde, sauf le user connecté
            $box->clearBoxUsers();
            $box->addUser($this->getUser());
        } else {
            // On s'assure que le User connecté et encore dans la liste.
            $box->addUser($this->getUser());
        }
    }

    /**
     * Homepage
     *
     * @Route("/envoyer", name="myboxes_box_create")
     * @return Response
     */
    public function create(Request $request, MyBoxesBoxManager $boxManager, MailerService $mailer)
    {
        $box = new MyBoxesBox();
        $box->setUser($this->getUser());
        $box->addUser($this->getUser());
        $box->setState(MyBoxesBox::STATE_BOX_USER_SENT);
        $box->setIsEmpty(false);
        $form = $this->createForm(BoxType::class, $box, [
            'modeSelect' => 'create',
        ]);

        if (sizeof($box->getBoxUsers()) > 1) {
            $box->setType('group');
        } else {
            $box->setType('perso');
        }

        // Update method
        if ('POST' === $request->getMethod()) {
            // Bind value with form
            $form->handleRequest($request);

            $this->checkBoxUserList($box);

            if ($form->isValid()) {
                $box->cleanBoxUsers();
                // Si le codebarre existe, alors, j'écrase l'ancienne.
                $boxOld = $boxManager->findOneBy(['barcode' => $box->getBarcode()]);
                if ($boxOld) {
                    $boxManager->remove($boxOld);
                }
                $boxManager->save($box);
                $mailer->sendMailMyBoxesBoxUserSent($box);
                // TODO : Devra être sur le statut "Accepté" quand il sera mis en place ...

                $this->addFlash('flash_mb', "Votre demande a bien été enregistrée");
                return $this->redirect($this->generateUrl('myboxes_box_list'));
            }
        }

        return $this->render('hub/myboxes/box/create.html.twig', [
            'form' => $form->createView(),
            'mode' => 'create',
        ]);
    }

    /**
     * Homepage
     *
     * @Route("/renvoyer/{id}", name="myboxes_box_backhub")
     * @return Response
     */
    public function backHub(Request $request, MyBoxesBoxManager $boxManager, MailerService $mailer, MyBoxesBoxLogManager $myBoxesBoxLogManager, $id)
    {
        $box = $boxManager->findOneToEdit($id);
        if (!$box) {
            throw $this->createNotFoundException("No box found");
        }

        if (sizeof($box->getBoxUsers()) > 1) {
            $box->setType('group');
        } else {
            $box->setType('perso');
        }

        $box->setState(MyBoxesBox::STATE_BOX_USER_SENT);
        $box->setSeal(null);
        $form = $this->createForm(BoxType::class, $box, [
            'modeSelect' => 'update',
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {
            // Bind value with form
            $form->handleRequest($request);
            $this->checkBoxUserList($box);
            if ($form->isValid()) {
                $box->cleanBoxUsers();
                if ($box->getIsEmpty()) {
                    $box->setName('- vide - '); // On met un nom particulier jusqu'à l'inventaire des stocks de VEOLOG
                    $box->setContent('');
                    $box->setSeal('');
                    $box->clearBoxMedias();
                    // On laisse les users jusqu'à l'inventaire des stocks de VEOLOG
                }
                $boxManager->save($box);
                $mailer->sendMailMyBoxesBoxUserSent($box);
                $myBoxesBoxLogManager->log($box, 'resend');
                $this->addFlash('flash_mb', "Votre demande a bien été enregistrée");
                return $this->redirect($this->generateUrl('myboxes_box_list'));
            }
        }

        return $this->render('hub/myboxes/box/create.html.twig', [
            'form' => $form->createView(),
            'mode' => 'update',
        ]);
    }

    /**
     * Homepage
     *
     * @Route("/stock", name="myboxes_box_list")
     * @return Response
     */
    public function list(Request $request, MyBoxesBoxManager $boxManager, MyBoxesCommandManager $commandManager)
    {
        $perso = $boxManager->findAllPerso($this->getUser());
        $team = $boxManager->findAllTeam($this->getUser());
        $commands = $commandManager->findListableByUser($this->getUser());

        $bring = new MyBoxesSelectBox();
        $formBringHome = $this->createForm(SelectBoxType::class, $bring, [
            'user' => $this->getUser(),
            'type' => 'bring-home',
        ]);

        $back = new MyBoxesSelectBox();
        $formBackHub = $this->createForm(SelectBoxType::class, $bring, [
            'user' => $this->getUser(),
            'type' => 'back-hub',
        ]);

        return $this->render('hub/myboxes/box/index.html.twig', [
            'formBringHome' => $formBringHome->createView(),
            'openBringHome' => false, // $openBringHome,
            'formBackHub' => $formBackHub->createView(),
            'openBackHub' => false, // $openBackHub,
            'perso' => $perso,
            'team' => $team,
            'commands' => $commands,
        ]);
    }

    /**
     * @Route("/stock/rapatrier", name="myboxes_box_bringhome_test")
     * @return Response
     */
    public function testBringHome(Request $request)
    {
        $select = new MyBoxesSelectBox();
        $form = $this->createForm(SelectBoxType::class, $select, [
            'user' => $this->getUser(),
            'type' => 'bring-home',
        ]);

        $open = false;
        if ('POST' === $request->getMethod()) {
            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                return $this->redirect($this->generateUrl('myboxes_command_box', [
                    'id' => $select->getBox()->getId(),
                ]));
            } else {
                $open = true;
            }
        }

        return $this->redirect($this->generateUrl('myboxes_box_list', [
            'openBringHome' => $open,
            'selectBringHome' => $select->getBox() ? $select->getBox()->getId() : null,
        ]));
    }

    /**
     * @Route("/stock/renvoyer", name="myboxes_box_backhub_test")
     * @return Response
     */
    public function testBackHub(Request $request)
    {
        $select = new MyBoxesSelectBox();
        $form = $this->createForm(SelectBoxType::class, $select, [
            'user' => $this->getUser(),
            'type' => 'back-hub',
        ]);

        $open = false;
        if ('POST' === $request->getMethod()) {
            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                return $this->redirect($this->generateUrl('myboxes_box_backhub', [
                    'id' => $select->getBox()->getId(),
                ]));
            } else {
                $open = true;
            }
        }

        return $this->redirect($this->generateUrl('myboxes_box_list', [
            'openBackHub' => $open,
            'selectBackHub' => $select->getBox() ? $select->getBox()->getId() : null,
        ]));
    }

    /**
     * Page de visulisation de box
     *
     * @Route("/box/{id}", name="myboxes_box_view")
     * @return Response
     */
    public function view(Request $request, MyBoxesBoxManager $boxManager, $id)
    {
        $box = $boxManager->findForView($id);

        if (!$box) {
            throw $this->createNotFoundException("No box found");
        }

        return $this->render('hub/myboxes/box/show.html.twig', [
          'box' => $box,
          'bringableHome' => $boxManager->isBringable($box->getId()),
        ]);
    }

    /**
     * Edit box
     *
     * @Route("/box/{id}/modifier", name="myboxes_box_edit")
     * @return Response
     */
    public function edit(Request $request, MyBoxesBoxManager $boxManager, $id)
    {
        $box = $boxManager->findOneBy(['id' => $id]);

        if (!$box) {
            throw $this->createNotFoundException("No box found");
        }

        if (sizeof($box->getBoxUsers()) > 1) {
            $box->setType('group');
        } else {
            $box->setType('perso');
        }

        $form = $this->createForm(BoxEditType::class, $box, [
        ]);

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            $this->checkBoxUserList($box);

            if ($form->isValid()) {
                $box->cleanBoxUsers();
                $boxManager->save($box);
                $this->addFlash('flash_mb', "Votre demande a bien été enregistrée");
                return $this->redirect($this->generateUrl('myboxes_box_list'));
            }
        }

        return $this->render('hub/myboxes/box/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/media/upload", name="myboxes_media_upload")
     * @param Request $request
     *
     * @return Response
     */
    public function uploadMedia(Request $request, MediaManager $mediaManager, SerializerService $serializerService)
    {
        $response = new Response();
        $response->setStatusCode(400);

        // Get File to upload
        $file = $request->files->get('file', null);

        if (!$file) {
            return $response;
        }

        $media = new Media();
        $media->setFileMedia($file);
        $media = $mediaManager->save($media);

        if ($media) {
            $response->setStatusCode(200);
            $serializer = $this->get('serializer');
            $tab = array(
                'id' => $media->getId(),
                'extension' => $media->getExtension(),
                'mimetype' => $media->getMimetype(),
                'legend' => $media->getLegend(),
                'url' => $this->get('router')->generate('media_view', ['id' => $media->getId()], UrlGeneratorInterface::ABSOLUTE_PATH),
                'title' => $media->getTitle(),
                'alt' => $media->getAlt(),
            );
            $datas = $serializerService->serializeObjectToJson($tab, 'json');
            $response->setContent($datas);
        }

        return $response;
    }
}
