<?php

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;

use App\Entity\Media;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class MediaManager
 * `
 * Object manager of media
 *
 * @package App\Utils\Managers
 */
class MediaManager extends BaseManager
{
    /**
     * @var MediaRepository
     */
    protected $mediaRepository;

    /**
     * MediaManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager = null)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('mediaRepository', Media::class);
    }

    /**
     * @param Media $media
     *
     * @return Media
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Media $media)
    {
        $media->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($media);
        $this->getEntityManager()->flush();

        return $media;
    }


    /**
     * @param Media $media
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(Media $media)
    {
        $this->getEntityManager()->remove($media);
        $this->getEntityManager()->flush();
    }
    
    /**
     * Get all media
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->mediaRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->mediaRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->mediaRepository->findOneBy($filters);
    }
    /**
     * Count all page
     */
    public function countAll()
    {
        return $this->mediaRepository->countAll();
    }
}
