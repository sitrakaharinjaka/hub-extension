<?php

namespace App\Validator\Constraints;

use App\Entity\Project;
use App\Entity\ProjectItem;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockLineManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use DateTime;

class ProjectItemValidator extends ConstraintValidator
{
    /**
     * @var ProjectManager
     */
    private $projectManager;

    /**
     * @var StockLineManager
     */
    private $stockLineManager;

    /**
     * StockClassValidator constructor.
     */
    public function __construct(ProjectManager $projectManager, StockLineManager $stockLineManager)
    {
        $this->projectManager = $projectManager;
        $this->stockLineManager = $stockLineManager;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var ProjectItem $projectItem */
        $item = $value;
        /** @var Project $project */
        $project = $item->getProject();

        $datas = $this->projectManager->getParameters($project);

        if ($datas['cave'] == Project::CAVE_115) {
            $total = $this->stockLineManager->totalStockDateOnProduct($project->getStock(), $datas['cave'], $item->getProduct()->getId(), $datas['dateStart'], $datas['dateEnd'], true);
        }else{
            $total = $this->stockLineManager->quantityOf($item->getProduct(), Project::CAVE_PANTIN, new DateTime('now'), $project->getStock(), false);
        }

        $operation = ($project->getBaseItem($item->getProduct()->getId()) + $item->getQuantity());
        $operation = $operation + $project->getBookItem($item->getProduct()->getId());

        if ($operation > $total) {
            $this->context->buildViolation($constraint->message)->setParameters([
                '%count%' => $total
            ])->atPath('quantity')->addViolation();
        }
    }
}
