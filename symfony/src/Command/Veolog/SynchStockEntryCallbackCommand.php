<?php

namespace App\Command\Veolog;

use App\Command\Veolog\AbstractVeologCommand;
use App\Entity\Product;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\StockEntry;
use App\Entity\StockEntryItem;
use App\Entity\StockEntryRejection;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\StockEntryManager;
use App\Utils\Managers\StockLineManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Exception;
use DateTime;

//use Symfony\Component\HttpKernel\KernelInterface;

class SynchStockEntryCallbackCommand extends AbstractVeologCommand
{
    private $stockEntryManager;
    private $productManager;
    private $entityManager;
    private $stockLineManager;

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, StockEntryManager $stockEntryManager, ProductManager $productManager, EntityManagerInterface $entityManager, StockLineManager $stockLineManager)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->stockEntryManager    = $stockEntryManager;
        $this->productManager    = $productManager;
        $this->entityManager    = $entityManager;
        $this->stockLineManager = $stockLineManager;
    }

    protected static $defaultName = 'veolog:stock-entries-callback';

    protected function configure()
    {
        $this
            ->setDescription('Synch veolog stock entries callback')
            ->addOption(
                'vlgenv',
                null,
                InputOption::VALUE_OPTIONAL,
                'Env veolog prod?',
                false
            )
            ->setHelp('This command allows you to recept veolog stock entries callback');
    }

    /**
     * On veux les fichiers
     * - status_recept_XXXXXXXXX.csv
     * - /XXX/status_recept_XXXXXXXXX.csv
     * On ne veux pas :
     * - XXXstatus_recept_XXX.csv
     * - status_recept_XXX.xls
     * - status_recept_BOX_XXX.csv
     */
    private function isCommandFile($filename)
    {
        $pattern='~^/?([a-zA-Z0-9_]*/)*status_recept_[0-9]*\.csv$~';
        return preg_match($pattern, $filename);
    }

    /**
     * Get all files for status recept on FTP directory
     */
    private function getFTPFiles()
    {
        $this->log('info', 'Scan du répertoire');
        $directory = $this->getDirVlgOUT();
        $ftp_handle = $this->connectFTP();
        $filenames = ftp_nlist($ftp_handle, $directory);
        sort($filenames);

        $avalaible_file = [];
        foreach ($filenames as $filename) {
            if ($this->isCommandFile($filename)) {
                $avalaible_file[] = $filename;
            }
        }

        return $avalaible_file;
    }

    private function traitement($filename)
    {
        $this->log('info', 'Traitement du fichier ' . $filename);
        $ftp_handle = $this->connectFTP();
        $tmp_handle = fopen('php://temp', 'r+');

        $filename = $this->getDirVlgOUT() . '/' . basename($filename);

        if (ftp_fget($ftp_handle, $tmp_handle, $filename, FTP_ASCII)) {
            rewind($tmp_handle);
            $statusE = null;
            $stockEntry = null;
            $itemRowsToProcess = [];

            try {
                while ($row = fgetcsv($tmp_handle, 0, ';')) {

                    if ($row[0]=='E') {
                        $statusE = $row[7];
                        $stockEntry = $this->traitementStockEntry($row);
                    } elseif ($row[0] == 'L' || $row[0] == 'R') {
                        $itemRowsToProcess[] = $row;
                    }
                }
                if ($stockEntry) {
                    foreach($itemRowsToProcess as $rowToProcess){
                        $this->traitementItem($statusE, $stockEntry, $rowToProcess);
                    }
                    $this->stockEntryManager->save($stockEntry);
                }
                
                $this->removeFTPFile($ftp_handle, $filename);
            } catch (\Exception $e) {
                $this->log('info', '<error>Failure</error> : ' . $e->getMessage());
            }
            ftp_close($ftp_handle);
        } else {
            throw new Exception('Unable download ftp file');
        }
    }

    /**
     * 1 Type ligne                     A1  X E
     * 2 Entité de stocks               A15 X Ex : RNTFR
     * 3 Réf. réception donneur d’ordre A15 X Référence 1 de la réception
     * 4 Réf. Réception fournisseur     A15 Identifiant si reliquat
     * 5 Code fournisseur               A10 X Ce code peut être figé par défaut
     * 6 Date de réception              N8  X Format figé par exemple AAAAMMJJ
     * 7 Heure de réception             N4  X Format figé par exemple HHMM
     * 8 Etat                           A1  X I : intégré / S : stocké
     */
    private function traitementStockEntry($row)
    {
        $stockEntryId = $this->getIdFromVeologCode($row[2]);

        $stockEntry = $this->stockEntryManager->findOneToUpdate($stockEntryId);

        if ($stockEntry == null) {
            throw new Exception('Bad stock entry ID');
        }

        $this->log('info', 'Clearing previous rejections..');
        $this->entityManager->getRepository(StockEntryRejection::class)->removeByStockEntry($stockEntry);

        if ($row[7]=='I') {
            // ==> Status "Intégré par VEOLOG"
            $this->log('info', 'Intégré par VEOLOG');
            // TODO : Mise à jour du statut du stockEntry ? Attention dans ce cas à l'ordre des fichiers ...
            // $stockEntry->setEntryStatus(XXXXX);
            $this->mailer->sendStocketEntryConfirm($stockEntry);
        } elseif ($row[7]=='S') {
            // ==> Status "Stocké par VEOLOG"
            $this->log('info', 'Stocké par VEOLOG');
            // TODO : Mise à jour du statut du stockEntry ?
            // $stockEntry->setEntryStatus(XXXXX);
            $this->mailer->sendStocketEntryUpdate($stockEntry);
        } elseif ($row[7] == 'R') {
            // ==> Status "Rejeté par VEOLOG"
            $this->log('info', 'Rejeté par VEOLOG');
            
              //$this->mailer->sendStocketEntryReject($stockEntry, $item, $user);
        }

        return $stockEntry;
    }

    /**
     * 1 Type ligne         A1  X L
     * 2 Code article       A30 X
     * 3 Libellé article    A30
     * 4 Quantité           N   X
     * 
     * @param StockEntry $stockEntry
     */
    private function traitementItem($statusE, $stockEntry, $row)
    {
        if ($statusE == 'I') {
            $code = $row[1];
            $item = $stockEntry->getStockEntryItemByCode($code);
            if ($item) {
                $this->log('info', '--> INTEGRATED');
                $item->setStatus(StockEntry::STATE_INT_VEOLOG);
                return true;
            }

            throw new Exception('Product not found');

        }

        if ($statusE == 'S') {
            $code = $row[1];
            $qty = $row[3];
            $item = $stockEntry->getStockEntryItemByCode($code);
            $this->log('info', '--> ' . $qty . 'x ' . $code);
            if ($item) {
                $previous = $item->getQuantityReceived();
                $item->setQuantityReceived($previous + $qty);
                $this->log('info', '--> UPDATED');
                $item->setStatus(StockEntry::STATE_ITEM_UPDATE);

                /**
                 * #71293 - If quantity received equals quantity announced, we validate the stock entry
                 */
                if($item->getQuantity() == $item->getQuantityReceived()){
                    $this->log('info', '--> Qty received == qty requested. Validating stock entry...');
                    $item->setStatus(StockEntry::STATE_ITEM_VALIDATE);
                    $this->stockLineManager->addLine($stockEntry->getStock(), $item->getProduct(), 'Entrée de stock #' . $stockEntry->getId(), $item->getQuantityReceived(), new DateTime('now'), 'pantin', null, 'stockEntry');
                }

                return true;
            }

            throw new Exception('Product not found');

        }
        
        if ($statusE == 'R') {
            $veologRejectMessage = $row[2];

            // Check if the message is a product code and if a stock entry item with this product code exists
            $item = $stockEntry->getStockEntryItemByCode($veologRejectMessage);

            // If this is a product code, it means that Veolog was unable to find this product
            if ($item) {
                $this->log('info', '--> REJECTED');
                $item->setStatus(StockEntry::STATE_REJECT_VEOLOG);

                $this->createStockEntryRejection($stockEntry, $item, StockEntry::PRODUCT_NOT_FOUND_VEOLOG_REJECTION);

                return true;
            }

            // If no items found with the product code, it means that veolog rejected the stock entry for other reasons
            $motif = StockEntry::DUPLICATE_STOCK_ENTRY_VEOLOG_REJECTION; //To fetch
            $this->createStockEntryRejection($stockEntry, null, $motif);
        }
    }

    private function createStockEntryRejection($stockEntry, $stockEntryItem, $rejectionMsg)
    {
        $rejection = new StockEntryRejection();

        $rejection->setStockEntry($stockEntry);
        $rejection->setStockEntryItem($stockEntryItem);
        $rejection->setRejectionMsg($rejectionMsg);
        $rejection->setStatus(StockEntry::STATE_REJECT_VEOLOG);

        $this->entityManager->persist($rejection);
        $this->entityManager->flush();

        return $rejection;
    }
    
    private function getIdFromVeologCode($code)
    {
        $exploded = explode('_', $code);
        $this->log('info', 'Transform ID from VEOLOG : ' . $code . " => " . $exploded[0]);
        return intval($exploded[0]);
    }

    private function removeFTPFile($ftp_handle, $filename)
    {
        $this->log('info', 'Removing FTP file ' . $filename);
        if (ftp_delete($ftp_handle, $filename)) {
            $this->log('info', 'Removed FTP file ' . $filename . ' <info>success</info>');
        } else {
            $this->log('info', 'Removed FTP file ' . $filename . ' <comment>error</comment>');
        }
    }

    protected function executeCommand()
    {
        $this->log('info', 'Récupération des callback des entrées de stock effectuées par VEOLOG');
        $filenames = $this->getFTPFiles();
        foreach ($filenames as $filename) {
            $this->traitement($filename);
        }
        $this->log('info', 'Fin du traitement');
    }
}
