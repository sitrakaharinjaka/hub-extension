<?php

namespace App\Repository;

use App\Repository\BaseRepository;
use App\Command\Veolog\SynchProductCommand;

/**
 * Class ProductRepository
 */
class ProductRepository extends BaseRepository
{


  /**
   * ================= FRONT ===================
   */


    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearchInFront($stock, $cave = null, $name = null, $category = null, $project = null)
    {
        $qb = $this->createQueryBuilder('e')
                    ->select('e', 'et')
                    ->leftJoin('e.translations', 'et')
                    ->orderBy('et.name', 'asc');

        $qb ->andWhere(
            $qb->expr()->in(
                'e.id',
                $this->queryBuilderInStock($stock, $cave)->getDQL()
            )
        );
        $qb ->setParameter('stock', $stock);
        if (!empty($cave)) {
            $qb ->setParameter('cave', $cave);
        }

        if (!empty($name)) {
            $qb->andWhere('et.name LIKE :name'
                    .' OR  et.code LIKE :code'
                    .' OR  et.eanPart LIKE :eanPart'
                    .' OR  et.eanBox LIKE :eanBox'
                    .' OR  et.description LIKE :description'
                    .' OR  et.material LIKE :material'
                    .' OR  et.color LIKE :color')
                ->setParameter('name', '%'.$name.'%')
                ->setParameter('code', '%'.$name.'%')
                ->setParameter('eanPart', '%'.$name.'%')
                ->setParameter('eanBox', '%'.$name.'%')
                ->setParameter('description', '%'.$name.'%')
                ->setParameter('material', '%'.$name.'%')
                ->setParameter('color', '%'.$name.'%');
        }


        if (!empty($category)) {
            $qb->addSelect('c');
            $qb->leftJoin('et.categories', 'c');

            if (!is_array($category)) {
                $category = [$category];
            }

            $qb->andWhere($qb->expr()->in('c.id', $category));
        }

        if (!empty($project)) {
            if (!$project->getNeedReturn()) {
                // Remove this line to desactivate
                // $qb->andWhere('et.isExpendable = TRUE');
            }
        }

        $query = $qb->getQuery();

        return $query;
    }

    public function queryBuilderInStock($stock, $cave)
    {
        $qb = $this->createQueryBuilder('p');
        $qb ->select('p.id')
            ->distinct()
            ->leftJoin('p.stockLines', 'sl')
            ->leftJoin('sl.stock', 's')
            ->andWhere('s.id = :stock')
            ->andWhere('sl.stockVariation = false AND sl.archived = false AND sl.number > 0')
            ->setParameter('stock', $stock);
        if (!empty($cave)) {
            $qb ->andWhere('sl.cave = :cave')
                ->setParameter('cave', $cave);
        }
        return $qb;
    }

    public function getProductsByFilters($categoryId, $brandId, $name, $code, $ean, $limit)
    {
        $qb = $this->createQueryBuilder('p');
        $qb ->select('p')
            ->leftJoin('p.categories', 'c')
            ->andWhere('c.id = :categoryId')
            ->setParameter('categoryId', $categoryId)
            ->setMaxResults($limit);
        if ($name !== null) {
            $qb->andWhere('p.name LIKE :name')
                ->setParameter('name', '%' . $name . '%');
        }
        if ($code !== null) {
            $qb->andWhere('p.code LIKE :code')
                ->setParameter('code', '%' . $code . '%');
        }
        if ($ean !== null) {
            $qb->andWhere('p.eanBox LIKE :ean OR p.eanPart LIKE :ean')
                ->setParameter('ean', '%' . $ean . '%');
        }
        if ($brandId !== null) {
            $qb->leftJoin('p.brand', 'b')
                ->andWhere('b.id = :brandId')
                ->setParameter('brandId', $brandId);
        }
        return $qb->getQuery()->getResult();
    }


    /**
     * Check if code exists
     *
     * @return mixed
     */
    public function checkExist($code)
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->select('p')
            ->leftJoin('p.translations', 'pt')
            ->andWhere('pt.code = :code')
            ->setParameter('code', $code)
            ->setMaxResults(1)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Ceck Code and Name Correspondance validity
     *
     * @return mixed
     */
    public function checkCodeAndName($code, $name)
    {
        $name = strtolower($name);

        $qb = $this->createQueryBuilder('p');
        $qb
            ->leftJoin('p.translations', 'pt')
            ->andWhere('pt.code = :code')
            ->andWhere('LOWER(pt.name) = :name')
            ->setMaxResults(1)
            ->setParameter('code', $code)
            ->setParameter('name', $name)
        ;
        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * ================= BACK ===================
     */


    public function findOneTranslationBy($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
        ->select('e', 'et')
        ->leftJoin('e.translations', 'et');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'et';
                $qb->andWhere($alias . '.' . $key . ' = :' . $key);
                $qb->setParameter($key, $filter);
            }
        }

        $query = $qb->getQuery();
        return $query->getResult();
    }

    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    private function queryBuilderForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
                    ->select('e', 'et')
                    ->leftJoin('e.translations', 'et')
                    ->orderBy('et.name', 'asc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                if ($key == 'dateStart' or $key == 'dateEnd') {
                    continue;
                }

                if ($key == 'filterQty') {
                    continue;
                }

                $alias = 'et';

                if ($key == 'ean') {
                    $qb->andWhere($qb->expr()->orX(
                        $qb->expr()->like($alias . '.eanPart', ':' . $key),
                        $qb->expr()->like($alias . '.eanBox', ':' . $key)
                    ));
                    $qb->setParameter($key, '%' . $filter . '%');
                    continue;
                }

                if ($key == 'id') {
                    $alias = 'e';
                }
                if ($key == 'sku') {
                    $key = 'code';
                }
                if ($key == 'brand') {
                    $qb->andWhere($alias . '.brand = :' . $key);
                    $qb->setParameter($key, $filter);
                    continue;
                }

                $qb->andWhere($alias . '.' . $key . ' LIKE :' . $key);
                $qb->setParameter($key, '%' . $filter . '%');
            }
        }
        return $qb;
    }

    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->queryBuilderForSearch($filters)->getQuery();
    }

    public function queryForSearchWithTotal($filters = array(), $stock, $cave)
    {
        $qb = $this->queryBuilderForSearch($filters);

        // TOTAL
        $qb
            ->addSelect('SUM(sl1.number) as total')
            ->leftJoin('e.stockLines', 'sl1', 'WITH', $qb->expr()->andX(
                'sl1.archived = false',
                'sl1.cave = :cave',
                'sl1.stock = :idStock'
            ))
        ;

        // TOTAL ON DATE
        $dateStart = $filters['dateStart'];
        $dateEnd = $filters['dateEnd'];
        $qb
            ->addSelect('SUM(sl2.number) as totalWithHold')
            ->leftJoin('e.stockLines', 'sl2', 'WITH', $qb->expr()->andX(
                'sl2.id = sl1.id',
                'sl2.archived = false',
                'sl2.cave = :cave',
                'sl2.date <= :dateStart',
                'sl2.stock = :idStock'
            ))
        ;

        $qb
            ->addSelect('SUM(sl3.number) as totalHoldVariation')
            ->leftJoin('e.stockLines', 'sl3', 'WITH', $qb->expr()->andX(
                'sl3.id = sl1.id',
                'sl3.archived = false',
                'sl3.cave = :cave',
                'sl3.number < 0',
                'sl3.holdForProject = :hold',
                'sl3.date <= :dateEnd',
                'sl3.date > :dateStart',
                'sl3.stock = :idStock'
            ))
        ;

        $filterQty = $filters['filterQty'];
        if ($filterQty=='stock') {
            $qb
                ->having('total > 0')
                ;
        } elseif ($filterQty=='few') {
            $qb
                ->addSelect('COUNT(sl4.id) as exitsOneDayInStock')
                ->leftJoin('e.stockLines', 'sl4', 'WITH', $qb->expr()->andX(
                    'sl1 IS NULL',
                    'sl4.cave = :cave',
                    'sl4.stock = :idStock'
                ))
                ->having('total < 20 OR exitsOneDayInStock > 0')
                ;
        } elseif ($filterQty=='all') {
            // ALL = NO condition (all total, existing or not in history, etc.)
        }


        $qb
            ->groupBy('e, et')
            ->setParameter('cave', $cave)
            ->setParameter('hold', true)
            ->setParameter('dateEnd', $dateEnd)
            ->setParameter('dateStart', $dateStart)
            ->setParameter('idStock', $stock)
        ;

        return $qb->getQuery();
    }




    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
      ->select('e', 'et', 'c', 'cat', 'catt', 'br', 'brt', 'img', 'imgimg')
      ->leftJoin('e.translations', 'et')
      ->leftJoin('et.categories', 'cat')
      ->leftJoin('cat.translations', 'catt')
      ->leftJoin('et.brand', 'br')
      ->leftJoin('br.translations', 'brt')
      ->leftJoin('et.cover', 'c')
      ->leftJoin('et.imageGallery', 'img')
      ->leftJoin('img.image', 'imgimg')
      ->where('e.id = :id')
      ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }


    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findAllTranslations($product)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
      ->from('App\Entity\ProductTranslation', 't')
      ->select('t')
      ->andWhere('t.product = :product')
      ->setParameter('product', $product);

        $query = $qb->getQuery();
        return $query->getResult();
    }

    /**
     * Find all with less than $level
     *
     * @param integer $level
     * @return mixed
     */
    public function findProductsToWarnFew($stock, $cave, $level)
    {
        $qb = $this->createQueryBuilder('p');
        $qb ->select('p, pt')
            ->addSelect('SUM(sl.number) as total')
            ->leftJoin('p.translations', 'pt')
            // On passe par un left join avec des conditions afin de sélectionner également les produits qui ont un stock à 0 mais qui ont déjà eut une valeur supérieure.
            ->leftJoin('p.stockLines', 'sl', 'WITH', $qb->expr()->andX(
                'sl.archived = false',
                'sl.cave = :cave',
                'sl.stock = :stock'
            ))
            ->groupBy('p, pt')
            ->having('total <= :level')
            ->orderBy('pt.name', 'asc')
            ->setParameter('level', $level)
            ->setParameter('stock', $stock)
            ->setParameter('cave', $cave)
        ;
        $query = $qb->getQuery();
        return $query->getResult();
    }


    /**
     * @param $stock
     * @param $idProduct
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countProductsInCategories($stock, $project)
    {
        // Start
        $qb = $this->createQueryBuilder('e')
      ->select('COUNT(DISTINCT(e.id)) as count, c.id, COUNT(sl.id) as countStock');

        if (!empty($project)) {
            if (!$project->getNeedReturn()) {
                $qb->leftJoin('e.translations', 'pt');
            // Remove this line to desactivate
                // $qb->leftJoin('e.translations', 'pt', 'WITH', 'pt.isExpendable = TRUE');
            } else {
                $qb->leftJoin('e.translations', 'pt');
            }
        } else {
            $qb->leftJoin('e.translations', 'pt');
        }

        $qb->leftJoin('pt.categories', 'c')
      ->leftJoin('e.stockLines', 'sl')
      ->andWhere('sl.stockVariation = false AND sl.archived = false AND sl.number > 0')
      ->groupBy('c.id')
      ->andWhere('sl.stock = :stock')
      ->setParameter('stock', $stock);


        $query = $qb->getQuery();

        $results = $query->getArrayResult();

        return $results;
    }


    /**
     * @param $stock
     * @param $idProduct
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findBySlug($slug)
    {
        $qb = $this->createQueryBuilder('p')
      ->select('p', 'pt', 'c', 'ct')
      ->leftJoin('p.translations', 'pt', 'WITH', 'pt.locale = :locale')
      ->leftJoin('pt.categories', 'c')
      ->leftJoin('c.translations', 'ct')
      ->andWhere('pt.slug = :slug')
      ->setParameter('locale', 'fr')
      ->setParameter('slug', $slug)
      ->setMaxResults(1);

        $query = $qb->getQuery();

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param $stock
     * @param $code
     * @param $state
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneTranslationByStockEanState($stock, $code, $state)
    {
        $qb = $this->createQueryBuilder('e')
               ->select('e', 'et')
               ->leftJoin('e.translations', 'et')
               ->leftJoin('e.stockLines', 'sl')
               ->leftJoin('sl.stock', 's')
               ->andWhere('s.id = :stock')
               ->andWhere('et.code = :code')
               ->andWhere('et.state = :state')
               ->setParameter('stock', $stock)
               ->setParameter('code', $code)
               ->setParameter('state', $state);

        $query = $qb->getQuery();

        $result = $query->getOneOrNullResult();

        return $result;
    }

    public function getSimilars($stock, $product, $count)
    {
        $categories = $product->translate('fr')->getCategories();
        $ids = [];
        foreach ($categories as $category) {
            $ids[] = $category->getId();
        }

        $qb = $this->createQueryBuilder('p')
      ->innerJoin('p.translations', 'pt', 'WITH', 'pt.locale = :locale')
      ->innerJoin('pt.categories', 'c')
      ->innerJoin('p.stockLines', 'sl');

        $qb->andWhere($qb->expr()->in('c.id', ':categories'))
      ->andWhere('p.id != :id')
      ->andWhere('sl.stock = :stock')
      ->andWhere('sl.stockVariation = false AND sl.archived = false AND sl.number > 0')
      ->setParameter('locale', 'fr')
      ->setParameter('categories', $ids)
      ->setParameter('stock', $stock)
      ->setParameter('id', $product->getId())
      ->setMaxResults($count)
      ->getQuery()
      ->getResult();

        return $qb->getQuery()->getResult();
    }

    /**
     * Get all product no synch
     *
     * @return mixed
     */
    public function getProductNoSynch($mode, $cave)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et')
            ->leftJoin('e.translations', 'et')
            ->orderBy('et.name', 'asc');
        if ($mode == SynchProductCommand::MODE_DELTA) {
            $qb->andWhere('e.dateSynch IS NULL');
        } elseif ($mode == SynchProductCommand::MODE_FULL) {
            // Pas de restriction pour le full
        }
        return $qb->getQuery()->getResult();
    }


    /**
     * Find by prefix hub code
     *
     * @return mixed
     */
    public function findByPrefixHubCode()
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et')
            ->leftJoin('e.translations', 'et')
            ->orderBy('et.name', 'asc');
        $qb->where('et.code LIKE :code')
        ->setParameter('code', "HUB_%");

        return $qb->getQuery()->getResult();
    }



    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsCreated($start, $end)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('IFNULL(count(c.id), 0) as cpt, CONCAT(YEAR(c.created), \'/\' ,MONTH(c.created), \'/\' , DAY(c.created)) AS date, DAYOFYEAR(c.created) as dayofyear');
        $qb->andWhere("c.created IS NOT NULL");
        $qb->groupBy("dayofyear");
        $qb->addOrderBy('dayofyear', 'asc');

        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("c.created >= :start");
        $qb->andWhere("c.created <= :end");


        $results = $qb->getQuery()->getScalarResult();
        return $results;
    }


    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsCreatedHub($start, $end)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('IFNULL(count(c.id), 0) as cpt, CONCAT(YEAR(c.created), \'/\' ,MONTH(c.created), \'/\' , DAY(c.created)) AS date, DAYOFYEAR(c.created) as dayofyear');
        $qb->leftJoin('c.translations', 'ct');
        $qb->andWhere('ct.code LIKE :like');
        $qb->andWhere("c.created IS NOT NULL");
        $qb->groupBy("dayofyear");
        $qb->addOrderBy('dayofyear', 'asc');

        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("c.created >= :start");
        $qb->andWhere("c.created <= :end");

        $qb->setParameter('like', "HUB_%");

        $results = $qb->getQuery()->getScalarResult();
        return $results;
    }

    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statsCreatedNoHub($start, $end)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('IFNULL(count(c.id), 0) as cpt, CONCAT(YEAR(c.created), \'/\' ,MONTH(c.created), \'/\' , DAY(c.created)) AS date, DAYOFYEAR(c.created) as dayofyear');
        $qb->leftJoin('c.translations', 'ct');
        $qb->andWhere('ct.code NOT LIKE :like');
        $qb->andWhere("c.created IS NOT NULL");
        $qb->groupBy("dayofyear");
        $qb->addOrderBy('dayofyear', 'asc');

        $qb->setParameter('start', $start);
        $qb->setParameter('end', $end);
        $qb->andWhere("c.created >= :start");
        $qb->andWhere("c.created <= :end");

        $qb->setParameter('like', "HUB_%");

        $results = $qb->getQuery()->getScalarResult();
        return $results;
    }

    /**
     * @return int|mixed|string
     */
    public function countProductsByStock()
    {
        $qb = $this->createQueryBuilder('p');
        $qb->select('SUM(sl.number) as quantity');
        $qb->addSelect('pt.code as sku');
        $qb->addSelect('pt.name as product');
        $qb->addSelect('sl.cave as cave');
        $qb->addSelect('st.name as stock');

        $qb->leftJoin('p.stockLines', 'sl', 'WITH', 'sl.archived = 0');
        $qb->leftJoin('sl.stock', 's');
        $qb->innerJoin('s.translations', 'st', 'WITH', "st.locale = 'fr'");
        $qb->innerJoin('p.translations', 'pt', 'WITH', "st.locale = 'fr'");

        $qb->andWhere('sl.stockVariation = false AND sl.archived = false AND sl.number > 0');

        $qb->groupBy('sku, product, cave, stock');

        $qb->where("p.created IS NOT NULL");

        $results = $qb->getQuery()->getResult();

        return $results;
    }
}
