<?php

/**
 * Form type
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Type\BulkOrder;

use App\Entity\BillingCenter;
use App\Entity\Service;
use App\Entity\ServiceTranslation;
use App\Form\Model\BulkOrder;
use App\Entity\Stock;
use App\Utils\Managers\LocaleManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\Fo;

/**
 *  Form Type
 */
class BulkOrderType extends AbstractType
{

    /**
     * @param LocaleManager $localeManager
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('excelFile', FileType::class, [
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'application/vnd.ms-excel',
                        'application/vnd.ms-excel',
                        'application/vnd.ms-excel',
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                    ],
                    'mimeTypesMessage' => 'S\'il vous plaît, téléversez un fichier Excel',
                ])
            ],
            'required' => false,
        ]);
        $builder->add('beforeServices', EntityType::class, [
            'class' => Service::class,
            'query_builder' => function (EntityRepository $erb) {
                return $erb->createQueryBuilder('u')
                    ->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale')
                    ->setParameter('locale', 'fr')
                    ->andWhere('t.choiceMomentService LIKE :before')
                    ->setParameter('before', ServiceTranslation::CHOICE_SERVICE_BEFORE)
                    ->orderBy('t.name', 'ASC');
            },
            "placeholder" => "",
            'choice_label' => 'name',
            'choice_attr' => function($choice, $key, $value) {
                return ['data-description' => $choice->getDescription()];
            },
            'attr' => [
                'id'    => 'field-copacking',
            ],
            'label' => '',
            'required' => false,
            'expanded' => true,
            'multiple' => true
        ]);

        $builder->add('needQuote', CheckboxType::class, [
            'label' => 'app.project.form.need_quote',
            'required' => false,
            'attr'     => [
                'id' => 'field-copacking-quote'
            ],
            'form_focus_label' => false,
            'form_checkbox' => true
        ]);
        $builder ->add('beforeQuote', TextType::class, [
            'label' => "Numéro de devis format AAMM_XXX ",
            'attr' => [
                'class'        => 'field',
                'id'           => 'field-number',
                'autocomplete' => 'cc-csc',
                'maxlength'    => 8
            ],
            'required' => false,
        ]);
        $builder->add('stock', EntityType::class, array(
            'class'        => Stock::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('s')
                    ->andWhere('s.id = :stockId')
                    ->setParameter('stockId', $this->session->get('current_stock', null)->getId());
            },
            'choice_label' => 'name',
            'required'     => true,
            'attr'         => array(
                'class'            => 'js-select',
                'id'               => 'field-votre-stock',
                'data-placeholder' => 'Votre stock*'
            ),
            'multiple'     => false,
        ));
        $builder->add('projectName', TextType::class, [
            'attr' => [
                'class'        => 'field',
                'id'           => 'field-name',
                'autocomplete' => 'cc-csc',
            ]
        ]);
        $stock = $this->session->get('current_stock', null);
        $builder->add('billingCenter', EntityType::class, array(
            'placeholder'  => 'Sélectionnez une réponse',
            'required'     => true,
            'class'        => BillingCenter::class,
            'query_builder' => function (EntityRepository $erb) use ($stock) {
                $qb = $erb->createQueryBuilder('u');
                $qb->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale')
                    ->setParameter('locale', 'fr')
                    ->leftJoin('u.stocks', 's')
                    ->andWhere('s.id = :id_stock ')
                    ->setParameter('id_stock', $stock->getId())
                    ->orderBy('t.name', 'ASC');

                return $qb;
            },
            'choice_label' => 'name',
            'attr' => [
                'class' => 'js-select',
                'id'    => 'field-votre-centre',
            ]
        ));
        $builder->add('deliveryDate', DateType::class, array(
            'widget' => 'single_text',
            'label'  => 'Date de livraison demandée',
            'html5'  => false,
            'format' => 'dd/MM/yyyy',
            'attr'   => [
                'class'        => 'field datepicker jsDateStart no-weekend',
                'id'           => 'field-date',
                'autocomplete' => 'off'
            ],
            'form_date' => true
        ));
    }

    /**
     * Configure Options
     *
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
            'data_class'         => BulkOrder::class
        ));
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bulkOrder';
    }
}
