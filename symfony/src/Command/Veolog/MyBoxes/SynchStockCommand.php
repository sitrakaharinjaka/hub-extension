<?php

namespace App\Command\Veolog\MyBoxes;

use App\Command\Veolog\AbstractVeologCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\ImageGallery;
use App\Entity\Media;
use App\Entity\MyBoxesBarcode;
use App\Entity\MyBoxesBox;
use App\Entity\MyBoxesProduct;
use App\Entity\Product;
use App\Entity\ProductTranslation;
use App\Entity\Project;
use App\Utils\Managers\MyBoxesBarcodeManager;
use App\Utils\Managers\MyBoxesBoxManager;
use App\Utils\Managers\MyBoxesProductManager;
use App\Utils\Managers\UserManager;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\MediaManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockLineManager;
use App\Utils\Managers\StockManager;
use App\Utils\Managers\ProjectItemManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * @property  mailer
 */
class SynchStockCommand extends AbstractVeologCommand
{

    /** @var  EntityManager */
    private $rootDir;

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, EntityManagerInterface $em, MyBoxesBarcodeManager $barcodeManager, MyBoxesProductManager $productManager, MyBoxesBoxManager $boxManager, UserManager $userManager, KernelInterface $kernel)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->productManager = $productManager;
        $this->barcodeManager = $barcodeManager;
        $this->boxManager     = $boxManager;
        $this->em               = $em;
        $this->rootDir = $kernel->getProjectDir();
    }

    protected static $defaultName = 'veolog:myboxes:stock-update';

    protected function configure()
    {
        $this
            ->setDescription('Synch veolog stock boxes')
            ->addOption(
                'vlgenv',
                null,
                InputOption::VALUE_OPTIONAL,
                'Env veolog prod?',
                false
            )
            ->setHelp('This command allows you to synch veolog stock');
    }

    /**
     * On veux les fichiers
     * - stock_BOX_AAMMJJHHMMSS.csv
     * - /XXX/stock_BOX_AAMMJJHHMMSS.csv
     * On ne veux pas :
     * - XXXstock_BOX_AMMJJHHMMSS.csv
     * - stock_BOX_AMMJJHHMMSS.xls
     * - stock_AAMMJJHHMMSS.csv
     */
    private function isCommandFile($filename)
    {
        $pattern='~^/?([a-zA-Z0-9_]*/)*stock_BOX_[0-9]*\.csv$~';
        return preg_match($pattern, $filename);
    }

    private function getErrorFile($filename)
    {
        return str_replace(".csv", "_ERROR.csv", $filename);
    }

    protected function executeCommand()
    {
        $dirVlg = "/" . $this->getDirVlgOUT() . "/";

        $errors = [];

        try {
            $this->log('info', 'Connecting FTP ...');
            $ftp_handle = $this->connectFTP();
            $this->log('info', 'Connected FTP : <info>success</info>');
            $this->log('info', 'Scan directory ' . $dirVlg . ' to find csv');

            $list_files = ftp_nlist($ftp_handle, $dirVlg);
            sort($list_files);
            $avalaible_file = [];

            // Check to take only stock_files
            foreach ($list_files as $file) {
                if ($this->isCommandFile($file)) {
                    $avalaible_file[] = $file;
                }
            }
            $this->log('info', '<info>' . sizeof($avalaible_file) . ' files</info> found');


            foreach ($avalaible_file as $file) {
                $this->log('info', 'Traitement du fichier ' . $file);
                $tmp_handle = fopen('php://temp', 'r+');
                if (ftp_fget($ftp_handle, $tmp_handle, $file, FTP_ASCII)) {
                    rewind($tmp_handle);

                    while ($csv_row = fgetcsv($tmp_handle)) {
                        $type_line = $csv_row[0][0];

                        if ($type_line == 'L') {
                            $product_detail = explode(';', $csv_row[0]);

                            // Get Sku
                            $code  = $product_detail[2];
                            $quantity = $product_detail[4];
                            $this->log('info', '    Traitement du produit ' . $code . ' x' . $quantity);

                            // PRODUCT = EMPTY BOX (red, blue, yellow) - Case Sku code product

                            $product = $this->productManager->findOneBy(['sku' => $code]);

                            if ($product) {
                                $this->log('info', '        Produit réévaluer à ' . $quantity);
                                $product->setStock($quantity);
                                $this->productManager->save($product);
                                $this->log('info', '        Produit réévaluer à ' . $quantity . ' <info>success</info>');
                                continue;
                            }

                            // BAR CODE Management exist or not, used or not
                            $barcode = $this->barcodeManager->findOneBy(['code' => $code]);
                            if (!$barcode) {
                                $this->log('info', '        Creation du code barre ' . $code);
                                $barcode = new MyBoxesBarcode();
                                $barcode->setCode($code);
                                $this->barcodeManager->save($barcode);
                                $this->log('info', '        Creation du code barre ' . $code . ' <info>success</info>');
                            }

                            $box = $this->boxManager->findOneBy(['barcode' => $code]);

                            // BOX - EXIST
                            if ($box) {
                                if ($quantity >= 1) {
                                    switch ($box->getState()) {
                                        case MyBoxesBox::STATE_BOX_VEOLOG_STOCK:
                                        case MyBoxesBox::STATE_BOX_USER_ASK:
                                            // Rien n'a changé
                                            break;
                                        case MyBoxesBox::STATE_BOX_USER_SENT:
                                        case MyBoxesBox::STATE_BOX_VEOLOG_ACCEPTED:
                                        case MyBoxesBox::STATE_BOX_VEOLOG_REFUSED:
                                        case MyBoxesBox::STATE_BOX_VEOLOG_PREPARED:
                                            $box->setState(MyBoxesBox::STATE_BOX_VEOLOG_STOCK);
                                            if ($box->getIsEmpty()) {
                                                $box->setName('');
                                                $box->setContent(''); // Normalament déjà vide depuis le renvoi à vide
                                                $box->setSeal(''); // Normalament déjà vide depuis le renvoi à vide
                                                $box->clearBoxMedias(); // Normalament déjà vide depuis le renvoi à vide
                                                $box->clearBoxUsers();
                                                $this->log('info', '        Cleaning empty box');
                                            }
                                            $this->boxManager->save($box);
                                            $this->log('info', '        Box set to ' . $box->getState());
                                            break;
                                        case MyBoxesBox::STATE_BOX_VEOLOG_SENT:
                                        case MyBoxesBox::STATE_BOX_USER_STOCK:
                                            $this->log('info', '    <comment>NO UPDATE</comment> : bad status (' . $box->getState() . ') for quantity x' . $quantity);
                                            $errors[] = 'La box ' . $box->getBarcode() . ' a été inventoriée dans le stock de VEOLOG alors qu\'elle est dans un état ' . $box->getState();
                                            break;
                                    }
                                    if ($quantity > 1) {
                                        // WARING !! On a au moins 2 box qui sont senséêtre UNIQUE.
                                        $this->log('info', '    <comment>Box UNIQUE mais en x' . $quantity . ' exemplaires ...');
                                        $errors[] = 'La box ' . $code . ' a été inventorié x' . $quantity . ' par VEOLOG, merci de vérifier cette quantité anormale.';
                                    }
                                } else { // quantity === 0
                                    switch ($box->getState()) {
                                        case MyBoxesBox::STATE_BOX_VEOLOG_STOCK:
                                        case MyBoxesBox::STATE_BOX_USER_ASK:
                                            $this->log('info', '    <comment>NO UPDATE</comment> : bad status (' . $box->getState() . ') for quantity x' . $quantity);
                                            $errors[] = 'La box ' . $box->getBarcode() . ' n\'a pas été inventoriée dans le stock de VEOLOG alors qu\'elle est dans un état ' . $box->getState();
                                            break;
                                        case MyBoxesBox::STATE_BOX_VEOLOG_SENT:
                                        case MyBoxesBox::STATE_BOX_VEOLOG_ACCEPTED:
                                        case MyBoxesBox::STATE_BOX_VEOLOG_REFUSED:
                                        case MyBoxesBox::STATE_BOX_VEOLOG_PREPARED:
                                            $box->setState(MyBoxesBox::STATE_BOX_USER_STOCK);
                                            $box->setTrackers([]);
                                            $this->boxManager->save($box);
                                            $this->log('info', '        Box set to ' . $box->getState());
                                            break;
                                        case MyBoxesBox::STATE_BOX_USER_SENT:
                                        case MyBoxesBox::STATE_BOX_USER_STOCK:
                                            // Rien n'a changé
                                            break;
                                    }
                                }
                                continue;
                            }

                            if ($quantity >= 1) {
                                $this->log('info', '    <comment>NO UPDATE</comment> : bad box (' . $code . ')');
                                $errors[] = 'Le code barre ' . $code . ' a été envoyé avec une quantité à x' . $quantity . ', cependant aucun Produit ni aucune Box ne correspondent.';
                            }
                        }
                    }

                    $this->log('info', '    Deleting files ' . $file);
                    // Tente d'effacer le fichier $file
                    if (sizeof($errors)>0) {
                        ftp_rename($ftp_handle, $file, $this->getErrorFile($file));
                        $this->log('info', '    Deleting files ' . $file . ' <comment>renamed</comment>');
                    } elseif (ftp_delete($ftp_handle, $file)) {
                        $this->log('info', '    Deleting files ' . $file . ' <info>success</info>');
                    } else {
                        $this->log('info', '    Deleting files ' . $file . ' <comment>error</comment>');
                    }
                } else {
                    $this->log('info', '    ==> fichier ' . $file . ' <error>erreur</error>');
                }

                fclose($tmp_handle);
            }

            ftp_close($ftp_handle);
        } catch (\Exception $e) {
            $this->log('info', '<error>Failure</error> : ' . $e->getMessage());
        }

        if (sizeof($errors)>0) {
            $this->log('info', 'Sending mail with errors ...');
            $this->mailer->sendMailMyBoxesErrorStock($errors);
            $this->log('info', 'Sent mail with errors : <info>success</info>');
        }
    }
}
