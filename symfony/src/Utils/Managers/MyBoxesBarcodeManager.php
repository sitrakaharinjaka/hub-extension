<?php

/**
 * Manager
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;
use App\Entity\MyBoxesBarcode;
use App\Entity\MyBoxesBox;
use App\Repository\MyBoxesBarcodeRepository;
use App\Repository\MyBoxesBoxRepository;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class MyBoxesBarcodeManager
 *
 * Object manager of user
 */
class MyBoxesBarcodeManager extends BaseManager
{
    /**
    * @var MyBoxesBarcodeRepository
    */
    protected $barcodeRepository;

    /**
    * @var MyBoxesBoxRepository
    */
    protected $boxRepository;

    /**
     * BaseManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager = null)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('barcodeRepository', MyBoxesBarcode::class);
        $this->addRepository('boxRepository', MyBoxesBox::class);
    }


    /**
     * Get all example
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->barcodeRepository->queryForSearch($filters);
    }


    /**
     * Save a myBoxesBarcode
     *
     * @param MyBoxesBarcode $myBoxesBarcode
     */
    public function save(MyBoxesBarcode $myBoxesBarcode)
    {
        $myBoxesBarcode->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($myBoxesBarcode);
        $this->getEntityManager()->flush();

        return $myBoxesBarcode;
    }

    /**
     * Remove one myBoxesBarcode
     *
     * @param MyBoxesBarcode $myBoxesBarcode
     */
    public function remove(MyBoxesBarcode $myBoxesBarcode)
    {
        $this->getEntityManager()->remove($myBoxesBarcode);
        $this->getEntityManager()->flush();
    }

    /**
     * Check if code exists
     *
     * @return mixed
     */
    public function checkExist($code)
    {
        return $this->barcodeRepository->checkExist($code);
    }

    /**
     * Check if code is free to use
     *
     * @return mixed
     */
    public function checkFree($code)
    {
        return $this->boxRepository->checkBarcodeFree($code);
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->barcodeRepository->findOneBy($filters);
    }
}
