<?php

/**
 * Form type
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Type\BillingCenter;

use A2lix\TranslationFormBundle\Form\Type\TranslationsFormsType;
use App\Entity\BillingCenter;
use App\Entity\Stock;
use App\Utils\Managers\LocaleManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\Fo;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 *  Form Type
 */
class BillingCenterType extends AbstractType
{
    protected $localeManager;

    /**
     * @param LocaleManager $localeManager
     */
    public function __construct(LocaleManager $localeManager)
    {
        $this->localeManager = $localeManager;
    }


    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $defaultLocales = $this->localeManager->findDefaultSlug();

        $builder->add('stocks', EntityType::class, [
          'class' => Stock::class,
          'query_builder' => function (EntityRepository $erc) {
              return $erc->createQueryBuilder('u')
              ->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale')
              ->setParameter('locale', 'fr')
              ->orderBy('t.name', 'ASC');
          },
          'choice_label' => 'name',
          'by_reference' => false,
          'multiple' => true,
          'required'  => true
        ]);

        $builder->add('translations', TranslationsFormsType::class, [
            'label' => false,
            'default_locale' => $defaultLocales,
            'required_locales' => $defaultLocales,
            'locales' => $this->localeManager->findAllSlug(),
            'form_type' => BillingCenterTranslationType::class
        ]);
    }

    /**
     * Configure Options
     *
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
            'data_class' => BillingCenter::class
        ));
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'billingcenter';
    }
}
