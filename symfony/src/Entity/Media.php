<?php

namespace App\Entity;

use App\Entity\Traits\AuthorableTrait;
use App\Entity\Traits\GedmoTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Entity\Traits\GaufretteTrait as GaufretteTrait;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MediaRepository")
 * @ORM\Table(name="dk_media")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Media
{
    use GaufretteTrait;
    use ORMBehaviors\Translatable\Translatable;
    use GedmoTrait;
    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $pathMedia;

    protected $fileMedia;

    protected $fileAtDeleteMedia = false;

    protected $base64Media;

    /**
     * Mime Type
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $mimetype;


    /**
     * Extension
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $extension;

    /**
     * Size
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $size;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function __call($method, $arguments)
    {
        if (!method_exists(self::getTranslationEntityClass(), $method)) {
            $method = 'get'.ucfirst($method);
        }

        return $this->proxyCurrentLocaleTranslation($method, $arguments);
    }

    public function getName()
    {
        return $this->__call('title', array());
    }

    /**
     * @return mixed
     */
    public function getPathMedia()
    {
        return $this->pathMedia;
    }

    /**
     * @param mixed $pathMedia
     */
    public function setPathMedia($pathMedia)
    {
        $this->pathMedia = $pathMedia;
    }

    /**
     * @return mixed
     */
    public function getFileMedia()
    {
        return $this->fileMedia;
    }

    /**
     * @param mixed $fileMedia
     */
    public function setFileMedia($fileMedia)
    {
        $this->fileMedia = $fileMedia;
    }

    /**
     * @return boolean
     */
    public function isFileAtDeleteMedia()
    {
        return $this->fileAtDeleteMedia;
    }

    /**
     * @param boolean $fileAtDeleteMedia
     */
    public function setFileAtDeleteMedia($fileAtDeleteMedia)
    {
        $this->fileAtDeleteMedia = $fileAtDeleteMedia;
    }

    /**
     * @return mixed
     */
    public function getBase64Media()
    {
        return $this->base64Media;
    }

    /**
     * @param mixed $base64Media
     */
    public function setBase64Media($base64Media)
    {
        $this->base64Media = $base64Media;
    }

    /**
     * To string data
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Get public path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->getWebPath('Media');
    }

    /**
     * @return mixed
     */
    public function getMimetype()
    {
        if (!$this->mimetype) {
            $this->mimetype = "Undefined";
        }

        return $this->mimetype;
    }

    /**
     * @param mixed $mimetype
     */
    public function setMimetype($mimetype)
    {
        $this->mimetype = $mimetype;
    }

    /**
     * @return mixed
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param mixed $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        $exp = explode('/', $this->getPath());
        $exp = explode('_', $exp[count($exp) - 1]);
        return $exp[count($exp) - 1];
    }
}
