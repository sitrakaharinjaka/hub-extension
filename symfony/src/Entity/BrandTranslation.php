<?php

namespace App\Entity;

use App\Entity\Traits\AuthorableTrait;
use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\SeoTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;


// Use For Algolia Or Api
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BrandRepository")
 * @ORM\Table(name="dk_brand_translation")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class BrandTranslation implements NormalizableInterface
{
    use ORMBehaviors\Translatable\Translation;
    use GedmoTrait;
    
    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    
    /**
     * Name
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $name;

    /**
     * Slug
     *
     * @Gedmo\Slug(fields={"name"}, unique=false)
     * @ORM\Column(length=255, unique=false)
     */
    protected $slug;
    

    

    

    

    

    


    

    

    

    /**
     * Set id
     *
     * @param integer $id
     * @return Brand
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set name
     *
     * @param string $name
     * @return Brand
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Brand
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    

    

    

    

    

    

    

    

    


    /**
     * Use to serialize object for Algolia or Api
     *
     * @param NormalizerInterface $normalizer
     * @param null $format
     * @param array $context
     * @return array|bool|float|int|string
     */
    public function normalize(NormalizerInterface $normalizer, $format = null, array $context = array())
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'locale' => $this->getLocale(),
            
            
            'slug' => $this->getSlug(),
            
            'translatable_id' => $this->getTranslatable()->getId(),
        ];
    }
}
