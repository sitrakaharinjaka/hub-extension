<?php

namespace App\Utils\Managers;

use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class BulkOrderManager
 * `
 * Object manager of bulk order
 *
 * @package Disko\PageBundle\Managers
 */
class BulkOrderManager extends BaseManager
{
    /**
     * BulkOrderManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
    }
}
