<?php

/**
 * Repository
 *
 * @author Pierre Baumes <pierre.b@disko.fr>
 */

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class StockTranslationRepository
 *
 * @package App\Repository
 */
class StockTranslationRepository extends BaseRepository
{
}
