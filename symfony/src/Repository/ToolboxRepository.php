<?php

namespace App\Repository;

use App\Entity\Toolbox;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;

/**
 * Class ToolboxRepository
 */
class ToolboxRepository extends BaseRepository
{



    /**
     * ================= FRONT ===================
     */


    /**
     * ================= BACK ===================
     */


    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et')
            ->leftJoin('e.translations', 'et')
            ->orderBy('et.title', 'asc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'et';
                if ($key == 'id') {
                    $alias = 'e';
                }
                $qb->andWhere($alias.'.'.$key.' LIKE :'.$key);
                $qb->setParameter($key, '%'.$filter.'%');
            }
        }

        $query = $qb->getQuery();

        return $query;
    }

    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e', 'et', 'c')
            ->leftJoin('e.translations', 'et')
            ->leftJoin('et.cover', 'c')

            ->where('e.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        $query->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $query->getOneOrNullResult();
    }


    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findAllTranslations($toolbox)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->from('App\Entity\ToolboxTranslation', 't')
            ->select('t')
            ->andWhere('t.toolbox = :toolbox')
            ->setParameter('toolbox', $toolbox);

        $query = $qb->getQuery();
        return $query->getResult();
    }

    /**
     * Find one by category
     */
    public function findOneByCategory(string $category)
    {
        $query = $this->createQueryBuilder('t')
            ->select('t', 'tt')
            ->leftJoin('t.translations', 'tt')
            ->andWhere('tt.category LIKE :category')
            ->setParameter('category', $category)
            ->getQuery()
            ;

        try {
            $result = $query->getOneOrNullResult();
        } catch (NonUniqueResultException $exception) {
            $result = $query->getResult()[0];
        }

        return $result;
    }
}
