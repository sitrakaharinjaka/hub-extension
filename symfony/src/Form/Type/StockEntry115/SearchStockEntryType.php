<?php


namespace App\Form\Type\StockEntry115;


use App\Form\Model\SearchStockEntry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use App\Entity\Stock;
use App\Entity\StockEntry115;

/**
 * Class SearchStockEntryType
 * @package App\Form\Type\StockEntry
 */
class SearchStockEntryType extends AbstractType
{
    /**
     * @param FormBuilderInterface<mixed> $builder
     * @param array<string, mixed>        $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', IntegerType::class, [
                'required' => false
            ])
            ->add('stock', EntityType::class, [
                'class' => Stock::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale')
                    ->andWhere('u.cave = :cave')
                    ->setParameter('locale', 'fr')
                    ->setParameter('cave', '115')
                    ->orderBy('t.name', 'ASC');
                },
                'choice_label' => 'name',
                'multiple' =>false,
                'required'  => false
            ])
            /*->add('stock', ChoiceType::class,
              [
                'required' => false,
                'label' => 'Stock',
                'choices' => StockConstant::_STOCK_,
                'multiple' => false,
                'expanded' => false,
                'placeholder' => ':. Choix stock .:',
              ])*/
            ->add('requester', TextType::class, [
                'required' => false
            ])
            ->add('code', TextType::class, [
                'required' => false
            ])
            ->add('status', ChoiceType::class,
                array(
                'choices'  => array(
                    'Complète' => 'ok',
                    'Incomplète' => 'partial',
                    'Annulée' => 'empty',
                    'Validée' => StockEntry115::STATE_ITEM_VALIDATE,
                    'Terminée' => StockEntry115::STATE_IN_STOCK,
                    'En attente de traitement' => StockEntry115::STATE_ITEM_CREATE,

                ),
                'required' => false)
            )
            ->add('createdAt', DateType::class, array(
                    'required' => false,
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'format' => 'dd/MM/y',
                    'empty_data' => '',
                    )
                )
            ->add('delivery', DateType::class, array(
                    'required' => false,
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'format' => 'dd/MM/y',
                    'empty_data' => '',
                    )
                )
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchStockEntry::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'search';
    }
}
