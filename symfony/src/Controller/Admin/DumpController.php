<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class DumpController
 * @Route("/admin/dumps")
 * @package App\Controller
 */
class DumpController extends BaseController
{
    /**
     * Dump's list
     * @Route("/list", name="admin_dump_list")
     * @param Request $request Request
     */
    public function index(Request $request)
    {

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des dumps');

        $path = $this->getPath();
        $files = scandir($path, 1);

        $results = [];
        foreach ($files as $file) {
            if ($file == '.' or $file == '..' or $file == '.gitkeep') {
                continue;
            }

            $result = [];
            $result['inprogress'] = count(explode('.progress', $file)) > 1;
            $result['path'] = $file;
            $result['fullpath'] = $path.$file;
            $result['size'] = $this->fileSizeConvert(filesize($path.$file));
            $results[] = $result;
        }


        // View
        return $this->render(
            '@Admin/Dump/list.html.twig',
            array(
                'files' => $results,
            )
        );
    }

    /**
     * Dump's ddl
     * @Route("/ddl/{filename}", name="admin_dump_ddl")
     * @param Request $request Request
     */
    public function ddl(Request $request, $filename)
    {
        $path = $this->getPath().$filename;
        $file = new File($path);
        return $this->file($file);
    }

    /**
     * Dump's delete
     * @Route("/delete/{filename}", name="admin_dump_delete")
     * @param Request $request Request
     */
    public function delete(Request $request, $filename)
    {
        $path = $this->getPath();
        $process = new Process(
            'rm '.$filename,
            $path
        );
        $process->run();


        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            'Le dump "'.$filename.'" a bien été supprimé'
        );

        return $this->redirectToRoute('admin_dump_list');
    }


    /**
     * Dump's doit
     * @Route("/do-it", name="admin_dump_do_it")
     * @param Request $request Request
     */
    public function doit(Request $request)
    {
        $host = $this->getParameter('database_host');
        $name = $this->getParameter('database_name');
        $user = $this->getParameter('database_user');
        $password = $this->getParameter('database_password');

        $date = new \DateTime();

        $path = $this->getPath();

        $filename = $name.'_'.$date->format('dmY-His').'.sql';

        $process = new Process(
            'touch '.$filename.'.progress && mysqldump -u '.$user.' -p'.$password.' -h '.$host.' '.$name.' > '.$filename.' && rm '.$filename.'.progress',
            $path
        );
        $process->run();

        return $this->redirectToRoute('admin_dump_list');
    }

    /**
     * Converts bytes into human readable file size.
     *
     * @param string $bytes
     * @return string human readable file size (2,87 Мб)
     * @author Mogilev Arseny
     */
    private function fileSizeConvert($bytes)
    {
        $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

        foreach ($arBytes as $arItem) {
            if ($bytes >= $arItem["VALUE"]) {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", ",", strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
        }
        return $result;
    }

    /**
     * Get path of folder
     *
     * @return mixed|string
     *
     */
    private function getPath()
    {
        $path =  $this->getParameter('kernel.root_dir');
        $path = $path.'/../tools/dumps/';
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        return $path;
    }
}
