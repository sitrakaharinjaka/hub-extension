<?php

namespace App\Command\User;

use App\Utils\Managers\IPManager;
use App\Utils\Managers\UserManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;

class ImportActiveDirectoryCommand extends Command
{
    /** @var  EntityManager */
    private $userManager;

    private const URL_BACK_DEV  = 'https://api-back-dev.moet-hennessy.net:8443/ad';
    private const URL_FRONT_DEV = 'https://api-front-dev.moet-hennessy.net:8443/ad';
    private const URL_BACK_PP   = 'https://api-back-pre.moet-hennessy.net:8443/ad';
    private const URL_FRONT_PP  = 'https://api-front-pre.moet-hennessy.net:8443/ad';
    private const URL_BACK_PRD  = 'https://api-back.moet-hennessy.net:8443/ad';
    private const URL_FRONT_PRD = 'https://api-front.moet-hennessy.net:8443/ad';

    private const KEY_DEV  = 'H01750I5nh43ei43ljv0v9GM61i5qgN8VJ88Bk9Uc1hnECO30F2PZ0787x46313Qw2XH18025O5h8R03S9EC11Wo97144vh6a68s3BNn7xD9j03272600F02o7E4664E1H1847k3H84522baUUq66LU38M6No657249e7P3k46ol6098L8F2s93yG5141avH1m4n165kU35777718b631zy421Rd30Sg3Z071aH7W40so2a8T874kiN4246m10BE';
    private const KEY_PROD = 'XXXX';

    public function __construct(UserManager $userManager)
    {
        parent::__construct();
        $this->userManager = $userManager;
    }

    protected static $defaultName = 'disko:users:import-active-directory';

    protected function configure()
    {
        $this->setDescription('Import all user from Active Directory')
            ->addOption('debug', null, InputOption::VALUE_OPTIONAL, 'Mode DEBUG (no commit, versatile) enabled', false)
            ->addOption('prod', null, InputOption::VALUE_OPTIONAL, 'Mode PROD (API URL, API Key) enabled (versus DEV)', true)
            ->addOption('export', null, InputOption::VALUE_OPTIONAL, 'Export data into CSV file', false)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Import des User depuis l'Active Directory");

        $debug = $input->getOption('debug');
        $prod = $input->getOption('prod');
        $export = $input->getOption('export');

        if ($prod) {
            // Oui oui, c'est hallucinant, mais :
            // > Il ne faut pas changer l’url, ce sont bien l’URL de Dev et l’API Key de Dev qui sont à utiliser également depuis votre plate-forme de production.
            // > Autrement dit l’API « ad » est uniquement disponible sur notre plate-forme API de Dev (https://api-front-dev.moet-hennessy.net:8443/ad), avec toutefois les données AD de production.
            // $apiurl = ImportActiveDirectoryCommand::URL_FRONT_PRD . "/accounts";
            // $apikey = ImportActiveDirectoryCommand::KEY_PROD;
            $apiurl = ImportActiveDirectoryCommand::URL_FRONT_DEV . "/accounts";
            $apikey = ImportActiveDirectoryCommand::KEY_DEV;
        } else {
            $apiurl = ImportActiveDirectoryCommand::URL_FRONT_DEV . "/accounts";
            $apikey = ImportActiveDirectoryCommand::KEY_DEV;
        }

        $users = $this->getUsers($output, $apiurl, $apikey);
        if ($users && $export) {
            $this->doCSVFile($users, 'users_all.csv');
            $users = $this->filterUsers($users, $debug);
            $this->doCSVFile($users, 'users_filtered.csv');
        } elseif ($users) {
            $users = $this->filterUsers($users, $debug);
            $progress = new ProgressBar($output, sizeof($users));
            $progress->start();
            foreach ($users as $user) {
                $this->executeUser($input, $output, $user, $debug);
                $progress->advance(1);
            }
            $progress->finish();
            $output->writeln("Fin : <info>success</info>");
        } else {
            $output->writeln("<error>Impossible de récupérer la liste des Users</error>");
        }
    }

    private function getUsers(OutputInterface $output, $apiurl, $apikey)
    {
        // Dimanche dernier
        $date = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-date("w"), date("Y")));// YYYY-MM-DD
        $type = "full"; // full / delta_daily
        $mode = "application/json"; // application/json / text/plain
        try {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $apiurl . "?type=" . $type . "&date=" . $date);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Accept: ' . $mode,
                'Api_Key: ' . $apikey,
            ]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $data = curl_exec($curl);
            $data = json_decode($data, true);
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);

            if ($code == 200 || $code == 201) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    private function filterUsers($data, $debug)
    {
        $selected = [];
        foreach ($data as $item) {
            if ($item['Email Address'] == '') {
                continue;
            }
            if ($item['Last Name'] == '') {
                continue;
            }
            if ($item['First Name'] == '') {
                continue;
            }
            if (preg_match('/[0-9]/', $item['Last Name']) > 0) {
                continue;
            }
            if (preg_match('/[0-9]/', $item['First Name']) > 0) {
                continue;
            }
            $selected[] = $item;
        }
        if ($debug) {
            return array_slice($selected, 0, 100);
        } else {
            return $selected;
        }
    }

    private function doCSVFile($data, $filename)
    {
        $fp = fopen($filename, 'w');
        // Précision de l'encodage pour Excel ...
        fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));
        fputcsv($fp, array_keys($data[0]));
        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }
    }

    private function executeUser(InputInterface $input, OutputInterface $output, $item, $debug)
    {
        $email = $item['Email Address'];
        $user = $this->userManager->findOneBy(['email' => $email]);
        if ($user) {
            $this->userManager->updateFromAD($item, $user, $debug);
        } else {
            $this->userManager->createFromAD($item, $debug);
        }
    }
}
