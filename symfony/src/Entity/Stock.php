<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockRepository")
 * @ORM\Table(name="dk_stock")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Stock
{
    use GedmoTrait;
    use AuthorableTrait;
    use ORMBehaviors\Translatable\Translatable;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Many Stock have Many Users.
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="stocks")
     */
    private $users;

    /**
     * Many Stock have Many Users.
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="adminStocks")
     */
    private $adminUsers;

    /**
     * @var Collection|Project[]
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="stock", cascade={"all"})
     */
    protected $projects;

    /**
     * @var Collection|Project[]
     * @ORM\OneToMany(targetEntity="App\Entity\StockLine", mappedBy="stock", cascade={"all"})
     */
    protected $stockLines;

    /**
     * @var User|null
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="referentStocks", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $referentUser;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\BillingCenter", inversedBy="stocks", cascade={"persist"})
     * @JoinTable(name="billing_center_stock",
     *  joinColumns={
     *      @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="billing_center_id", referencedColumnName="id")
     *  })
     * @var BillingCenter
     */
    protected $billingCenters;

    /**
     * Cave
     *
     * @ORM\Column(length=255, name="cave", nullable=true)
     */
    protected $cave;


    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->adminUsers = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->stockLines = new ArrayCollection();
        $this->billingCenters = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function addUser(User $user)
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }
    }

    public function removeUser(User $user)
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }
    }

    public function getUsers()
    {
        return $this->users;
    }

    public function getName()
    {
        return $this->getTranslations()->first()->getName();
    }


    /**
     * {@inheritdoc}
     */
    public function addProject(Project $project)
    {
        if ($this->hasProject($project)) {
            return;
        }

        $this->projects->add($project);
        $project->setUser($this);
    }

    /**
     * {@inheritdoc}
     */
    public function removeProject(Project $project)
    {
        if ($this->hasProject($project)) {
            $this->projects->removeElement($project);
            $project->setUser(null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * {@inheritdoc}
     */
    public function clearProjects()
    {
        $this->projects->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function hasProject(Project $project)
    {
        return $this->projects->contains($project);
    }


    /**
     * {@inheritdoc}
     */
    public function addStockLine(StockLine $stockLine)
    {
        if ($this->hasStockLine($stockLine)) {
            return;
        }

        $this->stockLines->add($stockLine);
    }

    /**
     * {@inheritdoc}
     */
    public function removeStockLine(StockLine $stockLine)
    {
        if ($this->hasStockLine($stockLine)) {
            $this->stockLines->removeElement($stockLine);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getStockLines()
    {
        return $this->stockLines;
    }

    /**
     * {@inheritdoc}
     */
    public function clearStockLines()
    {
        $this->stockLines->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function hasStockLine(StockLine $stockLine)
    {
        return $this->stockLines->contains($stockLine);
    }

    /**
     * @return ArrayCollection
     */
    public function getBillingCenters()
    {
        return $this->billingCenters;
    }

    /**
     * @param mixed $billingCenters
     */
    public function setBillingCenters($billingCenters)
    {
        $this->billingCenters = $billingCenters;
    }

    /**
     * @param BillingCenter $billingCenter
     */
    public function addBillingCenter(BillingCenter $billingCenter)
    {
        if ($this->billingCenters->contains($billingCenter)) {
            return;
        }
        $this->billingCenters->add($billingCenter);
        $billingCenter->addStock($this);
    }

    /**
     * @param BillingCenter $billingCenter
     */
    public function removeBillingCenter(BillingCenter $billingCenter)
    {
        if (!$this->billingCenters->contains($billingCenter)) {
            return;
        }
        $this->billingCenters->removeElement($billingCenter);
    }


    public function addAdminUser(User $user)
    {
        if (!$this->adminUsers->contains($user)) {
            $this->adminUsers->add($user);
        }
    }

    public function removeAdminUser(User $user)
    {
        if ($this->adminUsers->contains($user)) {
            $this->adminUsers->removeElement($user);
        }
    }

    public function getAdminUsers()
    {
        return $this->adminUsers;
    }

    /**
     * @return User|null
     */
    public function getReferentUser(): ?User
    {
        return $this->referentUser;
    }

    /**
     * @param User|null $referentUser
     */
    public function setReferentUser(?User $referentUser): void
    {
        $this->referentUser = $referentUser;
    }

    /**
     * @return mixed
     */
    public function getCave()
    {
        return $this->cave;
    }

    /**
     * @param mixed $cave
     */
    public function setCave($cave)
    {
        $this->cave = $cave;
    }
}
