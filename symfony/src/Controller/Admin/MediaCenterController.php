<?php

namespace App\Controller\Admin;

use App\Entity\Media;
use App\Form\Model\SearchMedia;
use App\Form\Type\Media\MediaType;
use App\Form\Type\Media\SearchMediaType;
use App\Utils\Managers\MediaManager;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class MediaCenterController
 * @Route("/admin/media-center")
 * @package App\Controller
 */
class MediaCenterController extends BaseController
{

    /**
     * @Route("/list", name="admin_media_center_list")
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request, MediaManager $mediaManager)
    {

        // init search
        $data = $this->initSearch($request);


        // Init form
        $form = $this->createForm(SearchMediaType::class, $data);

        // Generate query
        $query = $mediaManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            12
        );

        return new Response($this->renderView('@Admin/Media/index.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView()
        )));
    }

    /**
     * @Route("/ajax/index/list", name="admin_media_center_index_ajax_list")
     * @param Request $request
     *
     * @return Response
     */
    public function ajaxIndexList(Request $request, MediaManager $mediaManager)
    {

        // init search
        $data = $this->initSearch($request);


        // Init form
        $form = $this->createForm(SearchMediaType::class, $data);

        // Generate query
        $query = $mediaManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            12
        );

        return new Response($this->renderView('@Admin/Media/partials/index_list.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView()
        )));
    }


    /**
     * @Route("/ajax/popin/list", name="admin_media_center_ajax_list")
     * @param Request $request
     *
     * @return Response
     */
    public function ajaxPopinList(Request $request, MediaManager $mediaManager)
    {

        // init search
        $data = $this->initSearch($request);


        // Init form
        $form = $this->createForm(SearchMediaType::class, $data);

        // Generate query
        $query = $mediaManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            12
        );

        if ($request->isMethod('PUT') or $request->query->get('search', false)) {
            return new Response($this->renderView('@Admin/Media/partials/content.html.twig', array(
                'pagination' => $pagination
            )));
        }

        return new Response($this->renderView('@Admin/Media/partials/list.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView()
        )));
    }


    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->get('search', array());
        $accepted = $request->get('accepted', (isset($filters['accepted'])) ? $filters['accepted'] : null);

        // Init form
        $data = new SearchMedia();
        $data->setSearch((isset($filters['search']))   ? $filters['search'] : '');
        $data->setAccepted((!empty($accepted))   ? $accepted : null);

        return $data;
    }
}
