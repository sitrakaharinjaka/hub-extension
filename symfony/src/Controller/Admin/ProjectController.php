<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\Project;
use App\Entity\ProjectTranslation;
use App\Form\Model\SearchProject;
use App\Form\Type\Project\ProjectTranslationType;
use App\Form\Type\Project\SearchProjectType;
use App\Form\Type\Project\ProjectType;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\LocaleManager;
use App\Utils\Managers\StockManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProjectController
 * @Route("/admin/projects")
 * @package Disko\AdminBundle\Controller
 */
class ProjectController extends BaseController
{
    protected $stockManager;

    /**
     * Project's list
     * @Route("/list/{askLocale}", name="admin_project_list", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listAction(Request $request, ProjectManager $projectManager, StockManager $stockManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        $this->stockManager = $stockManager;

        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchProjectType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des commandes');

        // Generate query
        $filters = $data->getSearchData();
        $query = $projectManager->queryForSearch($filters);

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/Project/index.html.twig', array(
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }


    /**
     * Project's list
     * @Route("/list-current/{askLocale}", name="admin_project_list_current", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listBisAction(Request $request, ProjectManager $projectManager, StockManager $stockManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        $this->stockManager = $stockManager;

        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchProjectType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des commandes');

        // Generate query
        $filters = $data->getSearchData();

        $currentStock = $this->get('session')->get('current_admin_stock');
        if ($this->getUser()->hasAdminStock($currentStock) or $this->isGranted('ROLE_GENERAL_ADMIN')) {
            $filters['stock'] = $currentStock->getId();
        } else {
            throw $this->createAccessDeniedException("");
        }

        $query = $projectManager->queryForSearch($filters);

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/Project/index.html.twig', array(
            'bismode' => true,
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }



    /**
     * Project's list
     * @Route("/list-veolog/{askLocale}", name="admin_project_list_veolog", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listVeologAction(Request $request, ProjectManager $projectManager, StockManager $stockManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        $this->stockManager = $stockManager;

        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchProjectType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des commandes');

        // Generate query
        $filters = $data->getSearchData();
        $filters['cave'] = Project::CAVE_PANTIN;
        $query = $projectManager->queryForSearch($filters);

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/Project/index.html.twig', array(
            'veologmode' => true,
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchProject();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setName((isset($filters['name']))   ? $filters['name'] : '');
        $data->setState((isset($filters['state']))   ? $filters['state'] : 'all');
        $data->setTrackers((isset($filters['trackingAvailable']))   ? $filters['trackingAvailable'] : '');
        $data->setType((isset($filters['type']))   ? $filters['type'] : 'all');
        $data->setStock((isset($filters['stock']))   ? $filters['stock'] : '');

        $data->setCave((isset($filters['cave']))   ? $filters['cave'] : 'all');

        $data->setDateAddVlg((!empty($filters['dateAddVlg']))   ? date_create_from_format("d/m/Y", $filters['dateAddVlg']) : null);
        $data->setDateStart((!empty($filters['dateStart']))   ? date_create_from_format("d/m/Y", $filters['dateStart']) : null);
        $data->setDateEnd((!empty($filters['dateEnd']))   ? date_create_from_format("d/m/Y", $filters['dateEnd']) : null);
        $data->setCreated((!empty($filters['created']))   ? date_create_from_format("d/m/Y", $filters['created']) : null);
        $data->setUserFullName((!empty($filters['userFullName']))   ? $filters['userFullName'] : '');
        return $data;
    }

    /**
     * Delete one project
     * @Route("/delete/{id}", name="admin_project_delete")
     * @param Request $request Request
     * @param integer $id      Id of project
     */
    public function deleteAction(Request $request, ProjectManager $projectManager, $id)
    {
        if ($this->isGranted('ROLE_VEOLOG_ADMIN')) {
            throw $this->createAccessDeniedException("Veolog not access");
        }

        // Find it
        /** @var Project $project */
        $project = $projectManager->findOneToEdit($id);
        if (!$project) {
            throw $this->createNotFoundException('Project not found');
        }

        $projectManager->remove($project);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été supprimé"
        );

        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * @Route("/show/{id}", name="admin_project_show")
     */
    public function showAction(ProjectManager $projectManager, $id)
    {
        // Find it
        /** @var Project $project */
        $project = $projectManager->findOneToEdit($id);
        if (!$project) {
            throw $this->createNotFoundException('Product not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des commandes", $this->get("router")->generate("admin_project_list"));
        $breadcrumbs->addItem("Fiche commande");

        // View
        return $this->render(
            '@Admin/Project/show.html.twig',
            array(
                'project' => $project
            )
        );
    }


    /**
     * @Route("/show-current/{id}", name="admin_project_show_current")
     */
    public function showBisAction(ProjectManager $projectManager, $id)
    {
        // Find it
        /** @var Project $project */
        $project = $projectManager->findOneToEdit($id);
        if (!$project) {
            throw $this->createNotFoundException('Product not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des commandes", $this->get("router")->generate("admin_project_list_current"));
        $breadcrumbs->addItem("Fiche commande");

        // View
        return $this->render(
            '@Admin/Project/show.html.twig',
            array(
                'bismode' => true,
                'project' => $project
            )
        );
    }


    /**
     * @Route("/show-veolog/{id}", name="admin_project_show_veolog")
     */
    public function showVeologAction(ProjectManager $projectManager, $id)
    {
        // Find it
        /** @var Project $project */
        $project = $projectManager->findOneToEdit($id);
        if (!$project) {
            throw $this->createNotFoundException('Product not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des commandes", $this->get("router")->generate("admin_project_list_veolog"));
        $breadcrumbs->addItem("Fiche commande");

        // View
        return $this->render(
            '@Admin/Project/show.html.twig',
            array(
                'veologmode' => true,
                'project' => $project
            )
        );
    }
}
