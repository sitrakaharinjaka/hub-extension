<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class StockEntryModelValidator extends ConstraintValidator
{
    private $translator;

    /**
     * StockClassValidator constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator    = $translator;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var StockEntryModel $stockEntryModel */
        $stockEntryModel = $value;

        $stockEntryItems    = $stockEntryModel->getStockEntryItems();
        $stockEntryItemsIds = array();

        foreach ($stockEntryItems as $stockEntryItem) {
            $stockEntryItemsIds []= $stockEntryItem->getProductId();
        }

        if (self::has_dupes($stockEntryItemsIds)) {
            $this
                ->context
                ->buildViolation($this->translator->trans('Vous avez sélectionné au moins deux produits identiques'))
                ->atPath('stockEntryItems')
                ->addViolation();
        }

        if (!$stockEntryModel->getStockEntryItems() || sizeof($stockEntryModel->getStockEntryItems()) ==0) {
            $this
                ->context
                ->buildViolation($this->translator->trans('Vous devez saisir au moins 1 article'))
                ->atPath('stockEntryItems')
                ->addViolation();
        }
    }

    public function has_dupes($array)
    {
        return count($array) !== count(array_unique($array));
    }
}
