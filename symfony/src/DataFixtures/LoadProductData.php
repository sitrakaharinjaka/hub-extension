<?php

/**
 * Fixture
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\DataFixtures;

use App\Entity\Locale;
use App\Entity\ProductTranslation;
use App\Utils\Managers\LocaleManager;
use App\Utils\Managers\ProductManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\Product;

/**
 * Class LoadProductData
 *
 * @package Disko\UserBundle\DataFixtures\ORM
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class LoadProductData extends Fixture implements DependentFixtureInterface
{
    private $productManager;
    private $localeManager;

    public function __construct(ProductManager $productManager, LocaleManager $localeManager)
    {
        $this->productManager = $productManager;
        $this->localeManager = $localeManager;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
//        $products = array(
//            ['name' => 'Bouteille'],
//            ['name' => 'Table'],
//            ['name' => 'Assiette'],
//            ['name' => 'Fauteuil'],
//            ['name' => 'Serviette'],
//            ['name' => 'Nappe'],
//            ['name' => 'Lumière'],
//            ['name' => 'Rideaux'],
//            ['name' => 'Verre'],
//        );
//
//        $locales = $this->localeManager->findAll();
//
//        foreach ($products as $product) {
//            $obj = new Product();
//            foreach ($locales as $locale) {
//                $trans = new ProductTranslation();
//                $trans->setLocale($locale->getSlug());
//                $trans->setName($product['name']);
//                $obj->addTranslation($trans);
//            }
//            $this->productManager->save($obj);
//            $this->addReference('admin-product-'.$obj->getId(), $obj);
//        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

    public function getDependencies()
    {
        return [
          LoadLocaleData::class
      ];
    }
}
