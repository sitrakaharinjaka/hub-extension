<?php

namespace App\Utils\Managers;

use App\Entity\Product;
use App\Entity\Project;
use App\Event\ProjectItemEvent;
use App\Repository\ProjectItemRepository;
use App\Utils\Managers\BaseManager;

use App\Utils\Managers\LocaleManager;
use App\Entity\ProjectItem;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ProjectItemManager
 * `
 * Object manager of projectItem
 *
 * @package Disko\PageBundle\Managers
 */
class ProjectItemManager extends BaseManager
{

    /**
     * Repository
     *
     * @var ProjectItemRepository
     */
    protected $projectItemRepository;

    /**
     * ProjectItemManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager, EventDispatcherInterface $dispatcher)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('projectItemRepository', ProjectItem::class);
    }

    /**
     * Save a projectItem
     *
     * @param ProjectItem $projectItem
     */
    public function save(ProjectItem $projectItem)
    {
        // $projectItem->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($projectItem);
        $this->getEntityManager()->flush();

        return $projectItem;
    }

    /**
     * Remove one projectItem
     *
     * @param ProjectItem $projectItem
     */
    public function remove(ProjectItem $projectItem)
    {
        $this->getEntityManager()->remove($projectItem);
        $this->getEntityManager()->flush();
    }


    /**
     * Create base
     *
     * @param Project $project
     * @param Product $product
     * @return ProjectItem
     */
    public function create($project, Product $product)
    {
        $projectItem = new ProjectItem();
        $projectItem->setProject($project);
        $projectItem->setProduct($product);

        return $projectItem;
    }

    /**
     * @param Project $project
     * @param Product $product
     * @return int
     */
    public function countInProject($project, Product $product)
    {
        $result = 0;

        if (!$project) {
            return $result;
        }

        $items = $project->getItems();
        foreach ($items as $item) {
            if ($item->getProduct()->getId() == $product->getId()) {
                $result = $result + $item->getQuantity();
            }
        }

        return $result;
    }


    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->projectItemRepository->findOneBy($filters);
    }
}
