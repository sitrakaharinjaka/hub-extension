<?php

/**
 * Form type
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Type\Stock;

use App\Entity\Brand;
use App\Entity\Category;
use App\Form\Model\StockEntry115ItemModel;
use App\Form\Model\StockEntryItemModel;
use App\Entity\Product;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\Fo;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

/**
 *  Form Type
 */
class StockEntry115ItemType extends AbstractType
{
    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('productId', HiddenType::class, array(
            'required' => true,
            'attr'     => array('class' => 'product')
        ));
        $builder->add('productCategory', EntityType::class, array(
            'class'         => Category::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('c')
                    ->leftJoin('c.translations', 't', 'WITH', 't.locale = :locale')
                    ->setParameter('locale', 'fr')
                    ->orderBy('t.name', 'ASC');
            },
            'choice_label' => 'name',
            'required'     => true,
            'attr'         => array(
                'class'            => 'category js-select',
                'data-placeholder' => 'Catégorie*'
            ),
            'multiple'     => false,
        ));
        $builder->add('productBrand', EntityType::class, array(
            'class'         => Brand::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('b')
                    ->leftJoin('b.translations', 't', 'WITH', 't.locale = :locale')
                    ->setParameter('locale', 'fr')
                    ->orderBy('t.name', 'ASC');
            },
            'choice_label' => 'name',
            'required'     => true,
            'attr'         => array(
                'class'            => 'input-entry-brand js-select',
                'data-placeholder' => 'Marque*'
            ),
            'label_attr'   => array(
                'class' => 'form__label'
            ),
            'multiple'     => false,
        ));
        $builder->add('productName', TextType::class, array(
            'required'     => true,
            'attr'         => array(
                'class'            => 'input-entry-name js-select-image form__field',
                'data-placeholder' => 'Nom du produit*'
            ),
            'label_attr'   => array(
                'class' => 'form__label'
            ),
        ));
        $builder->add('productCode', TextType::class, array(
            'required'     => true,
            'attr'         => array(
                'class'            => 'input-entry-code form__field',
                'data-placeholder' => 'Nom du produit*'
            ),
            'label_attr'   => array(
                'class' => 'form__label'
            ),
        ));
        $builder->add('productEan', TextType::class, array(
            'required'     => true,
            'attr'         => array(
                'class'            => 'input-entry-ean form__field',
                'data-placeholder' => 'Nom du produit*'
            )
        ));
        $builder->add('productState', ChoiceType::class, array(
            'choices'  => array(
                'Neuf'     => 'new',
                'Non-neuf' => 'not-new'
            ),
            'required' => true,
            'attr'     => array('class' => 'input-entry-state js-select'),
            #'disabled' => 'true'
        ));
        $builder->add('quantity', IntegerType::class, array(
            'required' => true,
            'attr'     => array(
                'class' => 'form__field',
            )
        ));
    }

    /**
     * Configure Options
     *
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => StockEntry115ItemModel::class
        ));
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'stock';
    }
}
