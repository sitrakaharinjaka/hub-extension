<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin\MyBoxes;

use App\Form\Type\MyBoxesBox\MyBoxesBoxType;
use App\Entity\MyBoxesBox;
use App\Entity\MyBoxesBoxUser;
use App\Entity\User;
use App\Form\Model\SearchMyBoxesBox;
use App\Form\Type\MyBoxesBox\SearchMyBoxesBoxType;
use App\Utils\Managers\MyBoxesBoxManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use App\Utils\Managers\MyBoxesBoxLogManager;
use App\Utils\Managers\UserManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;

/**
 * Class MyBoxesBoxController
 * @Route("/admin/myBoxesBoxs")
 * @package Disko\AdminBundle\Controller
 */
class MyBoxesBoxController extends BaseController
{
    /**
     * MyBoxesBox's list
     * @Route("/list/{askLocale}", name="admin_myBoxesBox_list", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listAction(Request $request, MyBoxesBoxManager $myBoxesBoxManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchMyBoxesBoxType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des boxes');

        // Generate query
        $query = $myBoxesBoxManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/MyBoxesBox/index.html.twig', array(
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchMyBoxesBox();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setBarcode((isset($filters['barcode']))   ? $filters['barcode'] : '');
        $data->setName((isset($filters['name']))   ? $filters['name'] : '');
        $data->setSeal((isset($filters['seal']))   ? $filters['seal'] : '');
        $data->setEmail((isset($filters['email']))   ? $filters['email'] : '');
        $data->setState((isset($filters['state']))   ? $filters['state'] : '');

        return $data;
    }


    /**
     * Delete one myBoxesBox
     * @Route("/delete/{id}", name="admin_myBoxesBox_delete")
     * @param Request $request Request
     * @param integer $id      Id of myBoxesBox
     */
    public function deleteAction(Request $request, MyBoxesBoxManager $myBoxesBoxManager, $id, MyBoxesBoxLogManager $myBoxesBoxLogManager)
    {
        // Find it
        /** @var MyBoxesBox $myBoxesBox */
        $myBoxesBox = $myBoxesBoxManager->findOneToEdit($id);
        if (!$myBoxesBox) {
            throw $this->createNotFoundException('MyBoxesBox not found');
        }

        $myBoxesBoxManager->remove($myBoxesBox);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été supprimé"
        );

        $myBoxesBoxLogManager->log($myBoxesBox, 'deletion');
        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * Export csv
     * @Route("/export/{locale}", name="admin_myBoxesBox_export")
     * @param Request $request
     */
    public function exportAction(Request $request, MyBoxesBoxManager $myBoxesBoxManager, $locale)
    {
        $data = $this->initSearch($request);
        $query = $myBoxesBoxManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Nom',
            'State',
            'Date de création',
        ], ';');

        $locale = $request->get('locale', $request->getDefaultLocale());
        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->getName(),
                $result->getState(),
                ($result->getCreated()) ? $result->getCreated()->format('d/m/Y') : '',
                
            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_'.$locale.'_boxes_export.csv"'
        ));
    }

    /**
     * Update a myBoxesBox
     * @Route("/edit/{id}", name="admin_myBoxesBox_edit")
     * @param Request $request Request
     * @param integer $id      Id of box
     * @param MyBoxesBoxLogManager $myBoxesBoxLogManager
     *
     * @return Response
     */
    public function editAction(Request $request, MyBoxesBoxManager $myBoxesBoxManager, MyBoxesBoxLogManager $myBoxesBoxLogManager, $id)
    {
        // Find it
        /** @var MyBoxesBox $myBoxesBox */
        $myBoxesBox = $myBoxesBoxManager->findOneToEdit($id);
        if (!$myBoxesBox) {
            throw $this->createNotFoundException('Boxes not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des Boxes", $this->get("router")->generate("admin_myBoxesBox_list"));
        $breadcrumbs->addItem("Modification");

        $initialData = [
            'name' => $myBoxesBox->getName(),
            'content' => $myBoxesBox->getContent(),
            'barcode' => $myBoxesBox->getBarcode(),
            'seal' => $myBoxesBox->getSeal(),
            'users' => []
        ];

        if(is_object($myBoxesBox->getBoxUsers())){
            foreach($myBoxesBox->getBoxUsers() as $boxUserInitial){
                $initialData['users'][] = $boxUserInitial->getUser()->getEmail();
            }
        }

        $newData = $initialData;

        $form = $this->createForm(MyBoxesBoxType::class, $myBoxesBox, []);

        $entityManager  = $this->getDoctrine()->getManager();

        // Update method
        if ('POST' === $request->getMethod()) {

            $requestData = $request->request->get('my_boxes_box');

            $submittedBoxUsers = $requestData['boxUsers'];

            if(count($submittedBoxUsers) > 0){
                $boxUsersFormChoices = [];
                
                $myBoxesBox->clearBoxUsers();

                foreach($submittedBoxUsers as $submittedBoxUser){
                    $user = $entityManager->getRepository(User::class)->find((int) $submittedBoxUser);
    
                    $boxUser = $entityManager
                        ->getRepository(MyBoxesBoxUser::class)
                        ->findOneBy(['user' => $user, 'box' => $myBoxesBox]);
        
                    if(!is_object($boxUser)){
                        $boxUser = new MyBoxesBoxUser();
                        $boxUser->setUser($user);
                    }
    
                    $boxUser->setBox($myBoxesBox);
                    $entityManager->persist($boxUser);
                    $entityManager->flush();

                    $myBoxesBox->addBoxUser($boxUser);

                    $boxUsersFormChoices[] = $boxUser;
                }

                //$myBoxesBoxManager->save($myBoxesBox);

                $boxUsersFormOptions = $form->get('boxUsers')->getConfig()->getOptions();
                $boxUsersFormOptions['choices'] = $boxUsersFormChoices;
                $boxUsersFormOptions['choice_loader'] = new CallbackChoiceLoader(function() use ($boxUsersFormChoices) {
                    return $boxUsersFormChoices;
                });

                $form->add('boxUsers', EntityType::class, $boxUsersFormOptions);
            }

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {

                $newData = [
                    'name' => $myBoxesBox->getName(),
                    'content' => $myBoxesBox->getContent(),
                    'barcode' => $myBoxesBox->getBarcode(),
                    'seal' => $myBoxesBox->getSeal(),
                    'users' => []
                ];

                if(is_object($myBoxesBox->getBoxUsers())){
                    foreach($myBoxesBox->getBoxUsers() as $boxUserNew){
                        $newData['users'][] = $boxUserNew->getUser()->getEmail();
                    }
                }

                $myBoxesBoxManager->save($myBoxesBox);

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'notice',
                    "Mise à jour effectuée"
                );

                //Log
                $changes = [];

                foreach($initialData as $field => $data){
                    if(is_array($data)){
                        $diff = array_diff($data, $newData[$field]);

                        if(is_array($diff) && count($diff) > 0){
                            $changes[$field] = [
                                0 => implode(', ', $data),
                                1 => implode(', ', $newData[$field])
                            ];
                        }else{
                            $diff2 = array_diff($newData[$field], $data);

                            if (is_array($diff2) && count($diff2) > 0) {
                                $changes[$field] = [
                                    0 => implode(', ', $data),
                                    1 => implode(', ', $newData[$field])
                                ];
                            }
                        }
                    }else{
                        if ($data != $newData[$field]) {
                            $changes[$field] = [
                                0 => $data,
                                1 => $newData[$field]
                            ];
                        }
                    }
                }

                if (count($changes) > 0) {
                    $myBoxesBoxLogManager->log($myBoxesBox, 'modification', $changes);
                }
                return $this->redirect($request->headers->get('referer'));
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/MyBoxesBox/edit.html.twig',
            array(
                'form'           => $form->createView(),
                'box'           => $myBoxesBox,
            )
        );
    }

    /**
     * StockLine add
     * @Route("/myBoxesBoxs/view/{id}", name="admin_myboxlog_view")
     * @param Request $request Request
     * @param MyBoxesBoxLogManager $myBoxesBoxLogManager
     * @param MyBoxesBoxManager $box
     * @param $id
     */
    public function editHistory(Request $request, MyBoxesBoxLogManager $myBoxesBoxLogManager, MyBoxesBoxManager $box, $id)
    {
        $myBoxesBoxLog = $myBoxesBoxLogManager->findBy(['box' => $id], ['created' => 'desc']);

        $box = $box->findOneBy(['id' => $id]);

        // Render view
        return $this->render('@Admin/MyBoxesBox/history-view.html.twig', array(
            'myBoxesLog' => $myBoxesBoxLog,
            'box' => $box,
        ));
    }

    /**
     * Get user list
     * @Route("/get-user-list-box", name="admin_box_get_user_list_ajax")
     * @param Request $request
     * @param UserManager $userManager
     *
     * @return JsonResponse
     */
    public function getUserListAjax(Request $request, UserManager $userManager)
    {
        $term = $request->query->get('term', '');

        $response = [
            'results' => []
        ];

        if (strlen($term) >= 3) {
            $userList = $userManager->findAllExceptAdmins($term);

            foreach ($userList as $user) {
                $response['results'][] = [
                    'id' => $user->getId(),
                    'text' => $user->getFullName()
                ];
            }
        }

        return new JsonResponse($response);
    }
}
