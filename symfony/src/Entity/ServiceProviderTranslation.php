<?php

namespace App\Entity;

use App\Entity\Traits\AuthorableTrait;
use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\SeoTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;


// Use For Algolia Or Api
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ServiceProviderRepository")
 * @ORM\Table(name="dk_serviceprovider_translation")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ServiceProviderTranslation implements NormalizableInterface
{
    use ORMBehaviors\Translatable\Translation;
    use GedmoTrait;

    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Name
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $name;

    /**
     * LastNameContact
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $lastNameContact;

    /**
     * FirstNameContact
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $firstNameContact;

    /**
     * Phone
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $phone;

    /**
     * Email
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $email;

    /**
     * Street
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $street;

    /**
     * Zipcode
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $zipCode;

    /**
     * City
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $city;

    /**
     * Slug
     *
     * @Gedmo\Slug(fields={"name"}, unique=false)
     * @ORM\Column(length=255, unique=false)
     */
    protected $slug;

    /**
     * Set id
     *
     * @param integer $id
     * @return ServiceProvider
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return ServiceProvider
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $nameCompany
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getLastNameContact()
    {
        return $this->lastNameContact;
    }

    /**
     * @param mixed $lastNameContact
     */
    public function setLastNameContact($lastNameContact): void
    {
        $this->lastNameContact = $lastNameContact;
    }

    /**
     * @return mixed
     */
    public function getFirstNameContact()
    {
        return $this->firstNameContact;
    }

    /**
     * @param mixed $firstNameContact
     */
    public function setFirstNameContact($firstNameContact): void
    {
        $this->firstNameContact = $firstNameContact;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street): void
    {
        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param mixed $zipCode
     */
    public function setZipCode($zipCode): void
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * Use to serialize object for Algolia or Api
     *
     * @param NormalizerInterface $normalizer
     * @param null $format
     * @param array $context
     * @return array|bool|float|int|string
     */
    public function normalize(NormalizerInterface $normalizer, $format = null, array $context = array())
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'locale' => $this->getLocale(),
            'slug' => $this->getSlug(),
            'translatable_id' => $this->getTranslatable()->getId(),
        ];
    }
}
