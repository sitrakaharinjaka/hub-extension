<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Form\Type\User\EmailTestSendType;
use App\Controller\BaseController;
use App\Utils\Managers\UserManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\Model\EmailTestSend;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class DetectorController
 * @Route("/admin/detector")
 * @package App\Controller
 */
class DetectorController extends BaseController
{
    /**
     * To do
     * @Route("/todo", name="admin_detect_todo")
     * @param Request $request Request
     */
    public function info(Request $request, UserManager $userManager, UserPasswordEncoderInterface $passwordEncoder)
    {

        // User Admin / Admin
        $user = $userManager->findOneBy(['email' => "admin@gmail.com"]);
        $userDetect = ($passwordEncoder->isPasswordValid($user, 'admin')) ? true : false;

        // Recaptcha
        $recaptcha = !($this->getParameter('recaptcha_enable') == true && $this->getParameter('recaptcha_site_key', false) &&  $this->getParameter('recaptcha_secret', false));

        // Recaptcha
        $ga = !($this->getParameter('gtm_enable') == 'true' && $this->getParameter('gtm_id', false));

        // favicon
        $path =  $this->getParameter('kernel.root_dir');

        $fav = false;
        if (file_exists($path.'/../public/favicon.ico') && !file_exists($path.'/../public/favicon.png')) {
            $fav = file_get_contents($path.'/../public/favicon.ico') == file_get_contents($path.'/../public/favicon-sf.ico');
        }



        // View
        return $this->render(
            '@Admin/Detector/todo.html.twig',
            [
                'userDetect' => $userDetect,
                'user' => $user,
                'ga' => $ga,
                'recaptcha' => $recaptcha,
                'fav' => $fav,
            ]
        );
    }
}
