<?php

namespace App\Listener;

use App\Entity\IP;
use App\Entity\Project;
use App\Utils\Managers\IPManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\UserManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Listener to check project
 */
class ProjectListener implements EventSubscriberInterface
{

    /**
     * @var ProjectManager
     */
    private $projectManager;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;


    /**
     * Constructor
     */
    public function __construct(
        UserManager $userManager,
        ProjectManager $projectManager,
        RouterInterface $router,
        SessionInterface $session,
        TokenStorageInterface $token,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->userManager = $userManager;
        $this->projectManager = $projectManager;
        $this->router = $router;
        $this->session = $session;
        $this->token = $token;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $user = false;
        $token = $this->token->getToken();
        if ($token) {
            $user = $token->getUser();
        }

        if ($event->isMasterRequest() && is_object($user)) {
            if (
                (preg_match('/\/saml/', $event->getRequest()->getRequestUri()) == 0) and
                (preg_match('/\/admin/', $event->getRequest()->getRequestUri()) == 0) and
                (preg_match('/\/projets\/creer/', $event->getRequest()->getRequestUri()) == 0)
                 and (preg_match('/\//', $event->getRequest()->getRequestUri()) == 0)
            ) {
                $project = $this->session->get('current_project', null);
                $stock = $this->session->get('current_stock', null);

                if ($project) {
                    $project = $this->projectManager->findOneBy(['id' => $project->getId()]);
                    if (!in_array($project->getState(), Project::STATE_EDITABLE)) {
                        $project = null;
                        $this->session->set('current_project', null);
                    }
                }

                if (!$project or $project->getStock()->getId() != $stock->getId()) {
                    $project = $this->projectManager->findOneByUserStockLatest($user, $stock);

                    if (!$project) {
                        $response = new RedirectResponse($this->router->generate('homepage'));
                        $event->setResponse($response);
                    } else {
                        $this->session->set('current_project', $project);
                    }
                }
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            //'kernel.request' => 'onKernelRequest',
            KernelEvents::REQUEST => array(
                array('onKernelRequest', 1),
            ),
        );
    }
}
