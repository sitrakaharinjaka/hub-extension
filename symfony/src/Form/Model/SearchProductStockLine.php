<?php

/**
 * Model for search
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Model;

/**
 * Class SearchProductStockLine
 *
 * @package App\Form\Model
 */
class SearchProductStockLine
{
    /**
     * Id
     *
     * @var string
     */
    protected $id;

    /**
     * Name
     *
     * @var string
     */
    protected $name;

    /**
     * Sku
     *
     * @var string
     */
    protected $sku;

    /**
     * EAN (Part or Box)
     *
     * @var string
     */
    protected $ean;

    /**
     * Description
     *
     * @var string
     */
    protected $description;

    /**
     * Marque
     *
     * @var string
     */
    protected $brand;

    /**
     * Date start
     *
     * @var string
     */
    protected $dateStart;


    /**
     * Date end
     *
     * @var string
     */
    protected $dateEnd;

    /**
     * filterQty
     *
     * @var string
     */
    protected $filterQty;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * @param string $dateStart
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
    }

    /**
     * @return string
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @param string $dateEnd
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getEan()
    {
        return $this->ean;
    }

    /**
     * @param string $ean
     */
    public function setEan($ean)
    {
        $this->ean = $ean;
    }

    /**
     * @return string
     */
    public function getFilterQty()
    {
        return $this->filterQty;
    }

    /**
     * @param string $filterQty
     */
    public function setFilterQty($filterQty)
    {
        $this->filterQty = $filterQty;
    }


    /**
     * Get search data
     *
     * @return array
     */
    public function getSearchData()
    {
        $tab = array();

        if (!empty($this->id)) {
            $tab['id'] = $this->id;
        }
        if (!empty($this->name)) {
            $tab['name'] = $this->name;
        }
        if (!empty($this->sku)) {
            $tab['sku'] = $this->sku;
        }
        if (!empty($this->ean)) {
            $tab['ean'] = $this->ean;
        }
        if (!empty($this->description)) {
            $tab['description'] = $this->description;
        }
        if (!empty($this->brand)) {
            $tab['brand'] = $this->brand;
        }
        if (!empty($this->dateStart)) {
            $tab['dateStart'] = $this->dateStart;
        }
        if (!empty($this->dateEnd)) {
            $tab['dateEnd'] = $this->dateEnd;
        }
        $tab['filterQty'] = $this->filterQty;
        return $tab;
    }

    /**
     * Get description
     *
     * @return  string
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param  string  $description  Description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get marque
     *
     * @return  string
     */ 
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set marque
     *
     * @param  string  $brand  Marque
     *
     * @return  self
     */ 
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }
}
