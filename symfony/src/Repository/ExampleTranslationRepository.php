<?php

/**
 * Repository
 *
 * @author Pierre Baumes <pierre.b@disko.fr>
 */

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class ExampleTranslationRepository
 *
 * @package App\Repository
 */
class ExampleTranslationRepository extends BaseRepository
{
}
