<?php

/**
 * Fixture
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\DataFixtures;

use App\Entity\Locale;
use App\Entity\MyBoxesProduct;
use App\Entity\ProductTranslation;
use App\Utils\Managers\LocaleManager;
use App\Utils\Managers\MyBoxesProductManager;
use App\Utils\Managers\ProductManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\Product;

/**
 * Class LoadMyBoxesProductData
 *
 * @package Disko\UserBundle\DataFixtures\ORM
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class LoadMyBoxesProductData extends Fixture implements DependentFixtureInterface
{
    private $myBoxesProductManager;

    public function __construct(MyBoxesProductManager $myBoxesProductManager)
    {
        $this->myBoxesProductManager = $myBoxesProductManager;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $mybp = new MyBoxesProduct();
        $mybp->setName("Boite Rouge 55L");
        $mybp->setDescription("Plastique et résitante");
        $mybp->setStock(0);
        $this->myBoxesProductManager->save($mybp);


        $mybp = new MyBoxesProduct();
        $mybp->setName("Boite Bleue 72L");
        $mybp->setDescription("Plastique et résitante");
        $mybp->setStock(0);
        $this->myBoxesProductManager->save($mybp);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

    public function getDependencies()
    {
        return [
          LoadLocaleData::class
      ];
    }
}
