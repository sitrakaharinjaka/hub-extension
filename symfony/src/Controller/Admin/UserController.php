<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\Model\SearchUser;
use App\Form\Type\User\SearchUserType;
use App\Form\Type\User\UserCreateType;
use App\Form\Type\User\UserType;
use App\Utils\Managers\UserManager;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 *
 * @Route("/admin/users")
 * @package App\Controller
 */
class UserController extends BaseController
{
    /**
     * User's list
     * @Route("/list", name="admin_user_list")
     * @param Request $request Request
     */
    public function listAction(Request $request, UserManager $userManager)
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchUserType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des utilisateurs');

        // Generate query
        $query = $userManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20
        );
        // Render view
        return $this->render('@Admin/User/index.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     *
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchUser();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setEmail((isset($filters['email']))   ? $filters['email'] : '');
        $data->setFirstName((isset($filters['firstName'])) ? $filters['firstName'] : '');
        $data->setLastName((isset($filters['lastName'])) ? $filters['lastName'] : '');

        return $data;
    }

    /**
     * Update a user
     * @Route("/create", name="admin_user_create")
     * @param Request $request Request
     *
     * @return Response
     */
    public function createAction(Request $request, UserManager $userManager)
    {
        // Find it
        $user =  new User();
        $user->setPlainPassword("diskodisko");

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des utilisateurs", $this->get("router")->generate("admin_user_list"));
        $breadcrumbs->addItem("Création");

        // Init form
        $form = $this->createForm(UserCreateType::class, $user, [
            'em' => $this->getDoctrine()->getManager(),
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {

                // Save
                $user = $userManager->save($user);

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'notice',
                    "Utilisateur créé"
                );

                return $this->redirectToRoute('admin_user_edit', ['id' => $user->getId()]);
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/User/create.html.twig',
            array(
                'form'           => $form->createView(),
                'user'           => $user,
            )
        );
    }

    /**
     * Update a user
     * @Route("/edit/{id}/{onglet}", name="admin_user_edit", defaults={"onglet"="general"})
     * @param Request $request Request
     * @param integer $id      Id of user
     *
     * @return Response
     */
    public function editAction(Request $request, UserManager $userManager, $id, $onglet)
    {
        // Find it
        $user =  $userManager->findOneBy(['id' => $id]);
        if (!$user) {
            throw $this->createNotFoundException('User not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des utilisateurs", $this->get("router")->generate("admin_user_list"));
        $breadcrumbs->addItem("Modification");

        // Init form
        $form = $this->createForm(UserType::class, $user, [
            'em' => $this->getDoctrine()->getManager(),
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {

                // Save
                $userManager->save($user);

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'notice',
                    "Mise à jour effectuée"
                );

                return $this->redirect($request->headers->get('referer'));
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/User/edit.html.twig',
            array(
                'form'           => $form->createView(),
                'user'           => $user,
                'onglet'         => $onglet,
            )
        );
    }

    /**
     * Delete on user
     * @Route("/delete/{id}", name="admin_user_delete")
     * @param Request $request Request
     * @param integer $id      Id of user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, UserManager $userManager, $id)
    {
        // Find it
        /** @var User $user */
        $user =  $userManager->findOneBy(['id' => $id]);
        if (!$user) {
            throw $this->createNotFoundException('User not found');
        }

        $userManager->remove($user);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            'L\'utilisateur "'.$user->getFirstName().' '.$user->getLastName().'" a bien été supprimé'
        );

        return $this->redirect($request->headers->get('referer'));
    }
}
