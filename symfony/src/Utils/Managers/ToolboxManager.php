<?php

namespace App\Utils\Managers;

use App\Repository\ToolboxRepository;
use App\Utils\Managers\BaseManager;

use App\Utils\Managers\LocaleManager;
use App\Entity\Toolbox;
use App\Entity\ToolboxTranslation;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class ToolboxManager
 * `
 * Object manager of toolbox
 *
 * @package Disko\PageBundle\Managers
 */
class ToolboxManager extends BaseManager
{

    /**
     * Repository
     *
     * @var ToolboxRepository
     */
    protected $toolboxRepository;

    /**
     * ToolboxManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('toolboxRepository', Toolbox::class);
    }

    /**
     * Save a toolbox
     *
     * @param Toolbox $toolbox
     */
    public function save(Toolbox $toolbox)
    {
        $toolbox->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($toolbox);
        $this->getEntityManager()->flush();

        return $toolbox;
    }

    /**
     * Remove one toolbox
     *
     * @param Toolbox $toolbox
     */
    public function remove(Toolbox $toolbox)
    {
        $this->getEntityManager()->remove($toolbox);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one Active
     *
     * @param $slugWl
     * @param $locale
     * @return mixed
     */
    public function findOneActive($slugWl, $locale)
    {
        $toolbox = $this->toolboxRepository->findOneActive($slugWl, $locale);

        return $toolbox;
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $toolbox = $this->toolboxRepository->findOneToEdit($id);

        return $toolbox;
    }

    /**
     * Get all toolbox
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->toolboxRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->toolboxRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->toolboxRepository->findOneBy($filters);
    }

    public function findOneByCategory(string $category)
    {
        return $this->toolboxRepository->findOneByCategory($category);
    }
}
