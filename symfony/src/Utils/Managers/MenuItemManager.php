<?php

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;

use App\Utils\Managers\LocaleManager;
use App\Entity\MenuItem;
use App\Entity\MenuItemTranslation;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class MenuItemManager
 * `
 * Object manager of menuitem
 *
 * @package Disko\PageBundle\Managers
 */
class MenuItemManager extends BaseManager
{

    /**
     * Repository
     *
     * @var MenuItemRepository
     */
    protected $menuitemRepository;

    /**
     * MenuItemManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('menuitemRepository', MenuItem::class);
    }

    /**
     * Save a menuitem
     *
     * @param MenuItem $menuitem
     */
    public function save(MenuItem $menuitem)
    {
        $menuitem->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($menuitem);
        $this->getEntityManager()->flush();

        return $menuitem;
    }

    /**
     * Remove one menuitem
     *
     * @param MenuItem $menuitem
     */
    public function remove(MenuItem $menuitem)
    {
        $this->getEntityManager()->remove($menuitem);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one Active
     *
     * @param $slugWl
     * @param $locale
     * @return mixed
     */
    public function findOneActive($slugWl, $locale)
    {
        $menuitem = $this->menuitemRepository->findOneActive($slugWl, $locale);

        return $menuitem;
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $menuitem = $this->menuitemRepository->findOneToEdit($id);

        return $menuitem;
    }

    /**
     * Get all menuitem
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->menuitemRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->menuitemRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->menuitemRepository->findOneBy($filters);
    }

    /**
     * Find all by pos deep
     *
     * @return mixed
     */
    public function findAllByPosDeep()
    {
        $items = $this->menuitemRepository->findAllByPosDeep();

        $results = [];
        if (count($items) > 0) {
            foreach ($items as $item) {
                if (!$item->getParent() or $item->getParent() == 0) {
                    $results[] = $item;
                    $results = $this->recursiveDeep($item, $items, $results);
                }
            }
        }

        return $results;
    }

    /**
     * Recursive deep
     * @param $item
     * @param $items
     * @param array $results
     * @return array
     */
    private function recursiveDeep($item, $items, $results = array())
    {
        foreach ($items as $child) {
            if ($child->getParent() == $item->getId()) {
                $results[] = $child;
                $results = $this->recursiveDeep($child, $items, $results);
            }
        }

        return $results;
    }
}
