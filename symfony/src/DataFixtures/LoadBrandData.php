<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use App\Entity\BrandTranslation;
use App\Utils\Managers\BrandManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Doctrine\Bundle\FixturesBundle\Fixture;

class LoadBrandData extends Fixture implements DependentFixtureInterface
{
    private $brandManager;
    private $localeManager;

    public function __construct(BrandManager $brandManager, LocaleManager $localeManager)
    {
        $this->brandManager = $brandManager;
        $this->localeManager = $localeManager;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
//        $brands = array(
//            ['name' => 'Evian'],
//            ['name' => 'Corona'],
//            ['name' => 'Ricard'],
//            ['name' => 'Ruinart']
//        );
//
//        $locales = $this->localeManager->findAll();
//
//        foreach ($brands as $brand) {
//            $obj = new Brand();
//            foreach ($locales as $locale) {
//                $trans = new BrandTranslation();
//                $trans->setLocale($locale->getSlug());
//                $trans->setName($brand['name']);
//                $obj->addTranslation($trans);
//            }
//            $this->brandManager->save($obj);
//            $this->addReference('admin-brand-'.$obj->getId(), $obj);
//        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

    public function getDependencies()
    {
        return [
            LoadLocaleData::class
        ];
    }
}
