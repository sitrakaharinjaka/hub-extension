<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ProjectZipConstraint extends Constraint
{
    public $message = 'Le code postal ne correspond à aucun code postal de la region parisienne.';
    public $messageCountry = 'Ce code postal n\'existe pas en France.';

    public function validatedBy()
    {
        return 'project_zip_constraint';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
