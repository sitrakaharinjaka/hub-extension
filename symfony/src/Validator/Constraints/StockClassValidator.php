<?php

namespace App\Validator\Constraints;

use App\Entity\Project;
use App\Entity\ProjectItem;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockLineManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use DateTime;

class StockClassValidator extends ConstraintValidator
{
    /**
     * @var ProjectManager
     */
    private $projectManager;

    /**
     * @var StockLineManager
     */
    private $stockLineManager;

    /**
     * StockClassValidator constructor.
     *
     * @param ProductManager $projectManager
     */
    public function __construct(ProjectManager $projectManager, StockLineManager $stockLineManager)
    {
        $this->projectManager = $projectManager;
        $this->stockLineManager = $stockLineManager;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $project = $value;
        
        $ids = [];
        $objects = [];

        foreach ($project->getItems() as $key => $item) {
            $ids[] = $item->getProduct()->getId();
            $objects[] = $item->getProduct();
        }

        $datas = $this->projectManager->getParameters($project);

        if ($datas['cave'] == Project::CAVE_115) {
            $totalWithHolds = $this->stockLineManager->totalStockOnProducts($project->getStock(), $datas['cave'], $ids, $datas['dateStart'], $datas['dateEnd'], true);
        }else{
            $totalWithHolds = [];
            foreach($objects as $object){
                $totalWithHolds[] = [
                    'total' => $this->stockLineManager->quantityOf($object, Project::CAVE_PANTIN, new DateTime('now'), $project->getStock(), false),
                    'id' => $object->getId()
                ];
            }
        }

        foreach ($project->getItems() as $key => $item) {
            $stock = 0;
            foreach ($totalWithHolds as $tab) {
                if ($tab["id"] == $item->getProduct()->getId()) {
                    $stock = $tab["total"];
                }
            }


            $operation = $item->getQuantity() + $project->getBookItem($item->getProduct()->getId());

            if ($operation > $stock) {
                $this->context->buildViolation($constraint->message)->setParameters([
                    '%count%' => $stock
                ])->atPath('items['.$key.'].quantity')->addViolation();
            }
        }
    }
}
