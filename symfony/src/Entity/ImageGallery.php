<?php

namespace App\Entity;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * @ORM\Table(name="dk_image_gallery")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ImageGallery
{
    use GedmoTrait;
    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Media", cascade={"all"})
     * @var Media
     */
    protected $image;

    /**
     * @var ProductTranslation|null
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductTranslation", inversedBy="imageGallery")
     * @ORM\JoinColumn(name="translatable_id", referencedColumnName="id")
     */
    protected $productTranslation;

    /**
     * @return mixed
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return ProductTranslation
     */
    public function getProductTranslation(): ProductTranslation
    {
        return $this->productTranslation;
    }

    /**
     * @param ProductTranslation $productTranslation
     */
    public function setProductTranslation(ProductTranslation $productTranslation): void
    {
        $this->productTranslation = $productTranslation;
    }
}
