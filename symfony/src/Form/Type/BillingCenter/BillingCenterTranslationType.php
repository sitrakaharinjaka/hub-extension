<?php

/**
 * Form type
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Type\BillingCenter;

use App\Entity\BillingCenterTranslation;
use App\Entity\Contact;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\Fo;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use App\Utils\Managers\UserManager;
/**
 *  Form Type
 */
class BillingCenterTranslationType extends AbstractType
{
    protected $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class);
        $builder->add('description', TextareaType::class, array('required' => false));
        $builder->add('house', TextType::class, array('required' => false));
        $builder->add('department', TextType::class, array('required' => false));
        $builder->add('company', TextType::class, array('required' => false));
        $builder->add('address1', TextType::class, array('required' => false));
        $builder->add('address2', TextType::class, array('required' => false));
        $builder->add('zipCode', TextType::class, array('required' => false));
        $builder->add('city', TextType::class, array('required' => false));

        // $builder->add('contact', EntityType::class, [
        //   'class' => User::class,
        //   'query_builder' => function (EntityRepository $erc) {
        //       return $erc->createQueryBuilder('u')
        //                  ->andWhere('u.roles NOT LIKE :role')
        //                  ->setParameter('role', '%ROLE_SUPER_ADMIN%')
        //                  ->orderBy('u.lastName', 'ASC');
        //   },
        //   'choice_label' => 'fullName',
        //   'by_reference' => false,
        //   'multiple' => true,
        //   'required'  => false
        // ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();

            if (!is_null($data) && !is_null($data->getContact())) {
                $options = $form->get('contact')->getConfig()->getOptions();
                $options['choices'] = $data->getContact();
                $form->add('contact', EntityType::class, $options);
            }
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            
            $userRepository = $this->em->getRepository(User::class);
            $contacts = $userRepository->findByIds($data['contact'] ?? []);

            $form = $event->getForm();
            $data['contact'] = $contacts;
            $options = $form->get('contact')->getConfig()->getOptions();
            $options['choices'] = $contacts;
            $form->add('contact', EntityType::class, $options);
            $event->getForm()->get('contact')->setData($contacts);
            $formData = $form->getData();
            $formData->setContact($contacts);
            $form->setData($formData);
        });

        $builder->add('contact', EntityType::class, [
            'placeholder' => 'Sélectionner un utilisateur',
            'multiple' => true,
            'required'  => false,
            'class' => User::class,
            'choices' => [],
            'choice_label' => 'fullName',
        ]);
    }

    /**
     * Configure Options
     *
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
            'data_class' => BillingCenterTranslation::class
        ));
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'billingcenter';
    }
}
