<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 25/09/2018
 * Time: 15:00
 */

namespace App\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use App\Entity\Example;

use Symfony\Component\Serializer\Normalizer\NormalizableInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

use Algolia\SearchBundle\IndexManagerInterface;

class AlgoliaIndexerListener
{
    protected $indexManager;

    public function __construct(IndexManagerInterface $indexingManager)
    {
        $this->indexManager = $indexingManager;
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->postPersist($args);
    }
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof NormalizableInterface) {
            return;
        }

        $entityManager = $args->getObjectManager();

        try {
            if ((method_exists($entity, 'isIndexable') && $entity->isIndexable()) or
                (method_exists($entity, 'getActive') && $entity->getActive())) {
                $this->indexManager->index($entity, $entityManager);
            } else {
                $this->indexManager->remove($entity, $entityManager);
            }
        } catch (\Exception $e) {
            // Put here debug if you want
        }
    }
}
