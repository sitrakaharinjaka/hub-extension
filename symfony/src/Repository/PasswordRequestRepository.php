<?php
// src/Repository/PasswordRequestRepository.php
namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class PasswordRequestRepository extends EntityRepository
{
    /**
     * @param $email
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByEmail($email)
    {
        $qb = $this->createQueryBuilder('p');

        $qb->select('p');
        $qb->andWhere("p.email = :email");
        $qb->setParameter('email', $email);
        $qb->andWhere("p.expireAt > CURRENT_TIMESTAMP()");

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }


    /**
     * @param $token
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByToken($token)
    {
        $qb = $this->createQueryBuilder('p');

        $qb->select('p');
        $qb->andWhere("p.token = :token");
        $qb->setParameter('token', $token);
        $qb->andWhere("p.expireAt > CURRENT_TIMESTAMP()");

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }
}
