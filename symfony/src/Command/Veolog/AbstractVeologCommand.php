<?php

namespace App\Command\Veolog;

use App\Command\AbstractCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractVeologCommand extends AbstractCommand
{
    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger)
    {
        parent::__construct($mailer, $parameterBag, $logger);
    }

    final private function getDirVlg($mode)
    {
        $vlgenv = $this->input == null ? false : $this->input->getOption('vlgenv', false);
        if ($vlgenv == 'false') {
            $dirVelog = 'TEST_' . $mode;
            $this->log('info', "VEOLOG Repository (1 - " . $vlgenv . " - " . $this->ENVIRONNEMENT . ") :" . $dirVelog);
        } elseif ($this->ENVIRONNEMENT == 'DEVELOPEMENT' || $this->ENVIRONNEMENT == 'DEV' || $this->ENVIRONNEMENT == 'RECETTE' || $this->ENVIRONNEMENT == 'REC' || $this->ENVIRONNEMENT == 'PREPROD'|| $this->ENVIRONNEMENT == 'PP') {
            $dirVelog = 'TEST_' . $mode;
            $this->log('info', "VEOLOG Repository (2 - " . $vlgenv . " - " . $this->ENVIRONNEMENT . ") :" . $dirVelog);
        } else {
            $dirVelog = '' . $mode;
            $this->log('info', "VEOLOG Repository (3 - " . $vlgenv . " - " . $this->ENVIRONNEMENT . ") :" . $dirVelog);
        }
        return $dirVelog;
    }

    final protected function getDirVlgOUT()
    {
        return $this->getDirVlg('OUT');
    }

    final protected function getDirVlgIN()
    {
        return $this->getDirVlg('IN');
    }

    final protected function connectFTP()
    {
        $ftp_handle = ftp_connect('ftp.veolog.fr');
        if (false === $ftp_handle) {
            $this->log('info', "Unable to connect on ftp.veolog.fr");
            throw new \Exception('Unable to connect FTP');
        }

        $loggedIn = ftp_login($ftp_handle, 'vlgcli18', '1L8vJv2U75');
        ftp_pasv($ftp_handle, true);
        if (true !== $loggedIn) {
            $this->log('info', "Unable to log in ftp.veolog.fr with vlgcli18");
            throw new \Exception('Unable to log in FTP');
        }

        $this->log('info', "Connect FTP <info>success</info>");
        return $ftp_handle;
    }

    abstract protected function executeCommand();
}
