<?php

/**
 * Model for search
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Model;

/**
 * Class SearchMyBoxesBox
 *
 * @package App\Form\Model
 */
class SearchMyBoxesBox
{
    /**
     * Id
     *
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $barcode;

    /**
     * @var string
     */
    protected $seal;

    /**
     * @var string
     */
    protected $email;

    /**
     * State
     *
     * @var string
     */
    protected $state;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param $barcode
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    }

    /**
     * @return string
     */
    public function getSeal()
    {
        return $this->seal;
    }

    /**
     * @param $seal
     */
    public function setSeal($seal)
    {
        $this->seal = $seal;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * Get search data
     *
     * @return array
     */
    public function getSearchData()
    {
        $tab = array();

        if (!empty($this->id)) {
            $tab['id'] = $this->id;
        }
        if (!empty($this->name)) {
            $tab['name'] = $this->name;
        }
        if (!empty($this->barcode)) {
            $tab['barcode'] = $this->barcode;
        }
        if (!empty($this->seal)) {
            $tab['seal'] = $this->seal;
        }
        if (!empty($this->email)) {
            $tab['email'] = $this->email;
        }
        if (!empty($this->state)) {
            $tab['state'] = $this->state;
        }


        return $tab;
    }
}
