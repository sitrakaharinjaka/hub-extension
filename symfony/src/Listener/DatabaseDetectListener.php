<?php

namespace App\Listener;

use App\Entity\IP;
use App\Utils\Managers\IPManager;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Process\Process;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

/**
 * Subscriber on kernelRequest
 * Checks database dump sql have
 */
class DatabaseDetectListener implements EventSubscriberInterface, ServiceSubscriberInterface
{

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    private $clientIp;

    /**
     * Constructor
     */
    public function __construct(
        RouterInterface $router,
        SessionInterface $session,
        TokenStorageInterface $token,
        AuthorizationCheckerInterface $authorizationChecker,
        ContainerInterface $container
    ) {
        $this->router = $router;
        $this->session = $session;
        $this->token = $token;
        $this->authorizationChecker = $authorizationChecker;
        $this->container = $container;
    }


    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        /**
        if (
            (preg_match('/\/admin/', $event->getRequest()->getRequestUri()) == 1) &&
            ($this->token->getToken() !== null && $this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN'))
           ) {
            if ($this->session->get("detect_time_protect", 0) <= time()) {
                $this->session->set("detect_time_protect", time() + 7200);


                $pathRootDir = $this->getParameter('kernel.root_dir');
                $process = new Process(
                    'php bin/console doctrine:schema:update --dump-sql',
                    $pathRootDir.'/../'
                );
                $process->run();

                $output = $process->getOutput();

                if (preg_match('[OK]', $output) != 1) {
                    $response = new RedirectResponse($this->router->generate('admin_database_detect_dump', ['secure' => true]));
                    $event->setResponse($response);
                }
            }
        }**/
    }

    /**
     * Gets a container parameter by its name.
     *
     * @return mixed
     *
     * @final
     */
    protected function getParameter(string $name)
    {
        if (!$this->container->has('parameter_bag')) {
            throw new ServiceNotFoundException('parameter_bag', null, null, [], sprintf('The "%s::getParameter()" method is missing a parameter bag to work properly. Did you forget to register your controller as a service subscriber? This can be fixed either by using autoconfiguration or by manually wiring a "parameter_bag" in the service locator passed to the controller.', \get_class($this)));
        }

        return $this->container->get('parameter_bag')->get($name);
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST => array(
                array('onKernelRequest', 1),
            ),
        );
    }

    public static function getSubscribedServices()
    {
        return [
            'parameter_bag' => '?'.ContainerBagInterface::class,
        ];
    }
}
