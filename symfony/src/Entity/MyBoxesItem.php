<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Paweł Jędrzejewski
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Validator\Constraints as ProjectAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MyBoxesItemRepository")
 * @ORM\Table(name="project_myboxes_item")
 */
class MyBoxesItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var integer
     */
    protected $id;

    /**
     * @var Project|null
     * @ORM\ManyToOne(targetEntity="App\Entity\MyBoxesCommand", inversedBy="items")
     * @ORM\JoinColumn(name="command_id", referencedColumnName="id")
     */
    protected $command;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MyBoxesProduct")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    protected $quantity = 0;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Project|null
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @param Project|null $command
     */
    public function setCommand($command)
    {
        $this->command = $command;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
    }
}
