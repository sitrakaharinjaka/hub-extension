<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\Fail2Ban;
use App\Form\Model\SearchFail2Ban;
use App\Form\Type\User\SearchFail2BanType;
use App\Utils\Managers\Fail2BanManager;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class Fail2BanController
 * @Route("/admin/fail2ban")
 * @package App\Controller
 */
class Fail2BanController extends BaseController
{
    /**
     * Fail2Ban's list
     * @Route("/list", name="admin_fail2ban_list")
     * @param Request $request Request
     */
    public function index(Request $request, Fail2BanManager $failtobanManager)
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchFail2BanType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des tentatives');

        // Generate query
        $query = $failtobanManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            50
        );

        // Render view
        return $this->render('@Admin/Fail2Ban/index.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchFail2Ban();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setUsername((isset($filters['username']))   ? $filters['username'] : '');
        $data->setIp((isset($filters['ip']))   ? $filters['ip'] : '');
        $data->setRoute((isset($filters['route']))   ? $filters['route'] : '');

        return $data;
    }

    /**
     * Delete one fail2ban
     * @Route("/delete/{id}", name="admin_fail2ban_delete")
     * @param Request $request Request
     * @param integer $id      Id of fail2ban
     */
    public function delete(Request $request, Fail2BanManager $failtobanManager, $id)
    {
        // Find it
        /** @var Fail2Ban $fail2ban */
        $fail2ban = $failtobanManager->findOneBy(['id' => $id]);
        if (!$fail2ban) {
            throw $this->createNotFoundException('Fail2Ban not found');
        }

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            'La tentative "'.$fail2ban->getId().'" a bien été supprimée'
        );


        $failtobanManager->remove($fail2ban);

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Export csv
     * @Route("/export", name="admin_fail2ban_export")
     * @param Request $request
     */
    public function export(Request $request, Fail2BanManager $failtobanManager)
    {
        $data = $this->initSearch($request);
        $query = $failtobanManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Login',
            'Ip',
            'Uri',
            'Date de création',
            'Succès',
        ], ';');

        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->getUsername(),
                $result->getIp(),
                $result->getRoute(),
                $result->getCreated()->format('d/m/Y H:i'),
                (int) $result->getSuccess(),
            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_fail2bans_export.csv"'
        ));
    }
}
