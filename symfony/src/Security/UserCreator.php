<?php
/**
 * Created by PhpStorm.
 * User: lsimonin
 * Date: 25/10/2018
 * Time: 10:59
 */

namespace App\Security;

use App\Entity\Connection;
use App\Entity\User;
use App\Utils\Managers\ConnectionManager;
use App\Utils\Managers\UserManager;
use LightSaml\Model\Protocol\Response;
use LightSaml\SpBundle\Security\User\UserCreatorInterface;
use LightSaml\SpBundle\Security\User\UsernameMapperInterface;

class UserCreator implements UserCreatorInterface
{
    /** @var UsernameMapperInterface */
    private $usernameMapper;

    /**
     * @var UserInterface
     */
    private $userManager;

    /**
     * @var ConnectionManager
     */
    private $connectionManager;

    /**
     * UserCreator constructor.
     *
     * @param                        $usernameMapper
     * @param UserInterface          $userManager
     */
    public function __construct($usernameMapper, UserManager $userManager, ConnectionManager $connectionManager)
    {
        $this->usernameMapper = $usernameMapper;
        $this->userManager = $userManager;
        $this->connectionManager = $connectionManager;
    }

    /**
     * @param Response $response
     */
    public function createUser(Response $response)
    {
    }

    public function createHennessyUser(Response $response, $attributes)
    {
        $username = $this->usernameMapper->getUsername($response);
        $user = new User();
        $user->setEmail($username);
        $user->setUsername($username);
        $user->setPlainPassword('pswd');
        $user->setRoles(['ROLE_USER']);
        $user->setActive(true);

        $user = $this->setHennessyUser($user, $attributes);

        if ($username == "mateo@disko.fr" or $user->getEmail() == "caroline.etienne@disko.fr") {
            $user->setRoles(['ROLE_USER', 'ROLE_GENERAL_ADMIN', 'ROLE_SUPER_ADMIN']);
        }

        $user->setLastConnection(new \DateTime());
        $this->userManager->save($user);
        $this->addConnection($user);

        return $user;
    }

    public function updateUser(User $user, $attributes)
    {
        $user = $this->setHennessyUser($user, $attributes);
        if ($user->getEmail() == "mateo@disko.fr" or $user->getEmail() == "caroline.etienne@disko.fr") {
            $user->setRoles(['ROLE_USER', 'ROLE_GENERAL_ADMIN', 'ROLE_SUPER_ADMIN']);
        }

        $user->setLastConnection(new \DateTime());
        $user = $this->setUser($user, $attributes);
        $this->userManager->save($user);
        $this->addConnection($user);

        return $user;
    }


    private function addConnection($user)
    {
        $date = new \DateTime();
        $doy = $date->format('z');
        $connection = $this->connectionManager->findOneBy(['user' => $user, 'doy' => $doy]);
        if (!$connection) {
            $connection = new Connection();
            $connection->setDate(new \DateTime());
            $connection->setDoy($doy);
            $connection->setUser($user);
            $this->connectionManager->save($connection);
        }
    }

    private function setHennessyUser(User $user, $attributes)
    {
        $user->setFirstName(isset($attributes['First-Name']) ? $attributes['First-Name'] : null);
        $user->setLastName(isset($attributes['Last-Name']) ? $attributes['Last-Name'] : null);
        $user->setDisplayName(isset($attributes['Display-Name']) ? $attributes['Display-Name'] : null);
        $user->setDepartment(isset($attributes['Department']) ? $attributes['Department'] : null);
        $user->setDivision(isset($attributes['Division']) ? $attributes['Division'] : null);
        $user->setCompany(isset($attributes['Company']) ? $attributes['Company'] : null);
        $user->setPhone(isset($attributes['Telephone-Number']) ? $attributes['Telephone-Number'] : null);
        $user->setOrganizationUnit(isset($attributes['Organization-Unit']) ? $attributes['Organization-Unit'] : null);

        if (is_array($attributes)) {
            $user->setAttributes(json_encode($attributes));
        }

        return $user;
    }


    private function setUser(User $user, $attributes)
    {
        return $user;
    }
}
