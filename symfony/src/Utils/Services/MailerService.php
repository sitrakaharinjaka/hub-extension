<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 26/12/2017
 * Time: 18:33
 */

namespace App\Utils\Services;

use App\Entity\Project;
use App\Entity\User;
use App\Entity\MyBoxesBox;
use App\Entity\MyBoxesCommand;
use App\Entity\MyBoxesBoxProduct;
use App\Event\ProjectEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use DateTime;
use DateTimeZone;
use Swift_Attachment;

/**
 * Class CoreServices
 *
 * @package App\Services
 */
class MailerService
{
    /** @var \Swift_Mailer */
    private $mailer;

    /** @var Environment */
    private $twig;

    /** @var UrlGeneratorInterface */
    private $router;

    private $senderAddress;

    private $translator;

    private $parameter;

    protected $ENVIRONNEMENT;

    private $logger;

    private const EMAILS_SERVICE_COURRIER = ['hub142@moethennessy.com'];
    private const EMAILS_SERVICE_RUNNER = ['115@moethennessy.com', 'maintenance-services142@moethennessy.com'];
    private const EMAILS_VEOLOG = ['hub142@veolog.fr'];
    private const EMAILS_VEOLOG_RCT = ['a.bertolino@veolog.fr'];
    private const EMAILS_CONTESTATION = ['hub142@moethennessy.com'];
    private const EMAILS_CONTESTATION_DEV = ['hub142@moethennessy.com'];

    /**
     * MailerService constructor.
     *
     * @param  \Swift_Mailer  $mailer
     * @param  Environment  $twig
     * @param  UrlGeneratorInterface  $router
     * @param  string  $senderAddress
     */
    public function __construct(\Swift_Mailer $mailer, TranslatorInterface $translator, Environment $twig, UrlGeneratorInterface $router, ParameterBagInterface $parameter, string $senderAddress, LoggerInterface $logger)
    {
        $this->mailer        = $mailer;
        $this->twig          = $twig;
        $this->router        = $router;
        $this->senderAddress = $senderAddress;
        $this->translator    = $translator;
        $this->parameter     = $parameter;
        $this->ENVIRONNEMENT = $parameter->get('server_mode');
        $this->logger        = $logger;
    }

    /**
     * @param  User  $user
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendResettingEmailMessage(User $user)
    {
        $url      = $this->router->generate(
            'password_reset',
            array('token' => $user->getConfirmationToken()),
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $rendered = $this->twig->render(
            'common/mails/resetting_password.html.twig',
            array(
                'user'            => $user,
                'confirmationUrl' => $url,
            )
        );
        $this->sendEmailMessage($rendered, $user->getEmail(), 'Reset password');
    }

    /**
     * @param  Project  $project
     */
    public function sendWarnNearProjectDateReturnEmailMessage(Project $project)
    {
        $emails = [];
        $emails[] = $project->getEmailReturn();
        return $this->createMail(
            'Date de retour commande "' . $project->getName() . '"',
            $emails,
            'mails/warn_near_project_date_return',
            [
                'project'            => $project,
            ]
        );
    }

    /**
     * @param  Project  $project
     */
    public function sendNotifyPresidenceProject(Project $project)
    {
        $emails = [];

        if ($project->getStock()->getCave() == Project::CAVE_115) {
            $emails[] = '115@moethennessy.com';
        }else{
            $emails = [
                'admin.patin@veolog.fr',
                'a.dacruz@veolog.fr',
                'hub142@moethennessy.com',
                'bmaraval@moethennessy.com'
            ];
            
            if ($this->ENVIRONNEMENT == 'PREPROD' || $this->ENVIRONNEMENT == 'PP' || $this->ENVIRONNEMENT == 'RECETTE' || $this->ENVIRONNEMENT == 'REC') {
                foreach(MailerService::EMAILS_VEOLOG_RCT as $e){
                    $emails[] = $e;
                }
            }else{
                foreach(MailerService::EMAILS_VEOLOG as $e){
                    $emails[] = $e;
                }
            }
        }

        return $this->createMail(
            'URGENCE - PRESIDENCE',
            $emails,
            'mails/warn_presidence',
            [
                'project'            => $project,
            ],
            [],
            null,
            'cc'
        );
    }

    /**
     * @param Stock $stock
     * @param array $list
     */
    public function sendWarnFewProducts($stock, $list)
    {
        $emails = [];
        foreach ($stock->getAdminUsers() as $admin) {
            $emails[] = $admin->getEmail();
        }
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $emails[] = $email;
        }
        return $this->createMail(
            'Produits niveaux bas sur le stock "' . $stock->translate('fr')->getName() . '"',
            $emails,
            'mails/warn_few_products',
            [
                'stock' => $stock,
                'list'  => $list,
            ]
        );
    }

    /**
     * @param MyBoxesBoxProduct $product
     */
    public function sendWarnFewBoxProducts($product)
    {
        $emails = ['hub142@moethennessy.com'];

        if ($this->ENVIRONNEMENT == 'PREPROD' || $this->ENVIRONNEMENT == 'PP' || $this->ENVIRONNEMENT == 'RECETTE' || $this->ENVIRONNEMENT == 'REC' || $this->ENVIRONNEMENT == 'DEVELOPEMENT') {
            $emails[] = 'dtang-ext@moethennessy.com';
        }else{
            $emails[] = 'bmaraval@moethennessy.com';
        }

        return $this->createMail(
            'Niveau bas MyBoxes "' . $product->getName() . '"',
            $emails,
            'mails/warn_few_box_products',
            [
                'product' => $product,
            ]
        );
    }

    /**
     * @param  string  $renderedTemplate
     * @param  string  $toEmail
     * @param  string  $subject
     * @param  string|null  $fromEmail
     */
    public function sendEmailMessage($renderedTemplate, $toEmail, $subject, $fromEmail = null)
    {
        if (null === $fromEmail) {
            $fromEmail = $this->senderAddress;
        }
        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail)
            ->setBody($renderedTemplate);
        $this->mailer->send($message);
    }


    /**
     * @param  Project  $project
     * @param  string  $from
     *
     * @return int
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendMailRemoveProject(Project $project, string $from)
    {
        // @todo : $to = [$this->ldapConfiguration->getHennessyServiceMail()];
        $to = [];
        if (ProjectEvent::FROM_ADMIN === $from) {
            $to = array_merge($to, [
            $project->getUser()->getEmail() => $project->getUser()->getFirstName()
                                                . ' '
                                                . $project->getUser()->getLastName()
            ]);
        } else {
            // @todo : $to = array_merge($to, $this->ldapConfiguration->getAdminsMail());
            $to = [];
        }

        return $this->createMail(
            'sylius.ui.mail.remove_project',
            $to,
            'mails/state_update_project',
            [
              'project' => $project
            ]
        );
    }

    /**
     * @param  Project  $project
     *
     * @return int
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendMailTestCron($mails)
    {
        $imp_mails = explode(',', $mails);

        return $this->createMail(
            'sylius.ui.mail.test_cron',
            $imp_mails,
            'mails/test'
        );
    }

    /**
     * @param  Project  $project
     *
     * @return int
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendMailSubmitProject(Project $project)
    {
        return $this->createMail(
            'sylius.ui.mail.submit_project',
            ["contact@hub142.moethennessy.com"],
            'mails/submit_project',
            [
                'project' => $project
            ]
        );
    }

    /**
     * @param  Project  $project
     *
     * @return int
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendMailnoIntegrateByVeolog(Project $project, $mails)
    {
        $imp_mails = explode(',', $mails);

        return $this->createMail(
            'sylius.ui.mail.veolog_no_integrate_project',
            $imp_mails,
            'mails/veolog_no_integrate_project',
            [
              'project' => $project
            ]
        );
    }

    /**
     * @param  Project  $project
     *
     * @return int
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendMailIntegrationWarningVeolog(Project $project)
    {
        $mails = [
            'hub142@moethennessy.com',
            'bmaraval@moethennessy.com',
            'service.info@veolog.fr',
            'support-hub142@disko.fr '
        ];

        return $this->createMail(
            'Anomalie – Commande # ' . $project->getId() . ' non intégrée sur le Hub142',
            $mails,
            'mails/veolog_integration_warning',
            [
              'project' => $project
            ]
        );
    }

    /**
     * @param  Project  $project
     *
     * @return int
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendMailCancelByVeolog(Project $project, $mails)
    {
        $imp_mails = [];
        foreach ($mails as $mail) {
            $imp_mails[] = $mail;
        }
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $imp_mails[] = $email;
        }
        return $this->createMail(
            'sylius.ui.mail.veolog_cancel_project',
            $imp_mails,
            'mails/veolog_cancel_project',
            [
              'project' => $project
            ]
        );
    }

    public function sendMailRejectByVeolog(Project $project, $mails)
    {
        $imp_mails = [];
        foreach ($mails as $mail) {
            $imp_mails[] = $mail;
        }
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $imp_mails[] = $email;
        }
        return $this->createMail(
            'MyHub - Commande #' . $project->getId() . ' rejetée par Veolog',
            $imp_mails,
            'mails/veolog_reject_project',
            [
              'project' => $project
            ]
        );
    }


    /**
     * @param  Project  $project
     *
     * @return int
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendMailValidateProjectToRef(Project $project, $mails)
    {
        $imp_mails = explode(',', $mails);

        $subject = $project->getState() == Project::STATE_HUB_ACCEPTED_FIXED? 'sylius.ui.mail.notification_validate_project': 'sylius.ui.mail.notification_validate_project_notfixed';

        return $this->createMail(
            $subject,
            $imp_mails,
            'mails/notification_validate_project',
            [
              'project' => $project,
            ]
        );
    }

    /**
     * @param  Project  $project
     *
     * @return int
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendMailExpeditionProjectToRef(Project $project, $mails)
    {
        $imp_mails = [];
        foreach ($mails as $mail) {
            $imp_mails[] = $mail;
        }
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $imp_mails[] = $email;
        }
        return $this->createMail(
            'sylius.ui.mail.notification_expedition_project',
            $imp_mails,
            'mails/notification_expedition_project',
            [
              'project' => $project,
            ]
        );
    }

    /**
     * @param  Project  $project
     *
     * @return int
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendMailDeleteProjectToRefAndVeolog(Project $project, $mails)
    {
        $imp_mails = explode(',', $mails);

        if ($project->getState() == Project::STATE_HUB_DRAFT_CANCEL_ASK) {
            return $this->createMail(
                'sylius.ui.mail.notification_delete_project_draft',
                $imp_mails,
                'mails/notification_delete_project_draft',
                [
                    'project' => $project,
                ]
            );
        } else if ($project->getState() == Project::STATE_HUB_CANCEL_ASK) {
            return $this->createMail(
                'sylius.ui.mail.notification_delete_project',
                $imp_mails,
                'mails/notification_delete_project',
                [
                    'project' => $project,
                ]
            );
        }
    }

    public function sendMailMyBoxesBoxAccepted(MyBoxesBox $box)
    {
        $emails = [];
        foreach ($box->getBoxUsers() as $boxuser) {
            $emails[] = $boxuser->getUser()->getEmail();
        }
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $emails[] = $email;
        }
        return $this->createMail(
            (sizeof($box->getBoxUsers()) == 1 ? 'MyBoxes' : 'MyTEAMBoxes'). ' - Confirmation de rapatriement',
            $emails,
            'mails/notification_mybox_box_accepted',
            [
              'box' => $box,
            ]
        );
    }

    public function sendMailMyBoxesBoxSent(MyBoxesBox $box)
    {
        $emails = [];
        foreach ($box->getBoxUsers() as $boxuser) {
            $emails[] = $boxuser->getUser()->getEmail();
        }
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $emails[] = $email;
        }
        return $this->createMail(
            (sizeof($box->getBoxUsers()) == 1 ? 'MyBoxes' : 'MyTEAMBoxes'). ' - Rapatriement - Confirmation d\'expédition',
            $emails,
            'mails/notification_mybox_box_sent',
            [
              'box' => $box,
            ]
        );
    }

    public function sendMailMyBoxesCommandAccepted(MyBoxesCommand $command)
    {
        $emails = [];
        $emails[] = $command->getUser()->getEmail();
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $emails[] = $email;
        }
        return $this->createMail(
            'MyBoxes - Confirmation de commande',
            $emails,
            'mails/notification_mybox_command_accepted',
            [
              'command' => $command,
            ]
        );
    }

    public function sendMailMyBoxesCommandSent(MyBoxesCommand $command)
    {
        $emails = [];
        $emails[] = $command->getUser()->getEmail();
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $emails[] = $email;
        }
        return $this->createMail(
            'MyBoxes - Confirmation d\'expédition',
            $emails,
            'mails/notification_mybox_command_sent',
            [
              'command' => $command,
            ]
        );
    }

    public function sendMailMyBoxesBoxUserSent(MyBoxesBox $box)
    {
        $emails = [];
        foreach ($box->getBoxUsers() as $boxuser) {
            $emails[] = $boxuser->getUser()->getEmail();
        }
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $emails[] = $email;
        }
        foreach (MailerService::EMAILS_VEOLOG as $email) {
            $emails[] = $email;
        }
        return $this->createMail(
            (sizeof($box->getBoxUsers()) == 1 ? 'MyBoxes' : 'MyTEAMBoxes'). ' - Confirmation d\'envoi au Hub',
            $emails,
            $box->getIsEmpty() ? 'mails/notification_mybox_user_sent_empty' : 'mails/notification_mybox_user_sent',
            [
              'box' => $box,
            ]
        );
    }

    public function sendMailMyBoxesErrorStock($errors)
    {
        $emails = [];
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $emails[] = $email;
        }
        foreach (MailerService::EMAILS_VEOLOG as $email) {
            $emails[] = $email;
        }
        return $this->createMail(
            'MyBox - Erreur lors de la synchronisation des stocks',
            $emails,
            'mails/notification_mybox_stock_error',
            [
              'errors' => $errors,
            ]
        );
    }

    public function sendMailMyBoxesErrorConfirm($errors)
    {
        $emails = [];
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $emails[] = $email;
        }
        foreach (MailerService::EMAILS_VEOLOG as $email) {
            $emails[] = $email;
        }
        return $this->createMail(
            'MyBox - Erreur lors de la confirmation de commande',
            $emails,
            'mails/notification_mybox_confirm_error',
            [
              'errors' => $errors,
            ]
        );
    }

    public function sendMailMyBoxesBackReminderSent(MyBoxesBox $box, $nbHours)
    {
        $emails = [];
        foreach ($box->getBoxUsers() as $boxuser) {
            $emails[] = $boxuser->getUser()->getEmail();
        }
        // foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
        //     $emails[] = $email;
        // }
        return $this->createMail(
            (sizeof($box->getBoxUsers()) == 1 ? 'MyBoxes' : 'MyTEAMBoxes'). ' - Pensez à renvoyer votre Box au Hub142',
            $emails,
            'mails/notification_mybox_back_reminder',
            [
              'box' => $box,
              'nbHours' => $nbHours,
            ]
        );
    }

    public function sendMailMyBoxesForecastOfReceipt($items)
    {
        $emails = [];
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $emails[] = $email;
        }
        foreach (MailerService::EMAILS_VEOLOG as $email) {
            $emails[] = $email;
        }
        return $this->createMail(
            'MyBoxes - Liste des boxes en partance pour le HUB',
            $emails,
            'mails/notification_mybox_forecast_of_receipt',
            [
              'items' => $items,
            ]
        );
    }

    public function sendMailCave115ValidateProject(Project $project)
    {
        $emails = [$project->getUser()->getEmail()];
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $emails[] = $email;
        }
        foreach (MailerService::EMAILS_SERVICE_RUNNER as $email) {
            $emails[] = $email;
        }
        return $this->createMail(
            'Cave 115 - Commande en attente',
            $emails,
            'mails/notification_cave115_validate',
            [
              'project' => $project,
            ]
        );
    }

    public function sendMailCave115PrepareProject(Project $project)
    {
        // Rien à faire.
    }

    public function sendMailCave115UnprepareProject(Project $project)
    {
        // Rien à faire
    }

    public function sendMailCave115ExpeditionProject(Project $project)
    {
        $emails = [];
        $emails[] = $project->getUser()->getEmail();
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $emails[] = $email;
        }
        foreach (MailerService::EMAILS_SERVICE_RUNNER as $email) {
            $emails[] = $email;
        }
        return $this->createMail(
            'Cave 115 - Commande préparée en cours d\'acheminement',
            $emails,
            'mails/notification_cave115_inway',
            [
              'project' => $project,
            ]
        );
    }

    public function sendMailCave115DeliverProject(Project $project)
    {
        $emails = [];
        $emails[] = $project->getUser()->getEmail();
        $emails[] = $project->getRunner()->getEmail();
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $emails[] = $email;
        }
        foreach (MailerService::EMAILS_SERVICE_RUNNER as $email) {
            $emails[] = $email;
        }
        return $this->createMail(
            'Cave 115 - Commande livrée',
            $emails,
            'mails/notification_cave115_delivered',
            [
              'project' => $project,
            ]
        );
    }

    public function sendSynchStockErrors($stockVariations)
    {
        $emails = [];
        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            // $emails[] = $email;
        }
        foreach (MailerService::EMAILS_VEOLOG as $email) {
            // $emails[] = $email;
        }

        //if ($this->ENVIRONNEMENT == 'PREPROD' || $this->ENVIRONNEMENT == 'PP' || $this->ENVIRONNEMENT == 'RECETTE' || $this->ENVIRONNEMENT == 'REC') {
            $emails[] = 'dtang-ext@moethennessy.com';
        //}

        $dateNow = new DateTime('now');
        $timezone = new DateTimeZone('Europe/Paris');
        $dateNow->setTimezone($timezone);

        return $this->createMail(
            'MyHub - Variations de stock',
            $emails,
            'mails/notification_myhub_synch_stock_errors',
            [
              'stockVariations' => $stockVariations,
              'date' => $dateNow
            ]
        );
    }

    public function sendChallengeSynchStockErrors($stockVariation)
    {
        $emails = [];

        if ($this->ENVIRONNEMENT == 'PREPROD' || $this->ENVIRONNEMENT == 'PP' || $this->ENVIRONNEMENT == 'RECETTE' || $this->ENVIRONNEMENT == 'REC') {
            $emailContestation = MailerService::EMAILS_VEOLOG_RCT;
        }else{
            $emailContestation = MailerService::EMAILS_VEOLOG;
        }

        foreach ($emailContestation as $email) {
            $emails[] = $email;
        }

        foreach (MailerService::EMAILS_SERVICE_COURRIER as $email) {
            $emails[] = $email;
        }

        return $this->createMail(
            'HUB142 - Contestation d\'une variation de stock',
            $emails,
            'mails/veolog_challenge_synch_stock_errors',
            [
              'stockVariation' => $stockVariation,
            ]
        );
    }

    public function sendStockEntryDocuments($stockEntry, $docs)
    {
        $emails = [
            'hub142@moethennessy.com',
            'admin.pantin@veolog.fr',
            'a.dacruz@veolog.fr'
        ];

        if ($this->ENVIRONNEMENT == 'PREPROD' || $this->ENVIRONNEMENT == 'PP' || $this->ENVIRONNEMENT == 'RECETTE' || $this->ENVIRONNEMENT == 'REC') {
            foreach(MailerService::EMAILS_VEOLOG_RCT as $e){
                $emails[] = $e;
            }
        }else{
            foreach(MailerService::EMAILS_VEOLOG as $e){
                $emails[] = $e;
            }
        }

        return $this->createMail(
            'ENTREE DE STOCK - DOCUMENTS D\'ACCOMPAGNEMENT',
            $emails,
            'mails/veolog_stockentry_docs',
            [
              'stockEntry' => $stockEntry,
            ],
            [],
            null,
            'cc',
            $docs
        );
    }

    public function sendStocketEntryCreate($stockEntry)
    {
        $emails = [];
        $emails[] = $stockEntry->getUser()->getEmail();
        $emails[] = $stockEntry->getEmail();
        $hub_email = ['hub142@moethennessy.com'];
        foreach ($hub_email as $email) {
            $emails[] = $email;
        }

        return $this->createMail(
            'MyHub - Entrée de stock ' . $this->translator->trans('app.cave.service.cave_' . $stockEntry->getStock()->getCave()) . ' - Créée',
            $emails,
            'mails/notification_myhub_stock_entry_create',
            [
              'stockEntry' => $stockEntry,
            ],
            [],
            null,
            'cc'
        );
    }

    public function sendStocketEntrySend($stockEntry)
    {
        $emails = [];
        $emails[] = $stockEntry->getUser()->getEmail();
        $emails[] = $stockEntry->getEmail();
        return $this->createMail(
            'MyHub - Entrée de stock ' . $this->translator->trans('app.cave.service.cave_' . $stockEntry->getStock()->getCave()) . ' - Déclarée',
            $emails,
            'mails/notification_myhub_stock_entry_send',
            [
              'stockEntry' => $stockEntry,
            ],
            [],
            null,
            'cc'
        );
    }

    public function sendStocketEntryConfirm($stockEntry)
    {
        //dd('here');
        $emails = [];
        $emails[] = $stockEntry->getUser()->getEmail();
        $emails[] = $stockEntry->getEmail();
        $stockEntryUrl = $this->router->generate('entree_stock', [
          'id' => $stockEntry->getId(),
          'cave' => $stockEntry->getStock()->getCave()
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        return $this->createMail(
            'MyHub - Entrée de stock ' . $this->translator->trans('app.cave.service.cave_' . $stockEntry->getStock()->getCave()) . ' - En attente',
            $emails,
            'mails/notification_myhub_stock_entry_confirm',
            [
              'stockEntry' => $stockEntry,
              'stockEntryUrl' => $stockEntryUrl,
            ],
            [],
            null,
            'cc'
        );
    }

    public function sendStocketEntryUpdate($stockEntry)
    {
        $emails = [];
        $emails[] = $stockEntry->getUser()->getEmail();
        $emails[] = $stockEntry->getEmail();

        $stockEntryUrl = $this->router->generate('entree_stock', [
          'id' => $stockEntry->getId(),
          'cave' => $stockEntry->getStock()->getCave()
        ], UrlGeneratorInterface::ABSOLUTE_URL);
        return $this->createMail(
            'MyHub - Entrée de stock ' . $this->translator->trans('app.cave.service.cave_' . $stockEntry->getStock()->getCave()) . ' - Reçue',
          array_unique($emails),
            'mails/notification_myhub_stock_entry_update',
            [
              'stockEntry' => $stockEntry,
              'stockEntryUrl' => $stockEntryUrl,
            ],
            [],
            null,
            'cc'
        );
    }

    public function sendStocketEntryReject($stockEntry, $stockEntryItem, $user)
    {
        $emails = [];
        $emails[] = $stockEntry->getUser()->getEmail();
        $emails[] = $stockEntry->getEmail();
        $emails[] = $user->getEmail();

        $emailVeologContestation = MailerService::EMAILS_VEOLOG;

        if ($this->ENVIRONNEMENT == 'PREPROD' || $this->ENVIRONNEMENT == 'PP' || $this->ENVIRONNEMENT == 'RECETTE' || $this->ENVIRONNEMENT == 'REC') {
            $emailVeologContestation = MailerService::EMAILS_VEOLOG_RCT;
        }

        foreach (MailerService::EMAILS_CONTESTATION as $email) {
            $emails[] = $email;
        }

        foreach (MailerService::EMAILS_SERVICE_RUNNER as $email) {
          $emails[] = $email;
        }

        if (Project::CAVE_115 != $stockEntry->getStock()->getCave()) {
            foreach ($emailVeologContestation as $email) {
                $emails[] = $email;
            }
        }

        if (Project::CAVE_115 == $stockEntry->getStock()->getCave()) {
            $emails[] = '115@moethennessy.com';
        } else {
            $recipients = [];

            foreach ($emails as $recipient) {
                if (strtolower($recipient) != '115@moethennessy.com') {
                    $recipients[] = $recipient;
                }
            }

            $emails = $recipients;
        }

        return $this->createMail(
            'MyHub - Entrée de stock ' . $this->translator->trans('app.cave.service.cave_' . $stockEntry->getStock()->getCave()) . ' - Contestation',
            $emails,
            'mails/notification_myhub_stock_entry_reject',
            [
              'user' => $user,
              'stockEntry' => $stockEntry,
              'stockEntryItem' => $stockEntryItem
            ],
            [],
            null,
            'cc'
        );
    }

    public function sendStockEntry115PackingSlip($stockEntry, $packingSlip)
    {
        $emails = [
            '115@moethennessy.com',
            'd.raty@bouygues-es.com'
        ];

        return $this->createMail(
            $this->translator->trans('app.cave.service.cave_' . $stockEntry->getStock()->getCave()) . ' – Bordereau d\'envoi en stock #' . $stockEntry->getId(),
            $emails,
            'mails/notification_cave115_packingslip',
            [
                'stockEntry' => $stockEntry,
            ],
            [],
            null,
            'cc',
            null,
            $packingSlip
        );
    }

    /**
     * @param string $subject
     * @param mixed $addresses
     * @param string $template
     * @param array $parameters
     * @param array $subjectParameters
     * @param string|null $from
     * @param string|null $mode
     * @param null $attachment
     *
     * @return int
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function createMail(string $subject, $addresses, string $template, array $parameters = [], array $subjectParameters = [], ?string $from = null, ?string $mode = null, $attachment = null, $dynamicAttachment = null): int
    {

        if ($this->ENVIRONNEMENT == 'PREPROD' || $this->ENVIRONNEMENT == 'PP' || $this->ENVIRONNEMENT == 'RECETTE' || $this->ENVIRONNEMENT == 'REC' || $this->ENVIRONNEMENT == 'DEVELOPEMENT') {
            $emailSubject = '[ENV TEST] ' . $this->translator->trans($subject, $subjectParameters);

            /**
             * https://bugkiller.disko.fr/issues/71941
             * Remove all recipients when in PP
             */
            $ppAddresses = [
                'dtang-ext@moethennessy.com',
                'valery.ambroise@disko.fr',
                'emmanuelle.leridon@disko.fr'
            ];

            if(in_array('bmaraval@moethennessy.com', $addresses) || in_array('hub142@moethennessy.com', $addresses)){
                $ppAddresses[] = 'bmaraval@moethennessy.com';
            }

            if(in_array('a.bertolino@veolog.fr', $addresses)){
                $ppAddresses[] = 'a.bertolino@veolog.fr';
            }
            
            $addresses = $ppAddresses;
        }else{
            $emailSubject = $this->translator->trans($subject, $subjectParameters);
        }

        $message = (new \Swift_Message($emailSubject))
            ->setBody(
                $this->twig->render(
                    sprintf('%s.html.twig', $template),
                    $parameters
                ),
                'text/html'
            )
            ->addPart(
                $this->twig->render(
                    sprintf('%s.txt.twig', $template),
                    $parameters
                    ),
                'text/plain'
            );

        $debugTo   = "";
        $debugFrom = "";
        $debugCc   = "";
        $debugBcc  = "";

        if (null !== $from and '' !== $from) {
            $debugFrom .= $from;
            $message->setFrom($from);
        } else {
            $debugFrom .= $this->parameter->get('mailer_from');
            $message->setFrom($this->parameter->get('mailer_from'));
        }

        if (!empty($attachment) && !is_null($attachment)) {
            if(is_array($attachment)){
                foreach($attachment as $path){
                    $message->attach(\Swift_Attachment::fromPath($path));
                }
            }else{
                $message->attach(\Swift_Attachment::fromPath($attachment));
            }
        }

        if (!empty($dynamicAttachment) && !is_null($dynamicAttachment)) {
            // Create the attachment with your data
            $dynamicAttachment = new Swift_Attachment($dynamicAttachment, null, 'application/pdf');

            // Attach it to the message
            $message->attach($dynamicAttachment);
        }

        if (!empty($addresses)) {
            $addresses = array_filter($addresses, function ($value) {
                return !is_null($value) && $value !== '' && $value != "''";
            });
            $addresses = array_unique($addresses);

            foreach (array_keys($addresses, 'vbasse@ruinart.com', true) as $key) {
                unset($addresses[$key]);
            }

            if ($mode == null) {
                $debugTo .= implode(', ', $addresses);
                $message->setTo($addresses);
            } elseif ($mode == 'bcc') {
                $debugBcc .= implode(', ', $addresses);
                $message->setBcc($addresses);
            } elseif ($mode == 'cc') {
                $first = array_shift($addresses);

                $debugTo .= $first[0];
                $debugCc .= implode(', ', $addresses);
                $message->setTo($first);
                $message->setCc($addresses);
            } else {
                $debugTo .= implode(', ', $addresses);
                $message->setTo($addresses);
            }
        }

        $this->logger->info('Sending mail "' . $message->getSubject() . '" from : ' . $debugFrom . ' to : ' . $debugTo . ', cc : ' . $debugCc . ', bcc : ' . $debugBcc);

        return $this->mailer->send($message);
    }

    /**
     * @param $fileName
     * @return int
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendMailExportProducts($fileName)
    {
        return $this->createMail(
            'Etat des stocks Hub142',
            [MailerService::EMAILS_SERVICE_COURRIER],
            'mails/notification_export_products',
            [],
            [],
            null,
            'cc',
            $fileName
        );
    }
}
