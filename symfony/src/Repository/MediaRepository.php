<?php

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class MediaRepository
 *
 * @package App\Repository
 */
class MediaRepository extends BaseRepository
{

    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('m')
            ->select('m')
            ->leftJoin('m.translations', 'mt')
            ->orderBy('m.created', 'desc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                if ($key == 'search') {
                    $qb->andWhere('(mt.legend LIKE :legend OR m.size LIKE :size OR mt.alt LIKE :alt OR m.pathMedia LIKE :pathMedia OR m.mimetype LIKE :mimetype OR mt.title LIKE :title)');
                    $qb->setParameter('pathMedia', '%'.$filter.'%');
                    $qb->setParameter('mimetype', '%'.$filter.'%');
                    $qb->setParameter('title', '%'.$filter.'%');
                    $qb->setParameter('legend', '%'.$filter.'%');
                    $qb->setParameter('size', '%'.$filter.'%');
                    $qb->setParameter('alt', '%'.$filter.'%');
                }


                if ($key == 'accepted') {
                    if ($filter == 'images/*') {
                        $qb->andWhere('(m.mimetype LIKE :png OR m.mimetype LIKE :gif OR m.mimetype LIKE :jpg OR m.mimetype LIKE :jpeg)');
                        $qb->setParameter('png', '%png%');
                        $qb->setParameter('gif', '%gif%');
                        $qb->setParameter('jpg', '%jpg%');
                        $qb->setParameter('jpeg', '%jpeg%');
                    } else {
                        $qb->andWhere('m.mimetype LIKE :accepted');
                        $qb->setParameter('accepted', '%'.$filter.'%');
                    }
                }
            }
        }

        return $qb->getQuery();
    }


    public function findByIds($ids)
    {
        $qb = $this->createQueryBuilder('m')
            ->select('m');

        if (empty($ids)) {
            $ids = [0];
        }
        $qb->add('where', $qb->expr()->in('m.id', $ids));

        $query = $qb->getQuery();

        return $query->getResult();
    }



    /**
     * Count all
     */
    public function countAll()
    {
        $qb = $this->createQueryBuilder('e')
            ->select('COUNT(e.id) as cpt');

        $query = $qb->getQuery();

        return $query->getScalarResult()[0]['cpt'];
    }
}
