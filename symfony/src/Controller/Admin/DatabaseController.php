<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Form\Type\User\EmailTestSendType;
use App\Controller\BaseController;
use App\Utils\Managers\UserManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\Model\EmailTestSend;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Process\Process;

/**
 * Class DetectorController
 * @Route("/admin/database")
 * @package App\Controller
 */
class DatabaseController extends BaseController
{
    /**
     * Database detect changement
     *
     * @Route("/detect", name="admin_database_detect_dump")
     * @param Request $request Request
     */
    public function detect(Request $request)
    {

        // Update DB
        $pathRootDir = $this->getParameter('kernel.root_dir');
        $process = new Process(
            'php bin/console doctrine:schema:update --dump-sql',
            $pathRootDir.'/../'
        );
        $process->run();

        $output = $process->getOutput();

        $canexec = preg_match('[OK]', $output) != 1;
        $secure = $request->get('secure', false);

        if (!$canexec && $secure) {
            return $this->redirectToRoute("admin_homepage");
        }

        // View
        return $this->render(
            '@Admin/Database/detect.html.twig',
            [
                'canexec' => $canexec,
                'secure' => $request->get('secure', false),
                'output' => $output
            ]
        );
    }


    /**
     * Database detect changement
     *
     * @Route("/execute", name="admin_database_force_update")
     * @param Request $request Request
     */
    public function force(Request $request)
    {
        // Update DB
        $pathRootDir = $this->getParameter('kernel.root_dir');
        $process = new Process(
            'php bin/console doctrine:schema:update --force',
            $pathRootDir.'/../'
        );
        $process->run();


        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "La base de données est à jour"
        );


        return $this->redirect($request->headers->get('referer'));
    }
}
