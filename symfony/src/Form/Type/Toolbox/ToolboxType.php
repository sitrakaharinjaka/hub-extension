<?php

/**
 * Form type
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Type\Toolbox;

use A2lix\TranslationFormBundle\Form\Type\TranslationsFormsType;
use App\Entity\Toolbox;
use App\Utils\Managers\LocaleManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 *  Form Type
 */
class ToolboxType extends AbstractType
{
    protected $localeManager;

    /**
     * @param LocaleManager $localeManager
     */
    public function __construct(LocaleManager $localeManager)
    {
        $this->localeManager = $localeManager;
    }


    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $defaultLocales = $this->localeManager->findDefaultSlug();

        $builder
            ->add('translations', TranslationsFormsType::class, [
                'label' => false,
                'default_locale' => $defaultLocales,
                'required_locales' => $defaultLocales,
                'locales' => $this->localeManager->findAllSlug(),
                'form_type' => ToolboxTranslationType::class
            ]);
    }

    /**
     * Configure Options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
            'data_class' => Toolbox::class
        ));
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'toolbox';
    }
}
