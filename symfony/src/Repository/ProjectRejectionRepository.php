<?php

namespace App\Repository;

use App\Entity\ProjectRejection;
use App\Entity\Project;
use App\Repository\BaseRepository;

class ProjectRejectionRepository extends BaseRepository
{

    public function clearByProject(Project $project)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete(ProjectRejection::class, 'r');
        $qb->andWhere('r.project = :project');
        $qb->setParameter('project', $project);

        return $qb->getQuery()->execute();
    }
}
