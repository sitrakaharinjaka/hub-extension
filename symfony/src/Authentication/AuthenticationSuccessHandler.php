<?php
/**
 * Created by PhpStorm.
 * User: lsimonin
 * Date: 2018-12-17
 * Time: 16:32
 */

namespace App\Authentication;

use App\Entity\User;
use LightSaml\SpBundle\Security\Authentication\Token\SamlSpToken;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Http\HttpUtils;

class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * AuthenticationSuccessHandler constructor.
     *
     * @param ContainerInterface $container
     * @param HttpUtils          $httpUtils
     * @param array              $options
     */
    public function __construct(ContainerInterface $container, HttpUtils $httpUtils, array $options = array())
    {
        parent::__construct($httpUtils, $options);
        $this->container = $container;
    }


    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $user = $token->getUser();
        if ($user->hasRole("ROLE_ADMIN")) {
            return new RedirectResponse($this->container->get('router')->generate('admin_homepage'));
        }

        return parent::onAuthenticationSuccess($request, $token);
    }
}
