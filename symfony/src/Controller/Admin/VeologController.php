<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\Type\User\EmailTestSendType;
use App\Controller\BaseController;
use App\Utils\Managers\UserManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\Model\EmailTestSend;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Process\Process;

/**
 * @Route("/admin/veolog")
 * @package App\Controller
 */
class VeologController extends AbstractController
{

  /**
   * Veolog order ask
   *
   * @Route("/order-ask", name="admin_veolog_order_ask")
   * @param Request $request Request
   */
    public function order_ask(Request $request)
    {

    // Update DB
        $pathRootDir = $this->getParameter('kernel.root_dir');
        $process = new Process(
      'php bin/console veolog:order-ask',
      $pathRootDir.'/../'
    );
        $process->run();

        $output = $process->getOutput();

        $secure = $request->get('secure', false);

        // View
        return $this->render(
      '@Admin/Veolog/order-ask.html.twig',
      [
        'secure' => $request->get('secure', false),
        'output' => $output
      ]
    );
    }

    /**
     * Veolog order-confirm
     *
     * @Route("/order-confirm", name="admin_veolog_order_confirm")
     * @param Request $request Request
     */
    public function order_confirm(Request $request)
    {

    // Update DB
        $pathRootDir = $this->getParameter('kernel.root_dir');
        $process = new Process(
      'php bin/console veolog:order-confirm',
      $pathRootDir.'/../'
    );
        $process->run();

        $output = $process->getOutput();

        $secure = $request->get('secure', false);

        // View
        return $this->render(
      '@Admin/Veolog/order-confirm.html.twig',
      [
        'secure' => $request->get('secure', false),
        'output' => $output
      ]
    );
    }

    /**
     * Veolog stock-update
     *
     * @Route("/stock-update", name="admin_veolog_stock_update")
     * @param Request $request Request
     */
    public function stock_update(Request $request)
    {

    // Update DB
        $pathRootDir = $this->getParameter('kernel.root_dir');
        $process = new Process(
      'php bin/console veolog:stock-update',
      $pathRootDir.'/../'
    );
        $process->run();

        $output = $process->getOutput();

        $secure = $request->get('secure', false);

        // View
        return $this->render(
      '@Admin/Veolog/stock-update.html.twig',
      [
        'secure' => $request->get('secure', false),
        'output' => $output
      ]
    );
    }

    /**
     * Veolog synch-product
     *
     * @Route("/synch-product", name="admin_veolog_synch_product")
     * @param Request $request Request
     */
    public function synch_product(Request $request)
    {

    // Update DB
        $pathRootDir = $this->getParameter('kernel.root_dir');
        $process = new Process(
      'php bin/console veolog:synch-product',
      $pathRootDir.'/../'
    );
        $process->run();

        $output = $process->getOutput();

        $secure = $request->get('secure', false);

        // View
        return $this->render(
      '@Admin/Veolog/synch-product.html.twig',
      [
        'secure' => $request->get('secure', false),
        'output' => $output
      ]
    );
    }

    /**
     * Veolog check-integration
     *
     * @Route("/check-integration", name="admin_veolog_check_integration")
     * @param Request $request Request
     */
    public function check_integration(Request $request)
    {

    // Update DB
        $pathRootDir = $this->getParameter('kernel.root_dir');
        $process = new Process(
      'php bin/console veolog:check-integration',
      $pathRootDir.'/../'
    );
        $process->run();

        $output = $process->getOutput();

        $secure = $request->get('secure', false);

        // View
        return $this->render(
      '@Admin/Veolog/check-integration.html.twig',
      [
        'secure' => $request->get('secure', false),
        'output' => $output
      ]
    );
    }

    /**
     * Veolog check-projects
     *
     * @Route("/check-projects", name="admin_veolog_check_projects")
     * @param Request $request Request
     */
    public function check_projects(Request $request)
    {

    // Update DB
        $pathRootDir = $this->getParameter('kernel.root_dir');
        $process = new Process(
      'php bin/console disko:check:projects',
      $pathRootDir.'/../'
    );
        $process->run();

        $output = $process->getOutput();

        $secure = $request->get('secure', false);

        // View
        return $this->render(
      '@Admin/Veolog/check-projects.html.twig',
      [
        'secure' => $request->get('secure', false),
        'output' => $output
      ]
    );
    }

    /**
     * Veolog check-projects
     *
     * @Route("/import-product-sap", name="admin_import_product_sap")
     * @param Request $request Request
     */
    public function import_product_sap(Request $request)
    {

    // Update DB
        $pathRootDir = $this->getParameter('kernel.root_dir');
        $process = new Process(
      'php bin/console disko:import:product',
      $pathRootDir.'/../'
    );
        $process->run();

        $output = $process->getOutput();

        $secure = $request->get('secure', false);

        // View
        return $this->render(
      '@Admin/Veolog/import-product-sap.html.twig',
      [
        'secure' => $request->get('secure', false),
        'output' => $output
      ]
    );
    }
}
