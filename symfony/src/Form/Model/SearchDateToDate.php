<?php

/**
 * Model for search
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Model;

/**
 * Class SearchDateToDate
 *
 * @package App\Form\Model
 */
class SearchDateToDate
{
    protected $start;
    protected $end;

    /**
     * @return mixed
     */
    public function getStart()
    {
        if (!empty($this->end) && $this->end < $this->start) {
            $this->start = $this->end;
        }

        return $this->start;
    }

    /**
     * @param mixed $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return mixed
     */
    public function getEnd()
    {
        if (!empty($this->start) && $this->start > $this->end) {
            $this->end = $this->start;
        }

        return $this->end;
    }

    /**
     * @param mixed $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * Get search data
     *
     * @return array
     */
    public function getSearchData()
    {
        $tab = array();

        if (!empty($this->start)) {
            $tab['start'] = $this->start;
        }
        if (!empty($this->end)) {
            $tab['end'] = $this->end;
        }

        return $tab;
    }
}
