<?php


namespace App\Form\Type\StockEntry;


use App\Form\Model\SearchStockEntry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use App\Entity\Stock;
use App\Entity\StockEntry;

/**
 * Class SearchStockEntryType
 * @package App\Form\Type\StockEntry
 */
class SearchStockEntryType extends AbstractType
{
    /**
     * @param FormBuilderInterface<mixed> $builder
     * @param array<string, mixed>        $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', IntegerType::class, [
                'required' => false,
                'empty_data' => '',
            ])
            ->add('stock', EntityType::class, [
                'class' => Stock::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                    ->leftJoin('u.translations', 't', 'WITH', 't.locale = :locale')
                    ->andWhere('u.cave =:cave')
                    ->setParameter('locale', 'fr')
                    ->setParameter('cave', 'pantin')
                    ->orderBy('t.name', 'ASC');
                },
                'choice_label' => 'name',
                'multiple' =>false,
                'required'  => false
            ])
            /*->add('stock', ChoiceType::class,
              [
                'required' => false,
                'label' => 'Stock',
                'choices' => StockConstant::_STOCK_,
                'multiple' => false,
                'expanded' => false,
                'placeholder' => ':. Choix stock .:',
              ])*/
            ->add('requester', TextType::class, [
                'required' => false
            ])
            ->add('code', TextType::class, [
                'required' => false
            ])
            ->add('status', ChoiceType::class,
                array(
                'choices'  => array(
                    'En attente de traitement' => StockEntry::STATE_ITEM_CREATE,
                    'Contestation en cours' => StockEntry::STATE_ITEM_REJECT,
                    'Réceptionnée chez Veolog' => StockEntry::STATE_ITEM_UPDATE,
                    'Entrée de stock validée' => StockEntry::STATE_ITEM_VALIDATE,
                    'En attente de réception produits' => StockEntry::STATE_INT_VEOLOG,
                    'Rejetée par Veolog' => StockEntry::STATE_REJECT_VEOLOG,

                ),
                'required' => false)
            )
            ->add('createdAt', DateType::class, array(
                    'required' => false,
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'format' => 'dd/MM/y',
                    'empty_data' => '',
                    )
                )
            ->add('delivery', DateType::class, array(
                    'required' => false,
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'format' => 'dd/MM/y',
                    'empty_data' => '',
                    )
                )
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SearchStockEntry::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'search';
    }
}
