<?php

/**
 * Repository
 *
 * @author Pierre Baumes <pierre.b@disko.fr>
 */

namespace App\Repository;

use App\Repository\BaseRepository;

/**
 * Class ContactTranslationRepository
 *
 * @package App\Repository
 */
class ContactTranslationRepository extends BaseRepository
{
}
