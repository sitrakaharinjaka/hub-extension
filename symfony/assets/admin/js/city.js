var lastSelectedFile = null;
var currentDropZoneSelect = null;
$( document ).ready(function() {

    if($('#towns').length > 0){
        let urlTown = $('#towns').data('url');
        let codeTown = $('#towns').data('code');
        let listPageTown = $('#towns').data('listing');
        let inProgressTown = false;
        let xhrTown;

        if (inProgressTown) {
            xhrTown.abort();
            inProgressTown = false;
        }

        inProgressTown = true;
        xhrTown = $.ajax({
            url: urlTown,
            method: 'POST',
            data: {code: codeTown, listPage: listPageTown},
            beforeSend: function(){
                $('#loader-town').show();
            },
            success: function(data){
                $('#loader-town').hide();
                $('#paginator-towns').html(data.paginator)
            }
        })

        $('body').on('click', 'div#towns li.page', function(e){
            e.preventDefault();

            let urlTown = $(this).find('a').attr('href');
            inProgressTown = true;

            xhrTown = $.ajax({
                url: urlTown,
                method: 'POST',
                data: {code: codeTown, listPage: listPageTown},
                beforeSend: function () {
                    $('#loader-town').show();
                    $('#paginator-towns').hide();
                },
                success: function (data) {
                    $('#loader-town').hide();
                    $('#paginator-towns').html(data.paginator);
                    $('#paginator-towns').show();
                }
            })
        });

        $('body').on('click', 'div#towns li.previous', function(e){
            e.preventDefault();

            let urlTown = $(this).find('a').attr('href');
            inProgressTown = true;

            xhrTown = $.ajax({
                url: urlTown,
                method: 'POST',
                data: {code: codeTown, listPage: listPageTown},
                beforeSend: function () {
                    $('#loader-town').show();
                    $('#paginator-towns').hide();
                },
                success: function (data) {
                    $('#loader-town').hide();
                    $('#paginator-towns').html(data.paginator);
                    $('#paginator-towns').show();
                }
            })
        });

        $('body').on('click', 'div#towns li.first', function(e){
            e.preventDefault();

            let urlTown = $(this).find('a').attr('href');
            inProgressTown = true;

            xhrTown = $.ajax({
                url: urlTown,
                method: 'POST',
                data: {code: codeTown, listPage: listPageTown},
                beforeSend: function () {
                    $('#loader-town').show();
                    $('#paginator-towns').hide();
                },
                success: function (data) {
                    $('#loader-town').hide();
                    $('#paginator-towns').html(data.paginator);
                    $('#paginator-towns').show();
                }
            })
        });

        $('body').on('click', 'div#towns li.next', function(e){
            e.preventDefault();

            let urlTown = $(this).find('a').attr('href');
            inProgressTown = true;

            xhrTown = $.ajax({
                url: urlTown,
                method: 'POST',
                data: {code: codeTown, listPage: listPageTown},
                beforeSend: function () {
                    $('#loader-town').show();
                    $('#paginator-towns').hide();
                },
                success: function (data) {
                    $('#loader-town').hide();
                    $('#paginator-towns').html(data.paginator);
                    $('#paginator-towns').show();
                }
            })
        });

        $('body').on('click', 'div#towns li.last', function(e){
            e.preventDefault();

            let urlTown = $(this).find('a').attr('href');
            inProgressTown = true;

            xhrTown = $.ajax({
                url: urlTown,
                method: 'POST',
                data: {code: codeTown, listPage: listPageTown},
                beforeSend: function () {
                    $('#loader-town').show();
                    $('#paginator-towns').hide();
                },
                success: function (data) {
                    $('#loader-town').hide();
                    $('#paginator-towns').html(data.paginator);
                    $('#paginator-towns').show();
                }
            })
        });

        $('body').on('click', 'div#towns li.sortable', function(e) {
            e.preventDefault();

            let urlTown = $(this).find('a').attr('href');
            inProgressTown = true;

            xhrTown = $.ajax({
                url: urlTown,
                method: 'POST',
                data: {code: codeTown, listPage: listPageTown},
                beforeSend: function () {
                    $('#loader-town').show();
                    $('#paginator-towns').hide();
                },
                success: function (data) {
                    $('#loader-town').hide();
                    $('#paginator-towns').html(data.paginator);
                    $('#paginator-towns').show();
                }
            })
        });
    }

    if($('#citys').length > 0){

        let url = $('#citys').data('url');
        let code = $('#citys').data('code');
        let listPage = $('#citys').data('listing');
        let inProgress = false;
        let xhr;

        if(inProgress) {
            xhr.abort();
            inProgress = false;
        }

        inProgress = true;
        xhr = $.ajax({
            url: url,
            method: 'POST',
            data: {code: code, listPage: listPage},
            beforeSend: function(){
                $('#loader-city').show();
            },
            success: function(data){
                $('#loader-city').hide();
                $('#paginator-citys').html(data.paginator);
            }
        });

        $('body').on('click', 'div#citys li.page', function(e){
            e.preventDefault();

            let url = $(this).find('a').attr('href');
            inProgress = true;
            xhr = $.ajax({
                url: url,
                method: 'POST',
                data: {code: code, listPage: listPage},
                beforeSend: function(){
                    $('#loader-city').show();
                    $('#paginator-citys').hide();
                },
                success: function(data){
                    $('#loader-city').hide();
                    $('#paginator-citys').html(data.paginator);
                    $('#paginator-citys').show();
                }
            })
        });

        $('body').on('click', 'div#citys li.previous', function(e){
            e.preventDefault();

            let url = $(this).find('a').attr('href');
            inProgress = true;
            xhr = $.ajax({
                url: url,
                method: 'POST',
                data: {code: code, listPage: listPage},
                beforeSend: function(){
                    $('#loader-city').show();
                    $('#paginator-citys').hide();
                },
                success: function(data){
                    $('#loader-city').hide();
                    $('#paginator-citys').html(data.paginator);
                    $('#paginator-citys').show();
                }
            })
        });

        $('body').on('click', 'div#citys li.first', function(e){
            e.preventDefault();

            let url = $(this).find('a').attr('href');
            inProgress = true;
            xhr = $.ajax({
                url: url,
                method: 'POST',
                data: {code: code, listPage: listPage},
                beforeSend: function(){
                    $('#loader-city').show();
                    $('#paginator-citys').hide();
                },
                success: function(data){
                    $('#loader-city').hide();
                    $('#paginator-citys').html(data.paginator);
                    $('#paginator-citys').show();
                }
            })
        });

        $('body').on('click', 'div#citys li.next', function(e){
            e.preventDefault();

            let url = $(this).find('a').attr('href');
            inProgress = true;
            xhr = $.ajax({
                url: url,
                method: 'POST',
                data: {code: code, listPage: listPage},
                beforeSend: function(){
                    $('#loader-city').show();
                    $('#paginator-citys').hide();
                },
                success: function(data){
                    $('#loader-city').hide();
                    $('#paginator-citys').html(data.paginator);
                    $('#paginator-citys').show();
                }
            })
        });

        $('body').on('click', 'div#citys li.last', function(e){
            e.preventDefault();

            let url = $(this).find('a').attr('href');
            inProgress = true;

            xhr = $.ajax({
                url: url,
                method: 'POST',
                data: {code: code, listPage: listPage},
                beforeSend: function(){
                    $('#loader-city').show();
                    $('#paginator-citys').hide();
                },
                success: function(data){
                    $('#loader-city').hide();
                    $('#paginator-citys').html(data.paginator);
                    $('#paginator-citys').show();
                }
            })
        });

        $('body').on('click', 'div#citys li.sortable', function(e){
            e.preventDefault();

            let url = $(this).find('a').attr('href');
            inProgress = true;

            xhr = $.ajax({
                url: url,
                method: 'POST',
                data: {code: code, listPage: listPage},
                beforeSend: function(){
                    $('#loader-city').show();
                    $('#paginator-citys').hide();
                },
                success: function(data){
                    $('#loader-city').hide();
                    $('#paginator-citys').html(data.paginator);
                    $('#paginator-citys').show();
                }
            })
        });
    }
});

