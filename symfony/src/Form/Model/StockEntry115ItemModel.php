<?php

namespace App\Form\Model;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

class StockEntry115ItemModel
{
    /**
     * Product Id
     */
    protected $productId;

    /**
     * Quantity
     */
    protected $quantity;

    /**
     * Product name
     */
    protected $productName;

    /**
     * Category
     */
    protected $productCategory;

    /**
     * Brand
     */
    protected $productBrand;

    /**
     * Product Code
     */
    protected $productCode;

    /**
     * Product EAN
     */
    protected $productEan;

    /**
     * Product State
     */
    protected $productState;

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param mixed $productName
     */
    public function setProductName($productName): void
    {
        $this->productName = $productName;
    }

    /**
     * @return mixed
     */
    public function getProductCategory()
    {
        return $this->productCategory;
    }

    /**
     * @param mixed $productCategory
     */
    public function setProductCategory($productCategory): void
    {
        $this->productCategory = $productCategory;
    }

    /**
     * @return mixed
     */
    public function getProductBrand()
    {
        return $this->productBrand;
    }

    /**
     * @param mixed $productBrand
     */
    public function setProductBrand($productBrand): void
    {
        $this->productBrand = $productBrand;
    }

    /**
     * @return mixed
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * @param mixed $productCode
     */
    public function setProductCode($productCode): void
    {
        $this->productCode = $productCode;
    }

    /**
     * @return mixed
     */
    public function getProductEan()
    {
        return $this->productEan;
    }

    /**
     * @param mixed $productEan
     */
    public function setProductEan($productEan): void
    {
        $this->productEan = $productEan;
    }

    /**
     * Get product State
     */ 
    public function getProductState()
    {
        return $this->productState;
    }

    /**
     * Set product State
     *
     * @return  self
     */ 
    public function setProductState($productState)
    {
        $this->productState = $productState;

        return $this;
    }
}
