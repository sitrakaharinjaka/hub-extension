<?php
/**
 * Created by PhpStorm.
 * User: lsimonin
 * Date: 26/04/2018
 * Time: 17:12
 */
namespace App\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HennessyExtension extends AbstractTypeExtension
{
    public static function getExtendedTypes(): iterable
    {
        return array(FormType::class);
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['form_time'] = $options['form_time'];
        $view->vars['form_date'] = $options['form_date'];
        $view->vars['form_icon'] = $options['form_icon'];
        $view->vars['form_md'] = $options['form_md'];
        $view->vars['form_focus_label'] = $options['form_focus_label'];
        $view->vars['form_checkbox'] = $options['form_checkbox'];
        $view->vars['form_checkbox_white'] = $options['form_checkbox_white'];
        $view->vars['form_textarea'] = $options['form_textarea'];
        $view->vars['readonly'] = $options['readonly'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('form_time', false);
        $resolver->setDefault('form_date', false);
        $resolver->setDefault('form_icon', false);
        $resolver->setDefault('form_md', false);
        $resolver->setDefault('form_focus_label', true);
        $resolver->setDefault('form_checkbox', false);
        $resolver->setDefault('form_checkbox_white', false);
        $resolver->setDefault('form_textarea', false);
        $resolver->setDefault('readonly', false);
    }
}
