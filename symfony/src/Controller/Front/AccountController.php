<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Front;

use App\Controller\BaseController;
use App\Entity\Project;
use App\Form\Model\ProjectSwitch;
use App\Form\Type\Project\ProjectSwitchType;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockEntry115Manager;
use App\Utils\Managers\StockEntryManager;
use App\Utils\Managers\StockManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AccountController
 *
 * @Route("/projets")
 * @package App\Controller
 */
class AccountController extends BaseController
{

    /**
     * @Route(path="/dashboard", name="account", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function projects(ProjectManager $projectManager)
    {
        $messageCart = $this->get('session')->get('flash_project');
        if (null !== $messageCart) {
            $this->get('session')->set('flash_project', null);
        }

        $user = $this->getUser();

        return $this->render('hub/account/dashboard.html.twig', [
            'message_cart' => $messageCart,
            'projects' => $projectManager->findProjectAvailableByUser($user, $this->get('session')->get('current_stock')),
        ]);
    }

    /**
     * @Route(path="/ajaxStatus/", name="dashboard_project_ajax_status")
     *
     * @param Request          $request
     * @param ProjectManager $projectManager
     *
     * @return Response
     */
    public function AjaxProjectStatus(Request $request, ProjectManager $projectManager)
    {
      if ($request->isXmlHttpRequest() && 'POST' === $request->getMethod()) {

        $user = $this->getUser();
        $projects = $request->request->get('data');

        $items = $projectManager->findProjectStateByUser($user, $this->get('session')->get('current_stock'), $projects);

        return new JsonResponse($items, 200);
      }

      return new JsonResponse(array('code' => 500));
    }

    /**
     * @Route(path="/ajaxStockToValidate/", name="dashboard_stock_button_ajax_validate")
     *
     * @param Request          $request
     * @param ProjectManager $projectManager
     *
     * @return Response
     */
  public function AjaxStockToValidateBadge(Request $request, ProjectManager $projectManager, StockEntryManager $stockEntryManager, StockEntry115Manager $stockEntry115Manager)
    {
      if ($request->isXmlHttpRequest() && 'POST' === $request->getMethod()) {

        $user = $this->getUser();

        $stock = $this->get('session')->get('current_stock');

        if ($stock->getCave() == Project::CAVE_PANTIN) {
          $stock_badge_number = 0;
          $stockEntries = $stockEntryManager->findBy(['stock' => $stock->getId()]);
          foreach ($stockEntries as $stockEntry) {
            if ($stockEntry->awaitingValidation()) {
              $stock_badge_number++;
            }
          }

        return new JsonResponse($stock_badge_number, 200);
      } else if ($stock->getCave() == Project::CAVE_115) {

        $stock_badge_number = 0;
        $stockEntries = $stockEntry115Manager->findBy(['stock' => $stock->getId()]);
        foreach ($stockEntries as $stockEntry) {
          if ($stockEntry->awaitingValidation()) {
            $stock_badge_number++;
          }
        }

        return new JsonResponse($stock_badge_number, 200);
      }

      }

      return new JsonResponse(array('code' => 500));
    }
}
