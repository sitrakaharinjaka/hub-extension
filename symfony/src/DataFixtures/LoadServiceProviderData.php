<?php

namespace App\DataFixtures;

use App\Entity\ServiceProvider;
use App\Entity\ServiceProviderTranslation;
use App\Utils\Managers\LocaleManager;
use App\Utils\Managers\ServiceProviderManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadServiceProviderData
 *
 * @package Disko\UserBundle\DataFixtures\ORM
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class LoadServiceProviderData extends Fixture implements DependentFixtureInterface
{
    private $serviceProviderManager;
    private $localeManager;

    public function __construct(ServiceProviderManager $serviceProviderManager, LocaleManager $localeManager)
    {
        $this->serviceProviderManager = $serviceProviderManager;
        $this->localeManager = $localeManager;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
//        $servicesProvider = array(
//            [
//              'name' => 'Nettoyage SARL',
//              'firstNameContact' => 'Guy',
//              'lastNameContact' => 'Tarembois',
//              'phone' => '0444444444',
//              'email' => 'contact.'.rand(1, 1000).'@disko.dev',
//              'street' => 'Rue des étoiles',
//              'zipCode' => '34000',
//              'city' => 'Montpellier',
//            ],
//            [
//              'name' => 'SA Livraison',
//              'firstNameContact' => 'Alex',
//              'lastNameContact' => 'Terrieur',
//              'phone' => '0444444444',
//              'email' => 'contact.'.rand(1, 1000).'@disko.dev',
//              'street' => 'Rue des planètes',
//              'zipCode' => '75000',
//              'city' => 'Paris',
//            ],
//            [
//              'name' => 'Réparation',
//              'firstNameContact' => 'Anne',
//              'lastNameContact' => 'Honime',
//              'phone' => '0444444444',
//              'email' => 'contact.'.rand(1, 1000).'@disko.dev',
//              'street' => 'Rue des galaxies',
//              'zipCode' => '78000',
//              'city' => 'Versailles',
//            ]
//        );
//
//        $locales = $this->localeManager->findAll();
//
//        foreach ($servicesProvider as $sp) {
//            $obj = new ServiceProvider();
//            foreach ($locales as $locale) {
//                $trans = new ServiceProviderTranslation();
//                $trans->setLocale($locale->getSlug());
//                $trans->setName($sp['name']);
//                $trans->setFirstNameContact($sp['firstNameContact']);
//                $trans->setLastNameContact($sp['lastNameContact']);
//                $trans->setPhone($sp['phone']);
//                $trans->setEmail($sp['email']);
//                $trans->setStreet($sp['street']);
//                $trans->setZipCode($sp['zipCode']);
//                $trans->setCity($sp['city']);
//                $obj->addTranslation($trans);
//            }
//            $this->serviceProviderManager->save($obj);
//            $this->addReference('admin-service-provider-'.$obj->getId(), $obj);
//        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

    public function getDependencies()
    {
        return [
          LoadLocaleData::class
      ];
    }
}
