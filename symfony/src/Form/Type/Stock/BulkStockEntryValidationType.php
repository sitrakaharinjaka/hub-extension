<?php

/**
 * Form type
 *
 * @author Disko
 */

namespace App\Form\Type\Stock;

use App\Form\Model\BulkStockEntryValidation;
use App\Entity\StockEntryItem;
use App\Utils\Managers\LocaleManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\Fo;

/**
 *  Form Type
 */
class BulkStockEntryValidationType extends AbstractType
{

    /**
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('stockEntryItems', CollectionType::class, array(
            'entry_type'    => StockEntryItemType::class,
            'entry_options' => ['label' => false],
            'allow_delete'  => true,
            'allow_add'     => true,
            'required'      => true,
            'by_reference'  => false,
            'label'         => false,
            'error_bubbling' => false,
        ));
    }

    /**
     * Configure Options
     *
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
            'data_class'         => BulkStockEntryValidation::class
        ));
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bulkStockEntryValidation';
    }
}
