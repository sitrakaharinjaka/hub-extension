<?php

namespace App\Command\Veolog;

use App\Command\Veolog\AbstractVeologCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Utils\Managers\StockEntryManager;
use App\Utils\Managers\ProductManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\HttpKernel\KernelInterface;
use DateTime;
use DateTimeZone;

class SynchStockEntryCommand extends AbstractVeologCommand
{
    private const ALTERNATE_EMAIL = 'hub142@moethennessy.com';

    private const PRODUCT_CREATION_DELAY = '-30 minutes';

    private $stockEntryManager;
    private $productManager;

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, StockEntryManager $stockEntryManager, ProductManager $productManager, KernelInterface $kernel)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->stockEntryManager    = $stockEntryManager;
        $this->productManager       = $productManager;
        $this->rootDir              = $kernel->getProjectDir();
    }

    protected static $defaultName = 'veolog:stock-entries-update';

    protected function configure()
    {
        $this
            ->setDescription('Synch veolog stock entries')
            ->addOption(
                'vlgenv',
                null,
                InputOption::VALUE_OPTIONAL,
                'Env veolog prod?',
                false
            )
            ->setHelp('This command allows you to synch veolog stock entries');
    }

    private function createFilename($index)
    {
        // recept_AAMMJJHHMMSS.csv
        $currentDate = date("ymdhis");
        // $index à la place des secondes.
        $index = $index < 10 ? '0' . $index : $index;
        return 'recept_' . $currentDate . $index . '.csv';
    }

    private function getFullFilename($filename)
    {
        return $this->rootDir . '/veolog/' . $this->getDirVlgIN() . '/' . $filename;
    }

    private function getFTPFilename($filename)
    {
        return '/' . $this->getDirVlgIN() . '/' . $filename;
    }

    private function generateLocalFile($stockEntry, $filename)
    {
        $fullfilename = $this->getFullFilename($filename);
        $this->log('info', 'Creating file : ' . $fullfilename);
        $fp = fopen($fullfilename, "w");
        fputcsv($fp, $this->createLineE($stockEntry), ';');
        $nbLines = 0;
        $qty = 0;
        foreach ($stockEntry->getStockEntryItems() as $item) {
            fputcsv($fp, $this->createLineL($item, $qty), ';');
            $nbLines++;
        }
        fputcsv($fp, $this->createLineF($nbLines, $qty), ';');
        fclose($fp);
    }

    /**
     *
     *   1   Type ligne                      A1  X   E
     *   2   Entité de stocks                A15 X   Ex : RNTFR
     *   3   Réf. réception donneur d’ordre  A15 X   Référence 1 de la réception. ID unique et incrémental
     *   4   Réf. Réception fournisseur      A15 X   Ne pas renseigner par Disko
     *   5   Centre de coût                  A15 X
     *   6   Code fournisseur                A10 X   En dur : MHS
     *   7   Date prévue de réception        N8      Format figé par exemple AAAAMMJJ
     *   8   Heure prévue de réception       N4      Format figé par exemple HHMM
     *   9   Commentaire                     A250
     *   10  E-mail user                     A50 X   Info pour Veolog uniquement : stocker dans trans_e Blueway
    */
    private function createLineE($stockEntry)
    {
        $this->log('info', 'Creating line E');
        $array = [];
        $array[] = 'E';
        $array[] = $stockEntry->getStock()->translate('fr')->getCode();
        $array[] = $this->getStockEntryRef($stockEntry);
        $array[] = '';
        if (null !== $stockEntry->getBillingCenter()) {
            $array[] = strtoupper($stockEntry->getBillingCenter()->getName());
        }
        $array[] = 'MHS';
        /*
         * On va mettre la date du jour de réception de l'EDI
         * La date est rarement "fiable" au niveau utilisateur
         * Il vaut mieux partir du principe que l'EDI reçu fait foi avec sa date
         */
        $array[] = ''; // Date prévue : laisser vide
        $array[] = ''; // Heure prévue : laisser vide
        $services = '';
        foreach ($stockEntry->getStockEntryItems() as $item) {
            if ($item->getWashBasins()) {
                $services .= 'R02,';
            }
            if ($item->getDryCleaning()) {
                $services .= 'R03,';
            }
            if ($item->getWashGlasses()) {
                $services .= 'R01,';
            }
            $array[]= $services;
        }
        $array[] = strlen($stockEntry->getEmail()) < 50 ? $stockEntry->getEmail() : self::ALTERNATE_EMAIL;
        return str_replace('"', '', $array);
    }

    /**
     * 1    Type ligne      A1  X L
     * 2    Code article    A30 X
     * 3    Libellé article A30
     * 4    Quantité        N   X
     * 5    Code blocage    A15 X 'VIDE' = bon état ; 'MVS' = Mauvais
     */
    private function createLineL($item, &$qty)
    {
        $this->log('info', 'Creating line L');
        $array = [];
        $array[] = 'L';
        $array[] = strtoupper($item->getProduct()->translate('fr')->getCode());
        $array[] = strtoupper($item->getProduct()->translate('fr')->getName());
        $array[] = $item->getQuantity();
        $qty+= $item->getQuantity();
        $array[] = ''; // On met "Bon état", c'est VEOLOG qui juge le "mauvais état"
        return str_replace('"', '', $array);
    }

    /**
     * 1 Type ligne                     A1  X F
     * 2 Nombre de ligne de type L      N   X
     * 3 Quantité totale de la commande N   X
     */
    private function createLineF($nbLines, $qty)
    {
        $this->log('info', 'Creating line F');
        $array = [];
        $array[] = 'F';
        $array[] = $nbLines;
        $array[] = $qty;
        return str_replace('"', '', $array);
    }

    private function sendFTP($stockEntry, $filename)
    {
        $fullfilename = $this->getFullFilename($filename);
        $ftpfilename = $this->getFTPFilename($filename);
        $ftp_handle = $this->connectFTP();
        $this->log('info', 'Connect FTP : <info>success</info>');
        $this->log('info', 'Sending file : ' . $filename);
        if (ftp_put($ftp_handle, $ftpfilename, $fullfilename, FTP_ASCII)) {
            $this->log('info', 'Sent file : ' . $filename . ' <info>success</info>');
            $this->log('info', 'Updating Stock Entry');
            $stockEntry->setDateSendToVeolog(new DateTime('now'));
            // TODO : Mise à jour du statut du stockEntry ?
            // $stockEntry->setEntryStatus(XXXXX);
            $this->stockEntryManager->save($stockEntry);
            $this->log('info', 'Updated Stock Entry <info>success</info>');
        } else {
            $this->log('info', 'Sent file : ' . $filename . ' <error>error</error>');
        }
        ftp_close($ftp_handle);
    }

    private function getStockEntryRef($stockEntry)
    {
        if ($this->ENVIRONNEMENT == 'DEVELOPEMENT' || $this->ENVIRONNEMENT == 'DEV') {
            $plus = '_DEV';
        } elseif ($this->ENVIRONNEMENT == 'RECETTE' || $this->ENVIRONNEMENT == 'REC') {
            $plus = '_REC';
        } elseif ($this->ENVIRONNEMENT == 'PREPROD' || $this->ENVIRONNEMENT == 'PP') {
            $plus = '_PP';
        } else {
            $plus = '';
        }
        $this->log('info', 'Transform ID to VEOLOG : ' . $stockEntry->getId() . " => " . $stockEntry->getId() . $plus);
        return $stockEntry->getId() . $plus;
    }

    protected function executeCommand()
    {
        $now = new DateTime('now', new DateTimeZone('Europe/Paris'));

        $currentTime = $now->format('d/m/Y H:i:s');

        $this->log('info', $currentTime . ' - Envoie des entrées de stock à VEOLOG (machine : ' .gethostname() . ')');

        $since = new DateTime('now', new DateTimeZone(date_default_timezone_get()));
        $since->modify('-8 hours');

        $stockEntries  = $this->stockEntryManager->findAllToSynch($since);
        $total = count($stockEntries);
        $this->log('info', '--> <comment>' . $total . '</comment> trouvée(s)');

        foreach ($stockEntries as $key => $stockEntry) {
            /**
             * Delay the stock entry based on product creation time
             */
            $stockEntryCreationSafe = true;

            $this->log('info', 'Processing Stock Entry ' . $stockEntry->getId());

            foreach($stockEntry->getStockEntryItems() as $item){
                $productCreationDate = clone $item->getProduct()->getCreated();
                

                if($productCreationDate instanceof DateTime){
                    $nowMinusDelay = new DateTime($now->format('Y-m-d H:i:s'));
                    $nowMinusDelay->modify(self::PRODUCT_CREATION_DELAY);

                    $nowF = new DateTime($now->format('Y-m-d H:i:s'));

                    $this->log('info', "Time now " . $nowF->format('d/m/Y H:i:s'));
                    $this->log('info', "Time minus delay " . $nowMinusDelay->format('d/m/Y H:i:s'));
                    $this->log('info', "Product creation date " . $productCreationDate->format('d/m/Y H:i:s'));

                    if($productCreationDate > $nowMinusDelay){
                        $this->log('info', 'Skipping Stock Entry Item ' . $item->getId() . " - Product " . $item->getProduct()->translate('fr')->getCode() . ' creation date ' . $item->getProduct()->getCreated()->format('d/m/Y H:i:s'));
                        $stockEntryCreationSafe = false;
                    }
                }else{
                    $this->log('info', 'Skipping Stock Entry Item ' . $item->getId());
                    $stockEntryCreationSafe = false;
                }
            }

            if(!$stockEntryCreationSafe){
                continue;
            }
            /**
             * End Delay the stock entry based on product creation time
             */

            $this->log('info', 'Stock Entry ' . ($key+1) . " / " . $total);
            try {
                $filename = $this->createFilename($key);
                $this->generateLocalFile($stockEntry, $filename);
                $this->sendFTP($stockEntry, $filename);
                // Finalement, pas de mail sur l'envoie à VEOLOG
                // $this->mailer->sendStocketEntrySend($stockEntry);
            } catch (\Exception $e) {
                $this->log('info', '<error>Failure</error> : ' . $e->getMessage());
            }
        }
        $this->log('info', $currentTime . ' - Fin du traitement (machine : ' .gethostname() . ')');
    }
}
