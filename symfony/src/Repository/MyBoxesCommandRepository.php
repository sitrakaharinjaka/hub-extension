<?php

namespace App\Repository;

use App\Entity\MyBoxesBox;
use App\Repository\BaseRepository;

/**
 * Class MyBoxesCommandRepository
 */
class MyBoxesCommandRepository extends BaseRepository
{


  /**
   * ================= FRONT ===================
   */

    /**
     * Get all project by state
     *
     * @return mixed
     */
    public function findbyStateNoSendingToVlg($state, $type = "product")
    {
        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.state = :state')
            ->andWhere('p.dateAddVlg IS NULL')
            ->setParameter('state', $state)
            ->orderBy('p.updated', 'DESC');

        if ($type == 'product') {
            $qb->andWhere("p.box IS NULL");
        } else {
            $qb->andWhere("p.box IS NOT NULL");
        }

        return $qb->getQuery()->getResult();
    }


    /**
     * ================= BACK ===================
     */


    /**
     * Get all user query, using for pagination
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        $qb = $this->createQueryBuilder('e')
      ->select('e')
            ->addOrderBy('e.created', 'desc');

        if (count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $alias = 'e';
                if ($key == 'id') {
                    $alias = 'e';
                }

                if ($key == 'stock') {
                    $qb->andWhere($alias . '.' . $key . ' = :' . $key);
                    $qb->setParameter($key, $filter);
                } else {
                    $qb->andWhere($alias . '.' . $key . ' LIKE :' . $key);
                    $qb->setParameter($key, '%' . $filter . '%');
                }
            }
        }

        $query = $qb->getQuery();

        return $query;
    }

    /**
     * Find one for edit
     *
     * @param $id
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->leftJoin('e.box', 'b')
            ->leftJoin('e.items', 'i')
            ->where('e.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        return $query->getOneOrNullResult();
    }

    /**
     * Find last command by user
     *
     * @param array $filters
     * @return mixed
     */
    public function findListableByUser($user)
    {
        $qb = $this->createQueryBuilder('e');
        $qb ->select('e')
            ->leftJoin('e.box', 'b')
            ->leftJoin('e.items', 'i')
            ->andwhere('e.user = :user')
            ->andwhere(
                $qb->expr()->orX(
                    $qb->expr()->andX(
                        $qb->expr()->in('e.state', ':statesD5'),
                        "e.updated > DATE_ADD(CURRENT_DATE(), '-5', 'day')"
                    ),
                    $qb->expr()->andX(
                        $qb->expr()->in('e.state', ':statesD10'),
                        "e.updated > DATE_ADD(CURRENT_DATE(), '-10', 'day')"
                    )
                )
            )
            ->setParameter('user', $user)
            ->setParameter('statesD5', [
                MyBoxesBox::STATE_CMD_USER_PREPARED,
                MyBoxesBox::STATE_CMD_USER_ASKED,
                MyBoxesBox::STATE_CMD_VEOLOG_PREPARED,
                MyBoxesBox::STATE_CMD_VEOLOG_ACCEPTED,
            ])
            ->setParameter('statesD10', [
                MyBoxesBox::STATE_CMD_VEOLOG_REFUSED,
                MyBoxesBox::STATE_CMD_VEOLOG_SENT,
            ]);
        $query = $qb->getQuery();
        return $query->getResult();
    }
}
