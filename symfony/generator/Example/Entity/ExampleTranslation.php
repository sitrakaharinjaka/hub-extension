<?php

namespace App\Entity;

use App\Entity\Traits\AuthorableTrait;
use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\SeoTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;


// Use For Algolia Or Api
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExampleRepository")
 * @ORM\Table(name="dk_example_translation")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ExampleTranslation implements NormalizableInterface
{
    use ORMBehaviors\Translatable\Translation;
    use GedmoTrait;
    /**SEO**/use SeoTrait;/**SEO**/
    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**NAME**/
    /**
     * Name
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $name;

    /**
     * Slug
     *
     * @Gedmo\Slug(fields={"name"}, unique=false)
     * @ORM\Column(length=255, unique=false)
     */
    protected $slug;
    /**NAME**/

    /**MANTRA**/
    /**
     * Mantra
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $mantra;
    /**MANTRA**/

    /**CATEGORY**/
    /**
     * Category
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $category;
    /**CATEGORY**/

    /**MULTIPLES**/
    /**
     * Multiples
     *
     * @ORM\Column(type="array", nullable=true)
     */
    protected $multiples;
    /**MULTIPLES**/

    /**DESCRIPTION**/
    /**
     * Description
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;
    /**DESCRIPTION**/

    /**DESCRIPTIONUI**/
    /**
     * DescriptionUi
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $descriptionUi;
    /**DESCRIPTIONUI**/


    /**PUBLISHDATE**/
    /**
     * Edit
     *
     * @var datetime $publishDate
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $publishDate;
    /**PUBLISHDATE**/

    /**ACTIVE**/
    /**
     * @ORM\Column(type="boolean")
     */
    protected $active = false;
    /**ACTIVE**/

    /**COVER**/
    /**
     * Cover
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="cover_id", referencedColumnName="id", onDelete="SET NULL")
     * @var Media
     */
    protected $cover;
    /**COVER**/

    /**
     * Set id
     *
     * @param integer $id
     * @return Example
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**NAME**/
    /**
     * Set name
     *
     * @param string $name
     * @return Example
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Example
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    /**NAME**/

    /**MANTRA**/
    /**
     * Set mantra
     *
     * @param string $mantra
     * @return Example
     */
    public function setMantra($mantra)
    {
        $this->mantra = $mantra;

        return $this;
    }

    /**
     * Get mantra
     *
     * @return string
     */
    public function getMantra()
    {
        return $this->mantra;
    }
    /**MANTRA**/

    /**DESCRIPTION**/
    /**
     * Set description
     *
     * @param string $description
     * @return Example
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**DESCRIPTION**/

    /**ACTIVE**/
    /**
     * Set active
     *
     * @param boolean $active
     * @return Example
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
    /**ACTIVE**/

    /**COVER**/
    /**
     * @return mixed
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * @param mixed $cover
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    }
    /**COVER**/

    /**PUBLISHDATE**/
    /**
     * @return datetime
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * @param datetime $publishDate
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;
    }
    /**PUBLISHDATE**/

    /**DESCRIPTIONUI**/
    /**
     * @return mixed
     */
    public function getDescriptionUi()
    {
        return $this->descriptionUi;
    }

    /**
     * @param mixed $descriptionUi
     */
    public function setDescriptionUi($descriptionUi)
    {
        $this->descriptionUi = $descriptionUi;
    }
    /**DESCRIPTIONUI**/

    /**CATEGORY**/
    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }
    /**CATEGORY**/

    /**MULTIPLES**/
    /**
     * @return mixed
     */
    public function getMultiples()
    {
        return $this->multiples;
    }

    /**
     * @param mixed $multiples
     */
    public function setMultiples($multiples)
    {
        $this->multiples = $multiples;
    }
    /**MULTIPLES**/


    /**
     * Use to serialize object for Algolia or Api
     *
     * @param NormalizerInterface $normalizer
     * @param null $format
     * @param array $context
     * @return array|bool|float|int|string
     */
    public function normalize(NormalizerInterface $normalizer, $format = null, array $context = array())
    {
        return [
            'id' => $this->getId(),
            /**NAME**/'name' => $this->getName(),/**NAME**/
            'locale' => $this->getLocale(),
            /**DESCRIPTION**/'description' => $this->getDescription(),/**DESCRIPTION**/
            /**DESCRIPTIONUI**/'descriptionUi' => $this->getDescriptionUi(),/**DESCRIPTIONUI**/
            /**NAME**/'slug' => $this->getSlug(),/**NAME**/
            /**ACTIVE**/'active' => $this->getActive(),/**ACTIVE**/
            'translatable_id' => $this->getTranslatable()->getId(),
        ];
    }
}
