<?php

namespace App\Command\Veolog\MyBoxes;

use App\Command\Veolog\AbstractVeologCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\MyBoxesBox;
use App\Utils\Managers\MyBoxesBoxManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @property  mailer
 */
class ForecastOfReceiptCommand extends AbstractVeologCommand
{
    private $boxManager;

    protected static $defaultName = 'myboxes:forecast-of-receipt';

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, MyBoxesBoxManager $boxManager)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->boxManager = $boxManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Send mail to VEOLOG with all boxes sent by user')
            ->setHelp('This command allows you to send a mail to veolog to forecast of boxes receipt.');
    }

    private function getBoxOnRoadToVEOLOG()
    {
        return $this->boxManager->findBy(['state' => MyBoxesBox::STATE_BOX_USER_SENT]);
    }

    protected function executeCommand()
    {
        $boxes = $this->getBoxOnRoadToVEOLOG();
        $this->log('info', 'Envoi du mail pour prévenir VEOLOG sur les Box vers chez eux : <comment>' . count($boxes) . '</comment>');
        if (count($boxes) > 0) {
            $temps = [];
            foreach ($boxes as $box) {
                $sku = $box->getProduct()->getSku();
                $temps[$sku] = isset($temps[$sku]) ? $temps[$sku] : 0;
                $temps[$sku]++;
            }
            $items = [];
            foreach ($temps as $sku => $qty) {
                $items[] = [
                    'quantity' => $qty,
                    'name' => $sku,
                ];
            }
            $this->log('info', 'Envoi du mail : ');
            if ($this->mailer->sendMailMyBoxesForecastOfReceipt($items)) {
                $this->log('info', 'Envoi du mail : <info>success</info>');
            } else {
                $this->log('info', 'Envoi du mail : <error>erreur</error>');
            }
        }
        $this->log('info', '<info>Fin</info>');
    }
}
