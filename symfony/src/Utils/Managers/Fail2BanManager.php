<?php

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;

use Gaufrette\Filesystem;
use App\Entity\Fail2Ban;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class Fail2BanManager
 * `
 * Object manager of fail2Ban
 *
 * @package Disko\PageBundle\Managers
 */
class Fail2BanManager extends BaseManager
{
    /**
     * @var Fail2BanRepository
     */
    protected $fail2BanRepository;

    /**
     * Fail2BanManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager = null)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('fail2BanRepository', Fail2Ban::class);
    }


    /**
     * Save a fail2Ban
     *
     * @param Fail2Ban $fail2Ban
     */
    public function save(Fail2Ban $fail2Ban)
    {
        $this->getEntityManager()->persist($fail2Ban);
        $this->getEntityManager()->flush();

        return $fail2Ban;
    }
    /**
     * @param Fail2Ban $fail2Ban
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(Fail2Ban $fail2Ban)
    {
        $this->getEntityManager()->remove($fail2Ban);
        $this->getEntityManager()->flush();
    }

    /**
     * Get all fail2Ban
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->fail2BanRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->fail2BanRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->fail2BanRepository->findOneBy($filters);
    }

    /**
     * Count bad log
     *
     * @param $ip
     * @param $time
     * @return mixed
     */
    public function countBadLogBefore($ip, $time = '-1 hour')
    {
        return $this->fail2BanRepository->countBadLogBefore($ip, $time);
    }
}
