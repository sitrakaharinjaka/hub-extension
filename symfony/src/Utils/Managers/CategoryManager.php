<?php

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;

use App\Utils\Managers\LocaleManager;
use App\Entity\Category;
use App\Entity\CategoryTranslation;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class CategoryManager
 * `
 * Object manager of category
 *
 * @package Disko\PageBundle\Managers
 */
class CategoryManager extends BaseManager
{

    /**
     * Repository
     *
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * CategoryManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('categoryRepository', Category::class);
    }

    /**
     * Save a category
     *
     * @param Category $category
     */
    public function save(Category $category)
    {
        $category->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($category);
        $this->getEntityManager()->flush();

        return $category;
    }

    /**
     * Remove one category
     *
     * @param Category $category
     */
    public function remove(Category $category)
    {
        $this->getEntityManager()->remove($category);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one Active
     *
     * @param $slugWl
     * @param $locale
     * @return mixed
     */
    public function findOneActive($slugWl, $locale)
    {
        $category = $this->categoryRepository->findOneActive($slugWl, $locale);

        return $category;
    }

    /**
     * Find one to edit
     *
     * @param $id
     *
     * @return mixed
     */
    public function findOneToEdit($id)
    {
        $category = $this->categoryRepository->findOneToEdit($id);

        return $category;
    }

    /**
     * Get all category
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->categoryRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->categoryRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->categoryRepository->findOneBy($filters);
    }


    /**
     * Find all active
     *
     * @return mixed
     */
    public function findAllActive()
    {
        return $this->categoryRepository->findAllActive();
    }
}
