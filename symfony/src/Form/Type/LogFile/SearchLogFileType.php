<?php

/**
 * Form type
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Form\Type\LogFile;

use App\Form\Model\SearchLogFile;
use App\Utils\Services\FilesService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 *  Form Type
 *
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class SearchLogFileType extends AbstractType
{
    private $filesService;


    /**
     * SearchLogFileType constructor.
     *
     * @param $logsService
     */
    public function __construct(FilesService $logsService)
    {
        $this->filesService = $logsService;
    }


    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', ChoiceType::class, [
            'choices' => $this->filesService->findFiles(),
            'expanded' => false,
            'multiple' => false,
            'label' => false,
            'attr' => ['class' => 'log-choice']
        ]);

        $builder->add('nbLine', ChoiceType::class, [
            'choices' => [
                20 => 20,
                50 => 50,
                100 => 100,
                250 => 250
            ],
            'expanded' => false,
            'multiple' => false,
            'label' => false,
            'attr' => ['class' => 'log-choice']
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'kering',
            'logs' => [],
            'data_class' => SearchLogFile::class
        ]);
    }
}
