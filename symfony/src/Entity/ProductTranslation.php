<?php

namespace App\Entity;

use App\Entity\Traits\AuthorableTrait;
use App\Entity\Traits\GedmoTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\Constraints as Assert;

// Use For Algolia Or Api

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(name="dk_product_translation")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ProductTranslation implements NormalizableInterface
{
    use ORMBehaviors\Translatable\Translation;
    use GedmoTrait;
    use AuthorableTrait;

    public const STATE_NEW = 'new';
    public const STATE_OUT_OF_USE = 'out_of_use';

    public const CHAMPAGNE = 'champagne';
    public const COGNAC = 'cognac';
    public const RHUM = 'rhum';
    public const TEQUILA = 'tequila';
    public const SPARKLING_WINE = 'sparkling_wine';
    public const WINE = 'wine';
    public const VODKA = 'vodka';
    public const WHISKY = 'whisky';
    public const OTHER = 'other';

    public const RED = 'red';
    public const WHITE = 'white';
    public const ROSE = 'rose';

    public const LIST_STATES = [
      'app.product.states.'.self::STATE_NEW => self::STATE_NEW,
      'app.product.states.'.self::STATE_OUT_OF_USE => self::STATE_OUT_OF_USE
    ];

    public const LIST_WINE_CAT = [
      'app.product.wine_cat.'.self::CHAMPAGNE => self::CHAMPAGNE,
      'app.product.wine_cat.'.self::COGNAC => self::COGNAC,
      'app.product.wine_cat.'.self::RHUM => self::RHUM,
      'app.product.wine_cat.'.self::TEQUILA => self::TEQUILA,
      'app.product.wine_cat.'.self::WINE => self::WINE,
      'app.product.wine_cat.'.self::SPARKLING_WINE => self::SPARKLING_WINE,
      'app.product.wine_cat.'.self::VODKA => self::VODKA,
      'app.product.wine_cat.'.self::WHISKY => self::WHISKY,
      'app.product.wine_cat.'.self::OTHER => self::OTHER,
    ];

    public const LIST_WINE_COLOR = [
    'app.product.wine_color_list.'.self::RED => self::RED,
    'app.product.wine_color_list.'.self::WHITE => self::WHITE,
    'app.product.wine_color_list.'.self::ROSE => self::ROSE,
  ];

    public const LIST_ORIGIN = [
        'SAP' => 0,
        'NON SAP #1' => 1,
    ];

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Name
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $name;

    /**
     * Slug
     *
     * @Gedmo\Slug(fields={"name"}, unique=false)
     * @ORM\Column(length=255, unique=false)
     */
    protected $slug;

    /**
     * Description
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $description;

    /**
     * @var string|null
     * @ORM\Column(type="string", name="code", nullable=true)
     */
    protected $code;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $place;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $material;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $color;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $state = self::STATE_NEW;

    /**
     * @var string|null
     * @ORM\Column(type="string", name="reference_sap", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $referenceSAP;

    /**
     * @var string|null
     * @ORM\Column(type="string", name="reference_provider", nullable=true)
     */
    protected $referenceProvider;

    /**
     * @var string|null
     * @ORM\Column(type="string", name="valorization", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $valorization;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="is_fragile", options={"default":0})
     */
    protected $isFragile = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="is_expendable", options={"default":0})
     */
    protected $isExpendable = false;

    /**
     * @var float|null
     * @ORM\Column(type="float", name="weight", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $weight;

    /**
     * @var float|null
     * @ORM\Column(type="float", name="width", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $width;

    /**
     * @var float|null
     * @ORM\Column(type="float", name="height", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $height;

    /**
     * @var float|null
     * @ORM\Column(type="float", name="depth", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $depth;

    /**
     * Cover
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="cover_id", referencedColumnName="id", onDelete="SET NULL")
     * @var Media
     */
    protected $cover;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", inversedBy="productTranslation")
     * @var Category
     */
    protected $categories;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Brand", inversedBy="productTranslation")
     * @var Brand
     */
    protected $brand;

    protected $brandOther;

    /**
     * @var Collection|ImageGallery[]
     * @ORM\OneToMany(targetEntity="App\Entity\ImageGallery", mappedBy="productTranslation", cascade={"all"})
     */
    protected $imageGallery;

    /**
     * Origin : 0 => SAP or id of stock
     * @var string
     * @ORM\Column(type="string", name="origin", options={"default":0})
     */
    protected $origin = 0;

    /**
     * @var string
     * @ORM\Column(type="string", name="eanPart", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $eanPart;

    /**
     * @var integer
     * @ORM\Column(type="integer", name="totalUnitsByBox", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $totalUnitsByBox;

    /**
     * @var string
     * @ORM\Column(type="string", name="eanBox", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $eanBox;

    /**
     * @var float|null
     * @ORM\Column(type="float", name="widthBox", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $widthBox;

    /**
     * @var float|null
     * @ORM\Column(type="float", name="heightBox", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $heightBox;

    /**
     * @var float|null
     * @ORM\Column(type="float", name="weightBox", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $weightBox;

    /**
     * @var float|null
     * @ORM\Column(type="float", name="depthBox", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $depthBox;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"Bottle"})
     */
    protected $country;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"Bottle"})
     * @Assert\Type(type="Numeric")
     */
    protected $litrageBottle;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $designationBottle;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $vintage;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"Wine"})
     */
    protected $wineColor;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"Bottle"})
     */
    protected $wineCategory;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"Bottle"})
     * @Assert\Type(type="Numeric")
     */
    protected $degreeAlcohol;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Type(type="Numeric")
     */
    protected $unitConditioning;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $otherInformation;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="crd_boolean", options={"default":0})
     */
    protected $crd = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", name="is_effervescent", options={"default":0})
     */
    protected $isEffervescent = false;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"Bottle"})
     */
    protected $customsNomenclature;

    public function __construct()
    {
        $this->imageGallery = new ArrayCollection();
    }

    /**
       * Set id
       *
       * @param integer $id
       * @return Product
       */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Product
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * @param mixed $cover
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string|null
     */
    public function getPlace(): ?string
    {
        return $this->place;
    }

    /**
     * @param string|null $place
     */
    public function setPlace(?string $place): void
    {
        $this->place = $place;
    }

    /**
     * @return string|null
     */
    public function getMaterial(): ?string
    {
        return $this->material;
    }

    /**
     * @param string|null $material
     */
    public function setMaterial(?string $material): void
    {
        $this->material = $material;
    }

    /**
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param string|null $color
     */
    public function setColor(?string $color): void
    {
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }

    /**
     * @return string|null
     */
    public function getReferenceSAP(): ?string
    {
        return $this->referenceSAP;
    }

    /**
     * @param string|null $referenceSAP
     */
    public function setReferenceSAP(?string $referenceSAP): void
    {
        $this->referenceSAP = $referenceSAP;
    }

    /**
     * @return string|null
     */
    public function getReferenceProvider(): ?string
    {
        return $this->referenceProvider;
    }

    /**
     * @param string|null $referenceProvider
     */
    public function setReferenceProvider(?string $referenceProvider): void
    {
        $this->referenceProvider = $referenceProvider;
    }

    /**
     * @return string|null
     */
    public function getValorization(): ?string
    {
        return $this->valorization;
    }

    /**
     * @param string|null $valorization
     */
    public function setValorization(?string $valorization): void
    {
        $this->valorization = $valorization;
    }

    /**
     * @return bool
     */
    public function isFragile(): ?bool
    {
        return $this->isFragile;
    }

    /**
     * @param bool $isFragile
     */
    public function setIsFragile(bool $isFragile): void
    {
        $this->isFragile = $isFragile;
    }

    /**
     * @return bool
     */
    public function isExpendable(): ?bool
    {
        return $this->isExpendable;
    }

    /**
     * @param bool $isExpendable
     */
    public function setIsExpendable(bool $isExpendable): void
    {
        $this->isExpendable = $isExpendable;
    }

    /**
     * @return float|null
     */
    public function getWeight(): ?float
    {
        return $this->weight;
    }

    /**
     * @param float|null $weight
     */
    public function setWeight(?float $weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return float|null
     */
    public function getWidth(): ?float
    {
        return $this->width;
    }

    /**
     * @param float|null $width
     */
    public function setWidth(?float $width): void
    {
        $this->width = $width;
    }

    /**
     * @return float|null
     */
    public function getHeight(): ?float
    {
        return $this->height;
    }

    /**
     * @param float|null $height
     */
    public function setHeight(?float $height): void
    {
        $this->height = $height;
    }

    /**
     * @return float|null
     */
    public function getDepth(): ?float
    {
        return $this->depth;
    }

    /**
     * @param float|null $depth
     */
    public function setDepth(?float $depth): void
    {
        $this->depth = $depth;
    }

    /**
     * @return ArrayCollection|null
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return string
     */
    public function getCategoriesString()
    {
        if (is_null($this->categories)) {
            return null;
        }
        $response = '';
        foreach ($this->categories as $category) {
            if ($response != '') {
                $response.= ', ';
            }
            $response.= $category->getName();
        }
        return $response;
    }

    /**
     * @param ArrayCollection $categories
     */
    public function setCategories($categories)
    {
        foreach ($categories as $categorie) {
            if ($this->hasCategory($categorie)) {
                //$categories->removeElement($categorie);
            }
        }
        $this->categories = $categories;
    }

    public function hasCategory($category)
    {
        $cats = $this->getCategories();
        if (!empty($cats)) {
            foreach ($cats as $cat) {
                if ($category->getId() == $cat->getId()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param Brand $brand
     */
    public function setBrand(?Brand $brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return ArrayCollection|null
     */
    public function getImageGallery()
    {
        return $this->imageGallery;
    }

    /**
     * @param ArrayCollection $imageGallery
     */
    public function setImageGallery($imageGallery)
    {
        $this->imageGallery = $imageGallery;
    }

    /**
     * @param ImageGallery $imageGallery
     * @return $this
     */
    public function addImageGallery(ImageGallery $imageGallery)
    {
        $this->imageGallery[] = $imageGallery;
        $imageGallery->setProductTranslation($this);
        return $this;
    }

    /**
     * @param ImageGallery $imageGallery
     */
    public function removeImageGallery(ImageGallery $imageGallery)
    {
        $this->imageGallery->removeElement($imageGallery);
    }

    /**
     * @return string|null
     */
    public function getVolume(): ?string
    {
        if ($this->getHeight() && $this->getWidth() && $this->getWeight()) {
            return ((int) $this->getHeight() * (int) $this->getWidth() * (int) $this->getWeight()) / 1000000;
        }
        return null;
    }

    /**
     * @return string
     */
    public function getOrigin()
    {
        $hasPrefix = substr( strtoupper($this->getCode()), 0, 4 ) === "HUB_";

        if($hasPrefix === false){
            $this->origin = 0;
        }else{
            $this->origin = 1;
        }

        return $this->origin;
    }

    /**
     * @param string $origin
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;
    }

    /**
     * @return string
     */
    public function getEanPart()
    {
        return $this->eanPart;
    }

    /**
     * @param string $eanPart
     */
    public function setEanPart($eanPart)
    {
        $this->eanPart = $eanPart;
    }

    /**
     * @return int
     */
    public function getTotalUnitsByBox(): ?int
    {
        return $this->totalUnitsByBox;
    }

    /**
     * @param int $totalUnitsByBox
     */
    public function setTotalUnitsByBox(int $totalUnitsByBox): void
    {
        $this->totalUnitsByBox = $totalUnitsByBox;
    }

    /**
     * @return string
     */
    public function getEanBox()
    {
        return $this->eanBox;
    }

    /**
     * @param string $eanBox
     */
    public function setEanBox($eanBox)
    {
        $this->eanBox = $eanBox;
    }

    /**
     * @return float|null
     */
    public function getWidthBox(): ?float
    {
        return $this->widthBox;
    }

    /**
     * @param float|null $widthBox
     */
    public function setWidthBox(?float $widthBox): void
    {
        $this->widthBox = $widthBox;
    }

    /**
     * @return float|null
     */
    public function getHeightBox(): ?float
    {
        return $this->heightBox;
    }

    /**
     * @param float|null $heightBox
     */
    public function setHeightBox(?float $heightBox): void
    {
        $this->heightBox = $heightBox;
    }

    /**
     * @return float|null
     */
    public function getWeightBox(): ?float
    {
        return $this->weightBox;
    }

    /**
     * @param float|null $weightBox
     */
    public function setWeightBox(?float $weightBox): void
    {
        $this->weightBox = $weightBox;
    }

    /**
     * @return float|null
     */
    public function getDepthBox(): ?float
    {
        return $this->depthBox;
    }

    /**
     * @param float|null $depthBox
     */
    public function setDepthBox(?float $depthBox): void
    {
        $this->depthBox = $depthBox;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string|null
     */
    public function getLitrageBottle(): ?string
    {
        return $this->litrageBottle;
    }

    /**
     * @param string|null $litrageBottle
     */
    public function setLitrageBottle(?string $litrageBottle): void
    {
        $this->litrageBottle = $litrageBottle;
    }

    /**
     * @return string|null
     */
    public function getDesignationBottle(): ?string
    {
        return $this->designationBottle;
    }

    /**
     * @param string|null $designationBottle
     */
    public function setDesignationBottle(?string $designationBottle): void
    {
        $this->designationBottle = $designationBottle;
    }

    /**
     * @return string|null
     */
    public function getVintage(): ?string
    {
        return $this->vintage;
    }

    /**
     * @param string|null $vintage
     */
    public function setVintage(?string $vintage): void
    {
        $this->vintage = $vintage;
    }

    /**
     * @return string|null
     */
    public function getWineColor(): ?string
    {
        return $this->wineColor;
    }

    /**
     * @param string|null $wineColor
     */
    public function setWineColor(?string $wineColor): void
    {
        $this->wineColor = $wineColor;
    }

    /**
     * @return string|null
     */
    public function getWineCategory(): ?string
    {
        return $this->wineCategory;
    }

    /**
     * @param string|null $wineCategory
     */
    public function setWineCategory(?string $wineCategory): void
    {
        $this->wineCategory = $wineCategory;
    }

    /**
     * @return string|null
     */
    public function getDegreeAlcohol(): ?string
    {
        return $this->degreeAlcohol;
    }

    /**
     * @param string|null $degreeAlcohol
     */
    public function setDegreeAlcohol(?string $degreeAlcohol): void
    {
        $this->degreeAlcohol = $degreeAlcohol;
    }

    /**
     * @return string|null
     */
    public function getUnitConditioning(): ?string
    {
        return $this->unitConditioning;
    }

    /**
     * @param string|null $unitConditioning
     */
    public function setUnitConditioning(?string $unitConditioning): void
    {
        $this->unitConditioning = $unitConditioning;
    }

    /**
     * @return string|null
     */
    public function getOtherInformation(): ?string
    {
        return $this->otherInformation;
    }

    /**
     * @param string|null $otherInformation
     */
    public function setOtherInformation(?string $otherInformation): void
    {
        $this->otherInformation = $otherInformation;
    }

    /**
     * @return bool
     */
    public function isCrd(): ?bool
    {
        return $this->crd;
    }

    /**
     * @param bool $crd
     */
    public function setCrd(bool $crd): void
    {
        $this->crd = $crd;
    }

    /**
     * @return bool
     */
    public function isEffervescent(): ?bool
    {
        return $this->isEffervescent;
    }

    /**
     * @param bool $isEffervescent
     */
    public function setIsEffervescent(bool $isEffervescent): void
    {
        $this->isEffervescent = $isEffervescent;
    }

    /**
     * @return string|null
     */
    public function getCustomsNomenclature(): ?string
    {
        return $this->customsNomenclature;
    }

    /**
     * @param string|null $customsNomenclature
     */
    public function setCustomsNomenclature(?string $customsNomenclature): void
    {
        $this->customsNomenclature = $customsNomenclature;
    }

    /**
     * @return mixed
     */
    public function getBrandOther()
    {
        return $this->brandOther;
    }

    /**
     * @param mixed $brandOther
     */
    public function setBrandOther($brandOther): void
    {
        $this->brandOther = $brandOther;
    }

    /**
     * Use to serialize object for Algolia or Api
     *
     * @param NormalizerInterface $normalizer
     * @param null $format
     * @param array $context
     * @return array|bool|float|int|string
     */
    public function normalize(NormalizerInterface $normalizer, $format = null, array $context = array())
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'locale' => $this->getLocale(),
            'description' => $this->getDescription(),
            'slug' => $this->getSlug(),
            'translatable_id' => $this->getTranslatable()->getId(),
        ];
    }
}
