<?php
/**
 * Created by PhpStorm.
 * User: lsimonin
 * Date: 2018-12-03
 * Time: 14:32
 */

namespace App\Event;

use App\Entity\Project;
use Symfony\Component\EventDispatcher\Event;

class ProjectEvent extends Event
{
    const PROJECT_STATE_UPDATED = 'project.state.updated';
    const PROJECT_DELETE = 'project.delete';
    const PROJECT_SAVED_DRAFT = 'project.saved_draft';
    const ADMIN_USER_EXIT_IMPERSONATE = 'admin_user.exit_impersonate';

    public const FROM_FRONT = 'front';
    public const FROM_ADMIN = 'admin';

    /**
     * @var Project
     */
    protected $project;

    /**
     * @var string|null
     */
    protected $from;

    /**
     * ProjectStateUpdatedEvent constructor.
     *
     * @param Project     $project
     * @param string|null $from
     */
    public function __construct(Project $project, ?string $from = null)
    {
        $this->project = $project;
        $this->from = $from;
    }

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @return string|null
     */
    public function getFrom(): ?string
    {
        return $this->from;
    }
}
