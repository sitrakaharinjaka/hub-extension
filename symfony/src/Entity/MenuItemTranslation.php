<?php

namespace App\Entity;

use App\Entity\Traits\AuthorableTrait;
use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\SeoTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;


// Use For Algolia Or Api
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MenuItemRepository")
 * @ORM\Table(name="dk_menuitem_translation")
 * @ORM\HasLifecycleCallbacks
 */
class MenuItemTranslation implements NormalizableInterface
{
    use ORMBehaviors\Translatable\Translation;
    use GedmoTrait;
    
    use AuthorableTrait;

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    
    /**
     * Label
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $label;

    /**
     * Url
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $url;
    

    
    /**
     * Category
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $type = "url";

    
    /**
     * Description
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;


    

    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $active = false;
    

    
    /**
     * Cover
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="cover_id", referencedColumnName="id", onDelete="SET NULL")
     * @var Media
     */
    protected $cover;
    

    /**
     * Set id
     *
     * @param integer $id
     * @return MenuItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set label
     *
     * @param string $label
     * @return MenuItem
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    
    /**
     * Set description
     *
     * @param string $description
     * @return MenuItem
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    

    
    /**
     * Set active
     *
     * @param boolean $active
     * @return MenuItem
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
    

    
    /**
     * @return mixed
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * @param mixed $cover
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    }
    


    
    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }
    

    


    /**
     * Use to serialize object for Algolia or Api
     *
     * @param NormalizerInterface $normalizer
     * @param null $format
     * @param array $context
     * @return array|bool|float|int|string
     */
    public function normalize(NormalizerInterface $normalizer, $format = null, array $context = array())
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'locale' => $this->getLocale(),
            'description' => $this->getDescription(),
            'url' => $this->getUrl(),
            'active' => $this->getActive(),
            'translatable_id' => $this->getTranslatable()->getId(),
        ];
    }
}
