<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\CategoryTranslation;
use App\Utils\Managers\CategoryManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Doctrine\Bundle\FixturesBundle\Fixture;

class LoadCategoryData extends Fixture implements DependentFixtureInterface
{
    private $categoryManager;
    private $localeManager;

    public function __construct(CategoryManager $categoryManager, LocaleManager $localeManager)
    {
        $this->categoryManager = $categoryManager;
        $this->localeManager = $localeManager;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
//        $categories = array(
//            ['name' => 'Accessoires'],
//            ['name' => 'Mobilier'],
//            ['name' => 'Bouteille']
//        );
//
//        $locales = $this->localeManager->findAll();
//
//        foreach ($categories as $category) {
//            $obj = new Category();
//            foreach ($locales as $locale) {
//                $trans = new CategoryTranslation();
//                $trans->setLocale($locale->getSlug());
//                $trans->setName($category['name']);
//                $obj->addTranslation($trans);
//            }
//            $this->categoryManager->save($obj);
//            $this->addReference('admin-category-'.$obj->getId(), $obj);
//        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

    public function getDependencies()
    {
        return [
          LoadLocaleData::class
      ];
    }
}
