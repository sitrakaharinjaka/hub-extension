<?php
/**
 * Created by PhpStorm.
 * User: Luk
 * Date: 20/05/2017
 * Time: 20:35
 */

namespace App\Form\Type\MyBoxes;

use App\Entity\MyBoxesBox;
use App\Entity\MyBoxesProduct;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BoxType extends AbstractType
{
    protected $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('name', TextType::class, [
            'label' => "Nom de votre box",
            'required' => true,
            'attr' => [
            ]
        ]);
        $builder ->add('content', TextareaType::class, [
            'label' => "Contenu de votre box",
            'required' => true,
            'attr' => [
            ]
        ]);

        if ($options['modeSelect'] == 'create') {
            $builder ->add('barcode', TextType::class, [
                'label' => "Code barre box",
                'required' => true,
                'attr' => [
                ]
            ]);
        } elseif ($options['modeSelect'] == 'update') {
            $builder->add('isEmpty', CheckboxType::class, [
                'required' => false,
                'label' => 'app.myboxes.edit.label.isEmpty-update',
                'form_focus_label' => false,
                'form_checkbox' => true,
                'mapped' => true,
                'attr' => ['class' => 'mode-empty-source'],
            ]);
        }

        $builder ->add('seal', TextType::class, [
            'label' => "Numéro de scellé",
            'required' => true,
            'attr' => [
            ]
        ]);

        $builder->add('boxMedias', CollectionType::class, [
            'entry_type' => BoxMediaType::class,
            'allow_add' => true,
            'by_reference' => false,
            'allow_delete' => true,
            'entry_options' => [
                'attr' => ['class' => ''],
            ],
        ]);

        $builder->add('boxUsers', CollectionType::class, [
            'entry_type' => BoxUserType::class,
            'allow_add' => true,
            'by_reference' => false,
            'allow_delete' => true,
            'entry_options' => [
                'attr' => ['class' => ''],
            ],
        ]);

        $builder->add('product', EntityType::class, [
            'class' => MyBoxesProduct::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('p')
                           ->orderBy('p.name', 'ASC');
            },
            'choice_label' => 'name',
            'required'  => true
        ]);

        $builder->add('type', ChoiceType::class, [
            'label' => "Type de box",
            'choices'  => [
                'MyBoxes' => 'perso',
                'MyTEAMBoxes' => 'group',
            ],
            'attr' => [
                'class' => 'box-type',
            ],
            'required' => true,
        ]);
    }

    /**
     * Configure Options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'modeSelect' => null,
            'data_class' => MyBoxesBox::class,
            'validation_groups' => 'Default',
            'validation_groups' => function (FormInterface $form) {
                $data = $form->getData();
                if ($data->getIsEmpty()) {
                    return 'Empty';
                } else {
                    return 'Default';
                }
            }
        ));
    }
}
