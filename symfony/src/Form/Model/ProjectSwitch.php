<?php
/**
 * Created by PhpStorm.
 * User: lsimonin
 * Date: 09/11/2018
 * Time: 17:11
 */

namespace App\Form\Model;

use App\Entity\Project;

class ProjectSwitch
{
    /**
     * @var Project|null
     */
    private $project;

    /**
     * @return Project|null
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project|null $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }
}
