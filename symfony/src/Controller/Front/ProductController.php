<?php


/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Front;

use App\Controller\BaseController;
use App\Entity\Project;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\ProjectManager;
use App\Utils\Managers\StockLineManager;
use App\Utils\Managers\StockManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 *
 * @package App\Controller
 */
class ProductController extends BaseController
{

    /**
     * Productpage
     * @Route("/products/{slug}", name="product_view")
     * @return Response
     */
    public function view(Request $request, ProductManager $productManager, ProjectManager $projectManager, StockLineManager $stockLineManager, StockManager $stockManager, $askLocale = "fr", $slug)
    {
        $product = $productManager->findBySlug($slug);
        if (null === $product) {
            throw $this->createNotFoundException('Element not found');
        }


        return $this->render('hub/product/show.html.twig', [
            'product' => $product,
            'referer' => $request->headers->get('referer'),
            'askLocale' => $askLocale
        ]);
    }

    /**
     * @Route(path="/similaires-products/{id}/{count}", name="product_similars", methods={"GET"})
     * @param Request $request
     * @param         $id
     * @param         $count
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getSimilarProduct(Request $request, ProductManager $productManager, ProjectManager $projectManager, StockLineManager $stockLineManager, $id, $count = 3)
    {
        $product = $productManager->findOneBy(['id' => $id]);
        if (!$product) {
            throw $this->createNotFoundException('Product not found');
        }
        $stock = $this->get('session')->get('current_stock');
        $products = $productManager->getSimilars($stock, $product, $count);


        $project = $this->get('session')->get('current_project', null);
        $datas = $projectManager->getParameters($project);

        $ids = [];
        $objects = [];
        foreach ($products as $product) {
            $ids[] = $product->getId();
            $objects[] = $product;
        }

        $totalWithHolds = $total115WithHolds = $totalPantinWithHolds =null;
        if ($datas['cave']) {
            $totalWithHolds = $stockLineManager->totalStockOnProducts($stock, $datas['cave'], $ids, $datas['dateStart'], $datas['dateEnd'], true);

            if($datas['cave'] == Project::CAVE_PANTIN){
                $totalWithHolds = [];
                foreach($objects as $object){
                    $totalWithHolds[] = [
                        'total' => $stockLineManager->quantityOf($object, Project::CAVE_PANTIN, new \DateTime('now'), $stock, false),
                        'id' => $object->getId()
                    ];
                }
            }
        } else {
            if ($stock->getCave() == Project::CAVE_115) {
                $total115WithHolds = $stockLineManager->totalStockOnProducts($stock, Project::CAVE_115, $ids, $datas['dateStart'], $datas['dateEnd'], true);
            } elseif ($stock->getCave() == Project::CAVE_PANTIN) {
                //$totalPantinWithHolds = $stockLineManager->totalStockOnProducts($stock, Project::CAVE_PANTIN, $ids, $datas['dateStart'], $datas['dateEnd'], true);
                $totalPantinWithHolds = [];
                foreach($objects as $object){
                    $totalPantinWithHolds[] = [
                        'total' => $stockLineManager->quantityOf($object, Project::CAVE_PANTIN, new \DateTime('now'), $stock, false),
                        'id' => $object->getId()
                    ];
                }
            }
        }

        return $this->render('hub/product/partials/_horizontalList.html.twig', [
            'products' => $products,
            'totalWithHolds' => $totalWithHolds,
            'total115WithHolds' => $total115WithHolds,
            'totalPantinWithHolds' => $totalPantinWithHolds,
        ]);
    }


    /**
     * @param Request             $request
     * @param                     $productId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Route(path="/projects/{idProject}/en-stock/{id}", name="product_in_stock")
     */
    public function inStock(Request $request, $idProject, $id, ProductManager $productManager, StockLineManager $stockLineManager, ProjectManager $projectManager)
    {
        $user = $this->getUser();
        $product = $productManager->findOneBy(['id' => $id]);
        if (!$product) {
            throw $this->createNotFoundException("Product not found");
        }

        $stock = $this->get('session')->get('current_stock');

        $project = $projectManager->findOneBy(['id' => $idProject]);
        if (!$project or $project->getUser()->getId() != $user->getId()) {
            throw $this->createNotFoundException("No project found");
        }

        $datas = $projectManager->getParameters($project);

        $totalAvailable = $stockLineManager->totalStockDateOnProduct($stock, $datas['cave'], $product->getId(), $datas['dateStart'], $datas['dateEnd'], true);

        if($datas['cave'] == Project::CAVE_PANTIN){
            $totalAvailable = $stockLineManager->quantityOf($product, Project::CAVE_PANTIN, new \DateTime('now'), $stock, false);
        }

        return $this->render('hub/product/partials/_stock_alone.html.twig', [
            'totalAvailable' => $totalAvailable
        ]);
    }
}
