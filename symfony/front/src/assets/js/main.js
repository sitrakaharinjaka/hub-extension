import select2 from 'select2/dist/js/select2.full.js';

(function (window, document, $) {
  var $win = $(window);
  var $doc = $(document);

  var $body = $('body');

  var classes = {
    Hover: 'hover',
    Active: 'active',
    ShowNavMain: 'show-nav-main',
    FixedHeader: 'show-fixed-header',
    PageLoad: 'page-load',
  };

  var is = {
    Mobile: false,
    Desktop: false,
    Tablet: false,
  };

  var get = {
    Scroll: 0,
    WinWidth: 0,
  };

  var lastScrollTop = 0;

  /*
      # - Ready
    */

  addDeviceResolution();
  addBaseClickEvents();
  fieldFocusLabel();
  convertImgToSVG();
  datepickerInit();
  selectInit();
  ajaxIntro();
  categoryStateHandler();
  maxChar();
  productGallery();
  tabs();
  quantityCounter();
  ajaxProjectEdit();
  projectDelete();
  headerHandler();

  /*
      # - Load
    */

  $win.on('load', function () {
    $body.addClass(classes.PageLoad);
  });

  /*
      # - Scroll
    */

  $win.on('scroll', function () {
    get.Scroll = $win.scrollTop();

    $body.toggleClass(classes.FixedHeader, get.Scroll < 63);

    if (get.Scroll > 63) {
      headerHandler();
    }
  });

  /*
      - # - Resize Orientationchange
    */

  $win.on('resize orientationchange', function () {
    addDeviceResolution();

    productGallery();
  });

  /*
      # - Functions -
    */

  function headerHandler() {
    var st = $(this).scrollTop();
    var $header = $('.header');

    if (!$header.hasClass('animating')) {
      if (st > lastScrollTop) {
        // downscroll code
        $header.removeClass('slideDown').addClass('slideUp animating');
        $('.navbar-brand')
          .removeClass('slideDown')
          .addClass('slideUp animating');
      } else {
        // upscroll code
        $header.removeClass('slideUp').addClass('slideDown animating');
        $('.navbar-brand')
          .removeClass('slideUp')
          .addClass('slideDown animating');
      }

      lastScrollTop = st;

      setTimeout(function () {
        $header.removeClass('animating');
      }, 100);
    }
  }

  function projectDelete() {
    $('.jsProjectDelete').click(function (e) {
      var $this = $(this);
      var projectTitle = $this
        .closest('tr')
        .find('td:first-child h4')
        .text();

      e.preventDefault();

      setTimeout(function () {
        $('.message-wrapper--delete')
          .addClass('visible')
          .find('p span')
          .text(projectTitle);
      }, 50);
    });

    $('.jsProjectDeleteConfirm').click(function (e) {
      var $this = $(this);
      var projectTitle = $this
        .closest('.message__inner')
        .find('p span')
        .text();

      e.preventDefault();

      $('.table-projects-alt tr').each(function () {
        var $this = $(this);

        if ($this.find('h4').text() == projectTitle) {
          $this.remove();

          $('.message-wrapper--delete').removeClass('visible');
        }
      });
    });
  }

  function quantityCounter() {
    $('.jsCounter a').click(function (event) {
      event.preventDefault();

      var $this = $(this);
      var $qtyField = $this.closest('.jsCounter').find('.qty-field');
      var fieldVal = parseInt($qtyField.val(), 10) || 0;
      var min = $qtyField.attr('min');
      var max = $qtyField.attr('max');

      if ($this.hasClass('qty-less')) {
        if (fieldVal != min) {
          fieldVal--;
        }
      } else {
        if (fieldVal < max) {
          fieldVal++;
        }
      }

      $qtyField.val(fieldVal);
    });
  }

  function tabs() {
    $('.jsTabs .tabs__nav a').click(function (e) {
      var $this = $(this);
      var index = $this.parent().index();
      var $tabs = $this.closest('.jsTabs');

      e.preventDefault();

      $this
        .parent()
        .add($tabs.find('.tab:nth-child(' + (index + 1) + ')'))
        .addClass('active')
        .siblings()
        .removeClass('active');
    });
  }

  function productGallery() {
    var $gallery = $('.jsProductGallery');
    var $image = $gallery.find('a');

    if (is.Mobile) {
      if (!$gallery.hasClass('initialized-mobile')) {
        $gallery
          .addClass('initialized-mobile')
          .removeClass('initialized');

        $image.click(function (e) {
          e.preventDefault();
        });

        $image.each(function () {
          var $this = $(this);
          var imgLargeSrc = $this.data('image-large');

          // set slider(gallery) images
          $this.css('background-image', 'url(' + imgLargeSrc + ')');
        });

        // gallery goes to slider
        if (!$gallery.hasClass('slick-initialized')) {
          $gallery.slick({
            dots: true,
            arrows: false,
          });
        }
      }
    } else {
      // desktop gallery functionality
      if (!$gallery.hasClass('initialized')) {
        $gallery
          .removeClass('initialized-mobile')
          .addClass('initialized');

        $image.each(function () {
          var $this = $(this);
          var imgSrc = $this.data('image-small');
          var imgLargeSrc = $this.data('image-large');

          // set gallery images
          $this.css('background-image', 'url(' + imgSrc + ')');

          // preload images to avoid blinking when changing the main image
          $('<img />')
            .attr('src', imgLargeSrc)
            .appendTo('body')
            .css('display', 'none');
        });

        $image.click(function (e) {
          var $this = $(this);
          var imgSrc = $this.data('image-large');

          e.preventDefault();

          // switch gallery current state
          $this
            .parent()
            .addClass('current')
            .siblings()
            .removeClass('current');

          // change main image
          $this
            .closest('.product__image')
            .find('.product__image-main')
            .css('background-image', 'url(' + imgSrc + ')');
        });

        if ($gallery.hasClass('slick-initialized')) {
          $gallery.slick('unslick');
        }
      }
    }
  }

  function maxChar() {
    var $elem = $('[data-max-char]');

    $elem.each(function () {
      var $this = $(this);
      var maxChar = $this.data('max-char');

      if ($this.text().length > maxChar) {
        $this.text($this.text().substring(0, maxChar) + '...');
      }
    });
  }

  function categoryStateHandler() {
    $('.jsCategories li').click(function (e) {
      e.preventDefault();

      $(this)
        .addClass('current')
        .siblings()
        .removeClass('current');
    });
  }

  function ajaxIntro() {
    $('.jsAjaxIntro').click(function (e) {
      e.preventDefault();

      var $this = $(this);
      var url = $this.attr('href');
      var jqxhr = $.ajax(url)
        .done(function (response) {
          var $data = $(response).find('.intro__inner');

          $('.intro__inner').addClass('fadeout');

          setTimeout(function () {
            $('.intro')
              .addClass('intro--alt')
              .find('.intro__inner')
              .remove();

            $('.intro .container')
              .prepend($data)
              .find('.intro__inner')
              .addClass('absolute');

            fieldFocusLabel();
            datepickerInit();
            selectInit();
            convertImgToSVG();

            setTimeout(function () {
              $('.intro__inner').addClass('fadein');
              $('.intro__inner').removeClass('absolute');
            }, 50);
          }, 400);
        })
        .fail(function () {
          console.log('ajax loading error');
        });
    });
  }

  function ajaxProjectEdit() {
    $('.jsProjectEdit').click(function (e) {
      e.preventDefault();

      var $this = $(this);
      var url = $this.attr('href');
      var jqxhr = $.ajax(url)
        .done(function (response) {
          var $data = $(response)
            .find('.form__sidebar')
            .addClass('fadeout');

          $('.form__sidebar').addClass('fadeout');

          setTimeout(function () {
            $('.form__sidebar').remove();

            $('.form__inner').append($data);

            fieldFocusLabel();
            datepickerInit();
            selectInit();
            convertImgToSVG();

            setTimeout(function () {
              $('.form__sidebar')
                .removeClass('fadeout')
                .addClass('fadein');
            }, 50);
          }, 400);
        })
        .fail(function () {
          console.log('ajax loading error');
        });
    });
  }

  function closePopup() {
    $('.jsPopupClose').click(function (e) {
      e.preventDefault();

      var $this = $(this);

      var $popup = $this.closest('.popup');

      $popup.removeClass('visible');

      setTimeout(function () {
        $popup.remove();
      }, 400);

      $body.removeClass('show-popup');
    });
  }

  function selectInit() {
    $('select.custom-select').selectric({
      maxHeight: 263,
      disableOnMobile: false,
      nativeOnMobile: false,
      onOpen: function () {
        $('.selectric-scroll')
          .niceScroll({
            cursorwidth: '5px',
            cursorcolor: '#d8b693',
            railpadding: {
              top: 0,
              right: 0,
              left: 0,
              bottom: 10,
            },
            autohidemode: false,
          })
          .show();

        $('.selectric-scroll')
          .getNiceScroll()
          .resize();

        var $this = $(this);

        $this.closest('.js-group').addClass('filled');
      },
      onClose: function () {
        var $this = $(this);

        if (
          !$this
            .closest('.selectric-wrapper')
            .find('.selected.highlighted').length
        ) {
          if (!is.Mobile) {
            $this.closest('.js-group').removeClass('filled');
          }
        }

        if (
          $this
            .closest('.selectric-wrapper')
            .find('.label')
            .text().length
        ) {
          $this.closest('.jsFocusLabel').addClass('filled');
        }

        $this.on('change', function () {
          $this.closest('.js-group').addClass('filled');
        });
      },
    });

    $('select.custom-select-simple').selectric({
      maxHeight: 235,
      disableOnMobile: false,
      nativeOnMobile: false,
      onOpen: function () {
        var $this = $(this);

        $this.closest('.js-group').addClass('filled');
      },
      onClose: function () {
        var $this = $(this);

        if (
          !$this
            .closest('.selectric-wrapper')
            .find('.selected.highlighted').length
        ) {
          $this.closest('.js-group').removeClass('filled');
        }

        $this.on('change', function () {
          $this.closest('.js-group').addClass('filled');
        });
      },
    });
  }

  function datepickerInit() {
    $('.datepicker').each(function () {
      var $this = $(this);
      var $wrapper = $this.closest('.datepicker-wrapper');

      $this.datepicker({
        minDate: '0',
        showOtherMonths: true,
        selectOtherMonths: true,
        beforeShow: function (textbox, instance) {
          $('.datepicker-wrapper').append($('#ui-datepicker-div'));
          $wrapper.addClass('darken');

          if ($('.selectric-wrapper').length) {
            $('.selectric-wrapper').removeClass(
              'selectric-hover selectric-open'
            );
          }

          // if ($(this).data('sync-date')) {
          // 	var $field = $(this);
          // 	var $target = $($field.data('sync-date'));

          // 	if ($target.val().length) {
          // 		var date = $target.datepicker('getDate'),
          //          day  = date.getDate(),
          //          month = date.getMonth() + 1,
          //          year =  date.getFullYear();
          //      selectedDate = month + ',' + day + ',' + year;

          //   $field.datepicker('setDate', new Date(selectedDate) );

          //   $field.closest('.jsFocusLabel').addClass('filled');
          // 	}
          // }
        },
        regional: 'fr',
        onClose: function (dateText, inst) {
          $wrapper.removeClass('darken');

          if (!$this.val().length) {
            $this.closest('.jsFocusLabel').removeClass('filled');
          }
        },
        onSelect: function (date) {
          var $field = $(this);

          $field.closest('.jsFocusLabel').addClass('filled');

          if ($field.hasClass('jsDateStart')) {
            var $fieldEnd = $field
              .closest('form')
              .find('.jsDateEnd');
            var date2 = $field.datepicker('getDate');

            date2.setDate(date2.getDate() + 1);
            $fieldEnd.datepicker('setDate', date2);
            //sets minDate to dt1 date + 1
            $fieldEnd.datepicker('option', 'minDate', date2);

            $fieldEnd.closest('.jsFocusLabel').addClass('filled');
          }
        },
      });
    });

    $.datepicker.regional.fr = {
      closeText: 'Fechar',
      monthNames: [
        'Janvier',
        'Février ',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Aout',
        'Septembre',
        'Octobre',
        'Novembre',
        'décembre',
      ],
      monthNamesShort: [
        'Jan',
        'Fév',
        'Mar',
        'Avr',
        'Mai',
        'Jui',
        'Juil',
        'Aou',
        'Sep',
        'Oct',
        'Nov',
        'Déc',
      ],
      dayNamesShort: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
      dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
      dateFormat: 'dd/mm/yy',
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: '',
    };

    $.datepicker.setDefaults($.datepicker.regional['fr']);
  }

  // CONVERTING IMG SVG TO INLINE SVG
  // Easy to manipulate with class
  // To use just add class="svg" to your image
  function convertImgToSVG() {
    $('img.svg').each(function () {
      var $img = $(this);
      var imgID = $img.attr('id');
      var imgClass = $img.attr('class');
      var imgURL = $img.attr('src');

      $.get(
        imgURL,
        function (data) {
          // Get the SVG tag, ignore the rest
          var $svg = $(data).find('svg');

          // Add replaced image's ID to the new SVG
          if (typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
          }

          // Add replaced image's classes to the new SVG
          if (typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass + ' replaced-svg');
          }

          // Remove any invalid XML tags
          $svg = $svg.removeAttr('xmlns:a');

          if (!$svg.attr('viewBox')) {
            $svg.attr(
              'viewBox',
              '0 0 ' +
              $svg.attr('width') +
              ' ' +
              $svg.attr('height')
            );
          }

          // Replace image with new SVG
          $img.replaceWith($svg);
        },
        'xml'
      );
    });
  }

  function fieldFocusLabel() {
    if ($('.jsFocusLabel').length) {
      $('.jsFocusLabel .field:not(.datepicker)').each(function () {
        var $field = $(this);
        var $row = $field.closest('.js-group');

        $field
          .on('focus', function () {
            $row.addClass('focused');
          })
          .on('blur', function () {
            $row.removeClass('focused');

            if ($field.val().length) {
              $row.addClass('filled');
            } else {
              $row.removeClass('filled');
            }
          });
      });

      // Datepicker
      $('.jsFocusLabel .datepicker').each(function () {
        var $field = $(this);
        var $row = $field.closest('.js-group');

        $field.click(function () {
          $row.addClass('filled');
        });

        if (is.Mobile) {
          $field.change(function () {
            $row.addClass('filled');
          });
        }
      });

      $('.jsFocusLabel select').change(function () {
        var $this = $(this);

        if ($this.find('option:selected').length) {
          $this.closest('.jsFocusLabel').addClass('filled');
        }
      });
    }
  }

  function addBaseClickEvents() {
    $body.on('click touchstart', function (event) {
      var $target = $(event.target);

      if (is.Tablet || is.Mobile) {
        if (!$target.parents('.nav').length) {
          $('.nav li').removeClass(classes.Hover);
        }
      }

      if (!$target.parents('.navbar-collapse').length) {
        $('.navbar-toggler')
          .addClass('collapsed')
          .attr('aria-expanded', 'false');

        $('.navbar-collapse').removeClass('show');
      }

      if (!$target.parents('.message-wrapper').length) {
        $('.message-wrapper').removeClass('visible');
        $body.removeClass('scroll-disable');
      }
    });

    $('.jsSearchClose').click(function (e) {
      e.preventDefault();

      $('.navbar-toggler')
        .addClass('collapsed')
        .attr('aria-expanded', 'false');

      $('.navbar-collapse').removeClass('show');
    });

    $(document).on('click', '.jsMessageClose', function (e) {
      e.preventDefault();

      $(this)
        .closest('.message-wrapper')
        .removeClass('visible');
      $body.removeClass('scroll-disable');
    });

    $('.nav a').click(function (event) {
      if (is.Tablet || is.Mobile) {
        var $this = $(this);
        var $parent = $this.parent();

        if (
          $parent.find('> ul').length &&
          !$parent.hasClass(classes.Hover)
        ) {
          event.preventDefault();

          $parent
            .toggleClass(classes.Hover)
            .siblings()
            .removeClass(classes.Hover);
        }
      }
    });

    $('.btn-menu').click(function (event) {
      event.preventDefault();

      $body.toggleClass(classes.ShowNavMain);
    });
  }

  function addDeviceResolution() {
    get.WinWidth = $win.width();

    is.Desktop = get.WinWidth > 985;
    is.Mobile = get.WinWidth <= 767;
    is.Tablet = get.WinWidth <= 985 && get.WinWidth >= 768;
  }

  const manageImageUploads = ($container) => {
    const $this = $container;
    const $uploadField = $container.find('.js-upload');

    $uploadField.each(function () {
      const $this = $(this);

      $this.on('change', function () {
        handleTemplate(this);
      });
    });

    $container.on('click', '.js-remove-img', function (event) {
      event.preventDefault();

      const $this = $(event.target);
      const $parent = $this.closest('.preview');

      $parent.remove();
    });

    /**
     * Handle Template and append
     */
    const handleTemplate = (element) => {
      const $appendParent = $(element)
        .closest('.form')
        .find('.form__uploads');

      const template = $($(element).data('template')).html();

      const $clonedTemplate = $(template).clone();

      const firstImageHolder = $clonedTemplate.find('.preview__img')[0];

      $appendParent.append($clonedTemplate);

      readURL(element, firstImageHolder);
    };

    /**
     * Read Url from Input
     */

    const readURL = (input, imageHolder) => {
      if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function (e) {
          $(imageHolder).css({
            background:
              'url(' +
              e.target.result +
              ') no-repeat center center/cover ',
          });

          $(imageHolder)
            .closest('.preview')
            .addClass('uploaded');
        };

        reader.readAsDataURL(input.files[0]);
      }
    };
  };

  manageImageUploads($('.js-form-upload'));

  // Popup

  /*if ($('#popup-covid').length) {
    $.magnificPopup.open({
      items: {
        src: '#popup-covid',
        type: 'inline',
      },
      fixedContentPos: true,
      fixedBgPos: true,
      overflowY: 'auto',
      closeBtnInside: true,
      preloader: false,
      midClick: true,
      removalDelay: 300,
    });
  }

  */

  $('.js-link-popup').magnificPopup({
    type: 'inline',
    preloader: false,
  });
  // Delete line

  $('.js-delete-line').on('click', function (e) {
    e.preventDefault();
    $(this)
      .closest('tr')
      .slideToggle();
  });

  $('.js-add-new-row').click(function () {
    var lastname_id = $('.form__row input[type=text]:nth-child(1)')
      .last()
      .attr('id');
    var split_id = lastname_id.split('_');
    var index = Number(split_id[1]) + 1;
    var newel = $('.form__row:last').clone(true);

    $(newel)
      .find('input[type=text]:nth-child(1)')
      .attr('id', 'field-address_' + index);

    $(newel)
      .find('input[type=text]:nth-child(1)')
      .attr('name', 'field-address_' + index);

    $(newel)
      .find('input[type=text]:nth-child(1)')
      .val('');

    $(newel)
      .find('.form__label:nth-child(1)')
      .text('Adresse mail membre ' + index);

    $(newel)
      .find('.form__label:nth-child(1)')
      .attr('for', 'field-address_' + index);

    $(newel).insertAfter('.form__row:last');
    $('.form__row:last').removeClass('filled');
  });

  const initAccordions = ($accordion = $('.js-accordion')) => {
    //Hide the inactive sections
    $('.accordion__section')
      .not('.is-current')
      .find('.accordion__body')
      .hide();

    //Handle the show/hide logic
    $accordion.on('click', '.accordion__head', function (event) {
      const $accordionSection = $(this).closest('.accordion__section');
      const $accordionBody = $accordionSection.find('.accordion__body');

      $accordionBody.stop().slideToggle();

      $accordionSection.toggleClass('is-current');
    });
  };

  initAccordions();

  const initSelect2 = (element = '.js-select') => {
    const $element = $(element);
    if (!$element.length) {
      return false;
    }

    const settings = {
      minimumResultsForSearch: -1,
      allowClear: true,
      width: '100%',
      containerCssClass: 'select__container',
      dropdownCssClass: 'select__dropdown',
    };
    $element.each(function () {
      $(this).select2(settings);
    });
  };

  initSelect2();

  window.postLoadingPageInitSelect2 = function (element) {
    initSelect2(element);
  };

  const initSelect2Image = (element = '.js-select-image') => {
    const $element = $(element);
    if (!$element.length) {
      return false;
    }

    function formatState(state) {
      if (!state.id) {
        return state.text;
      }
      var $state = $(
        '<span><img src="assets/images/temp/photo.png" class="img-profile" /> ' +
        state.text +
        '</span>'
      );
      return $state;
    }

    const settings = {
      minimumResultsForSearch: -1,
      allowClear: true,
      width: '100%',
      templateResult: formatState,
      templateSelection: formatState,
      containerCssClass: 'select__container',
      dropdownCssClass: 'select__dropdown-alt',
    };
    $element.each(function () {
      $(this).select2(settings);
    });
  };

  initSelect2Image();

  function sickySidebar(element) {
    const $element = $(element);
    const $header = $('.header');
    const headerHeight = $header.height();
    const elementTop = $element.offset().top;

    $win.on('scroll', function () {
      const windowTop = $win.scrollTop();


      if (windowTop + headerHeight >= elementTop) {
        $element.addClass('is-sticky')
      } else {
        $element.removeClass('is-sticky')
      }
    });
  }

  const $sectionSidebar = $('.section-default--primary .section__aside')

  if ($sectionSidebar.length) {
    sickySidebar($sectionSidebar)
  }

  /**
   * Form dropdown checks functionality
   */
  const $dropdownChecks = $('.form__dropdown-checks');
  const $dropdownChecksLabel = $('.form__dropdown-checks > span');

  $dropdownChecksLabel.on('click', function () {
    $dropdownChecks.toggleClass('is-open');
  });

  $('.validate-stock-entry').click(function (e) {
    return confirm('Cette action est irréversible. Continuer ?');
  });

  if ($('#project_hourStart')) {
    if (typeof $('#project_dateStart').val() !== 'undefined') {
      var d = new Date();
      var n = d.getHours();
      var day = d.getDate();
      var selectedChoose = false;
      var dateParts = $('#project_dateStart').val().split("/");
      var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);

      if (day == dateObject.getDate()) {
        $("#project_hourStart option").each(function (index) {
          var optionHour = $(this).text().split("- ")
          optionHour = optionHour[1].split("h")
          optionHour = parseInt(optionHour[0])
          //console.log('heure ' + n + ': optionHour ' + optionHour)
          if (optionHour <= n) {
            $(this).attr("disabled", true)
            $(this).removeClass("selected")
          } else {
            if (selectedChoose == false) {
              $(this).attr("selected", true)
              selectedChoose = true
            }
          }
        });
        $("#project_hourStart").selectric('refresh')
      }
    }
  }
})(window, document, window.jQuery);
