<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class ZipUploadsController
 *
 * @Route("/admin/uploads-zip")
 * @package App\Controller
 */
class ZipUploadsController extends BaseController
{
    /**
     * Zip's list
     * @Route("/list", name="admin_zipupload_list")
     * @param Request $request Request
     */
    public function listAction(Request $request)
    {

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des zip d'uploads");

        $path = $this->getPath();
        $files = scandir($path, 1);

        $results = [];
        foreach ($files as $file) {
            if ($file == '.' or $file == '..' or $file == '.gitkeep') {
                continue;
            }

            $result = [];
            $result['inprogress'] = count(explode('uploads_', $file)) <= 1;
            $result['path'] = $file;
            $result['fullpath'] = $path.$file;
            $result['size'] = $this->fileSizeConvert(filesize($path.$file));
            $results[] = $result;
        }


        // View
        return $this->render(
            '@Admin/ZipUploads/list.html.twig',
            array(
                'files' => $results,
            )
        );
    }

    /**
     * Zip's ddl
     * @Route("/ddl/{filename}", name="admin_zipupload_ddl")
     * @param Request $request Request
     */
    public function ddlAction(Request $request, $filename)
    {
        $path = $this->getPath().$filename;

        $file = new File($path);
        return $this->file($file);
    }

    /**
     * Zip's delete
     * @Route("/delete/{filename}", name="admin_zipupload_delete")
     * @param Request $request Request
     */
    public function deleteAction(Request $request, $filename)
    {
        $path = $this->getPath();
        $process = new Process(
            'rm '.$filename,
            $path
        );
        $process->run();


        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            'Le zip "'.$filename.'" a bien été supprimé'
        );

        return $this->redirectToRoute('admin_zipupload_list');
    }


    /**
     * Zip's doit
     * @Route("/do-it", name="admin_zipupload_do_it")
     * @param Request $request Request
     */
    public function doitAction(Request $request)
    {
        $date = new \DateTime();

        $path = $this->getPath();

        $filename = 'uploads_'.$date->format('dmY-His').'.zip';

        $process = new Process(
            'zip -r  '.$filename.' ../../public/uploads/',
            $path
        );

        $process->start();

        return $this->redirectToRoute('admin_zipupload_list');
    }

    /**
     * Converts bytes into human readable file size.
     *
     * @param string $bytes
     * @return string human readable file size (2,87 Мб)
     * @author Mogilev Arseny
     */
    private function fileSizeConvert($bytes)
    {
        $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

        $result = '';
        foreach ($arBytes as $arItem) {
            if ($bytes >= $arItem["VALUE"]) {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", ",", strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
        }
        return $result;
    }


    /**
     * Get path of folder
     *
     * @return mixed|string
     *
     */
    private function getPath()
    {
        $path =  $this->getParameter('kernel.root_dir');
        $path = $path.'/../tools/uploads/';
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        return $path;
    }
}
