<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\Fail2Ban;
use App\Form\Model\SearchFail2Ban;
use App\Form\Type\User\SearchFail2BanType;
use App\Utils\Managers\Fail2BanManager;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Process\Process;

/**
 * Class CustomErrorPagesController
 * @Route("/admin/custom-error-pages")
 * @package App\Controller
 */
class CustomErrorPagesController extends BaseController
{
    /**
     * CustomErrorPages activation page
     *
     * @Route("/list", name="admin_custom_error_pages")
     * @param Request $request Request
     */
    public function index(Request $request)
    {
        // Render view
        return $this->render('@Admin/CustomErrorPages/index.html.twig', array(

        ));
    }
}
