<?php

namespace App\Command\Veolog;

use App\Command\Veolog\AbstractVeologCommand;
use App\Utils\Services\MailerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Psr\Log\LoggerInterface;

use App\Entity\Project;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use App\Utils\Managers\ProjectManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class SynchCheckIntegrationWarning extends AbstractVeologCommand
{

  /** @var  EntityManager */
    private $projectManager;
    private $rootDir;

    public function __construct(MailerService $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger, ProjectManager $projectManager, KernelInterface $kernel)
    {
        parent::__construct($mailer, $parameterBag, $logger);
        $this->projectManager = $projectManager;
        $this->rootDir = $kernel->getProjectDir();
    }

    protected static $defaultName = 'veolog:integration-warning';

    protected function configure()
    {
        $this
            ->setDescription('Check if project are integrated at Veolog otherwise send warning email')
            ->setHelp('Check if project are integrated at Veolog otherwise send warning email');
    }

    protected function executeCommand()
    {
        $this->log('info', 'Find projects to warn');
        
        $projectsSince = new \DateTime();
        $projectsSince->modify('-1 hours');

        $projectsToWarn = $this->projectManager->findProjectsToWarnVeolog($projectsSince);

        if(is_array($projectsToWarn) && count($projectsToWarn) > 0){
            foreach($projectsToWarn as $projectToWarn){
                if($projectToWarn->getState() == Project::STATE_HUB_ACCEPTED_FIXED){
                    $this->mailer->sendMailIntegrationWarningVeolog($projectToWarn);
                }
            }
        } else {
            $this->log('info', 'no project found to warn');
        }
    }
}
