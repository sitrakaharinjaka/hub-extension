<?php

namespace App\Validator\Constraints;

use App\Utils\Managers\MyBoxesBarcodeManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MyBoxesBarcodeValidator extends ConstraintValidator
{
    protected $myBoxesBarcodeManager;

    public function __construct(MyBoxesBarcodeManager $myBoxesBarcodeManager)
    {
        $this->myBoxesBarcodeManager = $myBoxesBarcodeManager;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($box, Constraint $constraint)
    {
        if (!$this->myBoxesBarcodeManager->checkExist($box->getBarcode())) {
            $this
                ->context
                ->buildViolation($constraint->message_exist)
                ->atPath('barcode')
                ->addViolation()
                ;
        } elseif (!$this->myBoxesBarcodeManager->checkFree($box->getBarcode())) {
            $this
                ->context
                ->buildViolation($constraint->message_free)
                ->atPath('barcode')
                ->addViolation()
                ;
        }
    }
}
