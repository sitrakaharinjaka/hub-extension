<?php

namespace App\Entity;

class ImportProduct
{
    protected $file;

    public function __construct()
    {
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file)
    {
        $this->file = $file;
    }
}
