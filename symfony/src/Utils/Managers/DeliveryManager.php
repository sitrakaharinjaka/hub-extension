<?php

/**
 * Manager
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;
use App\Entity\Delivery;
use App\Repository\DeliveryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class DeliveryManager
 * `
 * Object manager of delivery
 */
class DeliveryManager extends BaseManager
{
    /**
    * @var DeliveryRepository
    */
    protected $deliveryRepository;

    /**
     * BaseManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager = null)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('deliveryRepository', Delivery::class);
    }

    /**
     * Save a delivery
     *
     * @param Delivery $delivery
     */
    public function save(Delivery $delivery)
    {
        $delivery->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($delivery);
        $this->getEntityManager()->flush();

        return $delivery;
    }

    /**
     * Remove one delivery
     *
     * @param Delivery $delivery
     */
    public function remove(Delivery $delivery)
    {
        $this->getEntityManager()->remove($delivery);
        $this->getEntityManager()->flush();
    }

    /**
     * Get all delivery
     *
     * @param array $filters
     *
     * @return mixed
     */
    public function queryForSearch($filters = array())
    {
        return $this->deliveryRepository->queryForSearch($filters);
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->deliveryRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->deliveryRepository->findOneBy($filters);
    }


    /**
     * Remove all
     */
    public function removeAll()
    {
        return $this->deliveryRepository->removeAll();
    }
}
