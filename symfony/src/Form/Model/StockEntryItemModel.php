<?php

namespace App\Form\Model;

use App\Entity\Traits\GedmoTrait;
use App\Entity\Traits\AuthorableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\HttpFoundation\File\File;
class StockEntryItemModel
{
    /**
     * Product Id
     */
    protected $productId;

    /**
     * Quantity
     */
    protected $quantity;

    /**
     * Product name
     */
    protected $productName;

    /**
     * Category
     */
    protected $productCategory;

    /**
     * Brand
     */
    protected $productBrand;

    /**
     * Product Code
     */
    protected $productCode;

    /**
     * Product EAN
     */
    protected $productEan;

    /**
     * Product State
     */
    protected $productState;

    /**
     * @var boolean
     */
    protected $crd = false;

    /**
     * Wash Basins
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $washBasins;

    /**
     * Dry Cleaning
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $dryCleaning;

    /**
     * Wash Glasses
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $washGlasses;

    /**
     * Invoice File
     *
     * @var File
     */
    protected $invoiceFile;

    /**
     * Invoice filename
     */
    protected $invoiceFilename;

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param mixed $productName
     */
    public function setProductName($productName): void
    {
        $this->productName = $productName;
    }

    /**
     * @return mixed
     */
    public function getProductCategory()
    {
        return $this->productCategory;
    }

    /**
     * @param mixed $productCategory
     */
    public function setProductCategory($productCategory): void
    {
        $this->productCategory = $productCategory;
    }

    /**
     * @return mixed
     */
    public function getProductBrand()
    {
        return $this->productBrand;
    }

    /**
     * @param mixed $productBrand
     */
    public function setProductBrand($productBrand): void
    {
        $this->productBrand = $productBrand;
    }

    /**
     * @return mixed
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * @param mixed $productCode
     */
    public function setProductCode($productCode): void
    {
        $this->productCode = $productCode;
    }

    /**
     * @return mixed
     */
    public function getProductEan()
    {
        return $this->productEan;
    }

    /**
     * @param mixed $productEan
     */
    public function setProductEan($productEan): void
    {
        $this->productEan = $productEan;
    }

    /**
     * @return mixed
     */
    public function getWashBasins()
    {
        return $this->washBasins;
    }

    /**
     * @param mixed $washBasins
     */
    public function setWashBasins($washBasins): void
    {
        $this->washBasins = $washBasins;
    }

    /**
     * @return mixed
     */
    public function getDryCleaning()
    {
        return $this->dryCleaning;
    }

    /**
     * @param mixed $dryCleaning
     */
    public function setDryCleaning($dryCleaning): void
    {
        $this->dryCleaning = $dryCleaning;
    }

    /**
     * @return mixed
     */
    public function getWashGlasses()
    {
        return $this->washGlasses;
    }

    /**
     * @param mixed $washGlasses
     */
    public function setWashGlasses($washGlasses): void
    {
        $this->washGlasses = $washGlasses;
    }

    /**
     * Get product State
     */ 
    public function getProductState()
    {
        return $this->productState;
    }

    /**
     * Set product State
     *
     * @return  self
     */ 
    public function setProductState($productState)
    {
        $this->productState = $productState;

        return $this;
    }

    /**
     * Get the value of crd
     *
     * @return  boolean
     */ 
    public function getCrd()
    {
        return $this->crd;
    }

    /**
     * Set the value of crd
     *
     * @param  boolean  $crd
     *
     * @return  self
     */ 
    public function setCrd($crd)
    {
        $this->crd = $crd;

        return $this;
    }

    /**
     * Get invoice File
     *
     * @return  File
     */ 
    public function getInvoiceFile()
    {
        return $this->invoiceFile;
    }

    /**
     * Set invoice File
     *
     * @param  File  $invoiceFile  Invoice File
     *
     * @return  self
     */ 
    public function setInvoiceFile(File $invoiceFile)
    {
        $this->invoiceFile = $invoiceFile;

        return $this;
    }

    /**
     * Get invoice filename
     */ 
    public function getInvoiceFilename()
    {
        return $this->invoiceFilename;
    }

    /**
     * Set invoice filename
     *
     * @return  self
     */ 
    public function setInvoiceFilename($invoiceFilename)
    {
        $this->invoiceFilename = $invoiceFilename;

        return $this;
    }
}
