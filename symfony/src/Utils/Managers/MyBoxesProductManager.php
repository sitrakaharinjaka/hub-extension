<?php

/**
 * Manager
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;
use App\Entity\MyBoxesProduct;
use App\Repository\MyBoxesProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Filesystem;

/**
 * Class MyBoxesProductManager
 * `
 * Object manager of user
 */
class MyBoxesProductManager extends BaseManager
{
    /**
    * @var MyBoxesProductRepository
    */
    protected $myBoxesProductRepository;

    /**
     * BaseManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager = null)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('myBoxesProductRepository', MyBoxesProduct::class);
    }

    /**
     * Save a myBoxesProduct
     *
     * @param MyBoxesProduct $myBoxesProduct
     */
    public function save(MyBoxesProduct $myBoxesProduct)
    {
        $myBoxesProduct->setUpdated(new \DateTime());
        $this->getEntityManager()->persist($myBoxesProduct);
        $this->getEntityManager()->flush();

        return $myBoxesProduct;
    }

    /**
     * Remove one myBoxesProduct
     *
     * @param MyBoxesProduct $myBoxesProduct
     */
    public function remove(MyBoxesProduct $myBoxesProduct)
    {
        $this->getEntityManager()->remove($myBoxesProduct);
        $this->getEntityManager()->flush();
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->myBoxesProductRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->myBoxesProductRepository->findOneBy($filters);
    }
}
