#!/bin/bash

BLUE='\033[0;36m'
GREEN='\033[0;32m'
RED='\033[0;31m'
PURPLE='\033[0;35m'
NC='\033[0m'

echo "";
echo "${PURPLE}---------------------------------------${NC}";
echo "${PURPLE}            GIT FLOW GENERATOR         ${NC}";
echo "${PURPLE}---------------------------------------${NC}";
echo "";

BRANCH=$(git rev-parse --abbrev-ref HEAD)
if [[ "$BRANCH" != "master" ]]; then
  echo "${RED}Please move on ${GREEN}master${RED} branch to create all git flow${NC}";
echo "";
echo "";
  exit 1;
fi


BRANCH='preprod'
if [ `git branch --list $BRANCH` ]
then
   echo "${RED}Branch name ${GREEN}$BRANCH ${RED}already exists, just move on it.${NC}";
   git checkout preprod;
else
   echo "${BLUE}Branch ${GREEN}$BRANCH ${BLUE}created !${NC}";
   git checkout -b preprod;git push;
fi

BRANCH='recipe'
if [ `git branch --list $BRANCH` ]
then
   echo "${RED}Branch name ${GREEN}$BRANCH ${RED}already exists, just move on it.${NC}";
   git checkout recipe;
else
   echo "${BLUE}Branch ${GREEN}$BRANCH ${BLUE}created !${NC}";
   git checkout -b recipe;git push;
fi


BRANCH='develop'
if [ `git branch --list $BRANCH` ]
then
   echo "${RED}Branch name ${GREEN}$BRANCH ${RED}already exists, just move on it.${NC}";
   git checkout develop;
else
   echo "${BLUE}Branch ${GREEN}$BRANCH ${BLUE}created !${NC}";
   git checkout -b develop;git push;
fi

echo "";
echo "";
echo "${PURPLE}Job Done ! You are on ${GREEN}develop${PURPLE} now !${NC}";
echo "";
echo "";
