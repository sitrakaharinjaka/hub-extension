<?php

/**
 * Manager
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Utils\Managers;

use App\Utils\Managers\BaseManager;

use Gaufrette\Filesystem;
use App\Entity\Connection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ConnectionManager
 * `
 * Object manager of connection
 *
 * @package App\Utils\Managers\Connection
 */
class ConnectionManager extends BaseManager
{

    /**
     * ConnectionManager constructor.
     *
     * @param EntityManagerInterface       $em
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem, LocaleManager $localeManager = null)
    {
        parent::__construct($em, $fileSystem, $localeManager);
        $this->addRepository('connectionRepository', Connection::class);
    }


    /**
     * Save a connection
     *
     * @param Connection $connection
     */
    public function save(Connection $connection)
    {
        // Save connection
        $connection->setUpdated(new \DateTime());
        $this->em->persist($connection);
        $this->em->flush();

        $this->clearResultCache();

        return $connection;
    }

    /**
     * @param Connection $connection
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(Connection $connection)
    {
        $this->getEntityManager()->remove($connection);
        $this->getEntityManager()->flush();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->connectionRepository->findOneBy($filters);
    }


    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function stats($start, $end)
    {
        return $this->connectionRepository->stats($start, $end);
    }
}
