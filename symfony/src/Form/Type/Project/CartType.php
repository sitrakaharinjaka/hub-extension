<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Paweł Jędrzejewski
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace App\Form\Type\Project;

use App\Entity\Project;
use App\Form\Type\Project\EditableProjectType;
use App\Form\Type\Project\ProjectItemType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CartType extends EditableProjectType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        /** @var Project $project */
        $project = $builder->getData();
        if (null !== $project) {
            if (!$project->isEmpty()) {
                $builder
                    ->add('items', CollectionType::class, [
                        'entry_type' => ProjectItemType::class,
                        'allow_add' => false,
                        'allow_delete' => false,
                        'by_reference' => false,
                        'label' => 'sylius.form.cart.items',
                        'disabled' => $options['show_action']
                    ])
                ;
                if (!$options['show_action']) {
                    $builder->add('submit', SubmitType::class, array(
                        'attr' => ['class' => 'btn  btn--dark form__btn cart-btn'],
                        'label' => 'Valider ma demande',
                    ));
                }
            }
        }
        if (!$options['show_action'] && null !== $project && !$project->isEmpty()) {
            $builder->add('draft', SubmitType::class, [
                'attr'  => ['class' => 'btn btn--grey-dark form__btn cart-btn'],
                'label' => 'Sauvegarder',
            ]);
        }

        $builder->add('button', HiddenType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'show_action' => false,
            'validation_groups' => ['Edit']
        ]);
    }
}
