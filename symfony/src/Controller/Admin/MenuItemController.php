<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\MenuItem;
use App\Entity\MenuItemTranslation;
use App\Form\Type\MenuItem\MenuItemTranslationType;
use App\Form\Type\MenuItem\MenuItemType;
use App\Utils\Managers\MenuItemManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MenuItemController
 * @Route("/admin/menuitems")
 * @package Disko\AdminBundle\Controller
 */
class MenuItemController extends BaseController
{
    /**
     * MenuItem's list
     * @Route("/list/{askLocale}", name="admin_menuitem_list", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listAction(Request $request, MenuItemManager $menuItemManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Configuration du menu');


        $items = $menuItemManager->findAllByPosDeep();

        // Render view
        return $this->render('@Admin/MenuItem/index.html.twig', array(
            'items' => $items,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale
        ));
    }

    /**
     * Create a menuitem
     * @Route("/create", name="admin_menuitem_create")
     * @param Request $request Request
     *
     * @return Response
     */
    public function createAction(Request $request, MenuItemManager $menuItemManager, LocaleManager $localeManager)
    {
        // Response
        $response = new Response();


        // Init form
        $menuitem = new MenuItem();
        $menuitem->setDeepness($request->get('deepness', 1));
        $menuitem->setPosition($request->get('position', 0));
        $menuitem->setParent($request->get('parent', null));
        $form = $this->createForm(MenuItemType::class, $menuitem, [
            'validation_groups' => ['Default']
        ]);

        $response = new Response();

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($menuitem->getTranslations()) > 0) {
                    $menuItemManager->save($menuitem);


                    $items = $menuItemManager->findAllByPosDeep();
                    $response->setContent($this->renderView(
                        '@Admin/MenuItem/partials/list.html.twig',
                        array(
                            'items'           => $items,
                        )
                    ));
                    return $response;
                } else {

                    // Launch the message flash
                    $response->setStatusCode(400);
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            } else {

                // Launch the message flash
                $response->setStatusCode(400);
                $this->get('session')->getFlashBag()->set(
                    'error',
                    "Le formulaire contient des erreurs ou est vide"
                );
            }
        }

        $response->setContent($this->renderView(
            '@Admin/MenuItem/partials/create.html.twig',
            array(
                'form'           => $form->createView(),
                'menuitem'           => $menuitem,
            )
        ));

        // View
        return $response;
    }

    /**
     * Update a menuitem
     * @Route("/edit/{id}", name="admin_menuitem_edit")
     * @param Request $request Request
     * @param integer $id      Id of menuitem
     *
     * @return Response
     */
    public function editAction(Request $request, MenuItemManager $menuItemManager, LocaleManager $localeManager, $id)
    {
        // Find it
        /** @var MenuItem $menuitem */
        $menuitem = $menuItemManager->findOneToEdit($id);
        if (!$menuitem) {
            throw $this->createNotFoundException('MenuItem not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des menus", $this->get("router")->generate("admin_menuitem_list"));
        $breadcrumbs->addItem("Modification");

        $form = $this->createForm(MenuItemType::class, $menuitem, [
            'validation_groups' => ['Default']
        ]);


        $response = new Response();

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($menuitem->getTranslations()) > 0) {
                    $menuItemManager->save($menuitem);


                    $items = $menuItemManager->findAllByPosDeep();
                    $response->setContent($this->renderView(
                        '@Admin/MenuItem/partials/list.html.twig',
                        array(
                            'items'           => $items,
                        )
                    ));
                    return $response;
                } else {

                    // Launch the message flash
                    $response->setStatusCode(400);
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            }

            // Launch the message flash
            $response->setStatusCode(400);
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        $response->setContent($this->renderView(
            '@Admin/MenuItem/partials/edit.html.twig',
            array(
                'form'           => $form->createView(),
                'menuitem'           => $menuitem,
            )
        ));

        // View
        return $response;
    }

    /**
     * Delete one menuitem
     * @Route("/delete/{id}", name="admin_menuitem_delete")
     * @param Request $request Request
     * @param integer $id      Id of menuitem
     */
    public function deleteAction(Request $request, MenuItemManager $menuItemManager, $id)
    {
        // Find it
        /** @var MenuItem $menuitem */
        $menuitem = $menuItemManager->findOneToEdit($id);
        if (!$menuitem) {
            throw $this->createNotFoundException('MenuItem not found');
        }

        $menuItemManager->remove($menuitem);



        $items = $menuItemManager->findAllByPosDeep();
        $response = new Response();
        $response->setContent($this->renderView(
            '@Admin/MenuItem/partials/list.html.twig',
            array(
                'items' => $items,
            )
        ));

        return $response;
    }


    /**
     * Move one menuitem
     * @Route("/move", name="admin_menuitem_move")
     * @param Request $request Request
     * @param integer $id      Id of menuitem
     */
    public function moveAction(Request $request, MenuItemManager $menuItemManager)
    {
        $deepness = $request->get('deepness', null);
        $position = $request->get('position', null);
        $parent = $request->get('parent', null);
        $id = $request->get('id', null);

        if (is_null($deepness) or is_null($position) or is_null($id) or is_null($parent)) {
            throw $this->createNotFoundException("Not found");
        }

        // Find it
        /** @var MenuItem $menuitem */
        $menuitem = $menuItemManager->findOneToEdit($id);
        if (!$menuitem) {
            throw $this->createNotFoundException('MenuItem not found');
        }


        $items = $menuItemManager->findAllByPosDeep();
        $deepnessStop = null;
        $deepnessGap = 0;
        foreach ($items as $item) {
            if (!empty($deepnessStop)) {
                if ($item->getDeepness() > $deepnessStop) {
                    $item->setDeepness($item->getDeepness() + $deepnessGap);
                    $this->getDoctrine()->getManager()->persist($item);
                } else {
                    $deepnessStop = null;
                }
            }

            if ($item->getId() == $id) {
                $deepnessStop = $item->getDeepness();
                $deepnessGap = $deepness - $item->getDeepness();
            }
        }

        $menuitem->setPosition($position);
        $menuitem->setDeepness($deepness);
        $menuitem->setParent($parent);
        $menuItemManager->save($menuitem);



        $this->getDoctrine()->getManager()->clear();

        $items = $menuItemManager->findAllByPosDeep();
        $response = new Response();
        $response->setContent($this->renderView(
            '@Admin/MenuItem/partials/list.html.twig',
            array(
                'items' => $items,
            )
        ));

        return $response;
    }
}
