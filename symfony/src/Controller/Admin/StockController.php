<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin;

use App\Entity\Stock;
use App\Entity\StockTranslation;
use App\Form\Model\SearchStock;
use App\Form\Type\Stock\StockRestrictType;
use App\Form\Type\Stock\StockTranslationType;
use App\Form\Type\Stock\SearchStockType;
use App\Form\Type\Stock\StockType;
use App\Utils\Managers\StockManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\Managers\UserManager;

/**
 * Class StockController
 * @Route("/admin/stocks")
 * @package Disko\AdminBundle\Controller
 */
class StockController extends BaseController
{
    /**
     * Stock's list
     * @Route("/list/{askLocale}", name="admin_stock_list", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listAction(Request $request, StockManager $stockManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchStockType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des stocks');

        // Generate query
        $query = $stockManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/Stock/index.html.twig', array(
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchStock();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setName((isset($filters['name']))   ? $filters['name'] : '');

        return $data;
    }

    /**
     * Get user list
     * @Route("/get-user-list", name="admin_stock_get_user_list_ajax")
     * @param Request $request
     * @param UserManager $userManager
     *
     * @return JsonResponse
     */
    public function getUserListAjax(Request $request, UserManager $userManager)
    {
        $term = $request->query->get('term', '');

        $response = [
            'results' => []
        ];

        if (strlen($term) >= 3) {
            $userList = $userManager->findAllExceptAdmins($term);

            foreach ($userList as $user) {
                $response['results'][] = [
                'id' => $user->getId(),
                'text' => $user->getFullName()
            ];
            }
        }

        return new JsonResponse($response);
    }

    /**
     * Create a stock
     * @Route("/create", name="admin_stock_create")
     * @param Request $request Request
     *
     * @return Response
     */
    public function createAction(Request $request, StockManager $stockManager, LocaleManager $localeManager)
    {
        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");

        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des stocks", $this->get("router")->generate("admin_stock_list"));
        $breadcrumbs->addItem("Création");


        // Init form
        $stock = new Stock();
        $form = $this->createForm(StockType::class, $stock, [
            'validation_groups' => ['Default']
        ]);

        $entityAdmin = $stockManager->findAdminsStock($stock);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($stock->getTranslations()) > 0) {
                    $stockManager->save($stock);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Création terminée"
                    );

                    return $this->redirect($this->generateUrl('admin_stock_edit', array(
                        'id' => $stock->getId()
                    )));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            } else {

                // Launch the message flash
                $this->get('session')->getFlashBag()->set(
                    'error',
                    "Le formulaire contient des erreurs ou est vide"
                );
            }
        }

        // View
        return $this->render(
            '@Admin/Stock/create.html.twig',
            array(
                'form'           => $form->createView(),
                'stock'           => $stock,
                'entityAdmin'       => $entityAdmin,
            )
        );
    }

    /**
     * Update a stock
     * @Route("/edit/{id}", name="admin_stock_edit")
     * @param Request $request Request
     * @param integer $id      Id of stock
     *
     * @return Response
     */
    public function editAction(Request $request, StockManager $stockManager, LocaleManager $localeManager, $id)
    {
        // Find it
        /** @var Stock $stock */
        $stock = $stockManager->findOneToEdit($id);
        if (!$stock) {
            throw $this->createNotFoundException('Stock not found');
        }

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des stocks", $this->get("router")->generate("admin_stock_list"));
        $breadcrumbs->addItem("Modification");

        $form = $this->createForm(StockType::class, $stock, [
            'validation_groups' => ['Default']
        ]);

        $entityAdmin = $stockManager->findAdminsStock($stock);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($stock->getTranslations()) > 0) {
                    $stockManager->save($stock);

                    $stockSession = $this->get('session')->get('current_admin_stock');
                    if ($stockSession->getId() == $stock->getId()) {
                        // Si je modifie le stock courant, alros je le met à jour dans la session.
                        $this->get('session')->set('current_admin_stock', $stock);
                    }

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Mise à jour effectuée"
                    );

                    return $this->redirect($request->headers->get('referer'));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/Stock/edit.html.twig',
            array(
                'form'           => $form->createView(),
                'stock'           => $stock,
                'entityAdmin'       => $entityAdmin
            )
        );
    }

    /**
     * Delete one stock
     * @Route("/delete/{id}", name="admin_stock_delete")
     * @param Request $request Request
     * @param integer $id      Id of stock
     */
    public function deleteAction(Request $request, StockManager $stockManager, $id)
    {
        // Find it
        /** @var Stock $stock */
        $stock = $stockManager->findOneToEdit($id);
        if (!$stock) {
            throw $this->createNotFoundException('Stock not found');
        }

        $stockManager->remove($stock);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été supprimé"
        );

        return $this->redirect($request->headers->get('referer'));
    }



    /**
     * Duplicate one stock
     * @Route("/duplicate/{id}", name="admin_stock_duplicate")
     * @param Request $request Request
     * @param integer $id      Id of stock
     */
    public function duplicateAction(Request $request, StockManager $stockManager, $id)
    {
        // Find it
        /** @var Stock $stock */
        $stock = $stockManager->findOneToEdit($id);
        if (!$stock) {
            throw $this->createNotFoundException('Stock not found');
        }

        $this->getDoctrine()->getManager()->detach($stock);
        foreach ($stock->getTranslations() as $translation) {
            $translation->setId(null);
            $translation->setSlug(null);
            $this->getDoctrine()->getManager()->detach($translation);
        }
        $stockManager->save($stock);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été dupliqué"
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Export csv
     * @Route("/export/{locale}", name="admin_stock_export")
     * @param Request $request
     */
    public function exportAction(Request $request, StockManager $stockManager, $locale)
    {
        $data = $this->initSearch($request);
        $query = $stockManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Nom',
            'Date de création',

        ], ';');

        $locale = $request->get('locale', $request->getDefaultLocale());
        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->translate($locale)->getName(),
                ($result->translate($locale)->getCreated()) ? $result->translate($locale)->getCreated()->format('d/m/Y') : $result->getCreated()->format('d/m/Y'),

            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_'.$locale.'_stocks_export.csv"'
        ));
    }


    /**
     * Choice stock
     *
     * @param Request $request
     */
    public function choice(Request $request, StockManager $stockManager)
    {
        // Get Stocks
        $stocks = $stockManager->findByAdminUser($this->getUser());

        // View
        return $this->render(
            '@Admin/Stock/partials/choice.html.twig',
            array(
                'stocks'           => $stocks,
            )
        );
    }

    /**
     * Choice stock
     *
     * @param Request $request
     */
    public function current(Request $request, StockManager $stockManager)
    {
        $stock = $this->get('session')->get('current_admin_stock', null);

        // Get Stocks
        if ($stock) {
            $stock = $stockManager->findOneBy(['id' => $stock->getId()]);
        }

        // View
        return $this->render(
            '@Admin/Stock/partials/current.html.twig',
            array(
                'stock'=> $stock,
            )
        );
    }



    /**
     * Change stock
     *
     * @param Request $request
     * @Route("/change", name="admin_change_stock")
     */
    public function change(Request $request, StockManager $stockManager)
    {
      $previousUrl = $request->headers->get('referer');

      $id = $request->get('id', null);
        if (!$id) {
            throw $this->createNotFoundException("Id missing");
        }

        // Get Stocks
        $stocks = $stockManager->findByAdminUser($this->getUser());

        $find = false;
        $stockFind = null;
        foreach ($stocks as $stock) {
            if ($id == $stock->getId()) {
                $find = true;
                $stockFind = $stock;
            }
        }

        if ($find) {
            $this->get('session')->set('current_admin_stock', $stockFind);

            // Redirect to correct URL stock line if previous URL does not contain selected cave
            if (strpos($previousUrl, 'admin/stock-lines/pantin/list') or strpos($previousUrl, 'admin/stock-lines/115/list')) {
              return $this->redirectToRoute('admin_stockLine_list', ['cave' => $stockFind->getCave()]);
            }

            if (strpos($previousUrl, 'admin/stock-lines/pantin/ajouter') or strpos($previousUrl, 'admin/stock-lines/115/ajouter')) {
                return $stockFind->getCave() == '115'? $this->redirect(str_replace('pantin', '115', $previousUrl)): $this->redirect(str_replace('115', 'pantin', $previousUrl));
            }
        } else {
            throw $this->createAccessDeniedException("This stock is not accessbile for you");
        }

        // View
        return $this->redirect($request->headers->get('referer', $this->generateUrl('admin_homepage')));
    }




    /**
     * Update a stock
     * @Route("/edit/restrict/current", name="admin_stock_edit_current")
     * @param Request $request Request
     *
     * @return Response
     */
    public function editRestrictAction(Request $request, StockManager $stockManager, LocaleManager $localeManager)
    {
        $currentStock = $this->get('session')->get('current_admin_stock');

        // Find it
        /** @var Stock $stock */
        $stock = $stockManager->findOneToEdit($currentStock->getId());
        if (!$stock) {
            throw $this->createNotFoundException('Stock not found');
        }

        $entityAdmin = $stockManager->findAdminsStock($stock);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem("Liste des stocks", $this->get("router")->generate("admin_stock_list"));
        $breadcrumbs->addItem("Modification");

        $form = $this->createForm(StockRestrictType::class, $stock, [
            'validation_groups' => ['Default']
        ]);

        // Update method
        if ('POST' === $request->getMethod()) {

            // Bind value with form
            $form->handleRequest($request);

            if ($form->isValid()) {
                if (count($stock->getTranslations()) > 0) {
                    $stockManager->save($stock);

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'notice',
                        "Mise à jour effectuée"
                    );

                    return $this->redirect($request->headers->get('referer'));
                } else {

                    // Launch the message flash
                    $this->get('session')->getFlashBag()->set(
                        'error',
                        "Le formulaire est entièrement vide"
                    );
                }
            }

            // Launch the message flash
            $this->get('session')->getFlashBag()->set(
                'error',
                "Le formulaire contient des erreurs"
            );
        }

        // View
        return $this->render(
            '@Admin/Stock/edit.html.twig',
            array(
                'currentStockEdit' => true,
                'form'           => $form->createView(),
                'stock'           => $stock,
                'entityAdmin' => $entityAdmin
            )
        );
    }
}
