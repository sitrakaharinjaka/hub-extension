<?php

/**
 * Service
 *
 * @author Adrien Jerphagnon <adrien.j@disko.fr>
 */

namespace App\Utils\Services;

use Symfony\Component\Finder\Finder;

/**
 * Class UserService
 * `
 * Object manager of user
 *
 * @package Disko\PageBundle\Services
 */
class FilesService
{
    private $folder;

    /**
     * LogService constructor.
     *
     * @param $folder
     */
    public function __construct($folder)
    {
        $this->folder = $folder;
    }


    public function findFiles($folder = null)
    {
        if (null === $folder) {
            $folder = $this->folder;
        }
        $finder = new Finder();
        $finder->files()->in($folder);
        $logs = [];
        foreach ($finder as $file) {
            $logs[$file->getRelativePathname()] = $file->getPathname();
        }

        return $logs;
    }
}
