<?php

// src/Form/Type/User/ForgetType.php
namespace App\Form\Type\User;

use App\Form\Model\Forget;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ForgetType extends AbstractType
{
    /**
     * On va ici construire le formulaire en ajoutant les champs un par un
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
        ;
    }

    /**
     * On surcharge la configuration pour définir la classe modèle qui doit être associée
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Forget::class,
        ]);
    }
}
