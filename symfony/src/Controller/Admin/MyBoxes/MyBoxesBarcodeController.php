<?php

/**
 * Admin controller for manager a module
 *
 * @author Adrien Jerphagnon <adrien.jerphagnon@disko.fr>
 */

namespace App\Controller\Admin\MyBoxes;

use App\Entity\MyBoxesBarcode;
use App\Form\Model\SearchMyBoxesBarcode;
use App\Form\Type\MyBoxesBarcode\SearchMyBoxesBarcodeType;
use App\Utils\Managers\MyBoxesBarcodeManager;
use App\Utils\Managers\LocaleManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MyBoxesBarcodeController
 * @Route("/admin/myBoxesBarcodes")
 * @package Disko\AdminBundle\Controller
 */
class MyBoxesBarcodeController extends BaseController
{
    /**
     * MyBoxesBarcode's list
     * @Route("/list/{askLocale}", name="admin_myBoxesBarcode_list", defaults={"askLocale": "fr"})
     * @param Request $request Request
     */
    public function listAction(Request $request, MyBoxesBarcodeManager $myBoxesBarcodeManager, LocaleManager $localeManager, $askLocale = "fr")
    {
        // init search
        $data = $this->initSearch($request);

        // Init form
        $form = $this->createForm(SearchMyBoxesBarcodeType::class, $data);

        // Breadcrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Tableau de bord', $this->get("router")->generate("admin_homepage"));
        $breadcrumbs->addItem('Liste des codes barres');

        // Generate query
        $query = $myBoxesBarcodeManager->queryForSearch($data->getSearchData());

        // Paginate transform
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            20,
            array('wrap-queries'=>true)
        );

        // Render view
        return $this->render('@Admin/MyBoxesBarcode/index.html.twig', array(
            'pagination' => $pagination,
            'locales' => $localeManager->findAllSlug(),
            'askLocale' => $askLocale,
            'form' => $form->createView()
        ));
    }

    /**
     * Init Search object
     * @param Request $request Request
     */
    protected function initSearch(Request $request)
    {
        // Filters get
        $filters = $request->query->get('search', array());

        // Init form
        $data = new SearchMyBoxesBarcode();
        $data->setId((isset($filters['id'])) ? $filters['id'] : '');
        $data->setCode((isset($filters['code']))   ? $filters['code'] : '');

        return $data;
    }


    /**
     * Delete one myBoxesBarcode
     * @Route("/delete/{id}", name="admin_myBoxesBarcode_delete")
     * @param Request $request Request
     * @param integer $id      Id of myBoxesBarcode
     */
    public function deleteAction(Request $request, MyBoxesBarcodeManager $myBoxesBarcodeManager, $id)
    {
        // Find it
        /** @var MyBoxesBarcode $myBoxesBarcode */
        $myBoxesBarcode = $myBoxesBarcodeManager->findOneToEdit($id);
        if (!$myBoxesBarcode) {
            throw $this->createNotFoundException('MyBoxesBarcode not found');
        }

        $myBoxesBarcodeManager->remove($myBoxesBarcode);

        // Launch the message flash
        $this->get('session')->getFlashBag()->set(
            'notice',
            "L'élément a bien été supprimé"
        );

        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * Export csv
     * @Route("/export/{locale}", name="admin_myBoxesBarcode_export")
     * @param Request $request
     */
    public function exportAction(Request $request, MyBoxesBarcodeManager $myBoxesBarcodeManager, $locale)
    {
        $data = $this->initSearch($request);
        $query = $myBoxesBarcodeManager->queryForSearch($data->getSearchData());
        $results = $query->getResult();

        $handle = fopen('php://memory', 'r+');
        // Précision de l'encodage pour Excel ...
        fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($handle, [
            '#',
            'Code',
            'Utilisé',
            'Date de création',
            
        ], ';');

        $locale = $request->get('locale', $request->getDefaultLocale());
        foreach ($results as $result) {
            $fileRow = [
                $result->getId(),
                $result->getCode(),
                $myBoxesBarcodeManager->checkFree($result->getCode()) ? 'Non' : 'Oui',
                ($result->getCreated()) ? $result->getCreated()->format('d/m/Y') : '',
                
            ];
            fputcsv($handle, $fileRow, ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="'.date('Y-m-d').'_'.$locale.'_barcodes_export.csv"'
        ));
    }
}
